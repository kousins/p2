function tricontour(p,t,Hn,N)

% Contouring for functions defined on triangular meshes
%
%   tricontour(p,t,F,N)
%
% Draws contours of the surface F, where F is defined on the triangulation
% [p,t]. These inputs define the xy co-ordinates of the points and their
% connectivity:
%
%   p = [x1,y1; x2,y2; etc],            - xy co-ordinates of nodes in the 
%                                         triangulation
%   t = [n11,n12,n13; n21,n23,n23; etc] - node numbers in each triangle
%
% The last input N defines the contouring levels. There are several
% options:
%
%   N scalar - N number of equally spaced contours will be drawn
%   N vector - Draws contours at the levels specified in N
%
% A special call with a two element N where both elements are equal draws a
% single contour at that level.
%
% Type "contourdemo" for some examples.

% This function does NOT interpolate back onto a Cartesian grid, but
% instead uses the triangulation directly.
%
% If your going to use this inside a loop with the same [p,t] a good
% modification is to make the connectivity "mkcon" once outside the loop
% because usually about 50% of the time is spent in "mkcon".
%
% Darren Engwirda - 2005 (d_engwirda@hotmail.com)

% I/O checking
if nargin~=4
    error('Incorrect number of inputs')
end
if nargout>0
    error('Incorrect number of outputs')
end

% Error checking
if (size(p,2)~=2) || (size(t,2)~=3) || (size(Hn,2)~=1)
    error('Incorrect input dimensions')
end
if size(p,1)~=size(Hn,1)
    error('F and p must be the same length')
end
if (max(t(:))>size(p,1)) || (min(t(:))<=0)
    error('t is not a valid triangulation of p')
end
if (size(N,1)>1) && (size(N,2)>1)
    error('N cannot be a matrix')
end

numt = size(t,1);

% Make mesh connectivity data structures (edge based pointers)
[e,eINt,e2t] = mkcon(p,t);

% Nodes in triangles and edges
t1 = t(:,1); t2 = t(:,2); t3 = t(:,3); e1 = e(:,1); e2 = e(:,2);

% Centroids and edge midpoints
pt = (p(t1,:)+p(t2,:)+p(t3,:))/3;
pe = (p(e1,:)+p(e2,:))/2;

% Interpolate to centroids and edge midpoints
Ht = (Hn(t1)+Hn(t2)+Hn(t3))/3;   
He = (Hn(e1)+Hn(e2))/2;  

% DEAL WITH CONTOURING LEVELS
if length(N)==1
    lev = linspace(max(Ht),min(Ht),N+1);
    num = N;
else
    if (length(N)==2) && (N(1)==N(2))
        lev = N(1);
        num = 1;
    else
        lev = sort(N);
        num = length(N);
        lev = lev(num:-1:1);
    end
end

% MAIN LOOP
x   = [];
y   = [];
z   = [];
in  = false(numt,1);
vec = 1:numt;
old = in;

for v = 1:num       % Loop over contouring levels
    
    % Find centroid values >= current level
    i     = vec(Ht>=lev(v));
    i     = i(~old(i));         % Don't need to check triangles from higher levels
    in(i) = true;
    
    % Locate boundary edges in group
    bnd  = [i; i; i];
    next = 1;
    for k = 1:length(i)
        ct    = i(k);
        count = 0;
        for q = 1:3     % Loop through edges in ct
            ce = eINt(ct,q);
            if ~in(e2t(ce,1)) || ((e2t(ce,2)>0)&&~in(e2t(ce,2)))    
                bnd(next) = ce;     % Found bnd edge
                next      = next+1;
            else
                count = count+1;    % Count number of non-bnd edges in ct
            end
        end
        if count==3                 % If 3 non-bnd edges ct must be in middle of group
            old(ct) = true;         % & doesn't need to be checked for the next level
        end
    end
    bnd = bnd(1:next-1);
    
    % Place nodes approximately on contours by interpolating across bnd edges
    numb  = next-1;
    penew = pe;
    cc    = zeros(2*numb,2);
    for k = 1:numb
        
        ce = bnd(k);
        t1 = e2t(ce,1);
        t2 = e2t(ce,2);
        x1 = pt(t1,1);
        y1 = pt(t1,2);
        
        if t2>0     % Move node based on neighbouring centroids
            dx = pt(t2,1)-x1;
            dy = pt(t2,2)-y1;
            r  = (lev(v)-Ht(t1))/(Ht(t2)-Ht(t1));
        else        % Move node based on internal centroid & bnd midpoint
            dx = pe(ce,1)-x1;
            dy = pe(ce,2)-y1;
            r  = (lev(v)-Ht(t1))/(He(ce)-Ht(t1));
        end
        
        % New node position
        penew(ce,1) = x1+r*dx;
        penew(ce,2) = y1+r*dy;
        
        % Do a temp connection between adjusted node & endpoint nodes in
        % ce so that the connectivity between neighbouring adjusted nodes
        % can be determined
        m         = 2*k-1;
        cc(m,1)   = e1(ce);
        cc(m,2)   = ce;
        cc(m+1,1) = e2(ce);
        cc(m+1,2) = ce;
        
    end
    
    % Sort connectivity to place connected edges in sucessive rows
    [j,i] = sort(cc(:,1));
    cc    = cc(i,1:2);
    
    % Connect adjacent adjusted nodes
    k    = 1;
    next = 1;
    while k<(2*numb)
        if cc(k,1)==cc(k+1,1)
            cc(next,1) = cc(k,2);
            cc(next,2) = cc(k+1,2);
            next       = next+1;
            k          = k+2;       % Skip over connected edge
        else
            k = k+1;                % Node has only 1 connection - will be picked up above
        end
    end
    cc = cc(1:next-1,1:2);
    
    % xzy data
    xc = [penew(cc(:,1),1),penew(cc(:,2),1)];
    yc = [penew(cc(:,1),2),penew(cc(:,2),2)];
    zc = lev(v) + 0*xc;
    
    % Add contours to list
    x = [x; xc];
    y = [y; yc];
    z = [z; zc];
    
end

patch('Xdata',x','Ydata',y','Cdata',z','facecolor','none','edgecolor','flat','linewidth',2);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [e,eINt,e2t] = mkcon(p,t)

numt = size(t,1);
vect = 1:numt;

% Determine unique edges in mesh
 
e       = [t(:,[1,2]); t(:,[2,3]); t(:,[3,1])];             % Edges - not unique
vec     = (1:size(e,1))';                                   % List of edge numbers
[e,i,j] = unique(sort(e,2),'rows');                         % Unique edges
vec     = vec(j);                                           % Unique edge numbers
eINt    = [vec(vect), vec(vect+numt), vec(vect+2*numt)];    % Unique edges in each triangle


% Determine edge to triangle connectivity

i     = [vect, vect, vect];     % Triangle number associated with each edge
[j,k] = sort(eINt(:));          % Sorted list of unique edges in mesh
i     = i(k);                   % Match cell numbers after sorting

% Loop through elements picking up neighbours
% The 2 columns of the array are the neighbouring cell numbers for 
% that edge. Boundary edges have only 1 neighbour with the 2nd column 0

nume  = size(e,1); 
e2t   = zeros(nume,2); 
count = 1; 
next  = 1;

for k = 1:length(j)
    if j(k)==count
        e2t(count,next) = i(k); 
        next            = next+1;
    else
        count           = count+1;
        next            = 1;
        e2t(count,next) = i(k); 
        next            = next+1;
    end
end

return
