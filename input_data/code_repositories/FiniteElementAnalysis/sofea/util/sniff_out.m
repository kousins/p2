% Search the folders and apply some function to matching files.
%
% function sniff_out(Suffixes, apply)
%
% Search the current directory and its subdirectories And execute the apply
% function on the files. 
%
function sniff_out(Suffixes, apply)
    if  ~exist('Suffixes')
        Suffixes ={'.asv','.log','.png'};
    end
    function  echo_found(x)
        disp( ['Found ' x]);
    end
    if  ~exist('apply')
        apply =@echo_found;
    end
    run_in_subdirs(pwd, Suffixes, apply)
    return;
end

function run_in_subdirs(d, Suffixes, apply)
    sep=filesep;
    dl=dir(d);
    for i=1:length(dl)
%         dl(i)
        if (dl(i).isdir)
            if      (~strcmp(dl(i).name,'.')) && ...
                    (~strcmp(dl(i).name,'..'))
                run_in_subdirs([d sep dl(i).name], Suffixes, apply);
            end
        else
            [pathstr, name, ext, versn] = fileparts(dl(i).name) ;
            for j=1: length(Suffixes)
                if ((strcmp(ext,Suffixes{j})))
                    apply([d sep dl(i).name]);
                end
            end
        end
    end
end
