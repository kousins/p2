% Compute transformation matrix describing local material directions.
%
% function Rm = geniso_Rm(XYZ, tangents)
%
% XYZ = location at which the material directions are needed
% ts = tangent vectors to parametric coordinates in columns
%
% The basic assumption here is that the material is isotropic, and
% therefore the choice of the material directions does not really matter as
% long as they correspond to the dimensionality of the element.
%
% This function assumes that it is being called for
% and ntan-dimensional manifold element, which is embedded in a
% sdim-dimensional Euclidean space. If ntan == sdim,
% the material directions matrix is the identity; otherwise the local
% material directions are aligned with the linear subspace defined by the
% tangent vectors.
% 
% Warning: this *cannot* be reliably used to produce consistent stresses
% because each quadrature point gets a local coordinate system which
% depends on the orientation of the element.
%
function Rm = geniso_Rm(XYZ, tangents)
    [sdim, ntan] = size(tangents);
    if sdim==ntan
        Rm=eye(sdim);
    else
        e1=tangents(:,1)/norm(tangents(:,1));
        switch ntan
            case 1
                Rm = [e1];
            case 2
                n =skewmat(e1)*tangents(:,2)/norm(tangents(:,2));
                e2=skewmat(n)*e1;
                e2=e2/norm(e2);
                Rm = [e1,e2];
            otherwise
                error('Got an incorrect size of tangents');
        end
    end
end
