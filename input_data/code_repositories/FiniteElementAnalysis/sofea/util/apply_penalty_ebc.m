% Apply penalty essential boundary conditions.
%
% function [K,F]=apply_penalty_ebc(K,F,uebc,u,penfact)
%
% K= stiffness matrix
% F= Load vector 
% uebc = field which describes the constraints,
% u= field which does not have the constraints applied, and serves as the source of equation numbers,
% penfact= penalty multiplier
function [K,F]=apply_penalty_ebc(K,F,uebc,u,penfact)
    ip=gather(uebc,(1:get(uebc,'nfens')),'is_prescribed','noreshape');
    ix=find (ip~=0);
    pv=gather(uebc,(1:get(uebc,'nfens')),'prescribed_values','noreshape');
    eqnums=gather(u,(1:get(u,'nfens')),'eqnums','noreshape');
    eqnums=eqnums(ix);
    pv=pv(ix);
    penalty=penfact*max(diag(get(K,'mat')));
    K = assemble (K, elemat(struct ('mat', penalty*eye(length(eqnums)),'eqnums',eqnums)));
    if ~isempty(F)
        F = assemble (F, elevec(struct ('vec', penalty*pv,'eqnums',eqnums)));
    end
    return
end
