% Return the path to the SOFEA work folder.
%
% function p = sofea_work_path
%
function p = sofea_work_path
    p=[sofea_path filesep 'work'];
    return;
end
