% Compute transformation matrix describing local material directions.
%
% function Rm = default_Rm(XYZ)
%
% XYZ = location at which the material directions are needed
% 
% This function assumes that it is being called for
% and n-dimensional manifold element, which is embedded in a
% n-dimensional Euclidean space. Therefore, the most obvious choice for
% the material directions is the identity: the local material directions
% aligned with the global Cartesian axes.
function Rm = default_Rm(XYZ)
    Rm=eye(length(XYZ));
end
