% Visualize a field as arrows. 
%
% function show_field_as_arrows(gv,options)
%
function show_field_as_arrows(gv,options)
x= options.x;
u= options.u;
if isfield(options,'nl')
    nl = options.nl;
else
    nl =(1: get (x,'nfens'));
end
p=gather(x,nl,'values','noreshape');
v=gather(u,nl,'values','noreshape');
for i= nl
    draw_arrow (gv,p(i,:),v(i,:),[]);
end
