function collate(path,file)
% Collate and save to file contents of m-files in a directory tree
% Notes: Calls RDIR, available from File Exchange
% Example: collate('.','listing.txt')
% Dimitri Shvorob, dimitri.shvorob@gmail.com, 7/15/08

if exist(path,'dir') ~=7
   error('??? Path %s not found.',path)
end

% define separator
w = repmat(' ',1,80);
b = repmat('*',1,80);
s = sprintf(repmat('%s\n',1,5),w,b,b,b,w);

c = {};
f = rdir([path '\**\*.m']);

for i = 1:length(f)
    if ~f(i).isdir
       c{end+1} = urlread(['file:///' f(i).name]);  %#ok
    end
end

fid = fopen(file,'w');
fprintf(fid,[s '%s'],c{:});
fclose(fid);