%OB The object browser helper function.
%
% function OB(obj)
%
% Only objects of class classbase and its descendents can be displayed
% properly.  Arrays of objects cannot be handled, only individual objects.
%
% A brief description of an attribute is shown in a tool tip (displayed
% when the mouse is hovered above the button for the attribute).
% 
% NOTE: All the objects presented in the GUI are copies of the objects in
% the workspace. Therefore, no changes to the objects in the workspace will
% be propagated back to the GUI; if a workspace object changes, and we
% would like to use the GUI to inspect it, we have to restart OB (kill the
% current instance, and  repeat previous OB invocation).
% 
% (C) 2004-2007, Petr Krysl
%
function OB(obj)
    if nargin == 0
        error('Need an argument')
    end

    objname=inputname(1);
    if isempty(objname)
        objname='nameless';
    end
    if ~isa(obj,'classbase')
        disp(['Object ' objname ' is of class ' class(obj) ': only descendents of classbase can be handled with ' mfilename]);
        return;
    end
    if length(obj)>1
        prompt = {['There are ' num2str(length(obj)) ' objects in this array. Enter index:']};
        dlg_title = 'Select object';
        num_lines = 1;
        def = {'1'};
        answer = inputdlg(prompt,dlg_title,num_lines,def);
        %disp(['Object ' objname ' is an array of  ' num2str(length(obj)) ' elements: use ' mfilename ' only with individual objects']);
        try 
            index =str2num(answer{1});  
            OB(obj(index));
        catch, end;
%         OB(obj);
        return;
    end
    % determine width values
    ftSize=get(0,'DefaultUIControlFontSize');
    maxLen=48;
    lButt=maxLen*ftSize*0.7;
    scSize=get(0,'ScreenSize');
    distButt=2.3*ftSize;
    winWdth=lButt;
    
    % number of buttons
    getttablefieldns=get(obj);
    nfields=length(getttablefieldns);
    
    winHgth=   (distButt*1.2 +distButt*(nfields));
    winShft=distButt*1.84;
    persistent iShft shftDwn;
    if isempty(iShft)
        iShft= 1; shftDwn=0;
    end
    x=scSize(3)/40+0.75*iShft*winShft+0.6*shftDwn;
    y=scSize(4)-winHgth-iShft*winShft-shftDwn;
    iShft=iShft+1;
    if (x >scSize(3)-winWdth) | (y <winHgth)
        iShft= 1; shftDwn= shftDwn+winShft*1.3;
    end
    
    f1=figure('Position', [x y winWdth winHgth],...
        'Resize','off', 'DeleteFcn',@deleteme);
    set(f1,...
        'MenuBar','none','NumberTitle','off','Name','OB (C) 2005,Petr Krysl',...
        'Units', 'points');
    uicontrol('Style','text','String',[objname ': ' class(obj)],...
        'Position',[0 distButt*(nfields)+2 lButt distButt],...
        'FontSize',ftSize,...
        'FontWeight','bold','BackgroundColor',[0.9 0.77 0.6],...
        'Enable','inactive');
    % place buttons
    for i=1:nfields
        v=get(obj,char(getttablefieldns{i}{1}));
        if (strcmp (class (v),'char') )
            uicontrol('Style','text','String',[char(getttablefieldns{i}{1}) '=' char(v)],...
                'Position',[0 distButt*(nfields-i)+2 lButt distButt],...
                'TooltipString',char(getttablefieldns{i}{2}),...
                'FontSize',ftSize,...
                'Enable','on');
        elseif (length (v) ==1) & (strcmp (class (v),'double') )
            uicontrol('Style','text','String',[char(getttablefieldns{i}{1}) '=' num2str(v)],...
                'Position',[0 distButt*(nfields-i)+2 lButt distButt],...
                'TooltipString',char(getttablefieldns{i}{2}),...
                'FontSize',ftSize,...
                'Enable','on');
        else
            uicontrol('Style','pushbutton','String',getttablefieldns{i}{1},...
                'Position',[0 distButt*(nfields-i)+2 lButt distButt],...
                'TooltipString',char(getttablefieldns{i}{2}),...
                'FontSize',ftSize,...
                'Callback',@buttcall,'UserData',struct('obj',obj,'fig',f1));
        end
    end
    % wait for user reaction before returning
    %waitfor(f1);

    function buttcall(hObject,varargin)
        d=get(hObject,'UserData');
        name=get(hObject,'String');
        obj=d.obj;
        v=get(obj,name{1});
        if isa(v,'classbase')
            OB(v);
        else
            try
                fud=get(d.fig,'UserData');
                if isempty(fud)
                    fud={};
                end
                xy=get(get(hObject,'Parent'),'Position');
                xy=xy( 1:2) +xy(3:4)/2;
                fud{end+1}=ars6(v,name{1}, xy);
                set(d.fig,'UserData',fud);
            catch
            end
        end
        return;
    end

    function deleteme(hObject,varargin)
        for i=get(hObject,'UserData')
            try
                delete(i{1});
            catch
            end
        end
        return;
    end
end
