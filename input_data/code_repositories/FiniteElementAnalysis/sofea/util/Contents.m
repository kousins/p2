% Utilities
%
% Toolbox paths:
%   sofea_path                  - Return the path to the SOFEA topmost folder.
%   sofea_work_path             - Return the path to the SOFEA work folder.
% 
% Browsing of toolbox objects:
%   OB                          - The object browser helper function.
%   OBgui                       - The object browser GUI.
%   ars6                        - Array spreadsheet-like viewer.
%
% Graphics:
%   progressbar                 - Display a status/progress bar and inform about the elapsed
%   savefig                     - Save a figure as an image.
%   show_field_as_arrows        - Visualize a field as arrows. 
%   streamplot                  - Plot the data as a stream tube.
%   sphereplot                  - Plot data Evolving on a sphere.
%   tilefigs                    - Tile figure windows.
%   tricontour                  - Contouring for functions defined on triangular meshes
%   view3d                      - Interactively rotate, zoom and pan the view of a 3-D plot
%   showroom                    - Revolves a 3D plot showroom-style.
%   magnify                     - Install a magnification box into the current figure.
%   scan_on_image               - Collects points by scanning a curve in a graph image.
%   cadcolors                   - Compute a standard CAD 10-color colormap.
% 
% General utilities:
%   apply_penalty_ebc           - Apply penalty essential boundary conditions.
%   gaussquad                   - Gaussian quadrature integration.
%   default_Rm                  - Compute transformation matrix describing local material directions.
%   geniso_Rm                   - Compute transformation matrix describing local material directions.
%   nd_Rm                       - Compute transformation matrix describing local material directions.
%   richextrapol                - Richardson extrapolation.
% 
% Folder utilities:
%   convert                     - Convert between units from different systems.
%   highlight                   - Syntax Highlighting of Matlab M-files in HTML, LaTeX, RTF and XML.
%   sniff_out                   - Search the folders and apply some function to matching files.
%   clean_sweep                 - Clean up in the current directory and its subdirectories by removing designated files.
% 
% Utilities for 3-D rotation dynamics:
%   cayley                      - Compute the Cayley transform.
%   dexp                        - Compute the derivative of the exponential map operator.
%   dexpdot                     - Compute the time derivative of the derivative of the exponential map.
%   dexpinv                     - Compute the inverse of the derivative of the exponential map.
%                                 exponential map.
%   reortho                     - Re-orthogonalize an approximate rotation matrix
%   rotmat2rotvec               - Convert rotation matrix R to rotation vector V
%   rotmat                      - Compute rotation matrix from a rotation vector or from the associated
%   skewmat                     - Compute a skew matrix from its axial vector Theta.
% 

%   myaa                 - Render figure with anti-aliasing.
%   left_handed_axes     - Utility function to enable presentation of cable deflections in a
%   make_patch           - Make a file patch.
%   mlar                 - Matlab plain-text archiver.
