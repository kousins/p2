%OBGUI The object browser GUI.
%
% function OBgui
%
% Launches OB from a GUI. Each SOFEA object will have a button, which can
% be pushed to display the object with OB. If there are too many objects,
% more than one panel with buttons will be displayed.
%
% The class of an object is shown in a tool tip (displayed
% when the mouse is hovered above the button for the object).
% 
% For arrays of objects, the index may be input in the button; the lower
% and upper index are indicated next to the name of the object. Type in the
% index, and then click the button.
% 
% NOTE: All the objects presented in the GUI are copies of the object in
% the workspace. Therefore, no changes to the objects in the workspace will
% be propagated back to the GUI; if a workspace object changes, and we
% would like to use the GUI to inspect it, we have to restart OBgui.
% 
% (C) 2004-2007, Petr Krysl
%
function OBgui

    % determine width values
    ftSize=get(0,'DefaultUIControlFontSize');
    maxLen=48;
    lButt=maxLen*ftSize*0.7;
    scSize=get(0,'ScreenSize');
    distButt=2.3*ftSize;
    winWdth=lButt;

    % number of buttons
    s =evalin('caller','who(''*'')');
    nfields=length(s);
    onfields=0;
    for i=1:nfields
        try, obj=evalin('caller',s{i});
            if isa(obj,'classbase')
                onfields=onfields+1;
            end
            sobj{i} = obj;
        catch,end;
    end

    
    Maxonfields= round(scSize(4)/distButt*2/3);
    remnonfields =onfields;
    confields =min([Maxonfields,remnonfields]);

    f1=[];
    % place buttons
    j=1; cj=1; cf =1;
    for i=1:nfields
        if (isempty(f1)) || ((cj>confields) &&  (remnonfields >0))
            confields =min([Maxonfields,remnonfields]);
            f1=figure('Position', [(scSize(3)/3+winWdth/5*(cf-1)) (scSize(4)/5-distButt*(cf-3)) winWdth (distButt*1.2 +distButt*(confields))],...
                'Resize','off', 'DeleteFcn',@deleteme);
            set(f1,...
                'MenuBar','none','NumberTitle','off','Name','OBgui (C) 2004-2007, Petr Krysl',...
                'Units', 'points');
            uicontrol('Style','text','String', ['Panel ' num2str(cf) ': Objects'],...
                'Position',[0 distButt*(confields)+2 lButt distButt],...
                'FontSize',ftSize,...
                'FontWeight','bold','BackgroundColor',[0.9 0.9 0.96],...
                'Enable','inactive');
            remnonfields =remnonfields-confields;
            cj=1; cf=cf+1;
        end
        obj=sobj{i};
        if isa(obj,'classbase')
%             if length(obj)> 1
%                 e=uicontrol('Style','edit','String','1',...
%                     'Position',[4/5*lButt distButt*(confields-cj)+2 lButt/5 distButt],...
%                     'BackgroundColor',[0.95 0.9 0.9],...
%                     'FontSize',ftSize);
%                 uicontrol('Style','pushbutton','String',[s{i} ' (1:' num2str(length(obj)) ')'],'HorizontalAlignment','left',...
%                     'Position',[0 distButt*(confields-cj)+2 4/5*lButt distButt],...
%                     'TooltipString', class(obj),...
%                     'FontSize',ftSize,...
%                     'Callback',@buttcall,'UserData',struct('obj',obj,'name',s{i},'fig',f1,'edit',e));
%             else
                uicontrol('Style','pushbutton','String',s{i},'HorizontalAlignment','left',...
                    'Position',[0 distButt*(confields-cj)+2 lButt distButt],...
                    'TooltipString', class(obj),...
                    'FontSize',ftSize,...
                    'Callback',@buttcall,'UserData',struct('obj',obj,'name',s{i},'fig',f1));
%             end
            j=j+1; cj=cj+1;
        end
    end
    % wait for user reaction before returning
    %waitfor(f1);
end

function buttcall(hObject,varargin)
    d=get(hObject,'UserData');
    name=d.name;
    if length(d.obj)> 1
        %         e=d.edit;
        %         s=get(e,'String');
        %         try
        %             j= ceil (sscanf(s, '%d'));
        %             if isempty(j)
        %                 j=1;
        %                 set(e,'String',num2str(j));
        %             else
        %                 if (j <1)
        %                     j=1;
        %                     set(e,'String',num2str(j));
        %                 elseif j >length(d.obj)
        %                     j=length(d.obj);
        %                     set(e,'String',num2str(j));
        %                 end
        %             end
        %         catch
        %             j=1;
        %             set(e,'String',num2str(j));
        %         end
        prompt = {['There are ' num2str(length(d.obj)) ' objects in this array. Enter index:']};
        dlg_title = 'Select object';
        num_lines = 1;
        def = {'1'};
        answer = inputdlg(prompt,dlg_title,num_lines,def);
        %disp(['Object ' objname ' is an array of  ' num2str(length(obj)) ' elements: use ' mfilename ' only with individual objects']);
        try
            index =str2num(answer{1});
            eval ([name ' =d.obj;']);
            eval (['OB(' name '(' num2str(index) '))']);
        catch, end;
        %         OB(obj);
        return;
        %         try
        %             eval ([name ' =d.obj;']);
        %             eval (['OB(' name '(' num2str(j) '))']);
        %         catch
        %         end
    else
        try
            eval ([name ' =d.obj;']);
            eval (['OB(' name ')']);
        catch
        end
    end
    return;
end

function deleteme(hObject,varargin)
    for i=get(hObject,'UserData')
        try
            close(i{1});
        catch
        end
    end
    return;
end
