%CADCOLORS Compute a standard CAD 10-color colormap.
%
% function m=cadcolors()
% 
function m=cadcolors(This_is_ignored)
m = [ 0         0    1.0000
         0    0.6667    1.0000
         0    1.0000    1.0000
    0.3333    1.0000    0.6667
    0.5    1.0000    0.6667
    0.6667    1.0000   0.5 
    0.6667    1.0000    0.3333
    1.0000    1.0000         0
    1.0000    0.6667         0
    1.0000    0.        0];
