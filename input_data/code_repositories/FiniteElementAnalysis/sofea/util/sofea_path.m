%SOFEA_PATH Return the path to the SOFEA topmost folder.
%
% function p = sofea_path
%
function p = sofea_path
    s='sofea_init';
    p = which(s);
    p = p(1:length(p)-length(s)-3);
    return;
end
