% Distribute elements of the array on input to a number of variables on output.
% 
% function [varargout] = adeal(x)
% 
% Example:   x=[4,4,3*nLayers];
%            [nL,nW,nt] =adeal(x);
% 
function [varargout] = adeal(x)
    nout = max(nargout,1);
    s = length(x);
    for k=1:nout, varargout{k}= x(k); end
end
