% Compute transformation matrix describing local material directions.
%
% function Rm = nd_Rm(XYZ,tangents)
%
% XYZ = location at which the material directions are needed
% tangents = tangent vectors to parametric coordinates in columns
%
% This function assumes that it is being called for
% an ntan-dimensional manifold element, which is embedded in a
% sdim-dimensional Euclidean space. If ntan == sdim,
% the material directions matrix are the identity; otherwise the local
% material directions are aligned with the linear subspace defined by the
% tangent vectors.
% 
function Rm = nd_Rm(XYZ,tangents)
    [sdim, ntan] = size(tangents);
    if sdim==ntan
        Rm=eye(sdim);
    else
        switch ntan
            case 1
                e1=tangents(:,1)/norm(tangents(:,1));
                Rm = [e1];
            case 2
                normal =skewmat(tangents(:,1))*tangents(:,2);
                e1=tangents(:,1)/norm(tangents(:,1));
                e2=skewmat(normal)*tangents(:,2);
                e2=e2/norm(e2);
                Rm = [e1,e2];
            otherwise
                error('Got an incorrect size of tangents');
        end
    end
end
