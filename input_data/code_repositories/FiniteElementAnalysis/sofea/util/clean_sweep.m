% Clean up in the current directory and its subdirectories by removing designated files.
%
% function clean_sweep(Suffixes)
%
%
function clean_sweep(Suffixes)
    if  ~exist('Suffixes')
        Suffixes ={'.asv','.log'};
    end
    function   remove_found(x)
        try
            delete([x]);
        catch
            disp([x ' could not be deleted'])
        end
    end
%     sniff_out(Suffixes)
    sniff_out(Suffixes,@remove_found)
    return;
end
