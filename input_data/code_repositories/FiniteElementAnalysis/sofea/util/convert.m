function y=convert(x,u1,u2)
%CONVERT  Convert between units from different systems.
%   Y = CONVERT(X,U1,U2) converts number X from unit U1
%   to unit U2 if possible.
%
%   For example,
%      CONVERT(15,'gallon_US_liq','barrel_UK')
%   converts the weight 15 US gallons to UK barrels.
%
%   See also CONSTANT.

% Copyright (c) 2001-12-07, B. Rasmus Anthin.

%Length [m]
m=1;
A=1e-10;
XU=1.00208e-13;
fermi=1e-15;
au_l=.529177*A;
AU=1.49597870e11;
ly=9.46055e15;
pc=3.0857e16;
mile_n=1852;
mile=1609.344;
yd=.9144;
ft=.3048;
in=2.54e-2;
fot=.29690;

%Area [m^2]
m2=1;
barn=1e-28;
tunnland=4936;
acre=4046.86;
ha=1e4;
sqin=.64516e-3;
in2=sqin;
sqft=92.90304e-3;
ft2=sqft;
sqyd=.83612736;
yd2=sqyd;
sqmile=2.5899881e6;
mile2=sqmile;

%Volume [m^3]
m3=1;
l=1/1000;
barrel_UK=163.66*l;
barrel_US_pet=158.98*l;
barrel_US_liq=119.24*l;
gallon_UK=4.54609*l;
gallon_US_liq=3.785411784*l;
gallon_US=gallon_US_liq;
pint_UK=.56825*l;
pint_US_dry=.55060*l;
pint_US_liq=.47316*l;
cuin=.016387064*l;
in3=cuin;
cuft=28.316847*l;
ft3=cuft;
cuyd=.76455486;
yd3=cuyd;

%Angle [rad]
rad_a=1;
deg=pi/180;
o=deg;
min_a=deg/60;
sec=min_a/60;
gon=pi/200;
grad=gon;

%Time [s]
s=1;
min_t=60;
h=3600;
day=24*h;
d=day;
yr_trop=365.24219878*day;
yr=yr_trop;
yr_sid=365.25637*day;
yr_cal=365*day;
yr_leap=366*day;
au_t=2.418884e-17;

%Frequency [Hz]
Hz=1;
s01=1;
rpm=1/60;

%Speed [m/s]
m_s=1;
mph=.44704;
knot=.51444;
knop=knot;
km_h=1/3.6;
fps=.3048;

%Mass [kg]
kg=1;
u=1.66054e-27;
lb=.45359237;
lbm=6.48e-5;
slug=14.593903;
ton=1000;
tn=1016.0469088;
shtn=907.18474;
cwt=50.80234544;
shcwt=45.359237;
grain=.0000648;
oz=.02835;

%Density [mol/m^3]
mol_m3=1;
amagat=.04096/1000;

%Density [kg/m^3]
kg_m3=1;
lb_ft3=16.0185;
lb_in3=27.6799e3;

%Temperature [K]

%Energy [J]
J=1;
eV=1.60217733e-19;
Ry=2.1799e-18;
au_W=2*Ry;
erg=1e-7;
kWh=3.6e6;
cal=4.1868;
kcal=1000*cal;
Btu=1055.06;
quad=10^15*Btu;
Q=1000*quad;

%Power [W]
W=1;
hk=735.5;
hp=745.7;

%Angular momentum [Js]
Js=1;
au_am=1.0545727e-34;

%Force [N]
N=1;
dyn=1e-5;
kp=9.80665;
lbf=4.4482;

%Pressure [Pa]
Pa=1;
torr=1.33322e2;
atm=760*torr;
bar=1e5;
at=9.80665e4;
psi=6.8948e3;

%Current [A]
%A=1;
%abamp=10;
%statamp=3.3356e-10;

%Ampl/Att [Np]
Np=1;
B=log(10)/2;
dB=B/10;

%Electric dipole moment [C m]
Cm=1;
au_de=8.47828e-30;
D=3.3356e-30;
esu=1e18*D;

%Magnetic flux density [T]
T=1;
Vs_m2=1;
Vsm02=1;
G=1e-4;
gamma=1e-9;
gauss=1e-4;

%Magnetic flux [Wb]=[V s]
Wb=1;
Vs=1;
Mx=1e-8;

%Magnetizing field [A/m]
A_m=1;
oersted=1e3/4/pi;

%Magnetic dipole moment [Am^2]=[J/T]
Am2=1;
J_T=1;
au_dm=1.8548e-23;

%Activity [Bq]
Bq=1;
Ci=3.7e10;
Rd=1e6;

%Exposure [C/kg]
C_kg=1;
R=2.58e-4;

%Absorbed dose [J/kg]
J_kg=1;
Gy=1;
rad_d=1e-2;
%Dose equivalent [J/kg]
Sv=1;
rem=1e-2;

%Luminous intensity [cd]
cd=1;
hefner=.9;

%Luminance [cd/m^2]
cd_m2=1;
sb=1e4;
asb=1/pi;
cd_ft2=10.76391;
lambert=1e4/pi;

%Gas exposure [Pa s]
Pas=1;
torrs=1.33e2;
L=1.33e-4;

%Effect flux
W_m2Hz01=1;
Wm02Hz01=1;
W_m2_Hz=1;
W_m2s=1;
Wm02s=1;
Jy=1e-26;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

u1=strrep(u1,' ','');
u2=strrep(u2,' ','');
uu={u1 u2};
uu=strrep(uu,'-','0');
uu=strrep(uu,'^','');
uu=strrep(uu,'*','');
uu=strrep(uu,'/','_');
uu=strrep(uu,'.','_');
uu=strrep(uu,'�','A');
u1=uu{1};u2=uu{2};

%For new quantities always add to bottom!
units={{'m','A','XU','fermi','au','AU','ly','pc','mile_n','mile','yd','ft','in','ft','fot'},...
      {'m2','barn','tunnland','acre','ha','sqin','in2','sqft','ft2','sqyd','yd2','sqmile','mile2'},...
      {'m3','l','barrel_UK','barrel_US_pet','barrel_US_liq','gallon_UK','gallon_US_liq','gallon_US','pint_UK','pint_US_dry','pint_US_liq','cuin','in3','cuft','ft3','cuyd','yd3'},...
      {'rad','deg','o','min','sec','gon','grad'},...
      {'s','min','h','day','d','yr_trop','yr','yr_sid','yr_cal','yr_leap','au'},...
      {'Hz','s01','rpm'},...
      {'m_s','mph','knot','knop','km_h','fps'},...
      {'kg','u','lb','slug','ton','tn','shtn','cwt','shcwt','grain','lbm','oz'},...
      {'mol_m3','amagat'},...
      {'kg_m3','lb_ft3','lb_in3'},...
      {'K','oC','oF','oR'},...
      {'J','eV','Ry','au','erg','kWh','cal','kcal','Btu','quad','Q'},...
      {'W','hk','hp'},...
      {'N','dyn','kp','lbf'},...
      {'Pa','torr','atm','bar','at','psi'},...
      {'Np','B','dB'},...
      {'Cm','au','D','esu'},...
      {'T','Vs_m2','Vsm02','G','gamma','gauss'},...
      {'Wb','Vs','Mx'},...
      {'A_m','oersted'},...
      {'Bq','Ci','Rd'},...
      {'cd','hefner'},...
      {'cd_m2','sb','asb','cd_ft2','lambert'},...
      {'Pas','torrs','L'},...
      {'Js','au'},...
      {'Am2','J_T','au'},...
      {'C_kg','R'},...
      {'J_kg','Gy','rad','Sv','rem'},...
      {'W_m2Hz01','Wm02Hz01','W_m2_Hz','W_m2s','Wm02s','Jy'}};

q1=getquantity(u1,units);
q2=getquantity(u2,units);
if any(strcmp(u1,{'rad','min'})) & any(strcmp(u2,{'rad','min'}))
   u1=[u1 '_a'];
   u2=[u2 '_a'];
else
   for com={'au','rad','min'}
      if all(strcmp({u1,u2},com{1}))               %filters out the possibility of vector q1 and q2
         error('Unable to determine quantity.')
      elseif strcmp(u1,com{1}) & exist(u2)
         u1=changename(u1,q2,com{1});
      elseif strcmp(u2,com{1}) & exist(u1)
         u2=changename(u2,q1,com{1});
      end
   end
end


if ~isempty(q1) & ~isempty(q2) & all(q1~=q2)
   error('Cannot convert between different quantities.')
elseif exist(u1) & exist(u2)
   eval(['y=' num2str(x) '*' u1 '/' u2 ';']);                   %the core of the function
elseif any(strcmp(u1,{'K','oC','oF','oR'})) &...                %special case for temperatures!
      any(strcmp(u2,{'K','oC','oF','oR'}))
   switch u1
   case 'K'
      z=x;
   case 'oC'
      z=x+273.15;
   case 'oF'
      z=x*9/5+32+273.15;
   case 'oR'
      z=1.25*x+273.15;
   end
   switch u2
   case 'K'
      y=z;
   case 'oC'
      y=z-273.15;
   case 'oF'
      y=(z-273.15-32)*5/9;
   case 'oR'
      y=(z-273.15)/1.25;
   end
elseif isempty(q1)
   error(['Cannot convert from unit "' u1 '".'])
elseif isempty(q2)
   error(['Cannot convert to unit "' u2 '".'])
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ii=getquantity(u,units)
ii=[];
for i=1:length(units)
   for j=1:length(units{i})
      if strcmp(u,units{i}{j})
         ii=[ii i];
      end
   end
end

function uu=changename(u,q,prefix)
app='';
switch q    %cannot be a vector due to earlier check
case 1
   app='l';
case 4
   app='a';
case 5
   app='t';
case 12
   app='W';
case 17
   app='de';
case 25
   app='am';
case 26
   app='dm';
case 28
   app='d';
end
uu=strrep(u,prefix,[prefix '_' app]);