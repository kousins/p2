% Mirror a 2-D mesh in a plane given by its normal and one point. 
%
% function [fens1,gcells1] = mirror_mesh(fens, gcells, Normal, Point)
%
% fens, gcells= mesh, 
% Normal, Point= 2-D arrays
%  
% Warning: The code to relies on the numbering of the cells: two reverse
% the orientation of the mirrored cells, the connectivity is listed in
% inverse order.   If the mirrored cells do not comply, their areas will
% come out negative.
% 
function [fens1,gcells1] = mirror_mesh(fens, gcells, Normal, Point)
    Normal = Normal/norm (Normal);
    fens1 =fens;
    for i=1:length(fens1)
        xyz =get(fens1(i),'xyz');
        xyz =xyz-2*dot(Normal,xyz)*Normal;
        fens1(i) =set(fens1(i),'xyz',xyz);
    end
    % Reconnect the cells
    gcells1=gcells;
    for i=1:length(gcells1)
        conn=get(gcells1(i),'conn');
        gcells1(i)=set(gcells1(i),'conn',conn(end:-1:1));
    end
end
