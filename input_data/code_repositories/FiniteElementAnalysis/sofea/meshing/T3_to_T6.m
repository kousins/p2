% Convert a mesh of triangle T3 (three-node) to triangle T6.
%
% function [fens,gcells] = T3_to_T6(fens,gcells,options)
%
% options =attributes recognized by the constructor gcell_T6
function [fens,gcells] = T3_to_T6(fens,gcells,options)
    if ~isstruct(options)
        options = struct('id',1);
    end
    nedges=3;
    ec = [1, 2; 2, 3; 3, 1];
    % make a search structure for edges
    edges={};
    for i= 1:length(gcells)
        conn = get (gcells(i),'conn');
        for J = 1:nedges
            ev=conn(ec(J,:));
            anchor=min(ev);
            if length(edges)<anchor
                edges{anchor}=[];
            end
            edges{anchor}=unique([edges{anchor} max(ev)]);
        end
    end
    % now generate new node number for each edge
    n=length(fens);
    nodes=edges;
    for i= 1:length(edges)
        e=edges{i};
        for J = 1:length(e)
            n=n+1;
            e(J)=n;
        end
        nodes{i}=e;
    end
    fens(length(fens)+1:n)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    % calculate the locations of the new nodes
    % and construct the new nodes
    for i= 1:length(edges)
        e=edges{i};
        n=nodes{i};
        for J = 1:length(e)
            fens(n(J))=fenode(struct ('id',n(J),'xyz',(get(fens(i),'xyz')+get(fens(e(J)),'xyz'))/2));
        end
    end
    nfens=length(fens);% number of nodes in the original mesh plus number of the edge nodes
    % construct new geometry cells
    ngcells(1:length(gcells))=deal(gcell_T6(struct('id',1,'conn',(1:6))));
    nc=1;
    for i= 1:length(gcells)
        conn = get (gcells(i),'conn');
        econn=zeros(1,nedges);
        for J = 1:nedges
            ev=conn(ec(J,:));
            anchor=min(ev);
            e=edges{anchor};
            n=nodes{anchor};
            econn(J)=n(find(e==max(ev)));
        end
        options.id =nc;
        options.conn =[conn econn];
        ngcells(nc) =gcell_T6(options);
        nc= nc+ 1;
    end
    gcells=ngcells;
    return;
end
