% Project nodes onto a sphere of given radius.
%
% function fens= onto_sphere(fens,radius)
%
function fens= onto_sphere(fens,radius)
    for j= 1:length(fens)
        xyz=get(fens(j),'xyz');
        xyz =xyz*radius/norm(xyz);
        fens(j)=set(fens(j),'xyz', xyz);
    end
end
