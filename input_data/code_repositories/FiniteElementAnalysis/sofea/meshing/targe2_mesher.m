% Automatic triangulation of a general 2-D domain. 
%
% function [fens,gcells,groups,edge_gcells,edge_groups] =
%                     targe2_mesher(fs,thickness,varargin)
%
% Automatic triangulation of a general 2-D domain. The input is given as a cell array
% of commands to the mesher Targe2. These are described in the manual,
% targe2_users_guide.pdf.
% Input:
% fs -- cell array of strings, each string a command recognized
%       by the mesher (targe2_users_guide.pdf) 
% thickness -- thickness of the two-dimensional slab
% varargin-- optional argument: structure with name-value pairs
%                - quadratic: convert the triangulation to
%                             quadratic elements, true or false;
%                - merge_tolerance: see this option in
%                             targe2_users_guide.pdf
%                - and any other attributes recognized by the constructors
%                of the geometric cells, for instance axisymm.
% Output:
%  fens= the finite element nodes
%  gcells=the triangles
% The following are optional outputs:
%  groups=cell array of indexes of the triangles in the individual subregions;
%  For instance groups{3} is the list of the triangles that belong to subregion 3
%  edge_gcells=cell array of the edge elements
%  edge_groups=cell array of indexes of the edge elements on the individual edges;
%
function [fens,gcells,groups,edge_gcells,edge_groups] = targe2_mesher(fs,thickness,varargin)
    in='in';
    out ='out';
    quadratic= false;
    
    if nargin >2
        options=varargin{1};
    end
    options.id=0;
    options.conn = [];
    if isfield(options,'axisymm')
        if ~options.axisymm
            options.other_dimension=thickness;
        end
    else
        options.other_dimension=thickness;
    end
    merge_tolerance=eps;
    if isfield(options,'merge_tolerance')
        merge_tolerance =options.merge_tolerance;
    else
        merge_tolerance =eps;
    end

    if nargin >2
        if isfield(options,'quadratic')
            quadratic = options.quadratic;
        end
    end

    d=pwd;
    if ~isdir(sofea_work_path)
        mkdir(sofea_work_path);
    end
    cd(sofea_work_path);

    fid =fopen ([in], 'w');
    for i=1:length(fs)
        fprintf(fid,'%s\n',fs{i});
    end
    fclose (fid);

    if strcmp(computer,'PCWIN')
%         c=['"' sofea_path filesep 'meshing' filesep 'targe2' filesep 'Targe2.exe" -m ' num2str(options.merge_tolerance) ' -i ' in ' -f  2  -o ' out];
        c=['"' sofea_path filesep 'meshing' filesep 'targe2' filesep 'Targe2.exe" ' ' -i ' in ' -f  2  -o ' out];
    elseif strcmp(computer,'GLNX86')  
        exec=['"' sofea_path filesep 'meshing' filesep 'targe2' filesep 'targe2_GLNX86"'];
        c=[exec ' -i ' in ' -f  2 -o ' out ];
        system(['chmod +x ' exec]);
    elseif strcmp(computer,'MACI')
        exec=['"' sofea_path filesep 'meshing' filesep 'targe2' filesep 'targe2_MACI"'];
        c=[exec ' -i ' in ' -f  2 -o ' out ];
        system(['chmod +x ' exec]);
    else
        error(['Computer platform ' computer ' is not supported: contact the author'])
    end
    system (c);

    fid =fopen ([out], 'r');
    l=fgets(fid);
    totals =sscanf(l,'%d');
    xy =fscanf(fid,'%g', [4,totals(1)]);
    p = xy';

    es =fscanf(fid,'%g', [4,totals(2)]);
    es = es';

    ts =fscanf(fid,'%g', [5,totals(3)]);
    ts = ts';

    fclose (fid);

    %     Assign groups
    for i= 1:totals(3)
        group =ts(i,5);
        if ~exist('groups')
            groups{group} = [];
        end
        if length(groups)<group
            groups{group} = [];
        end
        groups{group} = [groups{group} i];
    end
    for i= 1:totals(2)
        group =es(i,4);
        if group~=0
            if ~exist('edge_groups')
                edge_groups{group} = [];
            end
            if length(edge_groups)<group
                edge_groups{group} = [];
            end
            edge_groups{group} = [edge_groups{group} i];
        end
    end

    nnodes= size (p, 1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
    for i=1: nnodes
        fens(i)=fenode(struct('id',p(i,1),'xyz',p(i,2:3)));
    end

    if quadratic
        nedges=size(es, 1);
        nmidnodes =nedges;
        es= [es, (nnodes+1:nnodes+nmidnodes)'];
        fens(nnodes+1:nnodes+nmidnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
        edge_gcells(1:nedges)=deal(gcell_L3(struct('id',1,'conn',[1 2 3],'other_dimension',1.0)));
        for i=1:nedges
            midx=p(es(i,2:3),2:3);
            fens(nnodes+i)=fenode(struct('id',nnodes+i,'xyz',(midx(1,:) +midx(2,:))/2));
            options.id =i;
            options.conn =[es(i,2:3),nnodes+i];
            edge_gcells(i)=gcell_L3(options);
        end

        ncells=size (ts, 1);
        options.id =1;
            options.conn =(1:6);
        gcells(1:ncells)=deal(gcell_T6(options));
        for i=1:ncells
            nns=[intersect(es(ts(i,4),2:3),es(ts(i,2),2:3)),...
                intersect(es(ts(i,2),2:3),es(ts(i,3),2:3)),...
                intersect(es(ts(i,3),2:3),es(ts(i,4),2:3))];
            enns=es(ts(i,2:4),5)';
            nns = [nns, enns];
            if det([1 p(nns(1),2:3); 1 p(nns(2),2:3); 1 p(nns(3),2:3)])<0
                nns=nns([1, 3, 2, 6, 5, 4]);
            end
            options.id =i;
            options.conn =nns;
            gcells(i)=gcell_T6(options);
        end
    else
        ncells=size (ts, 1);
        options.id =1;
            options.conn =(1:3);
        gcells(1:ncells)=deal(gcell_T3(options));
        for i=1:ncells
            nns=unique([es(ts(i,2),2:3) es(ts(i,3),2:3) es(ts(i,4),2:3)]);
            if det([1 p(nns(1),2:3); 1 p(nns(2),2:3); 1 p(nns(3),2:3)])<0
                nns =nns([1, 3, 2]);
            end
            options.id =i;
            options.conn =nns;
            gcells(i)=gcell_T3(options);
        end

        nedges=size(es, 1);
        edge_gcells(1:nedges)=deal(gcell_L2(struct('id',1,'conn',[1 2],'other_dimension',1.0)));
        for i=1:nedges
            options.id =i;
            options.conn =es(i,2:3);
            edge_gcells(i)=gcell_L2(options);
        end
    end
    cd(d);

    % mesh{1}=fens;
    % mesh{2}=gcells;
    % drawmesh(mesh); view (2);

    return;
end

