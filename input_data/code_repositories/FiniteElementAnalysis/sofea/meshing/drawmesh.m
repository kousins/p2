% Draw a finite element mesh.
%
% function gv = drawmesh(aMesh,varargin)
%
%   Call as:
%     drawmesh(Mesh),
%     drawmesh(Mesh,'gcells'), which chooses to draw just the cells; or
%     drawmesh(Mesh,'nodes'), which chooses to draw just the nodes; or
%     drawmesh(Mesh,'gcells','nodes'), draws both;
%     drawmesh(Mesh,'gcells','nodes','facecolor','none'), turns off fill
%     for element faces;
%     drawmesh(Mesh,'gcells','nodes','facecolor','green','shrink', 0.8),
%     chooses green as the color with which to feel the element faces, and
%     each element is shrunk with a factor of 0.8 about its barycenter
%   where
%     Mesh is either
%        string: name of the mesh file
%             or
%        cell array of two elements, such as the output of
%              a mesh file, i.e. {fens gcells}
%   Examples:
%     drawmesh('mpart','gcells')
%
%     [fens gcells]=block(10,3,4,5,3,8);
%     mesh{1}=fens;
%     mesh{2}=gcells;
%     drawmesh(mesh); or
%
%     drawmesh({fens gcells})
% 
% Optional name/value pairs:
%     facecolor=color to be used for the mesh faces (Default 'none');
%     edgecolor=Color to be used for the mesh edges (Default 'black');
%     shrink=factor by which each element is shrunk about its barycenter (Default 1);
%
function gv = drawmesh(aMesh,varargin)
    facecolor='none';
    edgecolor='black';
    linewidth=1;
    alpha=1;
    shrink=1;
    gv=[];
    label=[];
    node_list = [];
    if (nargin == 1)
        draw_nodes=1;
        draw_gcells=1;
    else
        draw_nodes=0;
        draw_gcells=0;
        pos = 1;
        while (pos <= nargin-1)
            arg = varargin{pos};
            switch arg
                case 'gcells'
                    draw_gcells=1;
                case 'nodes'
                    draw_nodes=1;
                case 'label'
                    pos=pos+1;
                    label=varargin{pos};
                case 'facecolor'
                    pos=pos+1;
                    facecolor=varargin{pos};
                case 'edgecolor'
                    pos=pos+1;
                    edgecolor=varargin{pos};
                case 'linewidth'
                    pos=pos+1;
                    linewidth=varargin{pos};
                case 'alpha'
                    pos=pos+1;
                    alpha=varargin{pos};
                case 'gv'
                    pos=pos+1;
                    gv=varargin{pos};
                case 'shrink'
                    pos=pos+1;
                    shrink=varargin{pos};
                case 'node_list'
                    pos=pos+1;
                    node_list=varargin{pos};
                otherwise
                    error('Unrecognized argument!');
            end
            pos = pos + 1;
        end
    end
    if ~(draw_gcells*draw_nodes)
        draw_gcells=1;
    end

    % Mesh
    if (ischar(aMesh))
        eval(['[fens,gcells] = ' aMesh ';']);
    else
        fens=aMesh{1};
        gcells=aMesh{2};
    end
    % Geometry
    geom = field(struct ('name', ['geom'], 'dim', length(get(fens(1),'xyz')), 'fens',fens));

    % Clear the figure
    if isempty(gv)
        gv=graphic_viewer;
        gv=reset (gv,struct ([]));
    end
    context = struct ('x',geom,'u',0*geom,...
        'facecolor',facecolor,...
        'edgecolor',edgecolor,...
         'linewidth',linewidth,...
         'alpha',alpha,...
        'shrink', shrink);
    if (draw_gcells)
        % Plot the gcells
        if isempty(label)
            for i=1:length(gcells)
                draw(gcells(i),gv,context);
            end
        else
            for i=1:length(gcells)
                if get(gcells(i),'label')==label
                    draw(gcells(i),gv,context);
                end
            end
        end
    end

    if (draw_nodes)
        % Plot the node numbers
        box=[inf,inf,inf,-inf,-inf,-inf];
        if (isempty(node_list))
            node_list =1:length(fens);
        end
        for i=node_list
            box(1:3)=min(box(1:3),get(fens(i),'xyz3'));
            box(4:6)=max(box(4:6),get(fens(i),'xyz3'));
        end
        context.offset=20*norm(box(4:6)-box(1:3))/(length(fens)^3);
        for i=node_list
            draw(fens(i), gv, context);
        end
    end
    % gv=rotate (gv,struct ([]));

    return;
end