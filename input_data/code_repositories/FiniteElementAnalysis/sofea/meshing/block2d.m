% Mesh of a rectangle located in a given range
%
% function [fens,gcells] = block2d(Length,Width,nL,nW,options)
%
% <0,Length> x <0,Width>
% Divided into elements: nL, nW in the first, second (x,y).
% options = structure with fields recognized by the constructor of the
%       gcell_Q4 finite element
% 
% See the visual gallery by running test_block
% 
function [fens,gcells] = block2d(Length,Width,nL,nW,options)
    nL = ceil(nL); nW = ceil(nW);
    nnodes=(nL+1)*(nW+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
    ncells=(nL)*(nW);
    gcells(1:ncells)=deal(gcell_Q4);
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end

    f=1;
    for j=1:(nW+1)
        for i=1:(nL+1)
            xyz=[(i-1)*Length/nL (j-1)*Width/nW];
            fens(f)=fenode(struct('id',f,'xyz',xyz));
            f=f+1;
        end
    end

    gc=1;
    for i=1:nL
        for j=1:nW
            nn=node_numbers(i,j,nL,nW);
            options.id =gc;
            options.conn =nn;
            gcells(gc)=gcell_Q4(options);
            gc=gc+1;
        end
    end
    return; % block
end

function nn=node_numbers(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[f (f+1)  f+(nL+1)+1 f+(nL+1)];
    return;
end
