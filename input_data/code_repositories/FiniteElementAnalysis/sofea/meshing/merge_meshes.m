% Merge two meshes together by gluing together nodes within tolerance.
%
% function [fens,gcells1,gcells2] = merge_meshes(fens1, gcells1, fens2,
%                     gcells2, tolerance)
%
% The two meshes, fens1, gcells1, and fens2, gcells2, are glued together by
% merging the nodes that fall within a box of size "tolerance".
% The merged node array, fens, and the two arrays of geometry cells with
% renumbered  connectivities are returned.
% 
% For efficiency reasons, we should make sure that
% length(fens1)<length(fens2).
% 
function [fens,gcells1,gcells2] = merge_meshes(fens1, gcells1, fens2, gcells2, tolerance)
    xyz = get(fens1(1),'xyz');
    dim =length(xyz); 
    xyz1 = zeros(length(fens1),dim);
    id1 = zeros(length(fens1),1);
    for i=1:length(fens1)
        xyz1(i,:) =get(fens1(i),'xyz');
        id1(i) = get(fens1(i),'id');
    end
     xyz = get(fens2(1),'xyz');
    xyz2 = zeros(length(fens2),dim);
    id2 = zeros(length(fens2),1);
    for i=1:length(fens2)
        xyz2(i,:) =get(fens2(i),'xyz');
        id2(i) = get(fens2(i),'id');
    end
    c1=ones(size(xyz2,1),1);
    % Mark nodes from the first array that are duplicated in the second
    for i=1:length(fens1)
        xyz =xyz1(i,:);
        xyzd=abs(xyz2-c1*xyz);
        j=find(sum(xyzd')'<tolerance);
        if (~isempty(j))
            id1(i) =-j(1);
        end
    end
    % Generate  merged arrays of the nodes
    xyzm = zeros(length(fens1)+length(fens2),dim);
    xyzm(1:length(fens2),:)=xyz2;
    idm = zeros(length(fens1)+length(fens2),1);
    idm(1:length(fens2))=(1:length(fens2));
    mid=length(fens2)+1;
    for i=1:length(fens1) % and then we pick only non-duplicated fens1
        if id1(i)>0
            id1(i)=mid;
            idm(mid)=mid;
            xyzm(mid,:)=xyz1(i,:);
            mid=mid+1;
        else 
            id1(i)=id2(-id1(i));
        end
    end
    nfens =mid-1;
    xyzm =xyzm(1:nfens,:);
    idm =idm(1:nfens,:);
    % Renumber the cells
    for i=1:length(gcells1)
        conn=get(gcells1(i),'conn');
        conn=id1(conn);
%         if ~isempty(find(conn==0))
%             error('here')
%         end
        gcells1(i)=set(gcells1(i),'conn',conn');
    end
    fens(1:nfens) = deal(fenode);
    for k=1:nfens
        fens(k)=fenode(struct('id',idm(k),'xyz',xyzm(k,:)));
    end
end
