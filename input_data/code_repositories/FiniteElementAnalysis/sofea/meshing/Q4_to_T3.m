% Convert a mesh of quadrilateral Q4's to 2 triangles T3 each.
%
% function [fens,gcells] = Q4_to_T3(fens,gcells,options)
%
% options =attributes recognized by the constructor gcell_T3, and
% orientation = 'default' or 'alternate' chooses which diagonal is taken
%      for splitting
% Example: 
% [fens,gcells] = Q4_to_T3(fens,gcells,struct('orientation','alternate'))
% will generate triangles by using the alternate title for splitting.
% 
function [fens,gcells] = Q4_to_T3(fens,gcells,options)
    if ~isstruct(options)
        options = struct('id',1);
    end
    connl1=(1:3);
    connl2=[1, 3, 4];
    if isfield(options,'orientation')
         if strcmp(options.orientation,'alternate')
             connl1=[1, 2, 4];
             connl2=[3, 4, 2];
         end
    end
    nedges=4;
    ngcells(1:2*length(gcells))=deal(gcell_T3(struct('id',1,'conn',(1:3))));
    nc=1;
    for i= 1:length(gcells)
        conn = get (gcells(i),'conn');
        options.id =nc;
        options.conn =conn(connl1);
        ngcells(nc) =gcell_T3(options);
        nc= nc+ 1;
        options.id =nc;
        options.conn =conn(connl2);
        ngcells(nc) =gcell_T3(options);
        nc= nc+ 1;
    end
    gcells=ngcells;
    return;
end
