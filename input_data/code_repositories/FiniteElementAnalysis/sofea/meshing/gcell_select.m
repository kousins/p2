% Select geometric cells.
%
% function celllist = gcell_select(fens, gcells, options)
%
% Select geometric cells using some criterion, for instance a gcell is selected
% because all its nodes are inside a box or on its surface.
% Examples:
% % Select all cells with all nodes inside the box:
%     gcell_select(fens,gcells,struct ('box',[1 -1 2 0 4 3]))
% % Select all cells with at least one node inside the box:
%     gcell_select(fens,gcells,struct ('box',[1 -1 2 0 4 3],'anynode'))
% % Select all cells with given label:
%     gcell_select(fens,gcells,struct ('label', 13))
% % Select all cells connected together (Starting from node 13):
%     gcell_select(fens,gcells,struct ('flood', true, 'startfen', 13))
% % Select all cells "facing" in the direction [1,0,0] (along the x-axis):
%     gcell_select(fens,gcells,struct ('facing', true, 'direction', [1,0,0]))
% % Select all cells "facing" in the direction [x(1),x(2),0] (away from the z-axis):
%     gcell_select(fens,gcells,struct ('facing', true, 'direction', @(x)(+[x(1:2),0])))
% Here x is the centroid of the nodes of each selected gcell.
% % Select all cells "facing" in the direction [x(1),x(2),0] (away from the z-axis):
%     gcell_select(fens,gcells,struct ('facing',1,'direction',@(x)(+[x(1:2),0]),'tolerance',0.01))
% Here the gcell is considered facing in the given direction if the dot
% product of its normal and the direction vector is greater than tolerance.
%
% The option 'inflate' may be used to increase or decrease the extent of
% the box (or the distance) to make sure some nodes which would be on the
% boundary are either excluded or included.
%
% This function uses the node selection function fenode_select() to search
% the nodes.
function celllist = gcell_select(fens, gcells, options)
    inside = true;
    anynode = false;
    flood = false;
    facing = false;
    label = [];
    if isfield(options,'anynode')
        inside = false;
        anynode = true;
    end
    if isfield(options,'flood')
        flood = true;
        if isfield(options,'startfen')
            startfen = options.startfen;
        else
            error('Need the identifier of the Starting node, startfen');
        end
    end
    if isfield(options,'facing')
        facing = true;
        if isfield(options,'direction')
            direction = options.direction;
        else
            error('Need the direction as a three element array');
        end
        tolerance =0;
        if isfield(options,'tolerance')
            tolerance = options.tolerance;
        end
    end
    if isfield(options,'label')
        label = options. label;
    end

    %     Select based on gcell label
    if ~isempty(label)
        celllist= [];
        for i=1: length (gcells)
            lbl=get(gcells(i),'label');
            if label==lbl
                celllist(end+1) =i;
            end
        end
        return;
    end

    % Select by flooding
    if (flood)
        distance =Inf;
        if isfield(options,'distance')
            distance = options.distance;
        end
        celllist= [];
        Futurefen = [startfen];
        Visitedfen = [];
        f2gmap=fenode_to_gcell_map (struct ('gcells',gcells));
        gmap=get (f2gmap,'gcell_map');
        nodelist = [];
        if (distance <Inf)
            nodelist=fenode_select(fens,struct ('distance',distance, 'from',get(fens(startfen),'xyz')));
        end
        while 1
            Visitedfen = unique([Visitedfen,Futurefen]);
            Currentfen =Futurefen;
            Futurefen = [];
            for i=1: length (Currentfen)
                gl=gmap{Currentfen(i)};
                if (~isempty(gl))
                    celllist(end+1:end+length(gl)) =gl;
                    for k=1:length(gl)
                        conn=get(gcells(gl(k)),'conn');
                        Futurefen = [Futurefen,conn];
                    end
                end
            end
            Futurefen=unique(Futurefen);
            if (~isempty(nodelist))
                Futurefen = intersect(Futurefen,nodelist);
            end
            Futurefen=setdiff(Futurefen,Visitedfen);
            if (isempty(Futurefen))
                break;
            end
        end
        celllist =unique(celllist);
        return;
    end
    
    if (facing)
        celllist= [];
        sdim =length(get(fens(1),'xyz'));
        mdim=get(gcells(1),'dim');
        if (mdim~=sdim-1) 
            error ('"Facing" selection of gcells make sense only for Manifold dimension ==Space dimension-1')
        end
        param_coords =zeros(1,mdim);
        Need_Evaluation = (strcmp(class (direction),'inline') || strcmp(class (direction),'function_handle'));
        if ( ~Need_Evaluation)
            d=direction/norm(direction);
        end
        for i=1: length (gcells)
            conn=get(gcells(i),'conn');
            xyz =zeros(length (conn),sdim);
            for j=1: length (conn)
                xyz(j,:) =get(fens(conn(j)),'xyz');
            end
            Nder = bfundpar (gcells(i), param_coords);
            Tangents =xyz'*Nder;
            N = normal (Tangents,sdim, mdim);
            if (Need_Evaluation)
                d=feval(direction,mean(xyz));
                d=d/norm(d);
            end
            if (dot(N,d)>tolerance)
                celllist(end+1)=i;
            end
        end
        celllist =unique(celllist);
        return;
    end
    function N = normal (Tangents,sdim, mdim)
        [sdim, mdim] = size(Tangents);
        if     mdim==1 % 1-D gcell
            N = [-Tangents(2,1),Tangents(1,1)];
        elseif     mdim==2 % 2-D gcell
            N =skewmat(Tangents(:,1))*Tangents(:,2);
        else
            error('Got an incorrect size of tangents');
        end
        N=N/norm(N);
    end

    %     Select based on location of nodes
    nodelist=fenode_select(fens, options);
    celllist= [];
    for i=1: length (gcells)
        conn=get(gcells(i),'conn');
        tf = ismember(conn, nodelist);
        if inside
            if sum(tf) == length(conn)
                celllist(end+1) =i;
            end
        end
        if anynode
            if sum(tf) >= 1
                celllist(end+1) =i;
            end
        end
    end
end
