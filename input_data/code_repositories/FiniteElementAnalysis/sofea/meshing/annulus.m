% Mesh of an annulus segment.
%
% function [fens,gcells] = annulus(rin,rex,nr,nc,thickness)
%
% Mesh of an annulus segment in the first quadrant, centered at the origin,
% with internal radius rin, and  external radius rex. 
% Divided into elements: nr, nc in the radial and circumferential direction respectively.
function [fens,gcells] = annulus(rin,rex,nr,nc,thickness)
trin=min(rin,rex);
trex=max(rin,rex);
[fens,gcells]=block2d(trex-trin,pi/2,nr,nc, thickness);
for i=1:length (fens)
    xy=get (fens(i),'xyz');
    r=trin+xy(1); a=xy(2);
    xy=[r*cos(a) r*sin(a)];
    fens(i)=set(fens(i),'xyz', xy);
end
