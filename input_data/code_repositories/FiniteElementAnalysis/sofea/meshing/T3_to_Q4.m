% Convert a mesh of triangles T3 to 4 quadrilateral Q4's each.
%
% function [fens,gcells] = T3_to_Q4(fens,gcells,options)
%
% options =attributes recognized by the constructor gcell_Q4
function [fens,gcells] = T3_to_Q4(fens,gcells,options)
    [fens,gcells] = T3_to_T6(fens,gcells,options);
    if ~isstruct(options)
        options = struct('id',1);
    end
    onfens=length(fens);
    fens(length(fens)+1:length(fens)+length(gcells))=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    % construct new geometry cells
    ngcells(1: 3*length(gcells))=deal(gcell_Q4(struct('id',1,'conn',(1:4))));
    nc=onfens+1;
    gc=1;
    for i= 1:length(gcells)
        conn = get(gcells(i),'conn');
        fens(nc)=fenode(struct ('id',nc,'xyz',(get(fens(conn(1)),'xyz')+get(fens(conn(2)),'xyz') +get(fens(conn(3)),'xyz'))/3));
        options.id =gc;
        options.conn =[conn(1) conn(4) nc conn(6)];
        ngcells(gc) =gcell_Q4(options);
        gc=gc+1;
        options.id =gc;
        options.conn =[conn(2) conn(5) nc conn(4)];
        ngcells(gc) =gcell_Q4(options);
        gc=gc+1;
        options.id =gc;
        options.conn =[conn(3) conn(6) nc conn(5)];
        ngcells(gc) =gcell_Q4(options);
        gc=gc+1;
        nc= nc+ 1;
    end
    gcells=ngcells;
    return;
end
