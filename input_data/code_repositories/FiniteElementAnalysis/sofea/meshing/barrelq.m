%   The Scordelis-Lo barrel vault mesh.
%
% function [fens,gcells] = barrelq
%
%   The Scordelis-Lo barrel vault: radius r=25 ft, subtended angle of 80
%   degrees, length 50 f, thickness 3in.
%   Elastic modulus 3 msi, Poisson's ratio 0, and
%   weight of the shell 90 lb per square ft. Two curved edges are
%   supported by rigid diaphragms, the other two edges are free.
%   This is the geometry of one quarter of the whole shell.
function [fens,gcells] = barrelq
xyz=[0.0 0.0 0.0; 0.0 6.25 0.0; 0.0 12.5 0.0; 0.0 18.75 0.0;0.0 25.0 0.0;
    4.34 0.0 -0.38;4.34 6.25 -0.38;4.34 12.5 -0.38;4.34 18.75 -0.38;4.34 25.0 -0.38;
    8.55 0.0 -1.51;8.55 6.25 -1.51;8.55 12.5 -1.51;8.55 18.75 -1.51;8.55 25.0 -1.51;
    12.5 0.0 -3.35;12.5 6.25 -3.35;12.5 12.5 -3.35;12.5 18.75 -3.35;12.5 25.0 -3.35;
    16.1 0.0 -5.85;16.1 6.25 -5.85;16.1 12.5 -5.85;16.1 18.75 -5.85;16.1 25.0 -5.85]*12;% convert the coordinates to inches
conns=[6 7 2 1; 7 8 3 2; 8 9 4 3; 9 10 5 4;
    11 12 7 6; 12 13 8 7; 13 14 9 8; 14 15 10 9;
    16 17 12 11; 17 18 13 12; 18 19 14 13; 19 20 15 14;
    21 22 17 16; 22 23 18 17; 23 24 19 18; 24 25 20 19];
fens = [];
for i=1:size(xyz,1)
    fens = [ fens; fenode(struct ('id',i,'xyz',xyz(i,:))) ];
end
gcells = [];
for i=1:size(conns,1)
    gcells = [ gcells; gcell_q4_shell(struct ('id',i, 'conn',conns(i,:),'other_dimension', 3.0)) ];
end
return;
