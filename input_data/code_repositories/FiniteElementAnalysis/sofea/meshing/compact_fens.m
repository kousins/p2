% Compact the finite element node array by deleting unconnected nodes.
% 
% function [fens, new_numbering] =compact_fens(fens, connected)
% 
% fens = array of finite element nodes
% connected = The array element connected(j) is either 0 (when j is an unconnected
%    node), or a positive number (when node j is connected to other nodes by
%    at least one geometric cell)  

% Output:
% fens = new array of finite element nodes
% new_numbering= array which tells where in the new fens array the
% connected nodes are (or 0 when the node was unconnected). For instance,
% node 5 was connected, and in the new array it is the third node: then
% new_numbering(5) is 3.
% 
% Let us say there are nodes not connected to any geometric cell that you
% would like to remove from the mesh: here is how that would be
% accomplished.
% 
% connected = find_unconn_fens(fens, gcells);
% [fens, new_numbering] =compact_fens(fens, connected);
% gcells = renumber_gcell_conn(gcells, new_numbering);
% 
% Finally, check that the mesh is valid:
% validate_mesh(fens, gcells);
% 
function [fens, new_numbering] =compact_fens(fens, connected)
    nconnfens =find(connected~=0);
    new_numbering=zeros(length(fens),1);
    nfens(1:nconnfens) =deal(fenode);
    id=1;
    for i=1:length(fens),
        if(connected(i)),
            new_numbering(i)=id; 
            nfens(id) =fens (i);
            nfens(id) =set(nfens(id), 'id',id);
            id=id+1;
        end,
    end
    fens=nfens;
end
