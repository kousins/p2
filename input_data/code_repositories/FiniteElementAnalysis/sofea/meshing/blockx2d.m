% Mesh of a 2-D block, Q4 cells. The nodes are located at the Cartesian
% product of the two intervals on the input.  This allows for
% construction of graded meshes.
%
% function [fens,gcells] = blockx2d(xs,ys,options)
%
% xs,ys =Locations of the individual planes of nodes. 
% options = structure with fields recognized by the constructor of the
%       gcell_Q4 finite element
% 
% Example:
% [fens,gcells] = blockx2d(1/125*(0:1:7).^3,4+(0:2:8), []); drawmesh({fens,gcells})
function [fens,gcells] = blockx2d(xs,ys,options)
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end
    nL =length(xs)-1;
    nW =length(ys)-1;

    nnodes=(nL+1)*(nW+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
    ncells=(nL)*(nW);
    gcells(1:ncells)=deal(gcell_Q4(struct ('id', 0,'conn',(1:4))));

    f=1;
    for j=1:(nW+1)
        for i=1:(nL+1)
            xyz=[xs(i) ys(j)];
            fens(f)=fenode(struct('id',f,'xyz',xyz));
            f=f+1;
        end
    end


    gc=1;
    for i=1:nL
        for j=1:nW
            nn=node_numbers(i,j,nL,nW);
            options.id =gc;
            options.conn =nn;
            gcells(gc)=gcell_Q4(options);
            gc=gc+1;
        end
    end

    return; % block
end

function nn=node_numbers(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[f (f+1)  f+(nL+1)+1 f+(nL+1)];
    return;
end
