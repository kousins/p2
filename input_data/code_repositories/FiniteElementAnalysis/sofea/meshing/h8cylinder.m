function [fens,gcells] = h8cylinder(InternalRadius, Thickness, Length, nC, nT, nL)
[fens,gcells] = block(2*pi,Thickness, Length, nC, nT, nL);
fens = transform_apply(fens,@(x, data) (x+ [0, InternalRadius, 0]), []);
climbPerRevolution= 0;
fens = transform_2_helix(fens,climbPerRevolution);
[fens,gcells] = merge_nodes(fens, gcells, min( [Thickness/nT,Length/nL,2*pi*InternalRadius/nC] )/100);
end