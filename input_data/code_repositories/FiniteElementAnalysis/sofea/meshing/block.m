% Mesh of a 3-D block located in a given range, H8 cells
%
% function [fens,gcells] = block(Length,Width,Height,nL,nW,nH)
%
% Range xyz =<0,Length> x <0,Width> x <0,Height>
% Divided into elements: nL, nW, nH in the first, second, and
% third direction (x,y,z). Geometric cells H8.
function [fens,gcells] = block(Length,Width,Height,nL,nW,nH)
    nnodes=(nL+1)*(nW+1)*(nH+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    ncells=(nL)*(nW)*(nH);
    gcells(1:ncells)=deal(gcell_H8(struct ('id', 0,'conn',(1:8))));

    f=1;
    for k=1:(nH+1)
        for j=1:(nW+1)
            for i=1:(nL+1)
                xyz=[(i-1)*Length/nL (j-1)*Width/nW (k-1)*Height/nH];
                fens(f)=fenode(struct('id',f,'xyz',xyz));
                f=f+1;
            end
        end
    end

    gc=1;
    for i=1:nL
        for j=1:nW
            for k=1:nH
                nn=node_numbers(i,j,k,nL,nW,nH);
                gcells(gc)=gcell_H8(struct ('id', gc,'conn',nn));
                gc=gc+1;
            end
        end
    end
    return; % block
end

% first go along Length, then Width, and finally Height
function nn=node_numbers(i,j,k,nL,nW,nH)
    f=(k-1)*((nL+1)*(nW+1))+(j-1)*(nL+1)+i;
    nn=[f (f+1)  f+(nL+1)+1 f+(nL+1)];
    nn=[nn nn+((nL+1)*(nW+1))];
end
