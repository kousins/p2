%  Validate finite element mesh.
%
% function validate_mesh(fens, gcells)
%
% A finite element mesh to them by the node array And the geometric cell
% array Is validated by checking the sanity of the numbering:
% -- the node numbers need to be positive and in serial order
% -- the gcell connectivity needs to refer to valid nodes
% 
% An error is reported as soon as it is detected.
% 
function validate_mesh(fens, gcells)
    nfens=length(fens);
    id1 = zeros(nfens,1);
    for i=1:length(fens)
        id1(i) = get(fens(i),'id');
    end
    if (sum(abs(id1-(1:length(fens))'))~=0)
        error ('Wrong node numbers: Nodes need to be numbered in serial order')
    end
    if (sum(abs(id1-abs(id1)))~=0)
        error ('Wrong node numbers: Node numbers need to be positive')
    end
    
    for i=1:length(gcells)
        conn=get(gcells(i),'conn');
        if (max(conn)>nfens)
            error (['Wrong connectivity (refers to nonexistent node) ' num2str(conn)])
        end
        if (min(conn)<1)
            error (['Wrong connectivity (refers to nonexistent node) ' num2str(conn)])
        end
        if (length(unique(conn))~=length(conn))
            error (['Wrong connectivity (multiply referenced node) ' num2str(conn)])
        end
    end
end
