% Select nodes.
%
% function nodelist = fenode_select(fens, options)
%
% Select nodes using some criterion, for instance and node is selected
% because it is inside a box or on its surface.
% Example: fenode_select(fens,struct ('box',[1 -1 2 0 4 3])), selects
% nodes which are strictly inside the box  
%  -1<= x <=1     0<= y <=2     3<= z <=4
% Example: fenode_select(fens,struct ('distance',0.5, 'from',[1 -1])), selects
% nodes which are Less than 0.5 units removed from the point [1 -1].
%
% The option 'inflate' may be used to increase or decrease the extent of
% the box (or the distance) to make sure some nodes which would be on the
% boundary are either excluded or included.
% 
function nodelist = fenode_select(fens, options)
    nodelist= zeros(1,length(fens)); nn= 0;
    if isfield(options,'box')
        box=options.box;
        inflate =0;
        if isfield(options,'inflate')
            inflate = (options.inflate);
        end
        dim=length (box)/2;
        for i=1: dim
            abox(2*i-1)=min(box(2*i-1),box(2*i))- inflate;
            abox(2*i)=max(box(2*i-1),box(2*i))+ inflate;
        end
        for i=1: length (fens)
            if inbox (abox,get(fens(i),'xyz'))
                nn =nn +1; nodelist(nn) =get(fens(i),'id');
            end
        end
    elseif isfield(options,'distance')
        from =0*get(fens(1),'xyz');
        if isfield(options,'from')
            from = options.from;
        end
        for i=1: length (fens)
            if norm (from-get(fens(i),'xyz'))<options.distance
                nn =nn +1; nodelist(nn) =get(fens(i),'id');
            end
        end
    end
    if (nn==0)
        nodelist = [];
    else
        nodelist =nodelist(1:nn);
    end
end

function b= inrange (range,x)
    b=((x>=range(1)) & (x<=range(2)));
end
function b= inbox (box,x)
    dim=length (box)/2;
    b=inrange (box(1:2),x(1));
    for i=2:dim
        b=(b & inrange (box(2*i-1:2*i),x(i)));
    end
end

