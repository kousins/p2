% Tetrahedral (T10) Mesh of a block located in the range
%
% function [fens,gcells] = t4blockT10(Length,Width,Height,nL,nW,nH)
%
% Range =<0,Length> x <0,Width> x <0,Height>
% Divided into elements: nL, nW, nH in the first, second, and
% third direction (x,y,z).
function [fens,gcells] = t4blockT10(Length,Width,Height,nL,nW,nH)
    [fens,gcells] = t4block(Length,Width,Height,nL,nW,nH);
    [fens,gcells] = T4_to_T10(fens,gcells);
end
