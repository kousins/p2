% Merge by gluing together nodes located within tolerance of each other.
%
% function [fens,gcells] = merge_nodes(fens, gcells, tolerance)
%
% The two meshes, fens1, gcells1, and fens2, gcells2, are glued together by
% merging the nodes that fall within a box of size "tolerance".
% The merged node array, fens, and the two arrays of geometry cells with
% renumbered  connectivities are returned.
% 
% For efficiency reasons, we should make sure that
% length(fens1)<length(fens2).
% 
function [fens,gcells] = merge_nodes(fens, gcells, tolerance)
    xyz = get(fens(1),'xyz');
    dim =length(xyz); 
    xyz1 = zeros(length(fens),dim);
    id1 = zeros(length(fens),1);
    for i=1:length(fens)
        xyz1(i,:) =get(fens(i),'xyz');
        id1(i) = get(fens(i),'id');
    end
    c1=ones(size(xyz1,1),1);
    % Mark nodes from the array that are duplicated 
    for i=1:length(fens)
        xyz =xyz1(i,:);
        xyzd=abs(xyz1-c1*xyz);
        j=find(sum(xyzd')'<tolerance);
        if (~isempty(j))   && (j(1) ~= i)
            id1(i) =-j(1);
        end
    end
    % Generate  merged arrays of the nodes
    xyzm = zeros(length(fens),dim);
    idm = zeros(length(fens),1);
    mid=1;
    for i=1:length(fens) % and then we pick only non-duplicated fens1
        if id1(i)>0
            id1(i)=mid;
            idm(mid)=mid;
            xyzm(mid,:)=xyz1(i,:);
            mid=mid+1;
        else 
            id1(i)=id1(-id1(i));
        end
    end
    nfens =mid-1;
    xyzm =xyzm(1:nfens,:);
    idm =idm(1:nfens,:);
    % Renumber the cells
    for i=1:length(gcells)
        conn=get(gcells(i),'conn');
        conn=id1(conn);
        gcells(i)=set(gcells(i),'conn',conn');
    end
    clear fens
    fens(1:nfens) = deal(fenode);
    for k=1:nfens
        fens(k)=fenode(struct('id',idm(k),'xyz',xyzm(k,:)));
    end
end
