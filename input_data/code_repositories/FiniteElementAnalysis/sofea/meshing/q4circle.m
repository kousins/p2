% Create a mesh of a quarter circle.
%
% function [fens,gcells]=q4circle(radius,nrefine,thickness)
%
% Create a mesh of a quarter circle of "radius". The  mesh will consist of
% three quadrilateral elements if "nrefine==0", or more if "nrefine>0".
% "nrefine" is the number of bisections applied  to refine the mesh.
% The quadrilaterals have thickness "thickness".
function [fens,gcells]=q4circle(radius,nrefine,thickness)
    [fens,gcells]=q4sphere(radius,nrefine,thickness);
    % apply transformation to project the locations of the nodes into the
    % plane x-y
    for j=1:length(fens)
        xyz=get(fens(j),'xyz');
        xyz = xyz(1:2)*((radius-norm(xyz(3)))+norm(xyz(3))/2)/radius; 
        fens(j)=set(fens(j),'xyz', xyz);
    end
end
