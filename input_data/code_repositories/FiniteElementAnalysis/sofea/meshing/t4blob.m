% Tetrahedral (t4) Mesh of a block located in a given range
%
% function [fens,gcells] = t4block(Length,Width,Height,nL,nW,nH)
%
% Range =<0,Length> x <0,Width> x <0,Height>
% Divided into elements: nL, nW, nH in the first, second, and
% third direction (x,y,z).
function [fens,gcells] = t4blob(Length,Width,Height,nL,nW,nH)
nnodes=(nL+1)*(nW+1)*(nH+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    f=1;
    for k=1:(nH+1)
        for j=1:(nW+1)
            for i=1:(nL+1)
                xyz=[Length,Width,Height].*rand(1,3);
                fens(f)=fenode(struct('id',f,'xyz',xyz));
                f=f+1;
            end
        end
    end
    x=zeros(nnodes,1);
    y=zeros(nnodes,1);
    z=zeros(nnodes,1);
    for k=1:length(fens)
        xyz = get (fens(k),'xyz');
        x(k)=xyz(1);
        y(k)=xyz(2);
        z(k)=xyz(3);
    end
    T = delaunayn([x,y,z]);
    ncells=size(T,1);
    gcells(1:ncells)=deal(gcell_T4(struct ('id', 0,'conn',(1:4))));
    for i=1:ncells
        xyz=zeros (4, 3);
        for k=1:4
            xyz(k,:) = get (fens(T(i,k)),'xyz');
        end
        if tetvol(xyz) > 0
            gcells(i)=gcell_T4(struct ('id', i,'conn',T(i,:)));
        else
            gcells(i)=gcell_T4(struct ('id', i,'conn',T(i,[1, 3, 2, 4])));
        end
    end
    return; % block
end

%compute volume of a tetrahedron
% Given the 4x3 vertex coordinate matrix V of a tetrahedron, TETVOL(V)
% returns the volume of the tetrahedron.
function vol = tetvol(v)
    vol = det([v(2,:)-v(1,:);v(3,:)-v(1,:);v(4,:)-v(1,:)])/6;
    return;
end
