% Extract the node numbers of the nodes  connected by given gcells. 
%
% function cn = connected_nodes(gcells)
%
% Extract the list of unique node numbers for the nodes that 
%  are connected by the gcells. Note that it is assumed that all the
%  geometric cells are of the same type (the same number of connected nodes
%  by each cell).
% 
function cn = connected_nodes(gcells)
    conn= get(gcells(1),'conn');
    all_nodes = zeros(length(gcells),length(conn));
    for i=1:length(gcells)
        all_nodes(i,:) = get (gcells(i),'conn');
    end
    cn = unique(reshape(all_nodes,length(gcells)*length(conn), 1),'rows');
    return
end
