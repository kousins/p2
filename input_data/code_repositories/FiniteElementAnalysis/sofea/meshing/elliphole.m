% Mesh of one quarter of a rectangular plate with an elliptical hole
%
% function [fens,gcells]=elliphole(xradius,yradius,L,H,nL,nH,nR,options)
%
% xradius,yradius = radius of the ellipse,
% L,H= and dimensions of the plate,
% nL,nH= numbers of edges along the side of the plate,
% nR= number of edges along the Circumference,
% options= options accepted by gcell_Q4
function [fens,gcells]=elliphole(xradius,yradius,L,H,nL,nH,nR,options)
    dA =pi/2/(nL +nH);
    tolerance =(xradius+yradius)/(nL*nH)/100;
    fens= []; gcells= [];
    for i= 1:nH
        xy = [xradius*cos((i-1)*dA),yradius*sin((i-1)*dA);...
            L,(i-1)/nH*H;...
            L,(i)/nH*H;...
            xradius*cos((i)*dA),yradius*sin((i)*dA)];
        [fens1,gcells1] = mesh_quadrilateral(xy,nR,1,options);
        if isempty(fens)
            fens=fens1; gcells =gcells1;
        else
            [fens,gcells1,gcells2] = merge_meshes(fens1, gcells1, fens, gcells, tolerance);
            gcells =[gcells1,gcells2];
        end
    end
    for i= 1:nL
        xy = [xradius*cos((nH+i-1)*dA),yradius*sin((nH+i-1)*dA);...
            (nL-i+1)/nL*L,H;...
            (nL-i)/nL*L,H;...
            xradius*cos((nH+i)*dA),yradius*sin((nH+i)*dA)];
        [fens1,gcells1] = mesh_quadrilateral(xy,nR,1,options);
        [fens,gcells1,gcells2] = merge_meshes(fens1, gcells1, fens, gcells, tolerance);
        gcells =[gcells1,gcells2];
    end
end
