% Tetrahedral (t4) Delaunay Mesh of a cylinder of Length and Radius.
%
% function [fens,gcells] = t4cylinderdel(Length,Radius,nL,nR)
%
% Divided into elements: nL (lengthwise), nR (along the radius).
function [fens,gcells] = t4cylinderdel(Length,Radius,nL,nR)
    dR=Radius/(nR+1/2);
    xnnodes= 3*(2*(0:1:nR)+1);
    nnodes=(nL+1)*sum(xnnodes);
    x=zeros(nnodes,1);
    y=zeros(nnodes,1);
    z=zeros(nnodes,1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    k = 1;
    for j = 0:1:nL
        xjiggle = Length/nL/10*j*(j-nL)/nL/nL*rand;
        for n= 0:1:nR
            r =dR*(1/2+n);
            rjiggle=Radius/nR/10*rand*(nR-n)/nR;
            r= r+rjiggle;
            dA =2*pi/xnnodes(n+1);
            dAjiggle =dA/10*rand*(nR-n)/nR;
            for m=1:xnnodes(n+1)
                x(k)=j*Length/nL+xjiggle;
                y(k)=r*cos(dAjiggle+dA*(m-1));
                z(k)=r*sin(dAjiggle+dA*(m-1));
                fens(k)=fenode(struct('id',k,'xyz',[x(k),y(k),z(k)]));
                k=k+1;
            end
        end
    end
    T = delaunay3(x,y,z);

    ncells=size(T,1);
    gcells(1:ncells)=deal(gcell_T4(struct ('id', 0,'conn',(1:4))));
    for i=1:ncells
        xyz=zeros (4, 3);
        for k=1:4
            xyz(k,:) = get (fens(T(i,k)),'xyz');
        end
        if tetvol(xyz) > 0
            gcells(i)=gcell_T4(struct ('id', i,'conn',T(i,:)));
        else
            gcells(i)=gcell_T4(struct ('id', i,'conn',T(i,[1, 3, 2, 4])));
        end
    end
    return; % block
end

%compute volume of a tetrahedron
% Given the 4x3 vertex coordinate matrix V of a tetrahedron, TETVOL(V)
% returns the volume of the tetrahedron.
function vol = tetvol(v)
    vol = det([v(2,:)-v(1,:);v(3,:)-v(1,:);v(4,:)-v(1,:)])/6;
    %     if abs (vol) < 0.1
    %         warning (' sliver?')
    %     end
    return;
end
