% Mesh of a rectangle located in a given range (Unstructured mesh).
%
% function [fens,gcells] = q4block2du(Length,Width,nL,nW,options)
%
% Range =<0,Length> x <0,Width>
% Divided into Quadrilateral (Q4) elements: nL, nW in the first, second (x,y).
% This function  produces an unstructured mesh of Q4 quadrilaterals.
function [fens,gcells] = q4block2du(Length,Width,nL,nW,options)
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end
    [fens,gcells] = t3block2du(Length,Width,1,1,options);
    [fens,gcells] = T3_to_Q4(fens,gcells,options);
    n=2;
    while (2*n < max([nL,nW]))
        [fens,gcells] = refineq4(fens,gcells);
        n =n*2;
    end
end
