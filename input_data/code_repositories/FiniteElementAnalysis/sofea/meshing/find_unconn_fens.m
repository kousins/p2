% Find nodes that are not connected to any cell in the array gcells.
% 
% function connected = find_unconn_fens(fens, gcells)
% 
% fens = array of finite element nodes
% 
% Output:
% connected = array is returned which is for the node k either true (node k is
%      connected), or false (node k is not connected).
% 
% Let us say there are nodes not connected to any geometric cell that you
% would like to remove from the mesh: here is how that would be
% accomplished.
% 
% connected = find_unconn_fens(fens, gcells);
% [fens, new_numbering] =compact_fens(fens, connected);
% gcells = renumber_gcell_conn(gcells, new_numbering);
% 
% Finally, check that the mesh is valid:
% validate_mesh(fens, gcells);
% 
function connected = find_unconn_fens(fens, gcells)
    connected=zeros(length(fens),1);
    f2gmap=fenode_to_gcell_map (struct ('gcells',gcells));
    gmap=get (f2gmap,'gcell_map');
    for i=1:length(gmap),
        connected(i) =(~isempty(gmap{i}));
    end
end
