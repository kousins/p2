% Convert a mesh of quadrilateral Q4's to 2 triangles T3 each.
%
% function [fens,gcells] = Q4_to_aT3(fens,gcells,options)
%
% options = those Attributes recognized by gcell_T3
function [fens,gcells] = Q4_to_aT3(fens,gcells,options)
    if ~isstruct(options)
        options = struct('id',1);
    end
    nedges=4;
    ngcells(1:2*length(gcells))=deal(gcell_T3(struct('id',1,'conn',(1:3))));
    nc=1;
    for i= 1:length(gcells)
        conn = get (gcells(i),'conn');
        options.id =nc;
        options.conn =conn([1, 2, 4]);
        ngcells(nc) =gcell_T3(options);
        nc= nc+ 1;
        options.id =nc;
        options.conn =conn([2, 3, 4]);
        ngcells(nc) =gcell_T3(options);
        nc= nc+ 1;
    end
    gcells=ngcells;
    return;
end
