% Refine a mesh of quadrilaterals by bisection
%
% function [fens,gcells] = refineq4(fens,gcells)
%
function [fens,gcells] = refineq4(fens,gcells)
nedges=4;
nsubcells=4;
% make a search structure for edges
edges={};
for i= 1:length(gcells)
    conn = get (gcells(i),'conn');
    for J = 1:nedges
        ev=conn([J, mod(J,nedges)+1]);
        anchor=min(ev);
        if length(edges)<anchor
            edges{anchor}=[];
        end
        edges{anchor}=unique([edges{anchor} max(ev)]);
    end
end
% now generate new node number for each edge
n=length(fens);
nodes=edges;
for i= 1:length(edges)
    e=edges{i};
    for J = 1:length(e)
        n=n+1;
        e(J)=n;
    end
    nodes{i}=e;
end
fens(length(fens)+1:n)=deal(fenode(struct('id',0,'xyz',[0 0])));
% calculate the locations of the new nodes
% and construct the new nodes
for i= 1:length(edges)
    e=edges{i};
    n=nodes{i};
    for J = 1:length(e)
        fens(n(J))=fenode(struct ('id',n(J),'xyz',(get(fens(i),'xyz')+get(fens(e(J)),'xyz'))/2));
    end
end
nfens=length(fens);% number of nodes in the original mesh plus number of the edge nodes
nifens=length(gcells);% number of internal nodes
fens(nfens+1:nfens+nifens)=deal(fenode(struct('id',0,'xyz',[0 0])));
nfens=length(fens);
% construct new geometry cells
ngcells(1:nsubcells*length(gcells))=deal(gcell_Q4(struct('id',1,'conn',[1 2 3 4],'other_dimension',1.0)));
nc=1;
for i= 1:length(gcells)
    conn = get (gcells(i),'conn');
    econn=zeros(1,nedges);
    for J = 1:nedges
        ev=conn([J, mod(J,nedges)+1]);
        anchor=min(ev);
        e=edges{anchor};
        n=nodes{anchor};
        econn(J)=n(find(e==max(ev)));        
    end
    xyz=get(fens(conn(1)),'xyz');
    for J=2: length(conn)
        xyz=xyz+get(fens(conn(J)),'xyz');
    end
    xyz=xyz/length(conn);
    inn=nfens-nifens+i;
    fens(inn)=fenode(struct ('id',inn,'xyz',xyz));
    Thickness =get(gcells(i),'other_dimension');
    ngcells(nc) =gcell_Q4(struct('id',nc,'conn',[conn(1) econn(1) inn econn(4)],...
        'other_dimension',Thickness, 'label',i));
    nc= nc+ 1;
    ngcells(nc) =gcell_Q4(struct('id',nc,'conn',[conn(2) econn(2) inn econn(1)],...
        'other_dimension',Thickness, 'label',i));
    nc= nc+ 1;
    ngcells(nc) =gcell_Q4(struct('id',nc,'conn',[conn(3) econn(3) inn econn(2)],...
        'other_dimension',Thickness, 'label',i));
    nc= nc+ 1;
    ngcells(nc) =gcell_Q4(struct('id',nc,'conn',[conn(4) econn(4) inn econn(3)],...
        'other_dimension',Thickness, 'label',i));
    nc= nc+ 1;
end
gcells=ngcells;

% for i=1:length(fens)
%     get(fens (i),'id')
% end
return;
