% Randomly shift nodes in a two-dimensional block.
%
% function fens = block2d_random_shift(fens, amplitude, options)
%
% The locations of the nodes are shifted randomly, but so that nodes on the
% boundary remain on the boundary.
function fens = block2d_random_shift(fens, amplitude, options)
    Dimension =length(get(fens(1),'xyz'));
    xyz =zeros(length(fens),Dimension);
    for j=1:length(fens)
        xyz(j, :)=get(fens(j),'xyz');
    end
    minxyz =[];
    maxxyz =[];
    for j=1:Dimension
        minxyz(j) =min(xyz(:,j));
        maxxyz(j) =max(xyz(:,j));
        xyz(:,j)=xyz(:,j) + amplitude* (2*rand(length(fens),1)-1);
    end
    Tol = norm(maxxyz-minxyz)/1e7;
    minxnl=fenode_select (fens,struct ('box',[minxyz(1) minxyz(1)  minxyz(2) maxxyz(2)],'inflate',Tol));
    maxxnl=fenode_select (fens,struct ('box',[maxxyz(1) maxxyz(1)  minxyz(2) maxxyz(2)],'inflate',Tol));
    minynl=fenode_select (fens,struct ('box',[minxyz(1) maxxyz(1)  minxyz(2) minxyz(2)],'inflate',Tol));
    maxynl=fenode_select (fens,struct ('box',[minxyz(1) maxxyz(1)  maxxyz(2) maxxyz(2)],'inflate',Tol));
    xyz(minxnl,1) =minxyz(1);
    xyz(maxxnl,1) =maxxyz(1);
    xyz(minynl,2) =minxyz(2);
    xyz(maxynl,2) =maxxyz(2);
    for j=1:length(fens)
        fens(j)=set(fens(j),'xyz', xyz(j, :));
    end    
end
  
