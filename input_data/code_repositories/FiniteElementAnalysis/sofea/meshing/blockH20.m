%BLOCKH20 Mesh of a block located in a given range, H20 cells
%
% function [fens,gcells] = blockH20(Length,Width,Height,nL,nW,nH)
%
% Mesh of a block located in a given range, H20 cells
% <0,Length> x <0,Width> x <0,Height>
% Divided into elements: nL, nW, nH in the first, second, and
% third direction (x,y,z). H20 elements.
function [fens,gcells] = blockH20(Length,Width,Height,nL,nW,nH)
    [fens,gcells] = block(Length,Width,Height,nL,nW,nH);
    [fens,gcells] = H8_to_H20(fens,gcells);
end
