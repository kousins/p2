% Apply geometry transformation to the locations of the nodes.
%
% function fens = transform_apply(fens,function_handle, function_data)
%
% Apply geometry transformation to the locations of the nodes. 
% The function handle function_handle is of type
%    function xyz=fun(xyz, function_data)
% Where xyz = location of a given node,
% function_data = anything that the function might need to do its job.
function fens = transform_apply(fens,function_handle, function_data)
    for m=1:length(fens)
        fens(m) = set(fens(m),'xyz', function_handle (get(fens(m),'xyz'), function_data));
    end
end
