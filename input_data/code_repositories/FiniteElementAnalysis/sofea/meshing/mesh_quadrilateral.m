% Mesh of a general quadrilateral given by the location of the vertices.
%
% function [fens,gcells] = mesh_quadrilateral(xyz,nL,nW,options)
%
% xyz = One vertex location per row; Either two rows (for a rectangular
% block given by the two corners), or four rows (General quadrilateral).
% Divided into elements: nL, nW in the first and second direction.
% options = Attributes recognized by the constructor of gcell_Q4.
function [fens,gcells] = mesh_quadrilateral(xyz,nL,nW,options)
    npts=size(xyz,1);
    if npts==2
        lo=min(xyz);
        hi=max(xyz);
        xyz=[[lo(1),lo(2)];...
            [hi(1),lo(2)];...
            [hi(1),hi(2)];...
            [lo(1),hi(2)]];
    elseif npts~=4
        error('Need 2 or 8 points');
    end


    [fens,gcells] = block2d(2,2,nL,nW,options);

    for i=1:length(fens)
        xyz1=get(fens(i),'xyz');
        N = bfun(xyz1-1);% shift coordinates by -1
        fens(i)=set(fens(i),'xyz',N'*xyz);
    end
end

function val = bfun(param_coords)
    one_minus_xi = (1 - param_coords(1));
    one_plus_xi  = (1 + param_coords(1));
    one_minus_eta = (1 - param_coords(2));
    one_plus_eta  = (1 + param_coords(2));

    val = [0.25 * one_minus_xi * one_minus_eta;
        0.25 * one_plus_xi  * one_minus_eta;
        0.25 * one_plus_xi  * one_plus_eta;
        0.25 * one_minus_xi * one_plus_eta];
    return;
end
