% Mesh of a line segment located in a given range, L2 Elements
%
% function [fens,gcells] = block1d(Length,nL,options)
%
% Interval x=<0,Length>
% Divided into nL elements of the L2 type.
function [fens,gcells] = block1d(Length,nL,options)
    nnodes=(nL+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
    ncells=(nL);
    gcells(1:ncells)=deal(gcell_L2);
    if ~isstruct(options)
        options= struct('other_dimension',options);
    end

    f=1;
    for i=1:(nL+1)
        xyz=[(i-1)*Length/nL];
        fens(f)=fenode(struct('id',f,'xyz',xyz));
        f=f+1;
    end

    gc=1;
    for i=1:nL
        options.id=gc;
        options.conn=[i,i+1];
        gcells(gc)=gcell_L2(options);
        gc=gc+1;
    end
    return; % block
end
