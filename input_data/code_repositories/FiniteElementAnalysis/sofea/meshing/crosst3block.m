% Mesh of a rectangle located in the range (Cross-diagonal).
%
% function [fens,gcells] = crosst3block(Length,Width,nL,nW,options)
%
% <0,Length> x <0,Width>
% Divided into triangular (t3) elements: nL, nW in the first, second (x,y).
% options = Attributes recognized by the constructor of gcell_T3.
% 
% See the visual gallery by running test_block
% 
function [fens,gcells] = crosst3block(Length,Width,nL,nW,options)
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end
    fens= [fenode(struct('id',1,'xyz',[0 0])),...
        fenode(struct('id',2,'xyz',[1 0])),...
        fenode(struct('id',3,'xyz',[1 1])),...
        fenode(struct('id',4,'xyz',[0 1])),...
        fenode(struct('id',5,'xyz',[1/2 1/2]))];
    options.id =1;
    options.conn =[1, 2, 5];
    gcells=[gcell_T3(options)];
    options.id =2;
    options.conn =[2, 3, 5];
    gcells=[gcells,gcell_T3(options)];
    options.id =3;
    options.conn =[3, 4, 5];
    gcells=[gcells,gcell_T3(options)];
    options.id =4;
    options.conn =[4, 1, 5];
    gcells=[gcells,gcell_T3(options)];
    fens1 =fens;
    gcells1=gcells;
    for i= 1:nL-1
        gcells2=gcells1;
        fens2 = transform_apply(fens1,@translate1, i);
        [fens,gcells,gcells2] = merge_meshes(fens, gcells, fens2, gcells2, 0.01);
        gcells= [gcells,gcells2];
    end
    fens1 =fens;
    gcells1=gcells;
    for i= 1:nW-1
        gcells2=gcells1;
        fens2 = transform_apply(fens1,@translate2, i);
        [fens,gcells,gcells2] = merge_meshes(fens, gcells, fens2, gcells2, 0.01);
        gcells= [gcells,gcells2];
    end
    fens = transform_apply(fens,@scale, [Length/nL,Width/nW]);
end

function xyz=translate1(xyz, i)
    xyz= [xyz(1)+i,xyz(2)];
end

function xyz=translate2(xyz, i)
    xyz= [xyz(1),xyz(2)+i];
end


function xyz=scale(xyz, scales)
    xyz= [xyz(1)*scales(1),xyz(2)*scales(2)];
end

