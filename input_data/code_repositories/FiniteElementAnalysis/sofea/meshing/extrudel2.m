% Extrude a mesh of line segments into a mesh of quadrilaterals.
%
% function [fens,gcells] = extrudel2(fens,gcells,nLayers,extrusionh)
%
% fens,gcells = nodes and cells of the L2 mesh,
% nLayers= number of layers,
% extrusionh= handle to function that computes the extrusion vector
function [fens,gcells] = extrudel2(fens,gcells,nLayers,extrusionh)
    nn1 =length(fens);
    nnt=nn1*nLayers;
    nfens(1:nnt)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    ngc=length(gcells)*nLayers;
    ngcells(1:ngc)=deal(gcell_Q4(struct ('id', 0,'conn',(1:4))));
    for j=1:nn1
        nfens(j)=fens(j);
        xyz=get(fens(j),'xyz');
        xyz =extrusionh(xyz,0);
        nfens(j)=set(fens(j),'xyz', xyz);
    end
    for k=1:nLayers
        for j=1:nn1
            xyz=get(fens(j),'xyz');
            xyz =extrusionh(xyz,k);
            id=get(fens(j),'id');
            f=j+k*nn1;
            nfens(f)=fenode(struct('id',f,'xyz',xyz));
        end
    end

    gc=1;
    for k=1:nLayers
        for i=1:length(gcells)
            conn = get(gcells(i),'conn');
            ngcells(gc)=gcell_Q4(struct('id', gc,'conn',[conn+(k-1)*nn1, conn(end:-1:1)+k*nn1]));
            gc=gc+1;
        end
    end
    fens =nfens;
    gcells=ngcells;
end
