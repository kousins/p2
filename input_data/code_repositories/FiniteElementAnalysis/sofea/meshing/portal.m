% Mesh of a truss portal.
%
% function [fens,gcells] = portal
%
function [fens,gcells] = portal
load portal_node_data
id=portal_node_data(:,1);
xyz=portal_node_data(:,2:4);
fens = [ fenode(struct ('id',id(1),'xyz',xyz(1,:))) ];
for i=2:length(id)
    fens=[ fens fenode(struct ('id',id(i),'xyz',xyz(i,:))) ];
end

load portal_cell_data
id=portal_cell_data(:,1);
conn=portal_cell_data(:,2:3);
area=0.002;
gcells = [ gcell_L2(struct ('id',id(1),'conn',conn(1,:),'other_dimension',area)) ];
for i=2:length(id)
    gcells=[ gcells gcell_L2(struct ('id',id(i),'conn',conn(i,:),'other_dimension',area)) ];
end
