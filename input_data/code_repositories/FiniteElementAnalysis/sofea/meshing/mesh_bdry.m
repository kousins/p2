% Extract the boundary gcells from a mesh.
%
% function bdry_gcells = mesh_bdry(gcells, options)
%
% Extract the boundary gcells of manifold dimension (n-1) from the
% supplied list of gcells of manifold dimension (n).
%    options = struct with any attributes that should be passed to the
%    constructor of the boundary geometric cells
function bdry_gcells = mesh_bdry(gcells, options)
    make =get (gcells(1),'bdrygcell');

    % Form all hyperfaces, non-duplicates are boundary cells
    bdryconn= get(gcells(1),'bdryconn');
    hypf= zeros(size(bdryconn, 1)*length(gcells),size(bdryconn, 2));
    ifac = 1;
    for i=1:length(gcells)
        bdryconn= get (gcells(i),'bdryconn');
        for k= 1:size(bdryconn, 1)
            hypf(ifac,:) = bdryconn(k,:);
            ifac = ifac+ 1;
        end
    end
    sorted_hypf = sort(hypf,2);
    %index vectors m and n such that b =sorted_hypf(m) and sorted_hypf = b(n)
    [b,m,n] = unique(sorted_hypf,'rows');
    u = find(histc(n,1:max(n))==1);
    bdry = hypf(m(u),:);
    nbc= size(bdry, 1);
    bdry_gcells(1:nbc)=deal(feval(make));
    for i=1:size(bdry, 1)
        options.id =i;
        options.conn =bdry(i,:);
        bdry_gcells(i)=feval(make,options);
    end
    return
end
