% Tetrahedral (t4) Mesh of a block located in a given range
%
% function [fens,gcells] = t4block(Length,Width,Height,nL,nW,nH)
%
% Range =<0,Length> x <0,Width> x <0,Height>
% Divided into elements: nL, nW, nH in the first, second, and
% third direction (x,y,z).
function [fens,gcells] = t4block(Length,Width,Height,nL,nW,nH)
nnodes=(nL+1)*(nW+1)*(nH+1);
fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
ncells=(nL)*(nW)*(nH);
gcells(1:ncells)=deal(gcell_T4(struct ('id', 0,'conn',(1:4))));
t4i = [1, 8, 5, 6; 3, 4, 2, 7; 7, 2, 6, 8; 4, 7, 8, 2; 2, 1, 6, 8; 4, 8, 1, 2];
f=1;
for k=1:(nH+1)
    for j=1:(nW+1)
        for i=1:(nL+1)
            xyz=[(i-1)*Length/nL (j-1)*Width/nW (k-1)*Height/nH];
            fens(f)=fenode(struct('id',f,'xyz',xyz));
            f=f+1;
        end
    end
end

gc=1;
for i=1:nL
    for j=1:nW
        for k=1:nH
            nn=node_numbers(i,j,k,nL,nW,nH);
            for r=1:6
                gcells(gc)=gcell_T4(struct ('id', gc,'conn',nn(t4i(r,:))));
                gc=gc+1;
            end
        end
    end
end
return; % block

function nn=node_numbers(i,j,k,nL,nW,nH)
f=(k-1)*((nL+1)*(nW+1))+(j-1)*(nL+1)+i;
nn=[f (f+1)  f+(nL+1)+1 f+(nL+1)];
nn=[nn nn+((nL+1)*(nW+1))];

