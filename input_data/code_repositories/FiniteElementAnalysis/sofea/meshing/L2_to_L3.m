% Convert a mesh of L2 elements (two-node) to L3 line elements.
%
% function [fens,gcells] = L2_to_L3(fens,gcells,options)
%
% options =attributes recognized by the constructor gcell_L3
function [fens,gcells] = L2_to_L3(fens,gcells,options)
    if ~isstruct(options)
        options = struct('id',1);
    end
    % now generate new node number for each Element
    n=length(fens);
    fens(length(fens)+1:n)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    ngcells(1:length(gcells))=deal(gcell_L3(struct('id',1,'conn',(1:3))));
    for i= 1:length(gcells)
        conn = get (gcells(i),'conn');
        fens(n+i)=fenode(struct ('id',n+i,'xyz',(get(fens(conn(1)),'xyz')+get(fens(conn(2)),'xyz'))/2));
        options.id =i;
        options.conn =[conn n+i];
        ngcells(i) =gcell_L3(options);
    end
    gcells=ngcells;
    return;
end
