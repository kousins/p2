% Create a mesh of 1/8 of the sphere of "radius". 
%
% function [fens,gcells]=q4sphere(radius,nrefine,thickness)
%
% Create a mesh of 1/8 of the sphere of "radius". The  mesh will consist of
% three quadrilateral elements if "nrefine==0", or more if "nrefine>0".
% "nrefine" is the number of bisections applied  to refine the mesh.
% The quadrilaterals have thickness "thickness".
function [fens,gcells]=q4sphere(radius,nrefine,thickness)
    fens(1) =fenode(struct('id',1,'xyz',[1, 0, 0]));
    fens(2) =fenode(struct('id',2,'xyz',[0, 1, 0]));
    fens(3) =fenode(struct('id',3,'xyz',[0, 0, 1]));
    fens(4) =fenode(struct('id',4,'xyz',[1, 1, 0]));
    fens(5) =fenode(struct('id',5,'xyz',[0, 1, 1]));
    fens(6) =fenode(struct('id',6,'xyz',[1, 0, 1]));
    fens(7) =fenode(struct('id',7,'xyz',[1, 1, 1]));
    gcells(1)=gcell_Q4(struct ('id', 1,'conn',[1, 4, 7, 6],'other_dimension', thickness));
    gcells(2)=gcell_Q4(struct ('id', 2,'conn',[4, 2, 5, 7],'other_dimension', thickness));
    gcells(3)=gcell_Q4(struct ('id', 3,'conn',[3, 6, 7, 5],'other_dimension', thickness));
    fens= onto_sphere(fens, radius);
    for i=1:nrefine
        [fens,gcells]=refineq4(fens,gcells);
        fens= onto_sphere(fens, radius);
    end
end
