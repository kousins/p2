% Mesh of a rectangle located in a given range.  Never bisects the corners.
%
% function [fens,gcells] = t3block2dn(Length,Width,nL,nW,options)
%
% Range =<0,Length> x <0,Width>
% Divided into triangular (t3) elements: nL, nW in the first, second (x,y).
% This function never bisects the corners.
% options = Attributes recognized by the constructor of gcell_T3.
% 
% See the visual gallery by running test_block
% 
function [fens,gcells] = t3block2dn(Length,Width,nL,nW,options)
    nnodes=(nL+1)*(nW+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0])));
    ncells=2*(nL)*(nW);
    gcells(1:ncells)=deal(gcell_T3);
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end
    f=1;
    for j=1:(nW+1)
        for i=1:(nL+1)
            xyz=[(i-1)*Length/nL (j-1)*Width/nW];
            fens(f)=fenode(struct('id',f,'xyz',xyz));
            f=f+1;
        end
    end

    gc=1;
    for i=1:nL
        for j=1:nW
            if ((i==1) & (j==nW)) | ((i==nL) & (j==1))
                 nn=node_numbers1(i,j,nL,nW);
                options.id =gc;
                options.conn =nn;
                gcells(gc)=gcell_T3(options);
                gc=gc+1;
                nn=node_numbers2(i,j,nL,nW);
                options.id =gc;
                options.conn =nn;
                gcells(gc)=gcell_T3(options);
                gc=gc+1;
           else
                nn=node_numbers3(i,j,nL,nW);
                options.id =gc;
                options.conn =nn;
                gcells(gc)=gcell_T3(options);
                gc=gc+1;
                nn=node_numbers4(i,j,nL,nW);
                options.id =gc;
                options.conn =nn;
                gcells(gc)=gcell_T3(options);
                gc=gc+1;
            end
        end
    end
    return; % block
end

function nn=node_numbers1(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[f (f+1) f+(nL+1)+1];
    return;
end
function nn=node_numbers2(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[f  f+(nL+1)+1 f+(nL+1)];
    return;
end

function nn=node_numbers3(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[f (f+1) f+(nL+1)];
    return;
end
function nn=node_numbers4(i,j,nL,nW)
    f=(j-1)*(nL+1)+i;
    nn=[(f+1)  f+(nL+1)+1 f+(nL+1)];
    return;
end
