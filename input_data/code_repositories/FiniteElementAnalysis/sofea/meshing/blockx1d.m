% Mesh of a 1-D block, L2 cells. The nodes are located at the supplied
% coordinates.  This allows for construction of graded meshes.
%
% function [fens,gcells] = blockx1d(xs,options)
%
% xs =Locations of the individual nodes.
% options = structure with fields recognized by the constructor of the
%       gcell_L2 finite element
%
% Example:
% [fens,gcells] = blockx1d((2:1:7).^3, struct('other_dimension', 0.1)); 
% drawmesh({fens,gcells},'nodes','gcells','facecolor','red')
function  [fens,gcells] = blockx1d(xs,options)
    if ~isstruct(options)
        other_dimension = options; clear options;
        options.other_dimension = other_dimension;
    end
    nL =length(xs)-1;

    nnodes=(nL+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0])));
    ncells=(nL);
    gcells(1:ncells)=deal(gcell_L2(struct ('id', 0,'conn',(1:2))));

    f=1;
    for i=1:(nL+1)
        xyz=[xs(i)];
        fens(f)=fenode(struct('id',f,'xyz',xyz));
        f=f+1;
    end

    gc=1;
    for i=1:nL
        nn=node_numbers(i,nL);
        options.id =gc;
        options.conn =nn;
        gcells(gc)=gcell_L2(options);
        gc=gc+1;
    end
end


function nn=node_numbers(i,nL)
    f=i;
    nn=[f (f+1)];
    return;
end
