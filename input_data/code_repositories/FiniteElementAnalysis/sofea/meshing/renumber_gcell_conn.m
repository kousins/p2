% Renumber the nodes in the connectivity of the geometric cells based on a new
% numbering for the nodes.
% 
% function gcells = renumber_gcell_conn(gcells, new_numbering)
% 
% gcells =array of geometric cells 
% new_numbering = new serial numbers for the nodes.  The connectivity
% should be changed as conn(j) --> new_numbering(conn(j))
% 
% Output:
% gcells = new array of geometric cells
% 
% Let us say there are nodes not connected to any geometric cell that you
% would like to remove from the mesh: here is how that would be
% accomplished.
% 
% connected = find_unconn_fens(fens, gcells);
% [fens, new_numbering] =compact_fens(fens, connected);
% gcells = renumber_gcell_conn(gcells, new_numbering);
% 
% Finally, check that the mesh is valid:
% validate_mesh(fens, gcells);
% 
function gcells = renumber_gcell_conn(gcells, new_numbering)
    for i=1:length(gcells),
        conn=get(gcells(i),'conn');
        gcells(i)=set(gcells(i),'conn',new_numbering(conn)');
    end
end
