% Import 10-node tetrahedra ABAQUS Mesh.
% 
% function [fens,gcells]=Abaqus_mesh_import(filename,xyzscale,saveReadData)
% 
% limitations: only the *NODE and *ELEMENT  sections are read. Only 10-node
% tetrahedra  are handled.
function [fens,gcells]=Abaqus_mesh_import(filename,xyzscale,saveReadData)

    if (~exist('saveReadData','var'))
        saveReadData = true;
    end

    [pathstr, name, ext] = fileparts(filename) ;
    if (isempty(pathstr)),pathstr ='.';end
    saveFile = [pathstr filesep name ,'_xyzconn' '.mat'];

    Doread = true;
    if (saveReadData)
        if (exist(saveFile,'file'))
            try,
                load(saveFile);
                Doread= false;
            catch,end;
        end
    end
    if (Doread)
        fid=fopen(filename,'r');

        if fid == -1,
            error('Unable to open specified file.');
        end

        chunk=1000;

        nnode=0;
        node=zeros(chunk,4);
        temp = '';
        while ~strncmpi(temp,'*NODE',5)
            temp = fgetl(fid);   if ~ischar(temp), break, end
        end
        if (~strncmpi(temp,'*NODE',5))
            error
        end

        temp = '';
        while true
            nnode=nnode+1;
            temp = fgetl(fid);
            if (strncmpi(temp,'*',1))
                nnode=nnode-1;
                break
            end
            A = sscanf(temp, '%g,%g,%g,%g');
            node(nnode,:)=A;
            if (size(node,1)<=nnode)
                node(size(node,1)+1:size(node,1)+chunk,:)=zeros(chunk,4);
            end
        end

        while ~strncmpi(temp,'*ELEMENT',8)
            temp = fgetl(fid);   if ~ischar(temp), break, end
        end
        if (~strncmpi(temp,'*ELEMENT',8))
            error
        end

        nelem=0;
        elem= [];
        ennod= 0;
        temp = '';
        while true
            nelem=nelem+1;
            temp = fgetl(fid);
            if (isempty(elem))
                All =sscanf(temp, '%d,',inf);
                ennod =length(All)-1;
                elem=zeros(chunk,ennod+1);
            end
            if (strncmpi(temp,'*',1))
                nelem=nelem-1;
                break
            end
            A = sscanf(temp, '%d,',inf);
            elem(nelem,:)=A';
            if (size(elem,1)<=nelem)
                elem(size(elem,1)+1:size(elem,1)+chunk,:)=zeros(chunk,ennod+1);
            end
        end

        fclose(fid);

        if (norm((1:nnode)'-node(1:nnode,1)))
            error
        end

        xyz=node(node(1:nnode,1),2:4);
        xyz =xyzscale*xyz;
        conn=elem(elem(1:nelem,1),2:end);
    end

    clear fens
    fens(1:size(xyz,1))=fenode(struct('id',0,'xyz',[0,0,0]));
    for i=1:size(xyz,1)
        fens(i)=fenode(struct('id',i,'xyz',xyz(i,:)));
    end
    clear gcells
    switch ennod
        case 4
            Constructor =@gcell_T4;
        case 10
            Constructor =@gcell_T10;
        otherwise
            error('Unable to open specified file.');
    end
    gcells(1:size(conn,1))= Constructor(struct('id',0,'conn',(1:ennod)));
    for i=1:size(conn,1)
        gcells(i)=Constructor(struct('id',i,'conn',conn(i,:)));
    end
    

    if (saveReadData && (Doread))
        save(saveFile,'xyz','conn')
    end
end
