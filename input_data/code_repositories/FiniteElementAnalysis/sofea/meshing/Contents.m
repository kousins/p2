% Mesh files
%
% Graphics utility:
%   drawmesh           - Draw a finite element mesh.
%   drawbasis2d        - Draw a basis function on a 2D finite element mesh.
%
% 3-D mesh creation functions:
%   blockx             - Mesh of a 3-D block, graded mesh, H8 cells
%   block              - Mesh of a 3-D block located in a given range, H8 cells
%   blockH20           - Mesh of a block located in a given range, H20 cells
%   t4block            - Tetrahedral (t4) Mesh of a block. Six-tet orientation.
%   t4blocka           - Tetrahedral (t4) Mesh of a block. Five-tet orientation.
%   t4blockb           - Tetrahedral (t4) Mesh of a block. Five-tet orientation.
%   t4blockc           - Tetrahedral (t4) Mesh of a block. Crosshatched
%   t4blockc2          - Tetrahedral (t4) Mesh of a block. Crosshatched, variation
%   t4blockT10         - Tetrahedral (T10) Mesh of a block located in the range
%   t4blockdel         - Tetrahedral (t4) Delaunay Mesh of a block located in a given range
%   t4cylinderdel      - Tetrahedral (t4) Delaunay Mesh of a cylinder of Length and Radius.
%   mesh_hexahedron    - Mesh of a general hexahedron given by the location of the
%                        corners
%   extrudeq4          - Extrude a mesh of quadrilaterals into a mesh of hexahedra.
%   Abaqus_mesh_import - Import 10-node tetrahedra ABAQUS Mesh.
%
% 2-D mesh creation functions:
%   mesh_quadrilateral - Mesh of a general quadrilateral given by the location of the vertices.
%                        corners
%   blockx2d           - Mesh of a 2-D block, graded mesh, Q4 cells
%   block2d            - Mesh of a rectangle located in a given range
%   q4block2du         - Mesh of a rectangle located in a given range (Unstructured mesh).
%                        unstructured
%   q4sphere           - Create a mesh of 1/8 of the sphere of "radius". 
%   q4circle           - Create a mesh of a quarter circle.
%   at3block2d         - Mesh of a rectangle located in a given range
%   at3block2dc        - Mesh of a rectangle located in a given range
%   ct3block2d         - Mesh of a rectangle located in a given range
%   crosst3block       - Mesh of a rectangle located in the range (Cross-diagonal).
%   t3annulus          - Mesh of an annulus segment , triangles T3.
%   t3block2d          - Mesh of a rectangle located in a given range
%   t3block2dc         - Mesh of a rectangle located in a given range. Corners are bisected.
%                        bisected corners
%   t3block2du         - Mesh of a rectangle located in a given range.  An unstructured mesh.
%                        unstructured
%   t3block2dn         - Mesh of a rectangle located in a given range.  Never bisects the corners.
%                        corners never bisected
%   targe2_mesher      - Automatic triangulation of a general 2-D domain. 
%                        The input is given as a cell array Of commands.
%   targe2_mesher_vl   - Create triangulation of a domain given as an array of vertices.
%                        polygon.
%   refineq4           - Refine a mesh of quadrilaterals by bisection
%   elliphole          - Mesh of one quarter of a rectangular plate with an elliptical hole
%   extrudel2          - Extrude a mesh of line segments into a mesh of quadrilaterals.
% 
% 1-D mesh creation functions:
%   blockx1d           - Mesh of a 1-D block, graded mesh, L2 cells
%   block1d            - Mesh of a line segment located in a given range, L2 Elements
%                        cells
% 
% Selection functions:
%   fenode_select      - Select nodes.
%   gcell_select       - Select geometric cells.
%   connected_nodes    - Extract the node numbers of the nodes  connected by given gcells. 
%                        nodes that are connected by given gcells.
% 
% Conversion functions (from linear to quadratic and so on):
%   L2_to_L3           - Convert a mesh of L2 Line elements to L3 elements
%   H8_to_H20          - Convert a mesh of hexahedron H8 to hexahedron H20.
%   Q4_to_Q8           - Convert a mesh of quadrilateral Q4 to quadrilateral Q8.
%   T3_to_T6           - Convert a mesh of triangle T3 (three-node) to triangle T6.
%   T4_to_T10          - Convert a mesh of Tetrahedron T4 (four-node) to Tetrahedron T10.
%   Q4_to_aT3          - Convert a mesh of quadrilateral Q4's to 2 triangles T3 each.
%   Q4_to_T3           - Convert a mesh of quadrilateral Q4's to 2 triangles T3 each.
%   T3_to_Q4           - Convert a mesh of triangles T3 to 4 quadrilateral Q4's each.
%
% Mesh manipulation functions:
%   merge_meshes       - Merge two meshes together by gluing together nodes within tolerance.
%                        within tolerance.
%   mirror_mesh        - Mirror a 2-D mesh in a plane given by its normal and one point. 
%                        one point. 
%   mesh_bdry          - Extract the boundary gcells from a mesh.
%   onto_sphere        - Project nodes onto a sphere of given radius.
%                        of a sphere by radial protection
%   transform_2_helix  - Generate the mesh of an helix. 
%   transform_apply    - Apply geometry transformation to the locations of the nodes.
%   block_function_shift - Shift nodes in a three-dimensional block using a displacement function.
%   renumber_gcell_conn  - Renumber the nodes in the connectivity of the geometric cells based on a new
%   find_unconn_fens     - Find nodes that are not connected to any cell in the array gcells.
%   compact_fens         - Compact the finite element node array by deleting unconnected nodes.
%   validate_mesh        - Validate finite element mesh.
%   renum_mesh           - Renumber the nodes and the cells using an optimal numbering of the finite
%   renum_mesh_numbering - Compute an optimal numbering of the finite
%                        element nodes.
%   renum_mesh_renumber  - Renumber the finite element nodes and the
%                          geometric cells.
%   conn_to_sparse       - Construct a sparse matrix from connectivities.
%   gcells_to_conns      - Make  a cell array of connectivities for use in mesh renumbering routines.
% 
% Miscellaneous meshes:
%   annulus            - Mesh of an annulus segment.
%   L2x2               - 2 x 2 quadrilateral element mesh of one quarter of a square domain with a square hole.
%   arc2               - Mesh of an arc. Hexahedra H8.
%   barrelq            - The Scordelis-Lo barrel vault mesh.
%   bridge             - Create mesh of a concrete bridge.
%   holyplate2         - Mesh of a quarter-plate with a hole: 2x2 division
%   mpart              - Mesh of a simple mechanical part
%   portal             - Mesh of a truss portal.


