% Mesh of a 3-D block, H8 cells. The nodes are located at the Cartesian
% product of the three intervals on the input.  This allows for
% construction of graded meshes.
%
% function [fens,gcells] = blockx(xs,ys,zs)
%
% xs,ys,zs =Locations of the individual planes of nodes. 
% 
% Example:
% [fens,gcells] = blockx(1/125*(0:1:5).^3,4+(0:2:8),[3, 5, 9]); drawmesh ({fens,gcells})
function [fens,gcells] = blockx(xs,ys,zs)
    nL =length(xs)-1;
    nW =length(ys)-1;
    nH =length(zs)-1;

    nnodes=(nL+1)*(nW+1)*(nH+1);
    fens(1:nnodes)=deal(fenode(struct('id',0,'xyz',[0 0 0])));
    ncells=(nL)*(nW)*(nH);
    gcells(1:ncells)=deal(gcell_H8(struct ('id', 0,'conn',(1:8))));

    f=1;
    for k=1:(nH+1)
        for j=1:(nW+1)
            for i=1:(nL+1)
                xyz=[xs(i) ys(j) zs(k)];
                fens(f)=fenode(struct('id',f,'xyz',xyz));
                f=f+1;
            end
        end
    end

    gc=1;
    for i=1:nL
        for j=1:nW
            for k=1:nH
                nn=node_numbers(i,j,k,nL,nW,nH);
                gcells(gc)=gcell_H8(struct ('id', gc,'conn',nn));
                gc=gc+1;
            end
        end
    end
    return; % block
end

% first go along Length, then Width, and finally Height
function nn=node_numbers(i,j,k,nL,nW,nH)
    f=(k-1)*((nL+1)*(nW+1))+(j-1)*(nL+1)+i;
    nn=[f (f+1)  f+(nL+1)+1 f+(nL+1)];
    nn=[nn nn+((nL+1)*(nW+1))];
end
