%MESH_HEXAHEDRON Mesh of a general hexahedron given by the location of the
%
% function [fens,gcells] = mesh_hexahedron(xyz,nL,nW,nH)
% 
% or, with the optional argument block_mesh_handle
% function [fens,gcells] = mesh_hexahedron(xyz,nL,nW,nH,block_mesh_handle)
%
% xyz = One vertex location per row; Either two rows (for a rectangular
%      block given by the two corners), or eight rows (general hexahedron).
% nL, nW, nH = Divided into elements: nL, nW, nH in the first, second, and
%      third direction. 
% Optional argument:
% block_mesh_handle = function handle of the block-generating mesh function
%      (having the signature of the function block()).
function [fens,gcells] = mesh_hexahedron(xyz,nL,nW,nH,block_mesh_handle)
    npts=size(xyz,1);
    if npts==2
        lo=min(xyz);
        hi=max(xyz);
        xyz=[[lo(1),lo(2),lo(3)];...
            [hi(1),lo(2),lo(3)];...
            [hi(1),hi(2),lo(3)];...
            [lo(1),hi(2),lo(3)];...
            [lo(1),lo(2),hi(3)];...
            [hi(1),lo(2),hi(3)];...
            [hi(1),hi(2),hi(3)];...
            [lo(1),hi(2),hi(3)]];
    elseif npts~=8
        error('Need 2 or 8 points');
    end

    if ~exist('block_mesh_handle')
        block_mesh_handle =@block;
    end

    [fens,gcells] = block_mesh_handle(2,2,2,nL,nW,nH);

    for i=1:length(fens)
        xyz1=get(fens(i),'xyz');
        N = bfun(xyz1-1);% shift coordinates by -1
        fens(i)=set(fens(i),'xyz',N'*xyz);
    end
end

function val = bfun(param_coords)
    one_minus_xi    = (1 - param_coords(1));
    one_minus_eta   = (1 - param_coords(2));
    one_minus_theta = (1 - param_coords(3));
    one_plus_xi     = (1 + param_coords(1));
    one_plus_eta    = (1 + param_coords(2));
    one_plus_theta  = (1 + param_coords(3));
    val = [one_minus_xi*one_minus_eta*one_minus_theta;...
        one_plus_xi*one_minus_eta*one_minus_theta;...
        one_plus_xi*one_plus_eta*one_minus_theta;...
        one_minus_xi*one_plus_eta*one_minus_theta;...
        one_minus_xi*one_minus_eta*one_plus_theta;...
        one_plus_xi*one_minus_eta*one_plus_theta;...
        one_plus_xi*one_plus_eta*one_plus_theta;...
        one_minus_xi*one_plus_eta*one_plus_theta] / 8;
    return;
end
