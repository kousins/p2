%
% Initialize the sofea toolbox.
% Usage: sofea_init()
%
function sofea_init()
    addpath('util');
    homePath = sofea_path;
    sep = filesep;


    % Set paths
    addpath(homePath,...
        [homePath sep 'classes'],...
        [homePath sep 'meshing'], ...
        [homePath sep 'examples'],...
        [homePath sep 'util']);
    add_subdirs_to_path ([homePath sep 'classes'], sep);
    add_subdirs_to_path ([homePath sep 'examples'], sep);
    add_subdirs_to_path ([homePath sep 'meshing'], sep);
    add_subdirs_to_path ([homePath sep 'extensions'], sep);
    if strcmp(version('-release'),'13')
        warning off MATLAB:m_warning_end_without_block
    end

    disp(' ');
    disp(['       Synergetic Objects for Finite Element Analysis  ']);
    disp(['        SOFEA 3.2 Toolbox (C) 05/13/2009, Petr Krysl.']);
%     disp(['        SOFEA 3.1 Toolbox (C) 03/13/2009, Petr Krysl.']);
%     disp(['        SOFEA 3.0 Toolbox (C) 02/13/2009, Petr Krysl.']);
%     disp(['        SOFEA 2.9 Toolbox (C) 01/03/2009, Petr Krysl.']);
%     disp(['        SOFEA 2.8 Toolbox (C) 08/31/2008, Petr Krysl.']);
%     disp(['        SOFEA 2.7 Toolbox (C) 07/31/2008, Petr Krysl.']);
%     disp(['        SOFEA 2.6 Toolbox (C) 05/31/2008, Petr Krysl.']);
    % disp(['        SOFEA 2.5 Toolbox (C) 04/25/2008, Petr Krysl.']);
    % disp(['        SOFEA 2.4 Toolbox (C) 02/13/2008, Petr Krysl.']);
    % disp(['        SOFEA 2.3 Toolbox (C) 12/31/2007, Petr Krysl.']);
    % disp(['        SOFEA 2.2 Toolbox (C) 12/31/2006, Petr Krysl.']);
    % disp(['        SOFEA 2.1 (C) 10/13/2006, Petr Krysl.']);
    % disp(['        SOFEA 1.0 (C) 10/13/2005, Petr Krysl.']);
    % disp('  Please report problems and/or bugs to: pkrysl@ucsd.edu');
    disp('  -------------------------------------------------------------');
    disp('  To get started, run some of the scripts in the "examples" ')
    disp('  directory. Help is available: type doc sofea (or help sofea).')
    disp(' ');

    % spl = mysplash;
    % pause(3.0);
    % delete(spl);
    return;

    function add_subdirs_to_path(d, sep)
        dl=dir(d);
        for i=1:length(dl)
            if (dl(i).isdir)
                if      (~strcmp(dl(i).name,'.')) & ...
                        (~strcmp(dl(i).name,'..')) & ...
                        (~strcmp(dl(i).name,'CVS')) & ...
                        (~strcmp(dl(i).name,'cvs')) & ...
                        (~strcmp(dl(i).name(1),'@'))
                    addpath([d sep dl(i).name])
                    add_subdirs_to_path([d sep dl(i).name], sep);
                end
            end
        end
        return;
    end
end
function varargout = mysplash(varargin)
    % SPLASH M-file for splash.fig
    %      SPLASH, by itself, creates a new SPLASH or raises the existing
    %      singleton*.
    %
    %      H = SPLASH returns the handle to a new SPLASH or the handle to
    %      the existing singleton*.
    %
    %      SPLASH('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in SPLASH.M with the given input arguments.
    %
    %      SPLASH('Property','Value',...) creates a new SPLASH or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before splash_OpeningFunction gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to splash_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Copyright 2002-2003 The MathWorks, Inc.

    % Edit the above text to modify the response to help splash

    % Last Modified by GUIDE v2.5 24-Dec-2004 10:49:23

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @splash_OpeningFcn, ...
        'gui_OutputFcn',  @splash_OutputFcn, ...
        'gui_LayoutFcn',  @splash_LayoutFcn, ...
        'gui_Callback',   []);
    if nargin
        if ischar(varargin{1})
            gui_State.gui_Callback = str2func(varargin{1});
        end
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end

% --- Executes just before splash is made visible.
function splash_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to splash (see VARARGIN)

    % Choose default command line output for splash
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes splash wait for user response (see UIRESUME)
    % uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = splash_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;
end

% --- Creates and returns a handle to the GUI figure.
function h1 = splash_LayoutFcn(policy)
    % policy - create a new figure or use a singleton. 'new' or 'reuse'.

    persistent hsingleton;
    if strcmpi(policy, 'reuse') & ishandle(hsingleton)
        h1 = hsingleton;
        return;
    end

    appdata = [];
    appdata.GUIDEOptions = struct(...
        'active_h', [], ...
        'taginfo', struct(...
        'figure', 2, ...
        'axes', 2, ...
        'text', 6), ...
        'override', 0, ...
        'release', 13, ...
        'resize', 'none', ...
        'accessibility', 'callback', ...
        'mfile', 1, ...
        'callbacks', 1, ...
        'singleton', 1, ...
        'syscolorfig', 1, ...
        'blocking', 0, ...
        'lastSavedFile', 'C:\Documents and Settings\pkrysl\My Documents\sofea\splash.m');
    appdata.lastValidTag = 'figure1';
    appdata.GUIDELayoutEditor = [];

    scSize=get(0,'ScreenSize');

    % 'Position',[scSize(3)/2-50 scSize(3)/2-13 100.2 26.9230769230769],...
    h1 = figure(...
        'Units','characters',...
        'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
        'IntegerHandle','off',...
        'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
        'MenuBar','none',...
        'Name','SOFEA (C) 2005-2007, Petr Krysl',...
        'NumberTitle','off',...
        'PaperPosition',get(0,'defaultfigurePaperPosition'),...
        'Position',[scSize(3)/5/2-50 scSize(4)/11/2-13 100.2 26.9230769230769],...
        'Renderer',get(0,'defaultfigureRenderer'),...
        'RendererMode','manual',...
        'Resize','off',...
        'HandleVisibility','callback',...
        'Tag','figure1',...
        'UserData',[],...
        'Visible','on',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );

    appdata = [];
    appdata.lastValidTag = 'text1';

    bg = uicontrol(...
        'Parent',h1,...
        'Units','characters',...
        'FontSize',47,...
        'BackgroundColor','white',...
        'Position',[0 0 100.2 26.9230769230769],...
        'String','',...
        'Style','text',...
        'Tag','bg1',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );

    h2 = uicontrol(...
        'Parent',h1,...
        'Units','characters',...
        'FontSize',96,'FontName','Helvetica',...
        'ForegroundColor','blue',...
        'BackgroundColor','white',...
        'Position',[6.2 8 88.6 9.846153846154],...
        'String','SOFEA',...
        'Style','text',...
        'Tag','text1',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );

    appdata = [];
    appdata.lastValidTag = 'text2';

    h3 = uicontrol(...
        'Parent',h1,...
        'Units','characters',...
        'FontSize', 24,...
        'BackgroundColor','white',...
        'Position',[8.4 1.30769230769231 84.2 3.53846153846154],...
        'String','(C) 2005-2007 Petr Krysl',...
        'Style','text',...
        'Tag','text2',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );

    appdata = [];
    appdata.lastValidTag = 'text3';

    appdata = [];
    appdata.lastValidTag = 'text4';

    h5 = uicontrol(...
        'Parent',h1,...
        'Units','characters',...
        'FontSize',24,...
        'BackgroundColor','white',...
        'Position',[8.6 22.0769230769231 83.8 3.38461538461539],...
        'String','Finite Element',...
        'Style','text',...
        'Tag','text4',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );

    appdata = [];
    appdata.lastValidTag = 'text5';

    h6 = uicontrol(...
        'Parent',h1,...
        'Units','characters',...
        'FontSize',24,...
        'BackgroundColor','white',...
        'Position',[22.8 19.0769230769231 54.4 3.23076923076923],...
        'String','Analysis Toolbox',...
        'Style','text',...
        'Tag','text5',...
        'CreateFcn', {@local_CreateFcn, '', appdata} );


    hsingleton = h1;
end

% --- Set application data first then calling the CreateFcn.
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

    if ~isempty(appdata)
        names = fieldnames(appdata);
        for i=1:length(names)
            name = char(names(i));
            setappdata(hObject, name, getfield(appdata,name));
        end
    end

    if ~isempty(createfcn)
        eval(createfcn);
    end
end

% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)


    %   GUI_MAINFCN provides these command line APIs for dealing with GUIs
    %
    %      SPLASH, by itself, creates a new SPLASH or raises the existing
    %      singleton*.
    %
    %      H = SPLASH returns the handle to a new SPLASH or the handle to
    %      the existing singleton*.
    %
    %      SPLASH('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in SPLASH.M with the given input arguments.
    %
    %      SPLASH('Property','Value',...) creates a new SPLASH or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before untitled_OpeningFunction gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to untitled_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".

    %   Copyright 1984-2004 The MathWorks, Inc.
    %   $Revision: 1.9 $ $Date: 2009/05/15 16:59:11 $

    gui_StateFields =  {'gui_Name'
        'gui_Singleton'
        'gui_OpeningFcn'
        'gui_OutputFcn'
        'gui_LayoutFcn'
        'gui_Callback'};
    gui_Mfile = '';
    for i=1:length(gui_StateFields)
        if ~isfield(gui_State, gui_StateFields{i})
            error('Could not find field %s in the gui_State struct in GUI M-file %s', gui_StateFields{i}, gui_Mfile);
        elseif isequal(gui_StateFields{i}, 'gui_Name')
            gui_Mfile = [gui_State.gui_Name, '.m'];
        end
    end

    numargin = length(varargin);

    gui_Create = 1;

    if gui_Create == 0
        varargin{1} = gui_State.gui_Callback;
        if nargout
            [varargout{1:nargout}] = feval(varargin{:});
        else
            feval(varargin{:});
        end
    else
        if gui_State.gui_Singleton
            gui_SingletonOpt = 'reuse';
        else
            gui_SingletonOpt = 'new';
        end

        % Open fig file with stored settings.  Note: This executes all component
        % specific CreateFunctions with an empty HANDLES structure.

        % Do feval on layout code in m-file if it exists
        if ~isempty(gui_State.gui_LayoutFcn)
            gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);
            % openfig (called by local_openfig below) does this for guis without
            % the LayoutFcn. Be sure to do it here so guis show up on screen.
            movegui(gui_hFigure,'onscreen')
        else
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt);
            % If the figure has InGUIInitialization it was not completely created
            % on the last pass.  Delete this handle and try again.
            if isappdata(gui_hFigure, 'InGUIInitialization')
                delete(gui_hFigure);
                gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt);
            end
        end

        % Set flag to indicate starting GUI initialization
        setappdata(gui_hFigure,'InGUIInitialization',1);

        % Fetch GUIDE Application options
        gui_Options = getappdata(gui_hFigure,'GUIDEOptions');

        if ~isappdata(gui_hFigure,'GUIOnScreen')
            % Adjust background color
            if gui_Options.syscolorfig
                set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
            end

            % Generate HANDLES structure and store with GUIDATA
            guidata(gui_hFigure, guihandles(gui_hFigure));
        end

        % If user specified 'Visible','off' in p/v pairs, don't make the figure
        % visible.
        gui_MakeVisible = 1;
        for ind=1:2:length(varargin)
            if length(varargin) == ind
                break;
            end
            len1 = min(length('visible'),length(varargin{ind}));
            len2 = min(length('off'),length(varargin{ind+1}));
            if ischar(varargin{ind}) & ischar(varargin{ind+1}) & ...
                    strncmpi(varargin{ind},'visible',len1) & len2 > 1
                if strncmpi(varargin{ind+1},'off',len2)
                    gui_MakeVisible = 0;
                elseif strncmpi(varargin{ind+1},'on',len2)
                    gui_MakeVisible = 1;
                end
            end
        end

        % Check for figure param value pairs
        for index=1:2:length(varargin)
            if length(varargin) == index
                break;
            end
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end

        % If handle visibility is set to 'callback', turn it on until finished
        % with OpeningFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end

        feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

        if ishandle(gui_hFigure)
            % Update handle visibility
            set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

            % Make figure visible
            if gui_MakeVisible
                set(gui_hFigure, 'Visible', 'on')
                if gui_Options.singleton
                    setappdata(gui_hFigure,'GUIOnScreen', 1);
                end
            end

            % Done with GUI initialization
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until finished with
        % OutputFcn
        if ishandle(gui_hFigure)
            gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
            if strcmp(gui_HandleVisibility, 'callback')
                set(gui_hFigure,'HandleVisibility', 'on');
            end
            gui_Handles = guidata(gui_hFigure);
        else
            gui_Handles = [];
        end

        if nargout
            [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
        else
            feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
        end

        if ishandle(gui_hFigure)
            set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
        end
    end
end
function gui_hFigure = local_openfig(name, singleton)

    % openfig with three arguments was new from R13. Try to call that first, if
    % failed, try the old openfig.
    try
        gui_hFigure = openfig(name, singleton, 'auto');
    catch
        % OPENFIG did not accept 3rd input argument until R13,
        % toggle default figure visible to prevent the figure
        % from showing up too soon.
        gui_OldDefaultVisible = get(0,'defaultFigureVisible');
        set(0,'defaultFigureVisible','off');
        gui_hFigure = openfig(name, singleton);
        set(0,'defaultFigureVisible',gui_OldDefaultVisible);
    end
end
