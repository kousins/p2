% 
% SOFEA: for copyright and other information please refer 
% to the <a href="matlab:edit 'README'">README</a> file.
% 
%   sofea_init    - Initialize the toolbox. Run this function to
%                   set up the path for the toolbox. If you
%                   encounter weird error messages about files
%                   not being found, the path is not set up
%                   correctly. Run this function!
% 
%   Folders:
%   <a href="matlab:helpwin 'sofea/classes'">sofea/classes</a>       - The working classes. 
%   <a href="matlab:helpwin 'sofea/documents'">sofea/documents</a>     - Various documents.
%   <a href="matlab:helpwin 'sofea/examples'">sofea/examples</a>      - Script/function drivers for various examples.
%   <a href="matlab:helpwin 'sofea/meshing'">sofea/meshing</a>       - Mesh command files.
%   <a href="matlab:helpwin 'sofea/util'">sofea/util</a>          - Utilities.
%   sofea/work          - Keep your temporary files here.
%
% SOFEA objects are best viewed with OBgui. See other details under the
% heading sofea/classes.
