% data_colormap
%   This class provides mapping of data values to colors. 
% 
%   @data_colormap/data_colormap - Constructor.
%   @data_colormap/map_data - Map the data.
