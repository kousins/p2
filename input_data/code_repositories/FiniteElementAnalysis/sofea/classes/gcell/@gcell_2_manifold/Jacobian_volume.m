% Evaluate the volume Jacobian.
%
% function detJ = Jacobian_volume(self, pc, x)
%
%   For the two-dimensional cell, the volume Jacobian is 
%       (i) the product of the surface Jacobian and the thickness;
%       or, when used as axially symmetric
%       (ii) the product of the surface Jacobian and the circumference of
%       the circle through the point pc.
%   Call as:    detJ = Jacobian_volume(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
function detJ = Jacobian_volume(self, pc, x)
    if self.axisymm
        N = bfun(self,pc);
        xyz =N'*x;
        detJ = Jacobian_surface(self, pc, x)*2*pi*xyz(1);
    else
        detJ = Jacobian_surface(self, pc, x)*other_dimension(self, pc, x);
    end
end
