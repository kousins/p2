% Constructor
%
% function retobj = gcell_2_manifold (varargin)
%
% Class represents a two-manifold geometric cell, which encloses some part
% of the computational domain.
% This class is abstract, i.e. every usable 2-D gcell has to be
% derived from it.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell.
% and other_dimension = other dimension (meaning other than the manifold
%                       dimension); default is 1.0.  See details below.
%     axisymm= is the cell going to be used in an axially symmetric
%              situation?
%   Example: gcell_2_manifold(struct ('id',13,'conn',[21 8 4],...
%                  'other_dimension',4.5))  creates a cell
%      that connects nodes 21 3 4, has thickness  of 4.5 units, 
%      and gives it the `name' 13. Note that the other dimension has the
%      meaning of thickness in 3-D or 2-D to produce volume.
%
function retobj = gcell_2_manifold (varargin)
    class_name='gcell_2_manifold';
    if (nargin == 0)
        self.axisymm=false;
        self.other_dimension = 1.0;
        parent=gcell;
        retobj = class(self,class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1};
            self.axisymm=false;
            if isfield(options,'axisymm')
                self.axisymm = options.axisymm;
            end
            self.other_dimension = 1.0;
            if isfield(options,'other_dimension')
                if  self.axisymm
                    error(' Thickness should not be set for axially symmetric gcell');
                else
                    self.other_dimension = options.other_dimension;
                end
            end
            parent=gcell(options);
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
