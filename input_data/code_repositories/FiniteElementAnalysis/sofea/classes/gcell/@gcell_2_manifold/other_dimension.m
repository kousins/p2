% Evaluate the other dimension (thickness) .
%
% function A=other_dimension(self, pc, x)
%
% Evaluate the other dimension (thickness) of the element at given
% parametric coordinates, or at any given spatial coordinate. Arguments in
% the same as for Jacobian.
function A=other_dimension(self, pc, x)
    if (class(self.other_dimension) == 'double')
        A=self.other_dimension;
    else
        N = bfun(self,pc);
        A=feval(self.other_dimension, pc, N' * x);
    end
end
