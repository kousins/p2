% Compute the divergence-displacement matrix for a two-manifold element.
%
% function B = divmat(self, pc, x, Rm)
%
% Compute the linear, displacement independent, divergence-displacement matrix
% for a two-manifold element.
%   Call as:      B = divmat(self, pc, x, Rm)
%   where
%      self=gcell
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
% 
% This code is not entirely correct since it ignores Rm at this point!
%  
% Local data structures:
%      Ndersp=matrix of spatial derivatives of the basis functions
%          with respect to the local coordinate system Rm.
% Output: B = divergence-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of displacement components, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
function B = divmat(self, pc, x, Rm)
    Nder = bfundpar (self, pc);
    Ndersp=bfundsp(self,Nder,x*Rm);
    nfens = size(Ndersp, 1);
    dim=length (Rm(:,1));
    if self.axisymm
        N = bfun(self, pc);
        xyz =N'*x;
        r=xyz(1);
        B = zeros(1,nfens*dim);
        for i= 1:nfens
            B(:,dim*(i-1)+1:dim*i)= [Ndersp(i,1)   Ndersp(i,2) N(i)/r 0 ];
        end
    else
        B = zeros(1,nfens*dim);
        for i= 1:nfens
            B(:,dim*(i-1)+1:dim*i)= [Ndersp(i,1)   Ndersp(i,2) ];
        end
    end
    return;
end

