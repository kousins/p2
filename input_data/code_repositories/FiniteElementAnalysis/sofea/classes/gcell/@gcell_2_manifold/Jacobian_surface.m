% Evaluate the surface Jacobian.
%
% function detJ = Jacobian_surface(self, pc, x)
%
%   Call as:    detJ = Jacobian_surface(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%   Local data structures:
%      tangents = matrix with the tangents to the parametric coordinates in
%           the columns; 2 columns.
function detJ = Jacobian_surface(self, pc, x)
    Nder = bfundpar (self, pc);
    tangents =x'*Nder;
    [sdim, ntan] = size(tangents);
    if     ntan==2 % 2-D gcell
        if sdim==ntan
            detJ = det(tangents);% Compute the Jacobian
        else
            detJ = norm(skewmat(tangents(:,1))*tangents(:,2));
        end
    else
        error('Got an incorrect size of tangents');
    end
end
