% Evaluate the derivatives of the basis functions. 
%
% function Nder = bfundpar (self, param_coords)
%
% Evaluate the derivatives of the basis function matrix for 10-node
% tetrahedron. 
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function Nder = bfundpar (self, param_coords)
    r = param_coords(1);
    s = param_coords(2);
    t = param_coords(3);
    Nder = [...
        -3+4*r+4*s+4*t,  4*r-1,0,  0, -8*r+4-4*s-4*t, 4*s,-4*s, -4*t, 4*t, 0;...
		%
	    -3+4*r+4*s+4*t, 0, 4*s-1, 0,  -4*r,  4*r,4-4*r-8*s-4*t,-4*t, 0,4*t;...  
		%
	    -3+4*r+4*s+4*t, 0, 0, 4*t-1,  -4*r,  0, -4*s,-8*t+4-4*r-4*s, 4*r, 4*s;...
	]';
    return;
end



