% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'bdryconn'}, {'connectivity (node identifiers), array [4,6]'}};
            {{'bdrygcell'}, {'constructor of the boundary gcell, function handle'}}
            };
        val = cat(1, get(self.gcell_3_manifold), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case {'faceconn','bdryconn'}
                conn=get(self.gcell_3_manifold,'conn');
                val = [conn([1, 3, 2, 7, 6, 5]);conn([1, 2, 4, 5, 9, 8]);conn([2, 3, 4, 6, 10, 9]);conn([3, 1, 4, 7, 8, 10])];
            case {'bdrygcell'}
                val = @gcell_T6;
            otherwise
                val = get(self.gcell_3_manifold, prop_name);
        end
    end
end
