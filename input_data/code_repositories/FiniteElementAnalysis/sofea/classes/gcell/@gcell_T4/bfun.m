% Evaluate the basis function matrix for four-node tetrahedron.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
val = [(1 - param_coords(1) - param_coords(2) - param_coords(3));...
    param_coords(1); ...
    param_coords(2); ...
    param_coords(3)];
return;


