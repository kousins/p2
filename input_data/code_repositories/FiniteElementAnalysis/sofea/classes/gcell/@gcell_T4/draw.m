% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    geom=geometry field
% with optional fields
% facecolor = color of the faces, solid
% colorfield =field with vertex colors
% only one of the facecolor and colorfield may be supplied
%  shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    f =[1, 2, 3; 2, 4, 3; 3, 4, 1; 4, 2, 1];
    ef =[f,  f(:, 1)];
    xu=x+u;
    if isfield(context,'shrink')
        c=sum(xu,1)/4;
        xu=(1-context.shrink)*ones(4,1)*c+context.shrink*xu;
    end
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        edgecolor = 'Black';
        if (isfield(context,'edgecolor'))
            edgecolor = context.edgecolor;
        end
        options=struct ('facecolor',facecolor,'edgecolor',edgecolor);
    end
    edgecolor='black';
    if (isfield(context,'edgecolor'))
        edgecolor =context.edgecolor;
    end
    options.edgecolor =edgecolor;
    if ~strcmp(edgecolor,'none')
        for i=1:4
            draw_polyline (gv, xu, ef(i,:), options);
        end
    end
    if (isfield(context,'alpha'))
        options.alpha = context.alpha;
    end
    options.edgecolor ='none';
    for i=1:4
        draw_polygon (gv, xu, f(i,:), options);
    end
    return;
end
