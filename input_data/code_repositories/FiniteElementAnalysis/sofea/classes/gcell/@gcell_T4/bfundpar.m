% Evaluate the derivatives of the basis functions.
%
% function val = bfundpar (self, param_coords)
%
% Evaluate the derivatives of the basis function matrix for four-node
% tetrahedron.
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfundpar (self, param_coords)
    val = [-1 -1 -1; ...
        +1  0  0; ...
        0 +1  0; ...
        0  0 +1];
    return;
end



