% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    geom=geometry field
% with optional fields
% facecolor = color of the faces, solid
% colorfield =field with vertex colors
% only one of the facecolor and colorfield may be supplied
%  shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    f=[1,4,6;2,5,4;3,6,5;4,5,6];
    ef=[1,4,2,5,3,6,1];
    xu=x+u;
    if isfield(context,'shrink') & context.shrink~=1
        pc= [0,0;1,0;0,1;0.5,0;0.5,0.5;0,0.5]; 
        n=length(conn);
        c=ones(n,1)*(sum(pc)/n);
        pc=c+context.shrink*(pc-c);
        xus=xu;
        for j=1:n
            N=bfun(self,pc(j,:));
            xus(j,:)=N'*xu;
        end
        xu=xus;
    end
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        edgecolor = 'Black';
        if (isfield(context,'edgecolor'))
            edgecolor = context.edgecolor;
        end
        options=struct ('facecolor',facecolor,'edgecolor',edgecolor);
    end
    edgecolor='black';
    if (isfield(context,'edgecolor'))
        edgecolor =context.edgecolor;
    end
    options.edgecolor =edgecolor;
    if ~strcmp(edgecolor,'none')
        draw_polyline (gv, xu, ef, options);
    end
    if (isfield(context,'alpha'))
        options.alpha = context.alpha;
    end
    options.edgecolor ='none';
    for i=1:size(f,1)
        draw_polygon (gv, xu, f(i,:), options);
    end
    return;
end
