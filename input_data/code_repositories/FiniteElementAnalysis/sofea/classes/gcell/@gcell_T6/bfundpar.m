% Evaluate the derivatives of the basis function matrix.
%
% function val = bfundpar (self, param_coords)
%
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfundpar (self, param_coords)
    r=param_coords(1);
    s=param_coords(2);
    t = 1 - r - s;
    val = [ ...
        -3+4*r+4*s, -3+4*r+4*s;
        4*r-1, 0;
        0, 4*s-1;
        4-8*r-4*s, -4*r;
        4*s, 4*r;
        -4*s, 4-4*r-8*s];
    return;
end



