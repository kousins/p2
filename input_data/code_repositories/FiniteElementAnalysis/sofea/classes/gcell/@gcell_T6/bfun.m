% Evaluate the basis function matrix for an 6-node triangle.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
    r=param_coords(1);
    s=param_coords(2);
    t = 1 - r - s;
    val = [...
        t * (t + t - 1);
        r * (r + r - 1);
        s * (s + s - 1);
        4 * r * t;
        4 * r * s;
        4 * s * t];
    return;
end

