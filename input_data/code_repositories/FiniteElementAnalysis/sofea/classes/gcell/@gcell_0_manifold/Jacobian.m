% Evaluate the manifold Jacobian.
%
% function detJ = Jacobian(self, pc, x)
%
% 
%   For and m-dimensional cell, the manifold Jacobian is
%       m=0: +1
%       m=1: Jacobian_curve
%       m=2: Jacobian_surface
%       m=3: Jacobian_volume
%
%   Call as:
%      detJ = Jacobian(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
function detJ = Jacobian(self, pc, x)
    detJ = 1;
end
