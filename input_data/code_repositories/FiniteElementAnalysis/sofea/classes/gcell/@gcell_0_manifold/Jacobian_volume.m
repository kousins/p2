% Evaluate the volume Jacobian.
%
% function detJ = Jacobian_volume(self, pc, x)
%
%
%   For the zero-dimensional cell, the volume Jacobian is the product of the
%    0-D Jacobian and the other dimension.
%   Call as:
%      detJ = Jacobian_volume(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
function detJ = Jacobian_volume(self, pc, x)
    detJ= 1*other_dimension(self, pc, xyz);
end
