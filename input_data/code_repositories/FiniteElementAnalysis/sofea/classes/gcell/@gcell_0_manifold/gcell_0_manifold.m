% Constructor
%
% function retobj = gcell_0_manifold (varargin)
%
% Class represents a zero-dimensional manifold geometric cell  (a point
% in space).
% This class is abstract, i.e. every usable 0-D gcell has to be
% derived from it.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell.
% and other_dimension = other dimension (meaning other than the manifold
%                       dimension); default is 1.0.  See details below.
%   Example: gcell_0_manifold(struct ('id',13,'conn',[21],...
%                'other_dimension',4.5))  creates a cell
%      that is attached to the node 21, has volume of 4.5 units,
%      and gives it the `name' 13. Note that the other dimension may have a
%      different meaning depending on the application: it may mean volume
%      in 3-D or 2-D to produce volume, or thickness in 2-D to produce
%      length, or area in 1-D to produce an area.
%
function retobj = gcell_0_manifold (varargin)
    class_name='gcell_0_manifold';
    if (nargin == 0)
        self.other_dimension= 1.0;
        self.axisymm=false;
        parent=gcell;
        retobj = class(self,class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1};
            self.other_dimension = 1.0;
            self.axisymm=false;
            if isfield(options,'other_dimension')
                self.other_dimension = options.other_dimension;
            end
            self.axisymm=false;
            if isfield(options,'axisymm')
                self.axisymm = options.axisymm;
            end
            parent=gcell(options);
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
