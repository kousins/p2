% Compute the strain-displacement matrix for a zero-manifold element.
%
% function B = Blmat(self, pc, x, Rm)
%
% Compute the linear, displacement independent, strain-displacement matrix
% for a zero-manifold element.
%   Call as:
%      B = Blmat(self, pc, x, Rm)
function B = Blmat(self, pc, x, Rm)
    error('Zero-manifold element does not have a strain displacement matrix')
end
