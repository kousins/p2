% Evaluate the curve Jacobian.
%
% function detJ = Jacobian_curve(self, pc, x)
%
%
%   For the zero-dimensional cell, the curve Jacobian is
%       (i) the product of the 0-D Jacobian and the other dimension;
%       or, when used as axially symmetric
%       (ii) the product of the 0-D Jacobian and the circumference of
%       the circle through the point pc.
%   Call as:
%      detJ = Jacobian_curve(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
function detJ = Jacobian_curve(self, pc, x)
    if self.axisymm
        N = bfun(self,pc);
        xyz =N'*x;
        detJ = 1*2*pi*xyz(1);
    else
        detJ = 1*other_dimension(self, pc, xyz);
    end
end
