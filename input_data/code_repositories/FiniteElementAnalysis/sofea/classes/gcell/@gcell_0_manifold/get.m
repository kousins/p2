% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val={{{'dim'}, {'manifold dimension (0 = point)'}};...
            {{'other_dimension'}, {'other dimension (length, area, volume)'}};...
            {{'axisymm'}, {'axially symmetric?'}};
            };
        val = cat(1, get(self.gcell), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'dim'
                val = 0;
            case 'other_dimension'
                val = self.other_dimension;
            case 'axisymm'
                val = self.axisymm;
            otherwise
                val = get(self.gcell, prop_name);
        end
    end
end
