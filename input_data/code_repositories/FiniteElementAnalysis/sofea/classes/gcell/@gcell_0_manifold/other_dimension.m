% Compute the other dimension (Length, surface, volume).
%
% function A=other_dimension(self, pc, x)
%
% Compute the other dimension (Length, surface, volume).
function A=other_dimension(self, pc, x)
    A=self.other_dimension;
end
