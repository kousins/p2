% Evaluate the surface Jacobian.
%
% function detJ = Jacobian_surface(self, pc, x)
%
%   For the one-dimensional cell, the surface Jacobian is 
%       (i) the product of the curve Jacobian and the thickness;
%       or, when used as axially symmetric
%       (ii) the product of the curve Jacobian and the circumference of
%       the circle through the point pc.
%   Call as:    detJ = Jacobian_surface(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
function detJ = Jacobian_surface(self, pc, x)
    if self.axisymm
        N = bfun(self,pc);
        xyz =N'*x;
        detJ = Jacobian_curve(self, pc, x)*2*pi*xyz(1);
    else
        detJ = Jacobian_curve(self, pc, x)*other_dimension(self, pc, x);
    end
end
