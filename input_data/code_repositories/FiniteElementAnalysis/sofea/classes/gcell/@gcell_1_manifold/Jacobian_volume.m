% Evaluate the volume Jacobian.
%
% function detJ = Jacobian_volume(self, pc, x)
%
%
%   For the one-dimensional cell, the volume Jacobian is either
%       (i) the product of the curve Jacobian and the cross-sectional area;
%       or, when used as axially symmetric
%       (ii) the product of the surface Jacobian and the thickness.
%   Call as:
%      detJ = Jacobian_volume(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
function detJ = Jacobian_volume(self, pc, x)
    if self.axisymm
        detJ = Jacobian_surface(self, pc, x)*other_dimension(self, pc, x);
    else
        detJ = Jacobian_curve(self, pc, x)*other_dimension(self, pc, x);
    end
end
