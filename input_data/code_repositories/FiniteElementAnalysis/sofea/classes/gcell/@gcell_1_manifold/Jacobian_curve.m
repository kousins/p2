% Evaluate the curve Jacobian.
%
% function detJ = Jacobian_curve(self, pc, x)
%
%   Call as:    detJ = Jacobian_curve(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%   Local data structures:
%      tangents = matrix with the tangents to the parametric coordinates in
%           the columns; one column.
function detJ = Jacobian_curve(self, pc, x)
    Nder = bfundpar (self, pc);
    tangents =x'*Nder;
    [sdim, ntan] = size(tangents);
    if     ntan==1 % 1-D gcell
        detJ = norm(tangents);
    else
        error('Got an incorrect size of tangents');
    end
end
