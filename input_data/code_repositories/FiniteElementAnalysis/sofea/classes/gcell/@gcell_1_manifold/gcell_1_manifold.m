% Constructor
%
% function retobj = gcell_1_manifold (varargin)
%
% Class represents a one-dimensional manifold geometric cell, which encloses some part
% of the computational domain.
% This class is abstract, i.e. every usable 1-D gcell has to be
% derived from it.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell.
% and other_dimension = other dimension (meaning other than the manifold
%                       dimension); default is 1.0.  See details below.
%     axisymm= is the cell going to be used in an axially symmetric
%              situation?
%   Example: gcell_1_manifold(struct ('id',13,'conn',[21 3],...
%                  'other_dimension',4.5))  creates a cell
%      that connects nodes 21 3, has cross-sectional area of 4.5 units, 
%      and gives it the `name' 13. Note that the other dimension may have a
%      different meaning depending on the application: it may mean area
%      in 3-D or 2-D to produce volume, or thickness in 2-D to produce
%      area.
%
function retobj = gcell_1_manifold (varargin)
    class_name='gcell_1_manifold';
    if (nargin == 0)
        self.other_dimension= 1.0;
        self.axisymm=false;
        parent=gcell;
        retobj = class(self,class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1};
            self.other_dimension = 1.0;
            self.axisymm=false;
            if isfield(options,'other_dimension')
                self.other_dimension = options.other_dimension;
            end
            self.axisymm=false;
            if isfield(options,'axisymm')
                self.axisymm = options.axisymm;
            end
            parent=gcell(options);
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
