% Compute the strain-displacement matrix for a one-manifold element.
%
% function B = Blmat(self, pc, x, Rm)
%
% Compute the linear, displacement independent, strain-displacement matrix
% for a one-manifold element.
%   Call as:
%      B = Blmat(self, pc, x, Rm)
%   where
%      self=gcell
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
%
% Local data structures:
%      Ndersp=matrix of spatial derivatives of the basis functions
%          with respect to the local coordinate system Rm.
% Output:
% B = strain-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of strains, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
%
function B = Blmat(self, pc, x, Rm)
    Nder = bfundpar (self, pc);
    Ndersp=bfundsp(self,Nder,x*Rm);
    nfens = size(Ndersp, 1);
    dim=length (Rm(:,1));
    B = zeros(1,nfens*dim);
    for i= 1:nfens
        B(:,dim*(i-1)+1:dim*i)=  Ndersp(i,1) *Rm(:,1)';
    end
    return;
end
