% Evaluate the derivatives of the basis function matrix.
%
% function Nder = bfundpar (self, param_coords)
%
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=dim.
%
function Nder = bfundpar (self, param_coords)
    xi=param_coords(1);
    Nder = [(xi-1/2); (xi+1/2); -2*xi];
    return;
end



