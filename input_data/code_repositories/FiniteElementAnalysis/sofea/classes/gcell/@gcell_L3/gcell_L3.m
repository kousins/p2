% Constructor a three-node (quadratic) curve geometric cell.
%
% function retobj = gcell_L3 (varargin)
%
% Class represents a three-node (quadratic) curve geometric cell.
% 
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell, gcell_1_manifold.
%
function retobj = gcell_L3 (varargin)
    class_name='gcell_L3';
    nfens=3;
    if (nargin == 0)
        options.nfens=nfens;
        parent=gcell_1_manifold(options);
        retobj = class(struct([]),class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1}; options.nfens=nfens;
            parent=gcell_1_manifold(options);
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
