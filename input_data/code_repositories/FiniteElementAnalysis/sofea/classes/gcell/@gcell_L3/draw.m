% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    xu=x+u;
    shrink = 1.0;
    if isfield(context,'shrink')
      shrink =context.shrink;
    end
    if (shrink~= 1.0)
        xyz1 = map_to_xyz(self,-shrink,xu);
        xyz2 = map_to_xyz(self,+shrink,xu);
        xu(1,:)=xyz1;
        xu(2,:)=xyz2;
    end
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        options=struct ('facecolor',facecolor);
    end
    A1=other_dimension (self, -shrink);
    A2=other_dimension (self, +shrink);
    A3=other_dimension (self, 0);
    draw_cylinder(gv, xu(1,:),xu(3,:),sqrt(A1/pi),sqrt(A3/pi), context);
    draw_cylinder(gv, xu(3,:),xu(2,:),sqrt(A3/pi),sqrt(A2/pi), context);
    return;
end
