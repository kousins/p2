% Evaluate the basis function matrix for an 3-node line element (bar).
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
    xi=param_coords(1);
    val = [(xi-1)*xi/2;  (xi+1)*xi/2;  -(xi+1)*(xi-1)];
    return;
end

