% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'bdryconn'}, {'connectivity (node identifiers), array [nfens,2]'}};
            {{'bdrygcell'}, {'constructor of the boundary gcell, function handle'}}
            };
        val = cat(1, get(self.gcell_1_manifold), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case {'bdryconn'}
                conn=get(self.gcell_1_manifold,'conn');
                val = conn(1:2)';
            case {'bdrygcell'}
                val = @gcell_P1;
            otherwise
                val = get(self.gcell_1_manifold, prop_name);
        end
    end
end
