% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'bdryconn'}, {'connectivity (node identifiers), array [6,4]'}};
            {{'bdrygcell'}, {'constructor of the boundary gcell, function handle'}}
            };
        val = cat(1, get(self.gcell_3_manifold), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case {'faceconn','bdryconn'}
                conn=get(self.gcell_3_manifold,'conn');
                val = [conn([1, 2, 3, 4,9,10,11,12]);
                    conn([1, 5, 6, 2,17,13,18,9]);
                    conn([2, 6, 7, 3,18,14,19,10]);
                    conn([3, 7, 8, 4,19,15,20,11]);
                    conn([4, 8, 5, 1,20,16,17,12]);
                    conn([6, 5, 8, 7,13,16,15,14])];
            case {'bdrygcell'}
                val = @gcell_Q8;
            otherwise
                val = get(self.gcell_3_manifold, prop_name);
        end
    end

