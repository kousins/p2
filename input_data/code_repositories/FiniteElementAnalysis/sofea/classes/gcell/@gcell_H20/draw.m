% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    % these are all defined in clockwise order, which is what Matlab expects
    %     f=[1,9,2,10,3,11,4,12,1;
    %         5,16,8,15,7,14,6,13,5;
    %         1,17,5,13,6,18,2,9,1;
    %         2,18,6,14,7,19,3,10,2;
    %         3,19,7,15,8,20,4,11,3;
    %         4,20,8,16,5,17,1,12,4];
    f=[1,9,12,12;9,2,10,10;10,3,11,11;11,4,12,12;9,10,11,12;
        5,16,13,13;16,8,15,15;15,7,14,14;14,6,13,13;16,15,14,13;
        1,17,9,9;17,5,13,13;13,6,18,18;18,2,9,9;17,13,18,9;
        2,18,10,10;18,6,14,14;14,7,19,19;19,3,10,10;18,14,19,10;
        3,19,4,4;19,7,15,15;15,8,20,20;20,4,11,11;19,15,20,11;
        4,20,12,12;20,8,16,16;16,5,17,17;17,1,12,12;20,16,17,12];
    ef=[1,9,2,10,3,11,4,12,1;
        5,16,8,15,7,14,6,13,5;
        1,17,5,13,6,18,2,9,1;
        2,18,6,14,7,19,3,10,2;
        3,19,7,15,8,20,4,11,3;
        4,20,8,16,5,17,1,12,4];
    % normally, we define faces with counterclockwise traversal of the vertices
    %     f =[    1     4     3     2    12    11    10     9
    %      1     2     6     5     9    18    13    17
    %      2     3     7     6    10    19    14    18
    %      3     4     8     7    11    20    15    19
    %      4     1     5     8    12    17    16    20
    %      5     6     7     8    13    14    15    16];
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        options=struct ('facecolor',facecolor);
    end
    xu=x+u;
    if isfield(context,'shrink') & context.shrink~=1
        pc= [-1,-1,-1;+1,-1,-1;+1,+1,-1;-1,+1,-1;...
            -1,-1,+1;+1,-1,+1;+1,+1,+1;-1,+1,+1;...
            0,-1,-1;1,0,-1;0,1,-1;-1,0,-1;...
            0,-1,1;1,0,1;0,1,1;-1,0,1;...
            -1,-1,0;+1,-1,0;+1,+1,0;-1,+1,0];
        n=length(conn);
        c=ones(n,1)*(sum(pc)/n);
        pc=c+context.shrink*(pc-c);
        xus=xu;
        for j=1:n
            N=bfun(self,pc(j,:));
            xus(j,:)=N'*xu;
        end
        xu=xus;
    end
    edgecolor='black';
    if (isfield(context,'edgecolor'))
        edgecolor =context.edgecolor;
    end
    options.edgecolor =edgecolor;
    if ~strcmp(edgecolor,'none')
        for i=1:6
            draw_polyline (gv, xu, ef(i,:), options);
        end
    end
    options.edgecolor ='none';
    for i=1:size(f,1)
        draw_polygon (gv, xu, f(i,:), options);
    end
    return;
end
