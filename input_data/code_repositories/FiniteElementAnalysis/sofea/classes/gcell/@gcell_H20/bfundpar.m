% Evaluate the derivatives of the basis function matrix.
%
% function Nder = bfundpar (self, param_coords)
%
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function Nder = bfundpar (self, param_coords)
    xim    = -(-1 + param_coords(1));
    etam   = -(-1 + param_coords(2));
    zetam  = -(-1 + param_coords(3));
    xip    = (1 + param_coords(1));
    etap   = (1 + param_coords(2));
    zetap  = (1 + param_coords(3));
    twoppp =(2+param_coords(1)+param_coords(2)+param_coords(3));
    twompp =(2-param_coords(1)+param_coords(2)+param_coords(3));
    twopmp =(2+param_coords(1)-param_coords(2)+param_coords(3));
    twoppm =(2+param_coords(1)+param_coords(2)-param_coords(3));
    twommp =(2-param_coords(1)-param_coords(2)+param_coords(3));
    twopmm =(2+param_coords(1)-param_coords(2)-param_coords(3));
    twompm =(2-param_coords(1)+param_coords(2)-param_coords(3));
    twommm =(2-param_coords(1)-param_coords(2)-param_coords(3));
    n_xi= [1.0/8*etam*zetam*twoppp-1.0/8*xim*etam*zetam;
        -1.0/8*etam*zetam*twompp+1.0/8*xip*etam*zetam;
        -1.0/8*etap*zetam*twommp+1.0/8*xip*etap*zetam;
        1.0/8*etap*zetam*twopmp-1.0/8*xim*etap*zetam;
        1.0/8*etam*zetap*twoppm-1.0/8*xim*etam*zetap;
        -1.0/8*etam*zetap*twompm+1.0/8*xip*etam*zetap;
        -1.0/8*etap*zetap*twommm+1.0/8*xip*etap*zetap;
        1.0/8*etap*zetap*twopmm-1.0/8*xim*etap*zetap;
        -1.0/4*xip*etam*zetam+1.0/4*xim*etam*zetam;
        1.0/4*etam*etap*zetam;
        -1.0/4*xip*etap*zetam+1.0/4*xim*etap*zetam;
        -1.0/4*etam*etap*zetam;
        -1.0/4*xip*etam*zetap+1.0/4*xim*etam*zetap;
        1.0/4*etam*etap*zetap;
        -1.0/4*xip*etap*zetap+1.0/4*xim*etap*zetap;
        -1.0/4*etam*etap*zetap;
        -1.0/4*zetam*zetap*etam;
        1.0/4*zetam*zetap*etam;
        1.0/4*zetam*zetap*etap;
        -1.0/4*zetam*zetap*etap];
    n_eta = [1.0/8*xim*zetam*twoppp-1.0/8*xim*etam*zetam;
        1.0/8*xip*zetam*twompp-1.0/8*xip*etam*zetam;
        -1.0/8*xip*zetam*twommp+1.0/8*xip*etap*zetam;
        -1.0/8*xim*zetam*twopmp+1.0/8*xim*etap*zetam;
        1.0/8*xim*zetap*twoppm-1.0/8*xim*etam*zetap;
        1.0/8*xip*zetap*twompm-1.0/8*xip*etam*zetap;
        -1.0/8*xip*zetap*twommm+1.0/8*xip*etap*zetap;
        -1.0/8*xim*zetap*twopmm+1.0/8*xim*etap*zetap;
        -1.0/4*xim*xip*zetam;
        -1.0/4*xip*etap*zetam+1.0/4*xip*etam*zetam;
        1.0/4*xim*xip*zetam;
        -1.0/4*xim*etap*zetam+1.0/4*xim*etam*zetam;
        -1.0/4*xim*xip*zetap;
        -1.0/4*xip*etap*zetap+1.0/4*xip*etam*zetap;
        1.0/4*xim*xip*zetap;
        -1.0/4*xim*etap*zetap+1.0/4*xim*etam*zetap;
        -1.0/4*zetam*zetap*xim;
        -1.0/4*zetam*zetap*xip;
        1.0/4*zetam*zetap*xip;
        1.0/4*zetam*zetap*xim];
    n_zeta= [1.0/8*xim*etam*twoppp-1.0/8*xim*etam*zetam;
        1.0/8*xip*etam*twompp-1.0/8*xip*etam*zetam;
        1.0/8*xip*etap*twommp-1.0/8*xip*etap*zetam;
        1.0/8*xim*etap*twopmp-1.0/8*xim*etap*zetam;
        -1.0/8*xim*etam*twoppm+1.0/8*xim*etam*zetap;
        -1.0/8*xip*etam*twompm+1.0/8*xip*etam*zetap;
        -1.0/8*xip*etap*twommm+1.0/8*xip*etap*zetap;
        -1.0/8*xim*etap*twopmm+1.0/8*xim*etap*zetap;
        -1.0/4*xim*xip*etam;
        -1.0/4*etam*etap*xip;
        -1.0/4*xim*xip*etap;
        -1.0/4*etam*etap*xim;
        1.0/4*xim*xip*etam;
        1.0/4*etam*etap*xip;
        1.0/4*xim*xip*etap;
        1.0/4*etam*etap*xim;
        -1.0/4*xim*etam*zetap+1.0/4*xim*etam*zetam;
        -1.0/4*xip*etam*zetap+1.0/4*xip*etam*zetam;
        -1.0/4*xip*etap*zetap+1.0/4*xip*etap*zetam;
        -1.0/4*xim*etap*zetap+1.0/4*xim*etap*zetam];
    Nder = [n_xi, n_eta, n_zeta];
    return;
end



