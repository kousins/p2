% Evaluate the basis function matrix for an 8-node brick.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
    xim    = (-1 + param_coords(1));
    etam   = (-1 + param_coords(2));
    zetam  = (-1 + param_coords(3));
    xip    = (1 + param_coords(1));
    etap   = (1 + param_coords(2));
    zetap  = (1 + param_coords(3));
    val = [ 1.0/8*xim*etam*zetam*(2+param_coords(1)+param_coords(2)+param_coords(3));
        -1.0/8*xip*etam*zetam*(2-param_coords(1)+param_coords(2)+param_coords(3));
        1.0/8*xip*etap*zetam*(2-param_coords(1)-param_coords(2)+param_coords(3));
        -1.0/8*xim*etap*zetam*(2+param_coords(1)-param_coords(2)+param_coords(3));
        1.0/8*xim*etam*zetap*(-2-param_coords(1)-param_coords(2)+param_coords(3));
        -1.0/8*xip*etam*zetap*(-2+param_coords(1)-param_coords(2)+param_coords(3));
        1.0/8*xip*etap*zetap*(-2+param_coords(1)+param_coords(2)+param_coords(3));
        -1.0/8*xim*etap*zetap*(-2-param_coords(1)+param_coords(2)+param_coords(3));
        -1.0/4*xim*xip*etam*zetam;
        1.0/4*etam*etap*xip*zetam;
        1.0/4*xim*xip*etap*zetam;
        -1.0/4*etam*etap*xim*zetam;
        1.0/4*xim*xip*etam*zetap;
        -1.0/4*etam*etap*xip*zetap;
        -1.0/4*xim*xip*etap*zetap;
        1.0/4*etam*etap*xim*zetap;
        -1.0/4*zetam*zetap*xim*etam;
        1.0/4*zetam*zetap*xip*etam;
        -1.0/4*zetam*zetap*xip*etap;
        1.0/4*zetam*zetap*xim*etap];
    return;
end

