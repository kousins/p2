% Get property from the specified object and return the value.
%
% function val = get(self,varargin) 
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'bdryconn'}, {'connectivity (node identifiers), array [6,4]'}};
            {{'bdrygcell'}, {'constructor of the boundary gcell, function handle'}}
            };
        val = cat(1, get(self.gcell_3_manifold), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case {'faceconn','bdryconn'}
                conn=get(self.gcell_3_manifold,'conn');
                val = [conn([1, 4, 3, 2]);
                    conn([1, 2, 6, 5]);
                    conn([2, 3, 7, 6]);
                    conn([3, 4, 8, 7]);
                    conn([4, 1, 5, 8]);
                    conn([6, 7, 8, 5])];
            case {'bdrygcell'}
                val = @gcell_Q4;
            otherwise
                val = get(self.gcell_3_manifold, prop_name);
        end
    end

