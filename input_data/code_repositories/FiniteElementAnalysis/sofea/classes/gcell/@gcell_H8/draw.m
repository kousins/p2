% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    % these are all defined in clockwise order, which is what Matlab expects
    f =[ 4     1     2     3
        2     1     5     6
        3     2     6     7
        4     3     7     8
        1     4     8     5
        6     5     8     7];
    % normally, we define faces with counterclockwise traversal of the vertices
    % f=[1     4     3     2;...
    %     1     2     6     5;...
    %     2     3     7     6;...
    %     3     4     8     7;...
    %     4     1     5     8;...
    %     5     6     7     8];
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        edgecolor = 'Black';
        if (isfield(context,'edgecolor'))
            edgecolor = context.edgecolor;
        end
        options=struct ('facecolor',facecolor,'edgecolor',edgecolor);
    end
    xu=x+u;
    if isfield(context,'shrink')
        c=sum(xu,1)/8;
        xu=(1-context.shrink)*ones(8,1)*c+context.shrink*xu;
    end
    % c=f(:,1);f(:,1)=f(:,2);f(:,2)=c;
    % c=f(:,3);f(:,3)=f(:,4);f(:,4)=c;
    edgecolor='black';
    if (isfield(context,'edgecolor'))
        edgecolor =context.edgecolor;
    end
    options.edgecolor =edgecolor;
    if ~strcmp(edgecolor,'none')
        for i=1:6
            draw_polyline (gv, xu, f(i,:), options);
        end
    end
    if (isfield(context,'alpha'))
        options.alpha = context.alpha;
    end
    options.edgecolor ='none';
    for i=1:6
        draw_polygon (gv, xu, f(i,:), options);
    end
    return;
end
