% Evaluate the basis function matrix for an 8-node brick.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
  one_minus_xi    = (1 - param_coords(1));
  one_minus_eta   = (1 - param_coords(2));
  one_minus_theta = (1 - param_coords(3));
  one_plus_xi     = (1 + param_coords(1));
  one_plus_eta    = (1 + param_coords(2));
  one_plus_theta  = (1 + param_coords(3));
  val = [one_minus_xi*one_minus_eta*one_minus_theta;...
         one_plus_xi*one_minus_eta*one_minus_theta;...
         one_plus_xi*one_plus_eta*one_minus_theta;...
         one_minus_xi*one_plus_eta*one_minus_theta;...
         one_minus_xi*one_minus_eta*one_plus_theta;...
         one_plus_xi*one_minus_eta*one_plus_theta;...
         one_plus_xi*one_plus_eta*one_plus_theta;...
         one_minus_xi*one_plus_eta*one_plus_theta] / 8;
  return;
end

