% Evaluate the volume Jacobian. Enhanced-efficiency version that takes
% advantage of available matrix of basis functions derivatives instead of
% computing them itself.
%
% function detJ = Jacobian_volume_eff(self, pc, x, Nder)
%
%   Call as:
%      detJ = Jacobian_volume_eff(self, pc, x, Nder)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      Nder = array of derivatives of the basis functions with respect to the parametric coordinates 
%
%   Local data structures:
%      tangents = matrix with the tangents to the parametric coordinates in
%           the columns; 3 columns.
%
function detJ = Jacobian_volume_eff(self, pc, x, Nder)
%     Nder = bfundpar (self, pc);
    tangents =x'*Nder;
    [sdim, ntan] = size(tangents);
    if     ntan==3
        detJ = det(tangents);% Compute the Jacobian
    else
        error('Got an incorrect size of tangents');
    end
end
