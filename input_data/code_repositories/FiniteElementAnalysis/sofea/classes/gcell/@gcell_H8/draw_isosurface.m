function   draw_isosurface(self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    xu=x+u;
    if (isfield(context,'scalarfield'))
        scalars =gather(context.scalarfield, conn, 'values', 'noreshape'); % coordinates of nodes
    else
        return
    end
    if (isfield(context,'isovalue'))
        isovalue =context.isovalue;
    else
        isovalue =0;
    end
    if ((min(scalars)>isovalue) ||  (max(scalars)<isovalue))
        return
    end
    if (isfield(context,'color'))
        color =context.color;
    else
        color =[1,1,1]/2;
    end
    
    xis =linspace(-1,+1,3);
    [X,Y,Z] = ndgrid(xis,xis,xis);
    V=0*Z;
    for k=1:length(xis)
        for j=1:length(xis)
            for i=1:length(xis)
                N = bfun(self, [xis(i),xis(j),xis(k)]);
                V(i,j,k)=N'*scalars;
            end
        end
    end
    [f,v] = isosurface(X,Y,Z,V,isovalue);
    for i=1:size(v,1)
        N = bfun(self, v(i,:));
        v(i,:)=N'*xu;
    end
    patch('Faces',f,'Vertices',v,'FaceColor',color,'EdgeColor','none');
end

% [fens,gcells] = blockx( [0, 1],[0, 1],[0, 1]);
% [fens,gcells] = H8_to_H27(fens,gcells);
% n=2;
% xis =linspace(-1,+1,n+1);
% idx=1;
% for k=1:length(xis)
%     for j=1:length(xis)
%         for i=1:length(xis)
%             N = bfun(gcells(1), [xis(i),xis(j),xis(k)]);
%             Ns(:,idx)=N; idx=idx+1;
%         end
%     end
% end
