% Constructor of represents a three-node triangle geometric cell.
%
% function retobj = gcell_T3 (varargin)
%
% 
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell, gcell_2_manifold.
%
function retobj = gcell_T3 (varargin)
    class_name='gcell_T3';
    nfens=3;
    if (nargin == 0)
        options.nfens=nfens;
        parent=gcell_2_manifold(options);
        retobj = class(struct([]),class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1}; options.nfens=nfens;
            parent=gcell_2_manifold(options);
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
