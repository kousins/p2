% Evaluate the basis function matrix for an 3-node triangle.
%
% function val = bfun(self,param_coords)
%
%   Call as:    N = bfun (g, pc)
%      g=gcell whose basis functions are to be evaluated,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
function val = bfun(self,param_coords)
    val = [(1 - param_coords(1) - param_coords(2));...
        param_coords(1); ...
        param_coords(2)];
    return;
end

