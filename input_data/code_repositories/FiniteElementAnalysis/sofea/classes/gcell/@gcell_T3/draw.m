% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    geom=geometry field
% with optional fields
% facecolor = color of the faces, solid
% colorfield =field with vertex colors
% only one of the facecolor and colorfield may be supplied
%  shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    f=[1 2 3];
    xu=x+u;
    if isfield(context,'shrink')
        c=sum(xu,1)/3;
        xu=(1-context.shrink)*ones(3,1)*c+context.shrink*xu;
    end
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        options=struct ('facecolor',facecolor);
    end
    if (isfield(context,'edgecolor'))
        options.edgecolor =context.edgecolor;
    end
    draw_polygon (gv, xu, f, options);
    return;
end
