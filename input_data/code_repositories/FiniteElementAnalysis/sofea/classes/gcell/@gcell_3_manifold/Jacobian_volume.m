% Evaluate the volume Jacobian.
%
% function detJ = Jacobian_volume(self, pc, x)
%
%   Call as:
%      detJ = Jacobian_volume(self, pc, x)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
%   Local data structures:
%      tangents = matrix with the tangents to the parametric coordinates in
%           the columns; 3 columns.
%
function detJ = Jacobian_volume(self, pc, x)
    Nder = bfundpar (self, pc);
    tangents =x'*Nder;
    [sdim, ntan] = size(tangents);
    if     ntan==3
        detJ = det(tangents);% Compute the Jacobian
    else
        error('Got an incorrect size of tangents');
    end
end
