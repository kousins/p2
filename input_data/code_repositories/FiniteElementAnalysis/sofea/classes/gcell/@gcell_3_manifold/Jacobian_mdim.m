% Evaluate the manifold Jacobian.
%
% function detJ = Jacobian_mdim(self, pc, x, m)
%
%
%   For and m-dimensional cell, the manifold Jacobian is
%       m=0: +1
%       m=1: Jacobian_curve
%       m=2: Jacobian_surface
%       m=3: Jacobian_volume
%
%   Call as:
%      detJ = Jacobian_mdim(self, pc, x, m)
%   where
%      self=gcell,
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      m= manifold dimension
%
function detJ = Jacobian_mdim(self, pc, x, m)
    switch (m)
        case 3
            detJ = Jacobian_volume(self, pc, x);
        otherwise
            error('Wrong dimension');
    end
end
