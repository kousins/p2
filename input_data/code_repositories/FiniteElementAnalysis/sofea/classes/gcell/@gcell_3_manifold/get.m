% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'dim'}, {'topological dimension (3=solid)'}};
            };
        val = cat(1, get(self.gcell), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'dim'
                val = 3; 
            otherwise
                val = get(self.gcell, prop_name);
        end
    end
end
