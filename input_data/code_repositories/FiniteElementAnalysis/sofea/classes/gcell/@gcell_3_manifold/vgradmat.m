% Compute the vector gradient-displacement matrix for a 3-manifold element.
%
% function B = vgradmat(self, pc, x, Rm)
%
% Compute the linear, displacement independent, divergence-displacement matrix
% for a three-manifold element.
%   Call as:      B = vgradmat(self, pc, x, Rm)
%   where
%      self=gcell
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
% 
% This code is not entirely correct since it ignores Rm at this point!
%  
% Local data structures:
%      Ndersp=matrix of spatial derivatives of the basis functions
%          with respect to the local coordinate system Rm.
% Output: B = divergence-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of displacement components, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
function B = vgradmat(self, pc, x, Rm)
    Nder = bfundpar (self, pc);
    Ndersp=bfundsp(self,Nder,x*Rm);
    nfens = size(Ndersp, 1);
    dim=3;
    B = zeros(dim*dim,nfens*dim);
    for i= 1:dim
        B(dim*(i-1)+1:dim*i,i:dim:nfens*dim-dim+i)=Ndersp'; 
    end
    return;
end
