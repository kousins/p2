% Compute the divergence-displacement matrix for a three-manifold element.
%
% function B = divmat(self, pc, x, Rm)
%
% Compute the linear, displacement independent, divergence-displacement matrix
% for a three-manifold element.
%   Call as:      B = divmat(self, pc, x, Rm)
%   where
%      self=gcell
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
% Local data structures:
%      Ndersp=matrix of spatial derivatives of the basis functions
%          with respect to the local coordinate system Rm.
% Output: B = divergence-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of displacement components, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
function B = divmat(self, pc, x, Rm)
    Nder = bfundpar (self, pc);
    Ndersp=bfundsp(self,Nder,x*Rm);
    nfens = size(Ndersp, 1);
    dim=3;
    B = zeros(3,nfens*dim);
    for i= 1:nfens
        B(:,dim*(i-1)+1:dim*i)...
            = [ Ndersp(i,1) 0           0  ; ...
            0           Ndersp(i,2) 0  ; ...
            0           0           Ndersp(i,3) ]*Rm';
    end
    return;
end
