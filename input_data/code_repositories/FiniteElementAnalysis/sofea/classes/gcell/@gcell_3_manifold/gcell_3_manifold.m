% Constructor
%
% function retobj = gcell_3_manifold (varargin)
%
% Class represents a three-manifold geometric cell, which encloses some part
% of the computational domain.
% This class is abstract, i.e. every usable 3-D gcell has to be
% derived from it.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell.
% and thickness = thickness of the element
%
%   Example: gcell_3_manifold(struct ('id',13,'conn',[1 8 4 18]))  creates a cell
%      that connects nodes 1 8 4 18, and gives it the `name' 13.
%
function retobj = gcell_3_manifold (varargin)
    class_name='gcell_3_manifold';
    if (nargin == 0)
        parent=gcell;
        retobj = class(struct([]),class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1};
            parent=gcell(options);
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
