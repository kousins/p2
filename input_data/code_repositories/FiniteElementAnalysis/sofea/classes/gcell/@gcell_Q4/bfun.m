% Evaluate the basis function matrix for an 4-node quadrilateral.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
    one_minus_xi = (1 - param_coords(1));
    one_plus_xi  = (1 + param_coords(1));
    one_minus_eta = (1 - param_coords(2));
    one_plus_eta  = (1 + param_coords(2));

    val = [0.25 * one_minus_xi * one_minus_eta;
        0.25 * one_plus_xi  * one_minus_eta;
        0.25 * one_plus_xi  * one_plus_eta;
        0.25 * one_minus_xi * one_plus_eta];
    return;
end

