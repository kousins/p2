% Evaluate the derivatives of the basis function matrix.
%
% function val = bfundpar (self, param_coords)
%
% Returns an array of NFENS rows, and DIM columns, where
%    NFENS=number of nodes, and
%    DIM=number of spatial dimensions.
% Call as:
%    Nder = bfundpar(g, pc)
% where g=gcell
%       pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
% Column j holds derivatives of the basis functions with respect
% to the parametric coordinate j. Row i holds derivatives of the basis
% function i with respect to all the parametric coordinates in turn.
%
function val = bfundpar (self, param_coords)
    xi  =  param_coords(1);
    eta =  param_coords(2);
    val = [ ...
        -(1. - eta) * 0.25, -(1. - xi) * 0.25;
        (1. - eta) * 0.25, -(1. + xi) * 0.25;
        (1. + eta) * 0.25, (1. + xi) * 0.25;
        -(1. + eta) * 0.25, (1. - xi) * 0.25];
    return;
end
