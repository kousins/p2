% Constructor of the geometric cell Class.
%
% function retobj = gcell(varargin)
%
% Constructor of the class that represents a geometric cell, which encloses some part
% of the computational domain.
% This class is abstract, i.e. every usable gcell has to be
% derived from it.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%     id=identifier of this gcell: any integer.  This is useful
%        for debugging.
%     conn=connectivity of the gcell: numbers of nodes this cell connects.
%     nfens=number of nodes this cell *should* connect; it is checked
%           against length(conn): if there is a mismatch, an error is reported.
%
function retobj = gcell(varargin)
    class_name='gcell';
    if (nargin == 0)
        parent=classbase;
        retobj.id = 0;
        retobj.conn = [];
        retobj.nfens = 0;
        retobj.label = 0;
        retobj = class(retobj,class_name,parent);
        return;
    elseif (nargin == 1)
        parent=classbase;
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            retobj.id = 0;
            if isfield(options,'id')
                retobj.id = options.id;
            end
            retobj.conn = [];
            if isfield(options,'conn')
                retobj.conn = options.conn;
            end
            retobj.nfens = 0;
            if isfield(options,'nfens')
                retobj.nfens = options.nfens;
            end
            retobj.label = 0;
            if isfield(options,'label')
                retobj.label = options.label;
            end
            if (~ isempty(retobj.conn) & (retobj.nfens ~= length(retobj.conn)))
                error('Wrong number of nodes!');
            end
            retobj = class(retobj,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end

