% Evaluate the basis function matrix.
%
% function val = bfun(self, param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=DIM.
%
function val = bfun(self, param_coords)
    val = []; % This doesn't do anything special, since we don't know how to interpolate over this cell yet.
    % The specialization will know how to interpolate.
    return;
end

