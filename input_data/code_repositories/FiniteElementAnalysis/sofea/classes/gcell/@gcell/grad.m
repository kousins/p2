% Evaluate the gradient of the scalar on input.
%
% function F = grad(self,Nder_spatial,a)
%
%   Call as:
%      F = grad (g, Nder_spatial, a)
%   where
%      g=gcell,
%      Nder_spatial=matrix of spatial derivatives of the basis functions,
%                   size(Nder_spatial)=[nbfuns,dim]
%      a=array of the values at the nodes, size(x) = [nbfuns],
%                nbfuns=number of nodes (== number of basis functions) per gcell
%                       of the class class(g)
%
function F = grad(self,Nder_spatial,a)
    F=a'*Nder_spatial;% this is a general-purpose expression
    return;
end
