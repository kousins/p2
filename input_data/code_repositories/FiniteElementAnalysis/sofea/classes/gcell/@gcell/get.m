% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'id'}, {'gcell identifier, scalar'}};
            {{'nfens'}, {'number of nodes connected by this cell, scalar'}};
            {{'label'}, {'user-interpreted label for this cell, scalar'}};
            {{'conn'}, {'connectivity (node identifiers), array [nfens,1]'}}};
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'id'
                val = self.id;
            case 'conn'
                val = self.conn;
            case 'label'
                val = self.label;
            case 'nfens'
                val = self.nfens;
            case 'dim'
                val = 0;% this needs to be overridden by the specialization
            otherwise
                val = get(self.classbase,prop_name);
        end
    end
end
