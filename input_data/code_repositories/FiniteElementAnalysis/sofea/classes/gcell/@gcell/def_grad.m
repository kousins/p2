% Evaluate the deformation gradient.
%
% function F = def_grad(self,Nder_spatial,x)
%
%   Call as:
%      F = def_grad (g, Nder_spatial, x)
%   where
%      g=gcell,
%      Nder_spatial=matrix of spatial derivatives of the basis functions,
%                   size(Nder_spatial)=[nbfuns,dim]
%      x=array of the coordinates of the nodes (or components of displacement),
%               of size(x) = [nbfuns,dim],
%         where, dim=number of spatial dimensions
%                nbfuns=number of nodes (== number of basis functions) per gcell
%                       of the class class(g)
%
% Note: the derivatives and components must be with respect to the same
% coordinate system.
%
function F = def_grad(self,Nder_spatial,x)
    F=x'*Nder_spatial;% this is a general-purpose expression
    return;
end
