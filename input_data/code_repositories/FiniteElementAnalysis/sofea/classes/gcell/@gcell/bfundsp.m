% Evaluate the spatial derivatives of the basis functions.
%
% function Ndersp = bfundsp (self, Nder, x)
%
%   Call as:    Ndersp = bfundsp (self, Nder, x)
%   where
%      self=gcell whose basis functions derivatives are to be evaluated,
%      Nder=matrix of basis function derivatives wrt parametric 
%           coordinates (see bfundpar); size(Nder) = [nbfuns,dim]
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%      nbfuns=number of nodes (== number of basis functions) per gcell
%           of the class class(self)
function Ndersp = bfundsp (self, Nder, x)
    [nbfuns,dim] = size(Nder);
    if (size(Nder) ~= size(x))
        error('Wrong dimensions of arguments!');
    end
    J = x' * Nder;% Compute the Jacobian matrix
    detJ = det(J);% Compute the Jacobian
    if (detJ <= 0) % trouble
        error('Non-positive Jacobian');
    end
    Ndersp = Nder * inv(J);
end
