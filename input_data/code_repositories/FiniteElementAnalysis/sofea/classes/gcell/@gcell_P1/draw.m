% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
return;

