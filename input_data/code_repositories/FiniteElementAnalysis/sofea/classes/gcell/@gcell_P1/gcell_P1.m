% Constructor Of the point geometric cell.
%
% function retobj = gcell_P1 (varargin)
%
% Class represents a point geometric cell.
% 
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell, gcell_0_manifold.
%
function retobj = gcell_P1 (varargin)
    class_name='gcell_P1';
    nfens=1;
    if (nargin == 0)
        options.nfens=nfens;
        parent=gcell_0_manifold(options);
        retobj = class(struct([]),class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1}; options.nfens=nfens;
            parent=gcell_0_manifold(options);
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
