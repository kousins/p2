% gcell
%   A gcell is a geometric cell.  
%   The gcell class is abstract (i.e. there is no point 
%   in making objects of this class), but all other gcells
%   (gcell_H8 for hexahedra, ...) are descendants of this 
%   class. More precisely, they are descendents of the classes
%   gcell_X_manifold, which endow the class gcell with a
%   dimension (0 for a point, 1 for a curve, and so on).
% 
% -base class
%   @gcell/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell/bfun - Values of basis functionsValues of basis functions
%   @gcell/bfundsp - Derivatives of basis functions with respect to spatial
%                     (Cartesian) coordinates.
%   @gcell/get - Get an attribute
%   @gcell/set - Set an attribute
%   @gcell/grad - Compute a gradient of some quantity
%   @gcell/gcell - Constructor
%   @gcell/def_grad - Compute the deformation gradient
% 
% Specializations to manifolds of various dimensions:
% -zero-dimensional manifold (point)
%   @gcell_0_manifold/gcell_0_manifold - Constructor
%   @gcell_0_manifold/other_dimension - Get the associated other dimension
%   @gcell_0_manifold/Jacobian_mdim - Compute Jacobian, give dimensions 
%   @gcell_0_manifold/Jacobian_volume - Compute Jacobian, volume 
%   @gcell_0_manifold/Jacobian_surface - Compute Jacobian, Surface 
%   @gcell_0_manifold/Jacobian_curve - Compute Jacobian, curve 
%   @gcell_0_manifold/Jacobian - Jacobian appropriate for this manifold.
%   @gcell_0_manifold/get - Gets an attribute
%   @gcell_0_manifold/Blmat - Compute strain-displacement matrix.
% -one-dimensional manifold (curve)
%   @gcell_1_manifold/gcell_1_manifold - constructor
%   @gcell_1_manifold/other_dimension - Get the associated other dimension
%   @gcell_1_manifold/Jacobian_mdim - Compute Jacobian, give dimensions 
%   @gcell_1_manifold/Jacobian_volume - Compute Jacobian, volume 
%   @gcell_1_manifold/Jacobian_surface - Compute Jacobian, Surface 
%   @gcell_1_manifold/Jacobian_curve - Compute Jacobian, curve 
%   @gcell_1_manifold/Jacobian - Jacobian appropriate for this manifold.
%   @gcell_1_manifold/get - Get an attribute
%   @gcell_1_manifold/Blmat - Compute strain-displacement matrix.
% -two-dimensional manifold (surface)
%   @gcell_2_manifold/gcell_2_manifold - Constructor
%   @gcell_2_manifold/other_dimension - Get the associated other dimension
%   @gcell_2_manifold/Jacobian_mdim - Compute Jacobian, give dimensions 
%   @gcell_2_manifold/Jacobian_volume - Compute Jacobian, volume 
%   @gcell_2_manifold/Jacobian_surface - Compute Jacobian, Surface 
%   @gcell_2_manifold/Jacobian - Jacobian appropriate for this manifold.
%   @gcell_2_manifold/get - Got an attribute
%   @gcell_2_manifold/Blmat - Compute strain-displacement matrix.
% -three-dimensional manifold (solid)
%   @gcell_3_manifold/gcell_3_manifold - constructor
%   @gcell_3_manifold/Jacobian_mdim -  Compute Jacobian, give dimensions 
%   @gcell_3_manifold/Jacobian_volume - Compute Jacobian, volume 
%   @gcell_3_manifold/Jacobian - Jacobian appropriate for this manifold.
%   @gcell_3_manifold/get - Get an attribute
%   @gcell_3_manifold/Blmat - Compute strain-displacement matrix.
% 
% Concrete classes:
%-four-node tetrahedron
%   @gcell_T4/gcell_T4 - Constructor
%   @gcell_T4/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_T4/bfun - Values of basis functions
%   @gcell_T4/get - Gets an attribute
%   @gcell_T4/draw - Draw the cell
% -six-node triangle
%   @gcell_T6/gcell_T6 - Constructor
%   @gcell_T6/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_T6/bfun - Values of basis functions
%   @gcell_T6/get - Gets an attribute
%   @gcell_T6/draw - Draw the cell
% -eight-node Quadrilateral
%   @gcell_Q8/gcell_Q8 -  constructor
%   @gcell_Q8/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_Q8/bfun - Values of basis functions
%   @gcell_Q8/get - Gets an attribute
%   @gcell_Q8/draw - Draw the cell
% -three-node triangle
%   @gcell_T3/gcell_T3 - Constructor
%   @gcell_T3/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_T3/bfun - Values of basis functions
%   @gcell_T3/get - Get an attribute
%   @gcell_T3/draw - Draw the cell
% -10-node tetrahedron
%   @gcell_T10/gcell_T10 - Constructor
%   @gcell_T10/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_T10/bfun - Values of basis functions
%   @gcell_T10/get - Get an attribute
%   @gcell_T10/draw - Draw the cell
% -20-node hexahedron
%   @gcell_H20/gcell_H20 - Constructor
%   @gcell_H20/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_H20/bfun - Values of basis functions
%   @gcell_H20/get - Got an attribute
%   @gcell_H20/draw - Draw the cell
% -two-node segment
%   @gcell_L2/gcell_L2 - Constructor
%   @gcell_L2/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_L2/bfun - Values of basis functions
%   @gcell_L2/get - Gets an attribute
%   @gcell_L2/draw - Draw the cell
% -four-node quadrilateral
%   @gcell_Q4/gcell_Q4 - Constructor
%   @gcell_Q4/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_Q4/bfun - Values of basis functions
%   @gcell_Q4/get - Get an attribute
%   @gcell_Q4/draw - Draw the cell
% -one-node point
%   @gcell_P1/gcell_P1 - Constructor
%   @gcell_P1/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_P1/bfun - Values of basis functions
%   @gcell_P1/get - Get an attribute
%   @gcell_P1/draw - Draw the cell
% -three-node segment
%   @gcell_L3/gcell_L3 - Constructor
%   @gcell_L3/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_L3/bfun - Values of basis functions
%   @gcell_L3/get - Gets an attribute
%   @gcell_L3/draw - Draw the cell
% -eight-node Hexahedron
%   @gcell_H8/gcell_H8 -  constructor
%   @gcell_H8/bfundpar - Derivatives of basis functions with the respect to 
%                     parametric coordinates.
%   @gcell_H8/bfun - Values of basis functions
%   @gcell_H8/get - Get an attribute
%   @gcell_H8/draw - Draw the cell



