% Evaluate the basis function matrix for an 4-node quadrilateral.
%
% function val = bfun(self,param_coords)
%
%   Call as:
%      N = bfun (g, pc)
%   where
%      g=gcell,
%      pc=parametric coordinates, -1 < pc(j) < 1, length(pc)=3.
%
function val = bfun(self,param_coords)
    xim    = (-1 + param_coords(1));
    etam   = (-1 + param_coords(2));
    xip    = (1 + param_coords(1));
    etap   = (1 + param_coords(2));
    val = [ -1.0/4*xim*etam*(1+param_coords(1)+param_coords(2));
        1.0/4*xip*etam*(1-param_coords(1)+param_coords(2));
        -1.0/4*xip*etap*(1-param_coords(1)-param_coords(2));
        1.0/4*xim*etap*(1+param_coords(1)-param_coords(2));
        1.0/2*xim*xip*etam;
        -1.0/2*etam*etap*xip;
        -1.0/2*xim*xip*etap;
        1.0/2*etam*etap*xim];
    return;
end
