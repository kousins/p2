% Constructor of a eight-node quadrilateral geometric cell.
%
% function retobj = gcell_Q8(varargin)
%
% 
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: same as gcell, gcell_2_manifold.
%
function retobj = gcell_Q8(varargin)
    class_name='gcell_Q8';
    nfens=8;
    if (nargin == 0)
        options.nfens=nfens;
        parent=gcell_2_manifold(options);
        retobj = class(struct([]),class_name, parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options=varargin{1}; options.nfens=nfens;
            parent=gcell_2_manifold(options);
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
