% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'bdryconn'}, {'connectivity (node identifiers), array [nfens,2]'}}
            };
        val = cat(1, get(self.gcell_2_manifold), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case {'edgeconn','bdryconn'}
                conn=get(self.gcell_2_manifold,'conn');
                val = [conn([1,2,5]);conn([2,3,6]);conn([3,4,7]);conn([4,1,8])];
            case {'bdrygcell'}
                val = @gcell_L3;
            otherwise
                val = get(self.gcell_2_manifold, prop_name);
        end
    end
end
