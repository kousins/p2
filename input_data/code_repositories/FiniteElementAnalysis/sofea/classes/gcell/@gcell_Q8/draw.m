% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
    conn = get(self, 'conn'); % connectivity
    x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
    u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
    xu=x+u;
    f=[1,5,8;2,6,5;3,7,6;4,8,7;5,6,7;7,8,5];
    ef=[1,5,2,6,3,7,4,8,1];
    if isfield(context,'shrink') & context.shrink~=1
        pc= [-1,-1;1,-1;1,1;-1,1;0,-1;1,0;0,1;-1,0]; 
        n=length(conn);
        c=ones(n,1)*(sum(pc)/n);
        pc=c+context.shrink*(pc-c);
        xus=xu;
        for j=1:n
            N=bfun(self,pc(j,:));
            xus(j,:)=N'*xu;
        end
        xu=xus;
    end
    if (isfield(context,'colorfield'))
        colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
        options=struct ('colors',colors);
    else
        facecolor = 'red';
        if (isfield(context,'facecolor'))
            facecolor = context.facecolor;
        end
        edgecolor = 'Black';
        if (isfield(context,'edgecolor'))
            edgecolor = context.edgecolor;
        end
        options=struct ('facecolor',facecolor,'edgecolor',edgecolor);
    end
    edgecolor='black';
    if (isfield(context,'edgecolor'))
        edgecolor =context.edgecolor;
    end
    options.edgecolor =edgecolor;
    if ~strcmp(edgecolor,'none')
        draw_polyline (gv, xu, ef, options);
    end
    if (isfield(context,'alpha'))
        options.alpha = context.alpha;
    end
    options.edgecolor ='none';
    for i=1:size(f,1)
        draw_polygon (gv, xu, f(i,:), options);
    end
end
