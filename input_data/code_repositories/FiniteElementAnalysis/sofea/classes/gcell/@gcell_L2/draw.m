% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
%
function draw (self, gv, context)
conn = get(self, 'conn'); % connectivity
x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
xu=x+u;
if isfield(context,'shrink')
    c=sum(xu,1)/2;
    xu=(1-context.shrink)*ones(2,1)*c+context.shrink*xu;
end
if (isfield(context,'colorfield'))
    colors =gather(context.colorfield, conn, 'values', 'noreshape'); % coordinates of nodes
    options=struct ('colors',colors);
else
    facecolor = 'red';
    if (isfield(context,'facecolor'))
        facecolor = context.facecolor;
    end
    options=struct ('facecolor',facecolor);
end
A1=other_dimension (self, -1);
A2=other_dimension (self, +1);
draw_cylinder(gv, xu(1,:),xu(2,:),sqrt(A1/pi),sqrt(A2/pi), context);
return;

