% Draw the color bar
%
% function draw_colorbar(self, options)
%
% options= Structure with optional fields:
% colormap=Colormap
% position=Where to put it in the normalized coordinates
% minmax=Minimum and maximum
% label= description text
% 
function draw_colorbar(self, options)
    cmap=colormap;
    if isfield(options,'colormap')
        cmap = options.colormap;
    end
    color=[0, 0, 0];
    if isfield(options,'color')
        color = options.color;
    end
    Fontsize=12;
    if isfield(options,'fontsize')
        Fontsize = options.fontsize;
    end
    position =[0.869 0.15 0.05 0.7];
    if isfield(options,'position')
        position = options.position;
    end
    minmax = [0, 1];
    if isfield(options,'minmax')
        minmax= options.minmax;
    end
    label = ' Data';
    if isfield(options,'label')
        label= options.label;
    end
    colormap(cmap);
    cbh=colorbar;
    set(cbh,...
        'Position',position,'YLim',minmax);
    YTick= get(cbh,'YTick');
    YTick=(YTick-minmax(1))./(minmax(2)-minmax(1));
    YTickLabel= get(cbh,'YTickLabel');
    m=size(YTickLabel,1);
    yl(1:m)=deal({' '});
    for i=1:m
        yl(i)={YTickLabel(i,:)};
    end
    YTickLabel=yl;
    mtd=1/5/length(YTick);
    if abs(YTick(1)) >mtd
        YTick=[0,YTick];
        YTickLabel=cat(2,{num2str(minmax(1))},YTickLabel);
    end
    if abs(YTick(end)-1) >mtd
        YTick=[YTick,1];
        YTickLabel= cat(2,YTickLabel,{num2str(minmax(2))});
    end
    set(cbh,...
        'Position',position,'XColor',color,'YColor',color,...
        'CLim',[0, 1],'YLim',[0, 1],'YTick',YTick,'YTickLabel',YTickLabel);
    if (~isempty(Fontsize))
        set(cbh,'Fontsize',Fontsize);
    end
    set(get(cbh,'XLabel'),'String',label);
    if (~isempty(Fontsize))
        set(get(cbh,'XLabel'),'Fontsize',Fontsize);
    end
    
end
