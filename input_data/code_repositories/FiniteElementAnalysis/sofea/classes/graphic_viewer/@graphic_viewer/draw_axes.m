% Draw the basis vector of the global Cartesian coordinate system.
%
% function draw_axes(self, options)
%
% Draw the basis vector of the global Cartesian coordinate system as
% arrows, red, green, and blue.
% 
% options= optional structure, with fields
% length = length of the axes' arrows
% 
function draw_axes(self, options)
    if ((nargin >1) && isstruct(options) && isfield(options,'length'))
        a=options.length;
    else
        ax=get(self.figure,'CurrentAxes');
        a=abs(get(ax,'XLim'));
        a=a+abs(get(ax,'YLim'));
        a=a+abs(get(ax,'ZLim'));
        a=sum(a)/20;
        options = struct('color', [0, 0, 0]);
    end
    options.color='red';
    draw_arrow(self, [0 0 0], [a  0 0],options);
    draw_text(self, [a  0 0], 'X', options);
    options.color='green';
    draw_arrow(self, [0 0 0], [0  a 0], options);
    draw_text(self, [0 a  0], 'Y', options);
    options.color='blue';
    draw_arrow(self, [0 0 0], [0  0 a], options);
    draw_text(self, [0 0  a], 'Z', options);
    return;
end

