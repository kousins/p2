% Draw polyline.
%
% function draw_polyline(self, x, face, options)
%
%
% required arguments:
% x=array of nvert number of rows and 3 columns; the coordinates
% edge=array of N rows; N=the numbers of vertices
%        of an N-sided polyLine.  The indices point into
%        the x array.
% options = struct with following optional fields
%    edgecolor = color if polyline should be drawn in solid color,
%    colors = array of nvert number of rows and 3 columns; per vertex colors
%
function draw_polyline(self, x, edge, options)
    edgecolor='black';
    if isfield(options,'edgecolor')
        edgecolor =options.edgecolor;
    end
    if (size(x,2)==3)
        line(x(edge,1),x(edge,2),x(edge,3), 'Color',edgecolor);
    else
        line(x(edge,1),x(edge,2),'Color',edgecolor);
    end
end
