% Draw text.
%
% function draw_text(self, xyz, txt, context)
%
%
% context = struct
% with mandatory fields
% xyz = anchor location
% txt = text
% context = struct
% with optional fields
% color = color of the text
% offset = offset with respect to the text anchor location xyz
%
function draw_text(self, xyz, txt, context)
    color ='black';
    if isfield(context,'color')
        color=context.color;
    end
    offset = 0;
    if isfield(context,'offset')
        offset=context.offset;
    end
     font = 'Times';
    if isfield(context,'font')
        offset=context.offset;
    end
    xyz=xyz + offset;
    xyz=[xyz 0 0];% just in case the node is in one- or two a dimensions
    h=text(xyz(1),xyz(2),xyz(3),txt,'Color', color);
    if isfield(context,'backgroundcolor')
       set(h,'backgroundcolor',context.backgroundcolor);
    end
    if isfield(context,'fontname')
       set(h,'fontname',context.fontname);
    end
    if isfield(context,'fontsize')
       set(h,'fontsize',context.fontsize);
    end
    if isfield(context,'fontangle')
       set(h,'fontangle',context.fontangle);
    end
    return;
end