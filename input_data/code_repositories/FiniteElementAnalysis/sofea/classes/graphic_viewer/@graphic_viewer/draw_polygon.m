% Draw polygon.
%
% function draw_polygon(self, x, face, options)
%
%
% required arguments:
% x=array of nvert number of rows and 3 columns; the coordinates
% face=array of N rows; N=the numbers of vertices
%        of an N-sided polygonal patch.  The indices point into
%        the x array.
% options = struct with following optional fields
%    facecolor = color if polygons should be drawn in solid color,
%    colors = array of nvert number of rows and 3 columns; per vertex colors
%    fill=string: either 'none' or 'interp' or 'flat'
%
function draw_polygon(self, x, face, options)
    edgecolor='black';
    if isfield(options,'edgecolor')
        edgecolor =options.edgecolor;
    end
    if isfield(options,'colors')
        h=patch('Faces', face, ...
            'Vertices', x,'NormalMode','manual','BackFaceLighting','lit',...
            'AmbientStrength',0.75,...
            'FaceVertexCData', options.colors,...
            'FaceColor', 'interp','EdgeColor',edgecolor);
    else
        facecolor='red';
        if isfield(options,'facecolor')
            facecolor =options.facecolor;
        end
        h=patch('Faces', face, ...
            'Vertices', x,'NormalMode','manual','BackFaceLighting','lit',...
            'AmbientStrength',0.75,...
            'FaceColor', facecolor,'EdgeColor',edgecolor);
    end
    if isfield(options,'alpha')
        set (h,'FaceAlpha', options.alpha);
    end
end
