% Set camera settings for the view. 
%
% function camset (self,v)
%
% Set camera settings for the view. The values to use may be conveniently
% obtained with camget() after the view settings have been adjusted via the user interface.
function camset (self,v)
if (~self.initialized)
    self.figure = figure;
    view(3);
    hold on;
    %     cameratoolbar;
    self.initialized=true;
end
ax=get(self.figure,'CurrentAxes');
set(ax,'CameraPosition',v(1:3));
set(ax,'CameraTarget',v(4:6));
set(ax,'CameraUpVector',v(7:9));
set(ax,'CameraViewAngle',v(10));
set(self.figure,'CurrentAxes', ax);
return;

