% Rotate with mouse button.
%
% function retobj = rotate (self,options)
%
function retobj = rotate (self,options)
    drawnow;
    figure(self.figure);
    if get(self.figure,'CurrentAxes') ~=self.axes
        axes(self.axes);
    end
    view3d rot;
    retobj=self;
    return;
end
