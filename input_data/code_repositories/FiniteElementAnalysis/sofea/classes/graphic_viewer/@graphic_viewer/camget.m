% Get camera settings for the view.
%
% function v = camget (self)
%
function v = camget (self)
    v=[];
    if (self.initialized)
        ax=get(self.figure,'CurrentAxes');
        v(1:3)=get(ax,'CameraPosition');
        v(4:6)=get(ax,'CameraTarget');
        v(7:9)=get(ax,'CameraUpVector');
        v(10)=get(ax,'CameraViewAngle');
    end;
end

