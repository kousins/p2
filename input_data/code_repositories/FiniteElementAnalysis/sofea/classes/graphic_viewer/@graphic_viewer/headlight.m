% Turn on the headlight in the viewer's figure.
%
% function retobj = headlight(self)
%
function retobj = headlight(self)
if (self.initialized)
    figure(self.figure);
    camlight headlight;
end
retobj=self;
return;
