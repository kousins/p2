% Reset view for drawing.
%
% function retobj = reset (self, options)
%
% options= structure with optional fields:
% limits = array of the extents of the drawing
function retobj = reset (self, options)
self =clear(self, options);
if isfield(options,'peek')
    if options.peek
        peekb  = uicontrol('Parent',self.figure,'Style','pushbutton',...
            'Units','Normalized','Position',[.9 .0 .1 .05],...
            'String','Peek','Value',0,...
            'TooltipString','Invoke command line in order to peek at data',...
            'Callback',@peek_button_pressed);
    end
end
if ~isfield(options,'limits')
    view(3); axis equal vis3d;
    ax=get(gcf,'CurrentAxes');
    set(ax,'XLimMode','auto');
    set(ax,'YLimMode','auto');
    set(ax,'ZLimMode','auto');
else
    if length (options.limits) > 4
        axis equal vis3d;
        ax=get(gcf,'CurrentAxes');
        set(ax,'XLim',options.limits(1:2));
        set(ax,'YLim',options.limits(3:4));
        set(ax,'ZLim',options.limits(5:6));
    else
        axis equal;
        ax=get(gcf,'CurrentAxes');
        set(ax,'XLim',options.limits(1:2));
        set(ax,'YLim',options.limits(3:4));
    end
end
set(ax,'Units','normalized')
set(ax,'position',[0.05  0.1  0.8  0.8])
set(gcf,'CurrentAxes',ax);
set(gcf,'Renderer','OpenGL');
if (ispc) &&  (~strcmp(version('-release'),'13'))
    opengl hardware
end
% set(gcf,'DoubleBuffer','on');
% set(gcf,'Renderer','zbuffer');
% camlight left;
% camlight right;
% camlight headlight;
% lighting flat;
xlabel('X');
ylabel('Y');
zlabel('Z');
retobj=self;
return;

function peek_button_pressed(h,varargin)
keyboard;
return;
