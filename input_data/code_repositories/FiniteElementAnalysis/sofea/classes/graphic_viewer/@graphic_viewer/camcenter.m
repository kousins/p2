% Set camera  target for the view. 
%
% function camcenter (self,v)
%
% Set camera settings for the view. The values to use may be conveniently
% obtained with camget() after the view settings have been adjusted via the user interface.
function camcenter (self,v)
if (~self.initialized)
    self.figure = figure;
    view(3);
    hold on;
    %     cameratoolbar;
    self.initialized=true;
end
ax=get(self.figure,'CurrentAxes');
set(ax,'CameraTarget',v);
set(self.figure,'CurrentAxes', ax);
return;

