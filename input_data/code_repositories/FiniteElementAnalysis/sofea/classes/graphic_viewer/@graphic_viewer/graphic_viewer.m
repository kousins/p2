% Constructor of the graphic viewer class.
%
% function retobj = graphic_viewer (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%
function retobj = graphic_viewer (varargin)
    class_name='graphic_viewer';
    if (nargin == 0)
        parent=classbase;
        self.initialized =false;
        self.limits=[];
        self.figure =0;
        self.axes =0;
        self = class(self, class_name, parent);
        retobj = self;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=classbase;
            self.initialized =false;
            self.figure=0;
            self.axes =0;
            self.limits=[];
            self = class(self, class_name, parent);
            retobj = self;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
