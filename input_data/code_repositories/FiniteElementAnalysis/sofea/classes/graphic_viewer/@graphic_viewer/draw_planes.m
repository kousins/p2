% Draw the coordinate planes of the global Cartesian coordinate system.
%
% function draw_planes(self, options)
%
% Draw the basis vector of the global Cartesian coordinate system as
% arrows, red, green, and blue.
% 
% options= optional structure, with fields
% length = length of the axes' arrows
% 
function draw_planes(self, options)
    if ((nargin >1) && isstruct(options) && isfield(options,'length'))
        a=options.length;
    else
        ax=get(self.figure,'CurrentAxes');
        a=abs(get(ax,'XLim'));
        a=a+abs(get(ax,'YLim'));
        a=a+abs(get(ax,'ZLim'));
        a=sum(a)/20;
        options = struct('color', [0, 0, 0]);
    end
alpha=0.3;
    if (isfield(options,'alpha'))
        alpha= options.alpha;
    end
    options.facecolor='red';
    options.edgecolor='none';
    options.alpha=alpha;
    draw_polygon(self,  [[0 0 0]; [0 a 0]; [0 a a]; [0 0 a]], [1,2,3,4], options);
    draw_text(self, [a  0 0], 'X', options);
    options.facecolor='green';
    options.edgecolor='none';
    options.alpha=alpha;
    draw_polygon(self,  [[0 0 0]; [a  0 0]; [a  0 a]; [0 0 a]], [1,2,3,4], options);
    draw_text(self, [0 a  0], 'Y', options);
    options.facecolor='blue';
    options.edgecolor='none';
    options.alpha=alpha;
    draw_polygon(self,  [[0 0 0]; [a  0 0]; [a  a 0]; [0 a 0]], [1,2,3,4], options);
    draw_text(self, [0 0  a], 'Z', options);
    return;
end

