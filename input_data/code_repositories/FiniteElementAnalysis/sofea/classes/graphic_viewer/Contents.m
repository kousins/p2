% graphic_viewer
%   This class provides methods to draw the shape of the 
%   finite element mesh, with optional coloring of some
%   function defined on the mesh, integration point quantities, arrows, etc.
%
%   @graphic_viewer/graphic_viewer - Constructor
%   @graphic_viewer/clear - Clear the figure
%   @graphic_viewer/get - Gets an attribute
%   @graphic_viewer/reset - Reset the figure
%   @graphic_viewer/camget - Get the camera orientation
%   @graphic_viewer/camset - Set the camera orientation
%   @graphic_viewer/headlight - Turn on the headlight
%   @graphic_viewer/draw_axes - Draw coordinate axis arrows
%   @graphic_viewer/draw_text - Draw some text
%   @graphic_viewer/draw_cylinder - Draw cylinder
%   @graphic_viewer/draw_ellipsoid - Draw an ellipsoid
%   @graphic_viewer/draw_polygon - Draw a polygon
%   @graphic_viewer/draw_arrow - Draw an arrow
%   @graphic_viewer/draw_colorbar - Draw a color bar
%   @graphic_viewer/rotate -  Turn on rotation/zoom/panning controls
