% Should the element matrix be assembled symmetrically?
%
% function val = placesymm(self)
%
% Is the element matrix to be assembled with the same indexes in rows and
% columns (symmetrically)? Otherwise, separate equation numbers apply to
% rows as to columns. Then the matrices to be assembled may also 
% be rectangular.
% 
% Call as: if (placesymm(self)),...,end
function val = placesymm(self)
    val= isempty(self.eqnums_col);
end
