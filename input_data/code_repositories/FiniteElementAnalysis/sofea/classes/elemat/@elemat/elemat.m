% Constructor of the element matrix class.
%
% function self = elemat (varargin)
%
%  See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%
% Options:
% eqnums= equation numbers,
% mat = matrix
%
function self = elemat (varargin)
    class_name='elemat';
    parent=classbase;
    if (nargin == 0)
        self.eqnums_row = [];
        self.eqnums_col = [];
        self.mat = [];
        self = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            options =varargin{1};
            if isfield(options,'eqnums')
                self.eqnums_row = options.eqnums;
                self.eqnums_col = [];
            else
                self.eqnums_row = options.eqnums_row;
                self.eqnums_col = options.eqnums_col;
            end
            self.mat = options.mat;
            [m,n] = size(self.mat);
            if isempty(self.eqnums_col)
                gn=length(self.eqnums_row);
                if (gn ~= n | gn ~= m | n ~= m )
                    error('Size mismatch!');
                end
            else
                gm=length(self.eqnums_row);
                gn=length(self.eqnums_col);
                if (gn ~= n | gm ~= m)
                    error('Size mismatch!');
                end
            end
            self = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end

