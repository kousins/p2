% Set property in the specified object.
%
% function retval = set(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retval = set(self,varargin)
    if (nargin == 1)
        retval = {{{'eqnums'}, {'equation numbers, array [m,1]; for symmetric matrices only'}};
            {{'eqnums_row'}, {'row equation numbers, array [m,1]'}};
            {{'eqnums_col'}, {'column equation numbers, array [n,1]'}};
            {{'mat'}, {'data matrix, array [m,n], or array [m,1] (for diagonal matrix)'}}};
        return;
    end
    prop_name=varargin{1};
    val=varargin{2};
    switch prop_name
        case 'mat'
            self.mat = val;
        case 'eqnums'
            self.eqnums_row = val;
        case 'eqnums_row'
            self.eqnums_row = val;
        case 'eqnums_col'
            self.eqnums_col = val;
        otherwise
            error(['Unknown property ''' prop_name '''!'])
    end
    retval=self; % return self
end
