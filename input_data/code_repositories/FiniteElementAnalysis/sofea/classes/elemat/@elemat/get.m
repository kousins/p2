% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'eqnums'}, {'equation numbers, array [m,1]; for symmetric matrices only'}};
            {{'eqnums_row'}, {'row equation numbers, array [m,1]'}};
            {{'eqnums_col'}, {'column equation numbers, array [n,1]'}};
            {{'mat'}, {'data matrix, array [m,n]'}}};
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'eqnums'
                val = self.eqnums_row;
            case 'eqnums_row'
                val = self.eqnums_row;
            case 'eqnums_col'
                if isempty(self.eqnums_col)
                    val = self.eqnums_row;
                else
                    val = self.eqnums_col;
                end
            case 'mat'
                val = self.mat;
            otherwise
                error(['Unknown property name ''' prop_name '''!']);
        end
    end
end
