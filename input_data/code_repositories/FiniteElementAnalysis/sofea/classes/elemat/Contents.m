% elemat
%   Element matrix class. This class provides means of storing
%   element matrices (mass, stiffness), and transmitting them to
%   assembly by a sysmat object. Each object represents the pair
%   of a matrix and an array (or arrays for unsymmetric matrices)
%   that says where to assemble this matrix into the global
%   system matrix.
% 
%   @elemat/placesymm - Should the matrix be assembled symmetrically?
%   @elemat/get -  Get attribute.
%   @elemat/elemat - Constructor.
%   @elemat/set - Said an attribute.
