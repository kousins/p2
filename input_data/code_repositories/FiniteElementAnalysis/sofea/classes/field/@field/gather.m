% Gather an attribute of the field.
%
% function val = gather (self, conn, what, varargin)
%
% Gather an attribute of the field corresponding to the finite element node
% indices. 
%
%   Call as:
%       gather(f,conn,what), or
%       gather(f,conn,what,'noreshape')
%   where
%       conn  - node numbers for which to gather stuff; if supplied as
%                 empty, all nodes are assumed to be implied; and 
%       what  - What should be gathered?
%          what == 'values' to gather the values
%                           of the degree of freedom parameters;
%          what == 'eqnums' to gather the equation numbers,
%                           with the equations of a given degree
%                           of freedom parameter being ordered
%                           consecutively;
%          what == 'is_prescribed'  to gather the fixed/free tags,
%                           with the components of a given degree
%                           of freedom parameter being ordered
%                           consecutively;
%                           is_prescribed(i,j)==0 means j-th component of
%                                                 node i is FREE;
%                           is_prescribed(i,j)~=0 means j-th component of
%                                                 node i is FIXED.
%          what == 'prescribed_values'  to gather the prescribed
%                           values with the components of a given degree
%                           of freedom parameter being ordered
%                           consecutively;
%
%       If the additional flag 'noreshape' is used, the actual
%       data in rectangular array form are returned; otherwise
%       the data are reshaped into a column vector of (n*dim) elements,
%       where n=length(conn) and dim=dimension of the nodal parameter
%       of the field, with the components ordered consecutively.
%
%   For instance, if
%       f.eqnums=[ 1 2 3; 4 0 5; 0 0 6 ]
%   and conn=[1 3]
%       gather(f,conn,'eqnums') yields [ 1 2 3 0 0 6 ]
%
function val = gather (self, conn, what, varargin)
    [nfens,dim] = size(self.values);
    if (isempty(conn))
        conn = (1:nfens);
    end
    n = length(conn);
    should_reshape = 1; % default is to reshape
    if (nargin == 4)
        if (strcmp(varargin{1},'noreshape'))
            should_reshape = 0;
        else
            error('Flag not recognized!');
        end
    end
    switch what
        case 'eqnums'
            val = self.eqnums(conn,:);
        case 'fixed'
            val = self.is_prescribed(conn,:);
        case 'is_prescribed'
            val = self.is_prescribed(conn,:);
        case 'prescribed_values'
            val = self.prescribed_values(conn,:);
        case 'values'
            val = self.values(conn,:);
        otherwise
            error(['Unknown argument ''' what '''!']);
    end
    if (should_reshape)
        val = reshape(val',n*dim,1);
    end
    return;
end

