% Apply translation to each nodal parameter.
%
% function retobj = translate(self, c)
%
% Note: makes sense only for 3D geometry fields!
%
function retobj = translate(self, c)
    [nfens,dim] = size(self.values);
    if (dim ~= 3)
        error('Dimensiom mismatch! Need 3.');
    end
    retobj = self;
    for i=1:nfens
        retobj.values(i,:)=retobj.values(i,:)+c;
    end
    return;
end
