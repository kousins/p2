% Term by term multiplication of two fields (scalar product).
%
% function val = times(A,B)
%
%
function val = times(A,B)
    if (isa(B,'field') & isa(A,'field'))
        if (size(A.values) ~= size(B.values))
            error('Size mismatch!');
        end
        [nfens,dim] = size(B.values);
        d = 0;
        for i=1:nfens
            d = d + dot(A.values(i,:),B.values(i,:));
        end
        val = d;
    else
        error ('Invalid operands!');
    end
    return;
end
