% Number the equations.
%
% function retobj = numbereqns (self,varargin)
%
%    Call as:
%      f=numbereqns(f);
%    or
%      f=numbereqns(f,n);
% where
% n=either (1) a scalar which is the total number of equations, 
%       or (2) an array of equation numbers when size(n)==[nfens,dim], 
%       or (3) permutation vector to be applied to the current numbering.
% 
% Note that we're assigning to f: f changes inside this function!
%
% The free components in the field are numbered consecutively.
% No effort is made to optimize the numbering in any way.
%
function retobj = numbereqns (self,varargin)
    invalid_eqnum = get(sysmat, 'invalid_eqnum');
    [nfens,dim] = size(self.values);
    arg= [];
    if nargin > 1
        arg=varargin{1};
    end
    if ~isempty(arg)
        if prod(size(arg))==1
            self.neqns = arg;
        elseif (size(arg)==[nfens,dim])
            self.eqnums=arg;
            self.neqns =max(max(self.eqnums));
            retobj = self;
            return;
        elseif (length(arg)==self.neqns)
            permutation=arg;
            k=1;
            for i=1:nfens
                for j=1:dim
                    if (~self.is_prescribed(i,j))
                        self.eqnums(i,j) = permutation(k);
                        k=k+1;
                    end
                end
            end
            retobj = self;
            return
        end
    else
        self.neqns = 0;
    end
    % regular numbering
    for i=1:nfens
        for j=1:dim
            if (~self.is_prescribed(i,j))
                self.neqns = self.neqns + 1;
                self.eqnums(i,j) = self.neqns;
            else
                self.eqnums(i,j) = invalid_eqnum;
            end
        end
    end
    retobj = self;
end


