% Add a scalar to a field, or add two fields together.
%
% function retobj = plus (A,B)
%
%    Call as:
%      plus(field,double) or plus(double,field), or plus(field1,field2)
% *Both* the values attribute and the
% prescribed_values attribute are modified.
%
function retobj = plus (A,B)
    if     (isa(A,'field') & isa(B,'double'))
        A.values = A.values + B;
%         A.prescribed_values = A.prescribed_values + B;
        retobj = A;
    elseif (isa(B,'field') & isa(A,'double'))
        B.values = B.values + A;
%         B.prescribed_values = B.prescribed_values + A;
        retobj = B;
    elseif (isa(B,'field') & isa(A,'field'))
        B.values = B.values + A.values;
        B.prescribed_values = B.prescribed_values + A.prescribed_values;
        retobj = B;
    else
        error('Undefined operands!');
    end
    return;
end

