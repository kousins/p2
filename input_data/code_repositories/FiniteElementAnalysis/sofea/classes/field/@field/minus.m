% Subtract a scalar from a field, or subtract a field from another field.
%
% function retobj = minus (A,B)
%
%    Call as:
%      minus(field,double) or minus(double,field) or minus(field,field)
% *Both* the values attribute and the
% prescribed_values attribute are modified.
%
function retobj = minus (A,B)
    if     (isa(A,'field') & isa(B,'double'))
        A.values = A.values - B;
        A.prescribed_values = A.prescribed_values - B;
        retobj = A;
    elseif (isa(B,'field') & isa(A,'double'))
        B.values = B.values - A;
        B.prescribed_values = B.prescribed_values - A;
        retobj = B;
    elseif (isa(B,'field') & isa(A,'field'))
        A.values = A.values - B.values;
        A.prescribed_values = A.prescribed_values - B.prescribed_values;
        retobj = A;
    else
        error('Undefined operands!');
    end
    return;
end
