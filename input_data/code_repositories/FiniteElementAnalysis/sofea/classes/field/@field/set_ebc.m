% Set the EBC's (essential boundary conditions).
%
% function retobj = set_ebc (self, fenids, is_prescribed, comp, val)
%
% To actually apply the EBC's (that is to copy the prescribed values
% into the appropriate places in the values array), use apply_ebc().
%    Call as:
%       retobj = set_ebc (self, fenids, is_prescribed, comp, val)
%    where
%       self           - field
%       fenids         - array of node identifiers
%       is_prescribed  - an array which say whether the component
%                        is being prescribed (value 1), or whether
%                        it is to be free (value 0).
%       comp           - components to which to apply the EBC's; comp
%                        may be supplied as '[]', in which case the
%                        prescribed values apply to all components.
%       val            - values of prescribed component magnitudes,
%                        length(val) == length(fenids)
%
function retobj = set_ebc (self, fenids, is_prescribed, comp, val)
    [nfens,dim] = size(self.values);
    n = length(fenids);
    for i=1:n
        k = fenids(i);
        if (isempty(comp))
            m = 1:dim;
        else
            m = comp(i);
            if m>dim
                error(['Component: ' num2str(m) ' versus dim=' num2str(dim)]);
            end
        end
        self.is_prescribed(k,m) = is_prescribed(i);
        if (self.is_prescribed(k,m))
            self.prescribed_values(k,m) = val(i);
        end
    end
    retobj = self;
end
