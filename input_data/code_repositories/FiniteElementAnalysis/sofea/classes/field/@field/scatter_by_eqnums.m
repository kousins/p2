% Scatter values to the field by  equation numbers.
%
% function retobj = scatter_by_eqnums (self, eqnums, val)
%
% Scatter values to the field corresponding
% to the equation numbers.
%   Call as:
%     f=scatter_by_eqnums(f, eqnums, val)
%   where
%     f=field (note that we have to assign to f: f changes inside this function)
%     eqnums=array of equation numbers
%     val=array of values to set (i.e. double numbers)
%
function retobj = scatter_by_eqnums (self, eqnums, val)
    n = length(eqnums);
    [nfens,dim] = size(self.values);
    for i=1:nfens
        for j=1:dim
            en = self.eqnums(i,j);
            for k=1:n
                if ((eqnums(k) > 0) & (eqnums(k) == en))
                    self.values(i,j) = val(k);
                end
            end
        end
    end
    retobj = self;
end
