% Draw a graphic representation of the field.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
%    dof=direction for which to draw the prescribed value arrow
%    length=length of the arrows
% with optional fields
%    color = color of the arrows
%
function draw (self, gv, context)
    x = context.x.values; % coordinates of nodes
    u = context.u.values; % Field values at nodes
    direction = zeros(1,length(context.x.values(1, :)));
    direction(context.dof) = context.length;
     for i=1:size(self.is_prescribed, 1)
         if (self.is_prescribed(i, context.dof))
             draw_arrow(gv, x(i,:)+u(i,:), direction, context);
         end
     end
end
