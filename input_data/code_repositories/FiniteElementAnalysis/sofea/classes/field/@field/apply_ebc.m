% Apply EBC's (essential boundary conditions).
%
% function retobj = apply_ebc (self)
%
% This function applies existing boundary conditions.  In other
% words, it sets value=prescribed_values for all parameters and components
% of parameters that are being prescribed as essential boundary
% conditions (EBC's).
%
% To set the prescribed values and to mark components as being prescribed,
% use set_ebc().
%
% Don't forget to assign the field to itself, because this function
% changes the internal state of self.
%    Call as:
%      f = apply_ebc(f);
%
function retobj = apply_ebc (self)
    [nfens,dim] = size(self.values);
    for i=1:nfens
        for j=1:dim
            if (self.is_prescribed(i,j))
                self.values(i,j) = self.prescribed_values(i,j);
            end
        end
    end
    retobj = self;
end
