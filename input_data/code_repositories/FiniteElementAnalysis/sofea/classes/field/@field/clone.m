% Make a new FIELD by cloning another field
%
% function retobj = clone(fld,name)
%
%   Call as:
%     clonedf = clone(f, name)
%   where
%     f=field to clone
%     name=name of the new field
%
function retobj = clone(fld,name)
    retobj = field(fld);
    retobj.name = name;
    return;
end

