% Gather values from the field corresponding to the equation numbers.
%
% function val = gather_by_eqnums (self, eqnums)
%
% Gather values from the field corresponding
% to the equation numbers.
%
function val = gather_by_eqnums (self, eqnums)
    n = length(eqnums);
    val = zeros(n,1);
    [nfens,dim] = size(self.values);
    for i=1:nfens
        for j=1:dim
            en = self.eqnums(i,j);
            for k=1:n
                if (eqnums(k) == en)
                    val(k) = self.values(i,j);
                end
            end
        end
    end
    return;
end

