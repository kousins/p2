% Make a new FIELD by taking a slice of another field
%
% function retobj = slice(fld,idc,name)
%
%   Call as:
%     nf = slice(f, idc, name)
%   where
%     f=field to clone
%     idc = array of indexes of components; the new field consists of the
%     specified components taken out of the nodal parameters of the
%     original field.
%     name=name of the new field
%
function retobj = slice(fld,idc,name)
    values = get (fld,'values');
    retobj = field(struct('name',name,'data',values (:,idc)));
    return;
end

