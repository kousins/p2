% Constructor of the field class.
%
% function retobj = field (varargin)
%
% Class that represents fields: geometry, displacement, incremental rotation,
% temperature,...
%   This class provides the means of manipulating the finite
%   element expansion of the unknowns, i.e. a field of the form
%   u_h(x) = sum_i N_i(x)*u_i
%   where N_i(x) are the basis functions, and u_i are the
%   unknown nodal parameters.
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%  Options:
% always required:
%     name=name of the field
% In addition, supply one of the following combinations:
%     dim=dimension of the unknown parameters
%         (1=scalar field,
%          2=2D vector field,
%          3=3D vector field, etc)
%     nfens=number of unknown parameters in this field (equal to the number
%     of nodes)
%   or
%     dim=dimension of the unknown parameters
%         (1=scalar field,
%          2=2D vector field,
%          3=3D vector field, etc)
%     fens=array of objects of the class fenode
%   or
%     data=data array of the unknown parameters
%
%   The second form is useful to initialize a geometry field (i.e. a field
%   where the parameters are the coordinates of the nodes). The third form
%   is useful when creating a field out of of a data array, rows
%   corresponding to nodal parameters, number of columns is the dimension
%   of the field.
%
function retobj = field (varargin)
    class_name='field';
    if (nargin == 0)
        parent=classbase;
        self.name = '';
        self.dim  = 0;
        self.nfens = 0;
        self.values = [];
        self.eqnums = [];
        self.is_prescribed = [];
        self.prescribed_values = [];
        self.neqns = 0;
        retobj = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            parent=classbase;
            options =varargin{1};
            self.name = options.name;
            if (isfield(options,'fens'))
                self.dim  = options.dim;
                self.nfens = length(options.fens);
                self.values = zeros(self.nfens,self.dim);
                for i=1:self.nfens
                    id = get(options.fens(i),'id');
                    x = get(options.fens(i), 'xyz');
                    self.values(id,:) = x(1:self.dim);
                end
            elseif (isfield(options,'nfens'))
                self.dim  = options.dim;
                self.nfens = options.nfens;
                self.values = zeros(self.nfens,self.dim);
            elseif (isfield(options,'data'))
                self.dim = size (options.data,2);
                self.nfens = size (options.data,1);
                self.values = options.data;
            else
                error('no recognized option!');
            end
            self.eqnums = zeros(self.nfens,self.dim);
            self.is_prescribed = zeros(self.nfens,self.dim);
            self.prescribed_values = zeros(self.nfens,self.dim);
            self.neqns = 0;
            retobj = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end

