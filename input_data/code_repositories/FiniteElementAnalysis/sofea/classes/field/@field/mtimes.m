% Multiply a field by a scalar.
%
% function retobj = mtimes (A,B)
%
%    Call as:
%       mtimes(field,double) or mtimes(double,field)
% *Both* the values attribute and the
% prescribed_values attribute are modified.
%
function retobj = mtimes (A,B)
    if     (isa(A,'field') & isa(B,'double'))
        A.values = B * A.values;
        A.prescribed_values = B * A.prescribed_values;
        retobj = A;
    elseif (isa(B,'field') & isa(A,'double'))
        B.values = A * B.values;
        B.prescribed_values = A * B.prescribed_values;
        retobj = B;
    else
        error('Undefined operands!');
    end
    return;
end
