% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'name'}, {'name of the field, character array'}};
            {{'dim'}, {'dimension of the nodal parameters, scalar'}};
            {{'nfens'}, {'number of nodes associated with the field, scalar'}};
            {{'eqnums'}, {'equation numbers, array [nfens,dim]'}};
            {{'neqns'}, {'number of equations (i.e. total number of free components), scalar'}};
            {{'values'}, {'values of the nodal parameters, array [nfens,dim]'}};
            {{'is_prescribed'}, {'is the component of a nodal parameter prescribed or is it free? (0=free, 1=prescribed), array [nfens,dim]'}};
            {{'prescribed_values'}, {'prescribed values of the nodal parameters, array [nfens,dim]'}}};
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'name'
                val = self.name;
            case 'dim'
                [nfens,dim] = size(self.values);
                val = dim;
            case 'nfens'
                [nfens,dim] = size(self.values);
                val = nfens;
            case 'eqnums'
                val = self.eqnums;
            case 'neqns'
                val = self.neqns;
            case 'values'
                val = self.values;
            case 'is_prescribed'
                val = self.is_prescribed;
            case 'prescribed_values'
                val = self.prescribed_values;
            otherwise
                error(['Unknown property name ''' prop_name '''!']);
        end
    end
end

