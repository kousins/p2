% Apply rotation matrix to each nodal parameter.
%
% function retobj = rotate(self, R)
%
% Note: makes sense only for 3D geometry fields!
%
function retobj = rotate(self, R)
    [nfens,dim] = size(self.values);
    if (dim ~= 3)
        error('Dimensiom mismatch! Need 3.');
    end
    retobj = self;
    for i=1:nfens
        retobj.values(i,:)=(R*self.values(i,:)');
    end
    return;
end
