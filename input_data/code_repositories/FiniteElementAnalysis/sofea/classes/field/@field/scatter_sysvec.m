% Scatter values to the field from a sysvec.
%
% function retobj = scatter_sysvec (self, vec)
%
%   Call as:
%     f=scatter_sysvec(f, vec)
%   where
%     f=field (note that we have to assign to f: f changes inside this function)
%     vec=either a sysvec, or an array of doubles
%
function retobj = scatter_sysvec (self, vec)
    if (isa(vec, 'sysvec'))
        vec = get(sysv,'vec');
    end
    [nfens,dim] = size(self.values);
    for i=1:nfens
        for j=1:dim
            en = self.eqnums(i,j);
            if (en > 0 & en <= self.neqns)
                self.values(i,j) = vec(en);
            end
        end
    end
    retobj = self;
end
