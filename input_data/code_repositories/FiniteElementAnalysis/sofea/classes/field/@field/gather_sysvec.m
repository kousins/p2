% Gather values from the field for the whole system vector.
%
% function vec = gather_sysvec (self)
%
%   Call as:
%       v=gather_sysvec(f)
%
function vec = gather_sysvec (self)
    [nfens,dim] = size(self.values);
    vec=zeros(self.neqns,1);
    for i=1:nfens
        for j=1:dim
            en = self.eqnums(i,j);
            if (en > 0 & en <= self.neqns)
                vec(en)=self.values(i,j);
            end
        end
    end
    return;
end

