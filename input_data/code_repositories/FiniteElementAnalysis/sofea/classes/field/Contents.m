% field
%   This class provides the means of manipulating the finite
%   element expansion of the unknowns, i.e. a field of the form
%   u_h(x) = sum_i N_i(x)*u_i
%   where N_i(x) are the basis functions, and u_i are the 
%   unknown nodal parameters.
% 
%   @field/clone - Make a new FIELD by cloning another field
%   @field/translate - Apply translation to each nodal parameter.
%   @field/apply_ebc - Apply EBC's (essential boundary conditions).
%   @field/norm - Compute the norm of the field values array.
%   @field/scatter_by_eqnums - Scatter values to the field corresponding
%                     to the equation numbers.
%   @field/plus - Add a scalar to a field, or add two fields together.
%   @field/slice - Make a new FIELD by taking a slice of another field
%   @field/gather_sysvec - Gather values from the field for the whole
%                     system vector.
%   @field/gather_by_eqnums - Gather values the field corresponding
%                     to the equation numbers.
%   @field/gather - Gather an attribute of the field corresponding to given
%                      Finite element node indices.
%   @field/field - constructor
%   @field/get - Get an attribute
%   @field/rotate - Apply rotation matrix to each nodal parameter.
%   @field/mtimes - Multiply a field by a scalar.
%   @field/set_ebc - Set the EBC's (essential boundary conditions).
%   @field/numbereqns - Number the equations.
%   @field/minus - Subtract a scalar from a field, or subtract a field from
%                     another field.
%   @field/scatter - Scatter values to the field corresponding
%                     to given Finite element node indices.
%   @field/combine - Combine two fields into one.
%   @field/scatter_sysvec - Scatter values to the field from a system vector.
%   @field/times - Term by term multiplication of two fields (scalar
%                     product).
