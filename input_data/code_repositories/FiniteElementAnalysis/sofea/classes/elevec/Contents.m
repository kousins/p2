% elevec
%   Element vector class. This class provides means of storing
%   element vectors (loads), and transmitting them to assembly by
%   a sysvec object.
% 
%   @elevec/get - Gets an attribute
%   @elevec/set - Set an attribute.
%   @elevec/elevec - Constructor.
