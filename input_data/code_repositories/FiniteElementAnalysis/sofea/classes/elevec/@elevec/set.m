% Set property in the specified object.
%
% function retval = set(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retval = set(self,varargin)
    if (nargin == 1)
        retval = {{{'eqnums'}, {'equation numbers, array [d,1]'}};
            {{'vec'}, {'data matrix, array [d,1]'}}};
        return;
    end
    prop_name=varargin{1};
    val=varargin{2};
    switch prop_name
        case 'vec'
            self.vec = val;
        case 'eqnums'
            self.eqnums = val;
        otherwise
            error(['Unknown property ''' prop_name '''!'])
    end
    retval=self; % return self
end
