% Constructor of the element vector class.
%
% function self = elevec (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% eqnums= equation numbers,
% vec = data matrix
% Call as: self = elevec (struct('eqnums',eqnums,'vec',vec))
%
function self = elevec (varargin)
    class_name='elevec';
    parent=classbase;
    if (nargin == 0)
        self.eqnums = [];
        self.vec = [];
        self = class(self,class_name,parent);
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            options =varargin{1};
            self.eqnums = options.eqnums;
            self.vec = options.vec;
            n = length(self.vec);
            if (length(self.eqnums) ~= n)
                error('Size mismatch!');
            end
            self = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end


