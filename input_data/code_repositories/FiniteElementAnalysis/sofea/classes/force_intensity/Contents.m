% force_intensity
%   This class provides methods for manipulating distributed
%   loads (force-like objects).
%
%   @force_intensity/get - Get an attribute
%   @force_intensity/force_intensity - Constructor
%   @force_intensity/get_magn - Compute the magnitude at given point.
