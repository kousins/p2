% Get force intensity magnitude at the specified point.
%
% function val = get_magn(self,xyz)
%
%
function val = get_magn(self,xyz)
    if strcmp(class (self.magn),'inline') | strcmp(class (self.magn),'function_handle')
        val=feval(self.magn,xyz);
        if length(val)==3
            val=reshape(val, 3, 1);
        end
    else
        val = self.magn;%  position-independent
    end
    return;
end
