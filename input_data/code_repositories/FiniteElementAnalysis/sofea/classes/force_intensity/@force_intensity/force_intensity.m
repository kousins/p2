% Constructor of the force intensity class.
%
% function self = force_intensity (varargin)
%
% Constructor of the force intensity class (force per unit volume, be it
% force/length^3, force/length^2, force/length^1, or force/length^0).
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%      magn=vector of magnitudes, dimension must correspond to the
%            dimension of the displacement/geometry fields; may be an array
%            of doubles or a function handle/in-line function with signature
%                     function val=f(xyz)
%            where        
%                     xyz = location,
%            at which the force intensity is to be evaluated.
%
function self = force_intensity (varargin)
    class_name='force_intensity';
    parent = classbase;
    if (nargin == 0)
        self.magn = [];
        self = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            options = varargin{1};
            self.magn = options.magn;
            self = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
