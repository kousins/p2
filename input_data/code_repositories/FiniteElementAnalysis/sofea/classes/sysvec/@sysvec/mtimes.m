% Multiply a system vector by a scalar.
%
% function retobj = mtimes (A,B)
%
%    Call as:
%       mtimes(sysvec,double) or mtimes(double,sysvec)
%
function retobj = mtimes (A,B)
    if     (isa(A,'sysvec') & isa(B,'double'))
        A.vec = B * A.vec;
        retobj = A;
    elseif (isa(B,'sysvec') & isa(A,'double'))
        B.vec = A * B.vec;
        retobj = B;
    else
        error('Undefined operands!');
    end
    return;
end
