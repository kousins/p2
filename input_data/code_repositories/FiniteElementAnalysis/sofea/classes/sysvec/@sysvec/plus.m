% Add two system vectors together.
%
% function retobj = plus (A,B)
%
%
function retobj = plus (A,B)
    if (isa(B,'sysvec') & isa(A,'sysvec'))
        B.vec = B.vec + A.vec;
        retobj = B;
    else
        error('Undefined operands!');
    end
    return;
end

