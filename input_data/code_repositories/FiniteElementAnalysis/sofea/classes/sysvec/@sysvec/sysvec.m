% System vector constructor.
%
% function retobj = sysvec (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retobj = sysvec (varargin)
    class_name='sysvec';
    parent=classbase;
    if (nargin == 0)
        self.vec=[];
        retobj = class(self, class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            error(['Illegal argument ' inputname(1) ' of class ' class(varargin{1}) '!']);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
