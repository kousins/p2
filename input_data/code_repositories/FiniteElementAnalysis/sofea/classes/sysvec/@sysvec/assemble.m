% Assemble a cell array of Element vectors.
%
% function retobj = assemble (self, evs)
%
%
function retobj = assemble (self, evs)
    invalid_eqnum = get(sysmat, 'invalid_eqnum');
    neqns = length(self.vec);
    for m = 1:length(evs)
        eqnums = get(evs(m), 'eqnums');
        vec    = get(evs(m), 'vec');
        n = length(eqnums);
        for i = 1:n
            gi = eqnums(i);
            if ((gi > neqns))
                error('Index out of range!');
            end
            if ((gi ~= invalid_eqnum))
                self.vec(gi) = self.vec(gi) + vec(i);
            end
        end
    end
    retobj = self;
end
