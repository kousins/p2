% Finish vector assembly. 
%
% function retobj = finish (self, dim)
%
% Calling this method is optional: 
% it doesn't do anything useful at this point. 
%
function retobj = finish (self, dim)
  retobj = self;
  return;
end
