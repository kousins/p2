% Start vector assembly.  The vector is zeroed out.
%
% function retobj = start (self, dim)
%
%   dim=number of equations
%
function retobj = start (self, dim)
    self.vec = zeros(dim,1);
    retobj = self;
end


