% sysvec
%   This  class  represents the global, system vector
%   of loads (i.e. the right-hand side, or forcing term).
% 
%   @sysvec/start - Initialize the system vector.
%   @sysvec/finish - NOT used
%   @sysvec/get - Get an attribute
%   @sysvec/sysvec - Constructor
%   @sysvec/assemble - Assemble an array of element vectors.


