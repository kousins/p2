% Convert a 6-vector to a  matrix of 3x3 stress components (tensor)
%
% function t = stress_6v_to_3x3t (self,v)
%
% convert a 6-vector to a *symmetric*
% matrix of 3x3 stress components (tensor)
%
function t = stress_6v_to_3x3t (self,v)
    t(1,1) = v(1);
    t(2,2) = v(2);
    t(3,3) = v(3);
    t(1,2) = v(4);
    t(2,1) = v(4);
    t(1,3) = v(5);
    t(3,1) = v(5);
    t(3,2) = v(6);
    t(2,3) = v(6);
    return;

