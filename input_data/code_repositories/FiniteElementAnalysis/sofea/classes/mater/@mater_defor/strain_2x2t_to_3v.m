% Convert a matrix of 2x2 strain components  into a 3-component vector.
%
% function v = strain_2x2t_3v (self,t)
%
% Convert a matrix of 2x2 strain components (tensor)
% into a 3-component vector.
%
function v = strain_2x2t_3v (self,t)
  v(1,1) = t(1,1);
  v(2,1) = t(2,2);
  v(3,1) = t(1,2) + t(2,1);
  return;
