% Constructor of the class that represents deformable materials.
%
% function retobj = mater_defor(varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% optional parameters
%     rho=mass density (default is 1.0)
%
function retobj = mater_defor(varargin)
    class_name='mater_defor';
    parent=mater;
    if (nargin == 0)
        retobj = class(struct([]),class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
