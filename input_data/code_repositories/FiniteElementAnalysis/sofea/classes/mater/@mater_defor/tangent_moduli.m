% Calculate the material stiffness matrix.
%
% function val = tangent_moduli(self, context)
%
%   Call as:
%     D = tangent_moduli(m, context)
%   where
%     m=material
%    context=structure
%  with mandatory and optional fields that are required by the specific material implementation.
%
% the output arguments are
%     D=matrix 6x6
%
%   This is the base class: descendant needs to override this method.
%
function val = tangent_moduli(self, context)
    error('Base class does not implement this behaviour!');
    return;
end
