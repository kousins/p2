% Convert a strain 3-vector to a  matrix of 2x2 strain components (tensor)
%
% function t = strain_3v_to_2x2t (self,v)
%
% Convert a strain 3-vector to a *symmetric*
% matrix of 2x2 strain components (tensor)
%
function t = strain_3v_to_2x2t (self,v)
    t(1,1) = v(1);
    t(2,2) = v(2);
    t(1,2) = v(3)/2;
    t(2,1) = v(3)/2;
    return;

