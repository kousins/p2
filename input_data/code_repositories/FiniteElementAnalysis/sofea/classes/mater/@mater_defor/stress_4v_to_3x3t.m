% Convert a 4-vector to a  matrix of 3x3 stress components (tensor). 
%
% function t = stress_4v_to_3x3t (self,v)
%
%
% Convert a 4-vector to a *symmetric*
% matrix of 3x3 stress components (tensor).  This is
% conversion routine that would be useful for plane strain or axially symmetric conditions.
% The stress vector components are ordered as: 
%     sigma_x, sigma_y, tau_xy, sigma_z.
% Therefore, for axially symmetric analysis the components need to be
% reordered, as from the consider the equation they come out as sigma_x, sigma_y, sigma_z, tau_xy.
%
function t = stress_4v_to_3x3t (self,v)
    t=zeros (3, 3);
    t(1,1) = v(1);
    t(2,2) = v(2);
    t(1,2) = v(3);
    t(2,1) = v(3);
    t(3,3) = v(4);
    return;

