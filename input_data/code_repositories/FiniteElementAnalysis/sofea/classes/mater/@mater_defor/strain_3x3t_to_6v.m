% Convert a matrix of 3x3 strain components to a 6-component vector.
%
% function v = strain_3x3t_to_6v (self,t)
%
% convert a matrix of 3x3 strain components (tensor)
% into a 6-component vector.
%
function v = strain_3x3t_to_6v (self,t)
    v=zeros(6,1);
  v(1,1) = t(1,1);
  v(2,1) = t(2,2);
  v(3,1) = t(3,3);
  v(4,1) = t(1,2) + t(2,1);
  v(5,1) = t(1,3) + t(3,1);
  v(6,1) = t(3,2) + t(2,3);
  return;
