% Convert a 3-vector to a matrix of 3x3 stress components (tensor)
%
% function t = stress_3v_to_3x3t (self,v)
%
% Convert a 3-vector to a *symmetric*
% matrix of 3x3 stress components (tensor)
%
function t = stress_3v_to_3x3t (self,v)
    t=zeros (3, 3);
    t(1,1) = v(1);
    t(2,2) = v(2);
    t(1,2) = v(3);
    t(2,1) = v(3);
    return;

