% Convert a matrix of 3x3 stress components to a 6-component vector.
%
% function v = stress_3x3t_to_6v (self,t)
%
% Convert a matrix of 3x3 stress components (tensor)
% into a 6-component vector.
%
function v = stress_3x3t_to_6v (self,t)
    v(1,1) = t(1,1);
    v(2,1) = t(2,2);
    v(3,1) = t(3,3);
    v(4,1) = 1/2*(t(1,2) + t(2,1));
    v(5,1) = 1/2*(t(1,3) + t(3,1));
    v(6,1) = 1/2*(t(3,2) + t(2,3));
    return;

