% Convert a symmetric matrix of 2x2 stress components to a 3-component vector.
%
% function v = stress_2x2_to_3v (self,t)
%
% Convert a symmetric matrix of 2x2 stress components (tensor)
% into a 3-component vector.
%
function v = stress_2x2_to_3v (self,t)
  v(1,1) = t(1,1);
  v(2,1) = t(2,2);
  v(3,1) = 1/2*(t(1,2) + t(2,1));
  return;

