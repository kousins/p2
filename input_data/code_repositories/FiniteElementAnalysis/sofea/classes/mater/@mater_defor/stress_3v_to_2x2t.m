% Convert a 3-vector to a  matrix of 2x2 stress components (tensor)
%
% function t = stress_3v_to_2x2t (self,v)
%
% Convert a 3-vector to a *symmetric*
% matrix of 2x2 stress components (tensor)
%
function t = stress_3v_to_2x2t (self,v)
    t(1,1) = v(1);
    t(2,2) = v(2);
    t(1,2) = v(3);
    t(2,1) = v(3);
    return;

