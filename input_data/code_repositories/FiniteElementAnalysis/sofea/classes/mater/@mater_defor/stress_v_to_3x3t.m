% Convert a stress vector to  3x3 matrix of stress components (tensor)
%
% function t = stress_v_to_3x3t (self,v)
%
% Convert a stress vector to a *symmetric*
% 3x3 matrix of stress components (tensor)
%
function t = stress_v_to_3x3t (self,v)
    error ('This is a pure virtual function and needs to be overridden');

