% Constructor Of linearly elastic isotropic materials.
%
% function retobj = mater_defor_ss_linel_uniax(varargin)
%
% This class represents deformable linearly elastic isotropic materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields
%    property = property object
%
function retobj = mater_defor_ss_linel_uniax(varargin)
    class_name='mater_defor_ss_linel_uniax';
    if (nargin == 0)
        parent=mater_defor;
        self.property = [];
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=mater_defor(options);
            self.property = options.property;
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
