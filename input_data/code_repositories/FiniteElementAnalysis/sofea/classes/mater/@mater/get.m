% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
if (nargin == 1)
    val = {};
    return;
else
    prop_name=varargin{1};
    switch prop_name
        otherwise
            val = get(self.classbase, prop_name);
    end
end
