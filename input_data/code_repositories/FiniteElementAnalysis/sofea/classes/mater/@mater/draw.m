% Draw a graphic representation of data at the integration point.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
%  with mandatory fields
%    xyz         - location at which the draw
%    ms          - material state
%    update_context     - context to be passed to the update () function.
%  with optional fields
%    quantity  - string: name of the quantity to display
%    component - index that says which component of the `quantity' quantity;
%                remember: the component points into the stress vector
%    data_cmap - data color map to use to color the quantity
%    es - transformation 3x3 matrix, with basis vectors in columns, that
%         describes the local Cartesian system for the material point
%
function draw (self, gv, context)
    error('Base class does not implement: method needs to be overridden');
    return;
end
