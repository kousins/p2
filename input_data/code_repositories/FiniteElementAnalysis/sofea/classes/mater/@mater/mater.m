% Constructor Of an abstract material class.
%
% function retobj = mater (varargin)
%
% This class represents materials. It is abstract.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retobj = mater (varargin)
    class_name='mater';
    parent=classbase;
    if (nargin == 0)
        retobj = class(struct([]),class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            retobj = class(struct([]),class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
