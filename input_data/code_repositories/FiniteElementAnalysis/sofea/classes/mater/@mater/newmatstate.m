% Create a new material state.
%
% function v = newmatstate (self)
%
%   Call as:
%     v = newmatstate(m)
%   where
%     m=an instance of a descendant of the class material
%
function v = newmatstate (self)
   error('Base class does not implemented: method needs to be overridden');
end
