% mater
%   This is a collection of classes implementing various
%   materials.
%
% -base class
%   @mater/mater - Constructor
%   @mater/update - Update material state.
%   @mater/newmatstate - Create the material state
%   @mater/get - Get an attribute
%   @mater/draw - Draw material quantity
% -class for heat conducting materials
%   @mater_diffusion/mater_diffusion - 
%   @mater_diffusion/update - Update material state.
%   @mater_diffusion/newmatstate - Create new material state
%   @mater_diffusion/get - Gets an attribute
%   @mater_diffusion/draw - Draw material quantity
% -base class for deformable materials
%   @mater_defor/mater_defor -  constructor
%   @mater_defor/get - And get an attribute
%   @mater_defor/set -  Sets and attribute
%   @mater_defor/tangent_moduli - Calculate the material stiffness matrix.
%   @mater_defor/stress_v_to_3x3t - Convert a stress vector to a *symmetric*
%                     3x3 matrix of stress components (tensor)
%   @mater_defor/stress_3x3t_to_6v - Convert a matrix of 3x3 stress components (tensor)
%                     into a 6-component vector.
%   @mater_defor/stress_6v_to_3x3t - convert a 6-vector to a *symmetric*
%                     matrix of 3x3 stress components (tensor)
%   @mater_defor/strain_3x3t_to_6v - convert a matrix of 3x3 strain components (tensor)
%                     into a 6-component vector.
%   @mater_defor/strain_6v_to_3x3t - convert a strain 6-vector to a *symmetric* 
%                     matrix of 3x3 strain components (tensor)
%   @mater_defor/stress_3v_to_3x3t - Convert a 3-vector to a *symmetric*
%                     matrix of 3x3 stress components (tensor)
%   @mater_defor/stress_3v_to_2x2t - Convert a 3-vector to a *symmetric*
%                     matrix of 2x2 stress components (tensor)
%   @mater_defor/strain_2x2t_to_3v - Convert a matrix of 2x2 strain components (tensor)
%                     into a 3-component vector.
%   @mater_defor/strain_3v_to_2x2t - Convert a strain 3-vector to a *symmetric*
%                     matrix of 2x2 strain components (tensor)
%   @mater_defor/stress_4v_to_3x3t - Convert a 4-vector to a *symmetric*
%                     matrix of 3x3 stress components (tensor). 
%   @mater_defor/stress_2x2t_to_3v - Convert a symmetric matrix of 2x2 stress components (tensor)
%                     into a 3-component vector.
%   @mater_defor/Lagrangean_to_Eulerian - Convert a Lagrangean constitutive
%                     matrix to an Eulerian one.
% -class for triaxial deformable material
%   @mater_defor_ss_linel_triax/mater_defor_ss_linel_triax - Constructor
%   @mater_defor_ss_linel_triax/update - Update material state.
%   @mater_defor_ss_linel_triax/thermal_stress - 
%   @mater_defor_ss_linel_triax/newmatstate - Create a new material state.
%   @mater_defor_ss_linel_triax/get - 
%   @mater_defor_ss_linel_triax/draw - Draw a graphic representation of
%                     data at the integration point.
%   @mater_defor_ss_linel_triax/tangent_moduli - Calculate the material
%                     stiffness matrix.
% -class for biaxial deformable material
%   @mater_defor_ss_linel_biax/mater_defor_ss_linel_biax - Constructor
%   @mater_defor_ss_linel_biax/update - Update material state.
%   @mater_defor_ss_linel_biax/thermal_stress - 
%   @mater_defor_ss_linel_biax/newmatstate - Create a new material state.
%   @mater_defor_ss_linel_biax/get - Get an attribute
%   @mater_defor_ss_linel_biax/draw - Draw a graphic representation of data
%                     at the integration point.
%   @mater_defor_ss_linel_biax/tangent_moduli - Calculate the material
%                     stiffness matrix.
% -class for uniaxial deformable material
%   @mater_defor_ss_linel_uniax/mater_defor_ss_linel_uniax - Constructor
%   @mater_defor_ss_linel_uniax/update - Update material state.
%   @mater_defor_ss_linel_uniax/thermal_stress - 
%   @mater_defor_ss_linel_uniax/newmatstate - Create a new material state.
%   @mater_defor_ss_linel_uniax/get - Get an attribute
%   @mater_defor_ss_linel_uniax/tangent_moduli - Calculate the material
%                     stiffness matrix.
