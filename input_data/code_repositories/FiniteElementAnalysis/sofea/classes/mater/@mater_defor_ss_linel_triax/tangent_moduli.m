% Calculate the material stiffness matrix.
%
% function D = tangent_moduli(self, context)
%
%   Call as:
%     D = tangent_moduli(m, context)
%   where
%     m=material
%    context=structure with options
%            interpreted by the property object
%
% the output arguments are
%     D=matrix 6x6
%
function D = tangent_moduli(self, context)
    D= get(self.property,'D', context);
end
