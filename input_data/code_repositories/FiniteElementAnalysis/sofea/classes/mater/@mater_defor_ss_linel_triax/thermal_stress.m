% Calculate vector of thermal stress components.
%
% function v = thermal_stress(self,context)
%
%   Call as:
%     v = thermal_stress(m,context)
%  where
%     m=material
%     context=structure; see the update() method
%
function v = thermal_stress(self,context)
if ( ~isempty(context.dT))
    D = tangent_moduli(self, context);
    alphas = get(self.property,'alphas');
    v =-D*context.dT*[alphas.*ones(3, 1); zeros(3, 1)];
else
    v=zeros(6, 1);
end
end
