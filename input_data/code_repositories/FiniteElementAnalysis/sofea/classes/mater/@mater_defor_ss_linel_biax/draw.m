% Draw a graphic representation of data at the integration point.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
%  with mandatory fields
%    xyz         - location at which the draw
%    ms          - material state
%    update_context     - context to be passed to the update () function.
%  with optional fields
%    quantity  - string: name of the quantity to display
%                 if quantity=='stress'
%                    the graphics representation is an ellipsoid of radii corresponding
%                    to the principal values of stress;
%                 otherwise
%                    the graphics representation is a sphere of radius corresponding
%                    to the value
%    component - index that says which component of the `quantity' quantity;
%                remember: the component points into the stress vector
%    data_cmap - data color map to use to color the quantity
%    es - transformation 3x3 matrix, with basis vectors in columns, that
%         describes the local Cartesian system for the material point
%
function draw (self, gv, context)
    quantity='stress';
    if (isfield(context,'quantity'))
        quantity= context.quantity;
    end
    scale = 1.0;
    if (isfield(context,'scale'))
        scale= context.scale;
    end
    switch (quantity)
        case 'stress'
            [sigmav,ignoreme] = update(self, context.ms, context.update_context);
            % sigmav=10*[10000; -100000; 200000];
            % make sure I get a 3 x 3 tensor
            if length (sigmav) ==3
                sigmav (end+1) =0;
            end
            sigma=stress_4v_to_3x3t(self,sigmav);
            if size(context.Rm)==[2 2]
                Rm=[context.Rm zeros(2,1); zeros(1,2) 1];
            else
                Rm=context.Rm;
            end
            sigma=Rm*sigma*Rm';
            [V,D]=eig(sigma);
            component =1;
            if isfield(context,'component')
                component = context.component;
            end
            color =[1 1 0];
            if isfield(context,'data_cmap')
                color = map_data(context.data_cmap, sigmav(component));
            end
            context.facecolor= color;
            draw_ellipsoid(gv, context.xyz, V, scale*diag(D), context);
        otherwise
            %  do nothing;
    end
    return;
end
