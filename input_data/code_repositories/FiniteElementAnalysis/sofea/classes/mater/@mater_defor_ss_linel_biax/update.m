% Update material state. 
%
% function [out, newms] = update (self, ms, context)
%
% Update material state.  Return the updated material state, and the
% requested quantity (default is the stress).
%   Call as:
%     [out,newms] = update(m, ms, context)
%  where
%     m=material
%     ms = material state
%     context=structure
%        with mandatory fields
%           strain = strain vector
%        and optional fields
%           output=type of quantity to output, and interpreted by the
%           particular material; [] is returned when the material does not
%           recognize the requested quantity to indicate uninitialized
%           value.  It can be tested with isempty ().
%              output ='Cauchy' - Cauchy stress; this is the default
%                      when output type is not specified.
%              output ='2ndPK' - 2nd Piola-Kirchhoff stress;
%           It is assumed that stress is output in 3-component vector form
%           for plane stress, and four-component vector form for plane strain.
%   The output arguments are
%     out=requested quantity
%     newms=new material state; don't forget that if the update is final
%           the material state newms must be assigned and stored.  Otherwise
%           the material update is lost!
%
function [out, newms] = update (self, ms, context)
    Ev = context.strain;
    D  = tangent_moduli (self, context);
    tSigma = thermal_stress(self,context);
    stress = D * Ev + tSigma;
    switch self.reduction
        case 'strain' % output through-the-thickness stress
            alphas = get(self.property,'alphas');
            D6x6= get(self.property,'D', context);
            sz=D6x6(3,1:2)*Ev(1:2)-context.dT*D6x6(3,1:2)*alphas(1:2)-D6x6(3,3)*context.dT*alphas(3);
%             C =inv(D6x6(1:3,1:3));
%             sz=(C(3,1:2)*stress(1:2) + context.dT*alphas(3))/C(3,3);
            stress = [stress;sz];
    end
    if isfield(context,'output')
        switch context.output
            case 'Cauchy'
                out = stress;
            case 'pressure'
                switch self.reduction
                    case 'stress'
                        out = -sum(stress(1:2))/3;
                    case 'strain'
                        out = -sum(stress([1,2,4]))/3;
                    otherwise
                        out = -sum(stress(1:3))/3;
                end
            case 'vol_strain'
                out = sum(Ev(1:2));%RAW this is incorrect for axi
            otherwise
                out = [];
        end
    else
        out = stress;
    end
    newms = ms;
    return;
end
