% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'property'}, {'property object'}}};
        val = cat(1, val, get(self.mater_defor));
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'property'
                val = self.property;
            otherwise
                val = get(self.mater_defor,varargin{:});
        end
    end
end
