% Calculate the material stiffness matrix.
%
% function D = tangent_moduli(self, context)
%
%   Call as:
%     D = tangent_moduli(m, context)
%   where
%     m=material
%    context=structure with optional field:
%            interpreted by the property object
%   The output arguments are
%     D=matrix 3X3 or 4x4
function D = tangent_moduli(self, context)
    D= get(self.property,'D', context);
    switch self.reduction
        case 'axisymm'
            D =D(1:4, 1:4);
        case 'strain'
            D =D([1, 2, 4],[1, 2, 4]);
        case 'stress'
            Dt =D(1:2, 1:2)-D(1:2,3)*D(3,1:2)/D(3,3);
            D =D([1, 2, 4],[1, 2, 4]);
            D(1:2, 1:2)= Dt;
        otherwise
            error([' Reduction ' self.reduction ' not recognized']);
    end
end
