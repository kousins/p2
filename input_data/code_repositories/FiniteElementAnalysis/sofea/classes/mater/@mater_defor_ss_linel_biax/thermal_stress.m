% Calculate vector of thermal stress components.
%
% function v = thermal_stress(self,context)
%
%   Call as:
%     v = thermal_stress(m,context)
%  where
%     m=material
%     context=structure; see the update() method
%
function v = thermal_stress(self,context)
    alphas = get(self.property,'alphas');
    switch self.reduction
        case 'axisymm'
            D = tangent_moduli(self, context);
            v = -D*context.dT*[alphas(1:3).*ones(3, 1); 0];
        case 'strain'
            D= get(self.property,'D', context);% need 3-D
            v = -context.dT*[D(1:2, 1:2)*(alphas(1:2).*ones(2,1))+...
                alphas(3)*D(1:2,3); 0];
        otherwise
            D = tangent_moduli(self, context);
            v = -D*context.dT*[alphas(1:2).*ones(2, 1); 0];
    end
end
