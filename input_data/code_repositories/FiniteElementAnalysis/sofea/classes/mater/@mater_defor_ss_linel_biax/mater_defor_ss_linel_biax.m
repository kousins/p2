% Constructor of linear elastic, material for biaxial strain/stress states.
%
% function retobj = mater_defor_ss_linel_biax (varargin)
%
% This class represents deformable materials, linear elastic,
% for biaxial strain/stress states.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields
%     property = property object
% optional field
%     reduction = type of stress state reduction, 'stress' (for plane stress), or 'strain'
%          (for plane strain), or 'axisymm' (for axially symmetric).
%
function retobj = mater_defor_ss_linel_biax (varargin)
    class_name='mater_defor_ss_linel_biax';
   if (nargin == 0)
        parent=mater_defor;
        self.property = [];
        self.reduction='strain';
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=mater_defor(options);
            self.property = options.property;
            self.reduction ='strain';
            if isfield(options,'reduction')
                self.reduction =options.reduction;
            end
            switch self.reduction
                case 'axisymm'
                case 'strain'
                case 'stress'
                otherwise
                    error([' Reduction ' self.reduction ' not recognized']);
            end
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
