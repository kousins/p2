% Update material state.  
%
% function [out, newms] = update (self, ms, context)
%
% Update material state.  Return the updated material state, and the
% requested quantity (default is the heat flux).
%   Call as:
%     [out,newms] = update(m, ms, context)
%  where
%     m=material
%     ms = material state
%     context=structure
%        with mandatory fields
%           gradtheta= temperature gradient
%        and optional fields
%           output=type of quantity to output, and interpreted by the
%           particular material; [] is returned when the material does not
%           recognize the requested quantity to indicate uninitialized
%           value.  It can be tested with isempty ().
%              output ='flux' - heat flux vector; this is the default
%                      when output type is not specified.
%   The output arguments are
%     out=requested quantity
%     newms=new material state; don't forget that if the update is final
%           the material state newms must be assigned and stored.  Otherwise
%           the material update is lost!
%
function [out, newms] = update (self, ms, context)
    Kappa = get(self.property,'conductivity');
    gradtheta=reshape (context.gradtheta, length (context.gradtheta),1);
    flux = - Kappa * gradtheta;
    if isfield(context,'output')
        switch context.output
            case 'flux'
                out = flux;
            otherwise
                out = [];
        end
    else
        out = flux;
    end
    newms = ms;
    return;
end
