% Constructor linear diffusion models for materials.
%
% function retobj = mater_diffusion (varargin)
%
% This class represents linear diffusion models in materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%     property=property object
%
function retobj = mater_diffusion (varargin)
    class_name='mater_diffusion';
    parent=mater;
    if (nargin == 0)
        self.property = [];
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            self.property = options.property;
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
