% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
if (nargin == 1)
    val = {{{'property'}, {'property object'}}};
    return;
else
    prop_name=varargin{1};
    switch prop_name
        case 'property'
            val = self.property;
        case 'conductivity'
            val = get(self.property,prop_name);
        case 'source'
            val = get(self.property,prop_name);
        case 'specific_heat'
            val = get(self.property,prop_name);
        otherwise
            val = get(self.mater, prop_name);
    end
end
