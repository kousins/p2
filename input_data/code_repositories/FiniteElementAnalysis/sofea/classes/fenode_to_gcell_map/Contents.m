% fenode_to_gcell_map
%   This class provides mapping of finite element nodes to the
%   geometric cells connecting those nodes.
%
%   @fenode_to_gcell_map/fenode_to_gcell_map - Constructor
%   @fenode_to_gcell_map/get - Get an attribute
