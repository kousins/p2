% Constructor of the map from finite element nodes to the geometric cells.
%
% function retobj = fenode_to_gcell_map (varargin)
%
% Constructor of the map from finite element nodes to the geometric cells
% that connect the nodes.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%    gcells =array  of geometric cells
%
function retobj = fenode_to_gcell_map (varargin)
class_name='fenode_to_gcell_map';
parent=classbase;
if (nargin == 0)
    self.gcell_map = {};
    self.fenodes = {};
    self = class(self, class_name, parent);
    retobj = self;
    return;
elseif (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) % copy constructor.
        retobj = arg;
        return;
    else
        options = varargin{1};
        self.gcell_map = {};
        self.fenodes = {};
        for i=1:length(options.gcells)
            conn=get(options.gcells(i),'conn');
            for j=1:length(conn)
                self.gcell_map{conn(j)}=[];
                self.fenodes{conn(j)}=conn(j);
            end
        end
        for i=1:length(options.gcells)
            conn=get(options.gcells(i),'conn');
            for j=1:length(conn)
                m=self.gcell_map{conn(j)};
                m=[m i];
                self.gcell_map{conn(j)}=m;
            end
        end
        self = class(self, class_name, parent);
        retobj = self;
        return;
    end
else
    error('Illegal arguments!');
    return;
end
return;

