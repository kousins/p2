% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
if (nargin == 1)
    val = {{{'gcell_map'}, {'map from finite element node (indexed by node ID) to the geometric cells, array [d,1]'}};
        };
    return;
else
    prop_name=varargin{1};
    switch prop_name
        case 'gcell_map'
            val = self.gcell_map;
        case 'fenodes'
            val = self.fenodes;
        otherwise
            error(['Unknown property name ''' prop_name '''!']);
    end
end

