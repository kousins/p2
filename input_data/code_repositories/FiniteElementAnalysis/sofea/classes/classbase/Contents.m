% classbase
%   This class is the base class of all SOFEA classes.
%   It provides the basic set, get, and display methods.
% 
%   @classbase/classbase - Constructor.
%   @classbase/get - Get an attribute.
%   @classbase/display - Display the object.
%   @classbase/set - Set an attribute.
