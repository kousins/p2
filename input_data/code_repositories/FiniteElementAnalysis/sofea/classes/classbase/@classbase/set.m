% Set property in the specified object.
%
% function retval = set(self,varargin)
% 
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
function retval = set(self,varargin)
    if (nargin == 1)
        retval={{{''}, {''}}};
        return;
    end
    prop_name=varargin{1};
    val=varargin{2};
    switch prop_name
        otherwise
            error(['Unknown property ''' prop_name '''!']);
    end
    retval=self; % return self
end
