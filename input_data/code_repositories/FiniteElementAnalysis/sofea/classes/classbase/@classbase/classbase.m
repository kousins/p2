% Constructor of the base class.
%
% function retobj = classbase (varargin)
% 
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: none
function retobj = classbase (varargin)
    class_name='classbase';
    if (nargin == 0)
        self. Something = 0;% must have a field: no field causes Matlab crash
        self = class(self, class_name);
        retobj = self;
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            error(['Illegal argument ' inputname(1) ' of class ' class(varargin{1}) '!']);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
