% Display object to screen.
%
% function display (self)
%
function display (self)
    disp(' ');
    disp([inputname(1),' = '])
    if (length(self)==1)
        disp(['   ' class(self) ' object:']);
        disp('=== Gettable properties: =======================');
        props=get(self);
        for i=1:length (props)
            disp([char(props{i}{1}) '   -   ' char(props{i}{2})]);
        end
        disp('=== Settable properties: =======================');
        props=set(self);
        for i=1:length (props)
            disp([char(props{i}{1}) '   -   ' char(props{i}{2})]);
        end
    else
        s=size(self);
        b=num2str(s(1));
        for j= 2:length(s)
            b=[b '-by-' num2str(s(j))];
        end
        disp(['   ' class(self) ' object: ' b]);
    end
end
