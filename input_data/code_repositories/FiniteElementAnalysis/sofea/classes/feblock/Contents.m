% feblock
%   This tree of classes provides operations on block of finite elements.
% 
% - feblock is the base class, from which specialized blocks may
%   be derived to implement or override behaviour.
%
%   @feblock/measure - Integrate over the block cells.
%   @feblock/draw_mesh - Draw the mesh.
%   @feblock/feblock - Constructor.
%   @feblock/get - Get an attribute
%   @feblock/draw - Draw the cells.
%   @feblock/set - Set an attribute.
%   @feblock/material_directions - Compute orientation matrix.
%
% - feblock_defor_ss implements the small strain, small
%   displacement deformation behavior.
%
%   @feblock_defor_ss/draw_integration_points - Draw quantities at
%                                       integration points.
%   @feblock_defor_ss/stiffness - Compute cell array of element stiffness
%                                       matrices.
%   @feblock_defor_ss/distrib_loads - Compute cell array of element vectors
%                     of distributed loads.
%   @feblock_defor_ss/get - Gets an attribute.
%   @feblock_defor_ss/inspect_integration_points - Inspect the integration
%                     point quantities.
%   @feblock_defor_ss/nz_ebc_loads - Compute cell array of element vectors
%                     of loads due to nonzero boundary conditions.
%   @feblock_defor_ss/field_from_integration_points - Create a field from
%                     quantities at integration points.
%   @feblock_defor_ss/thermal_strain_loads - Compute cell array of element vectors
%                     of thermal loads.
%   @feblock_defor_ss/mass - Compute cell array of element  mass
%                                       matrices.
%   @feblock_defor_ss/feblock_defor_ss -  Constructor.
% 
% - feblock_defor_ss_sri implements the small strain, small
%   displacement deformation behavior, with selective reduce
%   integration (SRI). 
% 
%   @feblock_defor_ss_sri/stiffness - 
%   @feblock_defor_ss_sri/feblock_defor_ss_sri - Constructor.
%   @feblock_defor_ss_sri/get - Get an attribute 
% 
% - feblock_diffusion implements the behavior for thermal
%   analysis.
% 
%   @feblock_diffusion/nz_ebc_loads_conductivity - Compute cell array of element vectors
%                     of loads due to nonzero boundary conditions,
%                     contribution of the conductivity matrix.
%   @feblock_diffusion/source_loads - Compute cell array of element vectors
%                     of loads due to internal heat sources and sinks.
%   @feblock_diffusion/draw_integration_points - 
%   @feblock_diffusion/feblock_diffusion - Constructor
%   @feblock_diffusion/nz_ebc_loads_capacity - Compute cell array of element vectors
%                     of loads due to nonzero boundary conditions,
%                     contribution of the capacity matrix.
%   @feblock_diffusion/flux_loads - Compute cell array of element vectors
%                     of loads due to boundary flux.
%   @feblock_diffusion/get - Get an attribute
%   @feblock_diffusion/conductivity - Compute cell array of element
%                     conductivity   matrices.
%   @feblock_diffusion/capacity - Compute cell array of element
%                      capacity   matrices.
%   @feblock_diffusion/surface_transfer - Compute cell array of element
%                     surface-transfer conductivity   matrices.
%   @feblock_diffusion/surface_transfer_loads - Compute cell array of element vectors
%                     of loads due to surface transfer flux
%   @feblock_diffusion/nz_ebc_loads_surface_transfer - Compute cell array of element vectors
%                     of loads due to nonzero boundary conditions,
%                     contribution of the  Surface-transfer matrix.
% 
% - feblock_defor_taut_wire is a toy block, used only as a
%   demonstration in the first part of the finite element book.
% 
%   @feblock_defor_taut_wire/stiffness - Compute cell array of element stiffness
%                                       matrices.
%   @feblock_defor_taut_wire/get - Get an attribute
%   @feblock_defor_taut_wire/nz_ebc_loads - Compute cell array of element vectors
%                     of loads due to nonzero boundary conditions.
%   @feblock_defor_taut_wire/feblock_defor_taut_wire - Constructor
%   @feblock_defor_taut_wire/mass - Compute cell array of element  mass
%                                       matrices.
%   @feblock_defor_taut_wire/body_loads - Compute cell array of element vectors
%                     of loads due to  Distributed transversal load.

 
