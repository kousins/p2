%  Draw graphic representation for all gcells where the Jacobian at at
%  least one integration point falls below a certain value (zero by default).
%
% function draw_mesh_quality(self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with  mandatory fields
%    x=reference geometry field
% and with optional fields
%    facecolor = color of the faces, solid
%    shrink = shrink factor
% and with any others that a particular implementation of a gcell might
% require or recognize.
%
% Call as for instance:
% gv=reset (graphic_viewer,[]);
% draw_mesh_quality(feb, gv, struct ('x', geom,'u',u,'facecolor','red'))
% 
%
function draw_mesh_quality(self, gv, context)
    geom= context.x;
    gcells =self.gcells;
    ngcells = length(self.gcells);
    mindetJ = 0;
    % Integration rule
    integration_rule = get(self, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape');
        % Loop over all integration points
        for j=1:npts_per_gcell
            detJ = Jacobian(gcells(i),pc(j,:),x);
            if (detJ<=mindetJ)
                draw(gcells(i), gv, context);
            end
        end
    end
    return;
end
