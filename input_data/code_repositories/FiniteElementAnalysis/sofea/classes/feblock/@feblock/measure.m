% Measure a function over the discrete manifold. 
%
% function result = measure (self, geom, fh, varargin)
%
% Integrate some scalar function over the geometric cells. When the scalar
% function returns just +1 [measure(feb,geom,inline('1'))], the result
% measures the volume (number of points, length, area, 3-D volume,
% according to the manifold dimension). When the function returns the mass
% density, the method measures the mass, when the function returns the
% x-coordinate equal measure the static moment with respect to the y- axis,
% and so on.  
% Arguments: self = feblock, geom = geometry field, fh   = function handle
% varargin= optional: manifold dimension to be supplied to Jacobian_mdim().
function result = measure (self, geom, fh, varargin)
    gcells =self.gcells;
    ngcells = length(self.gcells);
    % Integration rule
    integration_rule = get(self, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    if nargin >=4
        m =varargin{1};
    else
        m= get(gcells(1),'dim');
    end
    result = 0;
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape');
        % Loop over all integration points
        for j=1:npts_per_gcell
            detJ = Jacobian_mdim(gcells(i),pc(j,:),x,m);
            N = bfun(gcells(i), pc(j,:));
            result = result + fh(N'*x)*detJ*w(j);
        end
    end
end
