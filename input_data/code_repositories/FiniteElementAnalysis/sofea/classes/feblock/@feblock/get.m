% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'mater'}, {'material object'}};
            {{'gcells'}, {'geometric cells, array of descendants of the gcell class'}};
            {{'integration_rule'}, {'integration rule, object'}};
            {{'Rm'}, {'transformation matrix, local material directions in columns'}}};
        return;
    else
        %  disp('looking for feblock.mater');
        prop_name=varargin{1};
        %  self.mater
        switch prop_name
            case 'mater'
                val = self.mater;
            case 'gcells'
                val = self.gcells;
            case 'integration_rule'
                val = self.integration_rule;
            case 'Rm'
                val = self.Rm;
%             case 'feblock'
%                 val = self;
            otherwise
                error(['Unknown property name ''' prop_name '''!']);
        end
    end
end
