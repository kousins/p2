% Compute the lumped (diagonal) versions of element wise matrices of the
% individual gcells. The Hinton, Rock, Zienkiewicz lumping method is used.
%
% function ems = lumped_hrz(self,ems)
%
% Return an array of them so they may be assembled.
%    Call as
%       ems = lumped_hrz(self,ems)
%    where
%       ems=element wise matrices
% 
function ems = lumped_hrz(self,ems)
    for j=1:length(ems)% Hinton, Rock, Zienkiewicz lumping
        Me=get(ems(j),'mat');
        em2=sum(sum(Me));
        dem2=sum(diag(Me));
        ems(j)=set(ems(j),'mat',diag(diag(Me)/dem2*em2));
    end
end
