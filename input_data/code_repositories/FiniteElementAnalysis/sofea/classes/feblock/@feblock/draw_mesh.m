%  Draw graphic representation for all gcells.
%
% function draw(self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with typically mandatory fields
%    x=reference geometry field
%    u=displacement field
% and with optional fields
%    facecolor = color of the faces, solid
%    colorfield =field with vertex colors
%       only one of the facecolor and colorfield may be supplied
%    shrink = shrink factor
% and with any others that a particular implementation of a gcell might
% require or recognize.
%
% Call as: draw(hotfeb, gv, struct ('x',geom, 'u',100*geom,...
%             'colorfield',colorfield, 'shrink',1));
%
function draw(self, gv, context)
    for i=1:length (self.gcells)
        draw(self.gcells(i), gv, context);
    end
    return;
end
