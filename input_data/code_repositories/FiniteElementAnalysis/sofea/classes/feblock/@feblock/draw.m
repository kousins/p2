%  Draw graphic representation for all gcells.
%
% function draw(self, gv, context)
%
%
% See the function draw_mesh()
%
function draw(self, gv, context)
    draw_mesh(self, gv, context);
    return;
end
