% Measure a field function over the discrete manifold. 
%
% function result = measure_field (self, geom, a_field, fh, varargin)
%
% Integrate given function Of a given field over the geometric cells. 
% 
% Arguments: 
% self = feblock, geom = geometry field, a_field = an arbitrary field,
% fh   = function handle, function f(Location,Field_value_at_location)
% varargin= optional: manifold dimension to be supplied to Jacobian_mdim().
function result = measure_field (self, geom, a_field, fh, varargin)
    gcells =self.gcells;
    ngcells = length(self.gcells);
    % Integration rule
    integration_rule = get(self, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    if nargin >=5
        m =varargin{1};
    else
        m= get(gcells(1),'dim');
    end
    result = [];
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape');
        V = gather(a_field, conn, 'values', 'noreshape');
        % Loop over all integration points
        for j=1:npts_per_gcell
            detJ = Jacobian_mdim(gcells(i),pc(j,:),x,m);
            N = bfun(gcells(i), pc(j,:));
            if (isempty(result))
                result =fh(N'*x,N'*V)*detJ*w(j);
            else
                result = result + fh(N'*x,N'*V)*detJ*w(j);
            end
        end
    end
end
