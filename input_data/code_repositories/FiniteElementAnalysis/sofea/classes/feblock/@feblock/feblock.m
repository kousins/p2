% Constructor of the finite elements block class.
%
% function retobj = feblock (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%       mandatory:
% gcells = cell array of geometry cells,
%       optional:
% mater =material,
% integration_rule= integration rule object
% Rm= transformation matrix, in columns are the basis vectors expressed in
%   the global Cartesian basis.
%
function retobj = feblock (varargin)
    class_name='feblock';
    parent=classbase;
    if (nargin == 0)
        self.mater = [];
        self.gcells = [];
        self.integration_rule = [];
        self.Rm=[];
        retobj = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            self.mater = [];
            self.gcells = [];
            self.integration_rule = [];
            self.Rm=[];
            if isfield(options,'mater')
                self.mater = options.mater;
            end
            self.gcells = options.gcells;
            if (length(self.gcells) < 1)
                error('Need at least one gcell!');
            end
            for i=1:length(self.gcells)
                if (~isa(self.gcells(i),class(self.gcells(1))))
                    error ('All gcells must be of the same class!');
                end
            end
            if isfield(options,'integration_rule')
                self.integration_rule = options.integration_rule;
            end
            Rm=[];
            if isfield(options,'Rm')
                Rm= options.Rm;
                if strcmp(class(Rm),'double')
                    if norm(Rm'*Rm-eye(size(Rm, 2)))>1e-9
                        error('Non-orthogonal local basis matrix!');
                    end
                elseif strcmp(class(Rm),'function_handle')
                else
                    error(['Cannot handle class of Rm: ' class(Rm)])
                end
            end
            self.Rm=Rm;
            retobj = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end

