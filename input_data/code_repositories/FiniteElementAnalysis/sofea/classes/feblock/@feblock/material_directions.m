% Compute transformation matrix describing local material directions.
%
% function Rmout = material_directions(self,gcell,pc,x)
%
%      self=finite element block
%      gcell = Geometric cell
%      pc=parametric coordinates
%      x=array of spatial coordinates of the nodes, size(x) = [nbfuns,dim]
%
% Local data structures:
% XYZ = location at which the material directions are needed
% ts = tangent vectors to parametric coordinates in columns
function Rmout = material_directions(self,gcell,pc,x)
    Rm=get(self,'Rm');
    if isempty(Rm)
        Rmout=eye(size(x,2));
    else
        if strcmp(class(Rm),'double')
            Rmout =Rm;
        elseif strcmp(class(Rm),'function_handle')
            N=bfun(gcell,pc);
            Nder=bfundpar(gcell,pc);
            Rmout= feval(Rm, N'*x, x'*Nder);
        end
    end
    return;
end
