% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'split'}, {'which volumetric/deviatotic split should be used? lambda, or bulk'}};
            };
        val = cat(1, get(self.feblock_defor_ss), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'split'
                val = self.split;
            otherwise
                val = get(self.feblock_defor_ss, prop_name);
        end
    end
end
