% Constructor of the finite element block for selective reduce integration.
%
% function retobj = feblock_defor_ss_sri (varargin)
%
% Constructor of the finite element block class to be used with small
% displacement, small strain    deformation mechanics for continuum,
% isoparametric elements. Descendent of feblock_defor_ss.
%
% Note: Selective reduced integration is used; see Hughes (1987).
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: those recognized by feblock Plus
% split = volumetric/deviatoric split type: 'lambda' or 'bulk'
%
function retobj = feblock_defor_ss_sri (varargin)
    class_name='feblock_defor_ss_sri';
    if (nargin == 0)
        parent = feblock_defor_ss;
        self.split ='lambda';
        retobj = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent = feblock_defor_ss(varargin{:});
            self.split ='lambda';
            if isfield(options,'split')
                self.split = options.split;
            end
            if ~(strcmp(self.split,'lambda') | ...
                    strcmp(self.split,'bulk'))
                error ('Unknown split')
            end
            retobj = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
