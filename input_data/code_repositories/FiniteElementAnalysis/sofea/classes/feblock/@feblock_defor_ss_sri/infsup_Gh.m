% Compute the stiffness matrices of the individual gcells.
%
% function ems = stiffness (self, geom, u)
%
% Return an array of the element matrices so they may be assembled.
%     geom=geometry field
%     u=displacement field
function ems = infsup_Gh (self, geom, u)
    gcells = get(self,'gcells');
    nfens = get(gcells(1),'nfens');
    ems(1:length(gcells)) = deal(elemat);% Pre-allocate
    % Integration rule
    integration_rule = get(self, 'integration_rule');
    % - volumetric terms
    pc_v = get(integration_rule, 'param_coords',1);
    w_v  = get(integration_rule, 'weights',1);
    n_v = get(integration_rule, 'npts',1);
    pc=pc_v; w=w_v; npts_per_gcell=n_v;
    % - shear terms
    pc_s = get(integration_rule, 'param_coords',2);
    w_s  = get(integration_rule, 'weights',2);
    n_s = get(integration_rule, 'npts',2);
    Ke = zeros(get(geom,'dim')*nfens); % preallocate
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Ke = 0*Ke; % zero out element stiffness
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun(gcells(i),pc(j,:));
            Rm = material_directions(self,gcells(i),pc(j,:),x);
            Nder = bfundpar (gcells(i),pc(j,:));
            Ndersp=bfundsp(gcells(i),Nder,x*Rm);
            detJ = Jacobian_volume(gcells(i),pc(j,:),x);
            B = divmat(gcells(i), pc(j,:), x, Rm);
            Ke = Ke + B'*B * detJ * w(j);
        end
        ems(i) = set(ems(i), 'mat', Ke);
        ems(i) = set(ems(i), 'eqnums', gather(u,conn,'eqnums'));
    end
    return;
end
