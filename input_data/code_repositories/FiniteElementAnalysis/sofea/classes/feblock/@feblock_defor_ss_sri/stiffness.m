% Compute the stiffness matrices of the individual gcells.
%
% function ems = stiffness (self, geom, u)
%
% Return an array of them so they may be assembled.
%    Call as
% ems = stiffness(feb, geom, u)
%     geom=geometry field
%     u=displacement field
%
function ems = stiffness (self, geom, u)
    gcells = get(self,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    % Pre-allocate the element matrices
    ems(1:ngcells) = deal(elemat);
    % Integration rule
    integration_rule = get(self, 'integration_rule');
    % - volumetric terms
    pc_v = get(integration_rule, 'param_coords',1);
    w_v  = get(integration_rule, 'weights',1);
    n_v = get(integration_rule, 'npts',1);
    % - shear terms
    pc_s = get(integration_rule, 'param_coords',2);
    w_s  = get(integration_rule, 'weights',2);
    n_s = get(integration_rule, 'npts',2);
    % Material
    mat = get(self, 'mater');
    % Loop over all the geometry cells
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Ke = zeros(dim*nfens); % element stiffness matrix
        % First the lambda-proportional stiffness
        for j=1:n_v
            Rm=material_directions(self,gcells(i),pc_v(j,:),x);
            detJ= Jacobian_volume(gcells(i),pc_v(j,:),x);
            B = Blmat(gcells(i), pc_v(j,:), x, Rm);
            D = tangent_moduli (mat,struct('kind',self.split));
            Ke = Ke + B'*D*B * detJ * w_v(j);
        end
        % and then the shear-proportional stiffness
        for j=1:n_s
            Rm=material_directions(self,gcells(i),pc_s(j,:),x);
            detJ= Jacobian_volume(gcells(i),pc_s(j,:),x);
            B = Blmat(gcells(i), pc_s(j,:), x, Rm);
            D = tangent_moduli (mat, ...
                struct('kind',[self.split '_shear']));
            Ke = Ke + B'*D*B * detJ * w_s(j);
        end
        ems(i) = set(ems(i), 'mat', Ke);
        ems(i) = set(ems(i), 'eqnums',gather(u,conn,'eqnums'));
    end
    return;
end
