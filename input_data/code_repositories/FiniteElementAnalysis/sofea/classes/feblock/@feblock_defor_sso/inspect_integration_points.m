% Inspect the integration point quantities.
%
% function idat = inspect_integration_points(self, ...
%         geom, u, dT, gcell_list, context,...
%         inspector, idat)
%
% Input arguments
%    geom - reference geometry field
%    u - displacement field
%    dT - temperature difference field
%    gcell_list - indexes of the geometric cells that are to be inspected:
%          The gcells to be included are: gcells(gcell_list).
%    context    - structure: see the update() method.
%    inspector - function handle or in-line function with the signature
%             idat =inspector(idat, out, xyz, pc),
%        where
%     idat - a structure or an array that the inspector may
%                use to maintain some state, for instance minimum or
%                maximum of stress, out is the output  of the update()
%                method, and xyz is the location of the integration point
%                in the *reference* configuration. The argument pc are the
%                parametric coordinates of the quadrature point.
% Output arguments
%     idat - see the description of the input argument
%
function idat = inspect_integration_points(self, ...
        geom, u, dT, gcell_list, context,...
        inspector, idat)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self.feblock, 'mater');
    matstates = get(self, 'matstates');
    context.strain= [];
    % Now loop over all gcells in the block
    for m=1:length(gcell_list)
        i=gcell_list(m);
        conn = get(gcells(i), 'conn'); % connectivity
        X = gather(geom, conn, 'values', 'noreshape'); % coord
        U = gather(u, conn, 'values'); % displ
        if isempty(dT)
            dTs=zeros(nfens,1);
        else
            dTs=gather(dT, conn, 'values', 'noreshape');
        end
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun (gcells(i), pc(j,:));
            Rm=material_directions(self,gcells(i),pc(j,:),X);
            B = Blmat(gcells(i), pc(j,:), X, Rm);
            context.strain = B*U;
            context.dT = dTs'*N;
            context.xyz =N'*X;
            [out,ignore] = update(mat, matstates{i,j}, context);
            if ~isempty (inspector)
                idat =feval(inspector,idat,out,N'*X,pc(j,:));
            end
        end
    end
    return;
end
