%  Draw graphic representation for material states in all gcells.
%
% function draw_integration_points(self, gv, context)
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with typically mandatory fields
%    x=reference geometry field
%    u=displacement field to be used for the calculation of the stresses
%    u_displ=displacement field to be used for display
%    dT=temperature difference field
% with optional fields
%    scale = scale the representation of the material state graphic by this
%       factor
%    dT=temperature difference (with respect to the reference temperature);
%       temperature differences assumed to be zero if it is not supplied.
% Any other fields in the context struct may be interpreted by the
% implementation of the material: see the draw() function for the
% particular material.
%
function draw_integration_points(self, gv, context)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(context.x,'dim');
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts'); % number of integration point
    % Material
    mat = get(self.feblock, 'mater');
    % Now loop over all gcells in the block
    context.update_context= [];
    context.ms = [];
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        X = gather(context.x, conn, 'values', 'noreshape'); % reference coordinates
        U_displ = gather(context.u_displ, conn, 'values', 'noreshape'); % displacement
        Uv = gather(context.u, conn, 'values'); % displacement
        if isfield(context,'dT')
            dTs = gather(context.dT, conn, 'values', 'noreshape'); % displacement
        else
            dTs = zeros(nfens,1);
        end
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun (gcells(i), pc(j,:));
            Nder = bfundpar (gcells(i), pc(j,:));
            Rm=material_directions(self,gcells(i),pc(j,:),X);
            NderX = bfundsp (gcells(i), Nder,X*Rm);
            context.xyz =N'*(X+U_displ);
            context.Rm=Rm;
            B = Blmat(gcells(i), pc(j,:), X, Rm);
            context.update_context.strain = B*Uv;
            context.update_context.dT = dTs'*N;
           draw(mat, gv, context);
        end
    end
    return;
end
