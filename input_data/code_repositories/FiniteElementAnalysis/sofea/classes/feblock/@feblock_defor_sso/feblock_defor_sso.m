% Constructor of block for small displacement, small strain deformation.
%
% function self = feblock_defor_sso (varargin)
%
% Constructor of the finite element block class to be used with small
% displacement, small strain    deformation mechanics for continuum,
% isoparametric elements.
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: those recognized by feblock
%
function self = feblock_defor_sso (varargin)
    class_name='feblock_defor_sso';
    if (nargin == 0)
        parent = feblock;
        self.matstates= [];
        self.hBlmat= [];
        self = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            parent = feblock(varargin{:});
            self.matstates = [];
            self.hBlmat= [];
            self = class(self,class_name,parent);
            integration_rule = get(self.feblock, 'integration_rule');
            npts_per_gcell = get(integration_rule, 'npts_per_gcell');
            gcells = get(self.feblock,'gcells');
            ngcells = length(gcells);
            self.matstates = cell(get(gcells(1),'nfens')*ngcells,npts_per_gcell);
            mat = get(self.feblock, 'mater');
            for i=1:ngcells
                for j=1:npts_per_gcell;
                    self.matstates{i,j} = newmatstate(mat);
                end
            end
            gcells = get(self,'gcells');
            dim= get(gcells(1),'dim');
            switch dim
                case 1
                    self.hBlmat=@feblock_defor_ss_Blmat1;
                case 2
                    if get(gcells(1),'axisymm')
                        self.hBlmat=@feblock_defor_ss_Blmat2axisymm;
                    else
                        self.hBlmat=@feblock_defor_ss_Blmat2;
                    end
                case 3
                    self.hBlmat=@feblock_defor_ss_Blmat3;
                otherwise
                    error (' Not implemented');
            end
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end

