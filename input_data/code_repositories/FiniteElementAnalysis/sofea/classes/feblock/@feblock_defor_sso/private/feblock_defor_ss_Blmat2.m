% Compute the strain-displacement matrix for a two-manifold element.
%
% B = feblock_defor_ss_Blmat2(self,Ndersp,c,Rm)
%
% Compute the linear, displacement independent, strain-displacement matrix
% for a two-manifold element.
%    
%   Arguments
%      self=gcell
%      Ndersp=matrix of basis function gradients
%      c=array of spatial coordinates of the evaluation point
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
% Output: B = strain-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of strains, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
% 
function B = feblock_defor_ss_Blmat2(self,Ndersp,c,Rm)
    nfn= size(Ndersp,1);
    dim =size(Ndersp,2);
    B = zeros(3,nfn*dim);
    for i= 1:nfn
        B(:,2*(i-1)+1:2*i)=...
            [Ndersp(i,1) 0; ...
            0           Ndersp(i,2); ...
            Ndersp(i,2) Ndersp(i,1) ]*Rm(:,1:2)';
    end
    return;
end
