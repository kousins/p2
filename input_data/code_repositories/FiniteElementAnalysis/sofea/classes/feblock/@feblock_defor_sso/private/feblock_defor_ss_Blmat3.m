% Compute the strain-displacement matrix for a three-manifold element.
%
% B = feblock_defor_ss_Blmat3(self,Ndersp,c,Rm)
%
% Compute the linear, displacement independent, strain-displacement matrix
% for a three-manifold element.
%    
%   Arguments
%      self=gcell
%      Ndersp=matrix of basis function gradients
%      c=array of spatial coordinates of the evaluation point
%      Rm=orthogonal matrix with the unit basis vectors of the local
%         coordinate system as columns.
%         size(Rm)= [ndim,ndim], where ndim = Number of spatial dimensions
%         of the embedding space.
% Output: B = strain-displacement matrix, size (B) = [nstrain,nfens*dim],
%      where nstrain= number of strains, dim = Number of spatial dimensions of
%      the embedding space, and nfens = number of finite element nodes on
%      the element.
% 
function B = feblock_defor_ss_Blmat3(self,Ndersp,c,Rm)
    nfn= size(Ndersp,1);
    %dim =size(Ndersp,2);
    B = zeros(6,nfn*3);
%     for i= 1:nfn
%         B(:,3*(i-1)+1:3*i)...
%             = [ Ndersp(i,1) 0           0  ; ...
%             0           Ndersp(i,2) 0  ; ...
%             0           0           Ndersp(i,3) ; ...
%             Ndersp(i,2) Ndersp(i,1) 0  ; ...
%             Ndersp(i,3) 0           Ndersp(i,1) ; ...
%             0           Ndersp(i,3) Ndersp(i,2) ]*Rm';
%     end
B(1,1:3:end) =Ndersp(:,1);
B(2,2:3:end) =Ndersp(:,2);
B(3,3:3:end) =Ndersp(:,3);
B(4,1:3:end) =Ndersp(:,2);
B(4,2:3:end) =Ndersp(:,1);
B(5,1:3:end) =Ndersp(:,3);
B(5,3:3:end) =Ndersp(:,1);
B(6,2:3:end) =Ndersp(:,3);
B(6,3:3:end) =Ndersp(:,2);    
    return;
end



