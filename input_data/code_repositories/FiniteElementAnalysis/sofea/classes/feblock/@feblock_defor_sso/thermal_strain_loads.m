% Compute the thermal-strain load vectors of the individual biaxial gcells.
%
% function evs = thermal_strain_loads(self, geom, u, dT)
%
% Return an array of the element vectors so they may be assembled.
%    Call as
% ems = thermal_strain_loads(feb, geom, u, T)
%     geom=geometry field
%     u=displacement field
%     dT=temperature difference field (temperature at a point minus the
%     reference temperature at which the solid experiences no strains)
%
function evs = thermal_strain_loads(self, geom, u, dT)
    gcells = get(self,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    % Pre-allocate the element matrices
    evs(1:ngcells) = deal(elevec);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self, 'mater');
    matstates = get(self, 'matstates');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coordinates of nodes
        us = gather(u, conn, 'values'); % displacements
        dTs = gather(dT, conn, 'values'); % temperatures
        Fe = zeros(dim*nfens,1); % element load vector
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun(gcells(i),pc(j,:));
            Rm = material_directions(self,gcells(i),pc(j,:),x);
            detJ = Jacobian_volume(gcells(i),pc(j,:),x);
            B = Blmat(gcells(i), pc(j,:), x, Rm);
            context.dT =N'*dTs; context.xyz =N'*x;
            tSigma = thermal_stress(mat,context);
            Fe = Fe - B'*tSigma * detJ * w(j);
        end
        evs(i) = set(evs(i), 'vec', Fe);
        evs(i) = set(evs(i), 'eqnums', gather(u, conn, 'eqnums'));
    end
    return;
end
