% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'matstates'}, {'array of material states'}};
            {{'Blmat'}, {'strain-displacement matrix, handle'}};
            };
        val = cat(1, get(self.feblock), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'matstates'
                val=self.matstates;
            case 'Blmat'
                val=self.hBlmat;
            otherwise
                val = get(self.feblock, prop_name);
        end
    end
end
