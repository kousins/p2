% Compute the stiffness matrices of the individual gcells.
%
% function ems = infsup_Sh (self, geom, u)
%
% Return an array of the element matrices so they may be assembled.
%     geom=geometry field
%     u=displacement field
function ems = infsup_Sh (self, geom, u)
    gcells = get(self.feblock,'gcells');
    nfens = get(gcells(1),'nfens');
    ems(1:length(gcells)) = deal(elemat);% Pre-allocate
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self.feblock, 'mater');
    Ke = zeros(get(geom,'dim')*nfens); % preallocate
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Ke = 0*Ke; % zero out element stiffness
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun(gcells(i),pc(j,:));
            Rm = material_directions(self,gcells(i),pc(j,:),x);
            Nder = bfundpar (gcells(i),pc(j,:));
            Ndersp=bfundsp(gcells(i),Nder,x*Rm);
            detJ = Jacobian_volume(gcells(i),pc(j,:),x);
            DS=vgradmat(gcells(i),pc(j,:),x,Rm);
            Ke = Ke + DS'*DS * detJ * w(j);
        end
        ems(i) = set(ems(i), 'mat', Ke);
        ems(i) = set(ems(i), 'eqnums', gather(u,conn,'eqnums'));
    end
    return;
end
