% Compute the element load vectors corresponding to surface heat transfer.
%
% function evs = surface_transfer_loads (self, geom, temp, amb)
%
% Return an array of the element vectors so they may be assembled.
%     geom=geometry field
%     temp=temperature field
%     amb = ambient temperature field
function evs = surface_transfer_loads (self, geom, temp, amb)
    ems = surface_transfer(self, geom, temp);
    gcells = get(self.feblock,'gcells');
    % Pre-allocate the element matrices
    evs(1:length(gcells)) = deal(elevec);
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        pT = gather(amb, conn, 'prescribed_values');
        if norm (pT) ~= 0
            He =  get(ems(i),'mat'); % element matrix
            evs(i) = set(evs(i), 'vec', He*pT);
            evs(i) = set(evs(i), 'eqnums', ...
                gather(temp, conn, 'eqnums'));
        end
    end
    return;
end
