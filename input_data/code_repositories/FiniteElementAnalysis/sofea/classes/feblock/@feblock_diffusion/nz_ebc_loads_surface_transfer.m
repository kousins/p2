% Compute the element load vectors corresponding to nonzero
%
% function evs = nz_ebc_loads_surface_transfer(self, geom, temp)
%
% essential boundary
% conditions (prescribed temperature).
% Return an array of them so they may be assembled.
%    Call as
% evs = nz_ebc_loads_surface_transfer(feb, geom, temp)
%     geom=geometry field
%     temp=temperature field
%
function evs = nz_ebc_loads_surface_transfer(self, geom, temp)
    ems = surface_transfer(self, geom, temp);
    gcells = get(self,'gcells');
    % Pre-allocate the element matrices
    evs(1:length(gcells)) = deal(elevec);
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        pT = gather(temp, conn, 'prescribed_values');
        if norm (pT) ~= 0
            He = get(ems(i),'mat'); % element matrix
            evs(i) = set(evs(i), 'vec', -He*pT);
            evs(i) = set(evs(i), 'eqnums', ...
                gather(temp, conn, 'eqnums'));
        end
    end
    return;
end
