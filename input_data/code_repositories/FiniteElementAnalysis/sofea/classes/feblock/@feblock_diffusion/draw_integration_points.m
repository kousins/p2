%  Draw graphic representation for material states in all gcells.
%
% function draw_integration_points(self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with typically mandatory fields
%    x=reference geometry field
%    theta=temperature field
% with optional fields
%    scale = scale the representation of the material state graphic by this
%       factor
% Any other fields in the context struct may be interpreted by the
% implementation of the material: see the draw() function for the
% particular material.
%
function draw_integration_points(self, gv, context)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(context.x,'dim');
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts'); % number of integration point
    % Material
    mat = get(self.feblock, 'mater');
    matstates = get(self, 'matstates');
    % Now loop over all gcells in the block
    context.update_context= [];
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        X = gather(context.x, conn, 'values', 'noreshape'); % reference coordinates
        T = gather(context.theta, conn, 'values', 'noreshape'); % displacement
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun(gcells(i), pc(j,:));
            Nder = bfundpar (gcells(i), pc(j,:));
            NderX = bfundsp (gcells(i), Nder, X);
            context.ms=matstates{i,j};
            context.xyz=N'*X;
            context.update_context.gradtheta = grad(gcells(i), NderX, T);
            draw(mat, gv, context);
        end
    end
    return;
end
