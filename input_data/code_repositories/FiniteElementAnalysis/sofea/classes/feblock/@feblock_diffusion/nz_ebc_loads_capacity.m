% Compute element load vectors for nonzero essential boundary conditions.
%
% function evs = nz_ebc_loads_capacity (self, geom, temp_rate)
%
% Compute the element load vectors corresponding to nonzero
% essential boundary conditions (prescribed temperature rate).
% Return an array of them so they may be assembled.
%    Call as
% evs = nz_ebc_loads(feb, geom, temp_rate)
%     geom=geometry field
%     temp=temperature rate field
%
function evs = nz_ebc_loads_capacity (self, geom, temp_rate)
    ems = capacity (self, geom, temp_rate);
    gcells = get(self,'gcells');
    % Pre-allocate the element matrices
    evs(1:length(gcells)) = deal(elevec);
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        pT = gather(temp_rate, conn, 'prescribed_values');
        if norm (pT) ~= 0
            Ce = get(ems(i),'mat'); % element matrix
            evs(i) = set(evs(i), 'vec', -Ce*pT);
            evs(i) = set(evs(i), 'eqnums', ...
                gather(temp_rate, conn, 'eqnums'));
        end
    end
    return;
end
