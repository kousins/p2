% Compute the surface heat transfer matrices of the individual gcells.
%
% function ems = surface_transfer(self, geom, temp)
%
% Return an array of the element matrices so they may be assembled.
%    Call as     ems = surface_transfer(feb, geom, temp)
%    where    geom=geometry field
%             temp=temperature field
function ems = surface_transfer(self, geom, temp)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    % Pre-allocate the element matrices
    ems(1:ngcells) = deal(elemat);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % surface transfer coefficient
    h = self.surface_transfer;
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        He = zeros(nfens); % element matrix
        for j=1:npts_per_gcell % Loop over integration points
            N = bfun(gcells(i), pc(j,:));
            detJ = Jacobian_surface(gcells(i),pc(j,:),x);
            He = He + h*N*N' * detJ * w(j);
        end
        ems(i)=set(ems(i),'mat', He);
        ems(i)=set(ems(i),'eqnums',gather(temp,conn,'eqnums'));
    end
    return;
end
