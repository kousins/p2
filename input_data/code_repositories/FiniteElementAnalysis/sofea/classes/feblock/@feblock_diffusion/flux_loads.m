% Compute the element load vectors corresponding to prescribed heat flux. 
%
% function evs = flux_loads (self, geom, temp, fi)
%
% Return an array of the element loads so they may be assembled.
%    Call as
% evs = flux_loads(feb, geom, temp, fi)
%     geom=geometry field
%     temp=temperature field
%     fi=magnitude of the flux (force intensity)

% The heat flux is positive when the heat leaves the body, and negative when it enters.
%
function evs = flux_loads (self, geom, temp, fi)
    gcells = get(self.feblock,'gcells');
    nfens = get(gcells(1),'nfens');
    % Pre-allocate the element matrices
    evs(1:length(gcells)) = deal(elevec);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Fe = zeros(nfens, 1); % element matrix
        for j=1:npts_per_gcell % Loop over integration points
            N = bfun(gcells(i), pc(j,:));
            detJ = Jacobian_surface(gcells(i),pc(j,:),x);
            f=get_magn(fi, N'*x); 
            Fe = Fe - f*N * detJ * w(j);
        end
        evs(i) = set(evs(i), 'vec', Fe);
        evs(i) = set(evs(i), 'eqnums', ...
            gather(temp, conn, 'eqnums'));
    end
    return;
end
