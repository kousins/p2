% Constructor of the finite element block class.
%
% function retobj = feblock_diffusion (varargin)
%
%    This class implements the linear steady-diffusion operations.
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: those recognized by feblock, plus
% surface_transfer = surface transfer coefficient
%
function retobj = feblock_diffusion (varargin)
    class_name='feblock_diffusion';
    if (nargin == 0)
        parent = feblock;
        self.surface_transfer = 0;
        self.matstates= [];
        retobj = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent = feblock(varargin{:});
            surface_transfer =0;
            if isfield(options,'surface_transfer')
                surface_transfer= options.surface_transfer;
            end
            self.surface_transfer = surface_transfer;
            self.matstates= [];
            retobj = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
