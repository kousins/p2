% Compute the conductivity matrices of the individual gcells.
%
% function ems = conductivity(self, geom, temp)
%
% Return an array of the element matrices so they may be assembled.
%    Call as     ems = conductivity(feb, geom, temp)
%    where   geom=geometry field
%            temp=temperature field
function ems = conductivity(self, geom, temp)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    % Pre-allocate the element matrices
    ems(1:ngcells) = deal(elemat);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self.feblock, 'mater');
    kappa = get(get(mat,'property'),'conductivity');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom,conn,'values','noreshape');% node coord
        Ke = zeros(nfens); % element matrix
        % Loop over all integration points
        for j=1:npts_per_gcell
            N = bfun (gcells(i), pc(j,:));
            Nder = bfundpar (gcells(i), pc(j,:));
            Nspder = bfundsp(gcells(i), Nder, x);
            detJ = Jacobian_volume(gcells(i),pc(j,:),x);
            Rm = material_directions(self,gcells(i),pc(j,:),x);
            Ke = Ke + Nspder*Rm*kappa*Rm'*Nspder' * detJ * w(j);
        end
        ems(i)=set(ems(i), 'mat', Ke);
        ems(i)=set(ems(i), 'eqnums',gather(temp,conn,'eqnums'));
    end
    return;
end
