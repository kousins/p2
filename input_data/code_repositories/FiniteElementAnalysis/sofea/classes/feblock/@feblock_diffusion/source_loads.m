% Compute the source-term load vectors of the individual gcells.
%
% function evs = source_loads(self, geom, temp)
%
% Return an array of them so they may be assembled.
%    Call as
%       evs = source_loads(feb, geom, temp)
%    where
%       geom=geometry field
%       temp=temperature field
%
function evs = source_loads(self, geom, temp)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    % Pre-allocate the element matrices
    evs(1:ngcells) = deal(elevec);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self.feblock, 'mater');
    Q = get(mat,'source');
    if (~ isempty(Q)) % Any loads due to internal heat sources?
        if (strcmp(class(Q),'function_handle'))
            need_to_eval=1;
        else
            need_to_eval=0;
        end
        % Now loop over all gcells in the block
        for i=1:ngcells
            conn = get(gcells(i), 'conn'); % connectivity
            x = gather(geom, conn, 'values', 'noreshape'); 
            Fe = zeros(nfens,1); % element load vector
            % Loop over all integration points
            for j=1:npts_per_gcell
                Nder = bfundpar (gcells(i), pc(j,:));
                detJ = Jacobian_volume(gcells(i),pc(j,:),x);
                N = bfun(gcells(i), pc(j,:));
                if (need_to_eval)
                    loc=[N'*x(:,1) N'*x(:,2)];
                    val = feval(Q,loc);
                else
                    val = Q;
                end
                Fe = Fe + N * val * detJ * w(j);
            end
            evs(i) = set(evs(i), 'vec', Fe);
            evs(i) = set(evs(i), 'eqnums', ...
                gather(temp, conn, 'eqnums'));
        end
    end
    return;
end
