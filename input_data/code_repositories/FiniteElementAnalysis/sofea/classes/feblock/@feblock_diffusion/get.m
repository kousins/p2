% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'surface_transfer'}, {'surface heat transfer coefficient'}};...
            {{'matstates'}, {'array of material states'}};
            };
        val = cat(1, get(self.feblock), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'surface_transfer'
                val = self.surface_transfer;
            case 'matstates'
                if isempty (self.matstates)
                    integration_rule = get(self.feblock, 'integration_rule');
                    npts_per_gcell = get(integration_rule, 'npts');
                    gcells = get(self.feblock,'gcells');
                    ngcells = length(gcells);
                    self.matstates = cell(get(gcells(1),'nfens')*ngcells,npts_per_gcell);
                    mat = get(self.feblock, 'mater');
                    for i=1:ngcells
                        for j=1:npts_per_gcell;
                            self.matstates{i,j} = newmatstate(mat);
                        end
                    end
                end
                val=self.matstates;
            otherwise
                val = get(self.feblock, prop_name);
        end
    end
end
