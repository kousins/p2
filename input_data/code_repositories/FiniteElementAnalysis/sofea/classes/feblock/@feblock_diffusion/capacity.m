% Compute the capacity matrices of the individual gcells.
%
% function ems = capacity(self, geom, temp)
%
% Return an array of them so they may be assembled.
%    Call as
%       ems = capacity(feb, geom, temp)
%    where
%       geom=geometry field
%       temp=temperature field
%
function ems = capacity(self, geom, temp)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    % Pre-allocate the element matrices
    ems(1:ngcells) = deal(elemat);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Material
    mat = get(self.feblock, 'mater');
    cv = get(mat,'specific_heat');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Ce = zeros(nfens); % element matrix
        for j=1:npts_per_gcell % Loop over integration points
            N = bfun(gcells(i), pc(j,:));
            detJ = Jacobian_volume(gcells(i),pc(j,:),x);
            Ce = Ce + cv*N*N' * detJ * w(j);
        end
        ems(i)=set(ems(i),'mat', Ce);
        ems(i)=set(ems(i),'eqnums',gather(temp,conn,'eqnums'));
    end
    return;
end
