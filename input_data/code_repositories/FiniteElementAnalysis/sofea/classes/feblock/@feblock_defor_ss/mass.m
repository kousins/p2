% Compute the mass matrices of the individual gcells.
%
% function ems = mass (self, geom, u)
%
% Return an array of them so they may be assembled.
%   Call as:
%     ems = mass(feb, geom, u);
%   geom=geometry field
%   u=displacement field
%
function ems = mass (self, geom, u)
    gcells = get(self.feblock,'gcells');
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    ems(1:length(gcells)) = deal(elemat);% Pre-allocate 
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    mat = get(self, 'mater');% Material
    rho = get(get(mat,'property'), 'rho');
    Nexp = zeros(dim,dim*nfens);
    % Now loop over all gcells in the block
    Me = zeros(dim*nfens); % 
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Me =Me*0;% zero out element matrix
        % Loop over all integration points
        for j=1:npts_per_gcell
            detJ= Jacobian_volume(gcells(i), pc(j,:), x);
            N = bfun (gcells(i), pc(j,:));
            for l = 1:nfens;
                Nexp(1:dim,(l-1)*dim+1:(l)*dim)=eye(dim)*N(l);
            end;
            Me = Me + Nexp'*Nexp * rho * detJ * w(j);
        end
        ems(i) = set(ems(i), 'mat', Me);
        ems(i) = set(ems(i), 'eqnums', gather(u, conn, 'eqnums'));
    end
    return;
end
