% Compute the element load vectors.
%
% function evs = distrib_loads(self, geom, u, fi, m)
%
% Compute the element load vectors corresponding to applied distributed
% load. Here it means force per unit volume where volume could be
% length^3, length^2, length^1, or length^0, depending on the manifold
% dimension of the geometric cells.
% Return an array of the load vectors so they may be assembled.
%    Call as
% evs = distrib_loads(feb, geom, u, fi, m)
%     geom=geometry field
%     u=displacement field
%     fi=force intensity object
%     m= manifold dimension
%
function evs = distrib_loads(self, geom, u, fi, m)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(u,'dim');
    % Pre-allocate the element matrices
    evs(1:ngcells) = deal(elevec);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    w  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % node coord
        % Loop over all integration points
        Fe = zeros(dim*nfens, 1); % element vector
        for j=1:npts_per_gcell
            N = bfun (gcells(i), pc(j,:));
            detJ = Jacobian_mdim(gcells(i),pc(j,:),x,m);
            f=get_magn(fi,N'*x);
            Fe = Fe + reshape(f*N',dim*nfens,1) * detJ * w(j);
        end
        evs(i) = set(evs(i), 'vec', Fe);
        evs(i) = set(evs(i), 'eqnums', gather(u, conn, 'eqnums'));
    end
    return;
end
