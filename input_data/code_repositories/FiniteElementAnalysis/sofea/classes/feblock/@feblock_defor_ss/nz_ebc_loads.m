% Compute element load vectors for to nonzero essential boundary conditions
%
% function evs = nz_ebc_loads(self, geom, u)
%
% Compute the element load vectors corresponding to nonzero essential boundary
% conditions (settlement of the supports).
% Return an array of them so they may be assembled.
%    Call as
% evs = nz_ebc_loads(feb, geom, u)
%     geom=geometry field
%     u=displacement field
%
function evs = nz_ebc_loads(self, geom, u)
    ems = stiffness (self, geom, u);
    gcells = get(self.feblock,'gcells');
    % Pre-allocate the element matrices
    evs(1:length(gcells)) = deal(elevec);
    % Now loop over all gcells in the block
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn'); % connectivity
        pu = gather(u, conn, 'prescribed_values');
        if norm (pu) ~= 0
            Ke = get(ems(i),'mat'); % element stiffness matrix
            evs(i) = set(evs(i), 'vec', -Ke*pu);
            evs(i) = set(evs(i), 'eqnums', gather(u, conn, 'eqnums'));
        end
    end
    return;
end
