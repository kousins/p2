% Create a field from quantities at integration points.
%
% function fld = field_from_integration_points(self, ...
%         geom, u, dT, output, component)
%
% Input arguments
%     geom     - reference geometry field
%     u        - displacement field
%     dT       - temperature difference field
%     output   - this is what you would assign to the 'output' attribute of
%                the context argument of the material update() method.
%     component- component of the 'output' array: see the material update()
%                method.
% Output argument
%     fld - the new field that can be used to map values the colors
%
function fld = field_from_integration_points(self, ...
        geom, u, dT, output, component)
    gcells = get(self.feblock,'gcells');
    % Make the inverse map from finite element nodes to gcells
    f2gmap=fenode_to_gcell_map (struct ('gcells',gcells));
    gmap=get (f2gmap,'gcell_map');
    % Container of intermediate results
    sum_inv_dist =0*(1:length(gmap))';
    sum_quant_inv_dist =0*(1:length(gmap))';
    nvals=0*(1:length(gmap))';
    % This is an inverse-distance interpolation inspector.
    x= []; conn = [];
    function idat=idi(idat, out, xyz, pc)
        d=x-ones(length(conn),1)*xyz;
        ld=reshape(d',prod(size(d)),1);
        d =sum(reshape(ld.*ld,size(d,2), size(d,1)));
        zi = find(d==0);
        if (~isempty(zi))
            nzi = find(d>0);
            d(zi) =min(d(nzi))/1000;
        end
        invd =1./d;
        sum_quant_inv_dist(conn)=sum_quant_inv_dist(conn) + invd'*out(component);
        sum_inv_dist(conn)=sum_inv_dist(conn)+invd';
    end
    % Loop over cells to interpolate to nodes
    idat=0;
    for i=1:length(gcells)
        conn = get(gcells(i), 'conn');
        x=gather(geom,conn,'values','noreshape');
        idat = inspect_integration_points(self, geom, u, dT, ...
            i, struct ('output',output), @idi, idat);
    end
    % compute the data array
    nzi = find (sum_inv_dist);
    nvals(nzi)=sum_quant_inv_dist(nzi)./sum_inv_dist(nzi);
    % Make the field
    fld=field(struct ('name', [output num2str(component)],...
        'data', nvals));
    return;
end

%     function idat=idi2(idat, out, xyz, pc)
%         for i=1:length(idat.conn)
%             d=idat.node_xyz(i,:)-xyz;
%             nd=norm(d);
%             if nd >0
%                 invd=1/nd;
%             else
%                 invd=realmax;
%             end
%             k=idat.conn(i);
%             sum_quant_inv_dist(k)=sum_quant_inv_dist(k) + invd*out(component);
%             sum_inv_dist(k)=sum_inv_dist(k)+invd;
%         end
%         return;
%     end
%

% % Create a field from quantities at integration points.
% % Input arguments
% %     geom     - reference geometry field
% %     u        - displacement field
% %     dT       - temperature difference field
% %     output   - this is what you would assign to the 'output' attribute of
% %                the context argument of the material update() method.
% %     component- component of the 'output' array: see the material update()
% %                method.
% % Output argument
% %     fld - the new field that can be used to map values the colors
% %
% function fld = field_from_integration_points(self, ...
%         geom, u, dT, output, component)
%     gcells = get(self.feblock,'gcells');
%     ngcells = length(gcells);
%     % Make the inverse map from finite element nodes to gcells
%     f2gmap=fenode_to_gcell_map (struct ('gcells',gcells));
%     gmap=get (f2gmap,'gcell_map');
%     % The nodal data are initialized of the zero
%     nvals =0*(1:length(gmap))';
%     % Loop over cells to interpolate to nodes
%     for i=1:length(gmap)
%         idat.component=component;
%         idat.sum_inv_dist=0;
%         idat.sum_quant_inv_dist=0;
%         idat.node_xyz=gather(geom,[i],'values','noreshape');
%         idat = inspect_integration_points(self, geom, u, dT, ...
%             gmap{i}, struct ('output',output), @idi, idat);
%         if idat.sum_inv_dist~=0
%             nvals(i)=idat.sum_quant_inv_dist/idat.sum_inv_dist;
%         end
%     end
%     % Make the field
%     fld=field(struct ('name', [output num2str(component)],...
%         'data', nvals));
%     return;
% end
%
% % This is an inverse-distance interpolation inspector.
% function idat=idi(idat, out, xyz, pc)
%     d=idat.node_xyz-xyz;
%     nd=norm(d);
%     if nd >0
%         invd=1/nd;
%     else
%         invd=realmax;
%     end
%     idat.sum_quant_inv_dist=idat.sum_quant_inv_dist + invd*out(idat.component);
%     idat.sum_inv_dist=idat.sum_inv_dist+invd;
%     return;
% end
