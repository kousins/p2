% Compute the element load vectors corresponding to applied body load.
%
% function evs = body_loads(self, geom, w, bl)
%
% Return an array of them so they may be assembled.
%    Call as
% evs = body_loads(feb, geom, u, bl)
%     geom=geometry field
%     w=displacement field
%     bl= body load
%
function evs = body_loads(self, geom, w, bl)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    if dim ~= 1
        error(['wrong dimension: need dim=1']);
    end
    % Pre-allocate the element matrices
    evs(1:ngcells) = deal(elevec);
    % Integration rule
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    weights = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coordinates of nodes
        % Loop over all integration points
        rFe=zeros(dim*nfens,1);
        for j=1:npts_per_gcell
            Nder = bfundpar (gcells(i), pc(j,:));
            Nspatialder = bfundsp (gcells(i), Nder, x);
           detJ = Jacobian (gcells(i), Nder, x);
            N = bfun (gcells(i), pc(j,:));
            xyz = N'*x;
            b = reshape(get_magn(bl,xyz),1,1);
            eb = reshape(b*N',dim*nfens,1);
            rFe=rFe + eb * detJ * weights(j);
        end
        evs(i) = set(evs(i), 'vec', rFe);
        evs(i) = set(evs(i), 'eqnums', gather(w, conn, 'eqnums'));
    end
    return;
end
