% Compute the stiffness matrices of the individual uniaxial gcells.
%
% function ems = stiffness (self, geom, w)
%
% Return an array of them so they may be assembled.
%    Call as
% ems = stiffness(feb, geom, w)
%     geom=geometry field
%     w=displacement field
%
function ems = stiffness (self, geom, w)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    dim = get(geom,'dim');
    ems(1:ngcells) = deal(elemat);% Pre-allocate the element matrices
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    weights  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    P = self.P; % pre-stressing force
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coordinates of nodes
        Ke = zeros(dim*nfens); % element stiffness matrix
        % Loop over all integration points
        for j=1:npts_per_gcell
            Nder = bfundpar (gcells(i), pc(j,:));
            Nspatialder = bfundsp (gcells(i), Nder, x);
            detJ= Jacobian(gcells(i), pc(j,:), x);
            B = Blmat(gcells(i), pc(j,:), x, [1]);
            Ke = Ke + B'*P*B * detJ * weights(j);
        end
        ems(i) = set(ems(i), 'mat', Ke);
        ems(i) = set(ems(i), 'eqnums', gather(w, conn, 'eqnums'));
    end
    return;
end
