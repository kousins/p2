% Compute the mass matrices of the individual gcells.
%
% function ems = mass (self, geom, w)
%
% Return an array of them so they may be assembled.
%   Call as:
%     ems = mass(feb, geom, w);
%   geom=geometry field
%   w=displacement field
%
function ems = mass (self, geom, w)
    gcells = get(self.feblock,'gcells');
    ngcells = length(gcells);
    nfens = get(gcells(1),'nfens');
    ems(1:ngcells) = deal(elemat);% Pre-allocate the element matrices
    integration_rule = get(self.feblock, 'integration_rule');
    pc = get(integration_rule, 'param_coords');
    weights  = get(integration_rule, 'weights');
    npts_per_gcell = get(integration_rule, 'npts');
    mu = get(self, 'mu');
    % Now loop over all gcells in the block
    for i=1:ngcells
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coordinates of nodes
        Me = zeros(nfens); % element stiffness matrix
        % Loop over all integration points
        for j=1:npts_per_gcell
            detJ= Jacobian(gcells(i), pc(j,:), x);
            N = bfun (gcells(i), pc(j,:));
            Me = Me + N*N' * detJ * mu * weights(j);
        end
        ems(i) = set(ems(i), 'mat', Me);
        ems(i) = set(ems(i), 'eqnums', gather(w, conn, 'eqnums'));
    end
    return;
end
