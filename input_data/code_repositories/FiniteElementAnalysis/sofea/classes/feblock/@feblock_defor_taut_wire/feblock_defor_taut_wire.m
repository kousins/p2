% Constructor of the finite element block class.
%
% function retobj = feblock_defor_taut_wire (varargin)
%
%    This class implements small displacement, small strain deformation
%    mechanics for taut wire elements.
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options: those recognized by feblock
%
function retobj = feblock_defor_taut_wire (varargin)
    class_name='feblock_defor_taut_wire';
    if (nargin == 0)
        parent = feblock;
        self.P = 1.0;
        self.mu = 1.0;
        retobj = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            parent = feblock(varargin{:});
            options =varargin{1};
            self.P = options.P;
            self.mu = 1.0;
            if isfield(options,'mu')
                self.mu = options.mu;
            end
            retobj = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
