% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'P'}, {'Pre-stressing force'}};
            {{'mu'}, {'Mass per unit length'}}
            };
        val = cat(1, get(self.feblock), val);
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'P'
                val= self.P;
            case 'mu'
                val= self.mu;
            otherwise
                val = get(self.feblock, prop_name);
        end
    end
end

