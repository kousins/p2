% nodal_load
%   This convenience class provides methods for manipulating nodal loads:
%   (generalized) forces applied at the nodes.
%
%   @nodal_load/loads - Compute an array of Element vectors
%   @nodal_load/nodal_load - Constructor
