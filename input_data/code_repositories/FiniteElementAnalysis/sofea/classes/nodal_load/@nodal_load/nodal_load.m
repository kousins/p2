% Constructor of the nodal load class.
%
% function self = nodal_load (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%      id=finite element node ID, or an array of ID's
%      dir=vector of directions
%      magn=vector of magnitudes
%   For instance
%      nodal_load(struct ('id',3,'dir',[1 3],'magn',[-4 0.6]))
%   applies nodal forces at node 3, direction 1 and 3, magnitudes -4 and 0.6.
%
function self = nodal_load (varargin)
    class_name='nodal_load';
    parent = classbase;
    if (nargin == 0)
        self.id = 0;
        self.dir = [];
        self.magn = [];
        self = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            options = varargin{1};
            self.id = options.id;
            self.dir = options.dir;
            self.magn = options.magn;
            if (length(self.dir) ~= length(self.magn))
                error('dir and magn must be the same size!');
            end
            self = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
