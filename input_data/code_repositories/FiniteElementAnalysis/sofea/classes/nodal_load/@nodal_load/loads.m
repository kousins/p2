% Calculate the load vectors.  
%
% function evs = loads (self, u)
%
% Calculate the load vectors.  Return an array of
% them so they may be assembled into the system load
% vector.
%   u=displacement field
%
function evs = loads (self, u)
    eqnums=get(u,'eqnums');
    evs=[];
    for i = 1:length(self.id)
        en=[];
        l=[];
        for j=1:length(self.dir)
            en = [en; eqnums(self.id(i),self.dir(j))];
            l  = [l; self.magn(j)];
        end
        evs = [evs,elevec(struct ('eqnums',en,'vec',l))];
    end
end
