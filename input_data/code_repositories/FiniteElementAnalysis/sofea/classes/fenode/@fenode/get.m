% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'id'}, {'node identifier, positive integer'}};
            {{'xyz'}, {'coordinates of the node, array [dim,1], where dim = dimension of the embedding space'}};
            {{'xyz3'}, {'coordinates of the node, array [3,1]'}}};
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'id'
                val = self.id;
            case 'xyz'
                val = self.xyz;
            case 'xyz3'
                if (length(self.xyz)==1)
                    val = [self.xyz 0 0];
                elseif (length(self.xyz)==2)
                    val = [self.xyz 0];
                else
                    val = self.xyz;
                end
            otherwise
                error(['Unknown property ''' prop_name '''!']);
        end
    end
end
