% Constructor of the finite element node class.
%
% function self = fenode (varargin)
%
% Class representing finite element nodes, in one dimension,
% two dimensions, or three dimensions.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% id = identifier of the node, 
% xyz= coordinates as a one-dimensional,
%       two-dimensional, or three-dimensional array.
%
function self = fenode (varargin)
    class_name='fenode';
    parent = classbase;
    if (nargin == 0)
        self.id = 0;
        self.xyz = [];
        self = class(self,class_name,parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            self = arg;
            return;
        else
            options = varargin{1};
            self.id = options.id;
            xyz = options.xyz;
            [m,n]=size(xyz);
            self.xyz = xyz;
            self = class(self,class_name,parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end


