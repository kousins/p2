% Draw a graphic representation.
%
% function draw (self, gv, context)
%
%
% Input arguments
% self = self
% gv = graphic viewer
% context = struct
% with mandatory fields
%    x=reference geometry field
%    u=displacement field
% with optional fields
%    color = color of the text
%
function draw (self, gv, context)
conn = get(self, 'id');
x = gather(context.x, conn, 'values', 'noreshape'); % coordinates of nodes
u = gather(context.u, conn, 'values', 'noreshape'); % coordinates of nodes
draw_text (gv, x+u, num2str(self.id), context);
return;

