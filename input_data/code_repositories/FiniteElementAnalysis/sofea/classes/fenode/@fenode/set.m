% Set property in the specified object.
%
% function retval = set(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retval = set(self,varargin)
    if (nargin == 1)
        retval={{{'xyz'}, {'coordinates, array [dim,1], where dim = dimension of the embedding space'}}};
        return;
    end
    prop_name=varargin{1};
    val=varargin{2};
    switch prop_name
        case 'xyz'
            self.xyz=val;
        case 'id'
            self.id=val;
        otherwise
            error(['Unknown property ''' prop_name '''!']);
    end
    retval=self; % return self
end
