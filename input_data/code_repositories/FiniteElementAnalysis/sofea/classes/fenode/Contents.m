% fenode
%   This class implements the finite element node.
% 
%   @fenode/fenode - Constructor
%   @fenode/get - Get an attribute
%   @fenode/draw - Draw the node
%   @fenode/set - Set an attribute
