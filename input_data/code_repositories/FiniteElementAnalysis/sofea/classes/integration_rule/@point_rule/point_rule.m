% Constructor of the point rule (used exclusively for zero-dimensional manifolds).
%
% function retobj = point_rule (varargin)
%
%   Call as:
%      ir = point_rule
function retobj = point_rule (varargin)
    class_name='point_rule';
    parent=classbase;
    self.param_coords = 0;
    self.weights= 1;
    retobj = class(self,class_name,parent);
end
