% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
% There is a third form: since it is possible to request multiple
% integration rules, for instance to implement selective reduced
% integration, one may request a particular rule by supplying a third
% argument, the serial number of the rule. Thus, for instance if we created
% a selective reduced integration rule with
%      ir = gauss_rule(2, [2 1]);
% we may request the parametric coordinates of the quadrature points out
% the first rule, that is 2 x 2 rule, by writing
%      get(ir,'param_coords',1).
%
function val = get(self,varargin)
if (nargin == 1)
    val = {{{'nrules'}, {'number of individual quadrature rules: see the notes for the get() method and for the constructor, scalar'}};
        {{'npts'}, {'number of quadrature points, scalar'}};
        {{'npts_per_gcell'}, {'total number of quadrature points per gcell (sum of npts for all rules), scalar'}};
        {{'weights'}, {'array of weights, [npts,1]'}};
        {{'param_coords'}, {'parametric coordinates of quadrature points, array [npts,dim]'}};
        };
    return;
else
    prop_name=varargin{1};
    wr =1;
    if (nargin > 2)
        wr=varargin{2};
    end
    switch prop_name
        case 'nrules'
            val = length(self.weights);
        case 'npts'
            val = length(self.weights{wr});
        case 'npts_per_gcell'
            val = 0;
            for i=1:length(self.weights)
                val = val + length(self.weights{i});
            end
        case 'weights'
            val = self.weights{wr};
        case 'param_coords'
            val = self.param_coords{wr};
        otherwise
            error(['Unknown property name ''' prop_name '''!']);
    end
end
return;

