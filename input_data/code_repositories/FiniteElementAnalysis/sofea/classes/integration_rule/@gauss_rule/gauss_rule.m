% Constructor of the Gauss rule.
%
% function retobj = gauss_rule (varargin)
%
%   Call as:
%      ir = gauss_rule(dim, order);
%   where
%      dim=dimension of the cell (1=1D, 2=2D, 3=3D)
%      order=number of points along one dimension:
%            1=1 point rule, 2=two point rule, etc.
% It is possible to request a composite integration rule, for instance to
% implement selective reduced integration. In that case the argument
% 'order' is an array (typically with two elements). For example, to
% request selective reduced integration in two dimensions we would use
%      ir = gauss_rule(2, [2 1]);
% which corresponds to 2 x 2 rule and a one-point rule.
%
function retobj = gauss_rule (varargin)
class_name='gauss_rule';
parent=classbase;
if (nargin == 0)
    self.dim = 0;
    self.order = 0;
    self.param_coords = [];
    self.weights= [];
    retobj = class(self,class_name,parent);
elseif (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) % copy constructor.
        retobj = arg;
        return;
    else
        error(['Illegal argument ' inputname(1) ' of class ' class(varargin{1}) '!']);
        return;
    end
elseif (nargin == 2)
    self.dim = varargin{1};
    self.order = varargin{2};
    nrules=length (self.order);
    pcs=cell(nrules,1);
    ws=cell(nrules,1);
    for i=1:nrules
        switch self.order(i)
            case 1
                pcs{i} = [ 0 ];
                ws{i}  = [ 2 ];
            case 2
                pcs{i} = [ -0.577350269189626 0.577350269189626 ];
                ws{i}  = [ 1 1 ];
            case 3
                pcs{i} = [ -0.774596669241483  0  0.774596669241483 ];
                ws{i}  = [ 0.5555555555555556 0.8888888888888889 0.5555555555555556 ];
            case 4
                pcs{i} = [ -0.86113631159405  -0.33998104358486   0.33998104358486   0.86113631159405];
                ws{i}  = [ 0.34785484513745   0.65214515486255   0.65214515486255   0.34785484513745];
            otherwise
                [xx, ww] = gaussquad(self.order(i));
                pcs{i} = xx;
                ws{i}  = ww;
        end
    end
    switch self.dim
        case 1
            for m=1:nrules
                self.param_coords{m} = pcs{m}';
                self.weights{m} = ws{m};
            end
        case 2
            nrules=length (self.order);
            for m=1:nrules
                param_coords = [];
                weights = [];
                pc=pcs{m};
                w=ws{m};
                for i=1:self.order(m)
                    for j=1:self.order(m)
                        param_coords = [ param_coords; [ pc(i) pc(j) ] ];
                        weights = [ weights; w(i)*w(j) ];
                    end
                end
                self.param_coords{m} =param_coords;
                self.weights{m} =weights;
            end
        case 3
            nrules=length (self.order);
            for m=1:nrules
                param_coords = [];
                weights = [];
                pc=pcs{m};
                w=ws{m};
                for i=1:self.order(m)
                    for j=1:self.order(m)
                        for k=1:self.order(m)
                            param_coords = [ param_coords; [ pc(i) pc(j) pc(k) ] ];
                            weights = [ weights; w(i)*w(j)*w(k) ];
                        end
                    end
                end
                self.param_coords{m} =param_coords;
                self.weights{m} =weights;
            end
        otherwise
            error('Unknown dimension!');
    end
    retobj = class(self,class_name,parent);
else
    error('Illegal arguments!');
end
return;
