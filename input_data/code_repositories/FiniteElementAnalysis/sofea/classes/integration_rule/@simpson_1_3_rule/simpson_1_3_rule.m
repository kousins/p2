% Constructor of Simpson 1/3 quadrature rule, on the interval -1 <=x<= +1.
%
% function retobj = simpson_1_3_rule (varargin)
%
%   Call as:
%      ir = simpson_1_3_rule;
%
function retobj = simpson_1_3_rule (varargin)
    class_name='simpson_1_3_rule';
    parent=classbase;
    if (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            error('Illegal arguments!');
        end
    else
        self.param_coords = [-1, 0, 1]';
        self.weights = [1, 4, 1]/6*2;
        retobj = class(self,class_name,parent);
    end
    return;
end
