% Constructor of triangular quadrature rule.
%
% function retobj = tri_rule (varargin)
%
%   Call as:
%      ir = tri_rule(npts);
%   where
%      npts=number of points
%            1=1 point rule,  3=three point rule, etc.
%
function retobj = tri_rule (varargin)
class_name='tri_rule';
parent=classbase;
if (nargin == 0)
    self.npts = 0;
    self.param_coords = [];
    self.weights= [];
    retobj = class(self,class_name,parent);
elseif (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) % copy constructor.
        retobj = arg;
        return;
    else
        self.npts = varargin{1};
        switch self.npts
            case 1 % integrates exactly linear polynomials
                self.param_coords(1,:) = [1/3 1/3];
                self.weights = [1]/2;
            case 3 % integrates exactly quadratic polynomials
                self.param_coords = [ 2/3 1/6; 1/6 2/3; 1/6 1/6 ];
                self.weights = [1/3 1/3 1/3]/2;
            case 6 % integrates exactly quartic polynomials
                self.param_coords = [ 0.816847572980459 0.091576213509771;...
                    0.091576213509771 0.816847572980459; ...
                    0.091576213509771 0.091576213509771;...
                    0.108103018168070 0.445948490915965;...
                    0.445948490915965 0.108103018168070;...
                    0.445948490915965 0.445948490915965];
                self.weights = [0.109951743655322*[1, 1, 1] 0.223381589678011*[1, 1, 1] ]/2;
            otherwise
                error('Unsupported number of points');
        end
        retobj = class(self,class_name,parent);
    end
else
    error('Illegal arguments!');
end
return;

