% integration_rule
%   This is a collection of classes implementing various
%   quadrature rules.
%
% -Simpson 1/3 rule
%   @simpson_1_3_rule/simpson_1_3_rule -  constructor
%   @simpson_1_3_rule/get - Get an attribute
% -triangle quadrature rules
%   @tri_rule/tri_rule - Constructor
%   @tri_rule/get - Get an attribute
% -Gaussian rules
%   @gauss_rule/gauss_rule - Constructor
%   @gauss_rule/get - Get an attribute
% -hexahedron six-point rule
%   @hex_6pt_rule/hex_6pt_rule - Constructor
%   @hex_6pt_rule/get - Get an attribute
% -tetrahedron quadrature rules
%   @tet_rule/tet_rule - Constructor
%   @tet_rule/get - Get an attribute
% -integration at a point
%   @point_rule/point_rule - Constructor
%   @point_rule/get - Get an attribute
