% Constructor of a special six-point hexahedron quadrature rule.
%
% function retobj = hex_6pt_rule (varargin)
%
%   Call as:
%      ir = hex_6pt_rule;
%   where
%     no arguments
%
function retobj = hex_6pt_rule (varargin)
    class_name='hex_6pt_rule';
    parent=classbase;
    if (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        end
    else
        self.param_coords = ...
            [[-1, 0, 0];...
            [1, 0, 0];...
            [0,-1,0];...
            [0,1,0];...
            [0, 0,-1];...
            [0, 0, 1]];
        self.weights =4/3*ones(6, 1);
        retobj = class(self,class_name,parent);
        return;
    end
end
