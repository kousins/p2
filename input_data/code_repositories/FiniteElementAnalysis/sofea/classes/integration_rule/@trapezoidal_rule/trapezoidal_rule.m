% Constructor of trapezoidal quadrature rule, on the interval -1 <=x<= +1.
%
% function retobj = trapezoidal_rule (varargin)
%
%   Call as:
%      ir = trapezoidal_rule;
%
function retobj = trapezoidal_rule (varargin)
    class_name='trapezoidal_rule';
    parent=classbase;
    if (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            error('Illegal arguments!');
        end
    else
        self.param_coords = [-1, 1]';
        self.weights = [1, 1];
        retobj = class(self,class_name,parent);
    end
    return;
end
