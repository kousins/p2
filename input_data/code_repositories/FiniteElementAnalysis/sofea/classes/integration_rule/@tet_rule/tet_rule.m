% Constructor of a quadrature rule for tetrahedra.
%
% function retobj = tet_rule (varargin)
%
%   Call as:
%      ir = tet_rule(dim);
%   where
%      dim=number of points (1-- one-point rule, 4 -- four-point rule).
%
function retobj = tet_rule (varargin)
class_name='tet_rule';
parent=classbase;
if (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) % copy constructor.
        retobj = arg;
        return;
    else
        self.dim = varargin{1};
        switch self.dim
            case 1
                self.param_coords = ...
                    [0.25,0.25,0.25];
                self.weights = [1/6];
            case 4
                self.param_coords = ...
                    [[0.13819660,0.13819660,0.13819660];...
                    [0.58541020,0.13819660,0.13819660];...
                    [0.13819660,0.58541020,0.13819660];...
                    [0.13819660,0.13819660,0.58541020]];
                self.weights = [ 0.041666666666666666667,  0.041666666666666666667,  0.041666666666666666667,  0.041666666666666666667];
            otherwise
                error('Unknown number of points!');
        end
        retobj = class(self,class_name,parent);
        return;
    end
end
