% Classes: each folder corresponds to a class tree. The folders whose names
% start with the @ each hold the methods defined for the class. For
% instance, @gcell_H20 holds the methods of the gcell_H20 class.     
%
%   Folders:
%   sofea/classes/classbase        - Base class 
%   sofea/classes/data_colormap    - Mapping of values to colors. 
%   sofea/classes/elemat           - Element matrix class. 
%   sofea/classes/elevec           - Element vector class. 
%   sofea/classes/feblock          - Group of classes to represent the physics 
%                                    of the model (construction of the various 
%                                    matrices and so on). 
%   sofea/classes/fenode           - Finite element node. 
%   sofea/classes/fenode_to_gcell_map  - Map from geometric cells to
%                                      nodes.
%   sofea/classes/field            - Field class represents functions on the mesh. 
%   sofea/classes/force_intensity  - Force-intensity class 
%   sofea/classes/gcell            - Group of classes to represent various
%                                    finite element shapes (Geometric cells). 
%   sofea/classes/graphic_viewer   - Graphic viewer. 
%   sofea/classes/integration_rule - Group of classes for integration rules. 
%   sofea/classes/mater            - Group of classes to provide access to materials. 
%   sofea/classes/nodal_load       - Load at a node. 
%   sofea/classes/property         - Group of classes for material properties. 
%   sofea/classes/sysmat           - Group of classes for the system matrix.
%   sofea/classes/sysvec           - Global system vector. 
%
% Notes: 
% 1. One may find out which attributes are defined for a class by
%    typing the constructor on the command line.
%    For instance, executing `gcell_H20' will print a list of attributes 
%    that can be obtained from the object and those that can be set.
% 2. One may inquire which methods are defined for a class
%    using the `methods' Matlab command. For instance, `methods field'.
% 3. All objects descended from classbase may be inspected 
%    with OBgui (a Matlab command in the util folder).
% 4. Constructors always take either zero or one argument.  For no
%    arguments, the default object is created.  For one argument, the argument
%    may be either an object of the same type, in which case a copy is
%    created; or, it may be a struct with the one or more fields
%    representing options. For instance, these are equivalent calls to the
%    finite element node constructor: 
%       f=fenode(struct('id', 5,'xyz', [0, -.2])) 
%       Optionsf.id=5; Options.xyz=[0, -.2]; f=fenode(Options)
% 5. Attributes may be retrieved from or changed in an object using the
%    get/set method pair. The get method may be used as get(obj). This form
%    returns a cell array of pairs of strings for each "gettable"
%    attribute: The first is the attribute 
%    name, the second is a brief description of the attribute. When the get
%    method is used with an argument, like this
%      get(obj,Attr_name)
%    where
%      obj       - object
%      Attr_name - attribute name
%    it returns the value of the named attribute.
%    The set method may be used as set(obj). This form
%    returns a cell array of pairs of strings for each "settable"
%    attribute: The first is the attribute name, the second is a brief
%    description of the attribute. When the set method is used with an
%    argument, like this 
%      obj =set(obj,Attr_name,val)
%    where
%      obj       - object
%      Attr_name - attribute name
%      val       - value to be assigned to the Attribute.
%    the modified object is returned (do not forget to assign it!).
%
