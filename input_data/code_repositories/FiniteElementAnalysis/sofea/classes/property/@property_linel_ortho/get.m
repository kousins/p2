% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'E1'}, {'Young''s modulus, scalar'}};...
            {{'E2'}, {'Young''s modulus, scalar'}};...
            {{'E3'}, {'Young''s modulus, scalar'}};...
            {{'G12'}, {'Shear modulus, scalar'}};...
            {{'G12'}, {'Shear modulus, scalar'}};...
            {{'G23'}, {'Shear modulus, scalar'}};...
            {{'nu12'}, {'Poisson ratio, scalar'}};...
            {{'nu13'}, {'Poisson ratio, scalar'}};...
            {{'nu23'}, {'Poisson ratio, scalar'}};...
            {{'alpha1'}, {'Coefficient of thermal expansion, scalar'}};...
            {{'alpha2'}, {'Coefficient of thermal expansion, scalar'}};...
            {{'alpha3'}, {'Coefficient of thermal expansion, scalar'}};...
            };
        val = cat(1, val, get(self.property));
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'E1'
                val = self.E1;
            case 'E2'
                val = self.E2;
            case 'E3'
                val = self.E3;
            case 'G12'
                val = self.G12;
            case 'G13'
                val = self.G13;
            case 'G23'
                val = self.G23;
            case 'nu12'
                val = self.nu12;
            case 'nu13'
                val = self.nu13;
            case 'nu23'
                val = self.nu23;
            case 'alphas'
                val = [self.alpha1;self.alpha2;self.alpha3];
            case 'alpha1'
                val = self.alpha1;
            case 'alpha2'
                val = self.alpha2;
            case 'alpha3'
                val = self.alpha3;
            case 'D'
                 val = self.D;
            otherwise
                val = get(self.property, prop_name);
        end
    end
end
