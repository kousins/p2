% Constructor of properties for linearly elastic isotropic materials..
%
% function retobj = property_linel_iso(varargin)
%
% This class represents material properties of deformable linearly elastic isotropic materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields
%     E=Young's modulus
%     nu=Poisson ratio
% optional fields
%     alpha = coefficient of thermal expansion
%
function retobj = property_linel_iso(varargin)
    class_name='property_linel_iso';
    if (nargin == 0)
        parent=property;
        self.E = 1.0;
        self.nu = 0.0;
        self.D =[];
        self.alpha = 0.0;
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=property(options);
            self.E = options.E;
            self.nu = 0;
            self.D =[];
            if isfield(options,'nu')
                self.nu = options.nu;
                if (self.nu < 0)
                    error('Negative Poisson ratio!');
                elseif (self.nu >= 0.5)
                    error('Incompressible material!');
                end
            end
            self.alpha = 0.0;
            if isfield(options,'alpha')
                self.alpha= options.alpha;
            end
            % precompute
            E = self.E;
            nu = self.nu;
            lambda = E * nu / (1 + nu) / (1 - 2*(nu));
            mu     = E / (2 * (1 + nu));
            mI = diag([1 1 1 0.5 0.5 0.5]);
            m1 = [1 1 1 0 0 0]';
            self.D = lambda * m1 * m1' + 2 * mu * mI;
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
