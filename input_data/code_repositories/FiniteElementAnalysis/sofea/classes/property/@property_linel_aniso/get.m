% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'D'}, {'elastic coeffiecient 6x6 matrix'}};...
            {{'alpha'}, {'coefficient of thermal expansion, scalar'}};...
            {{'alphas'}, {'coefficients of thermal expansion, array of three numbers'}};...
            };
        val = cat(1, val, get(self.property));
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'alpha'
                val = self.alpha;
            case 'alphas'
                val = self.alpha*ones(3, 1);
            case 'D'
                if nargin >2
                    context =varargin{2};
                else
                    context = struct ([]);
                end
                val = tangent_moduli(self, context);
            otherwise
                val = get(self.property, prop_name);
        end
    end
end

% Calculate the material stiffness matrix.
%   Call as:
%     D = tangent_moduli(m, context)
%   where
%     m=material
%    context=structure with optional field:
%                kind = string with possible values lambda,
%                       lambda_shear, bulk, bulk_shear
%
% the output arguments are
%     D=matrix 6x6
%
function D = tangent_moduli(self, context)
    if isfield(context,'kind')
        switch context.kind
            case 'lambda'
                D = tangent_moduli_lambda(self, context);
            case 'lambda_shear'
                D = tangent_moduli_shear(self, context);
            case 'bulk'
                D = tangent_moduli_bulk(self, context);
            case 'bulk_shear'
                D = tangent_moduli_bulk_shear(self, context);
        end
    else
       D= self.D;
    end
    return;
end

% Calculate the part of the material stiffness matrix that corresponds to
% the lambda Lame coefficient.
function D = tangent_moduli_lambda(self, context)
    E = self.E;
    nu = self.nu;
    lambda = E * nu / (1 + nu) / (1 - 2*(nu));
    m1 = [1 1 1 0 0 0]';
    D = lambda * m1 * m1';
    return;
end

% Calculate the part of the material stiffness matrix that correspond to shear.
% Note: makes sense only for isotropic materials.
function D = tangent_moduli_shear(self, context)
    E = self.E;
    nu = self.nu;
    mu     = E / (2 * (1 + nu));
    mI = diag([1 1 1 0.5 0.5 0.5]);
    D = 2 * mu * mI;
    return;
end

% Calculate the part of the material stiffness matrix that corresponds to
% the bulk modulus.
function D = tangent_moduli_bulk(self, context)
    E = self.E;
    nu = self.nu;
    B = E / 3 / (1 - 2*(nu));
    m1 = [1 1 1 0 0 0]';
    D = B * m1 * m1';
    return;
end

% Calculate the part of the material stiffness matrix that correspond to shear.
% Note: makes sense only for isotropic materials.
function D = tangent_moduli_shear_bulk(self, context)
    E = self.E;
    nu = self.nu;
    mu     = E / (2 * (1 + nu));
    D = 2 * mu * [2/3*[2 -1 -1; -1 2 -1; -1 -1 2] zeros(3,3); zeros(3,3) eye(3,3) ];
    return;
end
