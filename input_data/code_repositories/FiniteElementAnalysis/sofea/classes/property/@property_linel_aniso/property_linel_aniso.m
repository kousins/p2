% Constructor of properties for linearly elastic anisotropic materials..
%
% function retobj = property_linel_aniso(varargin)
%
% This class represents material properties of deformable linearly elastic anisotropic materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields
%     D= the 6x6 matrix of elastic constants (symmetric!)
% optional fields
%     alpha = coefficient of thermal expansion
%
function retobj = property_linel_aniso(varargin)
    class_name='property_linel_aniso';
    if (nargin == 0)
        parent=property;
        self.D =[];
        self.alpha = 0.0;
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=property(options);
            self.D =options.D;
            if norm(self.D -self.D') > norm(self.D)/1e9 
                error('Unsymmetric material stiffness!');
            end
            self.alpha = 0.0;
            if isfield(options,'alpha')
                self.alpha= options.alpha;
            end
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
