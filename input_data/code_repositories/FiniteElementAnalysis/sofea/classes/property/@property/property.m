% Constructor of the class that represents material properties.
%
% function retobj = property(varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% optional parameters
%     rho=mass density (default is 1.0)
%
function retobj = property(varargin)
    class_name='property';
    parent=classbase;
    if (nargin == 0)
        self.rho = 1.0;
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            rho = 1.0; % mass density
            if isfield(options,'rho')
                rho=options.rho;
            end
            self.rho = rho;
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
