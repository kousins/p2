% Constructor  for diffusion properties of materials.
%
% function retobj = property_diffusion (varargin)
%
% This class represents diffusion properties of materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
%     conductivity=matrix of conductivities; 2x2 for 2-D domains, 3x3
%                  for 3-D domains; the components are given in the local
%                  coordinate frame (feblock decides what that frame is)
%     source=source of heat: function handle or a constant
%
function retobj = property_diffusion (varargin)
    class_name='property_diffusion';
    parent=property;
    if (nargin == 0)
        self.conductivity = 1.0;
        self.specific_heat= 1.0;
        self.source = [];
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            self.conductivity = options.conductivity;
            if (det(self.conductivity) < 0)
                error('Negative determinant of conductivity!');
            end
            specific_heat= 1.0;
            if isfield(options,'specific_heat')
                specific_heat=options.specific_heat;
            end
            self.specific_heat = specific_heat;
            rho= 1.0;
            if isfield(options,'rho')
                rho=options.rho;
            end
            self.rho = rho;
            source = [];
            if isfield(options,'source')
                source=options.source;
            end
            self.source = source;
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
