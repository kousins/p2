% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'conductivity'}, {'conductivity matrix, [d,d], where d=1, 2, or 3'}};...
            {{'specific_heat'}, {'specific heat, scalar'}};...
            {{'source'}, {'heat source, either scalar or function handle'}}};
        val = cat(1, val, get(self.property));
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'conductivity'
                val = self.conductivity;
            case 'specific_heat'
                val = self.specific_heat;
            case 'source'
                val = self.source;
            otherwise
                val = get(self.property, prop_name);
        end
    end
end
