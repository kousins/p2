% Constructor of linearly elastic,  Inhomogeneous isotropic material.
%
% function retobj = property_linel_iso_inhomog(varargin)
%
% This class represents material properties of deformable linearly elastic isotropic materials.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields
%     E=Young's modulus
%     nu=Poisson ratio
% optional fields
%     alpha = coefficient of thermal expansion
%
function retobj = property_linel_iso_inhomog(varargin)
    class_name='property_linel_iso_inhomog';
    if (nargin == 0)
        parent=property;
        self.E = 1.0;
        self.nu = 0.0;
        self.alpha = 0.0;
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=property(options);
            self.E = options.E;
            self.nu = 0;
            if isfield(options,'nu')
                self.nu = options.nu;
            end
            self.alpha = 0.0;
            if isfield(options,'alpha')
                self.alpha= options.alpha;
            end
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
