% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
    if (nargin == 1)
        val = {{{'E1'}, {'Young''s modulus along the fibers, scalar'}};...
            {{'E2'}, {'Young''s modulus in the isotropy plane, scalar'}};...
            {{'G12'}, {'Shear modulus, fiber/plane, scalar'}};...
            {{'nu12'}, {'Poisson ratio, fiber/plane, scalar'}};...
            {{'nu23'}, {'Poisson ratio in the isotropy plane, scalar'}}};
        val = cat(1, val, get(self.property));
        return;
    else
        prop_name=varargin{1};
        switch prop_name
            case 'E1'
                val = self.E1;
            case 'E2'
                val = self.E2;
            case 'G12'
                val = self.G12;
            case 'nu12'
                val = self.nu12;
            case 'nu23'
                val = self.nu23;
            case 'alpha1'
                val = self.alpha1;
            case 'alpha2'
                val = self.alpha2;
            case 'alphas'
                val = [self.alpha1;self.alpha2;self.alpha3];
            case 'D'
                if nargin >2
                    context =varargin{2};
                else
                    context = struct ([]);
                end
                val = self.D;
            otherwise
                val = get(self.property, prop_name);
        end
    end
end
