% Constructor of linearly elastic, transversely isotropic material.
%
% function retobj = property_linel_transv_iso (varargin)
%
% Constructor of deformable materials: linearly elastic,
% transversely isotropic. The axis 1 is the axis of transverse isotropy.
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
% Options:
% required fields,
%     E1, nu12, E2, G12, and nu23
% optional
%     alpha1, alpha2 (thermal expansion coefficients)
function retobj = property_linel_transv_iso (varargin)
    class_name='property_linel_transv_iso';
    if (nargin == 0)
        parent=property;
        self.D = [];
        self.E1=0;self.E2=0;self.E3=0;
        self.nu12=0;self.nu13=0;self.nu23=0;
        self.G12=0;self.G13=0;self.G23=0;
        self.alpha1= 0;
        self.alpha2= 0;
        self.alpha3= 0;
        retobj = class(self,class_name, parent);
        return;
    elseif (nargin == 1)
        arg = varargin{1};
        if strcmp(class(arg),class_name) % copy constructor.
            retobj = arg;
            return;
        else
            options =varargin{1};
            parent=property(options);
            E1=options.E1;
            nu12=options.nu12;
            E2=options.E2;
            G12=options.G12;
            nu23=options.nu23;
            % now for the transverse isotropy
            G23=E2/2/(1+nu23);
            E3=E2;
            nu13 = nu12;
            G13=G12;
            % compute the compliance, then the stiffness
            compliance =[1/E1      -nu12/E1    -nu13/E1  0   0   0;...
                -nu12/E1     1/E2      -nu23/E2  0   0   0;...
                -nu13/E1   -nu23/E2       1/E3   0   0   0;...
                0           0           0 1/G12 0   0;...
                0           0           0   0 1/G13 0;...
                0           0           0   0   0 1/G23];
            if rank(compliance)<6
                error(' Singular compliance?')
            end
            self.D= inv(compliance);
            self.E1=E1;self.E2=E2;self.E3=E3;
            self.nu12=nu12;self.nu13=nu13;self.nu23=nu23;
            self.G12=G12;self.G13=G13;self.G23=G23;
            self.alpha1= 0;
            self.alpha2= 0;
            self.alpha3= 0;
            % thermal properties
            if isfield(options,'alpha1')
                self.alpha1= options.alpha1;
                self.alpha2= options.alpha2;
                % now for the transverse isotropy
                self.alpha3= options.alpha2;
            end
            retobj = class(self,class_name, parent);
            return;
        end
    else
        error('Illegal arguments!');
    end
    return;
end
