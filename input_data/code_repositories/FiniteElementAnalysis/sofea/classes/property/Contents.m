% property
%   This is a collection of classes implementing various
%   property collections. For instance, the *linel*  classes
%   supply   material moduli for elastic materials. 
% 
% -base class
%   @property/get - Get an attribute
%   @property/set - Set an attribute
%   @property/property - Constructor
% -heat diffusion properties
%   @property_diffusion/get - Get an attribute
%   @property_diffusion/property_diffusion - 
% -linear elastic isotropic homogeneous mechanical properties
%   @property_linel_iso/property_linel_iso - Constructor
%   @property_linel_iso/get - Get an attribute
% -linear elastic isotropic inhomogeneous mechanical properties
%   @property_linel_iso_inhomog/property_linel_iso_inhomog - Constructor
%   @property_linel_iso_inhomog/get - Get an attribute
% -linear elastic transversely isotropic homogeneous mechanical properties
%   @property_linel_transv_iso/property_linel_transv_iso - Constructor
%   @property_linel_transv_iso/get - Got an attribute
% -linear elastic Orthotropic mechanical properties
%   @property_linel_ortho/get - Get an attribute
%   @property_linel_ortho/property_linel_ortho - Constructor


