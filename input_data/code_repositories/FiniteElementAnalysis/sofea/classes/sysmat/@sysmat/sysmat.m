% Constructor of a system matrix.
%
% function retobj = sysmat (varargin)
%
% 
function retobj = sysmat (varargin)
  class_name='sysmat';
  parent=classbase;
  if (nargin == 0)
     self.invalid_eqnum=0;
     retobj = class(self, class_name, parent);
     return;
  elseif (nargin == 1)
     arg = varargin{1};
     if strcmp(class(arg),class_name) % copy constructor.
        retobj = arg;
        return;
     else
      error(['Illegal argument ' inputname(1) ' of class ' class(varargin{1}) '!']);
        return;
     end
  else
     error('Illegal arguments!');
  end
  return;
