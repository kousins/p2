% Assemble a cell array of element matrices.
%
% function retobj = assemble (self, ems)
%
% Assemble a cell array of element matrices, that is object of type elemat,
% into a sparse matrix.
%
function retobj = assemble (self, ems)
    invalid_eqnum = get(self.sysmat, 'invalid_eqnum');
    neqns = length(self.rows);
    A=[];
    if ~self.initialized
        % first collect all connectivity arrays into a rectangular array for easier
        % and faster processing
        ne =length(ems);
        % find the dimension of the largest matrix
        maxnn=0;
        for m = 1:ne
            maxnn=max(maxnn,length(get(ems(m), 'eqnums')));
        end
        % allocate to be big enough for the largest matrix
        conns=zeros(ne,maxnn)+invalid_eqnum;
        for m = 1:ne
            eqnums=get(ems(m), 'eqnums');
            nn=length (eqnums);
            conns(m,1:nn) =reshape(eqnums,1,nn);
            if (sum(conns(m,:)~=invalid_eqnum))==0 % there was no valid number among the equation numbers:
                ems(m)=set(ems(m),'mat',0*get(ems(m),'mat'));% make it possible to ignore the contribution from this matrix
                conns(m,:) =1;% and then arbitrarily assemble all to node 1
            end
        end
        % Now replace invalid equation numbers by repeated valid node indexes
        % (maximum in each row, for simplicity): this does not change the sparsity pattern, but
        % simplifies considerably the code
        mv=max(conns,[],2);  % valid equation numbers in each row
        for i=1:maxnn,
            v=conns(:,i);
            dx=find(v==0);
            v(dx)=mv(dx);
            conns(:,i)=v;
        end
        % Now compute the sparsity pattern, and construct the sparse matrix
        [sppatti, sppattj] =conn_to_sparse(conns,neqns);
        % whatever we initialized A to, it needs to be subtracted later
        % Note: we are using a trick: in order to preserve the sparse pattern, we
        % needs to initialize the matrix to non-zeros in the potentially nonzero
        % entries.  For simplicity, we initialize to 1.0. this needs to be
        % subtracted from the matrix once it is assembled in order not to affect
        % the results.
        A=self.rows;
        for m = 1:length(self.rows)
            midx = find(sppatti==m);
            usppattj = unique(sppattj(midx));
            A{m}=sparse(ones(length(usppattj), 1), usppattj, [0.0], 1, neqns);
            self.rows{m} =A{m};
        end
        self.initialized = true;
    end
    % Now loop over at the element matrices and assemble them
    for m = 1:length(ems)
        if ~placesymm(ems(m)), error ('Unsymmetric matrices not implemented'); end
        eqnums = get(ems(m), 'eqnums');
        mat    = get(ems(m), 'mat');
        emask=eqnums>invalid_eqnum;
        valid_eqnums=eqnums(emask);
        for s= 1:length(emask)
            if (emask(s))
                row =self.rows{eqnums(s)};
                row(valid_eqnums) = row(valid_eqnums) + mat(s,emask);
                self.rows{eqnums(s)} = row;
            end
        end
    end
    % Note: we are using a trick: in order to preserve the sparse pattern, we
    % need to initialize the matrix to non-zeros in the potentially nonzero
    % entries.  For simplicity, we initialize to 1.0. this needs to be
    % subtracted from the matrix once it is assembled in order not to affect
    % the results.
    if ~isempty(A)
        for m = 1:length(self.rows)
            self.rows{m} =self.rows{m}-A{m};
        end
    end
    retobj = self;
    return;
end

    % conns = cell array of connectivity arrays.
function [sppatti, sppattj] = conn_to_sparse(conns, neqns)
    ne=size(conns,1);
    nn=size(conns,2);
    rowp = cell(neqns,1);
    for i=1:ne
        c=unique(conns(i,:));
        for j=1:length(c)
            ij=c(j);
            rowp{ij}= [rowp{ij} c];
        end
    end
    sl=0;
    for i=1:neqns
        rowp{i}= unique(rowp{i});
        sl=sl+length(rowp{i});
    end
    sppatti= zeros (1,sl);
    sppattj= zeros (1,sl);
    k=1;
    for i=1:neqns
        r= rowp{i};
        rn=length(r);
        if ~isempty (r)
            sppatti(k:k+rn-1) =i;
            sppattj(k:k+rn-1) =r;
            k = k+rn;
        end
    end
    sppatti=sppatti(1:k-1);
    sppattj=sppattj(1:k-1);
    return;
end
