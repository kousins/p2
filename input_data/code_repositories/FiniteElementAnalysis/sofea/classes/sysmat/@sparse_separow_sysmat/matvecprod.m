% Extract the diagonal of the system matrix as a sparse matrix.
%
% function retobj = assemble (self, ems)
%
% Assemble a cell array of element matrices, that is object of type elemat,
% into a sparse matrix.
%
function R = matvecprod(self, V)
    if ~self.initialized
        error('Matrix not initialized');
    else
        neqns = length(self.rows);
        R=zeros(neqns,1);
        for m = 1:neqns
            R(m)=self.rows{m}*V;
        end
    end
    return;
end
