% Start matrix assembly.  The matrix is zeroed out.
%
% function retobj = start (self, dim)
%
%   dim=number of equations
%
function retobj = start (self, dim)
    % this is just a placeholder, the matrix has not been initialized yet.
    [self.rows{1:dim}] = deal(0);
    retobj = self;
end


