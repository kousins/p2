% Start matrix assembly.  The matrix is zeroed out.
%
% function retobj = start (self, dim)
%
%   dim=number of equations
%
function retobj = start (self, dim)
  self.mat = zeros(dim,dim);
  retobj = self;
end
  
