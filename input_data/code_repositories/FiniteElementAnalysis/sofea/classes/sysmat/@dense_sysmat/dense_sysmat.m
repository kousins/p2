% Constructor of a dense system matrix
%
% function self = dense_sysmat (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%
function self = dense_sysmat (varargin)
class_name='dense_sysmat';
parent = sysmat;
if (nargin == 0)
    self.mat = [];
    self = class(self,class_name,parent);
    return;
elseif (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) % copy constructor.
        self = arg;
        return;
    else
        error ('Illegal call!');
        return;
    end
else
    error('Illegal arguments!');
end
return;


