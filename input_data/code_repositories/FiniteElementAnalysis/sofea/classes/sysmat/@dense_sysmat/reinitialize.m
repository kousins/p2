% Reinitialize for matrix assembly.  The matrix is zeroed out.
%
% function self = reinitialize (self)
%
%
function self = reinitialize (self)
    if (~isempty(self.mat))
        self.mat = 0 * self.mat;
    end
end


