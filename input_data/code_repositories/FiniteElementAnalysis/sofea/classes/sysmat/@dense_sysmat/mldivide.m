% Solve a system of linear equations. This method implements the
% Matlab "\" operator.
%
% function Solution = mldivide (self, V)
%
%  Use as K\V, where V may be a double array (of compatible dimension to
%  the system matrix), or a sysvec object.
%
function Solution = mldivide (self, V)
    if (strcmp(class(V),'sysvec'))
        V = get (V,'vec'); 
    end
    Solution = self.mat\ (V);
end