% Assemble a cell array of element matrices.
%
% function retobj = assemble (self, ems)
%
% Assemble a cell array of element matrices, that is object of type elemat,
% into a dense matrix.
%
function retobj = assemble (self, ems)
invalid_eqnum = get(self.sysmat, 'invalid_eqnum');
[neqns,neqns] = size(self.mat);
for m = 1:length(ems)
    if placesymm(ems(m))
        eqnums = get(ems(m), 'eqnums');
        mat    = get(ems(m), 'mat');
        valid_eqnums=eqnums(eqnums>invalid_eqnum);
        self.mat(valid_eqnums,valid_eqnums) = self.mat(valid_eqnums,valid_eqnums) ...
            + mat(eqnums>invalid_eqnum,eqnums>invalid_eqnum);
    else
        eqnums_row = get(ems(m), 'eqnums_row');
        eqnums_col = get(ems(m), 'eqnums_col');
        mat    = get(ems(m), 'mat');
        valid_eqnums_row=eqnums_row(eqnums_row>invalid_eqnum);
        valid_eqnums_col=eqnums_col(eqnums_col>invalid_eqnum);
        self.mat(valid_eqnums_row,valid_eqnums_col) = self.mat(valid_eqnums_row,valid_eqnums_col) ...
            + mat(eqnums_row>invalid_eqnum,eqnums_col>invalid_eqnum);
    end
end
retobj = self;
