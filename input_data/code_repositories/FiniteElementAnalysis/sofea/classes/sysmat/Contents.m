% sysmat
%   These are classes that represent the global, system matrix
%   (stiffness, mass, capacity, damping, ...)
% 
% -base class
%   @sysmat/get - Get an attribute
%   @sysmat/sysmat - Constructor
% -dense system matrix
%   @dense_sysmat/start - Initialize the system matrix
%   @dense_sysmat/finish - NOT used
%   @dense_sysmat/get - Get an attribute
%   @dense_sysmat/assemble - Assemble an array of element matrices
%   @dense_sysmat/dense_sysmat - Constructor
%   @dense_sysmat/mldivide - Solve the system of linear equations
%             (implements the Matlab �\� operator)
% -sparse system matrix
%   @sparse_sysmat/start - Initialize the system matrix
%   @sparse_sysmat/finish - NOT used
%   @sparse_sysmat/get - Get an attribute
%   @sparse_sysmat/assemble - Assemble an array of element matrices
%   @sparse_sysmat/sparse_sysmat - Constructor
%   @sparse_sysmat/mldivide - Solve the system of linear equations
%             (implements the Matlab �\� operator)
% -sparse system matrix for Cholesky factorization, optimized for storage
% efficiency and  efficient repeated solution of the system of linear
% equations
%   @sparse_chol_sysmat/start - Initialize the system matrix
%   @sparse_chol_sysmat/finish - NOT used
%   @sparse_chol_sysmat/get - Get an attribute
%   @sparse_chol_sysmat/assemble - Assemble an array of element matrices
%   @sparse_chol_sysmat/sparse_sysmat - Constructor
%   @sparse_chol_sysmat/mldivide - Solve the system of linear equations
%             (implements the Matlab �\� operator)
% -sparse system matrix with separate row storage
%   @sparse_separow_sysmat/start - Initialize the system matrix
%   @sparse_separow_sysmat/finish - NOT used
%   @sparse_separow_sysmat/get - Get an attribute
%   @sparse_separow_sysmat/assemble - Assemble an array of element matrices
%   @sparse_separow_sysmat/diag - Extract the diagonal of the system matrix
%   @sparse_separow_sysmat/matvecprod - Matrix vector product
%   @sparse_separow_sysmat/sparse_separow_sysmat - Constructor
