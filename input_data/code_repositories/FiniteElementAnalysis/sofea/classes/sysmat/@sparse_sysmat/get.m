% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>. 
%
function val = get(self,varargin)
if (nargin == 1)
    val = {{{'dim'}, {'dimension, array '}};
        {{'mat'}, {'sparse data matrix, array [dim,dim]'}}};
    return;
else
    prop_name=varargin{1};
    switch prop_name
        case 'mat'
            val = self.mat;
        case 'dim'
            [val,val] = size(self.mat);
        otherwise
            error(['Unknown property name ''' prop_name '''!']);
    end
end
return;
