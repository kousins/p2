% Constructor of a sparse system matrix.
%
% function retobj = sparse_sysmat (varargin)
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>. 
%
function retobj = sparse_sysmat (varargin)
class_name='sparse_sysmat';
parent=sysmat;
if (nargin == 0)
    self.mat = [];
    self.initialized = ~true;
    retobj = class(self, class_name,parent);
    return;
elseif (nargin == 1)
    arg = varargin{1};
    if strcmp(class(arg),class_name) %# copy constructor.
        retobj = arg;
        return;
    else
        error(['Illegal argument ' inputname(1) ' of class ' class(varargin{1}) '!']);
        return;
    end
else
    error('Illegal arguments!');
end
return;

