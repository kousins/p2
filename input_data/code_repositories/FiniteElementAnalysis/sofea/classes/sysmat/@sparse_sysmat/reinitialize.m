% Start matrix assembly.  The matrix is zeroed out.
%
% function retobj = start (self, dim)
%
%   dim=number of equations
%
function self = reinitialize (self)
    self.mat = 0 * self.mat;
end


