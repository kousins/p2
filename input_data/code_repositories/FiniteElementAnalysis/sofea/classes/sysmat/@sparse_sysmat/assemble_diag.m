% Assemble a vector to the diagonal.
%
% function retobj = assemble_diag(self, D)
%
% Assemble a vector to the diagonal. The vector must be the same length as
% the diagonal of the system matrix. Alternatively, a scalar may be passed
% in, which is then added to the entire diagonal.
%
function self = assemble_diag (self, D)
    n=size(self.mat);
    if (length(D)==1)
        D = zeros(n(1),1)+D;
    end
    for k = 1:n
        self.mat(k,k)=self.mat(k,k) +D(k);
    end
    self = self;
end