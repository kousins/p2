% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>.
%
function val = get(self,varargin)
    if (nargin == 0)
        val =self.get_method;
    else
        val =self.get_method(varargin{:});
    end
end