% Constructor of a sparse system matrix. This class provides methods for
% constructing a sparse-storage system matrix, factorization into a lower
% triangular matrix, and solution of the system of linear equations.
% 
% This class optimizes multiple solutions with different right hand sides
% as it computes the Cholesky factorization Of the system matrix at the
% time of the first solve, and then just does the forward and backward
% substitution for subsequent solves. Note that the factorization is 
% stored in-place (it overwrites the original matrix).
% 
% The forward/backward substitution can take advantage of the
% memory-conserving solvers of the package CXSparse, provided this package
% is installed. In particular, cs_lsolve() can solve the forward
% substitution in-place, and avoid the transpose of the stored triangular
% matrix of the Cholesky factors.
%
% function retobj = sparse_chol_sysmat ()
%
% See discussion of constructors in <a href="matlab:helpwin 'sofea/classes/Contents'">classes/Contents</a>.
%
function self = sparse_chol_sysmat (varargin)
    class_name='sparse_chol_sysmat';
    parent=sysmat;
    Factorized =false;
    invalid_eqnum = get(sysmat, 'invalid_eqnum');
    A = [];
    Ainit=1.0;
    neqns= 0;
    Have_CXSparse =  (exist('cs_lsolve') ==3);% is it a mex?


    function assemble_method (ems)
        % first collect all connectivity arrays into a rectangular array for easier
        % and faster processing
        ne =length(ems);
        % find the dimension of the largest matrix
        maxnn=0;
        for m = 1:ne
            maxnn=max(maxnn,length(get(ems(m), 'eqnums')));
        end
        % allocate to be big enough for the largest matrix
        conns=zeros(ne,maxnn)+invalid_eqnum;
        for m = 1:ne
            eqnums=get(ems(m), 'eqnums');
            nn=length (eqnums);
            conns(m,1:nn) =reshape(eqnums,1,nn);
            if (sum(conns(m,:)~=invalid_eqnum))==0 % there was no valid number among the equation numbers:
                ems(m)=set(ems(m),'mat',0*get(ems(m),'mat'));% make it possible to ignore the contribution from this matrix
                conns(m,:) =1;% and then arbitrarily assemble all to node 1
            end
        end
        % Now replace invalid equation numbers by repeated valid node indexes
        % (maximum in each row, for simplicity): this does not change the sparsity pattern, but
        % simplifies considerably the code
        mv=max(conns,[],2);  % valid equation numbers in each row
        for i=1:maxnn,
            v=conns(:,i);
            dx=find(v==0);
            v(dx)=mv(dx);
            conns(:,i)=v;
        end
        % Now compute the sparsity pattern, and construct the sparse matrix
        [sppatti, sppattj] =conn_to_sparse;
        % whatever we initialized A to, it needs to be subtracted later
        % Note: we are using a trick: in order to preserve the sparse pattern, we
        % needs to initialize the matrix to non-zeros in the potentially nonzero
        % entries.  For simplicity, we initialize to 1.0. this needs to be
        % subtracted from the matrix once it is assembled in order not to affect
        % the results.
        A=sparse(sppatti, sppattj, Ainit, neqns, neqns);
        %             spy(A)
        %             length(sppatti),nnz (A)
        %             clear sppatti  sppattj
        % Now loop over all the element matrices and assemble them
        for m = 1:length(ems)
            if placesymm(ems(m))
                eqnums = get(ems(m), 'eqnums');
                mat    = get(ems(m), 'mat');
                emask=eqnums>invalid_eqnum;
                valid_eqnums=eqnums(emask);
                A(valid_eqnums,valid_eqnums) = A(valid_eqnums,valid_eqnums) + mat(emask,emask);
            else
                eqnums_row = get(ems(m), 'eqnums_row');
                eqnums_col = get(ems(m), 'eqnums_col');
                mat    = get(ems(m), 'mat');
                valid_eqnums_row=eqnums_row(eqnums_row>invalid_eqnum);
                valid_eqnums_col=eqnums_col(eqnums_col>invalid_eqnum);
                A(valid_eqnums_row,valid_eqnums_col) = A(valid_eqnums_row,valid_eqnums_col) ...
                    + mat(eqnums_row>invalid_eqnum,eqnums_col>invalid_eqnum);
            end
        end

        A=A-sparse(sppatti, sppattj, Ainit, neqns, neqns);
        return;
        function [sppatti, sppattj] = conn_to_sparse
            ne=size(conns,1);
            nn=size(conns,2);
            rowp = cell(neqns,1);
            for i=1:ne
                c=unique(conns(i,:));
                for j=1:length(c)
                    ij=c(j);
                    rowp{ij}= [rowp{ij} c];
                end
            end
            sl=0;
            for i=1:neqns
                rowp{i}= unique(rowp{i});
                sl=sl+length(rowp{i});
            end
            sppatti= zeros (1,sl);
            sppattj= zeros (1,sl);
            k=1;
            for i=1:neqns
                r= rowp{i};
                rn=length(r);
                if ~isempty (r)
                    sppatti(k:k+rn-1) =i;
                    sppattj(k:k+rn-1) =r;
                    k = k+rn;
                end
            end
            sppatti=sppatti(1:k-1);
            sppattj=sppattj(1:k-1);
            return;
        end
    end
    self.assemble_method=@assemble_method;

    function assemble_diag_method (D)
        n=size(A);
        for k = 1:n
            A(k,k)=A(k,k) +D(k);
        end
        return;
    end
    self.assemble_diag_method=@assemble_diag_method;

    function x=solve_method (V)
        if (~Factorized)
%             tic;
            A = chol(A,'lower');%[L,p,q] = chol(A)
%             toc
            Factorized = true;
        end
        if (Have_CXSparse)
            x = cs_ltsolve (A, cs_lsolve (A, V)); 
        else
            x = A'\(A\V);% This operation allocates additional storage for A'
        end
        %
    end
    self.solve_method=@solve_method;

    function retobj=start_method (dim)
        neqns =dim;
        Factorized = ~true;
        A = [];
        retobj =self;
    end
    self.start_method=@start_method;

    function retobj=reinitialize_method ()
        retobj=start_method (neqns);
    end
    self.reinitialize_method=@reinitialize_method;

    function val = get_method(varargin)
        if (nargin == 0)
            val = {{{'dim'}, {'dimension, array '}};
                {{'mat'}, {'sparse data matrix, array [dim,dim]'}}};
            return;
        else
            prop_name=varargin{1};
            switch prop_name
                case 'mat'
                    val = A;
                case 'dim'
                    val = neqns;
                otherwise
                    error(['Unknown property name ''' prop_name '''!']);
            end
        end
        return;
    end
    self.get_method=@get_method;

    function retval=set_method(varargin)
        if (nargin == 1)
            retval = {};
            return;
        end
        prop_name=varargin{1};
        val=varargin{2};
        switch prop_name
            otherwise
                error(['Unknown property ''' prop_name '''!'])
        end
        retval=self; % return self
    end
    self.set_method=@set_method;

    self = class(self, class_name, parent);
end
