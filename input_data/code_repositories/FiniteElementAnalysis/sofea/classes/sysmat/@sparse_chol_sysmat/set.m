% Get property from the specified object and return the value.
%
% function val = get(self,varargin)
%
% See discussion of the get/set methods in <a href="matlab:helpwin
% 'sofea/classes/Contents'">classes/Contents</a>.
%
function retval = set(self,varargin)
    if (nargin == 0)
        retval =self.set_method;
    else
        retval =self.set_method(varargin{:});
    end
end