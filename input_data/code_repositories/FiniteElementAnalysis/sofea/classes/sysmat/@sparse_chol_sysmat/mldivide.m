% Solve a system of linear equations. This method implements the
% Matlab "\" operator. The system matrix Is factorized, if needed, and the
% forward and backward substitution are used to extract the solution.  Note
% that the factorization is in place, which means that the matrix is
% overwritten during factorization. It also means that subsequent solves
% will be much more efficient.
%
% function Solution = mldivide (self, V)
%
%  Use as K\V, where V may be a double array (of compatible dimension to
%  the system matrix), or a sysvec object.
%
function Solution = mldivide (self, V)
    if (strcmp(class(V),'sysvec'))
        V = get (V,'vec'); 
    end
    Solution = self.solve_method (V);
end
