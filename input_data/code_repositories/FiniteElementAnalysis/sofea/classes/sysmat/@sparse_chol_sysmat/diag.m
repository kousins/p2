% Extract the diagonal of the system matrix as a sparse matrix.
%
% function retobj = assemble (self, ems)
%
% Assemble a cell array of element matrices, that is object of type elemat,
% into a sparse matrix.
%
function M = diag(self)
    if ~self.initialized
        error('Matrix not initialized');
    else
        neqns = length(self.rows);
        M=zeros(neqns,1);
        for m = 1:neqns
            M(m)=self.rows{m}(m);
        end
        M = sparse((1:neqns),(1:neqns),M);
    end
    return;
end
