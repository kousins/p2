% Assemble a vector to the diagonal.
%
% function retobj = assemble_diag(self, D)
%
% Assemble a vector to the diagonal. The vector must be the same length as
% the diagonal of the system matrix.
%
function self = assemble_diag (self, D)
    self.assemble_diag_method (D)
    self = self;
end