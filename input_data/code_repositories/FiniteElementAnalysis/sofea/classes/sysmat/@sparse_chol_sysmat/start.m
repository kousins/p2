% Start matrix assembly.  The matrix is zeroed out.
%
% function retobj = start (self, dim)
%
%   dim=number of equations
%
function retobj = start (self, dim)
    retobj = self.start_method(dim);
end


