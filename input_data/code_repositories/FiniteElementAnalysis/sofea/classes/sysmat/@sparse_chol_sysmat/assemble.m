% Assemble a cell array of element matrices.
% 
% Note that all elements matrices must be assembled with a *single call* To
% assemble()! Each call of assemble() wipes the matrix clean.
%
% function retobj = assemble (self, ems)
%
% Assemble a cell array of element matrices, that is object of type elemat,
% into a sparse matrix.
%
function self = assemble (self, ems)
    self.assemble_method (ems)
    self = self;
end