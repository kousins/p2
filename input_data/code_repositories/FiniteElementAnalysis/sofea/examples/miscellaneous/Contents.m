% MISCELLANEOUS EXAMPLES
%
% Files
%   <a href="matlab: edit 'circle_area'">circle_area</a>      - Approximation of the area of a circle, T3 triangles
%   <a href="matlab: edit 'circle_areaq'">circle_areaq</a>     - Approximation of the area of a circle, T6 triangles
%   <a href="matlab: edit 'ex614'">ex614</a>            - Example 6.14
%   <a href="matlab: edit 'show_derivatives'">show_derivatives</a> - Interpolation example
%   <a href="matlab: edit 'test_boundary1'">test_boundary1</a>   - Boundary extraction
%   <a href="matlab: edit 'test_measure'">test_measure</a>     - Test of numerical integration on manifolds
%   <a href="matlab: edit 'two_circles'">two_circles</a>      - Interpolation, circle-with-hole domain
