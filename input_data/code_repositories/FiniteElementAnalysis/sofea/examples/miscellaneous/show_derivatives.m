Derivative = [];
zexpr='z=(0.5 *x.^3.*y -9*sin(0.2*x.^2).*y.^2)/100;';
zxexpr='z=(0.5 *3*x.^2.*y -9*0.2*2*x.*cos(0.2*x.^2).*y.^2)/100;';
zyexpr='z=(0.5 *x.^3 -9*sin(0.2*x.^2).*2.*y)/100;';
 
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    'curve 1  line   0 0 8 0',...
    'curve 2  line   8 0 8 6 ',...
    'curve 3  line   8 6 0 6',...
    'curve 4  line   0 6 0 0',...
    'subregion 1  property 1 boundary 1 2 3 4',...
    ['m-ctl-point constant ' num2str(0.85)]
    }, 1.0);
% drawmesh({fens,gcells},'gcells','nodes','shrink', 0.89);
% view(2)
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
xy=get(geom,'values'); x=xy(:,1); y=xy(:,2);


eval([zexpr]);
geom3 = field(struct ('name', ['geom3'],  'data',[x,y,z]));
gv=graphic_viewer;
gv=reset (gv,[]);
for i=1:length (gcells)
    if (~isempty( Derivative))
        conn = get(gcells(i), 'conn'); % connectivity
        x = gather(geom, conn, 'values', 'noreshape'); % coord
        Nder = bfundpar (gcells(i),  [0, 0]);
        Ndersp=bfundsp(gcells(i),Nder,x);
        xyz=get(geom3,'values');
        xyz(conn, 3) =xyz(conn, 3)'*Ndersp(:,Derivative);
        geom3x = field(struct ('name', ['geom3x'],  'data',xyz));
        draw(gcells(i), gv, struct ('x',geom3x, 'u',0*geom3x,...
            'facecolor','red','edgecolor','white', 'shrink',1));
        draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
            'facecolor','none'));
    else
        draw(gcells(i), gv, struct ('x',geom3, 'u',0*geom3,...
            'facecolor','red','edgecolor','white', 'shrink',1));
        draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
            'facecolor','none'));
    end
end
lightangle(135,15)
set(gcf,'Renderer','zbuffer')
set(findobj(gca,'type','surface'),...
    'FaceLighting','flat',...
    'AmbientStrength',.6,'DiffuseStrength',.8,...
    'SpecularStrength',.9,'SpecularExponent',25,...
    'BackFaceLighting','lit')

[fens,gcells] =t3block2d (8, 6, 60, 40, []);
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
xy=get(geom,'values'); x=xy(:,1); y=xy(:,2);
if (~isempty( Derivative))
    if (Derivative==1)
        eval([zxexpr]);
    else
        eval([zyexpr]);
    end
else
    eval([zexpr]);
end
geom3 = field(struct ('name', ['geom3'],  'data',[x,y, z]));
% gv=graphic_viewer;
% gv=reset (gv,[]);
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geom3, 'u',0*geom3,...
        'facecolor','yellow','edgecolor','none', 'shrink',1));
end
cameratoolbar
lightangle(135,15)
set(gcf,'Renderer','zbuffer')
set(findobj(gca,'type','surface'),...
    'FaceLighting','flat',...
    'AmbientStrength',.6,'DiffuseStrength',.8,...
    'SpecularStrength',.9,'SpecularExponent',25,...
    'BackFaceLighting','lit')