R1=25;
R2=15;
center1 =[13 4.2];
center2 =[16 3.];
exact_area =pi*R1^2-pi*R2^2;
errors = []
areas = []
mesh_sizes = [32, 16, 8, 4, 2,1];
for mesh_size = mesh_sizes
    [fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
        ['curve 1 circle  Center ' num2str(center1) '  radius ' num2str(R1)],...
        ['curve 2 circle  Center ' num2str(center2) '  radius ' num2str(R2)],...
        'subregion 1  property 1 boundary 1 hole -2',...
        ['m-ctl-point constant ' num2str(mesh_size)]
        }, 1.0, struct('quadratic',false));
    gv =drawmesh({fens,gcells},'gcells','facecolor','yellow','shrink', 1);
    view(2)
    geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
    feb = feblock (struct ('mater',[], 'gcells',gcells,...
        'integration_rule', tri_rule (1)));
    approximate_area =measure(feb,geom,inline('1','x'));
    errors = [errors abs(exact_area-approximate_area)/exact_area]
    areas = [areas approximate_area];
    disp([' The area is = ' num2str(approximate_area) ', to be compared with ' num2str(exact_area)])
    figure(get(gv,'figure'));
    pause(1);   clear (gv); close(gcf);
end
My_graphics
loglog(mesh_sizes, errors, 'bo-'); grid on
labels('log(mesh size)','log(normalized error)')