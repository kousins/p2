disp('Cooling: transient thermal stresses');
% Parameters:
dim=2; % 1D problem
kappa=[0.37 0; 0 0.37]; % conductivity matrix
cp = 1222;% specific heat
rho=1800;% mass density
s=0; % uniform heat source
Tini=100;
Tcold=20;
integration_order=2;
dt=1.0;
tend=16;
theta = 1; % generalized trapezoidal method
hcscale = 0.0001;
mechscale = 100;
E=90000;
nu=0.3;
integration_order=2;
alpha = 10e-6;
w=0.010;
h=0.020;
n=8;
graphics = ~false;
plot_temperature = ~true;

% Mesh
[fens,gcells] = block2d(w,h,n,2*n,1.0);

% Material: heat conduction
hcprop=property_diffusion(struct('conductivity',kappa,...
    'specific_heat',cp*rho,'source',s));
hcmater = mater_diffusion (struct('property',hcprop));
% Material: mechanical
mechprop = property_linel_iso (struct ('E',E,...
    'nu',nu,'reduction',['strain'],'alpha', alpha));
mechmater=mater_defor_ss_linel_biax(struct('property',mechprop));

% Finite element block: heat conduction
hcfeb = feblock_diffusion (struct (...
    'mater',hcmater,    'gcells',gcells,...
    'integration_rule',gauss_rule(2,integration_order)));
% Finite element block: mechanical
mechfeb = feblock_defor_ss (struct ('mater',mechmater, 'gcells',gcells, ...
    'integration_rule',gauss_rule(2,integration_order)));

% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Displacement
u = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Temperature field
tempn=field(struct('name',['tempn'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));

% Apply thermal EBC's
nl=fenode_select (fens,struct ('box',[0 0 0 h],'inflate',h/1000));
tempn = set_ebc(tempn, nl, nl*0+1, [], nl*0+Tcold);
tempn = apply_ebc (tempn);

% Apply mechanical EBC's
nl=fenode_select (fens,struct ('box',[0 w 0 0],'inflate',h/1000));
u = set_ebc(u, nl, nl*0+1, nl*0+2, nl*0);
u = apply_ebc (u);
nl=fenode_select (fens,struct ('box',[0 0 0 0],'inflate',h/1000));
u = set_ebc(u, nl, nl*0+1, nl*0+1, nl*0);
u = apply_ebc (u);

% Number equations
tempn = numbereqns (tempn);
u = numbereqns (u);

% Assemble the stiffness matrix
S = start (sparse_sysmat, get(u, 'neqns'));
S = assemble (S, stiffness(mechfeb, geom, u));

% Assemble the conductivity matrix
K = start (sparse_sysmat, get(tempn, 'neqns'));
K = assemble (K, conductivity(hcfeb, geom, tempn));
Km = get(K,'mat');

% Assemble the capacity matrix
C = start (sparse_sysmat, get(tempn, 'neqns'));
C = assemble (C, capacity(hcfeb, geom, tempn));
Cm = get(C,'mat');

% Thermal Load
F = start (sysvec, get(tempn, 'neqns'));
F = assemble (F, nz_ebc_loads_conductivity(hcfeb, geom, tempn));

% Initial conditions
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Tini);

% Select output node
outnl=fenode_select (fens,struct ('box',[0 0 h h],'inflate',h/1000));
outdeflection =[0];

if graphics
    gv=graphic_viewer;
    gv=reset (gv,[]);
end

t=0;
while t<tend+0.1*dt
    % solve the mechanical problem
    L = start (sysvec, get(u, 'neqns'));
    L = assemble (L, thermal_strain_loads(mechfeb, geom, u, tempn-Tini));
    L = finish(L);
    u = scatter_sysvec(u, get(S,'mat')\get(L,'vec'));
    outu = gather(u, outnl,'values');
    outdeflection(end+1) = outu(1);
    if graphics
        % Plot
        %     clf;
        if plot_temperature
            dcm=data_colormap(struct ('range',[Tini,Tcold], 'colormap',jet));
            colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, get(tempn,'values'))));
            geom3d=field(struct ('name', ['geom3d'], 'data',[get(geom,'values'), 0*get(tempn,'values')]));
            u3d=field(struct ('name', ['u3d'], 'data',[0*get(geom,'values'), hcscale*get(tempn,'values')]));
            gv=reset (gv,[]);
            set(gca,'CameraPosition',[0.11501 -0.0819162 0.129598],...
                'CameraTarget', [0.005 0.01 0.005],...
                'CameraUpVector',[-0.522464 0.397348 0.754417],...
                'CameraViewAngle',6.028);
            draw(hcfeb, gv, struct ('x',geom3d,'u',0*u3d, 'facecolor','none'));
            draw(hcfeb, gv, struct ('x',geom3d,'u',u3d, 'colorfield',colorfield));
            lighting  none;
            title (['Time =' num2str(t)]);
            pause(0.01);
        else
            cmap=jet;
            fld = field_from_integration_points(mechfeb, geom, u, tempn-Tini, 'Cauchy', 2);
            nvals=get(fld,'values');min(nvals),max(nvals)
            nvalsrange=[-15 55];%[min(nvals),max(nvals)]
            dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
            colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
            gv=reset (gv,struct('limits',[-0.25*w 1.25*w -0.02*h 1.25*h]));
            draw(mechfeb, gv, struct ('x',geom,'u', mechscale*u, 'colorfield',colorfield, 'shrink',1));
            %         draw(mechfeb, gv, struct ('x',geom,'u',0*u, 'facecolor','none'));
            lighting  none;
            title (['Thermal stress. Time =' num2str(t)]);
            colormap(cmap);
            cbh=colorbar;
            set(cbh,...
                'Position',[0.8 0.15 0.1 0.7],...
                'YLim',[0,1],...
                'YTick',[0,1],...
                'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
            set(get(cbh,'XLabel'),'String','\sigma_{yy}');
            pause(0.05);
        end
    end
    % Advance the solution of the thermal problem
    tempn1 = tempn;
    Tn=gather_sysvec(tempn);
    Tn1 = (1/dt*Cm+theta*Km) \ ((1/dt*Cm-(1-theta)*Km)*Tn+get(F,'vec'));
    tempn = scatter_sysvec(tempn1,Tn1);
    t=t+dt
end

