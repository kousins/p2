%  THERMO-MECHANICAL EXAMPLES
% 
%   <a href="matlab: edit 'actuatod1'">actuatod1</a>            - Thermally-Driven actuator 
%   <a href="matlab: edit 'actuatod1h8'">actuatod1h8</a>          - Thermally-Driven actuator, mesh of H8 elements 
%   <a href="matlab: edit 'actuatod2'">actuatod2</a>            - Thermally-Driven actuator, refined mesh 
%   <a href="matlab: edit 'alusteel'">alusteel</a>             - Aluminum/steel assembly
%   <a href="matlab: edit 'alusteelround'">alusteelround</a>        - Aluminum/steel assembly
%   <a href="matlab: edit 'cooling_stress_shock'">cooling_stress_shock</a> - Cooling stress shock
%   <a href="matlab: edit 'thermstrainbeam'">thermstrainbeam</a>      - Beam with thermal strain load
