actuator1;% run the thermal analysis
E= 169000000;
nu =0.278;
alpha = 2.5e-6;
% Material: mechanical
mechprop = property_linel_iso (struct ('E',E,...
    'nu',nu,'alpha', alpha));
mechmater=mater_defor_ss_linel_triax(struct('property',mechprop));

% Finite element block: mechanical
mechfeb = feblock_defor_ss (struct ('mater',mechmater, 'gcells',gcells, ...
    'integration_rule',gauss_rule(3,2)));

u=0*geom;
fenids=fenode_select(fens,struct('box',[x0,x4,y0,y0,z0,z3],...
    'inflate', t/1000)) ; % prescribed displacement
prescribed=fenids*0+1;
comp=[];
val=zeros(length(fenids),1)+0;
u = set_ebc(u, fenids, prescribed, comp, val);
fenids=fenode_select(fens,struct('box',[x0,x0,y0,100*y3,z0,100*z3],...
    'inflate', t/1000)) ; % prescribed displacement
prescribed=fenids*0+1;
comp=fenids*0+1;% x-compe
val=zeros(length(fenids),1)+0;
u = set_ebc(u, fenids, prescribed, comp, val);
u = apply_ebc (u);

u = numbereqns (u);
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(mechfeb, geom, u));
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, thermal_strain_loads(mechfeb, geom, u, theta-T_substrate));
u = scatter_sysvec(u, get(K,'mat')\get(F,'vec'));

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
draw(mechfeb, gv, struct ('x',geom, 'u',0*u,...
    'facecolor','none'));
U=get(u,'values');
Uz=U(:,3);
cmap= jet;
dcm=data_colormap(struct('range',[min(Uz),max(Uz)],'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, Uz)));
draw(mechfeb, gv, struct ('x',geom, 'u',u,...
    'colorfield',colorfield));
draw_colorbar(gv, struct('colormap',cmap,'label','Uz',...
    'minmax',[min(Uz),max(Uz)]))

rotate(gv);
