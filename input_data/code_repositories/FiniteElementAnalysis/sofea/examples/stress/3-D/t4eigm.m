E=210e3;% Pa
nu=0.3;
rho=1e-9;% kg

fens= [fenode(struct('id',1,'xyz',[-1,-2,-1]));
    fenode(struct('id',2,'xyz',[3, 2,-1]));
    fenode(struct('id',3,'xyz',[2,1,-2]));
    fenode(struct('id',4,'xyz',[-1,-1,3]))];
gcells=[gcell_T4(struct('id',1,'conn',(1:4)))];
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tet_rule (4)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = 0*geom; % zero out
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
ks=stiffness(feb, geom, u);
K = assemble (K, ks);
M = start (sparse_sysmat, get(u, 'neqns'));
ms=mass(feb, geom, u)
M = assemble (M, ms);
%
neigvs =12;
[W,Omega]=eig(full(get(K,'mat')+diag(1000000*eps*(1:get(u, 'neqns')))),full(get(M,'mat')));
[Omegas,ix]=sort(diag(Omega));
Omega= diag(Omegas);

% Plot
% gv=graphic_viewer;
% gv=reset (gv,[]);
w=clone(u,'w'); % make a copy of u
for i=1:neigvs
    %     norm(get(K,'mat')*W(:, ix(i))-Omega(i,i)*get(M,'mat')*W(:, ix(i)))
    disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(i,i))/2/pi) ]);
%     clf;
%     w = scatter_sysvec(w, W(:,ix(i)));
%     gv=reset (gv,[]);
%     wn=norm(W(:,ix(i)));
%     w=w*(1/wn);
%     for scale=2*sin((0:1:42)/21*2*pi)
%         gv =reset(clear (gv),struct ('limits', [-3,3,-3,3,-3,3]));
%         draw(feb,gv, struct ('x', geom, 'u', 0*w, 'facecolor','blue'));
%         draw(feb,gv, struct ('x', geom, 'u', scale*w,'facecolor','red'));
%         view(3);
%         pause(0.1);
%     end
%     pause(2); % next eigenvector
end

