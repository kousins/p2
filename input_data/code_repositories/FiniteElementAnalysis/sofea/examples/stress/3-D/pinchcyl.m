disp('Pinched cylinder');
E=3e6;
nu=0.3;
ir=gauss_rule(3, 2);
% ir=hex_6pt_rule();
% ir=tet_rule(4);
thickness = 3.0;
% analytical solution for the vertical deflection under the load
analyt_sol=-1.82488e-5;
graphics = true;
% Mesh
R=300;
L= 600;
neqs=[]; normalized_deflections = [];
for n=8%4:2:16;

    [fens,gcells] = block(90/360*2*pi,L/2,thickness,n,n,1);
    [fens,gcells] = H8_to_H20(fens,gcells);
%     [fens,gcells] = t4block(90/360*2*pi,L/2,thickness,n,n,1);
%     [fens,gcells] = T4_to_T10(fens,gcells);
    % Shape into a cylinder
    for i=1:length (fens)
        xyz=get (fens(i),'xyz');
        a=xyz(1); y=xyz(2); z=xyz(3);
        xyz=[(R-thickness/2+z)*sin(a) y (R-thickness/2+z)*cos(a)];
        fens(i)=set(fens(i),'xyz', xyz);
    end
    % Material
    prop = property_linel_iso (struct('E',E,'nu',nu));
    mater = mater_defor_ss_linel_triax (struct('property',prop));
    % Finite element block
    feb = feblock_defor_ss (struct('mater',mater,'gcells',gcells,...
        'integration_rule',ir));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
    % Define the displacement field
    u   = 0*geom; % zero out
    % Apply EBC's
    % rigid diaphragm
    nl=fenode_select (fens,struct ('box',[-10000 10000 0 0 -10000 10000],'inflate',0.1));
    u = set_ebc(u, nl, nl*0+1, nl*0+1, nl*0);
    u = set_ebc(u, nl, nl*0+1, nl*0+3, nl*0);
    u = apply_ebc (u);
    % plane of symmetry perpendicular to Y
    nl=fenode_select (fens,struct ('box',[-10000 10000 L/2 L/2 -10000 10000],'inflate',0.1));
    u = set_ebc(u, nl, nl*0+1, nl*0+2, nl*0);
    u = apply_ebc (u);
    % plane of symmetry perpendicular to X
    nl=fenode_select (fens,struct ('box',[0 0 -10000 10000 -10000 10000],'inflate',0.1));
    u = set_ebc(u, nl, nl*0+1, nl*0+1, nl*0);
    u = apply_ebc (u);
    % plane of symmetry  perpendicular to Z
    nl=fenode_select (fens,struct ('box',[-10000 10000 -10000 10000 0 0],'inflate',0.1));
    u = set_ebc(u, nl, nl*0+1, nl*0+3, nl*0);
    u = apply_ebc (u);
    % OB(u)
    % Number equations
    u   = numbereqns (u);
    % Assemble the system matrix
    K = start (sparse_sysmat, get(u, 'neqns'));
    K = assemble (K, stiffness(feb, geom, u));
    % Load
    nl=fenode_select (fens,struct ('box',[0 0 L/2 L/2 -1000  1000],'inflate',0.1));
    F = start (sysvec, get(u, 'neqns'));
    F = assemble (F, loads(nodal_load(struct ('id',nl,'dir',[3],'magn',[-1/4/length(nl)])), u));
    % Solve
    u = scatter_sysvec(u, get(K,'mat')\get(F,'vec'));
    corn=fenode_select (fens,struct('box',[0 0 L/2 L/2 -10000 10000],'inflate',R/n/100));
    ucorn= gather(u, corn,'values','noreshape');
    nd=abs(mean(ucorn(:,3))/analyt_sol)
    disp(['Vertical deflection under the load: ' num2str(nd*100) '%'])
    normalized_deflections = [normalized_deflections,nd];
    neqs = [neqs get(u, 'neqns')];

    if graphics
        % Plot
        gv=graphic_viewer;
        gv=reset (gv,struct('limits',1.2*[0,R,0,L/2,0,R]));
        scale=10000000;
        u=slice(u,(1:3),'u');
        draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','none'));
        % draw(feb,gv, struct ('x', x, 'u', +scale*u,'facecolor','red'));

        U=get(u,'values');
        dcm=data_colormap(struct ('range',[min(U(:,3)),max(U(:,3))], 'colormap',jet));
        colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, U(:,3))));
        draw(feb, gv, struct ('x', geom,'u', scale*u, 'colorfield',colorfield));%, 'shrink',0.9
        %
        % for i=1:length(fens)
        %   draw(fens(i),gv, struct ('x', x, 'u', 0*u, 'facecolor','blue'));
        % end

        draw_axes(gv, struct('limits',1.2*[0,R,0,L/2,0,R]));
        view3d rot
    end
end
% plot(neqs, normalized_deflections,'gd-','linewidth',3); hold on
% plot(neqs, normalized_deflections,'ro-','linewidth',3); hold on
% plot(neqs, normalized_deflections,'bx-','linewidth',3); hold on
% plot(h, normalized_deflections,'ks-.','linewidth',3); hold on