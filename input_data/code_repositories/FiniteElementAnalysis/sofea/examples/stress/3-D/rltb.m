%%  Define some auxiliary variables
% clear all classes;
E=1000;
nu=0.4999999;
W=2.5;
H=5;
L= 50;
htol=min([L,H,W])/1000;
uzex =-12.6;
magn = 0.2*uzex/4;
Force =magn*W*H*2;
Force*L^3/(3*E*W*H^3*2/12)
graphics = ~true;

eix=1;
% Average B tetrahedron: Note that this functionality is available only in
% the SOFEA extensions package. If you'd like to try it out, contact the
% author.
% eltyd(eix).mf =@t4block;
% eltyd(eix).blf =@feblock_defor_ss_simplex_avg_b;
% eltyd(eix).integration_rule=tet_rule (1);
% eltyd(eix).surface_integration_rule=tri_rule(1);
% eltyd(eix).styl='ks-';
% eix=eix+1;

% Selective reduced integration hexahedron
eltyd(eix).mf =@block;
eltyd(eix).blf =@feblock_defor_ss_sri;
eltyd(eix).integration_rule=gauss_rule (3,[1,2]);
eltyd(eix).surface_integration_rule=gauss_rule(2, 2);
eltyd(eix).styl='md-';
eix=eix+1;

% Full integration hexahedron
eltyd(eix).mf =@block;
eltyd(eix).blf =@feblock_defor_ss;
eltyd(eix).integration_rule=gauss_rule (3,2);
eltyd(eix).surface_integration_rule=gauss_rule(2, 2);
eltyd(eix).styl='mx--';
eix=eix+1;

% Reduced integration H20
eltyd(eix).mf =@blockH20;
eltyd(eix).blf =@feblock_defor_ss;
eltyd(eix).integration_rule=gauss_rule (3,2);
eltyd(eix).surface_integration_rule=gauss_rule(2, 2);
eltyd(eix).styl='ro-';
eix=eix+1;

% Full integration H20
eltyd(eix).mf =@blockH20;
eltyd(eix).blf =@feblock_defor_ss;
eltyd(eix).integration_rule=gauss_rule (3,3);
eltyd(eix).surface_integration_rule=gauss_rule(2, 2);
eltyd(eix).styl='b*-';
eix=eix+1;

% T10 tetrahedron
eltyd(eix).mf =@t4blockT10;
eltyd(eix).blf =@feblock_defor_ss;
eltyd(eix).integration_rule=tet_rule (4);
eltyd(eix).surface_integration_rule=tri_rule(3);
eltyd(eix).styl='gd-';
eix=eix+1;

mix = 1;
% mesd(mix).mult=1;
% mesd(mix).ns=3;
% mix = mix+1;
%
% mesd(mix).mult=2;
% mesd(mix).ns=2;
% mix = mix+1;

mesd(mix).mult=4;
mesd(mix).ns=(1:4);
mix = mix+1;

for eix = 1:length(eltyd)

    for mix = 1:length(mesd)
        ns=[];
        uzs=[];

        for     n=mesd(mix).ns;
            mult =mesd(mix).mult;
            %% Create the mesh and initialize the geometry
            [fens,gcells]= feval (eltyd(eix).mf, W, L, H,n,mult*n,2*n);
           %         mesh{1}=fens;
            %         mesh{2}=gcells;
            %         drawmesh(mesh,'shrink', 0.9,'facecolor','red'); view (2); pause
            prop = property_linel_iso (struct('E',E,'nu',nu));
            mater = mater_defor_ss_linel_triax (struct('property',prop));

            feb = feval (eltyd(eix).blf, struct ('mater',mater, 'gcells',gcells, 'integration_rule',eltyd(eix).integration_rule));
            geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));

            %% Define the displacement field, and zero it out
            u   = 0*geom;

            %% Apply the EBC''s
            ebc_fenids=fenode_select(fens,struct('box',[0 W 0 0 0 H]));
            ebc_prescribed=ones(length(ebc_fenids),1);
            ebc_comp=[];
            ebc_val=zeros(length(ebc_fenids),1);
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            ebc_fenids=fenode_select(fens,struct('box',[W W 0 L 0 H]));
            ebc_prescribed=ones(length(ebc_fenids),1);
            ebc_comp=ebc_fenids*0+1;
            ebc_val=zeros(length(ebc_fenids),1);
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            u   = apply_ebc (u);

            %% Number the equations
            u   = numbereqns (u);
            disp (['n=' num2str(n) ', number of equations =' num2str(get(u,'neqns'))])

            %% Assemble the stiffness matrix
            K = start (sparse_sysmat, get(u, 'neqns'));
            K = assemble (K, stiffness(feb, geom, u));
            
            %% Assemble the load vector
            fi= force_intensity(struct ('magn',[0;0; magn]));
            bdry_gcells = mesh_bdry(gcells, []);
            bcl = gcell_select(fens, bdry_gcells, ...
                struct ('box',[0 W L L 0 H],'inflate',htol));
            lfeb = feblock_defor_ss(struct ('mater',mater, 'gcells',bdry_gcells(bcl),...
                'integration_rule',eltyd(eix).surface_integration_rule));
            F = start (sysvec, get(u, 'neqns'));
            F = assemble (F, distrib_loads(lfeb, geom, u, fi,2));
            F = finish(F);


            %% Solve K*u=F
            b=get(F,'vec'); 
            x=get(K,'mat')\b;
            %     a=get(K,'mat');
            %     x = bicg(get(K,'mat'),get(F,'vec'), 1e-6, 14000, diag(diag (get(K,'mat'))));
            %     x=0*b;
            %     for it = 1:50
            %         res=b-a*x;
            %         norm (res)
            %         x= x+a\res;
            %     end
            %     [Lf,Uf]=lu(a);
            %     x=Uf\(Lf\b);

            %% Transfer the solution to the field u
            u = scatter_sysvec(u, x);
            ebc_fenids=fenode_select(fens,struct('box',[0 W L L 0 H]));
            uv=gather (u,ebc_fenids,'values','noreshape');
            uz=sum(uv(:, 3))/length(ebc_fenids);
            disp (['   Free end displacement =' num2str(uz)])
            ns=[ns n];
            uzs =[uzs uz];

            %% Plot the displacement field u
            if graphics
                gv=graphic_viewer;
                gv=reset (gv,[]);
                scale=1;
                % draw(feb,gv, struct ('x', geom,'u',0*u, 'facecolor','none'));
                fld = field_from_integration_points(feb, geom, u, [], 'Cauchy', 2);
                nvals=get(fld,'values');
                dcm=data_colormap(struct ('range',[min(nvals),max(nvals)], 'colormap',jet));
                colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
                draw(feb, gv, struct ('x',geom,'u', scale*u, 'colorfield',colorfield, 'shrink',1));
                colormap(jet);
                cbh=colorbar;
                set(cbh,...
                    'Position',[0.8 0.15 0.1 0.7],...
                    'YLim',[0,1],...
                    'YTick',[0,1],...
                    'YTickLabel',{[num2str(min(nvals))],[num2str(max(nvals))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
                set(get(cbh,'XLabel'),'String','\sigma_{y}');
                title ('Stress');
                camset (gv, [362.6179  179.7380  186.8838    4.5277   24.0360   -3.5642   -0.4020   -0.1748  0.8988    3.9476])
%                 camset (gv, [-577.5531  612.4095  322.0260    4.5854   46.3526  -10.6875    0.2718   -0.2643    0.9253    3.5144])
            else
                plot(2*ns,uzs/uzex,eltyd(eix).styl,'linewidth',3); hold on
                figure (gcf); pause (1)
            end
        end
    end
end