
function comp_plate_sym
    % Material:
%     Layerwise Composite Plate Analysis Using an Element Template Methodology
    % J. Engrg. Mech. Volume 124, Issue 5, pp. 587-590 (May 1998)
 E1=181000; E2=10300; E3=10300; G12=7170;  G13=7170; G23=2870;
    nu12= 0.28; nu13= 0.28; nu23= 0.33;
    L=200; W=400; t=8;
    magn = -0.015;
    angles =[-45,+45,-45,+45];
    nLayers =length(angles);
    [nL,nW,nt] =adeal([4,4,3*nLayers]);
    graphics = ~false;

    function l = Layer(XYZ, ts)
        for l=1:nLayers
            if (XYZ(3)<l*t/nLayers) &&  (XYZ(3)>(l-1)*t/nLayers)
                break;
            end
        end
%         l
    end
    function Rm = LayerRm(XYZ, ts)
        Rm= rotmat(angles(Layer(XYZ, ts))/180*pi* [0,0,1]);
    end
    % Mesh
    [fens,gcells] = block(L,W,t,nL,nW,nt);
    [fens,gcells] = H8_to_H20(fens,gcells);
    % Material
    prop = property_linel_ortho (...
        struct('E1',E1,'E2',E2,'E3',E3,'G12',G12,'G13',G13,'G23',G23,'nu12',nu12,'nu13',nu13,'nu23',nu23));
    mater = mater_defor_ss_linel_triax (struct('property',prop));
    % Finite element block
    feb = feblock_defor_ss (struct ('mater',mater,'gcells',gcells, ...
        'integration_rule',gauss_rule (3,2),'Rm',@LayerRm));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
    % Define the displacement field
    u   = 0*geom; % zero out
    % Apply EBC's
    nl=[fenode_select(fens, struct('box', [0,0,-Inf,Inf,-Inf,Inf],...
        'inflate',0.001*t))];
    u   = set_ebc(u,nl,nl*0+1, [], nl*0);
    u   = apply_ebc (u);
    % Number equations
    u   = numbereqns (u);
    % Assemble the system matrix
    K = start (sparse_sysmat, get(u, 'neqns'));
    K = assemble (K, stiffness(feb, geom, u));
    %
    fi= force_intensity(struct ('magn',[0;0; magn]));
    bdry_gcells = mesh_bdry(gcells, []);
    bcl = gcell_select(fens, bdry_gcells, ...
        struct ('box',[-Inf,Inf,-Inf,Inf,t,t],'inflate',t/1e5));
    lfeb = feblock_defor_ss(struct ('mater',mater, 'gcells',bdry_gcells(bcl),...
        'integration_rule',gauss_rule(2,2)));
    F = start (sysvec, get(u, 'neqns'));
    F = assemble (F, distrib_loads(lfeb, geom, u, fi,2));
    u = scatter_sysvec(u, K\F);

    % Plot
    if graphics
        gv=graphic_viewer;
        gv=reset (gv,[]);
        scale=10;
        % draw(feb,gv, struct ('x', geom,'u',0*u, 'facecolor','none'));
        fld = field_from_integration_points(feb, geom, u, [], 'Cauchy', 1);
        nvals=get(fld,'values');
        dcm=data_colormap(struct ('range',[min(nvals),max(nvals)], 'colormap',jet));
        colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
        draw(feb, gv, struct ('x',geom,'u', scale*u, 'colorfield',colorfield, 'shrink',1));
        colormap(jet);
        cbh=colorbar;
        set(cbh,...
            'Position',[0.8 0.15 0.1 0.7],...
            'YLim',[0,1],...
            'YTick',[0,1],...
            'YTickLabel',{[num2str(min(nvals))],[num2str(max(nvals))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
        set(get(cbh,'XLabel'),'String','\sigma_{y}');
        title ('Stress');
        %         view3d rot
        view([0,0])
        %         camset (gv, [362.6179  179.7380  186.8838    4.5277   24.0360   -3.5642   -0.4020   -0.1748  0.8988    3.9476])
        %                 camset (gv, [-577.5531  612.4095  322.0260    4.5854   46.3526  -10.6875    0.2718   -0.2643    0.9253    3.5144])
    end
end
