% Thick plate, small displacement, small strain analysis
disp('Timoshenko: clamped square plate with center load');
% !  Timoshenko: clamped square plate with center load
% ! w_max = 0.0056 * P * a^2 / (E * h^3 / (12 * (1 - nu^2))) =
% !     2.44608 for P=10, a=2, E=1e9, h=0.001, nu=0.3
% ! full model (not reduced by symmetry) -- 289 (displ) + 68 (lagr) unknowns
% ! reduced -- 81 (displ) + 54 (lagr)

% Parameters:
graphics =false; % graphic output
% Mesh
neqs=[]; normalized_deflections = [];
for h=[0.001, 0.005, 0.01, 0.02, 0.05, 0.075, 0.1];
    for nt=1
        for na =12%4:20
            P=2; a=2; E=1e9;  nu=0.3;
            w_max = 0.0056 * P * a^2 / (E * h^3 / (12 * (1 - nu^2)))
            scale = 0.5/w_max;
            % tic;
            %         [fens,gcells] = block (a/2,a/2,h,na,na,nt);
            %         [fens,gcells] = H8_to_H20(fens,gcells);
            % ir=gauss_rule(3, 2);
            [fens,gcells] = t4block (a/2,a/2,h,na,na,nt);
            [fens,gcells] = T4_to_T10(fens,gcells);
            ir= tet_rule(4);
            % Material
            prop = property_linel_iso (struct('E',E,'nu',nu));
            mater = mater_defor_ss_linel_triax (struct('property',prop));
            % Finite element block
            feb = feblock_defor_ss (struct('mater',mater,'gcells',gcells,...
                'integration_rule',ir));
            % Geometry
            geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
            % Define the displacement field
            u   = 0*geom; % zero out
            % Apply EBC's
            ebc_fenids=fenode_select (fens,struct('box',[0 0 0 a/2 0 h],'inflate',h/100));
            ebc_prescribed=ebc_fenids*0+1;
            ebc_comp=ebc_fenids*0+1;
            ebc_val=ebc_fenids*0;
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            ebc_fenids=fenode_select (fens,struct('box',[0 a/2 0 0 0 h],'inflate',h/100));
            ebc_prescribed=ebc_fenids*0+1;
            ebc_comp=ebc_fenids*0+2;
            ebc_val=ebc_fenids*0;
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            ebc_fenids=fenode_select (fens,struct('box',[0 a/2 a/2 a/2 0 h],'inflate',h/100));
            ebc_prescribed=ebc_fenids*0+1;
            ebc_comp=[];
            ebc_val=ebc_fenids*0;
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            ebc_fenids=fenode_select (fens,struct('box',[a/2 a/2 0 a/2 0 h],'inflate',h/100));
            ebc_prescribed=ebc_fenids*0+1;
            ebc_comp=[];
            ebc_val=ebc_fenids*0;
            u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
            u   = apply_ebc (u);
            % Number equations
            u   = numbereqns (u);
            % Assemble the system matrix
            ems = stiffness(feb, geom, u);
            K = start (sparse_sysmat, get(u, 'neqns'));
            K = assemble (K, ems);
            % Load
            F = start (sysvec, get(u, 'neqns'));
            corn=fenode_select (fens,struct('box',[0 0 0 0 0 h],'inflate',h/100));
            evs = loads(nodal_load(struct ('id',corn,'dir',[3],'magn',[-P/4/length(corn)])), u);
            F = assemble (F, evs);
            % Solve
            a = get(K,'mat');
            b = get(F,'vec');
            x = a\b;
            u = scatter_sysvec(u, x);
            ucorn= gather(u, corn,'values','noreshape');
            nd=abs(mean(ucorn(:,3))/w_max)
            %        toc
            normalized_deflections = [normalized_deflections,nd];
            neqs = [neqs get(u, 'neqns')];
            if graphics
                gv=graphic_viewer;
                gv=reset (gv,[]);
                for i=1:length (gcells)
                    draw(gcells(i), gv, struct ('x',geom,'u', scale*u, 'facecolor','none'));
                end
                camset(gv, [-3.3445,-4.2190,3.1679,0.3162,0.5517, -0.3039 , 0, 0,1.0000,10.3396]);
            end
        end
    end
end
% plot(h, normalized_deflections,'gd-','linewidth',3); hold on
% plot(neqs, normalized_deflections,'ro-','linewidth',3); hold on
% plot(neqs, normalized_deflections,'bx-','linewidth',3); hold on
% plot(h, normalized_deflections,'ks-.','linewidth',3); hold on