E=0.1e6;% Pa
nu=0.3;
rho=1000;% kg
R= 25.0e-3;% m
t= 2.0e-3;% m
rand('state',0);% try to comment out this line and compare
%                   results for several subsequent runs

% Mesh
[fens,gcells] = t4cylinderdel(t,R,1,4);
[fens,gcells] = T4_to_T10(fens,gcells);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tet_rule (4)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = 0*geom; % zero out
% Apply EBC's
for i=1:length(fens)
    xyz = get(fens(i),'xyz');
    if abs(R-norm(xyz(2:3))) <0.05*R
        u   = set_ebc(u, i, [1], [], 0.0);
    end
end
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
M = start (sparse_sysmat, get(u, 'neqns'));
M = assemble (M, mass(feb, geom, u));

%
neigvs = 4;
[W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');
[Omegas,ix]=sort(diag(Omega));
Omega= diag(Omegas);

%
lambda_ij2 = 10.22;
disp(['  Analytical eigenvalue 1: '  ...
    num2str(lambda_ij2/(2*pi*R^2)*sqrt (E*t^3/(12*rho*t*(1-nu^2)))) ' Hz']);
lambda_ij2 = 21.26;
disp(['  Analytical eigenvalue 2, 3: '  ...
    num2str(lambda_ij2/(2*pi*R^2)*sqrt (E*t^3/(12*rho*t*(1-nu^2)))) ' Hz']);
lambda_ij2 = 34.88;
disp(['  Analytical eigenvalue 4: '  ...
    num2str(lambda_ij2/(2*pi*R^2)*sqrt (E*t^3/(12*rho*t*(1-nu^2)))) ' Hz']);

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
scale=0.00025;
w=clone(u,'w'); % make a copy of u
for i=1:neigvs
    norm(get(K,'mat')*W(:, ix(i))-Omega(i,i)*get(M,'mat')*W(:,ix(i)))
    disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(i,i))/2/pi) ]);
    clf;
    w = scatter_sysvec(w, W(:,ix(i)));
    wv = get(w,'values');
    wmag = sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2);
    dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
    colors=map_data(dcm, wmag);
    colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
    gv=reset (gv,[]);
    set(gca,'FontSize',16)
    camset(gv,[-0.1591    0.0085   -0.3252    0.0001    0.0043    0.0013   -0.8985    0.0237    0.4383    5.4322]);
    %     draw(feb,gv, struct ('x', geom,'u',0*w, 'facecolor','none'));
    draw(feb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
    text(0,1.1*R,1.1*R,['\omega_' num2str(i) '=' num2str(sqrt(Omega(i,i))/2/pi) ],'FontSize',24);
    axis off
    saveas(gcf, ['drum_t10-' num2str(i) '.png'], 'png');
    pause(2); % next eigenvector
end

