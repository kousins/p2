function gv =pinchsphere
    disp('Pinched sphere');
    E=6.825e7;
    nu=0.3;
    ir=gauss_rule(3, 2);
    % ir=hex_6pt_rule();
    % ir=tet_rule(4);
    thickness = 0.04;
    % analytical solution for the vertical deflection under the load
    analyt_sol=0.0924;
    graphics = true;
    % Mesh
    R=10;
    nlayers =1;
    function xyz= radially(xyz, layer)
        xyz= (R-thickness/2+ layer/nlayers*thickness)*xyz/norm(xyz);
    end
    function [fens,gcells] = move_to_sphere(fens,gcells)
        bg=mesh_bdry(gcells, struct('other_dimension', 1));
        for i=1:length(bg)
            N = bfun (bg(i), [0, 0]);
            Nder = bfundpar (bg(i), [0, 0]);
            conn = get(bg(i), 'conn');
            x = zeros(length(conn), 3);
            for  j = 1:length(conn)
                x(j, :)= get(fens(conn(j)),'xyz');
            end
            tangents =x'*Nder;
            normal =skewmat(tangents(:,1))*tangents(:,2);
            xyz =N'*x;
            if (xyz*normal)/norm(xyz)/norm(normal)>0.8
                for  j = 1:length(conn)
                    fens(conn(j))= set(fens(conn(j)),'xyz',(R+ thickness/2)*x(j, :)/norm(x(j, :)));
                end
            end
            if (xyz*normal)/norm(xyz)/norm(normal)<-0.8
                for  j = 1:length(conn)
                    fens(conn(j))= set(fens(conn(j)),'xyz',(R- thickness/2)*x(j, :)/norm(x(j, :)));
                end
            end
        end
    end
    neqs=[]; normalized_deflections = [];
    for n=1:3%4:2:16;

        [fens,gcells]=q4sphere(R,n,thickness);
        [fens,gcells] = extrudeq4(fens,gcells,nlayers,@radially);
        [fens,gcells] = H8_to_H20(fens,gcells);
% length(fens)
%         [fens,gcells] = move_to_sphere(fens,gcells);
%                 drawmesh({fens,gcells},'gcells','facecolor','red')
        %     [fens,gcells] = t4block(90/360*2*pi,L/2,thickness,n,n,1);
        %     [fens,gcells] = T4_to_T10(fens,gcells);
        % Material
        prop = property_linel_iso (struct('E',E,'nu',nu));
        mater = mater_defor_ss_linel_triax (struct('property',prop));
        % Finite element block
        feb = feblock_defor_ss (struct('mater',mater,'gcells',gcells,...
            'integration_rule',ir));
        % Geometry
        geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
        % Define the displacement field
        u   = 0*geom; % zero out
        % Apply EBC's
        % plane of symmetry perpendicular to Y
        nl=fenode_select (fens,struct ('box',[-10000 10000 0 0 -10000 10000],'inflate',0.1));
        u = set_ebc(u, nl, nl*0+1, nl*0+2, nl*0);
        u = apply_ebc (u);
        % plane of symmetry perpendicular to X
        nl=fenode_select (fens,struct ('box',[0 0 -10000 10000 -10000 10000],'inflate',0.1));
        u = set_ebc(u, nl, nl*0+1, nl*0+1, nl*0);
        u = apply_ebc (u);
        % top
        nl=fenode_select (fens,struct ('box',[0 0 0 0 R R],'inflate',0.001));
        u = set_ebc(u, nl, nl*0+1, [], nl*0);
        u = apply_ebc (u);
        % OB(u)
        % Number equations
        u   = numbereqns (u);
        % Assemble the system matrix
        K = start (sparse_sysmat, get(u, 'neqns'));
        K = assemble (K, stiffness(feb, geom, u));
        % Load
        F = start (sysvec, get(u, 'neqns'));
        nl=fenode_select (fens,struct ('box',[0 0 -10000 10000 0  0],'inflate',0.001));
        F = assemble (F, loads(nodal_load(struct ('id',nl,'dir',[2],'magn',[-1/length(nl)])), u));
        nl=fenode_select (fens,struct ('box',[-10000 10000 0 0 0  0],'inflate',0.001));
        F = assemble (F, loads(nodal_load(struct ('id',nl,'dir',[1],'magn',[1/length(nl)])), u));
        % Solve
        u = scatter_sysvec(u, get(K,'mat')\get(F,'vec'));
        corn=fenode_select (fens,struct('box',[0 0 -10000 10000 0  0],'inflate',R/n/100));
        ucorn= gather(u, corn,'values','noreshape');
        nd=abs(mean(ucorn(:,2))/analyt_sol)
        disp(['Deflection under the load: ' num2str(nd*100) '%'])
        normalized_deflections = [normalized_deflections,nd];
        neqs = [neqs get(u, 'neqns')];

        if graphics
            % Plot
            gv=graphic_viewer;
            gv=reset (gv,struct('limits',1.2*[0,R,0,R,0,R]));
            camset(gv, [ 63.9770   83.3364   44.1803    6.8828    5.1908 6.3187   -0.2163   -0.2932    0.9313    6.3354]);
            set(gca,'FontSize', 14)
          text(R,R,R,['Deflection under the load: ' num2str(nd*100) '%'])
              scale=20;
            u=slice(u,(1:3),'u');
            draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','none'));
            % draw(feb,gv, struct ('x', x, 'u', +scale*u,'facecolor','red'));

            U=get(u,'values');
            dcm=data_colormap(struct ('range',[min(U(:,3)),max(U(:,3))], 'colormap',jet));
            colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, U(:,3))));
            draw(feb, gv, struct ('x', geom,'u', scale*u, 'colorfield',colorfield));%, 'shrink',0.9
            %
            % for i=1:length(fens)
            %   draw(fens(i),gv, struct ('x', x, 'u', 0*u, 'facecolor','blue'));
            % end

            draw_axes(gv, struct('limits',1.2*[0,R,0,R,0,R]));
            view3d rot
        end
    end
    % plot(neqs, normalized_deflections,'gd-','linewidth',3); hold on
    % plot(neqs, normalized_deflections,'ro-','linewidth',3); hold on
    % plot(neqs, normalized_deflections,'bx-','linewidth',3); hold on
    % plot(h, normalized_deflections,'ks-.','linewidth',3); hold on
end