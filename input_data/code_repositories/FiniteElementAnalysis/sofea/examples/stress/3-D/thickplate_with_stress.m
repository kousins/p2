function doit
    % Thick plate, small displacement, small strain analysis
    disp('Thick plate, small displacement, small strain analysis');

    % Parameters:
    E=1e3;
    nu=0.2;
    graphics =1; % graphic output
    uscale = 4;
    comp =6;

    % Mesh
    n=2;
    tic; % for n= 10, that is almost 14,000 equations, we need about half a minute
    [fens,gcells] = blockH20(4,4,0.5,2*n,2*n,n);
    % Material
    prop = property_linel_iso (struct('E',E,'nu',nu));
    mater = mater_defor_ss_linel_triax (struct('property',prop));
    % Finite element block
    feb = feblock_defor_ss (struct('mater',mater,'gcells',gcells,...
        'integration_rule',gauss_rule(3, 2)));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
    % Define the displacement field
    u   = 0*geom; % zero out
    % Apply EBC's
    ebc_fenids=fenode_select (fens,struct('box',[-10 10 4 4 -10 10]));
    ebc_prescribed=ebc_fenids*0+1;
    ebc_comp=[];
    ebc_val=ebc_fenids*0;
    u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
    u   = apply_ebc (u);
    % Number equations
    u   = numbereqns (u);
    % Assemble the system matrix
    ems = stiffness(feb, geom, u);
    K = start (sparse_sysmat, get(u, 'neqns'));
    K = assemble (K, ems);
    % Load
    fi= force_intensity(struct ('magn',@(x)(-0.25*(x(1)-2)*[0, 0, 1])));
    bdry_gcells = mesh_bdry(gcells, []);
    bcl = gcell_select(fens, bdry_gcells, ...
        struct ('box',[-10 10 -10 10 0.5 0.5],'inflate',0.001));
    lfeb = feblock_defor_ss(struct ('mater',mater, 'gcells',bdry_gcells(bcl),...
        'integration_rule',gauss_rule(2, 2)));
    F = start (sysvec, get(u, 'neqns'));
    F = assemble (F, distrib_loads(lfeb, geom, u, fi, 2));
    F = finish(F);
    % Solve
    a = get(K,'mat');
    b = get(F,'vec');
    x = a\b;
    u = scatter_sysvec(u, x);
    toc

    if graphics

        gv=graphic_viewer;
        gv=reset (gv,[]);
        % Continuous stress
        fld = field_from_integration_points(feb, geom, u, [], 'Cauchy', comp);
        nvals=get(fld,'values'); [min(nvals),max(nvals)]
        dcm=data_colormap(struct ('range', [min(nvals),max(nvals)], 'colormap',jet));
        colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
        draw(feb, gv, struct ('x',geom,'u', uscale*u, 'colorfield',colorfield, 'shrink',1));
        camset(gv, [ 37.2735  -21.7348 25.3973    1.8154 1.7344   -0.1523  -0.4295    0.2843 0.8572    4.1432])

        gv=graphic_viewer;
        gv=reset (gv,[]);
        % Ellipsoid representation of stress
        draw(feb, gv, struct ('x',geom,'u', uscale*u, 'facecolor','none'));
        id.comp=comp;
        id.container=-Inf;
        id=inspect_integration_points(feb, geom, u, [],...
            (1:length (gcells)), struct ('output',['Cauchy']),...
            @mx,id)
        max_stress=id.container
        id.container=Inf;
        id=inspect_integration_points(feb, geom, u, [], ...
            (1:length (gcells)), struct ('output',['Cauchy']),...
            @mn,id)
        min_stress =id.container
        dcm=data_colormap(struct ('range',[min(nvals),max(nvals)], 'colormap',jet));
        draw_integration_points(feb,gv,struct ('x',geom,'u',u,'u_displ',uscale*u, 'scale',0.03,'component',comp,'data_cmap', dcm));
        camset(gv, [ 37.2735  -21.7348 25.3973    1.8154 1.7344   -0.1523  -0.4295    0.2843 0.8572    4.1432])

    end
end

function id= mn(id,out,xyz,pc)
    id.container=min(out(id.comp), id.container);
end

function id= mx(id,out,xyz,pc)
    id.container=max(out(id.comp), id.container);
end

