% 3-D STRESS ANALYSIS EXAMPLES
%
% Files
%   <a href="matlab: edit 'clsqlconc'">clsqlconc</a>              - Clamped square plate with center load,
%                            hexahedral mesh
%   <a href="matlab: edit 'clsqlconct'">clsqlconct</a>             - Clamped square plate with center load,
%                             tetrahedral mesh
%   <a href="matlab: edit 'drum_t10'">drum_t10</a>               - Circular clamped plate, free vibration
%   <a href="matlab: edit 'drum_t4'">drum_t4</a>                - Circular clamped plate, free vibration
%   <a href="matlab: edit 'MechPartVibr'">MechPartVibr</a>           -  Mechanical part modal analysis
%   <a href="matlab: edit 'neinastranb'">neinastranb</a>            - Clamped beam
%   <a href="matlab: edit 'pinchcyl'">pinchcyl</a>               - Pinched cylinder Shell problem
%   <a href="matlab: edit 'pinchsphere'">pinchsphere</a>            - Pinched sphere
%   <a href="matlab: edit 'rltb'">rltb</a>                   - Incompressible beam
%   <a href="matlab: edit 't4eigm'">t4eigm</a>                 - Tetrahedron stiffness matrix eigenvalue
%                            problem
%   <a href="matlab: edit 'thickplate_with_stress'">thickplate_with_stress</a> - Thick plate, Show stress
%   <a href="matlab: edit 'twist_t10'">twist_t10</a>              - Twisted composite Beam
%   <a href="matlab: edit 'twist_t4'">twist_t4</a>               - Twisted composite Beam
%   <a href="matlab: edit 'Rod_cable_analogy'">Rod_cable_analogy</a>      - Investigation of cable vibration via the rod analogy
%   <a href="matlab: edit 'Rod_cable_analogy_big'">Rod_cable_analogy_big</a>  -  Bigger mesh for the above
