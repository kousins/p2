% Investigation of cable vibration via the rod analogy
E=200000;
nu=0.;
rho=150;
L= 0.5;
t= 0.01;
N =75;
P=20;% N
mu= 0.015;% kg/m
% Pinned-slider cable supports: analytical natural frequencies
analyt_Natural =@(k)(sqrt(P/mu)*((2*k-1)*(1/2)*pi/L));
graphics = false;

% Mesh
[fens,gcells] = t4block(t,t,L,3,3,N);
[fens,gcells] = T4_to_T10(fens,gcells);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
[fens, gcells]=renum_mesh(fens, gcells, 'symamd');
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tet_rule(4)));

% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = 0*geom; % zero out
% Apply EBC's
nl=[fenode_select(fens, struct('box', [0,0,0,t,0,L],...
    'inflate',0.001)),fenode_select(fens, struct('box', [t,t,0,t,0,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+1, nl*0);
nl=[fenode_select(fens, struct('box', [0,t,0,0,0,L],...
    'inflate',0.001)),fenode_select(fens, struct('box', [0,t,t,t,0,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+2, nl*0);
nl=[fenode_select(fens, struct('box', [0,t,0,t,L,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+3, nl*0);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
K = start (sparse_chol_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
M = start (sparse_sysmat, get(u, 'neqns'));
M = assemble (M, mass(feb, geom, u));
get(u, 'neqns')
%
neigvs = 20;
options.issym= true;
options.tol=1e-80;
options.maxit= 500;
options.disp= 0;
[W,Omega]=eigs(@(b)(K\b),get(u, 'neqns'),get(M,'mat'),neigvs,'SM', options);
frequency1=sqrt(Omega(1, 1))/2/pi;
[Ignore,ix] =sort (diag(Omega));

for i=1:5
    disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(ix(i),ix(i)))) ' (analyt=' num2str(analyt_Natural(i)) ')']);
end
    
% Plot
if graphics
    bg=mesh_bdry(gcells);
    bfeb = feblock(struct ('gcells',bg));
    gv=graphic_viewer;
    gv=reset (gv,[]);
    scale=0.00125;
    w=clone(u,'w'); % make a copy of u
    for i=1:5
        disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(ix(i),ix(i)))) ' (analyt=' num2str(analyt_Natural(i)) ')']);
        clf;
        w = scatter_sysvec(w, W(:,ix(i)));
        wv = get(w,'values');
        wmag = sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2);
        dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
        colors=map_data(dcm, wmag);
        colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
        gv=reset (gv,[]);
        set(gca,'FontSize',16)
        camset(gv,[2.4206   -2.2330   -2.6369   -0.0082   -0.0024    0.2399    0.8318    0.3401 0.4386    3.5434]);
        %         draw(feb,gv, struct ('x', geom,'u',0*w, 'facecolor','none'));
        draw(bfeb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
        draw_axes(gv)
        %         text(3.1*R,4.1*R,4.1*R,['\omega_' num2str(i) '=' num2str(sqrt (Omega(i,i))) ],'FontSize',24);
        axis off
        for xscale=scale*sin((0:1:42)/21*2*pi)
            reset (gv,[]);
            camset(gv,[2.4206   -2.2330   -2.6369   -0.0082   -0.0024    0.2399    0.8318    0.3401 0.4386    3.5434]);
            draw(bfeb,gv, struct ('x', geom, 'u', xscale*w,'colorfield',colorfield));
%             headlight(gv);
            pause(0.005);
        end
    %     saveas(gcf, ['twist_t4-' num2str(i) '.png'], 'png');
        pause(2); % next eigenvector
    end
end
