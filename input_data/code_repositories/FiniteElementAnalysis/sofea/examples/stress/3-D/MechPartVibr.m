% MPART small strain, small displacement modal analysis
% Parameters:

E=1e7;
nu=0.3;
integration_order=2;
ebc_fenids=[49 50 53 54 59 60 61 62 65 66];
ebc_prescribed=ones(length(ebc_fenids),1);
ebc_comp=[];
ebc_val=zeros(length(ebc_fenids),1);

% Mesh
[fens,gcells] = mpart;
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (3,integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = 0*geom; % zero out
% Apply EBC's
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = start (dense_sysmat, get(u, 'neqns'));
K = assemble (K, ems);
% Load
F = start (sysvec, get(u, 'neqns'));
evs = loads(nodal_load(struct ('id',[13,16],'dir',[3],'magn',[-10])), u);
F = assemble (F, evs);
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
scale=100000;
draw(feb,gv, struct ('x', geom,'u',0*u, 'facecolor','none'));
draw(feb,gv, struct ('x', geom,'u',+scale*u,'facecolor','red','shrink', 0.8));
xlabel('X');
ylabel('Y');
zlabel('Z');
title (' done with static analysis');
hold on; pause(2)

%
pause(2); % Assemble the mass matrix
M = sparse_sysmat;
M = start (M, get(u, 'neqns'));
ems = mass(feb, geom, u);
M = assemble (M, ems);
M = finish (M);

%
neigvs = 2;
[W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');

scale=10;
w=clone(u,'w'); % make a copy of u
for i=1:neigvs
    disp(['  Eigenvector ' num2str(i) ' eigenvalue ' num2str(Omega(i,i)) ]);
    clf;
    w = scatter_sysvec(w, W(:,i));
    wv = get(w,'values');
    wmag = sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2);
    dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
    colors=map_data(dcm, wmag);
    colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
    gv=reset (gv,[]);
    camset(gv,[-61.9200  -83.3021   60.6218    2.0000   -0.0000    0.0000         0         0    1.0000  5.3799]);
    draw(feb,gv, struct ('x', geom,'u',0*w, 'facecolor','none'));
    draw(feb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
    pause(2); % next eigenvector
end

