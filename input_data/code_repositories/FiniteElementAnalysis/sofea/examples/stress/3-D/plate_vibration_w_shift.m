E=200000;% Young's modulus
nu=0.28;% Poisson ratio
rho=7.800e-9;% mass density
OmegaShift=(2*pi*4) ^ 2;
W=2000;
H=50;
L =3000;
nL=200; nW=40; nH =4;
graphics = false;

% Mesh
[fens,gcells]= block(L, W, H, nL, nW, nH);
% drawmesh({fens,gcells},'gcells','facecolor','red','shrink',0.85)
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule(3,2)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
limits =reshape([min(get(geom,'values'));max(get(geom,'values'))], 6, 1);
% Define the displacement field
u   = 0*geom; % zero out
% No EBC's: free-floating
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);

% Assemble the system matrix
disp(['Stiffness ' num2str(get(u, 'neqns')) 'x' num2str(get(u, 'neqns')) ' assembly']);
tic
K = start (sparse_sysmat, get(u, 'neqns'));
evs=stiffness(feb, geom, u);
K = assemble (K, evs);
toc
clear all; return
M = start (sparse_sysmat, get(u, 'neqns'));
ems=mass(feb, geom, u);
% If lumped mass matrix, uncomment this block
for j=1:length(ems)% Hinton, Rock, Zienkiewicz lumping
    Me=get(ems(j),'mat');
    em2=sum(sum(Me));
    dem2=sum(diag(Me));
    ems(j)=set(ems(j),'mat',diag(diag(Me)/dem2*em2));
end
% % Consistent or lumped mass matrix
M = assemble (M, ems);
disp(['Number of DOF =' num2str(get(u, 'neqns'))]);
%
neigvs = 10;
options.issym= true;
options.tol=1e-35;
options.maxit= 500;
options.disp= 2;
[W,Omega]=eigs(get(K,'mat')+OmegaShift*get(M,'mat'),get(M,'mat'),neigvs,'SM', options);
Omega =diag(diag(Omega)-OmegaShift);
[Ignore,ix] =sort (diag(Omega));


for i=1:neigvs
        disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' [Hz]']);
end


% Plot
if graphics
    Camera =1.0e+003 * [   -1.0612
        -0.8377
        1.0948
        -0.0775
        0.0025
        0.0093
        0
        0
        0.0010
        0.0033];
    xyz=get(geom,'values');
    box =reshape([min(xyz);max(xyz)],1,6);
    annloc=[mean(box(1:2)), mean(box(3:4))-mean(box(1:2)), mean(box(5:6))];
    bg=mesh_bdry(gcells);
    bfeb = feblock(struct ('gcells',bg));
    gv=graphic_viewer;
    gv=reset (gv,struct('limits',  limits));
    scale=0.05;
    w=clone(u,'w'); % make a copy of u
    for i=1:neigvs
        disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' [Hz]']);
%         clf;
        w = scatter_sysvec(w, W(:,ix(i)));
        wv = get(w,'values');
        wmag = real (sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2));
        dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
        colors=map_data(dcm, wmag);
        colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
        for xscale=scale*sin((0:1:42)/21*2*pi)
            gv=reset (gv,[]);
            camset(gv,Camera);
            draw(bfeb,gv, struct ('x', geom, 'u', xscale*w,'colorfield',colorfield));
            draw_text(gv, annloc,['  Eigenvector ' num2str(i) '\newline     ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' [Hz]'],...
                struct('fontsize', 18));
            draw_axes (gv,struct('length',20 ));
            pause(0.005);
        end
        %     saveas(gcf, ['twist_t4-' num2str(i) '.png'], 'png');
        pause(2); % next eigenvector
        gv=reset (gv,[]);
        camset(gv,Camera);
        draw(bfeb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
        draw(bfeb,gv, struct ('x', geom,'u',0*w,'facecolor','yellow','edgecolor',.53+[0, 0, 0],'alpha', 0.2));
        draw_text(gv, annloc,['  Eigenvector ' num2str(i) '\newline     ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) '  [Hz]'],...
            struct('fontsize', 18));
        draw_axes (gv,struct('length',20 ));
        pause(2.55);
        
    end
end
