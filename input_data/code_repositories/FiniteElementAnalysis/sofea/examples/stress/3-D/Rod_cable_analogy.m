
% Investigation of cable vibration via the rod analogy
E=200000;% Young's modulus
nu=0.;% Poisson ratio
rho=150;% mass density
L= 0.5;% length
t= 0.01;% cross-sectional dimension
N =10;% number of elements along the length
P=20;% Prestress force, N
mu= 0.015;% Mass density of the cable, kg/m
analyt_Natural =@(k)(sqrt(P/mu)*((2*k-1)*(1/2)*pi/L));
graphics = ~false;

% Mesh
[fens,gcells] = t4block(t,t,L, 1,1,N);
[fens,gcells] = T4_to_T10(fens,gcells);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_triax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tet_rule(4)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = 0*geom; % zero out
% Apply EBC's
nl=[fenode_select(fens, struct('box', [0,0,0,t,0,L],...
    'inflate',0.001)),fenode_select(fens, struct('box', [t,t,0,t,0,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+1, nl*0);
nl=[fenode_select(fens, struct('box', [0,t,0,0,0,L],...
    'inflate',0.001)),fenode_select(fens, struct('box', [0,t,t,t,0,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+2, nl*0);
nl=[fenode_select(fens, struct('box', [0,t,0,t,L,L],...
    'inflate',0.001))];
u   = set_ebc(u,nl,nl*0+1, nl*0+3, nl*0);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
M = start (sparse_sysmat, get(u, 'neqns'));
ems=mass(feb, geom, u);
% If lumped mass matrix, uncomment this block
% for j=1:length(ems)% Hinton, Rock, Zienkiewicz lumping
%     Me=get(ems(j),'mat');
%     em2=sum(sum(Me));
%     dem2=sum(diag(Me));
%     ems(j)=set(ems(j),'mat',diag(diag(Me)/dem2*em2));
% end
% Consistent or lumped mass matrix
M = assemble (M, ems);
disp(['Number of DOF =' num2str(get(u, 'neqns'))]);
%
neigvs = 20;
options.issym= true;
options.tol=1e-80;
options.maxit= 500;
options.disp= 0;
[W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM', options);
frequency1=sqrt(Omega(1, 1))/2/pi;
[Ignore,ix] =sort (diag(Omega));

% Plot
if graphics
    bg=mesh_bdry(gcells);
    bfeb = feblock(struct ('gcells',bg));
    gv=graphic_viewer;
    gv=reset (gv,[]);
    scale=0.00125;
    w=clone(u,'w'); % make a copy of u
    for i=1:5
        disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' (analyt=' num2str(analyt_Natural(i)/2/pi) ') [Hz]']);
        clf;
        w = scatter_sysvec(w, W(:,ix(i)));
        wv = get(w,'values');
        wmag = sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2);
        dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
        colors=map_data(dcm, wmag);
        colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
        gv=reset (gv,[]);
        set(gca,'FontSize',16)
        camset(gv,[2.4206   -2.2330   -2.6369   -0.0082   -0.0024    0.2399    0.8318    0.3401 0.4386    3.5434]);
        draw(bfeb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
        draw_text(gv, [0, -L/2, L/3],['  Eigenvector ' num2str(i) '\newline     ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' (analyt=' num2str(analyt_Natural(i)/2/pi) ') [Hz]'],...
            struct('fontsize', 18));
        draw_axes (gv,struct('length',L/10 ));
        pause(1.55);
        for xscale=scale*sin((0:1:42)/21*2*pi)
            reset (gv,[]);
            camset(gv,[2.4206   -2.2330   -2.6369   -0.0082   -0.0024    0.2399    0.8318    0.3401 0.4386    3.5434]);
            draw(bfeb,gv, struct ('x', geom, 'u', xscale*w,'colorfield',colorfield));
            draw_text(gv, [0, -L/2, L/3],['  Eigenvector ' num2str(i) '\newline     ' num2str(sqrt(Omega(ix(i),ix(i)))/2/pi) ' (analyt=' num2str(analyt_Natural(i)/2/pi) ') [Hz]'],...
                struct('fontsize', 18));
            draw_axes (gv,struct('length',L/10 ));
            pause(0.005);
        end
        %     saveas(gcf, ['twist_t4-' num2str(i) '.png'], 'png');
        pause(2); % next eigenvector
    end
end
