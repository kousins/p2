function twist_t10
    nR = [1, 2, 3];
    nt = [10, 14, 18];
    fs =zeros(length(nR),length(nt));
    for i=1:length(nR)
        for j=1:length(nt)
            fs(i,j)= do_twist_t10(nR(i),nt(j))
        end
        save fs
    end
end

function frequency1 =do_twist_t10(nR,nt)
    % graphite polymer composite
    E1=155000;
    E2=12100;
    G12=4400;
    nu12=0.248;
    nu23=0.458;
    rho=5e-9;
    R= 2.5;
    t= 100.0;
    twist_angle= 15/360*2*pi;
    graphics = ~false;

    function Rm = default (XYZ, ts)
        Rm= eye(3);
    end
    function Rm = twist (XYZ, ts)
        r= norm(XYZ( 2:3));
        if r>0
            y=XYZ(2);z=XYZ(3);
            e2 = [0, y/r, z/r]';
            e3 = skewmat([1, 0, 0])*e2;
            Rm= [[1, 0, 0]',e2,e3];
            Rm=rotmat(r/R*twist_angle*skewmat(e2))*Rm;
        else
            Rm= eye(3);
        end
    end
    % Mesh
    [fens,gcells] = t4cylinderdel(t,R, nt,nR);
    [fens,gcells] = T4_to_T10(fens,gcells);
    % Material
    prop = property_linel_transv_iso (...
        struct('E1',E1,'E2',E2,'G12',G12,'nu12',nu12,'nu23',nu23,'rho',rho));
    mater = mater_defor_ss_linel_triax (struct('property',prop));
    % Finite element block
    feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
        'integration_rule',tet_rule (4),'Rm',@twist));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
    % Define the displacement field
    u   = 0*geom; % zero out
    % Apply EBC's
    nl=[fenode_select(fens, struct('box', [0,0,-R,R,-R,R],...
        'inflate',0.001)),...
        fenode_select(fens, struct('box', [t,t,-R,R,-R,R],...
        'inflate',0.001))];
    u   = set_ebc(u,nl,nl*0+1, [], nl*0);
    u   = apply_ebc (u);
    % Number equations
    u   = numbereqns (u);
    % Assemble the system matrix
    K = start (sparse_sysmat, get(u, 'neqns'));
    K = assemble (K, stiffness(feb, geom, u));
    M = start (sparse_sysmat, get(u, 'neqns'));
    M = assemble (M, mass(feb, geom, u));
    get(u, 'neqns')
    %
    neigvs = 20;
    options.issym= true;
    options.tol=1e-80;
    options.maxit= 500;
    options.disp= 0;
    [W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM', options);
    frequency1=sqrt(Omega(1, 1))/2/pi;

    % Plot
    if graphics
        bg=mesh_bdry(gcells);
        bfeb = feblock(struct ('gcells',bg));
        gv=graphic_viewer;
        gv=reset (gv,[]);
        scale=0.025;
        w=clone(u,'w'); % make a copy of u
        for i=1:1
            disp(['  Eigenvector ' num2str(i) ' frequency ' num2str(sqrt(Omega(i,i))/2/pi) ]);
            clf;
            w = scatter_sysvec(w, W(:,i));
            wv = get(w,'values');
            wmag = sqrt(wv(:,1).^2+wv(:,2).^2+wv(:,3).^2);
            dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
            colors=map_data(dcm, wmag);
            colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',colors));
            gv=reset (gv,[]);
            set(gca,'FontSize',16)
            camset(gv,[885.2912  -77.5338  228.2605   46.4468    3.2376    2.4532   -0.2576    0.0248    0.9659    2.6098]);
            %         draw(feb,gv, struct ('x', geom,'u',0*w, 'facecolor','none'));
            draw(bfeb,gv, struct ('x', geom,'u',+scale*w,'colorfield',colorfield));
            draw_axes(gv)
            %         text(3.1*R,4.1*R,4.1*R,['\omega_' num2str(i) '=' num2str(sqrt (Omega(i,i))) ],'FontSize',24);
            axis off
            %     saveas(gcf, ['twist_t4-' num2str(i) '.png'], 'png');
            pause(2); % next eigenvector
        end
    end
end
