% 1-D STRESS ANALYSIS EXAMPLES
%
% Files
%   <a href="matlab: edit 'Planar_truss_with_anim'">Planar_truss_with_anim</a> - Example 18: planar truss, example 3.7, p.72 from Hutton
%   <a href="matlab: edit 'PortalTrussModal'">PortalTrussModal</a>       - Example 22: free-vibration analysis of portal spatial truss
%   <a href="matlab: edit 'wvibalt'">wvibalt</a>                - Vibration of prestressed cable
