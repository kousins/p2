disp('Taut wire: characteristic frequencies');
L=7;
P=0.9;
mu= 80;
neigvs=5;
ns=[16, 32, 64, 128, 256, 512, 1024]; % number of elements
ns=[32]; % number of elements
eigenvalue_errors = zeros(length(ns),neigvs);
% Mesh
for mesh=1:length(ns)
    n=ns(mesh);
    [fens,gcells]= block1d(L,n, 1.0);
    % Finite element block
    prop = property_linel_iso (struct ('E',P,'rho',mu));
    mater = mater_defor_ss_linel_uniax (struct('property',prop));
    feb = feblock_defor_ss(struct ('mater',mater,...
        'gcells',gcells,...
        'integration_rule',gauss_rule(1, 4)));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    % Define the displacement field
    w   = 0*clone(geom,'w');
    % Apply EBC's
    fenids=[1,n+1]; prescribed=[1, 1]; component=[1, 1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);
    % Number equations
    w   = numbereqns (w);
    % Assemble the stiffness matrix
    K = start (sparse_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    % Assemble the consistent mass matrix
    M = start (sparse_sysmat, get(w, 'neqns'));
    M = assemble (M, mass(feb, geom, w));
    % Assemble the lumped mass matrix
    %     M = start (sparse_sysmat, get(w, 'neqns'));
    %     M = assemble(M, elemat( struct('eqnums', (1:n-1),'mat', eye(n-1) * mu*L/n)));
    % Solve
    [W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');
    [Omegas,ix]=sort(diag(Omega));

    scale=10;
    for i=1:neigvs
        analyt_eigenvalue =(P/mu*(i*pi/L) ^ 2);
        eigenvalue_errors(mesh,i) =...
            (analyt_eigenvalue-Omegas(i))/analyt_eigenvalue;
        disp(['  Eigenvector ' num2str(i) ...
            ' eigenvalue ' num2str(Omegas(i)) ' versus '...
            num2str(analyt_eigenvalue)]);
        figure;
        w = scatter_sysvec(w, W(:,ix(i)));
        plot (gather (geom, (1:n+ 1),'values'), ...
            gather (w, (1:n+ 1),'values'),'ro-','linewidth', 3);
        hold on
        figure (gcf)
        pause(2); % next eigenvector

    end
end
% plot (eigenvalue_errors)

