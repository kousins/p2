% Example 22: free-vibration analysis of portal spatial truss
echo off;
disp('Example 22: free-vibration analysis of portal spatial truss');

% Parameters:
E=1e7;
integration_order=1;

% Mesh
[fens,gcells]=portal;

% Construct fixities
ebc_fenids=[];
for i=1:length(fens)
    xyz=get(fens(i),'xyz');
    if (xyz(3) == 0)
        ebc_fenids=[ebc_fenids ones(1,3)*get(fens(i),'id')];
    end
end
ebc_prescribed=ebc_fenids*0+1;
ebc_comp=[];
ebc_val=ebc_fenids*0;

% Material
prop = property_linel_iso (struct('E',E));
mater = mater_defor_ss_linel_uniax (struct('property',prop));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells, ...
    'integration_rule',gauss_rule(1, integration_order),...
    'Rm',@geniso_Rm));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 3, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u')*0;
% Apply EBC's
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = dense_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
% Load
F = sysvec;
F = start (F, get(u, 'neqns'));
for i=31:32
    n=nodal_load(struct('id',i,'dir',1,'magn',+40));
    evs = loads(n, u);
    F = assemble (F, evs);
end
F = assemble (F, evs);
F = finish(F);
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
scale=2;
draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
draw(feb,gv, struct ('x', geom, 'u', (scale)*u,'facecolor','red'));
xlabel('X');
ylabel('Y');
zlabel('Z');
hold on
%

%
disp('Now the free vibration: ');
pause(2); % Solve the eigenvibration problem
M = sparse_sysmat;
M = start (M, get(u, 'neqns'));
ems = mass(feb, geom, u);
M = assemble (M, ems);
M = finish (M);

neigvs = 4;
[W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');
%

w=clone(u,'w'); % make a copy of u
for i=neigvs:-1:1
    clf;
    gv=reset (gv,[]);
    draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
    disp(['  Eigenvector ' num2str(i) ' eigenvalue ' num2str(Omega(i,i)) ]);
    w = scatter_sysvec(w, W(:,i));
    v = get(w,'values');
    limits=[inf -inf inf -inf inf -inf];
    for i=1:length(fens)
        xyz=get(fens(i),'xyz');
        limits(1:2:5)=min(xyz,limits(1:2:5));
        limits(2:2:6)=max(xyz,limits(2:2:6));
    end
    maxampl=max(sqrt(v(:,1).^2+v(:,2).^2+v(:,3).^2));
    limits(1:2:5)=limits(1:2:5)-0.5*maxampl;
    limits(2:2:6)=limits(2:2:6)+0.5*maxampl;
    cam=[2.7500    2.7500   71.7621    2.7500    2.7500    2.7500         0    1.0000         0    6.0276];
    reset (gv,struct ('limits', limits));
    headlight(gv);
    for scale=sin((0:1:42)/21*2*pi)
        reset (gv,struct ('limits', limits));
        camset(gv, cam);
        draw(feb,gv, struct ('x', geom, 'u', scale*w,'facecolor','red'));
        headlight(gv);
        pause(0.005);
    end
    disp('    Next eigenvalue: just a second');
    pause(2); % next eigenvector
end

