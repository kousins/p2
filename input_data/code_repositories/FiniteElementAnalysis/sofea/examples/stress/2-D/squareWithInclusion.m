function squareWithInclusion
    disp('square with inclusion');

    % Parameters:
    E=1e7;
    nu=0.3;
    integration_order=2;
    P=1;
    L=20;
    c=20;
    R= 3;
    Up= 3.2;

    function Ev= circle_E(xyz)
        if norm(xyz-[L/2,c/2]) <R
            Ev= 10*E;
        else
            Ev =E;
        end
    end

    % Mesh
    n= 25;
    [fens,gcells] = block2d(L,c,n,n, 1.0);
    % Material
    prop = property_linel_iso_inhomog (struct('E',@circle_E,'nu',nu));
    mater = mater_defor_ss_linel_biax (struct('property',prop, ...
        'reduction','strain'));
    % Finite element block
    feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells, 'integration_rule',gauss_rule (2, integration_order)));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
    % Define the displacement field
    u   = clone(geom,'u');
    u   = u*0; % zero out
    % Apply EBC's
    ebc_fenids=fenode_select (fens,struct('box',[0 0 0 0],'inflate', 0.0001));
    u   = set_ebc(u, ebc_fenids, 1, 1, 0);
    u   = set_ebc(u, ebc_fenids, 1, 2, 0);
    ebc_fenids=fenode_select (fens,struct('box',[0 0 0 100*c],'inflate', 0.0001));
    ebc_prescribed=ones(1,length (ebc_fenids));
    ebc_comp=ebc_fenids*0+1;
    ebc_val=ebc_fenids*0;
    u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
    ebc_fenids=fenode_select (fens,struct('box',[L L 0 100*c],'inflate', 0.0001));
    ebc_prescribed=ones(1,length (ebc_fenids));
    ebc_comp=ebc_fenids*0+1;
    ebc_val=ebc_fenids*0+Up;
    u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
    u   = apply_ebc (u);
    % Number equations
    u   = numbereqns (u);
    % Assemble the system matrix
    ems = stiffness(feb, geom, u);
    K = sparse_sysmat;
    K = start (K, get(u, 'neqns'));
    K = assemble (K, ems);
    K = finish (K);
    Kmat=get(K,'mat');
    % Load
    F = start (sysvec, get(u, 'neqns'));
    evs = nz_ebc_loads(feb, geom, u);
    F = assemble (F, evs);
    F = finish(F);
    Fmat=get(F,'vec');
    % Solve
    umat=Kmat\Fmat;
    u = scatter_sysvec(u, umat);

    % Plot
    scale=1;
    gv=graphic_viewer;
    gv=reset (gv,struct ('limits', [-0.1*L 1.1*L+Up -0.1*c 1.1*c]));
    draw(feb,gv, struct ('x', geom, 'u',u, 'facecolor','none'));
    fld = field_from_integration_points(feb, geom, scale*u, [], 'Cauchy', 2);
    nvals=get(fld,'values');
    dcm=data_colormap(struct ('range',[min(nvals),max(nvals)], 'colormap',jet));%
    colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
    draw(feb, gv, struct ('x',geom,'u', scale*u, 'colorfield',colorfield, 'shrink',1));
    view (2)
    %  max_sigma=inspect_integration_points(feb, geom, u, [], (1:length (gcells)), struct ('output',['Cauchy']),...
    %         inline('max(out(1), idat)','idat','out','xyz','pc'),...
    %         -Inf)
    %     min_sigma=inspect_integration_points(feb, geom, u, [], (1:length (gcells)), struct ('output',['Cauchy']),...
    %         inline('min(out(1), idat)','idat','out','xyz','pc'),...
    %         Inf)
    %     dcm=data_colormap(struct ('range',[min_sigma, max_sigma], 'colormap',jet));
    %     for i=1:length(gcells)
    %         sigma=inspect_integration_points(feb, geom, u, [], i, struct ('output',['Cauchy']),...
    %             inline('sum(out(1), idat)','idat','out','xyz','pc'),...
    %             0)/4;
    %         draw(gcells (i),gv, struct ('x', geom, 'u',scale*u, 'facecolor',map_data (dcm, sigma)));
    %     end
    %     draw(feb,gv, struct ('x', geom, 'u',0*u, 'facecolor','none'));
    %     colormap(jet);
    %     cbh=colorbar;
    %     set(cbh,...
    %         'Position',[0.8 0.15 0.1 0.7],...
    %         'YLim',[0,1],...
    %         'YTick',[0,1],...
    %         'YTickLabel',{[num2str(min_sigma)],[num2str(max_sigma)]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
    %     set(get(cbh,'XLabel'),'String','Tensile stress');
    %     view (2)

end