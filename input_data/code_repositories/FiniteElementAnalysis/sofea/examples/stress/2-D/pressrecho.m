disp('Pressure echo from a cavity');
% clear all classes
graphics = false;
% Parameters:
E=1e4;
nu=0.19;
vmag= 30;
dt =  0.5e-007;
tfinal = 2e-5;
rho = 1e-9;
scale = 100000;
tic
% Mesh'
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    'curve 1 line 0 -25 15 -25',...
    'curve 2 line 15 -25 15 25',...
    'curve 3 line 15 25 0 25',...
    'curve 4 line 0 25 0 5',...
    'curve 5 arc 0 5 5 0 center 0 0',...
    'curve 6 arc 5 0 0 -5 center 0 0 rev',...
    'curve 7 line 0 -5 0 -25',...
    ['subregion 1  property 1 boundary '...
    ' 1 2 3 4 5 6 7'],...
    ['m-ctl-point constant 0.45']
    }, 1.0,struct('axisymm', true));
% drawmesh({fens,gcells}); view(2)
% Material
prop = property_linel_iso(struct('E',E,'nu',nu,'rho',rho));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','axisymm'));
% Finite element block
feb = feblock_defor_ss(struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tri_rule (1)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% The axis of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 -50 50],'inflate',1/100));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Initial conditions
v = u;% copy everything from displacement field to velocity
fenids=fenode_select (fens,struct('box',[0 15 -25 -25],'inflate',1/100));
for j=1:length(fenids)
    xy= get (fens(fenids(j)),'xyz');
    v = scatter(v,fenids(j),[0, 1]*vmag);
end
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
M = start (sparse_sysmat, get(u, 'neqns'));
ems=mass(feb, geom, u);
for j=1:length(ems)
    Me=get(ems(j),'mat');
    ems(j)=set(ems(j),'mat',diag(sum(Me)));
end
M = assemble (M, ems);

% Solve
Kmat = get(K,'mat');
Mmat = get(M,'mat');
U0 = gather_sysvec(u);
V0= gather_sysvec(v);
A0 =Mmat\(-Kmat*U0);
t=0;
o2=eigs(Kmat,Mmat,1,'LM');
dt= 0.899* 2/sqrt(o2)
if graphics
    gv=graphic_viewer;
    gv=reset (gv,struct ('limits',[0 17 -26 26]));
end
ngraphics=  max (round(10*(1e-7)/dt),1);
igraphics =ngraphics-1;
snapshot =0;
while t <tfinal
    t=t+dt;
    U1 = U0 +dt*V0+(dt^2)/2*A0;
    A1 = Mmat\(-Kmat*U1);
    V1 = V0 +dt/2* (A0+A1);
    U0 = U1;
    V0 = V1;
    A0 = A1;
    igraphics=igraphics+1;
    if graphics & igraphics==ngraphics
        clear(gv); gv=reset (gv,struct ('limits',[0 17 -26 26]));
        u = scatter_sysvec(u, U1);
        fld = field_from_integration_points(feb, geom, u, [], 'pressure',1);
        nvals=get(fld,'values');min(nvals),max(nvals)
        nvalsrange=[-0.015, 0.015]%[min(nvals),max(nvals)]
        dcm=data_colormap(struct ('range',nvalsrange, 'colormap',jet));
        colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
        draw(feb,gv, struct ('x', geom, 'u', scale*u,...
            'colorfield',colorfield,'edgecolor','none'));
        view(2); pause(0.1);
        saveas(gcf, [mfilename '-33-' num2str(snapshot) '.png'], 'png');
        snapshot = snapshot +1;
        igraphics= 0;
    end
end
toc