disp('Beam of quad elements: thermal strains load, small displacement, small strain analysis');

% Parameters:
E=1e7;
nu=0.3;
DeltaT=100;
alpha = 10e-6;
width =1;
height =2;
% Mesh
[fens,gcells] = block2d(width, height,4,4, 0.1);
% mesh{1}=fens;
% mesh{2}=gcells;
% drawmesh(mesh); pause
% Material
prop = property_linel_iso (struct('E',E,'nu',nu,'alpha', alpha));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','strain'));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells, 'integration_rule',gauss_rule(2,2)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u'); 
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[0 width height height],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = dense_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
% Load
F = sysvec;
F = start (F, get(u, 'neqns'));
dT = field(struct ('name',['dT'], 'dim', 1, 'data',zeros(get(u, 'neqns'),1)+ DeltaT));
evs = thermal_strain_loads (feb, geom,u, dT);
F = assemble (F, evs);
F = finish(F);
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
scale=200;
draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','none'));
draw(feb,gv, struct ('x', geom, 'u', +scale*u,'facecolor','red'));
xlabel('X');
ylabel('Y');
zlabel('Z');
hold on

for i=1:length(fens)
  draw(fens(i),gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
end
draw_axes(gv, struct([]));
view (2)