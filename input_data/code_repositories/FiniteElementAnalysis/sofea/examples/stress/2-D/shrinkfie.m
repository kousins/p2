%clear all
Ts=50;
Tt=-10;
num_integ_pts=1; % 1-point quadrature
% Plot
gv=graphic_viewer;
gv=reset (gv,[0 160 0 160]);
scale=200;

alpha_steel = 17.3e-6;
nu_steel = 0.29;
E_steel = 211e9;
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    'curve 1 line 0 0 50 0',...
    'curve 2 arc 50 0 80 0 center 65 -0.001 ',...
    'curve 3 line 80 0 110 0',...
    'curve 4 line 110 0 110 50',...
    'curve 5 line 110 50 65 50 ',...
    'curve 6 arc 65 50 65 70 center 65.001 60  ',...
    'curve 7 line 65 70 110 70',...
    'curve 8 line 110 70 110 85',...
    'curve 9 arc 110 85 65 120 center 110 120 ',...
    'curve 10 line 65 120 0 120',...
    'curve 11 line 0 120 0 85',...
    'curve 12 arc 0 85 0 35 center -0.001 60 rev',...
    'curve 13 line 0 35 0 0',...
    'curve 14 line 110, 50, 160, 50',...
    'curve 15 line 160, 50, 160, 70',...
    'curve 16 line 160, 70, 110, 70',...
    ['subregion 1  property 1 boundary '...
    ' 1 2 3 4 5 6 7 8 9 10 11 12 13'],...
    ['m-ctl-point constant 3']
    }, 1.0);
prop_steel=property_linel_iso (struct('E',E_steel,'nu',nu_steel,'alpha', alpha_steel));
mater_steel = mater_defor_ss_linel_biax (struct('property',prop_steel, ...
    'reduction','strain'));
feb_steel = feblock_defor_ss (struct ('mater',mater_steel,...
    'gcells',gcells,...
    'integration_rule',tri_rule(num_integ_pts)));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
tempn=field(struct('name',['tempn'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Ts);

u   = clone(geom,'u'); 
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[65 65 50 50],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[65 65 70 70],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[1];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);

% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb_steel, geom, u));
% Load
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, thermal_strain_loads (feb_steel, geom,u, tempn));
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

draw(feb_steel,gv, struct ('x', geom, 'u', +scale*u,'facecolor','blue'));
draw(feb_steel,gv, struct ('x', geom, 'u', 0*u, 'facecolor','none'));
xlabel('X');
ylabel('Y');
zlabel('Z');
hold on


alpha_tungsten = 4.5e-6;
nu_tungsten = 0.28;
E_tungsten = 411e9;
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    'curve 1 line 0 0 50 0',...
    'curve 2 arc 50 0 80 0 center 65 -0.001 ',...
    'curve 3 line 80 0 110 0',...
    'curve 4 line 110 0 110 50',...
    'curve 5 line 110 50 65 50 ',...
    'curve 6 arc 65 50 65 70 center 65.001 60  ',...
    'curve 7 line 65 70 110 70',...
    'curve 8 line 110 70 110 85',...
    'curve 9 arc 110 85 65 120 center 110 120 ',...
    'curve 10 line 65 120 0 120',...
    'curve 11 line 0 120 0 85',...
    'curve 12 arc 0 85 0 35 center -0.001 60 rev',...
    'curve 13 line 0 35 0 0',...
    'curve 14 line 110, 50, 160, 50',...
    'curve 15 line 160, 50, 160, 70',...
    'curve 16 line 160, 70, 110, 70',...
    ['subregion 2  property 2 boundary '...
    ' -5 -6 -7 14 15 16'],...
    ['m-ctl-point constant 3']
    }, 1.0);
prop_tungsten=property_linel_iso (struct('E',E_tungsten,'nu',nu_tungsten,'alpha', alpha_tungsten));
mater_tungsten = mater_defor_ss_linel_biax (struct('property',prop_tungsten, ...
    'reduction','strain'));
feb_tungsten = feblock_defor_ss (struct ('mater',mater_tungsten,...
    'gcells',gcells,...
    'integration_rule',tri_rule(num_integ_pts)));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
tempn=field(struct('name',['tempn'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Tt);

u   = clone(geom,'u'); 
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[65 65 50 50],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[65 65 70 70],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[1];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);

% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb_tungsten, geom, u));
% Load
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, thermal_strain_loads (feb_tungsten, geom,u, tempn));
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

draw(feb_tungsten,gv, struct ('x', geom, 'u', +scale*u,'facecolor','none'));
draw(feb_tungsten,gv, struct ('x', geom, 'u', 0*u, 'facecolor','red'));
xlabel('X');
ylabel('Y');
zlabel('Z');
hold on

draw_axes(gv, struct([]));
view (2)
view3d zoom