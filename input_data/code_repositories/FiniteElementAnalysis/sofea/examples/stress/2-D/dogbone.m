function [h,maxtensile]=dogbone
    [h,maxtensile]=do_it(@t3block2d,@feblock_defor_ss,3);
    %     loglog(h,abs(convutip-utip)/convutip,'rd--','lineWIDTH', 3); hold on; figure (gcf); pause (1)
    % [h,utip]=do_it(smult,nu, @block2d,@feblock_defor_ss_biax_bbar,2);
    % loglog(h,abs(convutip-utip)/convutip,'gx:','lineWIDTH', 3); hold on; figure (gcf); pause (1)

    % [h,utip]=do_it(smult,nu, @block2d,@feblock_defor_ss_biax_sri,[1,2])
    % loglog(h,abs(convutip-utip)/convutip,'bo-.','lineWIDTH', 3); hold on; figure (gcf); pause (1)
end

function [h,maxtensile]=do_it(mf,blf,io)
    disp(['Dogbone specimen: ' func2str(blf)]);
    % Parameters:
    E=1e6; nu = 1/3;
    integration_order=io;
    graphics=~true;
    Width = 10;
    Length = 40;
    Height = 20;
    aspect=Length/Height;

    function x= tweak(x,d)
        x=x-(1-(x(1)/(Length/2))^2)*(x(1)<=Length/2)*[0,x(2)/4];
    end
    % Mesh
    h=[];
    maxtensile=[];
    magn=1;
    ns=[2,4,8,16];
    f=progressbar('Working');
    work0= sum (ns.^2); work = work0;
    for n=ns
        %     for n=[25]
        [fens,gcells] = mf(Length,Height, 2*n, 2/aspect*n, Width);
        [fens,gcells] = T3_to_T6(fens,gcells,[]);
        fens = transform_apply(fens,@tweak, []);
        %         drawmesh({fens,gcells},'gcells','shrink', 0.8)
        % view(2)

        %     mesh{1}=fens;
        %     mesh{2}=gcells;
        %     drawmesh(mesh); view(2); pause (1)
        %     Material
        prop = property_linel_iso (struct('E',E,'nu',nu));
        mater = mater_defor_ss_linel_biax (struct('property',prop, ...
            'reduction','strain'));
        % Finite element block
        feb = blf(struct ('mater',mater, 'gcells',gcells, 'integration_rule',tri_rule(io),'stabfact',0));
        % Geometry
        geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
        % Define the displacement field
        u   = clone(geom,'u');
        u   = u*0; % zero outfunction five
        % Apply EBC's
        ebc_fenids=fenode_select (fens,struct('box',[0,0,0,Height],'inflate',44/10000));
        ebc_prescribed=ones(1,length (ebc_fenids));
        ebc_comp=ebc_fenids*0+1;
        ebc_val=ebc_fenids*0;
        u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
        ebc_fenids=fenode_select (fens,struct('box',[0,Length,0,0],'inflate',44/10000));
        ebc_prescribed=ones(1,length (ebc_fenids));
        ebc_comp=ebc_fenids*0+2;
        ebc_val=ebc_fenids*0;
        u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
        ebc_fenids=fenode_select (fens,struct('box',[Length,Length,0,Height],'inflate',44/10000));
        ebc_prescribed=ones(1,length (ebc_fenids));
        ebc_comp=ebc_fenids*0+1;
        ebc_val=ebc_fenids*0+5;
        u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
        u   = apply_ebc (u);
        % Number equations
        u   = numbereqns (u);
        % Assemble the system matrix
        K = start (sparse_sysmat, get(u, 'neqns'));
        K = assemble (K, stiffness(feb, geom, u));
        % Load
        F = start (sysvec, get(u, 'neqns'));
        F = assemble (F, nz_ebc_loads(feb, geom, u));
        % Solve
        a = get(K,'mat');
        b = get(F,'vec');
        x = a\b;
        u = scatter_sysvec(u, x);

        % postprocessing
        fld = field_from_integration_points(feb, geom, u, [], 'Cauchy', 1);
        nvals=get(fld,'values');%min(nvals),max(nvals)
        % nvalsrange=[-0.0001 0.0001];%[min(nvals),max(nvals)]
        nvalsrange=[min(nvals),max(nvals)];
        maxtensile = [maxtensile,max(nvals)];
        h=[h,Length/n];
        x0nl=fenode_select (fens,struct('box',[0,0,0,Height],'inflate',44/10000));
        x0nls =gather(fld,x0nl,'values');
        x0nlx=gather(geom,x0nl,'values','noreshape');
        [B,IX] = sort(x0nlx(:,2));
        plot(x0nlx(IX,2),x0nls(IX),'bx-'); view(2); hold on
        if graphics
            gv=graphic_viewer;
            gv=reset (gv,struct ([]));
            scale=1;
            cmap = jet;
            dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
            colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
            draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
            % geom3d=combine (geom, 0*fld);
            % u3d=combine (u, 100000000*fld);
            % draw(feb,gv, struct ('x', geom3d, 'u', +scale*u3d,'colorfield',colorfield, 'shrink',1.0));
            colormap(cmap);
            cbh=colorbar;
            set(cbh,...
                'Position',[0.8 0.15 0.05 0.7],...
                'YLim',[0,1],...
                'YTick',[0,1],...
                'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
            set(get(cbh,'XLabel'),'String','\sigma_x');
            view (2)
        end
        work= work-n^2; progressbar((work0-work)/work0,f)
    end
    delete(f)
end
