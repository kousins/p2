disp('Balloon inflation');
febf=@feblock_defor_ss;
integration_order=2;
graphics = ~false;
ngraphics= 10;
% Parameters:
E1=1e4;
E2=1e4;
E3=5e4;
nu12=0.29;
nu13=0.29;
nu23=0.19;
G12=0.3e4;
G13=0.3e4;
G23=0.3e4;
rin=100;
rex =152;
vmag= 1;
dt = 0.00000005;
tfinal = 2000*dt;
rho = 1e-9;
scale = 4000000;

% Mesh'
[fens,gcells]=block2d(rex-rin,pi/2, 4,12, 0);
for i = 1:length(gcells)
    gcells(i) = gcell_Q4(struct('conn', get(gcells(i),'conn'),...
        'axisymm', true));
end
% Needs to select the cells before tweaking the node coordinates
bdry_gcells = mesh_bdry(gcells, struct('axisymm', true,'other_dimension', 0.0001));
icl = gcell_select(fens, bdry_gcells, struct('box', [0,0,0,pi/2],'inflate',rin/100));
for i=1:length (fens)
    xy=get (fens(i),'xyz');
    r=rin+xy(1); a=xy(2);
    xy=[r*cos(a) r*sin(a)];
    fens(i)=set(fens(i),'xyz', xy);
end
% Material
prop = property_linel_ortho(struct('E1',E1,'E2',E2,'E3',E3,...
    'nu12',nu12,'nu13',nu13,'nu23',nu23,...
    'G12',G12,'G13',G13,'G23',G23,'rho',rho));
%     prop = property_linel_iso (struct('E',E1,'nu',nu12));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','axisymm'));
% Finite element block
feb = febf (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% First the plane of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 rex 0 0],'inflate',rex/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% The axis of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 rex],'inflate',rex/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Initial conditions
v = u;% copy everything from displacement field to velocity
bg=bdry_gcells(icl);
for i=1:length(bg)
    conn= get(bg(i), 'conn');
    for j=1:length(conn)
        xy= get (fens(conn(j)),'xyz');
        v = scatter(v,conn(j),xy/norm(xy)*vmag);
    end
end
%     for j=1:length(fens)
%         xy= get (fens(j),'xyz');
%         v = scatter(v,j,xy.*[1, 0]*vmag);
%     end
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
M = start (sparse_sysmat, get(u, 'neqns'));
M = assemble (M, mass(feb, geom, u));
 drawmesh({fens,gcells})
% Solve
Kmat = get(K,'mat');
Mmat = get(M,'mat');
U0 = gather_sysvec(u);
V0= gather_sysvec(v);
A0 =Mmat\(-Kmat*U0);
t=0;
if graphics
    gv=graphic_viewer;
    gv=reset (gv,struct ('limits',[0 1.06*rex 0 1.6*rex]));
end
igraphics =0;
while t <tfinal
    t=t+dt;
    U1 = U0 +dt*V0+(dt^2)/2*A0;
    A1 = Mmat\(-Kmat*U1);
    V1 = V0 +dt/2* (A0+A1);
    U0 = U1; U1(1:3)
    V0 = V1;
    A0 = A1;
    igraphics=igraphics+1;
%     gv=reset (gv,struct ('limits',[0 1.06*rex 0 1.6*rex]));
    if graphics & igraphics==ngraphics
         u = scatter_sysvec(u, U1);
        gv=reset (gv,struct ('limits',[0 1.06*rex 0 1.6*rex]));view(2);
       draw(feb,gv, struct ('x', geom, 'u', scale*u,'facecolor','red'));
        draw(feb,gv, struct ('x', geom, 'u', 0*u,'facecolor','none'));
         pause(0.1);
        igraphics= 0;
    end
end
