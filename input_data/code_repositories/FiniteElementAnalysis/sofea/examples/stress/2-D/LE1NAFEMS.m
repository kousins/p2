disp('LE1 NAFEMS benchmark');

% Parameters:
E = 210e3;% 210e3 MPa
nu = 0.3;
p = 10;% 10 MPA Outward pressure on the outside ellipse
sigma_yD= 92.7;% tensile stress at [2.0, 0.0] meters
n=3;% number of elements through the thickness

integration_rule = gauss_rule(2, 2);
Edge_integration_rule =gauss_rule(1,2);
Convertf=[];
febf =@feblock_defor_ss;
Style ='ks-'; Label='Q4';

% integration_rule = gauss_rule(2,2);
% Edge_integration_rule =gauss_rule(1,3);
% Convertf=@Q4_to_Q8;
% febf =@feblock_defor_ss;
% Style ='ks-'; Label='Q8';

% Mesh'
[fens,gcells]=block2d(1.0,pi/2, n, n*2, struct('other_dimension', 0.1*1000));
if (~isempty( Convertf ))
  [fens,gcells] = Convertf(fens,gcells, struct('other_dimension', 0.1*1000));
end
bdry_gcells = mesh_bdry(gcells, struct('other_dimension', 0.1*1000));
icl = gcell_select(fens, bdry_gcells, struct('box', [1,1,0,pi/2],'inflate',1/100));
for i=1:length (fens)
    xy=get (fens(i),'xyz');
    t=xy(1); a=xy(2);
    xy= 1000*[(t*3.25+(1-t)*2)*cos(a) (t*2.75+(1-t)*1)*sin(a)];
    fens(i)=set(fens(i),'xyz', xy);
end
% drawmesh( {fens,gcells})
% view (2)
% return
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
  'reduction','stress'));
% Finite element block
feb = febf(struct ('mater',mater, 'gcells',gcells, 'integration_rule',integration_rule));
efeb = febf (struct ('mater',mater, 'gcells',bdry_gcells(icl),...
    'integration_rule',Edge_integration_rule));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% plane of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 Inf 0 0],'inflate',1/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% plane of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 Inf],'inflate',1/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% tic
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
% Load
fi=force_intensity(struct('magn',@(x) (p*[2.75/3.25*x(1),3.25/2.75*x(2)]'/norm([2.75/3.25*x(1),3.25/2.75*x(2)]))));
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, distrib_loads(efeb, geom, u, fi, 2));
% Solve
u = scatter_sysvec(u, K\F);
% get(u,'values')
% toc, tic
% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits',1000*[0 1.06*3.25 0 1.6*3.25]));
scale=1000;
cmap = jet;
fld = field_from_integration_points(feb, geom, u, [], 'Cauchy',2);
corner=fenode_select (fens,struct('box',1000*[2.0 2.0 0 0],'inflate',1/10000));
disp( ['Corner stress sigma_y=' num2str(gather (fld, corner,'values')) ' MPa'])
disp( [' i.e. ' num2str(gather(fld, corner,'values')/sigma_yD*100) '% of the Reference value'])
nvals=get(fld,'values');%min(nvals),max(nvals)
nvalsrange=[min(nvals),max(nvals)];
dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
draw(feb,gv, struct ('x', geom, 'u', 0*u,'facecolor','none', 'shrink',1.0));
% draw(efeb,gv, struct ('x', geom, 'u', 0*u,'facecolor','red', 'shrink',1.0));
colormap(cmap);
cbh=colorbar;
set(cbh,...
    'Position',[0.815 0.15 0.05 0.7],...
    'YLim',[0,1],...
    'YTick',[0,1],...
    'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
set(get(cbh,'XLabel'),'String','\sigma_y');
view (2)
