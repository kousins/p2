disp('Pipe with pinching load, small displacement, small strain analysis');

% Parameters:
E=1e7;
nu=0.3;
integration_order=2;
rin=1;
rex=2;
scale=50000;

% Mesh
[fens,gcells] = annulus(rin,rex,11,30, 1.0);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','strain'));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells, 'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
nl=fenode_select (fens,struct ('box',[0 0 0 100*rex],'inflate',rex/100));
u   = set_ebc(u, nl, nl*0+1, nl*0+1, nl*0);
u   = apply_ebc (u);
nl=fenode_select (fens,struct ('box',[0 100*rex 0 0],'inflate',rex/100));
u   = set_ebc(u, nl, nl*0+1, nl*0+2, nl*0);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the stiffness matrix
S = start (sparse_sysmat, get(u, 'neqns'));
S = assemble (S, stiffness(feb, geom, u));
S = finish (S);
% Load
nl=fenode_select (fens,struct ('box',[0 0 rex rex],'inflate',rex/100));
L = start (sysvec, get(u, 'neqns'));
L = assemble (L, loads(nodal_load(struct('id',nl,'dir',2,'magn',-4)), u));
L = finish(L);
% Solve
u = scatter_sysvec(u, get(S,'mat')\get(L,'vec'));

% Plot
gv=graphic_viewer;
% for i=1:length(fens)
%     draw(fens(i),gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
% end
gv=reset (clear (gv),struct('limits',[0 2.25 0 2.25]));
draw(feb,gv, struct ('x', geom, 'u',u, 'facecolor','none'));
fld = field_from_integration_points(feb, geom, scale*u, [], 'Cauchy', 2);
nvals=get(fld,'values');
dcm=data_colormap(struct ('range',[-1000000,1000000], 'colormap',jet));%[min(nvals),max(nvals)]
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
draw(feb, gv, struct ('x',geom,'u', scale*u, 'colorfield',colorfield, 'shrink',1));
view (2)
