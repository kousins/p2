disp('Squish cube: full integration');
febf=@feblock_defor_ss;
integration_order=2;

% disp('Squish cube: selective reduced integration');
% febf=@feblock_defor_ss_sri;
% integration_order=[1,2];

% Parameters:
E=1e7;
nu=0.499999;
% prescribed displacements
up=5;
vp=5;

% Mesh
L=100;
n=2;
[fens,gcells] = block2d(L,L, 8*n,8*n, 1.0);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','strain'));
% Finite element block
feb = febf (struct ('mater',mater, 'gcells',gcells, 'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% First the exterior boundary: completely fixed
ebc_fenids=fenode_select (fens,struct('box',[0 L 0 0],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
ebc_fenids=fenode_select (fens,struct('box',[0 L L L],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 L],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
ebc_fenids=fenode_select (fens,struct('box',[L L 0 L],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=[];
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Now the interior boundary: prescribed displacements
ebc_fenids=fenode_select (fens,struct('box',[3*L/8 5*L/8 3*L/8 5*L/8],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+1;
ebc_val=ebc_fenids*0+up;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+2;
ebc_val=ebc_fenids*0+vp;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = sparse_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
% Load
F = sysvec;
F = start (F, get(u, 'neqns'));
evs = nz_ebc_loads(feb, geom, u);
F = assemble (F, evs);
F = finish(F);
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits',[0 L 0 L]));
scale=1;
cmap = jet;
fld = field_from_integration_points(feb, geom, u, [], 'vol_strain', 1);
nvals=get(fld,'values');%min(nvals),max(nvals)
nvalsrange=[-0.0001 0.0001];%[min(nvals),max(nvals)]
dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
colormap(cmap);
cbh=colorbar;
set(cbh,...
    'Position',[0.85 0.15 0.05 0.7],...
    'YLim',[0,1],...
    'YTick',[0,1],...
    'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
set(get(cbh,'XLabel'),'String','Volumetric strain');
% view (2)
