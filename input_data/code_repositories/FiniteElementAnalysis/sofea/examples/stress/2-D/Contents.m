% 2-D STRESS ANALYSIS EXAMPLES
%
% Files
%   <a href="matlab: edit 'coneaxi'">coneaxi</a>             - Axial is symmetric cone
%   <a href="matlab: edit 'cookstress'">cookstress</a>          - Cook's membrane
%   <a href="matlab: edit 'dogbone'">dogbone</a>             - Dog-bone Tensile specimen, 2-D
%   <a href="matlab: edit 'dogbone3d'">dogbone3d</a>           - Dog-bone Tensile specimen, 3-D
%   <a href="matlab: edit 'LE1NAFEMS'">LE1NAFEMS</a>           - elliptical annulus membrane
%   <a href="matlab: edit 'Ltract'">Ltract</a>              - L-shaped membrane, in the xy-plane
%   <a href="matlab: edit 'Ltract3D'">Ltract3D</a>            - L-shaped membrane, in the 3-D space
%   <a href="matlab: edit 'Ltract_iter'">Ltract_iter</a>         - L-shaped membrane, Iteration solver
%   <a href="matlab: edit 'orthoballoon'">orthoballoon</a>        - Ortho tropic balloon
%   <a href="matlab: edit 'pipe_pinch'">pipe_pinch</a>          - Pinched pipe
%   <a href="matlab: edit 'pipe_pinch_stress'">pipe_pinch_stress</a>   - Pinched pipe with stress display
%   <a href="matlab: edit 'pressecho'">pressecho</a>           - Pressure echo, orthotropic material
%   <a href="matlab: edit 'pressrecho'">pressrecho</a>          - Pressure echo, isotropic material
%   <a href="matlab: edit 'pressrechoq'">pressrechoq</a>         - Pressure echo, isotropic material, with T6 triangles
%   <a href="matlab: edit 'propagate'">propagate</a>           - Hollow sphere With initial velocity
%   <a href="matlab: edit 'punchaxi'">punchaxi</a>            - Axially symmetric punch
%   <a href="matlab: edit 'q4eigmds'">q4eigmds</a>            - Mode shape of the Quadrilateral stiffness matrix
%   <a href="matlab: edit 'shaftwtrans0'">shaftwtrans0</a>        - Shaft with transition, axis(1) + clamped face(2)
%   <a href="matlab: edit 'shaftwtrans1'">shaftwtrans1</a>        - Shaft with transition, axis(1) + face(2)
%   <a href="matlab: edit 'shaftwtrans2'">shaftwtrans2</a>        - Shaft with transition, axis(1) + point(2)
%   <a href="matlab: edit 'shaftwtrans3'">shaftwtrans3</a>        - Shaft with transition, axis(1) + point(2) +
%                         self-equilibriated
%   <a href="matlab: edit 'shaftwtransition'">shaftwtransition</a>    - Shaft with transition
%   <a href="matlab: edit 'shrinkfie'">shrinkfie</a>           - Shrink-fit Expansion simulation
%   <a href="matlab: edit 'squareWithInclusion'">squareWithInclusion</a> - Square with circular inclusion
%   <a href="matlab: edit 'squishcube'">squishcube</a>          - Squishy cube
%   <a href="matlab: edit 'taper_q4'">taper_q4</a>            - Tapered Plate
