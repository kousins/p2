disp('Axially symmetric analysis of a cone');
febf=@feblock_defor_ss;
integration_order=2;

% disp('Squish cube: selective reduced integration');
% febf=@feblock_defor_ss_sri;
% integration_order=[1,2];

% Parameters:
E=1e7;
nu=0.29;
% prescribed displacements
vp=-15;

% Mesh'
L=100;
n=2;
[fens,gcells] = block2d(L/2,L, 4*n,8*n, struct('axisymm',true));
fens = transform_apply(fens,@(x, data) (x+[L/2-0.3*x(2), 0]), []);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','axisymm'));
% Finite element block
feb = febf (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% First the plane of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 100*L 0 0],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% The axis of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 L],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% Now the punch
ebc_fenids=fenode_select (fens,struct('box',[0 100*L L L],'inflate',L/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+2;
ebc_val=ebc_fenids*0+vp;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = sparse_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
% Load
F = sysvec;
F = start (F, get(u, 'neqns'));
evs = nz_ebc_loads(feb, geom, u);
F = assemble (F, evs);
F = finish(F);
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')

% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits',[0 1.6*L 0 1.1*L]));
scale=1;
cmap = jet;
fld = field_from_integration_points(feb, geom, u, [], 'Cauchy',2);
nvals=get(fld,'values');%min(nvals),max(nvals)
nvalsrange=[min(nvals),max(nvals)];
dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
colormap(cmap);
cbh=colorbar;
set(cbh,...
    'Position',[0.85 0.15 0.05 0.7],...
    'YLim',[0,1],...
    'YTick',[0,1],...
    'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
set(get(cbh,'XLabel'),'String','\sigma_y');
% view (2)

gv=graphic_viewer;
gv=reset (gv,struct ('limits',[0 1.6*L 0 1.1*L]));
draw(feb,gv, struct ('x', geom, 'u',u, 'facecolor','none'));
draw(feb,gv, struct ('x', geom, 'u',scale*u, 'facecolor','none'));
dcm=data_colormap(struct ('range',[-1000000,1000000], 'colormap',jet));
draw_integration_points(feb,gv,struct ('x',geom,'u',u,'u_displ',scale*u, 'scale',0.000004,'component',2,'data_cmap', dcm,'tessel', 9));
view (2)