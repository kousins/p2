function [h,utip]=cookstress
    smult=0;

    nu=1/3;
    convutip=23.97;


    [h,utip]=do_it(smult,nu, @t3block2d,@feblock_defor_ss,1);
%     loglog(h,abs(convutip-utip)/convutip,'rd--','lineWIDTH', 3); hold on; figure (gcf); pause (1)
    % [h,utip]=do_it(smult,nu, @block2d,@feblock_defor_ss_biax_bbar,2);
    % loglog(h,abs(convutip-utip)/convutip,'gx:','lineWIDTH', 3); hold on; figure (gcf); pause (1)

    % [h,utip]=do_it(smult,nu, @block2d,@feblock_defor_ss_biax_sri,[1,2])
    % loglog(h,abs(convutip-utip)/convutip,'bo-.','lineWIDTH', 3); hold on; figure (gcf); pause (1)

    % OPT element (Felippa)
    % loglog(44./[2, 4, 8, 16, 32, 64],(convutip-[20.56, 22.45, 23.43, 23.80, 23.91, 23.95])/convutip,'ko:','lineWIDTH', 1+i); hold on; figure (gcf); pause (1)
end

function [h,utip]=do_it(smult,nu, mf,blf,io)
    disp(['Cooks problem, plane stress: ' func2str(blf)]);
    % Parameters:
    E=1;
    integration_order=io;
    graphics=true;
    doeig=~true;
    aspect=1;

    % Mesh
    h=[];
    utip=[];
    magn=1;
%     for n=[2,4,8,16,32,64]
    for n=[2, 4,8]
        [fens,gcells] = mf(48,44, n, aspect*n, 1.0);
        dxy=min(48,44)/n/aspect/100;
        sxy=smult*dxy;
        rand('state',[0.3085,0.4953,0.0143,0.3137,0.7750,0.8827,0.6275,0.5996,0.3557,0.8033,0.4425,0.3749,0.3086,0.6245,0.0244,0.0309,0.1962,0.2670,0.8672,0.8259,0.3590,0.6446,0.3018,0.6694,0.5783,0.3251,0.0024,0.9082,0.4464,0.0331,0.9344,0.0261,0,0.0000,0.0000]');
        for i=1:length(fens)
            xy=get(fens(i),'xyz');
            if (xy(1)>dxy) & (xy(2)>dxy) & (xy(1)<48-dxy) & (xy(2)<44-dxy)
                xy=[xy(1)+(2*rand(1)-1)/2*sxy,xy(2)+(2*rand(1)-1)/2*sxy];
            end
            fens(i)=set(fens(i),'xyz',xy);
        end
        for i=1:length(fens)
            xy=get(fens(i),'xyz');
            y=xy(2) + (xy(1)/48)*(44 -xy(2)/44*28);
            fens(i)=set(fens(i),'xyz',[xy(1) y]);
        end
        %     mesh{1}=fens;
        %     mesh{2}=gcells;
        %     drawmesh(mesh); view(2); pause (1)
        %     Material
        prop = property_linel_iso (struct('E',E,'nu',nu));
        mater = mater_defor_ss_linel_biax (struct('property',prop, ...
            'reduction','stress'));
        % Finite element block
        feb = blf(struct ('mater',mater, 'gcells',gcells, 'integration_rule',tri_rule(io),'stabfact',0));
        % Geometry
        geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
        % Define the displacement field
        u   = clone(geom,'u');
        u   = u*0; % zero outfunction five
        % Apply EBC's
        ebc_fenids=fenode_select (fens,struct('box',[0,0,0, 44],'inflate',44/10000));
        ebc_prescribed=ones(1,length (ebc_fenids));
        ebc_comp=[];
        ebc_val=ebc_fenids*0;
        u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
        u   = apply_ebc (u);
        % Number equations
        u   = numbereqns (u);
        % Assemble the system matrix
        K = start (sparse_sysmat, get(u, 'neqns'));
        K = assemble (K, stiffness(feb, geom, u));
        K = finish (K);
        % Load
        F = sysvec;
        F = start (F, get(u, 'neqns'));
        tol=0.0001*44;
        nl=fenode_select (fens,struct ('box',[48,48,44,60],'inflate',tol));
        ni=length(nl)-1;
        for i=1:length(nl)
            F = assemble (F, loads(nodal_load(struct('id',nl(i),'dir',2,'magn',magn/ni/2)), u));
        end
        nl=fenode_select (fens,struct ('box',[48,48,44+10*tol,60-10*tol],'inflate',tol));
        for i=1:length(nl)
            F = assemble (F, loads(nodal_load(struct('id',nl(i),'dir',2,'magn',magn/ni/2)), u));
        end
        F = finish(F);
        % Solve
        a = get(K,'mat');
        b = get(F,'vec');
        x = a\b;
        u = scatter_sysvec(u, x);
        nl=fenode_select (fens,struct ('box',[48,48,52,52],'inflate',tol));
        theutip=gather(u,nl,'values');
        disp (['displacement =' num2str(theutip(2))])
        h=[h 44/n];
        utip =[utip theutip(2)];

        % Plot
        if graphics
            gv=graphic_viewer;
            gv=reset (gv,struct ([]));
            scale=1;
            cmap = jet;
            fld = field_from_integration_points(feb, geom, u, [], 'Cauchy', 1);
            nvals=get(fld,'values');%min(nvals),max(nvals)
            % nvalsrange=[-0.0001 0.0001];%[min(nvals),max(nvals)]
            nvalsrange=[min(nvals),max(nvals)];
            dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
            colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
            draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
            % geom3d=combine (geom, 0*fld);
            % u3d=combine (u, 100000000*fld);
            % draw(feb,gv, struct ('x', geom3d, 'u', +scale*u3d,'colorfield',colorfield, 'shrink',1.0));
            colormap(cmap);
            cbh=colorbar;
            set(cbh,...
                'Position',[0.8 0.15 0.05 0.7],...
                'YLim',[0,1],...
                'YTick',[0,1],...
                'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
            set(get(cbh,'XLabel'),'String','\sigma_x');
            view (2)
            %         for i=1:length(fens)
            %             draw(fens(i),gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
            %         end
            if doeig
                pause(2)
                %% Solve the eigenvibration problem
                neigvs = get(u,'neqns');
                [modes,Omega]=eig(full(a));
                [b,ix]=sort(diag(Omega));

                %% Display the first few eigenvibration modes
                mscale=1;
                w=clone(u,'w'); % make a copy of u
                for i=1:neigvs
                    disp(['  Eigenvector ' num2str(i) ' eigenvalue ' num2str(Omega(ix(i),ix(i))) ]);
                    w = scatter_sysvec(w, modes(:,ix(i)));
                    wv = get(w,'values');
                    wmag = sqrt(wv(:,1).^2+wv(:,2).^2);
                    dcm=data_colormap(struct ('range',[min(wmag),max(wmag)], 'colormap',jet));
                    colorfield = field(struct ('name',['cf'], 'dim', 3, 'data',map_data(dcm, wmag)));
                    for scale=mscale*sin((0:1:42)/21*2*pi)
                        gv=reset (clear (gv),struct ('limits', [-1/40*48 41/40*48 -1/40*60 5/4*60]));
                        %         headlight(gv);
                        draw(feb,gv, struct ('x', geom, 'u', scale*w,'colorfield',colorfield));
                        title(['  Eigenvector ' num2str(i) ' eigenvalue ' num2str(Omega(ix(i),ix(i))) ]);
                        pause(0.01);
                    end
                    title(['  Next eigenvector: type any key' ]);
                    clf;
                    pause(2); % next eigenvector
                end
            end
        end
    end
end