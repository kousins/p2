disp('tapered strip: q4 elements');

% Parameters:
E=1e7;
nu=0.3;
integration_order=2;
P=1;

% Mesh
L=20;
c=2;
nel=[];
mxsig=[];
se=[];
for n=1:4:16
[fens,gcells] = block2d(L,2*c,6*n,2*n, 1.0);
for i=1:length(fens)
    xy=get(fens(i),'xyz');
    xy(2)=xy(2) + (xy(1)>L/3)*(xy(1)<=2*L/3)*0.05*(xy(1)-L/3)*xy(2) + (xy(1)>2*L/3)*0.05*(L/3)*xy(2);
    fens(i)=set(fens(i),'xyz',xy);
end

% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','strain'));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells, 'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[0 L 0 0],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 100*c],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[L L 0 100*c],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_fenids*0+1;
ebc_val=ebc_fenids*0+c;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = sparse_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
Kmat=get(K,'mat');
% Load
F = start (sysvec, get(u, 'neqns'));
evs = nz_ebc_loads(feb, geom, u);
F = assemble (F, evs);
F = finish(F);
Fmat=get(F,'vec');
% Solve
umat=Kmat\Fmat;
u = scatter_sysvec(u, umat);

% Plot
scale=1;
% gv=graphic_viewer;
% gv=reset (gv,struct ('limits', [0 1.2*L 0 4*c]));
% draw(feb,gv, struct ('x', geom, 'u',u, 'facecolor','none'));
nel=[nel length(gcells)];
max_sigma=inspect_integration_points(feb, geom, u, [], (1:length (gcells)), struct ('output',['Cauchy']),...
    inline('max(out(1), idat)','idat','out','xyz','pc'),...
    -Inf);
min_sigma=inspect_integration_points(feb, geom, u, [], (1:length (gcells)), struct ('output',['Cauchy']),...
    inline('min(out(1), idat)','idat','out','xyz','pc'),...
    Inf);
mxsig=[mxsig max_sigma];
se=[se umat'*Fmat];
% dcm=data_colormap(struct ('range',[min_sigma, max_sigma], 'colormap',jet));
% for i=1:length(gcells)
%     sigma=inspect_integration_points(feb, geom, u, [], i, struct ('output',['Cauchy']),...
%         inline('min(out(1), idat)','idat','out','xyz'),...
%         Inf);
%     draw(gcells (i),gv, struct ('x', geom, 'u',scale*u, 'facecolor',map_data (dcm, sigma)));
% end
% colormap(jet);
% cbh=colorbar;
% set(cbh,...
%     'Position',[0.2 0.75 0.5 0.2],...
%     'YLim',[0,1],...
%     'YTick',[0,1],...
%     'YTickLabel',{[num2str(min_sigma)],[num2str(max_sigma)]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
% set(get(cbh,'XLabel'),'String','Tensile stress');
% view (2)
plot(nel,se,'o-');
figure(gcf);
pause(1)
end