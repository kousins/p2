
disp('A single quadrilateral: eigen values/eigen modes of the stiffness matrix');

% Parameters:
E=1e7;
nu=0.3;
integration_order=2;

% Mesh
fens=[fenode(struct ('id',1,'xyz',[-1 -1]));
    fenode(struct ('id',2,'xyz',[1 -1]));
    fenode(struct ('id',3,'xyz',[1 1]));
    fenode(struct ('id',4,'xyz',[-1 1]))];
gcells = [ gcell_Q4(struct ('id',1,'conn',[1 2 3 4], 'other_dimension',1.0)) ];
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','strain'));
% Finite element block
feb = feblock_defor_ss(struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (2,integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% ebc_fenids=[1];
% ebc_prescribed=[1];
% ebc_val=0*(1:2);
% ebc_comp=[1];
% for i=ebc_fenids
%     u   = set_ebc(u, i, ebc_prescribed, ebc_comp, ebc_val);
% end
% u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = dense_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
Km = get(K,'mat');
[V, D] = eig (Km);

gv=graphic_viewer;
for i=1:length(D)
    u = scatter_sysvec(u, V(:,i));
    disp(['  Eigenvector ' num2str(i) ' eigenvalue ' num2str(D(i,i)) ]);
    for scale=0.5*sin((0:1:42)/21*2*pi)
        gv =reset(gv,struct ('limits', [-1, 1.0, -1, 1.0]*1.3));
        draw(feb,gv, struct ('x', geom, 'u', 0*u, 'facecolor','blue'));
        draw(feb,gv, struct ('x', geom, 'u', scale*u,'facecolor','red'));
        pause(0.1);
    end
    disp('    Next eigenvalue');
    pause(2); % next eigenvector
end

