disp('Balloon inflation');
febf=@feblock_defor_ss;
integration_order=2;

% Parameters:
E1=1.0;
E2=1.0;
E3=3.0;
nu12=0.29;
nu13=0.29;
nu23=0.19;
G12=0.3;
G13=0.3;
G23=0.3;
p= 0.15;
rin=1;
rex =1.2;
n=2;

% Mesh'
[fens,gcells]=block2d(rex-rin,pi/2,5,20,struct('axisymm',true));
bdry_gcells = mesh_bdry(gcells, struct('axisymm', true,'other_dimension', 0.0001));
icl = gcell_select(fens, bdry_gcells, struct('box', [0,0,0,pi/2],'inflate',rin/100));
for i=1:length (fens)
    xy=get (fens(i),'xyz');
    r=rin+xy(1); a=xy(2);
    xy=[r*cos(a) r*sin(a)];
    fens(i)=set(fens(i),'xyz', xy);
end
% Material
prop = property_linel_ortho(struct('E1',E1,'E2',E2,'E3',E3,...
    'nu12',nu12,'nu13',nu13,'nu23',nu23,...
    'G12',G12,'G13',G13,'G23',G23));
%     prop = property_linel_iso (struct('E',E1,'nu',nu12));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','axisymm'));
% Finite element block
feb = febf (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (2, integration_order)));
efeb = febf (struct ('mater',mater, 'gcells',bdry_gcells(icl),...
    'integration_rule',gauss_rule (1, 2)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
% First the plane of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 rex 0 0],'inflate',rex/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% The axis of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 rex],'inflate',rex/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
tic
% Assemble the system matrix
K = start (sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
% Load
fi=force_intensity(struct('magn',@(x) (p*x'/norm(x))));
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, distrib_loads(efeb, geom, u, fi, 2));
% Solve
a = get(K,'mat');
b = get(F,'vec');
x = a\b;
u = scatter_sysvec(u, x);
% get(u,'values')
toc, tic
% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits',[0 1.06*rex 0 1.6*rex]));
scale=1;
cmap = jet;
fld = field_from_integration_points(feb, geom, u, [], 'Cauchy',3);
nvals=get(fld,'values');%min(nvals),max(nvals)
nvalsrange=[min(nvals),max(nvals)];
dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
draw(feb,gv, struct ('x', geom, 'u', +scale*u,'colorfield',colorfield, 'shrink',1.0));
draw(feb,gv, struct ('x', geom, 'u', 0*u,'facecolor','none', 'shrink',0.8));
draw(efeb,gv, struct ('x', geom, 'u', 0*u,'facecolor','red'));
colormap(cmap);
cbh=colorbar;
set(cbh,...
    'Position',[0.815 0.15 0.05 0.7],...
    'YLim',[0,1],...
    'YTick',[0,1],...
    'YTickLabel',{[num2str(nvalsrange(1))],[num2str(nvalsrange(2))]});%{[num2str((min(nvals)))],[num2str((max(nvals)))]}
set(get(cbh,'XLabel'),'String','\sigma_z');
view (2)
toc
% gv=graphic_viewer;
% gv=reset (gv,struct ('limits',[0 1.6*rex 0 1.1*rex]));
% draw(feb,gv, struct ('x', geom, 'u',u, 'facecolor','none'));
% draw(feb,gv, struct ('x', geom, 'u',scale*u, 'facecolor','none'));
% dcm=data_colormap(struct ('range',[-1000000,1000000], 'colormap',jet));
% draw_integration_points(feb,gv,struct ('x',geom,'u',scale*u, 'scale',0.000004,'component',2,'data_cmap', dcm,'tessel', 9));
% view (2)
