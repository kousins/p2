disp('Axially symmetric analysis of a shaft');

% Parameters:
E=210e6;% MPa
nu=0.29;
% geometry
R1= 20;% millimeters
Rf=5;% millimeters
R2=R1-Rf;
L1=40;%mm
L2=L1;%mm
% prescribed force
forc=pi*R1^2*200;%N

% Mesh'
mesh_size=3;
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    ['curve 1 line 0 0 ' num2str(R1) ' 0'],...
    ['curve 2 line ' num2str(R1) ' 0 ' num2str(R1) ' ' num2str(L1) ],...
    ['curve 3 arc ' num2str(R1) ' ' num2str(L1) ' ' num2str(R2) ' ' num2str(L1+Rf) ' center ' num2str(R1) ' ' num2str(L1+Rf)],...
    ['curve 4 line ' num2str(R2) ' ' num2str(L1+Rf) '  ' num2str(R2) ' ' num2str(L1+L2)],...
    ['curve 5 line ' num2str(R2) ' ' num2str(L1+L2) ' ' num2str(0) ' ' num2str(L1+L2)],...
    ['curve 6 line '  num2str(0) ' ' num2str(L1+L2)  ' ' num2str(0) ' ' num2str(0)],...
    ['subregion 1  property 1 boundary 1 2 3 4 5 6'],...
    ['m-ctl-point constant ' num2str(mesh_size)],...
    ['m-ctl-point 1 xy ' num2str(R1) ' ' num2str(L1+Rf) ' near ' num2str(mesh_size/400) ' influence ' num2str(mesh_size/10)],...
    }, 1.0, struct('axisymm',true,'quadratic',true));
% drawmesh({fens,gcells},'gcells','facecolor','red')

% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','axisymm'));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',tri_rule (3)));
efeb = feblock_defor_ss (struct ('mater',mater, 'gcells',edge_gcells(edge_groups{5}),...
    'integration_rule',gauss_rule (1, 2)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[0 R1 0 0],'inflate',R1/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+2;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
% The axis of symmetry
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 L1+L2],'inflate',L1/10000));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=ebc_prescribed*0+1;
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
K = start(sparse_sysmat, get(u, 'neqns'));
K = assemble (K, stiffness(feb, geom, u));
% Load
F = start (sysvec, get(u, 'neqns'));
F = assemble (F, nz_ebc_loads(feb, geom, u));
fi=force_intensity(struct('magn',[0;forc/(pi*R2^2)]));
F = assemble (F, distrib_loads(efeb, geom, u, fi, 2));
% Solve
u = scatter_sysvec(u, get(K,'mat')\get(F,'vec'));
% get(u,'values')

% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits',[0 1.06*R1 0 1.1*(L1+L2)]));
scale=1;
cmap = jet;
fld = field_from_integration_points(feb, geom, u, [], 'Cauchy',2);
nvals=get(fld,'values');%min(nvals),max(nvals)
nvalsrange=[min(nvals),max(nvals)];
dcm=data_colormap(struct ('range',nvalsrange, 'colormap',cmap));
colorfield=field(struct ('name', ['colorfield'], 'data',map_data(dcm, nvals)));
geom3=field(struct ('name', ['geom3'], ...
    'data',[get(geom,'values'),0.25*nvals]));
u3=field(struct ('name', ['u3'], ...
    'data',[get(u,'values'),0*nvals]));
draw(feb,gv, struct ('x', geom3, 'u', +scale*u3,'colorfield',colorfield, 'shrink',1.0));
draw_colorbar(gv, struct('colormap',cmap,'position',[0.85 0.15 0.05 0.7],...
    'minmax',nvalsrange,'label','\sigma'));
% view (2)
