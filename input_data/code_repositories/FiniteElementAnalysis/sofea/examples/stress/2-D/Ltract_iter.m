disp('L-shaped domain (one quarter of a square domain with a square hole)');

% Parameters:
E=1e7;
nu=0.3;
integration_order=2;
scale = 1;

% Mesh
[fens,gcells] = L2x2;
[fens,gcells]=refineq4(fens,gcells);
[fens,gcells]=refineq4(fens,gcells);
% [fens,gcells]=refineq4(fens,gcells);
% [fens,gcells]=refineq4(fens,gcells);
% Material
prop = property_linel_iso (struct('E',E,'nu',nu));
mater = mater_defor_ss_linel_biax (struct('property',prop, ...
    'reduction','stress'));
% Finite element block
feb = feblock_defor_ss (struct ('mater',mater, 'gcells',gcells,...
    'integration_rule',gauss_rule (2, integration_order)));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 2, 'fens',fens));
% Define the displacement field
u   = clone(geom,'u');
u   = u*0; % zero out
% Apply EBC's
ebc_fenids=fenode_select (fens,struct('box',[0 0 0 1],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=1*ones(1,length (ebc_fenids));
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[0 1 0 0],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=2*ones(1,length (ebc_fenids));
ebc_val=ebc_fenids*0;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
ebc_fenids=fenode_select (fens,struct('box',[0 1 1 1],'inflate', 0.0001));
ebc_prescribed=ones(1,length (ebc_fenids));
ebc_comp=2*ones(1,length (ebc_fenids));
ebc_val=ebc_fenids*0+0.25;
u   = set_ebc(u, ebc_fenids, ebc_prescribed, ebc_comp, ebc_val);
u   = apply_ebc (u);
% Number equations
u   = numbereqns (u);
% Assemble the system matrix
ems = stiffness(feb, geom, u);
K = dense_sysmat;
K = start (K, get(u, 'neqns'));
K = assemble (K, ems);
K = finish (K);
% Load
F = sysvec;
F = start (F, get(u, 'neqns'));
evs = nz_ebc_loads(feb, geom, u);
F = assemble (F, evs);
F = finish(F);
% Solve
A = get(K,'mat');
b = get(F,'vec');

% Plot
gv=graphic_viewer;
gv=reset (gv,struct ('limits', [0, 1, 0, 1.3]));

xex = A\b;
% D = diag(diag(A));
% C = (1.9A - D);
C = (triu (A) - diag(diag(A)));
D = A - C;
xp= 0.*xex; x=xp;
relax = 1.5;
tolerance =1e-2;
maxiter = 12;
iter=1;
while iter < maxiter
    xn  = D \ (- C * xp + b);
    x = relax*xn + (1-relax)*xp;
    u = scatter_sysvec(u, xex);
    gv=reset (gv,struct ('limits', [0, 1, 0, 1.3]));
    draw(feb, gv, struct ('x',geom,'u', scale*u, 'facecolor','red', 'shrink',1));
    u = scatter_sysvec(u, x);
    draw(feb, gv, struct ('x',geom,'u', scale*u, 'facecolor','none', 'shrink',1));
    view (2); pause(0.5)
    xp= x;
    norm(xex-x)
    iter=iter+1;
end