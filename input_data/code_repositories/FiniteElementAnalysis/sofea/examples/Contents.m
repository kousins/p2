% Examples
% Scripts:
% run_all_examples   - run all examples in the current directory and its
%                      subdirectories
% 
% Folders:
% simple_graphics    - graphics examples
% heat_diffusion     - thermal analysis examples
% mesh_gen           - mesh generation examples
% miscellaneous      - other examples
% stress             - stress analysis examples
% taut_wire          - examples for the taut wire model
% thermo_mechanical  - examples of thermomechanical analysis
