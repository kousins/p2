% TAUT WIRE EXAMPLES
%
% Files
%   <a href="matlab: edit 'w1'">w1</a>          - First example for the taut wire
%   <a href="matlab: edit 'w1d'">w1d</a>         - Taut wire, prescribed displacements
%   <a href="matlab: edit 'w1ssupp'">w1ssupp</a>     - Taut wire, Spring support
%   <a href="matlab: edit 'w2'">w2</a>          - Second example for the taut wire
%   <a href="matlab: edit 'woverspec'">woverspec</a>   - Example of over specified boundary conditions
%   <a href="matlab: edit 'wtransient1'">wtransient1</a> - Taut wire: transient vibration, built-in solver
%   <a href="matlab: edit 'wtransient2'">wtransient2</a> - Taut wire: transient vibration, trapezoidal rule
%   <a href="matlab: edit 'wtranssine1'">wtranssine1</a> - Taut wire: sinusoidal initial condition, built-in
%   <a href="matlab: edit 'wtranssine2'">wtranssine2</a> - Taut wire: sinusoidal initial condition, trapezoidal
%   <a href="matlab: edit 'wvib'">wvib</a>        - Free vibration, simply supported
%   <a href="matlab: edit 'wvibfree'">wvibfree</a>    - Free vibration, Free-floating
%   <a href="matlab: edit 'wvib33c'">wvib33c</a>     - Free vibration,  exercise 33-c
%   <a href="matlab: edit 'wvibmix'">wvibmix</a>     - Free vibration, mixture of mass matrices
%   <a href="matlab: edit 'wvibRich'">wvibRich</a>    - Free vibration, Richardson extrapolation
%   <a href="matlab: edit 'wvibTut'">wvibTut</a>     - Free vibration, pin-roller, Tutorial
%   <a href="matlab: edit 'wvibTut1'">wvibTut1</a>    - Free vibration, pin-pin, Tutorial
