% clear all classes
disp('Taut wire: overspecified boundary conditions');
L=65;
P=1400;
q=3.19;% This is consistent now with the sign convention in the textbook
F_0 =130.13;% This is consistent now with the sign convention in the textbook
F_L =F_0-q*L;
w_0 = -6.1;% This is consistent now with the sign convention in the textbook
N=15; % number of elements
% Mesh
[fens,gcells]= block1d(L,N-1, 1.0);
% Finite element block
feb = feblock_defor_taut_wire(struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',simpson_1_3_rule,...
    'P',P));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
% Define the displacement field
w   = 0*clone(geom,'w');
% Apply EBC's
fenids=[1]; prescribed=[1]; component=[1]; val=w_0;
w   = set_ebc(w, fenids, prescribed, component, val);
w   = apply_ebc (w);
% Number equations
w   = numbereqns (w);
% Assemble the system matrix: the regular part
K = start (dense_sysmat, get(w, 'neqns'));
K = assemble (K, stiffness(feb, geom, w));
% and now the special terms due to the boundary conditions
% 
z = zeros(N-1, 1); zt=z';
z(end) = 0;
A = [get(K,'mat'),z;zt,1];
% Load: body load, nonzero displacement
fi = force_intensity(struct ('magn',inline(num2str(q))));
F = start (sysvec, get(w, 'neqns'));
F = assemble (F, body_loads(feb, geom, w, fi));
F = assemble (F, nz_ebc_loads(feb, geom, w));
% and now the special terms
b=get(F,'vec');
b = [b; -(F_0-q*L)];
% Solve
x=A\b;
w = scatter_sysvec(w, x(1:end-1));
% Plot
xs= (0:0.01:L);
plot (xs, -q/P/2*xs.^2+F_0/P*xs +w_0,'r-','linewidth', 3); hold on
plot (gather (geom, (1:N),'values'), gather (w, (1:N),'values'),'bo-','linewidth', 3); hold on
left_handed_axes;% Plotting is consistent with the textbook sign convention
figure (gcf)
