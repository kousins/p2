function wtranssine1
    disp('Taut wire: transient, sinusoidal initial condition');
    L=7;
    P=0.9;
    mu= 80;
    p=4;
    n=58;
    [fens,gcells]= block1d(L,n, 1.0);
    % Finite element block
    feb = feblock_defor_taut_wire(struct ('mater',mater,...
        'gcells',gcells,...
        'integration_rule',gauss_rule(1, 4),...%simpson_1_3_rule,...
        'P',P,'mu',mu));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    % Define the displacement field
    w   = 0*clone(geom,'w');
    % Apply EBC's
    fenids=[1,n+1]; prescribed=[1, 1]; component=[1, 1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);
    % Number equations
    w   = numbereqns (w);
    % Initial condition
    w0 = gather_sysvec (w);
    v0 = w0; v0=sin((1:n-1)*L/n*p*pi/L)';
    % Assemble the stiffness matrix
    K = start (dense_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    Kmat =get(K,'mat');
    % Assemble the consistent mass matrix
    M = start (dense_sysmat, get(w, 'neqns'));
    % M = assemble (M, mass(feb, geom, w));
    % Assemble the lumped mass matrix
    M = assemble(M, elemat( struct('eqnums', (1:n-1),'mat', eye(n-1) * mu*L/n)));
    Mmat=get(M,'mat');
    function [ts,ys] = trapezoidal(nsteps,tspan,w0,v0)
        ts= zeros(nsteps, 1);
        nu =length(w0);
        ys = zeros(2*nu,nsteps);
        dt = (tspan(2)-tspan(1))/nsteps;
        t =tspan(1);
        for i=1:nsteps
            ts(i) =t;
            ys(1:nu,i) =w0; ys(nu+1:end,i) =v0; 
            v1=(Mmat+((dt/2) ^ 2)*Kmat)\((Mmat-((dt/2) ^ 2)*Kmat)*v0-dt*Kmat*w0);
            w1=w0+dt/2*(v0+v1);
            w0=w1;v0=v1;
            t=t+dt;
        end
        ys=ys';
    end
    %     ode23(@f,[0, 50],[w0;v0],odeset('Mass',firstMmat));
    [ts,ys]=trapezoidal(3000,[0, 150],w0,v0);
     function plot_history_surface(ts, ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        vs=[0*ys(:, 1), ys(:, 1:nt), 0*ys(:, 1)];

        surf(ts,(1:nt+2),vs', 'FaceColor', 'interp', 'EdgeColor', 'none');
        hold on;
        camlight headlight ;
        contour3(ts,(1:nt+2),vs', 10, 'k-');
    end
    function plot_history_animation(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        vmin=min(min(ys(:,1:nt)));
        vmax=max(max(ys(:,1:nt)));
        for j=1:nsteps
            v=ys(j,1:nt);
            plot((1:nt+2),[0, v, 0]);
            axis([1 nt+2 vmin vmax]);
            pause(1/nt^2);
        end
    end
    function plot_history_kinetic_energy(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        ke= 0*ts;
        for j=1:nsteps
            v=ys(j,nt+1:end);
            ke(j) =v*Mmat*v'/2;
        end
        plot(ts,ke)
    end
    function plot_history_potential_energy(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        ke= 0*ts;
        for j=1:nsteps
            w=ys(j,1:nt);
            ke(j) =w*Kmat*w'/2;
        end
        plot(ts,ke,'r-')
    end
%         plot_history_surface(ts,ys)
%         plot_history_animation(ts,ys)
    plot_history_kinetic_energy(ts,ys)
    hold on
    plot_history_potential_energy(ts,ys)
end