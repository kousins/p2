disp('Taut wire: characteristic frequencies');
L=0.5;
P=20;
mu= 0.0150;
neigvs=1;
ns=[5]; % number of elements
% ns=[32]; % number of elements
eigenvalue_errors = zeros(length(ns),neigvs);
% Mesh
for mesh=1:length(ns)
    n=ns(mesh);
    [fens,gcells]= block1d(L,n, 1.0);
    % Finite element block
    feb = feblock_defor_taut_wire(struct ('mater',mater,...
        'gcells',gcells,...
        'integration_rule',trapezoidal_rule,...%gauss_rule(1, 2),...%simpson_1_3_rule,...
        'P',P,'mu',mu));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    % Define the displacement field
    w   = 0*clone(geom,'w');
    % Apply EBC's
    fenids=[1,n+1]; prescribed=[1, 1]; component=[1, 1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);
    % Number equations
    w   = numbereqns (w);
    % Assemble the stiffness matrix
    K = start (sparse_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    % Assemble the consistent mass matrix
    M = start (sparse_sysmat, get(w, 'neqns'));
    M = assemble (M, mass(feb, geom, w));
    % Solve
    [W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');
    [Omegas,ix]=sort(diag(Omega));

    scale=10;
    for i=1:neigvs
        analyt_eigenvalue =(P/mu*(i*pi/L) ^ 2);
        eigenvalue_errors(mesh,i) =...
            (sqrt(Omegas(i))/2/pi-sqrt(analyt_eigenvalue)/2/pi)/(sqrt(analyt_eigenvalue)/2/pi);
        disp(['  Eigenvector ' num2str(i) ...
            ' eigenvalue ' num2str(sqrt(Omegas(i))/2/pi) ' versus '...
            num2str(sqrt(analyt_eigenvalue)/2/pi)]);
        figure;
        w = scatter_sysvec(w, W(:,ix(i)));
        plot (gather (geom, (1:n+ 1),'values'), ...
            gather (w, (1:n+ 1),'values'),'ro-','linewidth', 3);
        hold on
        figure (gcf)
        pause(0.2); % next eigenvector

    end
end
figure
plot (ns,eigenvalue_errors,'linewidth', 3)
set(gca,'FontSize', 14) 
xlabel('Number of elements [ND]');        ylabel('(f_h-f_a)/f_a');
                

