function wtransient1
    disp('Taut wire: transient vibration, built-in solver');
    L=7;
    P=0.9;
    mu= 80;
    n=58;
    [fens,gcells]= block1d(L,n, 1.0);
    % Finite element block
    feb = feblock_defor_taut_wire(struct ('mater',mater,...
        'gcells',gcells,...
        'integration_rule',gauss_rule(1, 4),...%simpson_1_3_rule,...
        'P',P,'mu',mu));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    % Define the displacement field
    w   = 0*clone(geom,'w');
    % Apply EBC's
    fenids=[1,n+1]; prescribed=[1, 1]; component=[1, 1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);
    % Number equations
    w   = numbereqns (w);
    % Initial condition
    w0 = gather_sysvec (w);
    v0 = w0; v0(round(n/2)) = 1;
    % Assemble the stiffness matrix
    K = start (dense_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    Kmat =get(K,'mat');
    % Assemble the consistent mass matrix
    M = start (dense_sysmat, get(w, 'neqns'));
    % Assemble the lumped mass matrix
    M = assemble(M, elemat( struct('eqnums', (1:n-1),'mat', eye(n-1) * mu*L/n)));
    Mmat=get(M,'mat');
    Mattilda= [eye(get(w, 'neqns')),zeros(get(w, 'neqns'));...
        zeros(get(w, 'neqns')),Mmat];
    % Solve
    function rhs =f(t,y)
        neq=length(y)/2;
        w=y(1:neq); v=y(neq+1:end);
        rhs= [v;-Kmat*w];
    end
    [ts,ys]=ode23(@f,[0, 1500],[w0;v0],odeset('Mass',Mattilda,'RelTol',1e-3));
    % Plotting
    function plot_history_surface(ts, ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        vs=[0*ys(:, 1), ys(:, 1:nt), 0*ys(:, 1)];
        surf(ts,(1:nt+2),vs', 'FaceColor', 'interp', 'EdgeColor', 'none');
        hold on;
        camlight headlight ;
        contour3(ts,(1:nt+2),vs', 10, 'k-');
    end
    function plot_history_animation(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        vmin=min(min(ys(:,1:nt)));
        vmax=max(max(ys(:,1:nt)));
        for j=1:nsteps
            v=ys(j,1:nt);
            plot((1:nt+2),[0, v, 0]);
            axis([1 nt+2 vmin vmax]);
            pause(1/nt^2);
        end
    end
    function ke=history_kinetic_energy(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        ke= 0*ts;
        for j=1:nsteps
            v=ys(j,nt+1:end);
            ke(j) =v*Mmat*v'/2;
        end
    end
    function pe=history_potential_energy(ts,ys)
        nsteps=length(ts);
        nt=round(size(ys,2)/2);
        pe= 0*ts;
        for j=1:nsteps
            w=ys(j,1:nt);
            pe(j) =w*Kmat*w'/2;
        end
    end
%     figure
%     set(gca,'FontSize', 14)
%     plot_history_surface(ts,ys)
%     xlabel(' Time [seconds]')
%     ylabel(' Coordinate')
%     zlabel(' Deflection amplitude')
%     figure
    %         plot_history_animation(ts,ys)
    ke=history_kinetic_energy(ts,ys);
    pe=history_potential_energy(ts,ys);
    set(gca,'FontSize', 14)
    plot(ts,pe+ke,'b-','linewidth', 3)
    xlabel(' Time [seconds]')
    ylabel(' Total energy')

end