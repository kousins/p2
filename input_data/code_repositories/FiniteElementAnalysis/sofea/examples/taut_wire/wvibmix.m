disp('Taut wire: characteristic frequencies');
L=7;
P=0.9;
mu= 80;
mix = 0.59;
neigvs=5;
ns=[8, 16, 32, 64, 128]; % number of elements
% ns=[32]; % number of elements
eigenvalue_errors = zeros(length(ns),neigvs);
% Mesh
for mesh=1:length(ns)
    n=ns(mesh);
    [fens,gcells]= block1d(L,n, 1.0);
    % Finite element block
    feb = feblock_defor_taut_wire(struct ('mater',mater,...
        'gcells',gcells,...
        'integration_rule',gauss_rule(1, 4),...%simpson_1_3_rule,...
        'P',P,'mu',mu));
    % Geometry
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    % Define the displacement field
    w   = 0*clone(geom,'w');
    % Apply EBC's
    fenids=[1,n+1]; prescribed=[1, 1]; component=[1, 1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);
    % Number equations
    w   = numbereqns (w);
    % Assemble the stiffness matrix
    K = start (sparse_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    % Assemble the consistent mass matrix
    Mc = start (sparse_sysmat, get(w, 'neqns'));
    Mc = assemble (Mc, mass(feb, geom, w));
    % Assemble the lumped mass matrix
    Ml = start (sparse_sysmat, get(w, 'neqns'));
    Ml = assemble(Ml, elemat( struct('eqnums', (1:n-1),'mat', eye(n-1) * mu*L/n)));
    % Solve
    [W,Omega]=eigs(get(K,'mat'),...
        mix*get(Mc,'mat')+(1-mix)*get(Ml,'mat'),neigvs,'SM');
    [Omegas,ix]=sort(diag(Omega));

    scale=10;
    for i=1:neigvs
        analyt_eigenvalue =(P/mu*(i*pi/L) ^ 2);
        eigenvalue_errors(mesh,i) =...
            (Omegas(i)-analyt_eigenvalue)/analyt_eigenvalue;
        disp(['  Eigenvector ' num2str(i) ...
            ' eigenvalue ' num2str(Omegas(i)) ' versus '...
            num2str(analyt_eigenvalue)]);
        %         figure;
        %         w = scatter_sysvec(w, W(:,ix(i)));
        %         plot (gather (geom, (1:n+ 1),'values'), ...
        %             gather (w, (1:n+ 1),'values'),'ro-','linewidth', 3);
        %         hold on
        %         figure (gcf)
        %         pause(2); % next eigenvector

    end
end
plot (ns,eigenvalue_errors,'linewidth', 3)
set(gca,'FontSize', 14)
xlabel('Number of elements [ND]');        ylabel('(\omega_h-\omega)/\omega');


