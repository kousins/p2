disp('Taut wire: example 3-- statics, uniform load+ support settlement');
L=6;
P=4;
q =0.1;% This is consistent now with the sign convention in the textbook
w0=0.2;
n=2; % number of elements
% Mesh
x=0;
fens=[];
for j= 1:n+1
    fens=[fens fenode(struct ('id',j,'xyz',[x]));];
    x = x+(L/n);
end
gcells = [];
for j= 1:n
    gcells = [gcells gcell_L2(struct('id',j,'conn',[j j+1]))];
end
% Finite element block
feb = feblock_defor_taut_wire(struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',simpson_1_3_rule,...
    'P',P));
% Geometry
geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
% Define the displacement field
w   = 0*clone(geom,'w');
% Apply EBC's
fenids=[1]; prescribed=[1]; component=[1]; val=w0;
w   = set_ebc(w, fenids, prescribed, component, val);
w   = apply_ebc (w);
% Number equations
w   = numbereqns (w);
% Assemble the system matrix
K = start (dense_sysmat, get(w, 'neqns'));
K = assemble (K, stiffness(feb, geom, w));
% Load
fi = force_intensity(struct ('magn',inline(num2str(q))));
F = start (sysvec, get(w, 'neqns'));
F = assemble (F, body_loads(feb, geom, w, fi));
F = assemble (F, nz_ebc_loads(feb, geom, w));
% Solve
w = scatter_sysvec(w, get(K,'mat')\get(F,'vec'));
% Plot
xs= (0:0.01:L);
plot (xs, w0-q/P*xs.*(xs/2-L),'r-','linewidth', 3); 
hold on
plot (gather (geom, (1:n+ 1),'values'), ...
    gather (w, (1:n+ 1),'values'),'bo-','linewidth', 3); 
left_handed_axes% Plotting is consistent with the textbook sign convention
figure (gcf)
