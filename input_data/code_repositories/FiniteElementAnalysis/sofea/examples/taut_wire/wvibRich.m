disp('Taut wire, simple supported: Richardson extrap of frequencies');
L=3.5; P=20; mu= 0.0150;
neigvs=10;
ns=[1,2,4]*15; % number of elements
analyt_errors = zeros(length(ns),neigvs);
Frequencies = zeros(length(ns),neigvs);
for mesh=1:length(ns)
    [fens,gcells]= block1d(L,ns(mesh), 1.0);
    feb = feblock_defor_taut_wire(struct ('mater',mater,'gcells',gcells,...
        'integration_rule',gauss_rule(1, 2),'P',P,'mu',mu));
    geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
    w   = 0*clone(geom,'w');
    fenids=[1,ns(mesh)+1]; prescribed=[1,1]; component=[1,1]; val=[0,0];
    w   = set_ebc(w, fenids, prescribed, component, val);
    w   = apply_ebc (w);    w   = numbereqns (w);
    K = start (sparse_sysmat, get(w, 'neqns'));
    K = assemble (K, stiffness(feb, geom, w));
    M = start (sparse_sysmat, get(w, 'neqns'));
    M = assemble (M,mass(feb, geom, w));
    [W,Omega]=eigs(get(K,'mat'),get(M,'mat'),neigvs,'SM');
    [Omegas,ix]=sort(diag(Omega));

    for i=1:neigvs
        analyt_frequency =sqrt((P/mu*(i*pi/L) ^ 2))/2/pi;
        Frequencies(mesh,i) =sqrt(Omegas(i))/2/pi;
        analyt_errors(mesh,i) =...
            (Frequencies(mesh,i)-analyt_frequency)/(analyt_frequency);
    end
end

Estimated_errors(1:neigvs) = 0;
for i=1:neigvs
    [xestim, beta] = richextrapol(Frequencies(:,i),L./ns);
    Estimated_errors(i) = (Frequencies(end,i)-xestim)/xestim;
end
figure;
plot (Estimated_errors,'linewidth', 3);
hold on
plot (analyt_errors(end,:) ,'r','linewidth', 3);
set(gca,'FontSize', 14)
xlabel(' Mode [ND]');        ylabel('Relative frequency error');
