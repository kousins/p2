[fens,gcells] = h8cylinder(0.13,0.195, 1.8425, 15, 4, 5);
length(fens)
bg=mesh_bdry(gcells);
gv =drawmesh({fens,bg},'gcells','facecolor','none');


% l=gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)(-[x(1:2),0]),'tolerance',eps));
% gv =drawmesh({fens,subset(bg,l)},'gv',gv,'gcells','facecolor','b')


l=[gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)(-[x(1:2),0]),'tolerance',eps)),...
    gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)([x(1:2),0]),'tolerance',eps))];
gv =drawmesh({fens,subset(bg,setdiff(1:length(bg),l))},'gv',gv,'gcells','facecolor','b');
l=[gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)(-[x(1:2),0]),'tolerance',eps))];
gv =drawmesh({fens,subset(bg,l)},'gv',gv,'gcells','facecolor','y');
