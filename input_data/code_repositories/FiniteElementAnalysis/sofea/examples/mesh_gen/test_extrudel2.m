function test_extrudel2
    [fens,gcells] = block1d(4.0,5, struct('other_dimension', 3.1));
    function xyz= upz(xyz, layer)
        xyz= [xyz+(layer^2)*[0.,-0.2], layer*2.5];
    end
    function xyz= up(xyz, layer)
        xyz= [xyz(1),layer*.5];
    end
    [fens,gcells] = extrudel2(fens,gcells,2,@up)
    % drawmesh({fens,gcells},'gcells','nodes','facecolor','red')
    drawmesh({fens,gcells},'gcells','facecolor','red')
    % ix =gcell_select(fens,bg,...
    %     struct ('box',[-100 100 -100 0 -100 0],'inflate', 0.5))
    % drawmesh({fens,bg(ix)},'facecolor','red','shrink', 1.0)
end