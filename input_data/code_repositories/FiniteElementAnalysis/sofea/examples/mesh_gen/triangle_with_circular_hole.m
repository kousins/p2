h=5;
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher({...
    'curve 1 line 0 0 75 0',...
    'curve 2 line 75 0 75 40',...
    'curve 3 line 75 40 0 0',...
    'curve 4 circle Center 60 10 radius 8',...
    ['subregion 1  property 1 boundary '...
    ' 1  2 3 hole -4'],...
    ['subregion 2  property 2 boundary 4'],...
    ['m-ctl-point constant ' num2str(h)]
    }, 1);
drawmesh({fens,gcells},'gcells','shrink', 0.8)
view(2)
