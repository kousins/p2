function test_merge
    [fens,gcells] = L2x2;
    function xyz= up(xyz, layer)
        xyz= [xyz+(layer^2)*[0.2,-0.2], layer*2.5];
    end
    function xyz= down(xyz, layer)
        xyz= [xyz-(layer^2)*[0.2,-0.2], -layer*2.5];
    end
    [fens1,gcells1] = extrudeq4(fens,gcells,4,@up);
    [fens2,gcells2] = extrudeq4(fens,gcells,5,@down);
    [fens,gcells1,gcells2] = merge_meshes(fens1, gcells1, fens2, gcells2, eps);
    gcells= cat(2,gcells1,gcells2);
    % drawmesh({fens,gcells},'gcells','nodes','facecolor','red')
    drawmesh({fens,gcells},'gcells','facecolor','red'); hold on
%     drawmesh({fens,gcells2},'gcells','facecolor','Green'); hold on
    % ix =gcell_select(fens,bg,...
    %     struct ('box',[-100 100 -100 0 -100 0],'inflate', 0.5))
    % drawmesh({fens,bg(ix)},'facecolor','red','shrink', 1.0)
end