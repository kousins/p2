function test_block
    L=2;H=3;
    Handles ={@block2d,@t3block2d,@t3block2dc,@at3block2d,@at3block2dc,@ct3block2d,@crosst3block,@t3block2dn,@at3block2dn,@t3block2du};
    nr=2;
    nc =ceil(length( Handles )/2);
    gv= graphic_viewer;
    i=1;
    for m= Handles
        subplot(nr,nc,i); i=i+1;
         axis equal ;
        [fens,gcells] = m{1}(L,H,3, 4, 1.0);
        drawmesh({fens,gcells},'gv',gv,'gcells','facecolor','red');
        title(func2str (m{1}))
        view(2)
        pause(2)
    end
end