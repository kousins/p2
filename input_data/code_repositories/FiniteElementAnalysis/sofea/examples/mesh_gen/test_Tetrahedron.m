[fens,gcells] = t4cylinderdel(3.0,1.2, 2, 1);
[fens,gcells] = T4_to_T10(fens,gcells);
bg=mesh_bdry(gcells);
% drawmesh({fens,gcells},'gcells','nodes','facecolor','red')
drawmesh({fens,bg},'gcells','facecolor','red')
% ix =gcell_select(fens,bg,...
%     struct ('box',[-100 100 -100 0 -100 0],'inflate', 0.5))
% drawmesh({fens,bg(ix)},'facecolor','red','shrink', 1.0)
