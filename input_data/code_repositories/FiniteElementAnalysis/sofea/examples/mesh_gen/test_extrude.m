function test_extrude
    [fens,gcells] = L2x2;
    function xyz= up(xyz, layer)
        xyz= [xyz+(layer^2)*[0.2,-0.2], layer*2.5];
    end
    [fens,gcells] = extrudeq4(fens,gcells,3,@up);
    % drawmesh({fens,gcells},'gcells','nodes','facecolor','red')
    drawmesh({fens,gcells},'gcells','facecolor','red')
    % ix =gcell_select(fens,bg,...
    %     struct ('box',[-100 100 -100 0 -100 0],'inflate', 0.5))
    % drawmesh({fens,bg(ix)},'facecolor','red','shrink', 1.0)
end