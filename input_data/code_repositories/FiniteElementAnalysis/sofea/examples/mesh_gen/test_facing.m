[fens,gcells] = t4block(0.5*pi,0.95, 0.425, 5, 4, 2);

fens = transform_apply(fens,@(x, data) (x+ [0, 2.5, 0]), []);
climbPerRevolution= 0;
fens = transform_2_helix(fens,climbPerRevolution);
bg=mesh_bdry(gcells);
gv =drawmesh({fens,bg},'gcells','facecolor','none')


l=gcell_select(fens,bg,struct ('facing', true, 'direction', [0,0,1]));
gv =drawmesh({fens,bg(l)},'gv',gv,'gcells','facecolor','y')
l=gcell_select(fens,bg,struct ('facing', true, 'direction', [0,0,-1]));
gv =drawmesh({fens,bg(l)},'gv',gv,'gcells','facecolor','r')
l=gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)(-x),'tolerance',eps));
gv =drawmesh({fens,bg(l)},'gv',gv,'gcells','facecolor','b')
l=gcell_select(fens,bg,struct ('facing', true, 'direction', @(x)(+[x(1:2),0]),'tolerance',0.01));
gv =drawmesh({fens,bg(l)},'gv',gv,'gcells','facecolor','c')
