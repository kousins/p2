function mksphere
    R= 300;
    thickness =15;
    nlayers =3;
    [fens,gcells]=q4sphere(R,1,thickness);
    function xyz= radially(xyz, layer)
        xyz= (R + layer/nlayers*thickness)*xyz/norm(xyz);
    end
    [fens,gcells] = extrudeq4(fens,gcells,nlayers,@radially);
    drawmesh({fens,gcells},'gcells','facecolor','red')
    %         function xyz= up(xyz, layer)
    %         xyz= [xyz+(layer^2)*[0.2,-0.2], layer*2.5];
    %     end
    %     [fens,gcells] = extrudeq4(fens,gcells,3,@up);
    %     % drawmesh({fens,gcells},'gcells','nodes','facecolor','red')
    %     drawmesh({fens,gcells},'gcells','facecolor','red')
    % ix =gcell_select(fens,bg,...
    %     struct ('box',[-100 100 -100 0 -100 0],'inflate', 0.5))
    % drawmesh({fens,bg(ix)},'facecolor','red','shrink', 1.0)
end