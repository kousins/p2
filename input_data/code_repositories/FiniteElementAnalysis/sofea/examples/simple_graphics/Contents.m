% EXAMPLES OF GRAPHICS OUTPUT
% 
% Files
%   <a href="matlab: edit 'Polly'">Polly</a>      - Polygon
%   <a href="matlab: edit 'arr'">arr</a>        - Arrow
%   <a href="matlab: edit 'cylin'">cylin</a>      - Cylinder
%   <a href="matlab: edit 'drawh20'">drawh20</a>    - Quadratic hexahedron
%   <a href="matlab: edit 'drawonehex'">drawonehex</a> - Hexahedron
%   <a href="matlab: edit 'drawq8'">drawq8</a>     - Quadratic quadrilateral
%   <a href="matlab: edit 'ellip'">ellip</a>      - Ellipsoids
%   <a href="matlab: edit 'textex'">textex</a>     - Text
