gv=graphic_viewer;
gv=reset (gv,struct ('limits', 2*[-2 2 -2 2 -2 2]));
x=[-1 -1 0];
v=0.5*[3 4 -5];
draw_arrow(gv,x,v, struct ('color',['yellow']));
draw_arrow(gv,x,-v, struct ('color',['black']));
draw_arrow(gv,x,[1 0 0], struct ('color',['red']));
draw_arrow(gv,x,[0 1 0], struct ('color',['Green']));
draw_arrow(gv,x,[0 0 1], struct ('color',['blue']));
% rotate(gv,[]);

