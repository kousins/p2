u=[      0         0         0
         0         0         0
         0         0         0
         0         0         0
   -0.0696    0.0741    0.0625
   -0.0946    0.0946    0.0347
   -0.0741    0.0696    0.0625
   -0.1381    0.1381   -0.3417];
[fens,gcells] = block(2, 2, 2, 1, 1, 1);
gv=graphic_viewer;
gv=reset (gv,[]);
dcm=data_colormap(struct ('range',[min(u(:,3)),max(u(:,3))], 'colormap',jet));
colors=map_data(dcm, u(:,3));
colorfield=field(struct ('name', ['colorfield'], 'data',colors));
geom=field(struct ('name', ['geom'], 'dim', 3,'fens',fens));
U=field(struct ('name', ['geom'], 'data',u));
draw(gcells(1), gv, struct ('x',geom,'u',U, 'colorfield',colorfield, 'shrink',0.9))
draw(gcells(1), gv, struct ('x',geom,'u',0*U, 'facecolor','none'))
