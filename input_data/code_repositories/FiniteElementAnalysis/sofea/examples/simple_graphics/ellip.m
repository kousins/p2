gv=graphic_viewer;
gv=reset (gv,[]);
headlight(gv);
x=[-1 -1 0];
v=(-2:2);
dcm=data_colormap(struct ('range',[min(v),max(v)], 'colormap',jet));
colors=map_data(dcm, v);
draw_ellipsoid(gv,x,eye(3,3), [1 2 3], struct ('facecolor','red'));
pause(2)
gv=clear(gv,[]);
gv=reset (gv,[]);
headlight(gv);
draw_ellipsoid(gv,x,eye(3,3), [1 2 3], struct ('facecolor','red', 'tessel', 24));
pause(2)
for i=1:10
    sigma=rand(3,3)-0.5; sigma=sigma+sigma';
    [V,D]=eig(sigma);
    gv=clear(gv,[]);
    gv=reset (gv,[]);
    headlight(gv);
    dcm=data_colormap(struct ('range',[min(v),max(v)], 'colormap',jet));
    color=map_data(dcm, sum (diag(D)));
    draw_ellipsoid(gv,x,V,diag(D), struct ('facecolor',color, 'tessel', 24));
    pause (1);
end
