gv=graphic_viewer;
gv=reset (gv,[]);
x=[-1 -1 0; 1 -2 0; 0 1 2; 4 2 3; -1 -2 -3; 1 4 2];
v=[0 1 2 -1 -2 0];
% dcm=data_colormap(struct ('rmin',min(v),'rmax',max(v)));
dcm=data_colormap(struct ('range',[min(v),max(v)], 'colormap',jet));
colors=map_data(dcm, v);
draw_polygon(gv,x,[1 2 4], struct ('colors',colors));
draw_polygon(gv,x,[3 2 4], struct ('colors',colors));
draw_polygon(gv,x,[3 5 4], struct ('colors',colors));
pause(2)
gv=clear(gv,[]);
gv=reset (gv,struct ('limits',[-10 10 -10 10 -10 10]));
draw_polygon(gv,x,[1 2 3], struct ('facecolor',['yellow']));
