[fens,gcells] = block2d(20, 20, 1, 1, []);
[fens,gcells] = Q4_to_Q8(fens,gcells, []);
fens = transform_apply(fens,@(x,d)([x,10]+[-10,-10,1]), []);
fens = transform_apply(fens,@(x,d)(x-0.00012*x(1)^2*x(2)^2*[0,1,0]+0.004*x(3)^2*[1,1,0]+0.007*x(2)^2*[0,0,1]), []);
gv=graphic_viewer;
gv=reset (gv,[]);
geom=field(struct ('name', ['geom'], 'dim', 3,'fens',fens));
U=0*geom;
draw(gcells(1), gv, struct ('x',geom,'u',0*U, 'facecolor','red','shrink',0.9))
draw(gcells(1), gv, struct ('x',geom,'u',0*U, 'facecolor','none'))

