gv=graphic_viewer;
gv=reset (gv,struct ('limits',[-10 10 -10 10 -10 10]));
x=[-1 -1 0; 1 -2 0; 0 1 2; 4 2 3; -1 -2 -3; 1 4 2];
v=[0 1 2 -1 -2 0];
% dcm=data_colormap(struct ('rmin',min(v),'rmax',max(v)));
draw_polygon(gv,x,[4 2 3], struct ('facecolor',['none']));
for i=1:size (x,1) 
    draw_text(gv,x(i,:),num2str(i), struct ('offset',0.1));
end