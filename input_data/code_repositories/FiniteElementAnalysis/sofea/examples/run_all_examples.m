% Run all in examples in the current directory and its subdirectories.
% The results are written into a log file.
function run_all_examples
if strcmp(version('-release'),'13')
    fid = fopen(['results-' '.log'],'w');
else
    fid = fopen(['results-' datestr(now, 'HH-dd-mm-yyyy') '.log'],'w');
end
run_in_subdirs(pwd, fid)
fclose (fid);
return;
end

function run_in_subdirs(d, fid)
sep=filesep;
dl=dir(d);
for i=1:length(dl)
    if (dl(i).isdir)
        if      (~strcmp(dl(i).name,'.')) & ...
                (~strcmp(dl(i).name,'..')) & ...
                (~strcmp(dl(i).name,'CVS')) & ...
                (~strcmp(dl(i).name,'cvs')) & ...
                (~strcmp(dl(i).name(1),'@'))
            run_in_subdirs([d sep dl(i).name], fid);
        end
    else
        if ((~strcmp(dl(i).name,'Contents.m')) &...
                (~strcmp(dl(i).name,'run_all_examples.m')) &...
                strcmp(dl(i).name,...
                [dl(i).name(1:length(dl(i).name)-2) '.m']))
            fn=dl(i).name(1:length(dl(i).name)-2);
            disp(['Running ' fn]);
            fprintf(fid,'%s',['Running ' fn ':']);
            eval(fn);
            disp(['         Done: pausing']);
            fprintf(fid,' %s\n',['done']);
            pause(1)
            while ~isempty(get(0,'CurrentFigure'))
                try
                    delete(get(0,'CurrentFigure'));
                catch
                end
            end
        end
    end
end
return;
end