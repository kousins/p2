% Composite block simulation

kappaLayer1=[2.25 0; 0 0.06]; % orthotropic conductivity matrix
kappaLayer2=[0.25 0; 0 0.25]; % isotropic conductivity matrix 
alpha =45;% local material orientation angle 
ca=cos(2*pi/360*alpha); 
sa=sin(2*pi/360*alpha); 
Rm = [ca, -sa;sa, ca];% local material directions 
num_integ_pts=1; % 1-point quadrature 
q = 330; % heat source on top surface
cv_composite1 = 300*1e3*7500*1e-9;
cv_composite2 = 100*1e3*7500*1e-9;
Tc = 200;
Thot = 500;
dt=5; % time step
tend= 100; % length of the time interval
t=0;
theta = 1.0; % generalized trapezoidal method
online_graphics= true;% plot the solution as it is computed?

[fens,gcells,groups,edge_gcells,edge_groups] = targe2_mesher({...
     ['curve 1 line -40 20 40 20'],...
     ['curve 2 line 40 20 40 40'],...
     ['curve 3 line 40 40 -40 40'],...
     ['curve 4 line -40 40 -40 20'],...
     ['curve 5 line -40 20 -40 0'],...
     ['curve 6 line -40 0 40 0'],...
     ['curve 7 line 40 0 40 20'],...
     ['curve 8 line -40 0 -40 -20'],...
     ['curve 9 line -40 -20 40 -20'],...
     ['curve 10 line 40 -20 40 0'],...
     ['curve 11 line -40 -20 -40 -40'],...
     ['curve 12 line -40 -40 40 -40'],...
     ['curve 13 line 40 -40 40 -20'],...
     ['subregion 1  property 1 ' ...
     '   boundary 1 2 3 4'],...
     ['subregion 2  property 2 '...
     '   boundary 6 7 -1 5 '],...
     ['subregion 3  property 1 ' ...
     '   boundary 9 10 -6 8'],...
     ['subregion 4  property 2 ' ...
     '   boundary 12 13 -9 11'],...
     ['m-ctl-point constant 2.75']
     }, 1.0);


% Layer 1 creation
propLayer1 = property_diffusion(struct('conductivity',kappaLayer1,...
     'specific_heat',cv_composite1, 'source',0));
materLayer1=mater_diffusion(struct('property',propLayer1));
febLayer1 = feblock_diffusion(struct ('mater',materLayer1,...
     'gcells',gcells(groups{1}),...
     'integration_rule',tri_rule(num_integ_pts),'Rm',Rm));

% Layer 2 creation
propLayer2 = property_diffusion(struct('conductivity',kappaLayer2,...
     'specific_heat',cv_composite2, 'source',0));
materLayer2=mater_diffusion(struct('property',propLayer2));
febLayer2 = feblock_diffusion(struct ('mater',materLayer2,...
     'gcells',gcells(groups{2}),...
     'integration_rule',tri_rule(num_integ_pts)));

% Layer 3 uses same material as Layer 1
febLayer3 = feblock_diffusion(struct ('mater',materLayer1,...
     'gcells',gcells(groups{3}),...
     'integration_rule',tri_rule(num_integ_pts),'Rm',Rm));

% Layer 4 uses same material as Layer 2
febLayer4 = feblock_diffusion(struct ('mater',materLayer2,...
     'gcells',gcells(groups{4}),...
     'integration_rule',tri_rule(num_integ_pts)));

geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens)); 
tempn = field(struct('name',['tempn'], 'dim', 1, 'nfens',get(geom,'nfens')));

% Apply a constant temperature on the left, bottom and right surface 
fenids1=[fenode_select(fens,struct('box', [-40 -40 -40 40],...
     'inflate', 0.01))];
fenids2=[fenode_select(fens,struct('box' ,[40 -40 -40 -40],...
     'inflate', 0.01))];
fenids3=[fenode_select(fens,struct('box', [40 40 -40 40],...
     'inflate', 0.01))];
fenids = cat(2, fenids1, fenids2);
fenids = cat(2, fenids, fenids3);
prescribed=ones(length(fenids),1);
comp=[];
val=zeros(length(fenids),1)+Tc;% hot temperature 
tempn = set_ebc(tempn, fenids, prescribed, comp, val); 

% Apply a constant temperature on the top surface
fenids=[fenode_select(fens,struct('box', [-40 40 40 40],...
     'inflate', 0.01))];
val=zeros(length(fenids),1)+Thot;% hot temperature 
tempn = set_ebc(tempn, fenids, prescribed, comp, val); 
 
tempn = apply_ebc (tempn);
% all temperature in all the layers are 200
tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Tc); 

K = start (sparse_sysmat, get(tempn, 'neqns')); 
K = assemble (K, conductivity(febLayer1, geom, tempn)); 
K = assemble (K, conductivity(febLayer2, geom, tempn)); 
K = assemble (K, conductivity(febLayer3, geom, tempn)); 
K = assemble (K, conductivity(febLayer4, geom, tempn)); 
Km = get(K,'mat');
C = start (sparse_sysmat, get(tempn, 'neqns'));
C = assemble (C, capacity(febLayer1, geom, tempn));
C = assemble (C, capacity(febLayer2, geom, tempn));
C = assemble (C, capacity(febLayer3, geom, tempn));
C = assemble (C, capacity(febLayer4, geom, tempn));
Cm = get(C,'mat');

if online_graphics
    gv=graphic_viewer;
    dcm=data_colormap(struct('range',[Tc,Thot],'colormap',jet));
end
minmaxT = []

while t<tend+0.1*dt % Time stepping
    if online_graphics
%         set(gca,'FontSize', 14)
        T=get(tempn,'values');
        colorfield=field(struct('name',['colorfield'],'data',...
            map_data(dcm, T)));
        geomT=field(struct ('name', ['geomT'], ...
            'data',[get(geom,'values'), get(tempn,'values')]));
        gv=reset (gv,struct('limits', [-40,40,-40,40,200,500]));
%         camset (gv,[-671.7042 -895.3086 695.7305 58.8095 ...
%             56.7155 2.9101 0 0 1 7.3342])
        for i=1:length (gcells)
            draw(gcells(i),gv,struct('x',geomT, 'u',0*geomT,...
                'colorfield',colorfield,'edgecolor','none', 'shrink',1.0));
%             draw(gcells(i),gv,struct('x',geom, 'u',0*geom, ...
%                 'facecolor','none'));
        end
%         camset (gv,[-671.7042 -895.3086 695.7305 58.8095 ...
%             56.7155 2.9101 0 0 1 7.3342])
        xlabel('X [mm]');        ylabel('Y [mm]');
        zlabel('Temperature [degrees C]')
        headlight(gv);
        figure(gcf);
        title (['Time =' num2str(t)]); hold off; pause(0.1);
                saveas(gcf, ['shrinkfit-' num2str(t) '.png'], 'png');
    end
    
    tempn1 = tempn;

    F = start (sysvec, get(tempn, 'neqns')); 
    F = assemble (F, nz_ebc_loads_conductivity(febLayer1, geom, tempn)); 
    F = assemble (F, nz_ebc_loads_conductivity(febLayer2, geom, tempn)); 
    F = assemble (F, nz_ebc_loads_conductivity(febLayer3, geom, tempn)); 
    F = assemble (F, nz_ebc_loads_conductivity(febLayer4, geom, tempn)); 
    Tn=gather_sysvec(tempn);
%     minmaxT= [minmaxT; min(Tn),max(Tn)]
    Tn1 = (1/dt*Cm+theta*Km) \ ((1/dt*Cm-(1-theta)*Km)*Tn+get(F,'vec'));
    tempn = scatter_sysvec(tempn1,Tn1);
    t=t+dt;
end
figure
plot((1:size(minmaxT, 1))*dt,minmaxT,'linewidth', 3)
set(gca,'FontSize', 14); grid on
xlabel('Time in seconds')
ylabel('Temperature in degrees Celsius')
