kappa=[35.0]; % conductivity matrix
cm = 440.5;% specific heat per unit mass
rho=7200;% mass density
cv =cm* rho;% specific heat per unit volume
Q=0; % uniform heat source
q0 =-2000;
Tampl=4;
Tamb=0;
num_integ_pts=2; % quadrature
Fluxdt=32;
L=0.25;% thickness
dt= 4; % time step
tend=  20*32; % length of the time interval
t=0;
theta =1; % generalized trapezoidal method
online_graphics= true;% plot the solution as it is computed?
n=7*5;% 
[fens,gcells] = block1d(L,n,1.0); % Mesh
prop=property_diffusion(struct('conductivity',kappa,...
    'specific_heat',cv,'rho',rho,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct (...
    'mater',mater,...
    'gcells',gcells,...
    'integration_rule',gauss_rule(1,num_integ_pts)));
efeb = feblock_diffusion (struct (...
    'mater',mater,...
    'gcells',gcell_P1(struct('conn',1)),...
    'integration_rule',point_rule));
geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
tempn = field(struct ('name',['temp'], 'dim', 1,...
    'nfens',get(geom,'nfens')));
% tempn = set_ebc(tempn, 1, 1, 1, Tbar(t));
tempn = set_ebc(tempn, n+1, 1, 1, Tamb);
tempn = apply_ebc (tempn);
tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Tamb);
K = start (dense_sysmat, get(tempn, 'neqns'));
K = assemble (K, conductivity(feb, geom, tempn));
Km = get(K,'mat');
C = start (dense_sysmat, get(tempn, 'neqns'));
C = assemble (C, capacity(feb, geom, tempn));
Cm = get(C,'mat');
Tface = [];
while t<tend+0.1*dt % Time stepping
    if online_graphics
        plot([0, 0],[-Tampl,Tampl],'.'); hold on
        plot(get(geom,'values'),get(tempn,'values'));
        figure(gcf);
        title (['Time =' num2str(t)]); hold off; pause(0.1);
    end
    tempn1 = tempn;
    tempn1 = set_ebc(tempn1, n+1, 1, 1, Tamb);
    tempn1 = apply_ebc (tempn1);
    F = start (sysvec, get(tempn, 'neqns'));
    fi= force_intensity(struct('magn',@(x)(round(round(t/Fluxdt)/2)*2==round(t/Fluxdt))*q0));
    F = assemble (F, flux_loads(efeb, geom, tempn, fi));
    Tn=gather_sysvec(tempn);
    Tface = [Tface Tn(1)];
    Tn1 = (1/dt*Cm+theta*Km) \ ((1/dt*Cm-(1-theta)*Km)*Tn+...
        get(F,'vec'));
    tempn = scatter_sysvec(tempn1,Tn1);
    t=t+dt;
end
plot((1:length(Tface))*dt,Tface,'linewidth', 3)
set(gca,'FontSize', 14); grid on
xlabel('Time in seconds')
ylabel('Temperature in degrees Celsius')