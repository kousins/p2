kappa=0.1*eye(2); % conductivity matrix
Q=0; % uniform heat source
a=1; b= 1.5;
qa =-10/a;
qb =6/b;
fens = [fenode(struct('id',1,'xyz',[0,b]));...
    fenode(struct('id',2,'xyz',[0,0]));...
    fenode(struct('id',3,'xyz',[a,0]));...
    fenode(struct('id',4,'xyz',[a,b]));
    ];
gcells = [gcell_T3(struct('id',1,'conn',[1,2,3],'other_dimension', 1)),
    gcell_T3(struct('id',2,'conn',[3,4,1],'other_dimension', 1))];
aedge_gcells = [gcell_L2(struct('id',3,'conn',[1,4],'other_dimension', 1))];
bedge_gcells = [gcell_L2(struct('id',4,'conn',[3,4],'other_dimension', 1))];
prop=property_diffusion(struct('conductivity',kappa,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',tri_rule(1)));
aedgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',aedge_gcells,...
    'integration_rule',gauss_rule(1,2)));
bedgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bedge_gcells,...
    'integration_rule',gauss_rule(1,2)));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
theta = set_ebc(theta, 4, 1, 1, 0.0);
theta = apply_ebc (theta);
theta = numbereqns (theta);
K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
F = assemble (F, flux_loads(aedgefeb, geom, theta, force_intensity(struct('magn',qa))));
F = assemble (F, flux_loads(bedgefeb, geom, theta, force_intensity(struct('magn',qb))));
theta = scatter_sysvec(theta, get(K,'mat')\get(F,'vec'));
% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',jet));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
geomT=field(struct ('name', ['geomT'], ...
    'data',[get(geom,'values'), get(theta,'values')/100]));
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geomT, 'u',0*geomT,...
        'colorfield',colorfield, 'shrink',0.9));
    draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
        'facecolor','none'));
end
draw_integration_points(feb, gv, struct ('x',geom,'u',0*geom, ...
    'theta', theta, 'scale', 0.13,'color','red'));
axis equal vis3d

get(theta,'values')