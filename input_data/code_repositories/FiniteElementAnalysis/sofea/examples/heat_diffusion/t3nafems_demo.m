kappa=[35.0]; % conductivity matrix
cm = 440.5;% specific heat per unit mass
rho=7200;% mass density
cv =cm* rho;% specific heat per unit volume
Q=0; % uniform heat source
Tampl=100;
Tamb=0;
Tbar =@(t)(Tampl*sin(pi*t/40)+ Tamb);%hot face temperature
num_integ_pts=2; % quadrature
L=0.1;% thickness
dt=2; % time step
tend= 8*32; % length of the time interval
t=0;
theta = 1; % generalized trapezoidal method
online_graphics=~ true;% plot the solution as it is computed?
n=2*5;% needs to be multiple of five
[fens,gcells] = block1d(L,n,1.0); % Mesh
prop=property_diffusion(struct('conductivity',kappa,...
    'specific_heat',cv,'rho',rho,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct (...
    'mater',mater,...
    'gcells',gcells,...
    'integration_rule',trapezoidal_rule));%gauss_rule(1,num_integ_pts)));
geom = field(struct ('name',['geom'], 'dim', 1, 'fens',fens));
tempn = field(struct ('name',['temp'], 'dim', 1,...
    'nfens',get(geom,'nfens')));
tempn = set_ebc(tempn, 1, 1, 1, Tbar(t));
tempn = set_ebc(tempn, n+1, 1, 1, Tamb);
tempn = apply_ebc (tempn);
tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Tamb);
K = start (dense_sysmat, get(tempn, 'neqns'));
K = assemble (K, conductivity(feb, geom, tempn));
Km = get(K,'mat');
C = start (dense_sysmat, get(tempn, 'neqns'));
C = assemble (C, capacity(feb, geom, tempn));
Cm = get(C,'mat');
Tfifth = [];
while t<tend+0.1*dt % Time stepping
    if online_graphics
        plot([0, 0],[-Tampl,Tampl],'.'); hold on
        plot(get(geom,'values'),get(tempn,'values'));
        figure(gcf);
        title (['Time =' num2str(t)]); hold off; pause(0.1);
    end
    tempn1 = tempn;
    tempn1 = set_ebc(tempn1, 1, 1, 1, Tbar(t+dt));
    tempn1 = set_ebc(tempn1, n+1, 1, 1, Tamb);
    tempn1 = apply_ebc (tempn1);
    F = start (sysvec, get(tempn, 'neqns'));
    F = assemble (F, nz_ebc_loads_conductivity(feb, geom, ...
        (theta*tempn1) + ((1-theta)*tempn)));
    F = assemble (F, nz_ebc_loads_capacity(feb, geom, ...
        (tempn1-tempn)*(1/dt)));
    Tn=gather_sysvec(tempn);
    Tfifth = [Tfifth Tn(n/5)];
    Tn1 = (1/dt*Cm+theta*Km) \ ((1/dt*Cm-(1-theta)*Km)*Tn+...
        get(F,'vec'));
    tempn = scatter_sysvec(tempn1,Tn1);
    t=t+dt;
end
plot((1:length(Tfifth))*dt,Tfifth,'linewidth', 3)
set(gca,'FontSize', 14); grid on
xlabel('Time in seconds')
ylabel('Temperature in degrees Celsius')