kappa=[16.3 0; 0 16.3]; % conductivity matrix, W/(m.K), 347 annealed stainless steel
Tin = 80;% degree Celsius
Tout = 20;% degree Celsius
Text = 50;% degree Celsius
h= 100;%  surface heat transfer coefficient, W/(m^2.K)
L = .4;% meters
R1 = 0.08;
R1 = 0.08;
R2a = 0.06;
R2b = 0.04;
R3a = 0.04;
R3b = 0.02;
t1 = 0.01;
t2 = 0.02;
[fens,gcells,groups,edge_gcells,edge_groups]=targe2_mesher_vl(...
    [0,0;R1,0;R1,t1;R2a,t1;R3a,t1+t2;R3b,L-t1-t2;R2b,L-t1;R1,L-t1;R1,L;0,L], 1.0,...
    struct('axisymm',true,'mesh_size',t1/2));
% mesh{1}=fens;
%     mesh{2}=gcells;
%     drawmesh(mesh,'gcells'); view (2);
prop=property_diffusion (struct('conductivity',kappa,'source',0));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',tri_rule(1)));
edgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',edge_gcells([edge_groups{:}]),...
    'integration_rule',gauss_rule(1,2),...
    'surface_transfer', h));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
for e= [1]
    eg=edge_gcells([edge_groups{e}]);
    for i= 1:length(eg)
        conn = get(eg(i),'conn');
        theta = set_ebc(theta, conn, conn*0+1, [], conn*0+Tin);
    end
end
for e= [9]
    eg=edge_gcells([edge_groups{e}]);
    for i= 1:length(eg)
        conn = get(eg(i),'conn');
        theta = set_ebc(theta, conn, conn*0+1, [], conn*0+Tout);
    end
end
theta = apply_ebc (theta);
theta = numbereqns (theta);
amb = clone(theta, ['amb']);
nl = connected_nodes(get (edgefeb,'gcells'));
amb = set_ebc(amb, nl, nl*0+1, [], nl*0+Text);
amb = apply_ebc (amb);
 K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
K = assemble (K, surface_transfer(edgefeb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
F = assemble (F,  nz_ebc_loads_conductivity(feb, geom, theta));
F = assemble (F, surface_transfer_loads(edgefeb, geom, theta, amb));
theta = scatter_sysvec(theta, K\F);

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
camset(gv,[-1.2474
    0.9447
  727.0379
    0.0430
    0.2441
   33.7645
    0.6705
   -0.3641
  258.5634
    7.5587]);
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',jet));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
geomT=field(struct ('name', ['geomT'], ...
    'data',[get(geom,'values'), get(theta,'values')]));
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geomT, 'u',0*geomT,...
        'colorfield',colorfield, 'shrink',1));
    draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
        'facecolor','none'));
end
draw_integration_points(feb, gv, struct ('x',geom,'u',0*geom, ...
    'theta', theta, 'scale', 0.000002,'color','red'));
camlight('left','infinite')
set(gca,'XLim', [0,R1])
set(gca,'YLim', [0,L])
set(gca,'ZLim', [0,max(T)])
zlabel('Temperature')
set(gca,'DataAspectRatio', [1, 1, 2*(max(T)/L)])
grid on

