kappa_steel=18*1e-3*eye(3); % W/K/mm
rho_steel = 7500*1e-9;% kg/mm^3
cv_steel =470*rho_steel;% J/kg/K
Length = 200; Thickness = 10; % millimeter
h=50*1e-6;% W/mm^2/K
q=-1e4/1e2;
Ta=17;
dt=0.75; % time step
Tcycle= 20;
tend= 4*Tcycle; % length of the time interval
t=0;
theta = 1.0; % generalized trapezoidal method
online_graphics= true;% plot the solution as it is computed?
Bell=@(x1,x2) (Length/5/(Length/5 +norm( x1-x2 )^2));

% [fens,gcells]=block(Length, Length, Thickness, 40, 40, 2);
[fens,gcells]=block(Length, Length, Thickness, 15, 15, 1);

prop_steel=...
    property_diffusion (struct('conductivity',kappa_steel,...
    'specific_heat',cv_steel,'source',0.0));
mater_steel=mater_diffusion (struct('property',prop_steel));
feb_steel = feblock_diffusion (struct ('mater',mater_steel,...
    'gcells',gcells,...
    'integration_rule',gauss_rule(3,2)));

bgcells=mesh_bdry(gcells);
fl =gcell_select(fens, bgcells, ...
    struct ('box',[0, Length, 0, Length, Thickness, Thickness],'inflate', 0.01*Thickness));
sfeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bgcells,...
    'integration_rule',gauss_rule(2,1),...
    'surface_transfer', h));
ffeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bgcells(fl),...
    'integration_rule',gauss_rule(2,3)));

geom = field(struct('name',['geom'], 'dim', 3, 'fens',fens));
tempn=field(struct('name',['tempn'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
amb = clone(tempn, ['amb']);
for i= 1:length(bgcells)
    conn = get(bgcells(i),'conn');
    amb = set_ebc(amb, conn, conn*0+1, [], conn*0+Ta);
end
amb = apply_ebc (amb);

tempn = numbereqns (tempn);
tempn = scatter_sysvec(tempn,gather_sysvec(tempn)*0+Ta);

K = start (sparse_sysmat, get(tempn, 'neqns'));
K = assemble (K, conductivity(feb_steel, geom, tempn));
K = assemble (K, surface_transfer(sfeb, geom, tempn));
Km = get(K,'mat');
C = start (sparse_sysmat, get(tempn, 'neqns'));
C = assemble (C, capacity(feb_steel, geom, tempn));
Cm = get(C,'mat');

if online_graphics
    gv=graphic_viewer;
    dcm=data_colormap(struct('range',[Ta,400*Ta],'colormap',jet));
end
minmaxT = []
while t<tend+0.1*dt % Time stepping
    if online_graphics
        set(gca,'FontSize', 14)
        T=get(tempn,'values');
        colorfield=field(struct('name',['colorfield'],'data',...
            map_data(dcm, T)));
        gv=reset (gv,[]);
%         camset (gv,[-671.7042 -895.3086 695.7305 58.8095 ...
%             56.7155 2.9101 0 0 1 7.3342])
        for i=1:length (gcells)
            draw(gcells(i),gv,struct('x',geom, 'u',0*geom,...
                'colorfield',colorfield, 'shrink',1.0));
            draw(gcells(i),gv,struct('x',geom, 'u',0*geom, ...
                'facecolor','none'));
        end
%         camset (gv,[-671.7042 -895.3086 695.7305 58.8095 ...
%             56.7155 2.9101 0 0 1 7.3342])
        figure(gcf);
        title (['Time =' num2str(t)]); hold off; pause(0.1);
%                 saveas(gcf, ['shrinkfit-' num2str(t) '.png'], 'png');
    end
    tempn1 = tempn;
    F = start (sysvec, get(tempn, 'neqns'));
    F=assemble(F, surface_transfer_loads(sfeb,geom,tempn,amb));
    magn=@(x) (q*Bell(x, [Length/2*[cos(2*pi*t/Tcycle),sin(2*pi*t/Tcycle)],Thickness]));
    fi= force_intensity(struct('magn',magn));
    F = assemble (F, flux_loads(ffeb, geom, tempn, fi));
    Tn=gather_sysvec(tempn);
    minmaxT= [minmaxT; min(Tn),max(Tn)]
    Tn1 = (1/dt*Cm+theta*Km) \ ((1/dt*Cm-(1-theta)*Km)*Tn+...
        get(F,'vec'));
    tempn = scatter_sysvec(tempn1,Tn1);
    t=t+dt;
end
figure
plot((1:size(minmaxT, 1))*dt,minmaxT,'linewidth', 3)
set(gca,'FontSize', 14); grid on
xlabel('Time in seconds')
ylabel('Temperature in degrees Celsius')
