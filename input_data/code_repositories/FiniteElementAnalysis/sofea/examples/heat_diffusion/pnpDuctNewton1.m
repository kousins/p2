kappa=0.5*eye(2); % conductivity matrix
hi=5;
ho=15;
Q=0; % uniform heat source
a=1;
Ti = 1000;
To = -20;
num_integ_pts=1; % 1-point quadrature
fens = [fenode(struct('id',1,'xyz',[0,0]));...
    fenode(struct('id',2,'xyz',[2*a,0]));...
    fenode(struct('id',3,'xyz',[a,a]));...
    fenode(struct('id',4,'xyz',[0,a]));
    ];
gcells = [gcell_T3(struct('id',1,'conn',[1,2,3],'other_dimension', 1)),
    gcell_T3(struct('id',1,'conn',[1,3,4],'other_dimension', 1))];
iedge_gcells = [gcell_L2(struct('id',1,'conn',[3,4],'other_dimension', 1))];
oedge_gcells = [gcell_L2(struct('id',1,'conn',[1,2],'other_dimension', 1))];
prop=property_diffusion(struct('conductivity',kappa,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',tri_rule(num_integ_pts)));
iedgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',iedge_gcells,...
    'integration_rule',gauss_rule(1,2),...
    'surface_transfer', hi));
oedgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',oedge_gcells,...
    'integration_rule',gauss_rule(1,2),...
    'surface_transfer', ho));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
fenids=1:length(fens);
theta = numbereqns (theta);
iamb = clone(theta, ['iamb']);
prescribed=ones(length(fenids),1);
comp=[];
val=zeros(length(fenids),1)+Ti;
iamb = set_ebc(iamb, fenids, prescribed, comp, val);
iamb = apply_ebc (iamb);
oamb = clone(theta, ['oamb']);
prescribed=ones(length(fenids),1);
comp=[];
val=zeros(length(fenids),1)+To;
oamb = set_ebc(oamb, fenids, prescribed, comp, val);
oamb = apply_ebc (oamb);
K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
K = assemble (K, surface_transfer(iedgefeb, geom, theta));
K = assemble (K, surface_transfer(oedgefeb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
F = assemble (F, source_loads(feb, geom, theta));
F = assemble (F, surface_transfer_loads(iedgefeb, geom, theta, iamb));
F = assemble (F, surface_transfer_loads(oedgefeb, geom, theta, oamb));
theta = scatter_sysvec(theta, get(K,'mat')\get(F,'vec'));
% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',hot));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
geomT=field(struct ('name', ['geomT'], ...
    'data',[get(geom,'values'), get(theta,'values')/100]));
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geomT, 'u',0*geomT,...
        'colorfield',colorfield, 'shrink',0.9));
    draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
        'facecolor','none'));
end
axis equal vis3d
get(theta,'values')