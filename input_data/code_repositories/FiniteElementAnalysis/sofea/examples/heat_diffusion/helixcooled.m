kappa=0.2*eye(3, 3); % conductivity matrix
Q=0.0; % no heat source
h=0.0002;
num_integ_pts=1; % 1-point quadrature
Width=1.45; 
Height = 0.4;
Angle =2.5*pi;
[fens,gcells] = t4block(Angle,Width,Height, 50, 6, 4);
Radius = 1.2;
fens = transform_apply(fens,@(x, data) (x.*[1,(1-x(1)/Angle/1.2), 1]), []);
fens = transform_apply(fens,@(x, data) (x+ [0, Radius, 0]), []);
climbPerRevolution= 1.3;
fens = transform_2_helix(fens,climbPerRevolution);
bgcells=mesh_bdry(gcells);
drawmesh({fens,bgcells},'gcells','facecolor','red')
prop=property_diffusion(struct('conductivity',kappa,'source',Q)); mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',tet_rule(num_integ_pts)));
bfeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bgcells,...
    'integration_rule',tri_rule(num_integ_pts),...
    'surface_transfer', h));
geom = field(struct('name',['geom'], 'dim', 3, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
amb = clone(theta, ['amb']);
fenids=(1:length(fens)); % prescribe ambient temperature
prescribed=fenids*0+1;
comp=[];
val=zeros(length(fenids),1)+15;
amb = set_ebc(amb, fenids, prescribed, comp, val);
amb = apply_ebc (amb);
fenids=fenode_select(fens,struct('box',[0 Radius+Width 0 0 -2 2],...
    'inflate', 0.01)); % prescribed temperature on one face
prescribed=fenids*0+1;
comp=[];
val=zeros(length(fenids),1)+55;
amb = set_ebc(amb, fenids, prescribed, comp, val);
amb = apply_ebc (amb);

theta = numbereqns (theta);
K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
K = assemble (K, surface_transfer(bfeb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
F = assemble (F, source_loads(feb, geom, theta));
F = assemble (F, surface_transfer_loads(bfeb, geom, theta, amb));
theta = scatter_sysvec(theta, get(K,'mat')\get(F,'vec'));

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',hot));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geom, 'u',0*geom,...
        'colorfield',colorfield, 'shrink',0.9));
end
