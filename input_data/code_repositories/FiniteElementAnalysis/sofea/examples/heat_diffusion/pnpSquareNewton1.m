kappa=[1.8 0; 0 1.8]; % conductivity matrix
h=2*1.8;
Q=15; % uniform heat source
a=1;
num_integ_pts=1; % 1-point quadrature
fens = [fenode(struct('id',1,'xyz',[-2*a,a]));...
    fenode(struct('id',2,'xyz',[-a,a]));...
    fenode(struct('id',3,'xyz',[-2*a,2*a]));...
    fenode(struct('id',4,'xyz',[-2*a,0]));...
    fenode(struct('id',5,'xyz',[-a,0]));...
    fenode(struct('id',6,'xyz',[0,0]));...
    ];
gcells = [gcell_T3(struct('id',1,'conn',[4,5,1],'other_dimension', 1)),
    gcell_T3(struct('id',1,'conn',[5,6,2],'other_dimension', 1)),
    gcell_T3(struct('id',1,'conn',[1,2,3],'other_dimension', 1)),
    gcell_T3(struct('id',1,'conn',[2,1,5],'other_dimension', 1))];
edge_gcells = [gcell_L2(struct('id',1,'conn',[4,5],'other_dimension', 1)),
    gcell_L2(struct('id',1,'conn',[5,6],'other_dimension', 1))];
prop=property_diffusion(struct('conductivity',kappa,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',tri_rule(num_integ_pts)));
edgefeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',edge_gcells,...
    'integration_rule',gauss_rule(1,2),...
    'surface_transfer', h));
geom = field(struct('name',['geom'], 'dim', 2, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
fenids=[fenode_select(fens,struct('box',[-2 0 0 0],...
    'inflate', 0.01))];
theta = numbereqns (theta);
amb = clone(theta, ['amb']);
prescribed=ones(length(fenids),1);
comp=[];
val=zeros(length(fenids),1)+20;
amb = set_ebc(amb, fenids, prescribed, comp, val);
amb = apply_ebc (amb);
K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
K = assemble (K, surface_transfer(edgefeb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
F = assemble (F, source_loads(feb, geom, theta));
F = assemble (F, surface_transfer_loads(edgefeb, geom, theta, amb));
theta = scatter_sysvec(theta, get(K,'mat')\get(F,'vec'));
% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',hot));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
geomT=field(struct ('name', ['geomT'], ...
    'data',[get(geom,'values'), get(theta,'values')]));
for i=1:length (gcells)
    draw(gcells(i), gv, struct ('x',geomT, 'u',0*geomT,...
        'colorfield',colorfield, 'shrink',0.9));
    draw(gcells(i), gv, struct ('x',geom, 'u',0*geom, ...
        'facecolor','none'));
end
axis equal vis3d
(get(theta,'values')-20)/(Q*a^2/6/kappa(1,1))