kappa=0.2*eye(3); % conductivity matrix
Q=0.; % no heat source
num_integ_pts=2; 
magn = -100;
h=0.1;
t= 5;
[fens,gcells]=block(200, 200, t, 10, 10, 4);
prop=property_diffusion(struct('conductivity',kappa,'source',Q));
mater=mater_diffusion (struct('property',prop));
feb = feblock_diffusion (struct ('mater',mater,...
    'gcells',gcells,...
    'integration_rule',gauss_rule(3,num_integ_pts)));
bgcells=mesh_bdry(gcells);
fl =gcell_select(fens, bgcells, ...
    struct ('box',[0, 200, 0, 200, t,t],'inflate', 0.01));
fluxfeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bgcells(fl),...
    'integration_rule',gauss_rule(2,num_integ_pts)));
coolfeb = feblock_diffusion (struct ('mater',mater,...
    'gcells',bgcells(setdiff((1:length(bgcells)),fl)),...
    'integration_rule',gauss_rule(2,num_integ_pts),...
    'surface_transfer', h));
geom = field(struct('name',['geom'], 'dim', 3, 'fens',fens));
theta=field(struct('name',['theta'], 'dim', 1, 'nfens',...
    get(geom,'nfens')));
theta = numbereqns (theta);
amb = clone(theta, ['amb']);
fenids=(1:length(fens)); % prescribe ambient temperature
prescribed=fenids*0+1;
comp=[];
val=zeros(length(fenids),1)+3.0;
amb = set_ebc(amb, fenids, prescribed, comp, val);
amb = apply_ebc (amb);
K = start (sparse_sysmat, get(theta, 'neqns'));
K = assemble (K, conductivity(feb, geom, theta));
K = assemble (K, surface_transfer(coolfeb, geom, theta));
F = start (sysvec, get(theta, 'neqns'));
fi= force_intensity(struct('magn',magn));
F = assemble (F, flux_loads(fluxfeb, geom, theta, fi));
F = assemble (F, surface_transfer_loads(coolfeb, geom, theta, amb));
theta = scatter_sysvec(theta, get(K,'mat')\get(F,'vec'));

% Plot
gv=graphic_viewer;
gv=reset (gv,[]);
% camset(gv,[ 154.6458  -70.7169  173.1645   24.5670   18.6838    3.9035   -0.6027   0.4142 0.6820   10.3396])
T=get(theta,'values');
dcm=data_colormap(struct('range',[min(T),max(T)],'colormap',jet));
colorfield=field(struct ('name', ['colorfield'], 'data',...
    map_data(dcm, T)));
for i=1:length (bgcells)
    draw(bgcells(i), gv, struct ('x',geom, 'u',0*geom,...
        'colorfield',colorfield, 'shrink',1.0));
end
