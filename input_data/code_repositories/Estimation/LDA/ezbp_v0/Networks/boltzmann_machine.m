function net = boltzmann_machine(bb,ww)

% net = boltzmann_machine(bb,ww)
%
% Makes a Boltzmann machine with biases bb and weights ww.
%
% net.numnode     the number of variables in the graphical model.
% net.vars        is a shape where indices correspond to variables, sizes 
%                 are the sizes of the domains of the variables.
% net.phi1{nn,mm} is single-site potential at location nn,mm.
% net.phi2{nn,mm} is pairwise potential on edge nn-mm.
% net.graph       is adjacency matrix of undirected graph.
% net.edge        is list of edges in undirected graph.


numnode = size(ww,1);
vars  = shape(1:numnode,2*ones(1,numnode));
phi1  = cell(numnode,1);
phi2  = cell(numnode,numnode);
graph = double(ww ~= 0);
graph = graph - diag(diag(graph));
edge  = zeros(0,2);

% generate potentials
% singleton 
for nn = 1:numnode
  phi1{nn} = table(exp([0 bb(nn)]),nn);
end

kk = 0;
for ii = 1:numnode, 
  for jj = ii+1:numnode,
    if graph(ii,jj) 
      kk = kk + 1;
      phi2{ii,jj}   = table(exp([0 0; 0 ww(ii,jj)]),[ii jj]);
      phi2{jj,ii}   = phi2{ii,jj};
      edge(kk,:)      = [ii,jj];
    end
  end
end

net.vars    = vars;
net.numnode = numnode;
net.bb      = bb;
net.ww      = ww;
net.phi1    = phi1;
net.phi2    = phi2;
net.graph   = graph;
net.edge    = edge;

