function net = gen_randombipartite(dd,nn,mm,pp,genpot1a,genpot1b,genpot2)

% net = gen_randombipartite(dd,nn,mm,pp,genpot1a,genpot1b,genpot2)
%
% make random bipartite graph where edges appear with probability pp.
% nodes are indexed 1...n on one side, n+1...n+m on other.
% single-site potentials for undirected graphical model are generated
% using genpot1a on one side, genpot1b on other, and pairwise potentials
% generated using genpot2.
% 
% net.nn net.mm   are number of nodes on either side.
% net.numnode     the number of variables in the graphical model.
% net.vars        is a shape where indices correspond to variables, sizes 
%                 are the sizes of the domains of the variables.
% net.phi1{nn}    is single-site potential at node nn.
% net.phi2{nn,mm} is pairwise potential on edge nn-mm.
% net.graph       is adjacency matrix of undirected graph.
% net.edge        is list of edges in undirected graph.
%
% Example: The following generates a (10,20) complete bipartite graph, 
% with binary variables, and where weights are Gaussian distributed, 
% single-site ones having std .1, pairwise ones .5:
%
%       dd = 2; nn = 10; mm = 20; pp = 1;
%       genpot1 = @() exp(randn(dd,1)*.1);
%       genpot2 = @() exp(randn(dd,dd)*.5);
%       gen_randombipartite(dd,nn,mm,pp,genpot1,genpot1,genpot2);


numnode  = nn + mm;
vars  = shape(1:numnode,dd*ones(1,numnode));
phi1  = cell(numnode,1);
phi2  = cell(numnode,numnode);
graph = zeros(numnode,numnode);
edge  = zeros(0,2);

% generate single-site potentials
for ii = 1:nn
  phi1{ii} = table(genpot1a(),ii);
end
for jj = 1:mm
  phi1{nn+jj} = table(genpot1b(),nn+jj);
end

kk = 0;
for nm2 = nn+1:nn+mm, 
  for nm1 = 1:nn,
    if rand < pp
      kk              = kk + 1;
      phi2{nm1,nm2}   = table(genpot2(),[nm1 nm2]);
      phi2{nm2,nm1}   = phi2{nm1,nm2};
      graph(nm1,nm2)  = 1;
      graph(nm2,nm1)  = 1;
      edge(kk,:)      = [nm1 nm2];
    end
  end
end

net.vars    = vars;
net.numnode = numnode;
net.nn      = nn;
net.mm      = mm;
net.phi1    = phi1;
net.phi2    = phi2;
net.graph   = graph;
net.edge    = edge;

