function net = gen_bipartite(dd,nn,mm,genpot1a,genpot1b,genpot2)

% net = gen_bipartite(dd,nn,mm,pp,genpot1a,genpot1b,genpot2)
%
% make complete bipartite graph.
% nodes are indexed 1...n on one side, n+1...n+m on other.
% single-site potentials for undirected graphical model are generated
% using genpot1a on one side, genpot1b on other, and pairwise potentials
% generated using genpot2.
% 
% net.nn net.mm   are number of nodes on either side.
% net.numnode     the number of variables in the graphical model.
% net.vars        is a shape where indices correspond to variables, sizes 
%                 are the sizes of the domains of the variables.
% net.phi1{nn}    is single-site potential at node nn.
% net.phi2{nn,mm} is pairwise potential on edge nn-mm.
% net.graph       is adjacency matrix of undirected graph.
% net.edge        is list of edges in undirected graph.
%
% Example: The following generates a (10,20) complete bipartite graph, 
% with binary variables, and where weights are Gaussian distributed, 
% single-site ones having std .1, pairwise ones .5:
%
%       dd = 2; nn = 10; mm = 20; 
%       genpot1 = @() exp(randn(dd,1)*.1);
%       genpot2 = @() exp(randn(dd,dd)*.5);
%       gen_bipartite(dd,nn,mm,genpot1,genpot1,genpot2);

net = gen_randombipartite(dd,nn,mm,1,genpot1a,genpot1b,genpot2);
