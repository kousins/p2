function net = grid_make(dd,nn,mm,genpot1,genpot2);

% net = grid_make(dd,nn,mm,genpot1,genpot2)
%
% make undirected graphical model with each variable taking on dd values, 
% the graph being a grid of size nn x mm.  each call to genpot1 returns a 
% length dd vector of single-site potentials, and each call to genpot2 
% returns a dd x dd matrix of pairwise potentials.
%
% net.nn net.mm   are number of rows, columns
% net.numnode     the number of variables in the graphical model.
% net.vars        is a shape where indices correspond to variables, sizes 
%                 are the sizes of the domains of the variables.
% net.ii(nn,mm)   is index of node at location nn,mm.
% net.phi1{nn,mm} is single-site potential at location nn,mm.
% net.phih{nn,mm} is potential on horizontal edge (nn,mm)-(nn,mm+1).
% net.phiv{nn,mm} is potential on vertical edge (nn,mm)-(nn+1,mm).
% net.phi2{i1,i2} is potential on edge i1-i2.
% net.graph       is adjacency matrix of undirected graph.
% net.edge        is list of edges in undirected graph.
%
% Example: The following generates a binary 10x20 grid, where potentials
% are Gaussian distributed, single-site ones having std .1, pairwise ones .5:
%
%       dd = 2; nn = 10; mm = 20;
%       genpot1 = @() exp(randn(dd,1)*.1);
%       genpot2 = @() exp(randn(dd,dd)*.5);
%       gen_grid(dd,nn,mm,genpot1,genpot2);

numnode  = nn * mm;
vars  = shape(1:numnode,dd*ones(1,numnode));
nm    = reshape(1:numnode,nn,mm);
phi1  = cell(nn,mm);
phih  = cell(nn,mm-1);
phiv  = cell(nn-1,mm);
phi2  = cell(numnode,numnode);
graph = zeros(numnode,numnode);
edge  = zeros(0,2);

% generate potentials
kk = 0;
for m1 = 1:mm, 
  for n1 = 1:nn,
    nm1              = nm(n1,m1);

    % singleton 
    phi1{n1,m1}      = table(genpot1(),nm1);

    % vertical edges
    if n1 < nn 
      kk  = kk + 1;
      nm2 = nm(n1+1,m1);
      phiv{n1,m1}    = table(genpot2(),[nm1 nm2]);
      phi2{nm1,nm2}  = phiv{n1,m1};
      phi2{nm2,nm1}  = phiv{n1,m1};
      graph(nm1,nm2) = 1;
      graph(nm2,nm1) = 1;
      edge(kk,:)     = [nm1 nm2];
    end

    % horizontal edges
    if m1 < mm 
      kk             = kk + 1;
      nm2            = nm(n1,m1+1);
      phih{n1,m1}    = table(genpot2(),[nm1 nm2]);
      phi2{nm1,nm2}  = phih{n1,m1};
      phi2{nm2,nm1}  = phih{n1,m1};
      graph(nm1,nm2) = 1;
      graph(nm2,nm1) = 1;
      edge(kk,:)     = [nm1 nm2];
    end
  end
end

net.vars    = vars;
net.numnode = numnode;
net.nn      = nn;
net.mm      = mm;
net.ii      = nm;
net.phi1    = phi1;
net.phih    = phih;
net.phiv    = phiv;
net.graph   = graph;
net.edge    = edge;

