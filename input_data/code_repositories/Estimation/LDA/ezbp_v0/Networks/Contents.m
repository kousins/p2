
% Constructions for various pairwise Markov networks.
% 
% Each network is a structure with at least the follow fields:
% numnode       The number of nodes.
% vars          A domain object with the indices of nodes.
% graph         The graphical structure of the network, as an adjacency matrix.
% edge          The list of edges in the network.
% phi1, phi2    The table objects of potentials on nodes and edges.
%
% Other possible fields are listed in the relevant network making utilities.
% Generally they are:
% nn,mm,ii      Network-specific node ordering, mapping to general ordering.
% phi*          table potentials ordered in network-specific ways.
%
% The networks available are:
% grid              2D square grid (the grid itself can be rectangle)
% bipartite         Complete bipartite network.
% complete          Completely connected network.
% random            Randomly connected network.
% randombipartite   Random bipartite network.
% boltzmann_machine A Boltzmann machine.

