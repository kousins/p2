
mex table_op_1arg.c          -Delemop_name=table_zeros      -output ../@table/zeros      table.o index.o shape.o iterator.o -lm
mex table_op_1arg.c          -Delemop_name=table_ones       -output ../@table/ones       table.o index.o shape.o iterator.o -lm
mex table_op_1arg.c          -Delemop_name=table_uminus     -output ../@table/uminus     table.o index.o shape.o iterator.o -lm
mex table_op_1arg.c          -Delemop_name=table_sumnorm    -output ../@table/sumnorm    table.o index.o shape.o iterator.o -lm
mex table_op_1arg.c          -Delemop_name=table_maxnorm    -output ../@table/maxnorm    table.o index.o shape.o iterator.o -lm
mex table_op_1arg.c          -Delemop_name=table_maxabsnorm -output ../@table/maxabsnorm table.o index.o shape.o iterator.o -lm

mex table_op_2arg.c          -Delemop_name=table_eq         -output ../@table/eq         table.o index.o shape.o iterator.o -lm
mex table_op_2arg.c          -Delemop_name=table_ne         -output ../@table/ne         table.o index.o shape.o iterator.o -lm
mex table_op_2arg.c          -Delemop_name=table_lt         -output ../@table/lt         table.o index.o shape.o iterator.o -lm
mex table_op_2arg.c          -Delemop_name=table_gt         -output ../@table/gt         table.o index.o shape.o iterator.o -lm
mex table_op_2arg.c          -Delemop_name=table_le         -output ../@table/le         table.o index.o shape.o iterator.o -lm
mex table_op_2arg.c          -Delemop_name=table_ge         -output ../@table/ge         table.o index.o shape.o iterator.o -lm

mex table_op_recursive2arg.c -Delemop_name=table_plus       -output ../@table/plus       table.o index.o shape.o iterator.o -lm
mex table_op_recursive2arg.c -Delemop_name=table_minus      -output ../@table/minus      table.o index.o shape.o iterator.o -lm
mex table_op_recursive2arg.c -Delemop_name=table_max        -output ../@table/max        table.o index.o shape.o iterator.o -lm
mex table_op_recursive2arg.c -Delemop_name=table_min        -output ../@table/min        table.o index.o shape.o iterator.o -lm

mex table_op_recursive2arg.c -Delemop_name=table_times      -output ../@table/times      table.o index.o shape.o iterator.o -lm
mex table_op_recursive2arg.c -Delemop_name=table_ldivide    -output ../@table/ldivide    table.o index.o shape.o iterator.o -lm
mex table_op_recursive2arg.c -Delemop_name=table_rdivide    -output ../@table/rdivide    table.o index.o shape.o iterator.o -lm

mex table_op_recursive2arg.c -Delemop_name=table_power      -output ../@table/power      table.o index.o shape.o iterator.o -lm

mex table_op_marg.c          -Delemop_name=table_sum        -output ../@table/sum        table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_maxmarg    -output ../@table/maxmarg    table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_minmarg    -output ../@table/minmarg    table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_prod       -output ../@table/prod       table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_sumto      -output ../@table/sumto      table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_maxmargto  -output ../@table/maxmargto  table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_minmargto  -output ../@table/minmargto  table.o index.o shape.o iterator.o -lm
mex table_op_marg.c          -Delemop_name=table_prodto     -output ../@table/prodto     table.o index.o shape.o iterator.o -lm

mex table_op_tensor.c        -Delemop_name=table_maxsum     -output ../@table/maxsum     table.o index.o shape.o iterator.o -lm
mex table_op_tensor.c        -Delemop_name=table_minsum     -output ../@table/minsum     table.o index.o shape.o iterator.o -lm
mex table_op_tensor.c        -Delemop_name=table_sumprod    -output ../@table/sumprod    table.o index.o shape.o iterator.o -lm
mex table_op_tensor.c        -Delemop_name=table_maxprod    -output ../@table/maxprod    table.o index.o shape.o iterator.o -lm

mex table_op_recursive2arg.c -Delemop_name=table_mtimes    -output ../@table/mtimes      table.o index.o shape.o iterator.o -lm

mex table_permute.c                                         -output ../@table/permute    table.o index.o shape.o iterator.o -lm

