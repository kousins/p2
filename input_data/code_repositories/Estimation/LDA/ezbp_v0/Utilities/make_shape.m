
mex shape_op_num2arg.c    -Delemop_name=shape_consistent      -output ../@shape/consistent shape.o index.o -lm
mex shape_op_num2arg.c    -Delemop_name=shape_subshape        -output ../@shape/subshape   shape.o index.o -lm

mex shape_op_1arg.c       -Delemop_name=shape_trim            -output ../@shape/trim       shape.o index.o -lm

mex shape_op_2arg.c       -Delemop_name=shape_union           -output ../@shape/union      shape.o index.o -lm
mex shape_op_2arg.c       -Delemop_name=shape_xor             -output ../@shape/xor        shape.o index.o -lm
mex shape_op_index2arg.c -Delemop_name=shape_indexintersect -output ../@shape/intersect  shape.o index.o -lm
mex shape_op_index2arg.c -Delemop_name=shape_indexdiff      -output ../@shape/diff       shape.o index.o -lm

mex shape_asgn.c                                              -output ../@shape/asgn       shape.o index.o -lm
mex shape_ref.c                                               -output ../@shape/ref        shape.o index.o -lm

mex shape_op_table.c      -Delemop_name=table_zeros           -output ../@shape/zeros      shape.o index.o table.o iterator.o -lm
mex shape_op_table.c      -Delemop_name=table_ones            -output ../@shape/ones       shape.o index.o table.o iterator.o -lm

