say Test the system by calculating log probability of observations in a 
say HMM using both brute force and forward-backward (actually just forward 
say suffices).
say

numstate = 3;
numnode  = 10;
transitionprobs = rand(numstate,numstate);
transitionprobs = transitionprobs ./ repmat(sum(transitionprobs,2),1,numstate);
initialprobs = rand(1,numstate);
initialprobs = initialprobs / sum(initialprobs);
observationprobs = rand(numnode,numstate); 

say Usual MATLAB code (without message normalization):
alphamessage = initialprobs.*observationprobs(1,:);
for ii = 2:numnode;
  alphamessage = (alphamessage*transitionprobs).*observationprobs(ii,:);
end
say '  log probability =' =log(sum(alphamessage)) 

% Construct tables.
initialtable = table(initialprobs,1,numstate);
transitiontable = cell(1,numnode);
observationtable = cell(1,numnode);
for ii = 1:numnode
  transitiontable{ii} = table(transitionprobs,[ii ii+1],[numstate numstate]);
  observationtable{ii} = table(observationprobs(ii,:),ii,numstate);
end

say Brute force:
bigtable = times(initialtable,transitiontable{1:numnode-1},observationtable{:});
say '  log probability =' =double(log(sum(bigtable,1:numnode)))

say Forward pass using tables:
alphatable = initialtable .* observationtable{1};
for ii = 1:numnode-1;
  alphatable = (alphatable*transitiontable{ii}).*observationtable{ii+1};
end
say '  log probability =' =double(log(sum(alphatable,numnode)))


