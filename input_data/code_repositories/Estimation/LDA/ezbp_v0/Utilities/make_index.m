
mex index_op_num1arg.c -Delemop_name=index_length     -output ../@index/length     index.o -lm
mex index_op_num1arg.c -Delemop_name=index_isempty    -output ../@index/isempty    index.o -lm

mex index_op_num2arg.c -Delemop_name=index_subindex   -output ../@index/subindex   index.o -lm

mex index_op_2arg.c    -Delemop_name=index_union      -output ../@index/union      index.o -lm
mex index_op_2arg.c    -Delemop_name=index_intersect  -output ../@index/intersect  index.o -lm
mex index_op_2arg.c    -Delemop_name=index_diff       -output ../@index/diff       index.o -lm
mex index_op_2arg.c    -Delemop_name=index_xor        -output ../@index/xor        index.o -lm



