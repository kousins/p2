say testing index constructors
peval d1 = index([1 3 5])
peval d2 = index([3 1 5])
peval d3 = index([1 1 5])
peval d4 = index([0 1 5])
peval d5 = index([-1 1 5])
peval d6 = index(shape([1 3 5],[2 3 4]))
peval d7 = index(ones(shape([1 3 5],[2 3 4])))

say testing index subscripts
peval d1.dd
peval d1([2 2 1 3])
peval d1([2 1])
peval d1([2 1])=[4 6];
peval d1.dd = [3 9];

say testing index properties and conversion
peval d1
peval length(d1)
peval double(d1)
peval isempty(d1)
peval isempty(index([]))
peval subindex(index([]),d1)
peval subindex(index(3),d1)
peval subindex(index([3 6 9]),d1)
peval subindex(d1,d1)

say testing index oeprators
peval d1
peval d2
peval intersect(d1,d2)
peval union(d1,d2)
peval diff(d1,d2)
peval xor(d1,d2)
