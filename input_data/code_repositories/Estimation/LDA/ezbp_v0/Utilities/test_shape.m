say testing shape constructors
peval s1 = shape(1:2:7,2:2:8)                            % { 1 3 5 7 | 2 4 6 8 }
peval s2 = shape(ones(2,4,6))                            % { 1 2 3 | 2 4 6 }
peval s3 = shape(table(ones(2,4,6),[1 3 4 5],[2 4 1 6])) % { 1 3 4 5 | 2 4 1 6 }
peval s4 = shape([],[]);                                 % { | }
peval s5 = shape([]);                                    % { 1 2 | 0 0 }
peval s6 = shape(ones(2,4));                             % { 1 2 | 2 4 }
peval ss = shape(0:2,1:3);                               % error
peval ss = shape(1:3,-1:1);                              % error
peval ss = shape(1:3,1:4);                               % error
peval ss = shape(1:.5:2,1:3);                            % error
peval ss = shape(1:3,1:.5:2);                            % error
say

say testing shape subscripts
peval s3.dd               % 1 3 4 5
peval s3.dd([1 3])        % 1 4
peval s3.ss(1:3)          % 2 4 1
peval s3([1 2 3])         % { 1 3 4 | 2 4 1 }
peval s3(index([1 2 3]))  % { 1 2 3 | 2 1 4 }
peval s3([1 3 4])         % { 1 4 5 | 2 1 6 }
peval s3(index([1 3 4]))  % { 1 3 4 | 2 4 1 }
peval tmp=s3; peval tmp.dd([2 1 3 4]) = [6 7 8 9] % { 6 7 8 9 | 4 2 1 6 }
peval tmp=s3; peval tmp.dd([2 1 3 4]) = [-1 7 8 9] % error
peval tmp=s3; peval tmp.ss([2 1 4]) = [7 8 9] % { 1 3 4 5 | 2 4 1 6 }
peval tmp=s3; peval tmp([2 1 4]) = [7 8 9] % { 1 3 4 5 | 2 4 1 6 }
peval tmp=s3; peval tmp(index([2 1 4])) = [7 8 9]   %...| 7 8 4 9 6 } not 8 7 4 9 6
say

say testing shape properties and conversions
peval numelem(s3)         % 48
peval length(s3)          % 4
peval trim(s3)            % { 1 3 5 | 2 4 6 }
peval double(s3)          % 2 1 4 1 6
peval consistent(s3,s2)   % 0
peval consistent(s1,s3)   % 1
peval consistent(s3,s1)   % 1
peval consistent(s2,s4)   % 1
peval consistent(s2,s5)   % 0
peval subshape(s4,s2)     % 1
peval subshape(s5,s2)     % 0
peval subshape(s6,s2)     % 1
say

say testing shape operators
peval intersect(s2,s3)                 % error
peval intersect(s2,s4)                 % { | }
peval intersect(s2,s5)                 % error
peval intersect(s2,s6)                 % { 1 2 | 2 4 }
peval union(s2,s3)                     % error
peval union(s2,s4)                     % { 1 2 3 | 2 4 6 }
peval union(s2,shape([3 4 5],[6 7 8])) % { 1 2 3 4 5 | 2 4 6 7 8 }
peval diff(s2,s3)                      % error
peval diff(s2,s4)                      % { 1 2 3 | 2 4 6 }
peval diff(s2,shape([3 4 5],[6 7 8]))  % { 1 2 | 2 4 }
peval xor(s2,s3)                       % error
peval xor(s2,s4)                       % { 1 2 3 | 2 4 6 }
peval xor(s2,shape([3 4 5],[6 7 8]))   % { 1 2 4 5 | 2 4 7 8 }

say testing shape table constructors
peval ones(s2)
peval zeros(s2)
peval zeros(s4)
peval zeros(s5)
