function makesystem(args);

if nargin==0
  args = 'all';
end

try
  cd CFiles
  switch args
  case 'all'
    echo on
    make_cfiles
    make_index
    make_shape
    make_table
    echo off
  case 'clean'
    echo on
    !rm ../@index/*.mex* ../@shape/*.mex* ../@table/*.mex* *.o
    echo off
  case 'index'
    echo on
    make_cfiles
    make_index
    echo off
  case 'shape'
    echo on
    make_cfiles
    make_shape
    echo off
  case 'table'
    echo on
    make_cfiles
    make_table
    echo off
  end
catch
  cd ..
  rethrow(lasterror);
  return;
end
cd ..
 
