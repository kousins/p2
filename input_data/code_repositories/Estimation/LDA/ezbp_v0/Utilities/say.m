function say(varargin);
% print statement to screen.
% if an equal sign is encountered the following expression is evaluated
% and output printed.
% to print a string including spaces put them in single quotes.
% to print an equal sign use backslach i.e. \=

for i=1:nargin
  if ischar(varargin{i}) 
    if varargin{i}(1)=='='
      varargin{i} = evalin('caller',varargin{i}(2:end));
    elseif varargin{i}(1)=='\'
      varargin{i} = varargin{i}(2:end);
    end
  end
  fprintf(1,'%s ',num2str(varargin{i}));
end
fprintf(1,'\n');

