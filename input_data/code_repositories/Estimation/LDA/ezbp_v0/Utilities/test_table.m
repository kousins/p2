say testing table constructors
peval table(shape([5 3 4],[2 3 4]))
peval t1 = table(reshape(1:24,[2 3 4]))
peval table(reshape(1:24,[2 3 4]),[5 3 4])
peval table(1:24,[5 3 4],[2 3 4])
peval table(1:24,shape([5 3 4],[2 3 4])) % different from above

say testing table subscripts
peval t1 = table(reshape(1:24,[2 3 4]))
peval t1.ee
peval t1.zz
peval t1.dd
peval t1.ss
peval t1.shape
peval t1([1 2 1],:,[2:end-1])
peval t1.ee = randn(1,24)
peval t1.zz = log(.1)
peval t1.ss = [3 2 4]
peval t1.dd = [8 7 9]
peval t1.shape = shape([1 2 3],[2 3 4])
peval t1(:,:,1) = reshape(1:6,[2 3])
peval t1(end:-1:1,:,2) = table(1:6,[1 2],[2 3])
peval t1(end:-1:1,:,3) = table(1:6,[1 3],[2 3])

say testing table properties and conversions
peval tt = sumnorm(t1)
peval numelem(tt)
peval numdim(tt)
peval double(tt)
peval tt(:,1,:)
peval trim(tt(:,1,:))
peval shape(tt)
peval setz(tt,0)
peval zeroz(tt)
peval sumnorm(tt)
peval maxnorm(tt)
peval maxabsnorm(tt)
peval zeros(tt)
peval ones(tt)
peval permute(double(tt),[2 3 1])
peval squeeze(double(permute(t1,[1 3],[5 4]))) % should be same

say testing table operators
peval t1 = sumnorm(table(rand(3,4),[1 2]))
peval t2 = sumnorm(table(rand(4,2),[2 3]))

peval -double(t1)
peval double(-t1)
peval exp(double(t1))
peval double(exp(t1))
peval log(double(t1))
peval double(log(t1))

peval repmat(double(t1),[1 1 2]) + repmat(double(t2),[3 1 1])
peval double(t1+t2)
peval repmat(double(t1),[1 1 2]) - repmat(double(t2),[3 1 1])
peval double(t1-t2)
peval max(repmat(double(t1),[1 1 2]),repmat(double(t2),[3 1 1]))
peval double(max(t1,t2))
peval min(repmat(double(t1),[1 1 2]),repmat(double(t2),[3 1 1]))
peval double(min(t1,t2))
peval repmat(double(t1),[1 1 2]) .* repmat(double(t2),[3 1 1])
peval double(t1.*t2)
peval repmat(double(t1),[1 1 2]) ./ repmat(double(t2),[3 1 1])
peval double(t1./t2)
peval repmat(double(t1),[1 1 2]) .\ repmat(double(t2),[3 1 1])
peval double(t1.\t2)
peval repmat(double(t1),[1 1 2]) .^ repmat(double(t2),[3 1 1])
peval double(t1.^t2)

peval sum(double(t1),2)
peval double(sum(t1,2))
peval prod(double(t1),2)
peval double(prod(t1,2))
peval max(double(t1),[],2)
peval double(maxmarg(t1,2))
peval min(double(t1),[],2)
peval double(minmarg(t1,2))
peval sum(double(t1),1)

peval double(sumto(t1,2))
peval prod(double(t1),1)
peval double(prodto(t1,2))
peval max(double(t1),[],1)
peval double(maxmargto(t1,2))
peval min(double(t1),[],1)
peval double(minmargto(t1,2))

peval double(sumprod(t1,t2,2))
peval maxsum(t1,t2,[1 3])
peval minsum(t1,t2,1:3)
peval maxsum(t1,t2,4)

peval double(t1)*squeeze(double(t2))
peval squeeze(double(t1 * t2))
