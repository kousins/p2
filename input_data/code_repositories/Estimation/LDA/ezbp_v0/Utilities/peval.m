function peval(varargin);
% print statement to screen and evaluate it.

statement = sprintf('%s ',varargin{:});
fprintf(1,'%s\n  --> ',statement);
try
  evalin('caller',statement)
catch
  e = lasterror;
  fprintf(1,'Error: %s\n',e.message);
end
