function ii = contasgn(oo,SS,ii);

if length(SS) > 1, 
  ii = subsasgn(oo,SS(2:end),ii); 
end

