function [ii, kk] = validindices(ii)
% check to see that domain indices are valid.

if any(ii~=int32(ii))
  error('Indices not integers.');
end
if any(ii<=0)
  error('Indices not positive.');
end

[ii kk] = sort(ii);

if any( diff(ii) == 0 )
  error('Indices repeated.')
end

