function oo = index(ii);

% Index Class
% An index is an unordered list of dimensions (nodes), which have to be
% positive integers.
%
% constructors
% ------------
% index(ss)             For ss a struct with a single field dd simply makes
%                       a MATLAB index object out of it.
% index(dims)           Constructs an index object with list of indices.
% index(ss), index(tt)  Constructs an index object out of the indices in 
%                       shape ss or table tt.
%
% subscripts
% ----------
% idx.dd                Returns the list of indices in idx as doubles.
% idx(nn)               Given list of numbers returns and index with the 
%                       entries corresponding to nn.
%
% properties and conversion
% -------------------------
% length(idx)           Returns the number of indices in idx.
% double(idx)           Returns the list of indices as a double vector
% isempty(idx)          Returns 1 if idx is empty, 0 otherwise.
% subindex(i1,i2)       Returns 1 if all indices in i1 are in i2.
% 
% operators
% ---------
% intersect(i1,i2)      Returns indices contained in both i1, i2.
% union(i1,i2)          Returns indices contained in either i1, i2.
% diff(i1,i2)           Returns indices in i1 but not i2.
% xor(i1,i2)            Returns indices in either i1 or i2, but not both.


if isa(ii,'struct'),
  oo    = class(ii,'index');
elseif isa(ii,'shape') || isa(ii,'table')
  oo.dd = ii.dd;
  oo    = class(oo,'index');
else
  oo.dd = validindices(double(ii));
  oo    = class(oo,'index');
end


