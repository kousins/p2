function oo = subsref(ii,SS);
% subsref

switch SS(1).type
case '.'
  switch SS(1).subs
  case 'dd' % dom.dd
    oo = contref(ii.dd,SS);
  otherwise
    error('Unknown field for index class.');
  end
case '()' % dom(location)
  if length(SS(1).subs) ~= 1
    error('Unknown () subsref type for index class.');
  end
  oo = ii;
  oo.dd = sort(oo.dd(SS(1).subs{1}));
  oo = contref(oo,SS);
  validindices(oo.dd);
otherwise
  error('Unknown {} subsref type for index class.');
end
