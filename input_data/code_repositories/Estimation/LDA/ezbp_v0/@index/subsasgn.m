function oo = subsasgn(oo,SS,ii);
% subsasgn for index class

switch SS(1).type
case '.'
  switch SS(1).subs 
  case 'dd' % dom.dd = ii
    ii    = validindices(ii);
    oo.dd = contasgn(oo.dd,SS,ii);
  otherwise
    error('Unknown field for index class.');
  end
case '()' % dom(location) = ii
  oo.dd = validindices(subsasgn(oo.dd,SS,ii));
otherwise
  error('Unknown subsasgn for index class.');
end

