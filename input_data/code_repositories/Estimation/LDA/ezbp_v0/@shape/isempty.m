function bb = isempty(ii)

bb = length(ii.dd) == 0;
