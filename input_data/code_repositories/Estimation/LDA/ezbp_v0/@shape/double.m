function nn = double(ii)
nn = ones(1,max([2 max(ii.dd(:))]));
nn(ii.dd) = ii.ss;
