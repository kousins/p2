function str = dispstr(ii);

str = [ sprintf('{ ') sprintf('%d ',ii.dd) sprintf('| ') ...
        sprintf('%d ',ii.ss) sprintf('}') ];
