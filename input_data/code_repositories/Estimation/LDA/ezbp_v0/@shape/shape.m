function oo = shape(ii,jj);

%         SHAPE
%         =====
% A shape consists of a list of dimensions (nodes), and the corresponding
% sizes along each dimension (cardinality of variables on those nodes).  A
% dimension is non-trivial if the corresponding size is ~= 1.  A dimension
% missing in a shape is considered a trivial dimension in the shape and has
% size 1.  It is assumed that dim is always sorted in increasing order, dims
% are positive integers, and sizes are non-negative integers.
% 
% constructors
% ------------
% shape(ss)               For ss a struct with fields 'dim' and 'size' simply
%                         makes a MATLAB object from the struct.
% shape(dims,sizes)       Constructs a shape with dims being the dimensions,
%                         and sizes being corresponding sizes.
% shape(array)            For multi-dimensional array constructs a shape where
%                         the dimensions are the dimensions along which array
%                         is non-trivial (has size ~= 1), and the sizes are
%                         simply the sizes of the array along these dimensions.
% shape(tt)               For a table tt returns its shape
%
% subscripts
% ----------
% shp.dd                  Returns the dimensions in shape shp.
% shp.ss                  Returns the sizes in shp.
% shp(nn)                 Given list of numbers nn returns the corresponding
%                         entries in shp.
% shp(dd)                 given index dd returns shape with corresponding
%                         indices.
% 
% properties and conversion
% -------------------------
% numelem(shp)            Returns the product of sizes in shp.
% length(shp)             Returns the number of dimensions in shp.
% trim(shp)               Removes trivial dimensions from shp.
% double(shp)             Returns a list of numbers such that the i'th number is
%                         the size of dimension i in shp (if i not in shp, is 1).
% isempty(shp)            Returns 1 if shp is empty (no dimensions and sizes).
% consistent(s1,s2)       Returns 1 if s1 and s2 are consistent shapes, i.e. the
%                         non-trivial dimensions have the same size.
% subshape(s1,s2)         Returns 1 if every non-trivial dimension of s1 is a
%                         non-trivial dimension of s2, and the sizes match.
% operators
% ---------
% intersect(s1,s2)        Constructs a shape whose set of non-trivial dimensions
%                         is the intersection of non-trivial dimensions of s1
%                         and s2, and whose sizes are those in s1, s2.
% union(s1,s2)            Same as intersect, but union of dimensions instead.
% diff(s1,s2)             Same as intersect, but dimensions are those in s1 not
%                         in s2.
% xor(s1,s2)              Same as intersect, but dimensions are those in s1 or
%                         s2 but not both.
% table construction
% ------------------
% ones(shp)               Constructs a table of shape shp filled with all ones.
% zeros(shp)              Constructs a table of shape shp filled with all zeros.


if isa(ii,'struct'), % convert struct to object
  oo = class(ii,'shape');
elseif isa(ii,'index')
  oo.dd  = double(ii);
  if nargin == 1 || isempty(jj) % index
    oo.ss = ones(size(oo.dd));
  elseif length(jj) == length(oo.dd) % index size
    oo.ss = validsizes(jj);
  else
    error('Sizes not consistent with indices.');
  end
  oo = class(oo,'shape');
elseif isa(ii,'table') % table
  oo    = ii.shape;
elseif nargin == 1 % array
  jj    = size(ii);
  oo.dd = find(jj ~= 1);
  oo.ss = jj(oo.dd);
  oo    = class(oo,'shape');
else % list-of-indices size
  [oo.dd oo.ss] = validindicessizes(double(ii),double(jj));
  oo            = class(oo,'shape');
end


