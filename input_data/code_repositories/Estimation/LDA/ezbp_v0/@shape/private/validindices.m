function [dd, kk] = validindices(dd)
% check to see that indices are valid.

if any( dd ~= max(1,int32(dd)) )
  error('Shape indices not positive integers.');
end

[dd kk] = sort(dd(:));

if any( diff(dd) == 0 )
  error('Shape indices repeated.')
end

