function ii = contref(ii,SS);

if length(SS) > 1, 
  ii = subsref(ii,SS(2:end)); 
end

