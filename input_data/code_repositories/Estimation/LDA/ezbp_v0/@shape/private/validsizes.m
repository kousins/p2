function ss = validsizes(ss)

if any( ss ~= max(0,int32(ss)) )
  error('Shape sizes not nonnegative integers.');
end
