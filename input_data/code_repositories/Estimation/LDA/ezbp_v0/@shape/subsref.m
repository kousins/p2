function oo = subsref(ii,SS);
% subsref

switch SS(1).type
case '.'
  switch SS(1).subs
  case 'dd' % shape.dd(location)
    oo = contref(ii.dd,SS);
  case 'ss'
    oo = contref(ii.ss,SS);
  otherwise
    error('Unknown field for shapes.');
  end
case '()'
  if length(SS(1).subs) ~= 1
    error('Unknown shape () subsref type.');
  end
  if isa(SS(1).subs{1},'index') % shape(index)
    oo = ref(ii,SS(1).subs{1});
  else                        % shape(location)
    oo = ii;
    [oo.dd oo.ss]=validindicessizes(oo.dd(SS(1).subs{1}),oo.ss(SS(1).subs{1}));
  end
  oo = contref(oo,SS);
otherwise
  error('Shape {} subsref undefined.');
end
