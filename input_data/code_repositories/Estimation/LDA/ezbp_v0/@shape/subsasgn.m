function oo = subsasgn(oo,SS,ii);
% subsasgn

switch SS(1).type
case '.'
  switch SS(1).subs
  case 'dd' % shape.dd(location) = double or = index
    oo.dd = contasgn(oo.dd,SS,double(ii));
    [oo.dd oo.ss] = validindicessizes(oo.dd,oo.ss);
  case 'ss' % shape.ss(location) = double
    oo.ss = validsizes(contasgn(oo.ss,SS,ii));
  otherwise
    error('Unknown field for shapes.');
  end
case '()' 
  if length(SS) > 1 ||  length(SS.subs) > 1
    error('Unknown subsasgn for shapes.');
  end
  if isa(SS.subs{1},'index') % shape(index) = something
    dd = double(SS.subs{1});
  else                        % shape(location) = something
    dd = oo.dd(SS.subs{1});
  end
  if isa(ii,'shape') % shape(something) = shape
    if any ( dd ~= ii.dd )
      error('Indices mismatched.');
    end
    oo = asgn(oo,dd,ii.ss);
  else               % shape(something) = sizes
    oo = asgn(oo,dd,ii);
  end
otherwise
  error('Unknown subsasgn for shapes.');
end

