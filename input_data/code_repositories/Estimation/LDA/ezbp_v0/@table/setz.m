function tt = setlogz(tt,logz);

zz    = exp(tt.zz-logz);
tt.zz = logz;
tt.ee = tt.ee * zz;
