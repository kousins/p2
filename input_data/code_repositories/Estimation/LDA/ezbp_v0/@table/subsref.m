function oo = subsref(tt,SS);
% subsref

switch SS(1).type
case '.'
  switch SS(1).subs
  case 'ee',      oo = contref(tt.ee,SS);              % table.ee
  case 'zz',      oo = contref(tt.zz,SS);              % table.zz
  case 'dd',      oo = contref(tt.dd,SS);              % table.dd
  case 'ss',      oo = contref(tt.ss,SS);              % table.ss
  case 'shape',   oo = contref(shape(tt.dd,tt.ss),SS); % table.shape
  otherwise
    error('Unknown field for tables.');
  end
case '()' % table(indices)
  tt.ee    = subsref(tt.ee,SS);
  for kk = 1:length(tt.ss)
    tt.ss(kk) = size(tt.ee,kk);
  end
  oo = tt;
otherwise
  error('Unknown subsref for tables.');
end
