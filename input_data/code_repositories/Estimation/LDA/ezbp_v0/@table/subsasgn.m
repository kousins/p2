function tt = subsasgn(tt,SS,ii);
% subsasgn

switch SS(1).type
case '.'
  switch SS(1).subs
  case 'ee',      tt.ee = contasgn(tt.ee,SS,ii);
  case 'zz',      tt.zz = ii;
  case 'shape',   [tt.dd tt.ss] = validindicessizes(...
                        contasgn(tt.dd,SS,ii.dd),...
                        contasgn(tt.ss,SS,ii.ss));
  case 'dd',      dd    = contasgn(tt.dd,SS,ii);
                  if any(dd~=int32(dd)) || any(diff(sort(dd))==0) || ...
                     length(dd)~=length(tt.dd)
                    error('New indices invalid.');
                  end
                  tt    = permute(tt,tt.dd,dd); 
  case 'ss',      tt.ss = contasgn(tt.ss,SS,validsizes(ii));
  otherwise
    error('Unknown field for tables.');
  end
  tt = validtable(tt);
case '()'
  if isa(ii,'table')
    if tt.zz < ii.zz
      tt.ee = tt.ee * exp(tt.zz - ii.zz);
      tt.zz = ii.zz;
    elseif tt.zz > ii.zz
      ii.ee = ii.ee * exp(ii.zz - tt.zz);
    end
    ii = ii.ee;
  else
    if tt.zz < 0
      tt.ee = tt.ee * exp(tt.zz);
      tt.zz = 0;
    else
      ii = ii * exp(-tt.zz);
    end
  end
  tt.ee = subsasgn(tt.ee,SS,ii);
  for kk = 1:length(tt.dd)
    tt.ss(kk) = size(tt.ee,kk);
  end
  validtable(tt);
otherwise
  error('Unknown subsref for tables.');
end

function ii = contasgn(oo,SS,ii);

if length(SS) > 1, 
  ii = subsasgn(oo,SS(2:end),ii); 
end
