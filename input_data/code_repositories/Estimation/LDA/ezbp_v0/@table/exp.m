function table = exp(table);

table = maxnorm(table);
table.ee = exp((table.ee-1)*exp(table.zz));
table.zz = exp(table.zz);
