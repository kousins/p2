function tt = table(aa,ii,jj)

%         TABLE
%          =====
% A table is a list of numbers arranged in a particular shape.  This is
% represented by an actual list of doubles ee, and another double zz.  The
% numbers are actually given by ee * exp(zz).
% table(tt)               For struct tt with fields 'shape','zz','ee', 
%                         constructs the corresponding table object.
% table(ss)               For shape ss returns a table of shape ss, entries 
%                         initialized to 0.
% table(array)            For multi-dimensional array converts the array into
%                         a table object whose shape is shape(array).
% table(array,dd)         For multi-dimensional array converts the array into
%                         a table object, but with the non-trivial indices
%                         given by dim instead.  dim need not be sorted.
% table(array,dd,ss)      Constructs table with entries in array, and shape
%                         with indices dd and sizes ss.
% table(array,ss)         Constructs table with entries in array, and shape ss.
%
% subscripts
% ----------
% NOTE: these are low-level operations.  For example setting zz using subscripts
% do not rescale the ee entries.  Use setz(tt,zz) instead for this purpose.
% tt.ee                   Returns list of numbers in table tt.
% tt.zz                   Returns the zz value.
% tt.dd                   Returns the dimensions of the table (tt.shape.dim).
% tt.ss                   Returns the sizes of the table (tt.shape.size).
% tt.shape                Returns the shape of the table.
% tt(I1,...,In)           Returns a table obtained by subscripting the entries
%                         with I1,...,In.  n should be equal to tt.numdim.  The
%                         ordering of I1,...,In conforms to the shape of the
%                         table.  For example, if tt has dimensions [2 3 4 8]
%                         then I1 indexes the second dimension I2 the third
%                         dimension, I3 the fourth dimension and I4 the eighth
%                         dimension.
% properties and conversion
% -------------------------
% numelem(tt)             Returns the number of elements in tt.
% numdim(tt)              Returns the number of dimensions in tt.
% double(tt)              Converts table into a multi-dimensional array.
% trim(tt)                Retains only non-trivial dimensions in tt's shape.
% shape(tt)               Returns the shape of tt.
% setz(tt,zz)             Rescales the entries of tt so that zz is as given.
% zeroz(tt)               Equivalent to tt.zz = 0. different from setz(tt,0).
% sumnorm(tt)             Rescales entries so that they sum to 1, adding the
%                         log normalization constant to zz.
% maxnorm(tt)             Same as sumnorm, but with maximum value 1.
% maxabsnorm(tt)          Same as sumnorm, but with maximum absolute value 1.
% zeros(tt)               Returns a table of 0's of same shape.
% ones(tt)                Returns a table of 1's of same shape.
% permute(tt,d1,d2)       Permutes entries table so that original dimensions in
%                         list of numbers d1 becomes new dimensions d2.
%                         Unenumerated dimensions are left as is.
% permute(tt,[d1 d2])     Same as permute(tt,d1,d2) with d1, d2 column vectors.
% 
% operators
% ---------
% Many operators are implemented.  Elementwise operators will "replicate"
% trivial dimensions of tables if need to (think repmat, genops).
% Some operators automatically rescales entries (and updates zz values)
% for numerical stability.  This should be transparent to the user.
% Further, these should work when some arguments are normal
% multi-dimensional arrays---these are automatically convert into
% tables by table(array).
% 
% unary operators: - + exp log
% comparative operators: == ~= < > <= >=
% binary elementwise operators: + - max min .* ./ .\ .^
%         These operators are "recursive".  If passed more than two arguments,
%         will expand as follows: operator(t1,t2,...,tn) becomes
%         operator(...operator(operator(t1,t2),t3),...)
% marginalization operators: sum prod maxmarg minmarg 
%                            sumto prodto maxmargto minmargto
%         These have form operator(tt,ss) where ss is a shape whose indices
%         are to be marginalized out.  ss can also simply be a list of indices
%         rather than a shape object.  The sumto, prodto etc operators are
%         similar to the correspond operators, but marginalizes out all other
%         indices instead.
% tensor operators: maxsum minsum sumprod maxprod
%         These have form operator(t1,t2,ss) where ss (either shape or list
%         of numbers) gives the indices to be marginalized out.  For example,
%         sumprod(t1,t2,ss) gives same output as sum(t1.*t2,ss), but is more 
%         efficient since the indices need not be expanded before summing.
% einstein summation: mtimes
%         t1 * t2 is equivalent to sumprod(t1,t2,intersect(t1.shape,t2.shape)).
 
if isa(aa,'struct'), % struct
  tt       = class(aa,'table');
elseif isa(aa,'shape'), % shape
  tt.dd    = aa.dd;
  tt.ss    = aa.ss;
  tt.zz    = 0.0;
  tt.ee    = zeros([tt.ss 1 1]);
  tt       = class(tt,'table');
elseif ~isnumeric(aa)
  error('Table constructor input matrix not numeric.');
else
  aa = double(aa);
end

if nargin==1 % matrix
  tt.dd    = 1:length(size(aa));
  tt.ss    = size(aa);
  tt.zz    = 0.0;
  tt.ee    = aa;
  tt       = trim(class(tt,'table'));
elseif isa(ii,'shape') % elements shape
  tt.dd    = ii.dd;
  tt.ss    = ii.ss;
  tt.zz    = 0.0;
  tt.ee    = reshape(aa,[tt.ss 1 1]);
  tt       = class(tt,'table');
elseif nargin==3 % elements dd ss
  tt.dd    = 1:length(jj);
  tt.ss    = double(jj);
  tt.zz    = 0.0;
  tt.ee    = reshape(aa,[jj 1 1]);
  tt       = permute(class(tt,'table'),tt.dd,double(ii));
else % elements dd
  ss       = size(aa);
  tt.dd    = find(ss~=1);
  tt.ss    = double(ss(tt.dd));
  tt.zz    = 0.0;
  tt.ee    = reshape(aa,[tt.ss 1 1]);
  tt       = permute(class(tt,'table'),tt.dd,double(ii));
end

