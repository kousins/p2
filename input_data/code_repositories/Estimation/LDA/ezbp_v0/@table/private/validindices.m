function [ii, kk] = validindices(ii)
% check to see that domain indices are valid.

if any( ii ~= max(1,int32(ii)) )
  error('Table indices must be positive integers.');
end

[ii kk] = sort(ii);

if any( diff(ii) == 0 )
  error('Table indices repeated.')
end

