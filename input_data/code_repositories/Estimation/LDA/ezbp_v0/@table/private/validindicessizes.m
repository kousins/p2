function [dd,ss,kk] = validindicessizes(dd,ss)
% check to see that domain indices and sizes are valid.

if numel(dd) ~= numel(ss)
  error('Shape indices and sizes not of equal length.');
end
validsizes(ss);
[dd kk] = validindices(dd);
ss      = ss(kk);
