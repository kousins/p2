function tt = validtable(tt);
% make sure table is valid.

if ~isnumeric(tt.zz) || numel(tt.zz)~=1
  error('Table zz field must be numeric scalar.');
end

validindicessizes(tt.dd,tt.ss);

if numel(tt.ee) ~= prod(tt.ss)
  error('Table sizes does not match number of entries.');
end

tt.ee = reshape(tt.ee,[tt.ss 1 1]);
