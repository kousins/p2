function ss = validsizes(ss)

if any( ss ~= max(0,int32(ss)) )
  error('Table sizes must be nonnegative integers.');
end
