function dd = double(tt);

dd = reshape(exp(tt.zz)*tt.ee,double(shape(tt.dd,tt.ss)));
