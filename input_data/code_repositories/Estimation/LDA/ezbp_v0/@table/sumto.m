function tt = sumto(tt,dd);

keyboard
tt = sum(tt,diff(index(tt.dim),dd));
