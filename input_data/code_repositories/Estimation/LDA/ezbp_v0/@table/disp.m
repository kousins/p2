function disp(tt);
% displays table

fprintf(1,'%s exp(%0.4f) * [\n', dispstr(shape(tt.dd,tt.ss)), tt.zz);

numelem = prod(tt.ss);
mm = min(6,numelem);
if mm > 0,
  dd = reshape(tt.ee(1:mm),1,mm);
  disp(dd);
  mm = min(12,numelem);
  if mm > 6, 
    dd = reshape(tt.ee(7:mm),1,mm-6);
    disp(dd);
    mm = min(18,numelem);
    if mm > 12, 
      dd = reshape(tt.ee(13:mm),1,mm-12);
      disp(dd);
    end
  end
end

if numelem > 18, fprintf(1,'... ]\n'); else fprintf(1,'    ]\n'); end
