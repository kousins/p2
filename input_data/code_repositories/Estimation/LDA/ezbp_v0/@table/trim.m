function tt = trim(tt);
% remove domains of size 1

ii    = find(tt.ss~=1);
tt.ss = tt.ss(ii);
tt.dd = tt.dd(ii);
tt.ee = reshape(tt.ee,[tt.ss 1 1]);
