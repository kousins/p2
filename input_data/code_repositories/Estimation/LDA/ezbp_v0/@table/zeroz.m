function tt = zeroz(tt);
% sets zz to 0.  E.g. zeroz(sumnorm(tt)) normalizes tt to sum to 1.

tt.zz = 0;
