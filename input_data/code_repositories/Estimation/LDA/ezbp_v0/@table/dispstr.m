function str = dispstr(tt);
% convert to a representation in string, summarizing the table.

if length(tt.ee)>3, ellipse = '... ]'; else ellipse = ']'; end
str = [ dispstr(shape(tt.dd,tt.ss)) sprintf(' exp(%0.4f) * [', tt.zz) ...
        sprintf(' %0.4f ',tt.ee(1:min(3,end))) ellipse ];


