function [AC] = EncoderRenormalise(AC);

while (AC.Range <= AC.BottomValue),

    tmp = bitshift(uint32(hex2dec('ff')),AC.ShiftBits);
    if (AC.Low < tmp),
    
        AC.BitStream = [AC.BitStream, AC.Buffer];
        
        while (AC.Help ~= 0),
            AC.BitStream = [AC.BitStream, uint8(hex2dec('ff'))];
            AC.Help = AC.Help - 1;    
        end

        AC.Buffer = bitshift(AC.Low,-AC.ShiftBits);
        
    else,
        
        if (bitand(AC.Low, AC.TopValue) ~= 0),
                       
            AC.BitStream = [AC.BitStream, AC.Buffer+1];
            
            while (AC.Help ~= 0),
                AC.BitStream = [AC.BitStream, uint8(hex2dec('00'))];
                AC.Help = AC.Help - 1;    
            end
            
            tmp = bitshift(AC.Low,-AC.ShiftBits);
            AC.Buffer = uint8(bitand(tmp, uint32(hex2dec('ff'))));

        else,
            AC.Help = AC.Help+1;            
        end
        
    end
    
    AC.Range = bitshift(AC.Range, 8);
    
    tmp = bitshift(AC.Low, 8);
    AC.Low = bitand(tmp, AC.TopValue-1);
    
    AC.ByteCount = AC.ByteCount+1;
end




%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
