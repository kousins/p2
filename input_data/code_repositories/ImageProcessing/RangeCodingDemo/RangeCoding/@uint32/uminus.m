function r = uminus(a);

r = -double(a);