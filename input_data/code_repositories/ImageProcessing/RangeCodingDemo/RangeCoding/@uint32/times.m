function r = times(a,b);

r = uint32(double(a) .* double(b));