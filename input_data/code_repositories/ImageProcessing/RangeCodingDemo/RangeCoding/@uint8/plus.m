function r = plus(a,b);

r = double(a) + double(b);
r = uint8(bitand(r,255)); % 2^8-1