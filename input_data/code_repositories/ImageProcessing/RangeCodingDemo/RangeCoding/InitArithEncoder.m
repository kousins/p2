function [ArithEncoder] = InitArithEncoder();

% Constants:
TopValue = uint32(hex2dec('80000000'));
BottomValue = uint32(hex2dec('00800000'));
ShiftBits = 23;
ExtraBits = 7;

% Variables:
Low = uint32(0);
Range = uint32(TopValue);
Buffer = uint8(0);
Help = uint32(0);
ByteCount = 0;

% Output:
BitStream = uint8([]);

ArithEncoder = struct('Low', Low, 'Range', Range, 'Buffer', Buffer, 'Help', Help, ...
    'TopValue', TopValue, 'BottomValue', BottomValue, 'ShiftBits', ShiftBits, 'ExtraBits', ExtraBits, ...
    'BitStream', BitStream, 'ByteCount', ByteCount);




%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
