function [AC] = EncoderFlush(AC);

AC = EncoderRenormalise(AC);


AC.ByteCount = AC.ByteCount+5;

var1 = bitand(AC.Low, AC.BottomValue-1);
var2 = uint32(bitand(AC.ByteCount, hex2dec('ffffff')));
var2 = bitshift(var2, -1);

if (var1 < var2),
    tmp = bitshift(AC.Low, -AC.ShiftBits);
else,
    tmp = bitshift(AC.Low, -AC.ShiftBits) + 1;
end
       

if (tmp > uint32(hex2dec('ff'))),
        
    AC.BitStream = [AC.BitStream, AC.Buffer+1];
    
    while (AC.Help ~= 0),
        AC.BitStream = [AC.BitStream, uint8(hex2dec('00'))];
        AC.Help = AC.Help - 1;    
    end
      
else,
    
    AC.BitStream = [AC.BitStream, AC.Buffer];
    
    while (AC.Help ~= 0),
        AC.BitStream = [AC.BitStream, uint8(hex2dec('ff'))];
        AC.Help = AC.Help - 1;    
    end
    
end

AC.BitStream = [AC.BitStream, uint8(bitand(tmp,hex2dec('ff')))];

tmp = bitshift(AC.Low,-(AC.ShiftBits-8));
AC.BitStream = [AC.BitStream, uint8(bitand(tmp,hex2dec('ff')))];

AC.BitStream = AC.BitStream(2:length(AC.BitStream)); % remove dummy byte at start...




%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	

