function [AC] = EncodeSymbol(AC, SymbolIndex, Histogram);

% SymbolIndex must be an integer greater than zero (within the range specified by Histogram). 

[Tot_f, Lt_f, Sy_f] = CalculateProbs(Histogram, SymbolIndex);

AC = EncoderRenormalise(AC);

R = uint32(double(AC.Range) / Tot_f);

tmp = R .* Lt_f;

if ((Lt_f + Sy_f) < Tot_f),

    AC.Range = R .* Sy_f;
    
else,
    
    AC.Range = AC.Range - tmp;
    
end

AC.Low = AC.Low + tmp;


%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
