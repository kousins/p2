% Range Coding Demo:
%
% Non-Adaptive case: It is assumed that the encoder and decoder both have a
% prior model of symbol probabilities. Thus a fixed histogram is used
% thoughout.
%
% IMPORTANT: It is assumed that all symbols are integers which lie in 
% the range [1,N] where N is the maximum symbol value. If this is not 
% the case, symbols need to me mapped to comply with this requirement 
% (and the inverse mapping can be applied after decoding).
%
%__________________________________________________________________________


%``````````````````````````````````````````````````````````````````````````
% Load Data, etc.
disp('Starting Range Coding Demo...');
clear all;
load RangeCodingDemoData DataIn % data to be encoded 
load RangeCodingDemoData Histogram; % (approximate) histogram showing distribution of data
NumSamples = length(DataIn);
%__________________________________________________________________________


%``````````````````````````````````````````````````````````````````````````
% Encoding...
AC = InitArithEncoder;  % Initialise Encoder 
for i=1:NumSamples,
    AC = EncodeSymbol(AC, DataIn(i), Histogram); % Encode each Sample
end
AC = EncoderFlush(AC);  % Finished Encoding
%__________________________________________________________________________

CodedBytes = AC.BitStream;

%``````````````````````````````````````````````````````````````````````````
% Decoding...
DataOut = [];
AC = InitArithDecoder(CodedBytes); % Initialise Decoder
EndOfBitStream = 0;
for i=1:NumSamples,
    [AC, DataOut(i), EndOfBitStream] = DecodeSymbol(AC, Histogram); % Decode each Sample
end
%__________________________________________________________________________


%``````````````````````````````````````````````````````````````````````````
% Show Results (Optional)...

disp([num2str(NumSamples), ' samples encoded using ', num2str(length(CodedBytes)), ' bytes:   ', ...
        num2str(length(CodedBytes)*8/NumSamples), ' bits per sample.']);

figure(1)
bar(Histogram)
xlabel('Symbol Index');
title('Histogram of Symbols');

figure(2)
plot(DataIn,'b.');
hold on 
plot(DataOut,'ro');
hold off
axis([0,NumSamples,0,max(DataIn)+1]);
legend('Input Data', 'OutputData');
xlabel('Sample Number');
ylabel('Symbol Index');

if (isequal(DataIn,DataOut)),
    disp('Encoding and Decoding Successful :-)');
else,   
    error('Error Encountered !!! ');
end
%__________________________________________________________________________



%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
