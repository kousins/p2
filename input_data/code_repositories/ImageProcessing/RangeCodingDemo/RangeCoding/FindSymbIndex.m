function SymbolIndex = FindSymbIndex(Histogram, SymCumProb);

CumProb = cumsum(Histogram);
Choice = SymCumProb < CumProb;

[y, SymbolIndex] = max(Choice);



%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
