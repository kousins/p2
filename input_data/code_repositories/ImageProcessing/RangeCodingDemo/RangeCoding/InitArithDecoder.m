function [ArithDecoder] = InitArithDecoder(BitStream);

% Constants:
TopValue = uint32(hex2dec('80000000'));
BottomValue = uint32(hex2dec('00800000'));
ShiftBits = 23;
ExtraBits = 7;

% Variables:
Low = uint32([]);
Range = uint32([]);
Buffer = uint8([]);
Help = uint32(0);

% Counter:
i = 1;

BitStream = [0, BitStream, 0 0 0];

ArithDecoder = struct('Low', Low, 'Range', Range, 'Buffer', Buffer, 'Help', Help, ...
    'TopValue', TopValue, 'BottomValue', BottomValue, 'ShiftBits', ShiftBits, 'ExtraBits', ExtraBits, ...
    'BitStream', BitStream, 'i', i);

ArithDecoder.Buffer = ArithDecoder.BitStream(2); 

if (length(BitStream)==4),
    ArithDecoder.i = 5;
else,
    ArithDecoder.i = 3;
end
    
ArithDecoder.Low = uint32(bitshift(ArithDecoder.Buffer, -(8-ArithDecoder.ExtraBits)));

ArithDecoder.Range = bitshift(uint32(1), ArithDecoder.ExtraBits);




%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
