function [CumulativeCount, PrevSymbCumCount, CurrentSymbCount] = CalculateProbs(Histogram, SymbolIndex);

Histogram = [0, Histogram];
SymbolIndex = SymbolIndex + 1;

CurrentSymbCount = Histogram(SymbolIndex);

CHist = cumsum(round(Histogram));

CumulativeCount = CHist(length(CHist));

PrevSymbCumCount = CHist(SymbolIndex-1);

%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
