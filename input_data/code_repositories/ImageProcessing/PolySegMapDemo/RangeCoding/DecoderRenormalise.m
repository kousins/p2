function [AC, EndOfBitStream] = DecoderRenormalise(AC);

EndOfBitStream = 0;
while (AC.Range <= AC.BottomValue),
    
    if (AC.i > length(AC.BitStream)),
        EndOfBitStream = 1;
        break;
    else,
        tmp1 = bitshift(AC.Low, 8);
        tmp2 = bitshift(AC.Buffer, AC.ExtraBits); % implicit and with "ff"
        AC.Low = bitor(tmp1, uint32(tmp2));
        
        AC.Buffer = AC.BitStream(AC.i);
        AC.i = AC.i + 1;
        
        tmp = bitshift(AC.Buffer, -(8 - AC.ExtraBits));
        AC.Low = bitor(AC.Low, uint32(tmp));
        
        AC.Range = bitshift(AC.Range, 8);
    end
    
end


%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
