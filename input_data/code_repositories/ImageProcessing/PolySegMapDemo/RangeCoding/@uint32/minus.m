function r = minus(a,b);

%r = uint32(double(a) - double(b));
r = double(a) - double(b);
r = uint32(bitand(r,2^32-1)); % 2^8-1