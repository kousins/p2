function [AC, SymbolIndex, EndOfBitStream] = DecodeSymbol(AC, Histogram);

% SymbolIndex is an integer greater than zero (within the range specified by Histogram). 

%--------------------------
% Decode Symbol:

[AC, EndOfBitStream] = DecoderRenormalise(AC);

if EndOfBitStream,
   SymbolIndex = [];
else,
    
    Tot_f = sum(Histogram);
    
    AC.Help = AC.Range ./ Tot_f;
    
    tmp = AC.Low ./ AC.Help;
    
    if (tmp>=Tot_f),
        SymCumProb = Tot_f - 1;    
    else,
        SymCumProb = tmp;
    end
    
    SymbolIndex = FindSymbIndex(Histogram, SymCumProb);
    
    
    %--------------------------
    % Update Decoder State:
    
    [Tot_f, Lt_f, Sy_f] = CalculateProbs(Histogram, SymbolIndex);
    
    tmp = AC.Help .* Lt_f;
    
    AC.Low = AC.Low - tmp; % tmp used again below...
    
    if (Lt_f + Sy_f < Tot_f),
        AC.Range = AC.Help .* Sy_f;    
    else,
        AC.Range = AC.Range - tmp;
    end
    
 end
 
 
 
%----------------------------------------------------------------	
% MATLAB CODE FOR RANGE CODING. 
% Marc Servais (m.servais@surrey.ac.uk) 02 February 2004.
% Based on Algorithm at http://www.arturocampos.com/ac_range.html 
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
