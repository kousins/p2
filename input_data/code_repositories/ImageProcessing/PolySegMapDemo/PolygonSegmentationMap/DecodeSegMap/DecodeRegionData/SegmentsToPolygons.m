function PolyList = SegmentsToPolygons(SegmentList);

% For a given list of segments, find the resulting polygons:

SegmentList = SortSegments(SegmentList);

[M, Nodes] = unmesh(SegmentList);
Nodes = round(Nodes);
M = full(M);

JoinedNodes = (M == -1);
NumSegsPerNode = diag(M);
clear M


% UNMESH will convert the data to the more common adjacency matrix, 
% where a non-zero entry (a -1 in this case) for element i,j indicates
% a connection between nodes i and j.  UNMESH also puts the degree on 
% the diagonal (total number of connections to a node).  Nodes with a 
% 1 on the diagonal are endpoints.  Nodes with 2 are part of a line 
% segment, and nodes with 3 or greater are "branch nodes".

NumPolygons = 0;

% for each unchecked link(segment), find the rest of the polygon (if there is one)

for n1=1:size(JoinedNodes,1),
    for n2=1:size(JoinedNodes,1),
        if JoinedNodes(n1,n2),
            
            % Find polygon that this segment belongs to:
            [Polygon, PolygonI, PArea] = FindRestOfPoly(n1, n2, Nodes, JoinedNodes, NumSegsPerNode);
            
            % Mark off the links in chain as checked 
            for i=1:size(PolygonI,1),
                JoinedNodes(PolygonI(i,1), PolygonI(i,2)) = 0;    
            end
            
            if PArea > 0,
                % Add the current polygon to the list of polygons
                NumPolygons = NumPolygons + 1;
                PolyList(NumPolygons) = struct('NumNodes', size(Polygon,1), 'Nodes', Polygon(:,1:2)+0.5, 'InteriorRegions', []);
                PolySize(NumPolygons) = PArea;
            end            
        end
    end
end

% Sort PolyList into Polygons of increasing size
UnsortedPolyList = PolyList;
clear PolyList
[y, SortI] = sort(PolySize);
for i=1:NumPolygons,
    PolyList(i) = UnsortedPolyList(SortI(i));
    PolyList(i).Nodes = flipud(PolyList(i).Nodes); % fix polygon orientation
end
clear UnsortedPolyList

% If a region(polygon) contains other regions(polygons), we need to specify this:
for p1=2:NumPolygons,
    for p2=1:(p1-1),
        [in, on] = inpolygon(PolyList(p2).Nodes(:,2),PolyList(p2).Nodes(:,1),PolyList(p1).Nodes(:,2),PolyList(p1).Nodes(:,1));
        in = in & (~on);
        if ~ismember(0,in),
            PolyList(p1).InteriorRegions = [PolyList(p1).InteriorRegions, p2];
        end
    end
end


%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	