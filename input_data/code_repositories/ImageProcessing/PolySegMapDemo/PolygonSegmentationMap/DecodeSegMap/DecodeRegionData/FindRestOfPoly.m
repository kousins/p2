function [Polygon, PolygonI, PArea] = FindRestOfPoly(SegNode1, SegNode2, Nodes, JoinedNodes, NumSegsPerNode);

% Find the Polygon that a given segment belongs to.
% ASSUMPTION: Each region has at least three nodes (and segments) 

% figure(1)
% hold on
% for i=1:size(JoinedNodes,1),
%     for j=1:size(JoinedNodes,2),
%         if JoinedNodes(i,j),
%             plot([Nodes(i,2), Nodes(j,2)], [Nodes(i,1), Nodes(j,1)], '.b-');            
%         end
%     end
% end
% hold off

    

PolygonI = [SegNode1, SegNode2];
Polygon = [Nodes(SegNode1,:), Nodes(SegNode2,:)];

StartNode = SegNode1;

NumNodes = size(Nodes,1);
BackToStart = 0;


while ~BackToStart,
    
%     figure(2)
%     plot(Nodes(:,2),Nodes(:,1),'ro');
%     hold on;
%     plot(Nodes(SegNode2,2),Nodes(SegNode2,1),'rs');
%     plot([Nodes(SegNode1,2), Nodes(SegNode2,2)], [Nodes(SegNode1,1), Nodes(SegNode2,1)], '.b-');
    
    SegmentAngles = [];
    
    % find possible next nodes in polygon:    
    PossibleNextNodes = [1:NumNodes]; 
    NeighbouringNodes = JoinedNodes(SegNode2,:); % nodes joined to SegNode2
    NeighbouringNodes(SegNode1) = 0; % (except for SegNode1)
    PossibleNextNodes = PossibleNextNodes(NeighbouringNodes); % list of nodes which may be next...
    
%     plot(Nodes(PossibleNextNodes,2), Nodes(PossibleNextNodes,1), 'xg');
    
        
    % for each possible next node, calculate the one which gives the
    % minimum angle between the incoming an outgoing edge
    MinAngle = Inf;
    for i=1:length(PossibleNextNodes),
        
        TrialNode = PossibleNextNodes(i);
        SignedArea =  (Nodes(SegNode2,1) - Nodes(SegNode1,1)) * (Nodes(TrialNode,2) - Nodes(SegNode1,2)) ...
            - (Nodes(TrialNode,1) - Nodes(SegNode1,1)) * (Nodes(SegNode2,2) - Nodes(SegNode1,2));                
        
        DotProd = dot(Nodes(SegNode2,:)-Nodes(SegNode1,:), Nodes(SegNode2,:)-Nodes(TrialNode,:));

        angle = atan2(SignedArea, DotProd);
    
        if (angle<0),
            angle = angle + 2*pi;
        end
    
        %text(Nodes(TrialNode,2), Nodes(TrialNode,1), num2str(round(angle*180/pi)));

        
        
        if angle<MinAngle,
            MinAngle = angle;
            SegNode3 = TrialNode;
        end
        
    end

    PolygonI = [PolygonI; SegNode2, SegNode3];
    Polygon = [Polygon; Nodes(SegNode2,:), Nodes(SegNode3,:)];
    
    SegNode1 = SegNode2;
    SegNode2 = SegNode3;
    
    if SegNode2==StartNode,
        BackToStart = 1;
    end
    
end

% Find (signed) Area of Polygon:
PArea =  PolyArea2(Polygon(:,1),Polygon(:,2));


%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	