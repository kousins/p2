function [PolyList, DetailBytesUsed, FinalDMax] = DecodeRegionData(CoreBS, DetailBS, rows, cols, FinalDMax);


%---------------------------------------------------------------
% Decode Core Segments: 
AC = InitArithDecoder(CoreBS);
[AC, SegmentList] = DecodeCoreNodes(AC, rows, cols);
%---------------------------------------------------------------


%---------------------------------------------------------------
% Segments to Polygons: 
PolyList = SegmentsToPolygons(SegmentList);
disp(['Number of Polygons: ', num2str(length(PolyList))]);
NumCoreNodes = size(unique(SegmentList(:,1:2),'rows'),1);
disp(['Number of Initial Nodes: ', num2str(NumCoreNodes)]);
%---------------------------------------------------------------


%---------------------------------------------------------------
% Decode Detail Nodes: 
AC = InitArithDecoder(DetailBS);
[PolyList, NumDetailNodes, DetailBytesUsed, FinalDMax] = DecodeDetailNodes(PolyList, FinalDMax, rows, cols, AC);
disp(['Number of Detail Nodes: ', num2str(NumDetailNodes)]);
%---------------------------------------------------------------


%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	