function [PolyList, NumDetailNodes, DetailBytesUsed, FinalMaxDist] = DecodeDetailNodes(PolyList, MaxDistEnd, rows, cols, AC);

MaxDistStart = 2^ceil(log2(sqrt(rows^2 + cols^2)/2));

NumDetailNodes = 0;

for reg=1:length(PolyList),
    PolyList(reg).Nodes = PolyList(reg).Nodes - 0.5;
end

%--------------------------------------------------------------------------

% Initialisation...

NumRegions = length(PolyList);

SegmentList = [];

for region = 1:NumRegions,
    
    NumRegionSegments = PolyList(region).NumNodes;
    PolySegmentList = [];
    
    for segment = 1:NumRegionSegments,
        
        % Find the two Segment nodes.... 
        if segment==NumRegionSegments,
            SegNodeStart = PolyList(region).Nodes(NumRegionSegments,:);
            SegNodeEnd = PolyList(region).Nodes(1,:); 
        else,
            SegNodeStart = PolyList(region).Nodes(segment,:);
            SegNodeEnd = PolyList(region).Nodes(segment+1,:);
        end
        
        % Check if the segment is already known....
        if ismember([SegNodeEnd, SegNodeStart], SegmentList, 'rows'), % segment endpoints will be in opposite order in other polygons
            % segment is already known...    
        else,
            % segment not yet known - add it to the list
            PolySegmentList = [PolySegmentList; [SegNodeStart, SegNodeEnd]];
        end        
    end
    
    SegmentList = [SegmentList; PolySegmentList];    
    
end


%--------------------------------------------------------------------------

OriginalSegmentList = SegmentList;
iSegmentList = [1:size(SegmentList,1)];
%i = 1;

StartedSplitting = 0;
MaxDist = MaxDistStart;
EndOfBitStream = 0;
proceed = (MaxDist>=MaxDistEnd) & (~EndOfBitStream);

while (proceed),
    
    
    if StartedSplitting,
        NodeSplitHist = [AddNode(1), 0] + AddNode(2)*2; % assume we can expect twice as many splits as for the previous MaxDist threshold
        NodeSplitHist = max(NodeSplitHist,1);
    else,
        [AC, SplitChoice, EndOfBitStream] = DecodeSymbol(AC, [1 1]);
        if (EndOfBitStream), 
            proceed = 0;
        end
        if (SplitChoice==2),
            StartedSplitting = 1;
            NodeSplitHist = [MaxDist, 1]; % reasonable guess at probabilities
        end
    end
    

    
    AddNode = [0 0]; 
    
    CheckedSegmentList = [];
    iCheckedSegmentList = [];
    

    
    % Split Each Segment as directed...
    while (size(SegmentList,1) & StartedSplitting & proceed),
        
        
        Node1 = SegmentList(1,1:2);
        Node2 = SegmentList(1,3:4);
        NumSegments = size(SegmentList,1);
        iSeg = iSegmentList(1);
        
        [AC, SplitChoice, EndOfBitStream] = DecodeSymbol(AC, NodeSplitHist);   
        if (EndOfBitStream), 
            proceed = 0;
            break;  
        end

        
        if (SplitChoice==2),
            % YES - will split segment.
            AddNode = AddNode + [0 1];
            
            NumDetailNodes = NumDetailNodes + 1; 
            [Node3, AC, EndOfBitStream] = CalcAbsoluteDist(Node1, Node2, MaxDist, rows, cols, AC);
            if (EndOfBitStream), 
                proceed = 0;
                break;
            end
            
            SegmentList = SegmentList(2:NumSegments,:);
            SegmentList = [SegmentList; Node1, Node3; Node3, Node2];
            
            iSegmentList = iSegmentList(2:length(iSegmentList));
            iSegmentList = [iSegmentList, iSeg, iSeg];
            
        else,
            % NO - will not split segment.
            AddNode = AddNode + [1 0];
            
            CheckedSegmentList = [CheckedSegmentList; Node1, Node2];
            SegmentList = SegmentList(2:NumSegments,:);
            
            iCheckedSegmentList = [iCheckedSegmentList, iSeg];
            iSegmentList = iSegmentList(2:length(iSegmentList));
            
        end
    end
    
    
    SegmentList = [SegmentList; CheckedSegmentList];
    iSegmentList = [iSegmentList, iCheckedSegmentList];

    
    % Check for Fixed Crossing edges...
    [AC, SegmentList, iSegmentList, EndOfBitStream] = FixCrossingEdges2dec(AC, SegmentList, iSegmentList, rows, cols);
        
    
    if (~EndOfBitStream),
        MaxDist = MaxDist/2;
    end
    
    proceed = (MaxDist>=MaxDistEnd) & (~EndOfBitStream);
   
    
end

PolyList = RefinePolygons(PolyList, SegmentList, iSegmentList, OriginalSegmentList);

DetailBytesUsed = AC.i - 5;
FinalMaxDist = MaxDist*2;

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	