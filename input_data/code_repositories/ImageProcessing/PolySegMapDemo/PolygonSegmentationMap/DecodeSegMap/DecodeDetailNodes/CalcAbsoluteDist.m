function [Node3, AC, EndOfBitStream] = CalcAbsoluteDist(Node1, Node2, DistThresh, rows, cols, AC);

% Inverse of CalcRelativeDist.m

StepSize = 1/sqrt(2);
theta = atan2((Node2(1)-Node1(1)), (Node2(2)-Node1(2)));
d = 1 / (1 + abs(tan(theta)));
StepSize = sqrt(2*d*d - 2*d + 1)*.99;

Ax = Node1(1);
Ay = Node1(2);
Bx = Node2(1);
By = Node2(2);
L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 ); % Length of Node1-Node2 line

[rHist, sHist, r0, s0] = CalculateProbModel(Node1, Node2, L, DistThresh, StepSize);

% Decode:

[AC, NodeInZone, EndOfBitStream] = DecodeSymbol(AC, [1 19]);

if EndOfBitStream,
    Node3 = [];
else,
    if (NodeInZone-1),
        
        [AC, sSign, EndOfBitStream] = DecodeSymbol(AC, [1 1]);     
        [AC, s, EndOfBitStream] = DecodeSymbol(AC, sHist);
        [AC, r, EndOfBitStream] = DecodeSymbol(AC, rHist);
        
        if EndOfBitStream, 
            Node3 = [];
        else,
            
            s = s-s0;
            r = r-r0;
            sSign = sSign-1;
            
            r = r*StepSize/L;
            s = s*StepSize;
            s = s + DistThresh;
            s = (sSign*2-1) * s/L;
            
            % Find Component along line:
            Node3 = (Node1+Node2)/2;
            
            UnitVec = (Node2-Node1);
            UnitVec = UnitVec ./ (sqrt(sum(UnitVec.^2)));
            Node3 = Node3 + UnitVec*r*L; 
            
            P = Node3;
            
            UnitVec = ([0 -1; 1 0] * UnitVec')';
            Node3 = Node3 - UnitVec*s*L;
            
            Node3 = round(Node3);
        end
    else,
        [AC, Node3r, EndOfBitStream] = DecodeSymbol(AC, ones(1,rows+1));
        [AC, Node3c, EndOfBitStream] = DecodeSymbol(AC, ones(1,cols+1));
        if EndOfBitStream,
            Node3 = [];
        else,
            Node3 = [Node3r, Node3c] - 1;
        end
    end
end

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	


