function PolyList = RefinePolygons(PolyList, SegmentList, iSegmentList, OriginalSegmentList);

% Convert a list of segments into regions...

%--------------------------------------------------------------------------
NumRegions = length(PolyList);

for reg=1:NumRegions,
    
    NumRegionSegments = PolyList(reg).NumNodes;
    SortedRegionSegments = [];
    
    for seg=1:NumRegionSegments,
        
        % Find the two original Segment nodes.... 
        if seg==NumRegionSegments,
            SegNodeStart = PolyList(reg).Nodes(NumRegionSegments,:);
            SegNodeEnd = PolyList(reg).Nodes(1,:); 
        else,
            SegNodeStart = PolyList(reg).Nodes(seg,:);
            SegNodeEnd = PolyList(reg).Nodes(seg+1,:);
        end
        
        % Find the original segment Index...
        [found,iSeg] = ismember([SegNodeStart, SegNodeEnd], OriginalSegmentList, 'rows'); 
        direction = 1;
        if ~found,
            [found,iSeg] = ismember([SegNodeEnd, SegNodeStart], OriginalSegmentList, 'rows');
            direction = -1;
        end

        % Find all sub-segments...
        SubSegments = SegmentList(iSegmentList==iSeg,:);
        
        % Sort these sub-segments...
        SortedSubSegments = [];
        CurrentPoint = PolyList(reg).Nodes(seg,:);
        found = 1;
        if (direction>0),
            for SubSeg = 1:size(SubSegments,1),
                SortedSubSegments = [SortedSubSegments; CurrentPoint];
                [found,iSeg] = ismember(CurrentPoint, SubSegments(:,1:2), 'rows');     
                if found,
                    CurrentPoint = SubSegments(iSeg,3:4);
                end
            end
        else,
            for SubSeg = 1:size(SubSegments,1),
                SortedSubSegments = [SortedSubSegments; CurrentPoint];
                [found,iSeg] = ismember(CurrentPoint, SubSegments(:,3:4), 'rows');     
                if found,
                    CurrentPoint = SubSegments(iSeg,1:2);
                end
            end   
        end
                                     
        SortedRegionSegments = [SortedRegionSegments; SortedSubSegments];            
    end
    
    PolyList(reg).Nodes = SortedRegionSegments + 0.5;
    PolyList(reg).NumNodes = size(SortedRegionSegments,1);
    
end

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	