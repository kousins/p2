function [AC, SegmentList, iSegmentList, EndOfBitStream] = FixCrossingEdges2dec(AC, SegmentList, iSegmentList, rows, cols);

% At the encoder, we check if any two segments cross one another. If they do, then refine them
% to prevent crossing. This is the corresponding function at the decoder.


MustRefine=1;

while MustRefine,
    
    NumSegments = size(SegmentList,1);

    [AC, MustRefine, EndOfBitStream] = DecodeSymbol(AC, [1 1]); 
    if EndOfBitStream,
        break;  
    end
    MustRefine = MustRefine-1;
    
    if MustRefine,
        [AC, seg, EndOfBitStream] = DecodeSymbol(AC, [1:NumSegments]); 
        if EndOfBitStream,
            break;    
        else,
            Node1 = SegmentList(seg,1:2);
            Node2 = SegmentList(seg,3:4);
        end
        
        % Decode the new node: (use absolute co-ordinates)
        [AC, Node3r, EndOfBitStream] = DecodeSymbol(AC, ones(1,rows+1)); 
        [AC, Node3c, EndOfBitStream] = DecodeSymbol(AC, ones(1,cols+1));
        if EndOfBitStream,
            break;    
        else,
            Node3 = [Node3r, Node3c] - 1;
        end
        
        % Update Segment List:
        indx = [[1:seg-1], [seg+1:size(SegmentList,1)]]; % i.e. not element "seg"
        SegmentList = SegmentList(indx,:);
        SegmentList = [SegmentList; Node1, Node3; Node3, Node2];
        seg2 = iSegmentList(seg);
        iSegmentList = iSegmentList(indx);
        iSegmentList = [iSegmentList, seg2, seg2]; % used to keep track of which sub-segments map to the original segments

    end
    
end

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
