function [rHist, sHist, r0, s0] = CalculateProbModel(Node1, Node2, L, DistThresh, StepSize);


% Perpendicular distance from line:
% (sign is encoded seperately)
sMin = 0;
sMax = round(DistThresh/StepSize);
sHistX = [sMin:sMax];
s0 = find (sHistX==0);
sHistX = sHistX - sMin + 1;  % starts at 1
sHist = 10 +  20 * (sMax - abs((sHistX-s0))) / sMax ; % Simple Triangular Probability Model
sHist = round(sHist);
sHist = max(sHist,1); % shouldn't really be required

%-------------------------------------------------

% Distance along line (from midpoint):
rMin = round(-0.6*L/StepSize);
rMax = round(0.6*L/StepSize);
rHistX = [rMin:rMax];
r0 = find (rHistX==0);
rHistX = rHistX - rMin + 1;
rHist = 10 +  10 * (rMax - abs((rHistX-r0))) / rMax ; % Simple Triangular Probability Model
rHist = round(rHist);
rHist = max(rHist,1); % shouldn't really be required

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	