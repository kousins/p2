function Rn = PolygonsToRegionMap(PolyList, rows, cols);

Rn = zeros(rows, cols);
Img = zeros(rows,cols);

for i=length(PolyList):-1:1,
    Rn(roipoly(Img,PolyList(i).Nodes(:,2),PolyList(i).Nodes(:,1))) = i;
end

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	