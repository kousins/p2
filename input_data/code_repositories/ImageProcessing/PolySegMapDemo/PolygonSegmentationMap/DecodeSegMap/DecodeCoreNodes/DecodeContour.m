function [AC, Segments] = DecodeContour(AC, rows, cols);


% Decode position of first node:
i = 1;
[AC, NodeA(1,1)] = DecodeSymbol(AC, [1:rows+1]);  
[AC, NodeA(1,2)] = DecodeSymbol(AC, [1:cols+1]); 
NodeA = NodeA - 1;
DecodedNodes = [NodeA];
Stack = [];
AtDeadEnd = 0;
AtKnownNode = 0;
JoinedNodes = [];


% Encode from current position:
proceed = 1;

while proceed,
        
    % Decide what needs to be done next:
    
    if AtDeadEnd,
        % Reached Image border and there is nowhere else to go.
        Instruction = 'PopStack';
    else,
        [AC, data] = DecodeSymbol(AC, [1 1]); 
        if (data==2),
            Instruction = 'EncodeSeg';    
        else,
            Instruction = 'PopStack';    
        end
    end
    
    % Perform next operation:
    
    switch Instruction,
        
        case 'EncodeSeg',
            
            [AC, KnowNodeB] = DecodeSymbol(AC, [1 1]);
            KnowNodeB = KnowNodeB - 1;
 
            if KnowNodeB,
                [AC, j] = DecodeSymbol(AC, [1:size(DecodedNodes,1)]); 
                NodeB = DecodedNodes(j,:);
                AtKnownNode = 1;
            else,
                % Decode distance to Next Node:
                % (if doing modelling, can have histogram centered around "offset")
                offset = NodeA + 1;
                
                histogram = abs([1:rows+1]-offset(1)); 
                histogram = ceil(1.2*max(histogram)) - histogram;
                [AC, distance(1,1)] = DecodeSymbol(AC, histogram); 

                histogram = abs([1:cols+1]-offset(1)); 
                histogram = ceil(1.2*max(histogram)) - histogram;
                [AC, distance(1,2)] = DecodeSymbol(AC, histogram); 

                NodeB = NodeA + distance - offset;        
                DecodedNodes = [DecodedNodes; NodeB];
                j = size(DecodedNodes,1);
            end
            
            
            % Mark nodes as linked by segment:
            JoinedNodes(i,j) = 1;
            JoinedNodes(j,i) = 1;
            
            % Are we at a branch?
            [AC, AtBranch] = DecodeSymbol(AC, [1 1]); 
            AtBranch = AtBranch - 1;

            if AtBranch,
                Stack = [Stack,i];    
            end
            
            % Update:
            NodeA = NodeB;
            i = j;
            
            % Are we at a Dead End? (Image boundary only!)
            if (NodeA(1)==0) | (NodeA(1)==(rows+1)) | (NodeA(2)==0) | (NodeA(2)==(cols+1)),
                AtDeadEnd = 1;
            else
                AtDeadEnd = 0;
            end
            
        case 'PopStack',
            
            if length(Stack),
                i = Stack(1);
                Stack = Stack(2:length(Stack));
                NodeA = DecodedNodes(i,:);
                AtKnownNode = 0;
                AtDeadEnd = 0;
            else,
                proceed = 0;
            end          
          
        otherwise,
            
            error('Unknown Option! (MARC)');
            
    end
        
end

% Create Segment List
Segments = [];
NumNodes = size(JoinedNodes);
for i=1:NumNodes,
    for j=1:i,
        if JoinedNodes(i,j),
            Segments = [Segments; DecodedNodes(i,:), DecodedNodes(j,:)];
            JoinedNodes(i,j) = 0;
            JoinedNodes(j,i) = 0;
        end
    end
end

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	