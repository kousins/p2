function SegmentList = SortSegments(SegmentList);

% Orders segments from top and from left...

NumSegments = size(SegmentList,1);


% Ensure that the top/left most node is listed first... (for each segment)
for i=1:NumSegments,
    
    if (SegmentList(i,1) < SegmentList(i,3)),
        swop = 0;
    elseif (SegmentList(i,1) == SegmentList(i,3)),
        if (SegmentList(i,2) < SegmentList(i,4)),
            swop = 0;
        else,
            swop = 1;
        end
    else,
        swop = 1;
    end
    
    if swop,
        seg = SegmentList(i,:);
        SegmentList(i,:) = [seg(3), seg(4), seg(1), seg(2)];
    end
    
end

SegmentList = unique(SegmentList, 'rows');
SegmentList = sortrows(SegmentList);

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	