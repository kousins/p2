function SegmentList = RestoreBorderSegments(SegmentList, rows, cols);

% Add segments along image border.


NewSegments = [];

% first row:
iA = SegmentList(:,1) == 0;
iB = SegmentList(:,3) == 0;
xA = SegmentList(iA,2);
xB = SegmentList(iB,4);
x = unique([0; xA; xB; cols]);
for i=1:(length(x)-1),
    NewSegments = [NewSegments; 0, x(i), 0, x(i+1)];    
end


% last row:
iA = SegmentList(:,1) == rows;
iB = SegmentList(:,3) == rows;
xA = SegmentList(iA,2);
xB = SegmentList(iB,4);
x = unique([0; xA; xB; cols]);
for i=1:(length(x)-1),
    NewSegments = [NewSegments; rows, x(i), rows, x(i+1)];    
end


% first column:
iA = SegmentList(:,2) == 0;
iB = SegmentList(:,4) == 0;
xA = SegmentList(iA,1);
xB = SegmentList(iB,3);
x = unique([0; xA; xB; rows]);
for i=1:(length(x)-1),
    NewSegments = [NewSegments; x(i), 0, x(i+1), 0];    
end


% last column:
iA = SegmentList(:,2) == cols;
iB = SegmentList(:,4) == cols;
xA = SegmentList(iA,1);
xB = SegmentList(iB,3);
x = unique([0; xA; xB; rows]);
for i=1:(length(x)-1),
    NewSegments = [NewSegments; x(i), cols, x(i+1), cols];    
end


SegmentList = [SegmentList; NewSegments];

%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	
