function [AC, SegmentList] = DecodeCoreNodes(AC, rows, cols);

[AC, NumContours] = DecodeSymbol(AC, [256:-1:1]); 
NumContours = NumContours - 1;

SegmentList = [];
for c=1:NumContours,
    [AC, ContourSegments] = DecodeContour(AC, rows, cols);
    SegmentList = [SegmentList; ContourSegments];
end


% Restore Image Border Segments:
SegmentList = RestoreBorderSegments(SegmentList, rows, cols);

SegmentList = SortSegments(SegmentList);

%SegmentListSize = size(SegmentList)
%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	