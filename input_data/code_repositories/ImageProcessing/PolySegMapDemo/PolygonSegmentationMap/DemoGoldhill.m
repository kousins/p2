%----------------------------------------------------------------	
% MATLAB CODE FOR PROGRESSIVE POLYGON CODING OF SEGMANTATION MAPS 
% Marc Servais (m.servais@surrey.ac.uk) 09 February 2004.
% Refer to readme.txt for more information.
%----------------------------------------------------------------	

addpath('../RangeCoding');
addpath('./DecodeSegMap');
addpath('./data');
addpath('./DecodeSegMap/DecodeCoreNodes');
addpath('./DecodeSegMap/DecodeRegionData');
addpath('./DecodeSegMap/DecodeDetailNodes');

load goldhill DetailBS CoreBS rows cols
 
disp(' ');
disp(' ');
disp(' ');
disp('----------------------------------------------------------------------');
disp('                        SEGMENTATION MAP DEMO                         ');
disp('----------------------------------------------------------------------');
disp(' ');
disp('This demo involves the decoding of an encoded JSEG segmentation map of');
disp('the "Goldhill" image. When decoding,  you need to specify the stopping');
disp('condition. This can be either the final value for dmax [choose "d"] or');
disp('the number of bytes to decode [choose "b"].');
disp(' ');
d = 1; b = 2;
choice = input('Please select a stopping condition [enter "d" or "b"]:');

disp(' ');    
disp(' ');    


if isequal(choice,1),
    disp('Now choose an appropriate value for dmax. For a coarse approximation');
    disp('choose a large value for dmax (e.g. 64). For a more accurate polygon');
    disp('approximation choose a small value for dmax (e.g. 2). Note that dmax');
    disp('should be an integer power of 2: (1,2,4,8,16,32,...,512).');
    disp(' ');
    DMax = input('Please choose a value for dmax: ');
    DMax = 2^floor(log2(max(DMax,1)));
    disp(['Using dmax = ', num2str(DMax)]);
else,
    disp(['Now choose the number of bytes to be decoded. There are ', num2str(length(CoreBS)), ' bytes']);
    disp('used to describe the initial segments (all of which are required).');
    disp(['Between 0 and ', num2str(length(DetailBS)), ' bytes can be used to specify the detail segments.']);
    disp('The more bytes selected, the more accurate the resulting polygon   ');
    disp('approximation will be.');
    disp(' ');
    NumDetailBytes = input(['Please specify the number of detail bytes - in the range [0,', num2str(length(DetailBS)),']: ']);
    NumDetailBytes = round(NumDetailBytes);
    NumDetailBytes = max(NumDetailBytes,0);
    NumDetailBytes = min(NumDetailBytes,length(DetailBS));
    disp(['Using ', num2str(NumDetailBytes), ' detail bytes']);
    DetailBS = DetailBS(1:NumDetailBytes);
    DMax = 0;
end
disp(' ');

%--------------------------------------------------------------------------
% Decode Polygon-shaped regions:
[PolyList, DetailBytesUsed, DMax] = DecodeRegionData(CoreBS, DetailBS, rows, cols, DMax);

% Colour-in Segmentation Map:
Rn2 = PolygonsToRegionMap(PolyList, rows, cols);
%--------------------------------------------------------------------------


% Calculate Statistics and compare with original segmentation map:

load goldhill Rn NumContourPoints cmap

ErrorPerc = 100-sum(sum(Rn==Rn2))/rows/cols*100;
TotalBits = (length(CoreBS) + DetailBytesUsed)*8;

disp(' ');
disp(' ');
disp('``````````````````````````````````````````````````````````````````````');
disp('  Finished Decoding.... Summary of Statistics:');
disp(' ');
disp(['  Number of Total Bytes       : ', num2str(TotalBits/8), ' (', num2str(length(CoreBS)), ' initial + ', num2str(DetailBytesUsed), ' detail)';]);
disp(['  Final value of dmax         : ', num2str(DMax)]);
disp(['  Incorrectly Labelled Pixels : ', num2str(ErrorPerc), ' %']);
disp(['  Bits per Contour Point      : ', num2str(TotalBits/NumContourPoints)]);
disp('----------------------------------------------------------------------');
disp(' ');

figure(1)
imshow(Rn,cmap); 
title('Original Segmentation Map');

figure(2)
imshow(Rn2,cmap); 
title('Decoded Segmentation Map');

figure(3)
imshow((Rn==Rn2)*255, gray)
title('Error in the reconstructed Segmentation Map (incorrectly labelled pixels shown in black)');
