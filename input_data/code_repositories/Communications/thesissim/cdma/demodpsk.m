function [Datarx, DiffPhRx] = DemodPSK(Phases,wordsize,DPSK)
%DEMODPSK Demodulates phase shift keyed data.
%	Datarx = DemodPSK(Phases,wordsize)
%	Demodulates the phase angles back into to data. The phase angle
%	can be from 0 - 2*pi, which is converted to data from 0 - 2^wordsize.
%
%	Phases is a column vector of the phase angle, (in radians) of each
%	data word. If Phases is a matrix then each column is treated as
%	a separate column of data.
%	
%	Wordsize indicates the number of phase locations, and is the
%	number of bits sent in each word.
%	e.g. wordsize = 1 for BPSK, with phase locations of 0, 180 deg
%	wordsize = 2 for QPSK, with phase locations of 0, 90, 180, 270 deg.
%
%	Datarx = DemodPSK(Phases,wordsize,'diff')
%	This demodulates the phases as differential phase shift keying
%	by using the first phase angle as a reference then using the
%	difference in phase to find the data.
%	The absolute phase  demodulation is the default.
%
%	See : MODPSK
%
%	Copyright (c) Eric Lawrey July 1997

%Modified:
%	10/7/97	First wrote the function based on the demodulation that
%		was done in the COFDM receiver.
%		This function is finished and appears to work.

if nargin ~=3,
	DPSK = 'abso';		%Set the default demod to absolute phase
end
if DPSK == 'diff',
	DiffPh = diff(Phases);		 	%Compare phase of current to previous symbol
else
	DiffPh = Phases;
end
DiffPh = DiffPh*360/(2*pi);		%convert from radians to degrees

NegPh=find(DiffPh<0);			%Make all phases between 0 - 360 degrees
DiffPh(NegPh)=DiffPh(NegPh)+360;

Datarx = zeros(size(DiffPh));
PhInc = 360/(2^wordsize);		%Find the increment between the phase locations
DiffPhRx = rem(DiffPh/(PhInc)+0.5,(2^wordsize))*(PhInc)-(PhInc/2);

Datarx = floor(rem(DiffPh/(360/(2^wordsize))+0.5,(2^wordsize)));
