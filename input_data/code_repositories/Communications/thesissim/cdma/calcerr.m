function Summary = calcerr(Datatx,Datarx,subsignal)
%CALCERR Calculates the Bit Error Rate, RMS Amplitude Error, and No. of Error for CDMA
%
% 	 Summary = calcerr(Datatx,Datarx,subsignal)
%	 Summary contains all the relevent error statistics:
%	 Summary = [BER, AmpErr, NumErr]
%	 
%	 Copyright (c) Eric Lawrey 1997

%Modifications:
%	18/6/97 Inital write up of the function.
%	

AmpErr = std(Datatx - subsignal);
	
%=====================================
%Calculate the BER
%=====================================
Errors = find(Datatx-Datarx);
NumErr = length(Errors);
NumData=size(Datarx,1)*size(Datarx,2);	%find the total number of data sent
BER = NumErr/NumData;
Summary = [BER,AmpErr,NumErr];
	