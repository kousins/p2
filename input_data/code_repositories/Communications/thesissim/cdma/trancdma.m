function basesignal = tranCDMA(datatx,procgain,seqnum,linktype)
%TRANCDMA Generates a CDMA waveform from an input data.
%	  basesignal = tranCDMA(datatx,procgain,seqnum,wordsize,linktype)
%
%	  This function generates a CDMA time waveform based on the input parameters
%	  and the data given. The data is transmformed into a single frame CDMA signal. 
%	  This for comparsion with the simulation of a COFDM phone system.
%
%	  INPUTS:
%	  ========
%	  datatx     : Input data to transmit, in the format of a single row vector.
%	  procgain   : Process gain of the CDMA signal.
%	  seqnum     : Sequence number of the pseudo random sequence used to spread the
%		       signal.
%	  wordsize   : Optional, Determines whether to transmit using BPSK (1) or QPSK (2)
%		       (4) 16PSK, (8) 256PSK.
%	  linktype   : Sets whether to simulate the forward link, or the reverse link
%		       linktype = 0, forward link (using orthogonal Walsh codes)
%		       linktype = 1, reverse link (uncorrelated non-orthogonal codes)
%		       This is an optional parameter, with the default being
%		       the forward link.
%			
%	  OUTPUTS:
%	  ========
%	  BaseSignal : This is the output time signal for the CDMA waveform. The format
%		       of the output is a row vector.
%	  
%	  Copyright (c) Eric Lawrey 1997
%
%	  See:	RECCDMA.

%===================================
%	Required external functions:
%	(They just have to be in the same directory, or in the Matlab path)
%	genrand.m
%	walsh.m
%
%	Modifications:
%	8/6/97	Redo of the previous simulation script. (based on distort.m)
%	15/6/97	Continuing work on this function, currently not finished
%	16/6/97 Continued work on function, it is now finished and works, but not all 
%		the features have been tested.
%		I have tested the windowing and the zeroed guard period. The code has also
%		been optimized a bit and runs ~10 times faster then it used to, plus
%		it can handle much bigger files (tested upto 87kbytes, wordsize=4), in 35sec
%		It appears to work as a function.
%		The function needs to be changed so that it does not read the file directly
%		but instead get the data as input.
%	17/6/97	Modified the function so that it does read the input file from within 
%		the transmit function. This is so that the file can be read else where
%		then split up into smaller frames, then processed by transmit.
%		Fixed up some logical errors in the program, and removed the output phase
%		variable as it can be easily calculated from the data being transmitted.
%	6/7/97	Modified from the COFDM transmit into a CDMA transmit. 
%		The function is finished and tested to work. The output time waveform is
%		a vector of 1,-1,1,-1,-1 etc
%	9/7/97	Modified it so that the function can generate QPSK data. So far it has been
%		written and appears to work, but has not been tested with the receiver yet.
%	10/7/97	Fixed bugs in the code so that it now works for any word size.
%	7/8/97	Added the link type parameter so that both the forward link and
%		reverse link can be simulated.
%	9/8/97	3:40pm	trancdma.m
%		MAJOR CHANGE. The simulations were giving BER twice what was expected
%		and the use of QPSK and phase modulation was not an appropriate
%		way to model the CDMA transmission, So I modifed all the functions
%		removing the variable word size, and changed it back to just
%		amplitude modulation. The reverse link generation was simplified.
if nargin < 4,
	linktype = 0;	%Set the default to use the forward link.
end


%Make sure procgain is a power of 2, so that it matches the length of the Walsh codes
procgain = 2^(ceil(log(procgain)/log(2)));

if linktype == 0,
%==========================
%Simulate the FORWARD LINK
%==========================

	W = walsh(procgain,'+-');		%Find all walsh codes
	W = W(seqnum,:);			%Select the walsh code to use

	L = ceil(log(length(datatx)+1)/log(2));	%Number of doublings of the walsh 
						%function to make it
						%as long as the datatx
	basesignal = zeros(1,length(datatx)*procgain);
	for k = 1:length(datatx),
		mint = (k-1)*procgain+1;
		maxt = mint+procgain-1;
		basesignal(mint:maxt) = datatx(k)*W;
	end
else
%==========================
%Simulate the REVERSE LINK
%==========================
	signal = zerohold(datatx,procgain);
	seed = rand('seed');			%save the previous seed
	rand('seed',seqnum*1321);		%make the random seq based on the seqnum
	prs = genrand(1,1,length(signal),'+-');	%generate the random seq to use
	rand('seed', seed);			%restore the previous seed
	basesignal = prs.*signal;
end