%SIMCDMA Simulates a CDMA link then returns the results to a file.
%	This script simulates the CDMA link over two parameters at a time.
%	i.e. the BER vs SNR vs Number of users can be investigated.
%
%	To use this script, setup all the fixed parameters required, then
%	choose two parameters to run the simulation over. Setup X and Y ranges
%	and increment approriate for the parameter, and set Xvar and Yvar
%	to the names of the parameters to use.
%
%	Note: These simulations can take a long time (>1 hour) thus
%	the parameters should be chosen carefully.
%
%	More help can be found by looking at the help for cdma.m
%
%	See CDMA
%
%	Copyright (c) July 1997 Eric Lawrey

%===================================
%	Required external functions:
%	(They just have to be in the same directory, or in the Matlab path)
%	cdma.m
%		-trancdma.m
%			-modpsk.m
%			-walsh.m
%		-reccdma.m
%			-demodpsk.m
%			-walsh.m
%		-zerohold.m
%		-subsamp.m
%		-genrand.m
%		-channel.m
%		-calcerr.m
%		-savefile.m
%

%======================
%	Modifications:
%	9/7/97	Modifided to handle QPSK CDMA transfer, but it is not complete.
%	10/7/97	Works for any wordsize, also added oversampling which is
%		not fully tested.
%	28/7/97	9:10pm	simcdma.m
%		Modified the simulation from the simtxrx2.m program so that
%		the program can loop to run the simulation over a range of
%		parameters.
%		11:05pm simcdma.m
%		The script is completed (apart from some possible extra
%		commenting required), and partly tested. All appears to work
%		well.
%	1/8/97	10:15pm	simcdma.m
%		Changed the allocation of the Walsh code to the users. It is
%		randomly allocated, so the simulated channel changes with
%		each repetition.
%	7/8/97	2:00pm	simcdma.m
%		Added the option of simulating the reverse link as well as the
%		forward link. The reverse link is modelled by using a long
%		random sequence to spread the data with.
%	9/8/97	3:40pm	simcdma.m
%		MAJOR CHANGE. The simulations were giving BER twice what was expected
%		and the use of QPSK and phase modulation was not an appropriate
%		way to model the CDMA transmission, So I modifed all the functions
%		removing the variable word size, and changed it back to just
%		amplitude modulation. I also removed some of the parameters
%		which were never being removed, such as amplitude tolerance.
%	17/8/97	8:00pm simcdma.m
%		Added the option to record the rms amplitude instead of the
%		BER as normal. RecordType sets what to plot verses.
%	17/8/97	10:00pm simcdma.m
%		Added the option to have a tolerance on the time difference
%		between the users. This is done on the oversampled signal,
%		however the results from the time tolerance appear to lower
%		the BER, up to 2 times. This doesn't match what would be
%		expected
%	19/10/99
%		Updated the code to run under MATLAB 5.3, and improved the result file
%		saving to give easier to understand outputs

clear all;
flops(0);
tic;			%Start timer of the simulation
TotalWords = 64*10;	%Total data words to simulate
			%The total number of words is only approximate as
			%the total words used for the simulation must
			%be able to be split evenly into the number of
			%users. For example if TotalWords = 200 and there
			%are 30 users then each user will transmit 7 words,
			%thus the actual total number is 210 words

procgain = 64;		%Process gain of the CDMA (max number of users)
numusers = 12;		%Number of simultaneous users in the system
linktype = 1;		%Link type (0 = forward link), (1 = reverse link)
Comp = 0;		%Peak Power Compression due to clipping (dB)
SNR = 300;		%Signal to Noise Ratio of channel (dB)

Delay = 1;		%Delay of the multipath signal (number of samples);
			%set to 1 if no multipath
MultiMag = 1/sqrt(2);	%Magnitude of the reflection

OverSamp = 1;		%Time Oversampling of the simulation so that subchip
			%timing can be simulated.

NoRep = 10;		%Number of repeats of the experiment
RecordType = 0;		%Whether to record the rms amplitude error at the receiver
			%or to record the BER. RecordType = 0, plot vs BER
			%RecordType = 1, plot vs Amplitude Error
TimeTolerance = 0;	%Each Users is offset by a random time tolerance upto
			%TimeTolerance number of samples. Using this appears
			%to lower the BER. The reason for this is unknown.
filename = 'result.txt';	%Filename of the file to store the results
MinX = 10;
MaxX = 30;
IncX = 20;
Xvar = 'numusers';	%Name of the variable to investigate over the X range

MinY = 4;
MaxY = 6;
IncY = 1;
Yvar = 'Delay';	  	%Name of the variable to investigate over the Y range

%Initialize the result matrix
Result = zeros(floor(((MaxY-MinY)/IncY)+1),floor(((MaxX-MinX)/IncX)+2));
Result(:,1) = [ MinY:IncY:MaxY]';	%Set up the axis of the results
%Result(1,:) = [0 (MinX:IncX:MaxX)];

headerstr = [Yvar];	%Header string for the output result file

for x = MinX:IncX:MaxX,
   headerstr = [headerstr ,',', Xvar ' ' num2str(x)];
	if x == 0,
		disp([Xvar, ' = ', num2str(1)]);
		eval([Xvar ' = 1;']);	%Set the variable to use to the x value
	else
		disp([Xvar, ' = ', num2str(x)]);
		eval([Xvar ' = x;']);	%Set the variable to use to the x value
	end
	for y = MinY:IncY:MaxY,
		%eval([Yvar ' = 10*log10(1/(y-1));']);	%Set the variable to use to the y value
		eval([Yvar ' = y;']);	%Set the variable to use to the y value
		disp(['   ', Yvar, ' = ', num2str(eval(Yvar))]);
		NumErr = 0;
		TotWords = 0;
		AmpErr = 0;
		for r = 1:NoRep,
			disp(['      Repeat = ', num2str(r)]);
			%=============================================
			% Set up the parameters of the cdma simulation
			%=============================================
			TotalWords = round(TotalWords / numusers)*numusers;
			Link = [TotalWords,procgain,numusers];
			%Physchan = [SNR-10*log10(64/numusers),Comp];
			Physchan = [SNR,Comp];
			Multi = zeros(Delay,1);
			Multi(1) = 1;
			Multi(Delay) = MultiMag;
			Misc = [OverSamp, linktype, TimeTolerance];

			%===================================
			%Run the simulation of the CDMA link
			%===================================

			[Summary,BaseSignal,subsignal] = cdma(Link,Physchan,Multi,Misc);
			NumErr = NumErr + Summary(3);	%Find the total errors
							%for repetitions
			AmpErr = AmpErr + Summary(2);
			%Find the total no. of words simulated over the repetitions
			TotWords = TotWords + TotalWords;
		end
		l = (x-MinX)/IncX+2;	%Find the x location for the result
		k = (y-MinY)/IncY+1;	%Find the y location
		if RecordType == 0,
			Result(k,l) = NumErr/TotWords;	%Find the BER
		else
			Result(k,l) = AmpErr/NoRep;	%Find the amplitude error
		end
	end
end
disp(['Total Time: ' num2str(toc) 'sec']);
disp(['Total FLOPS: ' num2str(flops)]);
disp(['Process Speed : ' num2str(flops/toc) ' flops/sec']);
disp(['Results saved in file: ' filename ]);

%===========================
% Save the result to a file
%===========================
savefile(filename,Result,headerstr);

