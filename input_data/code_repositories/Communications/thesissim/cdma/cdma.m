function [summary,BaseSignal,subsignal]=cdma(link,physchan,multi,misc)
%CDMA	This models a CDMA transmission, returning the error analysis
%	summary=cdma(link,physchan,misc)
%	All inputs and output from CDMA are arrays of parameters
%	describing properties of the link, channel, and other.
%	
%	LINK :	This is a row vector of the following parameters
%	1. TotalWords: Number of data words to simulate
%	2. Process Gain: This sets the CDMA chip rate gain. It is
%		also the maximum number of users.
%	3. Number Users: This is the number of users in the system
%	
%	PHYSCHAN: These describe the physical channel model to use
%	1. SNR: Signal to noise ratio (white noise) in dB.
%		Setting to >=300 gives no noise.
%	2. Peak Power Compression: Amount of clipping to apply,
%		in dB. Set to 0 for no clipping.
%
%	MULTI: FIR model to use for the multipath delay.
%		e.g. Multi=[1 0 0 0 0.5], has the direct signal
%		at time 0, and a single reflection of 50% strength
%		of the direct signal at a delay of 4 samples.
%	MISC:
%	1. Over-Sampling: This sets the amount of oversampling of
%		the time waveform, so that small time errors
%		can be modified. Oversampling effects the multipath
%		time, as the multipath is applied to the oversampled
%		signal. Oversampling increases the processing time
%		a large amount. Typically an increase of 5 times for
%		oversampling of 2, and 7 times for oversampling of 10.
%	2. Link Type: Sets whether to simulate the forward link, or the 
%		reverse link linktype = 0, forward link (using orthogonal 
%		Walsh codes) linktype = 1, reverse link (uncorrelated 
%		non-orthogonal codes) This is an optional parameter, with 
%		the default being the forward link.
%		
%
%	SUMMARY: Results of the simulation
%	1. BER: bit error rate
%	2. Number of data words in error.
%
%	Copyright (c) Eric Lawrey 1997

%===================================
%	Required external functions:
%	(They just have to be in the same directory, or in the Matlab path)
%	trancdma.m 
%		genrand.m
%		walsh.m
%	reccdma.m 
%		walsh.m
%		genrand.m
%		subsamp.m
%	zerohold.m 
%	subsamp.m 
%	genrand.m 
%	channel.m 
%	calcerr.m

%===================================
%	Modified:
%	11/7/97	Started function, based on the simtxrx.m script
%		Currently not finished the help comments or the 
%		code.
%	22/7/97 Continued work on the function.
%	28/7/97	CDMA finished and been tested, also changed it so that it doesn't
%		print the summary out by default.
%	1/8/97	Changed the allocation of sequence numbers to be random, plus
%		added the sequence number input to the function, so that
%		repeats of simulations can use random Walsh codes.
%		Changed the Simulation so the data of all users is
%		check for errors, thus speeding up the code.
%	2/8/97	Removed the option to specify a Sequence Number, also fixed
%		a bug where the seqnum to used wasn't based on the procgain
%		but had a fixed ranged of 64.
%	7/8/97	Added the link type parameter so that both the forward link and
%		reverse link can be simulated.
%	9/8/97	3:40pm	cdma.m
%		MAJOR CHANGE. The simulations were giving BER twice what was expected
%		and the use of QPSK and phase modulation was not an appropriate
%		way to model the CDMA transmission, So I modifed all the functions
%		removing the variable word size, and changed it back to just
%		amplitude modulation. I also removed some of the parameters
%		which were never being removed, such as amplitude tolerance.

TotalWords =link(1);
procgain = link(2);
numusers = link(3);

SNR = physchan(1);
Comp = physchan(2);
Multi = multi;
OverSamp = misc(1);
linktype = misc(2);	
TimeTolerance = misc(3);

%=====================
% TRANSMITTER SECTION
%=====================
if TimeTolerance ~= 0,
	BaseSignal = zeros(1,(TotalWords/numusers)*procgain*OverSamp+TimeTolerance);
else
	BaseSignal = zeros(1,(TotalWords/numusers)*procgain*OverSamp);
end

%usednum = [];
seqnumlist = randperm(procgain);

Datatx = zeros(numusers,TotalWords/numusers);

for k = 1:numusers,
	Datatx(k,:) = genrand(1,1,TotalWords/numusers,'+-');
	
	%=================================================================
	%Randomly pick seq numbers (Walsh codes) for users (no duplicates)
	%=================================================================
	%seqnum = round(rand*procgain+0.5);		%Pick a possible sequence number
   
   %while ~isempty(find(usednum==seqnum)),		%keep picking seq numbers until 
							%it is not one already used
	%	seqnum = round(rand*procgain+0.5);	%Pick a possible sequence number
	%end
   %usednum = [usednum seqnum];
   seqnum = seqnumlist(k);
	
	TimeSignal = trancdma(Datatx(k,:),procgain,seqnum,linktype);	%find the waveform
							%for one users.

	TimeSignal = zerohold(TimeSignal,OverSamp);	%Oversample the signal
	if TimeTolerance ~= 0,
		seed = rand('seed');				%Save the random seed
		rand('seed', k);				%Pick a random time offset
		RandDelay = round(rand*TimeTolerance)+1;
		rand('seed',seed);				%restore the previous seed
		TimeSignal = [zeros(1,RandDelay), TimeSignal, zeros(1,TimeTolerance-RandDelay)];
	end
	BaseSignal = BaseSignal+TimeSignal;		%Combine the signals from all the users
end

%===============
% CHANNEL MODEL
%===============
BaseSignal = channel(BaseSignal, Comp, SNR, Multi);	%Apply the channel model to the signal

if TimeTolerance == 0,
	BaseSignal = subsamp(BaseSignal,OverSamp);		%Remove any oversampling
end
%==================
% RECEIVER SECTION
%==================
TotalErr = 0;
AmpErr = 0;
%Receive each users data separately then average the results (BER, AmpErr) over all users
for k = 1:numusers,
	if TimeTolerance ~= 0,
		seed = rand('seed');				%Save the random seed
		rand('seed',k);					%Pick a random time offset
		RandDelay = round(rand*TimeTolerance)+1;
		rand('seed',seed);				%restore the previous seed
		TimeSignal = BaseSignal(RandDelay:length(BaseSignal)+RandDelay-TimeTolerance);
		TimeSignal = subsamp(TimeSignal,OverSamp);		%Remove any oversampling
		[Datarx, subsignal] = reccdma(TimeSignal,procgain,seqnumlist(k),linktype);
	else
		[Datarx, subsignal] = reccdma(BaseSignal,procgain,seqnumlist(k),linktype);
	end
	err = calcerr(Datatx(k,:),Datarx,subsignal);
	TotalErr = TotalErr + err(3);		%Find the total number of data errors
	AmpErr = AmpErr + err(2);		%RMS Amplitude Error
end
BER = TotalErr/(round(TotalWords/numusers)*numusers);
AmpErr = AmpErr/numusers;
summary=[BER AmpErr TotalErr];