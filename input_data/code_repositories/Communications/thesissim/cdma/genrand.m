function Datatx = genrand(NumSymb,wordsize,NumCarr,Type)
%GENRAND Generates random data to transmit
%	 Datatx = genrand(NumSymb,wordsize,NumCarr)
%	 Generates a data set with NumSymb number of rows, and NumCarr
%	 number of columns.
%	 genrand(NumSymb,wordsize,NumCarr,'+-') option shifts the
%	 data so that it is negative & positive instead of 0 up.
%	 e.g. if wordsize = 1, without '+-' data = [ 1 0 1 0 1 1 ]
%			       with '+-' data = [1 -1 1 -1 1 1]

%Modified:
%6/7/97	 Setup to generate data suitable for the COFDM simulation and
%	 the CDMA. The '+-' parameter was added. (Tested to work)

Datatx = floor(rand(NumSymb,NumCarr)*(2^wordsize));
if nargin == 4,
	if Type == '+-',
		Datatx = Datatx-(2^(wordsize-1));
		index = find(Datatx>=0);
		Datatx(index) = Datatx(index)+1;
	end
end


