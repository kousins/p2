function Y=subsamp(X,N)
%SUBSAMP Subsamples the input X by an amount N, using averaging
%	 Y=subsamp(X,N)
%	 The samples are subsampled as rows. Thus if X is a matrix
%	 each row is subsampled.
%	 If the length of X is not a multiple of N samples then remaining
%	 samples are ignored.

%Modified:
%	10/7/97	Started function. This function is finished and tested.

	X=X(:,1:floor(size(X,2)/N)*N);
if (size(X,2) > 1)&(N>1),
	Y = zeros(size(X,1),size(X,2)/N);
	for k = 1:size(X,1),
		Y(k,:)=mean(reshape(X(k,:),N,size(X,2)/N));	
	end
else	
	Y=X;
end