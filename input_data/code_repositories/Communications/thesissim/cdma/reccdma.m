function [datarx, subsignal] = recCDMA(TimeSignal,procgain,seqnum,linktype)
%RECCDMA Decodes a COFDM time waveform back into its data
%	Datarx = receive(TimeSignal,procgain,seqnum,wordsize)
%
%	INPUTS:
%	========
% 	TimeSignal : This is the input time signal for the COFDM waveform. The format
%		     of the output is a row vector.
%	procgain   : Process gain of the CDMA signal.
%	seqnum     : Sequence number of the pseudo random sequence used to spread the
%		     signal.
%	linktype   : Sets whether to simulate the forward link, or the reverse link
%		     linktype = 0, forward link (using orthogonal Walsh codes)
%		     linktype = 1, reverse link (uncorrelated non-orthogonal codes)
%		     This is an optional parameter, with the default being
%		     the forward link.
%				
%	OUTPUTS:
%	========
%	Datarx     : This is the output data that has been decoded from the 'TimeSignal'
%	      	     Its format is in words the same size as 'wordsize'. Each row of 
%	      	     Datarx is the data from one symbol.
%
%	Copyright (c) Eric Lawrey 1997
%
%	See:	TRANSMIT, WRFILE, CHANNEL

%===================================
%	Required external functions:
%	(They just have to be in the same directory, or in the Matlab path)
%	walsh.m
%	genrand.m
%	subsamp.m


%======================
%	Modifications:
%	16/6/97	Started working on the function, it isn't finished yet.
%	17/6/97 Continued work on the receive function, it is now mostly finished
%		and has been partly tested.
%	18/6/97	Changed the way thay the DiffPhTx is generated, so that the phase out is based
%		on the phase locations. This was done so the phase error can be easily 
%		calculated. Negative phase errors centered around the 0deg phasor are no
%		longer 359.6 deg for example but -0.4 deg. Fixed a bug due to having 
%		guardtype = 0
%	6/7/97	Modified from the COFDM receive function to a CDMA receiver. Finished and tested
%	9/7/97	Modified to handle QPSK reception. The QPSK reception is not yet complete,
%		most of the code needs a work over so that is will handle the complex numbers
%		for QPSK.
%	10/7/97	Finished & tested the code, which now works for any word size.
%	7/8/97	Added the link type parameter so that both the forward link and
%		reverse link can be simulated.
%	9/8/97	3:40pm	reccdma.m
%		MAJOR CHANGE. The simulations were giving BER twice what was expected
%		and the use of QPSK and phase modulation was not an appropriate
%		way to model the CDMA transmission, So I modifed all the functions
%		removing the variable word size, and changed it back to just
%		amplitude modulation. Changed the demodulation of the received signal,
%		to averaging the signal (subsampling) the squaring it up (was the signal
%		bigger or smaller then zero).
if nargin <4,
	linktype = 0;	%Set the default link type to be the forward link
end

%=======================================================
% Generate the random sequence the transmitter used
%=======================================================
procgain = 2^(ceil(log(procgain)/log(2)));
if linktype == 0,
%=====================
%Simulate FORWARD LINK
%=====================
	W = walsh(procgain,'+-');
	W = W(seqnum,:);

	L = ceil(log(length(TimeSignal)/procgain)/log(2));	
				%Number of doublings of the walsh 
				%function to make it as long as the datatx
	randseq = W;
	for k = 1:L,
		randseq = [randseq randseq];
	end
	randseq = randseq(1:length(TimeSignal)); 	%clip the sequence back to the data length required
else
%=====================
%Simulate REVERSE LINK
%=====================
	seed = rand('seed');	%Save the random seed so it isn't modified
	rand('seed',seqnum*1321);	%Set the random based on the sequence number
	randseq = genrand(1,1,length(TimeSignal),'+-');	%generate the random seq to use
	rand('seed',seed);				%Restore the seed from before
	
end	

%=========================================================================
%Strip back the number of samples to make it a multiple of the symbol size
%=========================================================================
TimeSignal = TimeSignal(1:(length(TimeSignal)-rem(length(TimeSignal),procgain)));

DataSignal = TimeSignal.*randseq;

%===========================================================
% Decode the data by averaging the signal over a data symbol
%===========================================================
subsignal = subsamp(DataSignal,procgain);
datarx = ones(1,length(subsignal));	%initialize the received signal
index = find(subsignal<0);		%square up the received signal
datarx(index) = ones(1,length(index))*(-1);
