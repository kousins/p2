function Phases = ModPSK(Datatx,wordsize,DPSK)
%MODPSK	Modulates the data using phase shift keying (PSK)
%	Phases = ModPSK(Datatx,wordsize)
%	
%	Datatx should be a column vector. If it is a matrix then
%	each column is treated in parallel as data streams.
%
%	Phases is returned as phase angles in radians, and will be
%	the same size as Datatx.
%
%	wordsize determines the number of phase locations to use
%	e.g. wordsize = 1, BPSK, data 0-1, and phase locations of 0, 180deg
%	wordsize = 1, QPSK, data 0-3, phase locations of 0,90,180,270 deg
%
%	Phases = ModPSK(Datatx,wordsize,'diff')
%	This option modulates using differential PSK. Phases is returned
%	with one extra row at the start which is the phase reference.
%	The phase reference is randomly chosen from 0-360 degrees.
%	The default is to use absolute phase angle modulation
%
%	See : DEMODPSK
%
%	Copyright (c) Eric Lawrey July 1997

%Modified:
%	10/7/97	Setup the function based on the transmit section of the
%		COFDM transmitter. This function is finished and tested.
if nargin ~=3,
	DPSK = 'abso';		%Set default to absolute phase angles
end

if DPSK == 'diff',
	PhaseRef = round(rand(1,size(Datatx,2))*(2^wordsize)+0.5);	%generate random phase ref.
	PSKdata = zeros(size(Datatx,1)+1,size(Datatx,2));		%initalize Phase matrix
	PSKdata(1,:) = PhaseRef;		%Insert the phase reference
	for k = 1:size(Datatx,1)
		PSKdata(k+1,:)=rem((Datatx(k,:) + PSKdata(k,:)-1),(2^wordsize))+1;
	end
else
	PSKdata = Datatx;
end
Phases = PSKdata*(2*pi/(2^wordsize));
