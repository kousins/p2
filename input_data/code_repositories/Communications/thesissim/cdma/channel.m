function OutSignal = channel(TimeSignal,clipcompress,SNR,Multipath)
% CHANNEL Applies a channel model to the waveform including, noise, multipath & clipping.
%
%	OutSignal = channel(TimeSignal,clipcompress,SNR,Multipath)
%
%	The model is the simulate some of the effects of a radio channel. For this reason,
%	the effects are applied in the following order : 
%	1. Clipping  : Effect from the output power amplifier.
%	2. Noise     : Thermal noise due to the channel.
%	3. Multipath : Channel effect at the receiver.
%
%	INPUTS:
%	TimeSignal   :	Time waveform of signal to apply the channel to.
%	clipcompress :	Amount of clipping to apply to the signal, in dB
%			Peak Power of original signal / Peak Power after clipping.	
%			if no clipping is needed choos clipcompress = 0.
%	SNR	     :	SNR of the transmitted signal, in dB,
%			RMS power of original signal / RMS power of noise to be added.
%			if no noise is needed choose SNR >= 300.
%	Multipath    :	This is a vector of the magnitude and delay for each reflection.
%			This is a coefficient vector for  modelling the multipath with an
%			FIR filter. The first coefficient must be 1 if a direct signal is 
%			needed.
%			For Example : for reflections at sample times : 5 & 7 with magnitude
%			of 50% & 30% respectively of the direct signal :
%			Multipath = [1 0 0 0 0 0.5 0 0.3]
%			If no multipath effect is needed make 'Multipath' = []
%	OUTPUTS:
%	OutSignal    : Output signal after the model.
%
%	Copyright (c) Eric Lawrey 1997

%	Modifications:
%	17/6/97 : Started working on the function.
%	10/7/97 : Modified it so that it will work with a complex timesignal. This is 
%		  not finished yet as how to do it is not clear yet.

complex = ceil(imag(TimeSignal(1)));	%Test whether the timesignal is complex
%================================
%Clip the signal
%================================
if clipcompress ~= 0,
	MaxAmp = (10^(-(clipcompress/20)))*max(abs(TimeSignal));
%	if complex == 0,	%if a real timesignal	
%		TimeSignal(find(abs(TimeSignal)>=MaxAmp))=ones(1,length(find(TimeSignal>=MaxAmp)))*MaxAmp;
%		TimeSignal(find(TimeSignal<=(-MaxAmp)))=ones(1,length(find(TimeSignal<=(-MaxAmp))))*(-MaxAmp);
%		PeaktoRms = 10*log10(max(TimeSignal.^2)/(std(TimeSignal)^2));
%	else
		%Find locations where the signal is bigger then the maximum
		index=find(abs(TimeSignal)>MaxAmp);		
	        ScaleFac = abs(TimeSignal(index))/MaxAmp;
		TimeSignal(index)=TimeSignal(index)./ScaleFac;
end
%================================	
%Add noise
%================================
if SNR < 300,
	SigPow = std(TimeSignal);	%find the signal power
	NoiseFac = 10^(0-(SNR/20));
	if complex == 0,
		TimeSignal=TimeSignal+randn(1,length(TimeSignal))*SigPow*NoiseFac; 
	else
		Noise = randn(1,length(TimeSignal))*SigPow*NoiseFac+...
			randn(1,length(TimeSignal))*SigPow*NoiseFac*i;
		TimeSignal=TimeSignal+Noise; 	
	end
end

%================================
%Add multipath
%================================
if ~isempty(Multipath)
	TimeSignal = filter(Multipath,1,TimeSignal);	%add multi path to the signal
end

OutSignal = TimeSignal;