%========================
%FIGURE 17 in Thesis
%========================
%This script simulates a COFDM link with multipath signal. The
%multipath consist of a single reflection.
%Effect of multipath on COFDM signal.

%This simulates the effect of a single reflection multipath signal which
%is 3dB weaker then the direct signal. The delay is varied.
%
%The script which runs the simulation is simmulti.m. However it does not
%generate the entire plot as several places required smaller increment
%size on the multipath delay.

%Modifications:
%19/6/97 Set up the script for Multipath simulation

clear all;
flops(0);
tic;			%Measure the time it takes to run the simulation

ifftsize = 2048;
guardtype = 3;
guardtime = 0.25*ifftsize;
windowtype = 0;
CarrSpacing = 1;

NumCarr = 800;
TotalWords =32000;	%Approximate total number of words to simulate
filename = 'result.txt';	%File to store the results in

%Carriers used for a single wide COFDM channel
MidFreq = ifftsize/4;			%find the middle of the spectrum
StartCarr = MidFreq - round(((NumCarr-1)*CarrSpacing/2));
FinCarr = MidFreq + floor(((NumCarr-1)*CarrSpacing/2));

carriers = [StartCarr:CarrSpacing:FinCarr ]+1;

rep = 10;
DelayMin = 1;
DelayMax = 769;
DelayInc = 32;
MultiMag = 2^(-0.5);		%3dB down from direct signal
WordSizes = [1,2,4];
NumSizes = length(WordSizes);

headerstr = 'Delay (samples)';
BERstr = [];
PhErrstr = [];

Result = zeros(floor((DelayMax-DelayMin)/DelayInc)+1,2*NumSizes+1);
for l = 1:NumSizes,	%Loop through all word sizes (1,2,4)
	wordsize = WordSizes(l);
	disp(['wordsize: ' num2str(wordsize)]);
   BERstr = [BERstr ', BER DPSK b/Hz: ', int2str(wordsize)];
   PhErrstr = [PhErrstr ', Ph Err(deg) b/Hz: ', int2str(wordsize)];
	for k = 1:((DelayMax-DelayMin)/DelayInc)+1,
		Delay = (k-1)*DelayInc + DelayMin;
		disp(['    Delay: ' num2str(Delay)]);

		%Repeat each run several times
		for r = 1:rep
			disp(['        Repeat ', num2str(r)]);
			Datatx = genrand(round(TotalWords/NumCarr),wordsize,NumCarr);
			BaseSignal = transmit(Datatx,ifftsize,carriers,...
				 wordsize,guardtype,guardtime,windowtype);

			Multi = zeros(Delay,1);
			Multi(1) = 1;
			Multi(Delay) = MultiMag;
			BaseSignal = channel(BaseSignal, 0, 300, Multi);
			[Datarx, DiffPhRx] = receive(BaseSignal,ifftsize,carriers,...
				 wordsize,guardtype,guardtime);

			[PhError, Summary] = calcerr(Datatx,Datarx,DiffPhRx,wordsize);
	
			Result(k,1) = Delay;
			%Add up the number of errors
			Result(k,l+1) = Result(k,l+1)+Summary(3);	
			Result(k,l+NumSizes+1) = Result(k,l+NumSizes+1)+Summary(2);
		end
	end	
end
headerstr = [headerstr, BERstr, PhErrstr];
NumData=size(Datatx,1)*size(Datatx,2);		%find the total number of data word sent
Result(:,2:NumSizes*2+1) = Result(:,2:NumSizes*2+1)/rep;		%Average the std dev
Result(:,2:NumSizes+1) = Result(:,2:NumSizes+1)/(NumData);	%Find the BER

savefile(filename,Result,headerstr);
disp(['Total Time: ' num2str(toc) 'sec']);
disp(['Total FLOPS: ' num2str(flops)]);
disp(['Process Speed : ' num2str(flops/toc) ' flops/sec']);
disp(['Results stored in ' filename]);