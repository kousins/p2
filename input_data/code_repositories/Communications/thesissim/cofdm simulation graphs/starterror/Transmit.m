function BaseSignal = transmit(Datatx,ifftsize,carriers,...
		      wordsize,guardtype,guardtime,windowtype)
%TRANSMIT Generates a COFDM waveform from an input data.
%	  function BaseSignal = transmit(Datatx,ifftsize,carriers,...
%		    wordsize,guardtype,guardtime,windowtype)
%
%	  This function generates a COFDM time waveform based on the input parameters
%	  and the data given. The data is transmformed into a single frame COFDM signal. 
%	  This for the simulation of a COFDM phone system.
%	  INPUTS:
%	  ========
%	  Datatx     : Input data to transmit, in the format returned by RDFILE.
%	  ifftsize   : Size of ifft to use for generating the waveform
%	  carriers   : Which carriers to use for the transmission
%	  wordsize   : Number of bits to transmit on each carrier eg. 2 => QPSK
%		       1 => BPSK, 4 => 16PSK, 8 => 256PSK.
%		       Must be one of: 1,2,4 or 8
%	  guardtype  : What type of guard period to use
%		       Options:
%		       0 = No Guard period
%		       1 = zero level guard period
%		       2 = cyclic extension of end of symbols
%		       3 = same as 2 but with the first half of the guard period = zero
%	  guardtime  : Number of sample to use for the total guard time
%	  windowtype : Type of window to apply to the time waveform of the symbol
%		       Options: 
%		       0 = No windowing
%		       1 = Hamming window
%			
%	  OUTPUTS:
%	  ========
%	  BaseSignal : This is the output time signal for the COFDM waveform. The format
%		       of the output is a row vector.
%	  
%	  Copyright (c) Eric Lawrey 1997
%
%	  See:	RECEIVE, RDFILE.

%	Uses :
%	This function uses the following other functions:
%	-	rdfile.m : reads the input text file

%	Modifications:
%	8/6/97	Redo of the previous simulation script. (based on distort.m)
%	15/6/97	Continuing work on this function, currently not finished
%	16/6/97 Continued work on function, it is now finished and works, but not all 
%		the features have been tested.
%		I have tested the windowing and the zeroed guard period. The code has also
%		been optimized a bit and runs ~10 times faster then it used to, plus
%		it can handle much bigger files (tested upto 87kbytes, wordsize=4), in 35sec
%		It appears to work as a function.
%		The function needs to be changed so that it does not read the file directly
%		but instead get the data as input.
%	17/6/97	Modified the function so that it does read the input file from within 
%		the transmit function. This is so that the file can be read else where
%		then split up into smaller frames, then processed by transmit.
%		Fixed up some logical errors in the program, and removed the output phase
%		variable as it can be easily calculated from the data being transmitted.

%====================
% Initialization
%====================
rand('seed',3567);
NumCarr = length(carriers);	%find the number of carriers

numsymb = size(Datatx,1)+1;	%find the total number of symbols 
				%including the phase reference symbol
%========================================
% Convert to DQPSK & add phase reference
%========================================
PhaseRef = round(rand(1,NumCarr)*(2^wordsize)+0.5);	%generate random phase ref.
DPSKdata = zeros(size(Datatx,1)+1,size(Datatx,2));
DPSKdata(1,:) = PhaseRef;
for k = 1:numsymb-1
	DPSKdata(k+1,:)=rem((Datatx(k,:) + DPSKdata(k,:)-1),(2^wordsize))+1;
end
clear Datatx;

%=====================================
%Find the required spectrums
%=====================================
[X,Y] = pol2cart(DPSKdata*(2*pi/(2^wordsize)),ones(size(DPSKdata)));
CarrCmplx = X+i*Y;

NegCarriers = ifftsize-carriers+2;	%find the bins for the negative frequency carriers
TxSpectrums = zeros(numsymb,ifftsize);

for k = 1:numsymb
	%Place the carriers used into the full spectrum
	TxSpectrums(k,carriers) = CarrCmplx(k,:);
	TxSpectrums(k,NegCarriers) = conj(CarrCmplx(k,:));
end
clear NegCarriers;
%==================================
%Find the time waveform using IFFT
%==================================
BaseSignal = real(ifft(TxSpectrums'));

clear TxSpectrums;	%Save Memory

%=================================
%Window the signal
%=================================
if windowtype==1
	window = hamming(ifftsize);		%make window
	window2 = zeros(ifftsize,numsymb);
	for k = 1:numsymb-1
		window2(:,k) = window;
	end
	
	BaseSignal = window2.*BaseSignal;	%window the waveform
	clear window2;				%save memory	
	clear window;
end

%=================================
%Add a Guard Period
%=================================
if guardtype~=0
	if guardtype == 1 			%if guard period is just a zero transmission
		BaseSignal=[zeros(guardtime,numsymb);BaseSignal];
	elseif guardtype == 2
		EndSignal = size(BaseSignal,1);	%Find the number of columns in the BaseSignal
		BaseSignal=[BaseSignal((EndSignal-guardtime+1):EndSignal,:); BaseSignal];
	elseif guardtype == 3
		EndSignal = size(BaseSignal,1);	%Find the number of columns in the BaseSignal
		BaseSignal=[zeros(guardtime/2,numsymb); ...
			BaseSignal((EndSignal-guardtime/2+1):EndSignal,:); BaseSignal];
	end	
end

BaseSignal = reshape(BaseSignal,1,size(BaseSignal,1)*size(BaseSignal,2));
