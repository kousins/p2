function Datatx = genrand(NumSymb,wordsize,NumCarr)
%GENRAND Generates random data to transmit
%	 Datatx = genrand(NumSymb,wordsize,NumCarr)

Datatx = floor(rand(NumSymb,NumCarr)*(2^wordsize));