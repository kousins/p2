%NOT USED IN THE THESIS
%This script can be used to show that if the multipath delay spread is
%less guard period than no intersymbol interference occurs.
%
%This script simulates a COFDM link with multipath signal. The
%multipath consist of a single reflection. 
%This simulates the effect of a single multipath signal as the amplitude
%of the reflected signal is varied.
 
%The reflected signal has a constant delay of 400 samples, while the
%reflected signal power to direct signal power ratio is varied from
%-16dB to +16dB.

%The simulation models BPSK, QPSK, and 16PSK.

%The simulation time was 1361 sec on a pentium 166MHz with 32MB ram,
%it requires 14.28 GFlops.

%Modifications:
%19/6/97 Set up the script for Multipath simulation
%19/10/99 Updated to matlab 5.3, plus improved output result file generation

clear all;
flops(0);
tic;			%Measure the time it takes to run the simulation

ifftsize = 2048;			%FFT size used
guardtype = 3;
guardtime = 0.25*ifftsize;		%Guard period length in samples
windowtype = 0;
CarrSpacing = 1;		%Spacing between the carriers (in FFT bins)

NumCarr = 800;			%Number of data carriers (must be < ifftsize/2 due to real spectrum)
TotalWords = 32000;	%Approximate total number of words to simulate

filename = 'result.txt';	%File to store the results in

%Carriers used for a single wide COFDM channel
MidFreq = ifftsize/4;			%find the middle of the spectrum
StartCarr = MidFreq - round(((NumCarr-1)*CarrSpacing/2));
FinCarr = MidFreq + floor(((NumCarr-1)*CarrSpacing/2));

carriers = [StartCarr:CarrSpacing:FinCarr ]+1;

rep = 1;
MultiMagMin = -16;
MultiMagMax = 16;
MultiMagInc = 2;
WordSizes = [1,2,4];
Delay = 400;
NumSizes = length(WordSizes);

headerstr = 'Multipath Pow (dB)';
BERstr = [];
PhErrstr = [];
Result = zeros(floor((MultiMagMax-MultiMagMin)/MultiMagInc)+1,2*NumSizes+1);
for l = 1:NumSizes,	%Loop through all word sizes (1,2,4)
	wordsize = WordSizes(l);
   disp(['wordsize: ' num2str(wordsize)]);
   BERstr = [BERstr ', BER DPSK b/Hz: ', int2str(wordsize)];
   PhErrstr = [PhErrstr ', Ph Err(deg) b/Hz: ', int2str(wordsize)];
	for k = 1:((MultiMagMax-MultiMagMin)/MultiMagInc)+1,
		MultiMag = (k-1)*MultiMagInc + MultiMagMin;
      disp(['    MultiMag: ' num2str(MultiMag)]);
      
		MultiLev = 10^(MultiMag/20);
		%Repeat each run several times
		for r = 1:rep
			disp(['        Repeat ', num2str(r)]);
			Datatx = genrand(round(TotalWords/NumCarr),wordsize,NumCarr);
			BaseSignal = transmit(Datatx,ifftsize,carriers,...
				 wordsize,guardtype,guardtime,windowtype);

			Multi = zeros(Delay,1);
			Multi(1) = 1;
			Multi(Delay) = MultiLev;
			BaseSignal = channel(BaseSignal, 0, 300, Multi);
			[Datarx, DiffPhRx] = receive(BaseSignal,ifftsize,carriers,...
				 wordsize,guardtype,guardtime);

			[PhError, Summary] = calcerr(Datatx,Datarx,DiffPhRx,wordsize);
	
			Result(k,1) = MultiMag;
			%Add up the number of errors
			Result(k,l+1) = Result(k,l+1)+Summary(3);	
			Result(k,l+NumSizes+1) = Result(k,l+NumSizes+1)+Summary(2);
		end
	end	
end

headerstr = [headerstr, BERstr, PhErrstr];
NumData=size(Datatx,1)*size(Datatx,2);		%find the total number of data word sent
Result(:,2:NumSizes*2+1) = Result(:,2:NumSizes*2+1)/rep;		%Average the std dev
Result(:,2:NumSizes+1) = Result(:,2:NumSizes+1)/(NumData);	%Find the BER

savefile(filename,Result,headerstr);
disp(['Total Time: ' num2str(toc) 'sec']);
disp(['Total FLOPS: ' num2str(flops)]);
disp(['Process Speed : ' num2str(flops/toc) ' flops/sec']);
disp(['Results stored in ' filename]);