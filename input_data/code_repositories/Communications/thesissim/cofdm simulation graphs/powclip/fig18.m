%FIGURE 18
%Effect of signal clipping on COFDM.
%
%This simulation looks at the effect of clipping the COFDM signal
%on the error rate. This is done by comparing the bit error rate (BER)
%at various peak power compression ratios, and using different 
%transmission techniques (DBPSK, DQPSK, D16PSK).
%
%The peak power compression ratio is the ratio of the peak power of the
%signal before clipping to the peak power of the clipped signal.
%
%No noise or multipath is added to the signal.
%
%The clipping of the signal is to simulate the effect of an output
%power amplifier being over driven. By clipping the signal the peak
%to rms power of the signal is reduced, allowing a greater signal power
%to be transmitted.
%
%The simulation can be run by executing the simclip.m script in matlab 4.0.
%This then simulates the various compression ratios at the various
%transmission methods. Each sub-simulation is repeated to give a better
%accuracy. 
%
%In all 180 sub-simulations are run using a total of 1.8GFLOPS.
%It executes at ~1.4MFLOPS/sec on a Pentium 166MHz with 32MB of RAM.
%Copyright Eric Lawrey 1999

clear all;
filename = 'result.txt';
wordsize = 1;
ifftsize = 2048;
guardtype = 0;
guardtime = 0;
windowtype = 0;
CarrSpacing = 1;

NumCarr = 800;
TotalWords = 32000;	%Approximate total number of words to simulate

%Carriers used for a single wide COFDM channel
MidFreq = ifftsize/4;			%find the middle of the spectrum
StartCarr = MidFreq - round(((NumCarr-1)*CarrSpacing/2));
FinCarr = MidFreq + floor(((NumCarr-1)*CarrSpacing/2));

carriers = [StartCarr:CarrSpacing:FinCarr ]+1;

rep = 3;
ClipMin = 5;
ClipMax = 25;
ClipInc = 1;
WordSizes = [1,2,4];
NumSizes = length(WordSizes);
headerstr = 'Clipping (dB)';
BERstr = [];
PhErrstr = [];
Result = zeros(((ClipMax-ClipMin)/ClipInc)+1,2*NumSizes+1);
for l = 1:NumSizes,	%Loop through all word sizes (1,2,4)
	wordsize = WordSizes(l);
	disp(['Wordsize: ' num2str(wordsize)]);
   BERstr = [BERstr ', BER DPSK b/Hz: ', int2str(wordsize)];
   PhErrstr = [PhErrstr ', Ph Err(deg) b/Hz: ', int2str(wordsize)];
	for k = 1:((ClipMax-ClipMin)/ClipInc)+1,
		Comp = (k-1)*ClipInc + ClipMin;
		disp(['    COMP: ' num2str(Comp)]);

		%Repeat each run several times
		for r = 1:rep
			disp(['        Repeat ', num2str(r)]);
			Datatx = genrand(round(TotalWords/NumCarr),wordsize,NumCarr);
			BaseSignal = transmit(Datatx,ifftsize,carriers,...
				 wordsize,guardtype,guardtime,windowtype);
			BaseSignal = channel(BaseSignal, Comp, 300, []);
			[Datarx, DiffPhRx] = receive(BaseSignal,ifftsize,carriers,...
				 wordsize,guardtype,guardtime);

			[PhError, Summary] = calcerr(Datatx,Datarx,DiffPhRx,wordsize);
	
			Result(k,1) = Comp;
			%Add up the number of errors
			Result(k,l+1) = Result(k,l+1)+Summary(3);	
			Result(k,l+NumSizes+1) = Result(k,l+NumSizes+1)+Summary(2);
		end
	end	
end
headerstr = [headerstr, BERstr, PhErrstr];

NumData=size(Datatx,1)*size(Datatx,2);		%find the total number of data word sent
Result(:,2:NumSizes*2+1) = Result(:,2:NumSizes*2+1)/rep;		%Average the std dev
Result(:,2:NumSizes+1) = Result(:,2:NumSizes+1)/(NumData);	%Find the BER
savefile(filename,Result,headerstr);
