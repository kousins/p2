%FIGURE 19
%Effect of white noise on a COFDM transmission
%
%This simulations looks at the effect of varying the SNR on the BER.
%The SNR is varied from 0 to 20dB, while the BER is tested by passing
%96000 data words through the link and measuring the number of data word
%errors. This is done for three different transmission methods, BPSK
%QPSK, and 16PSK.
%
%To run this simulation run the Matlab 4.0 script simsnr.m, which uses
%the other mfiles in the zip file.
%
%snr.xls contains the results obtained from the simulation, plus graphs
%showing the result.
%Copyright (c) 1999 Eric Lawrey
clear all;
flops(0);
tic;

filename = 'result.txt';	%File to store the results in
wordsize = 1;
ifftsize = 2048;
guardtype = 0;
guardtime = 0;
windowtype = 0;
CarrSpacing = 1;

NumCarr = 800;
TotalWords = 64000;	%Approximate total number of words to simulate

%Carriers used for a single wide COFDM channel
MidFreq = ifftsize/4;			%find the middle of the spectrum
StartCarr = MidFreq - round(((NumCarr-1)*CarrSpacing/2));
FinCarr = MidFreq + floor(((NumCarr-1)*CarrSpacing/2));

carriers = [StartCarr:CarrSpacing:FinCarr ]+1;

rep = 4;
SNRMin = 0;
SNRMax = 25;
SNRInc = 1;
WordSizes = [1,2,4];
NumSizes = length(WordSizes);

headerstr = 'SNR (dB)';
BERstr = [];
PhErrstr = [];

Result = zeros(floor((SNRMax-SNRMin)/SNRInc)+1,2*NumSizes+1);
for l = 1:NumSizes,	%Loop through all word sizes (1,2,4)
	wordsize = WordSizes(l);
	disp(['Wordsize: ' num2str(wordsize)]);
   BERstr = [BERstr ', BER DPSK b/Hz: ', int2str(wordsize)];
   PhErrstr = [PhErrstr ', Ph Err(deg) b/Hz: ', int2str(wordsize)];
	for k = 1:((SNRMax-SNRMin)/SNRInc)+1,
		SNR = (k-1)*SNRInc + SNRMin;
		disp(['    SNR: ' num2str(SNR)]);

		%Repeat each run several times
		for r = 1:rep
			disp(['        Repeat ', num2str(r)]);
			Datatx = genrand(round(TotalWords/NumCarr),wordsize,NumCarr);
			BaseSignal = transmit(Datatx,ifftsize,carriers,...
				 wordsize,guardtype,guardtime,windowtype);
			BaseSignal = channel(BaseSignal, 0, SNR, []);
			[Datarx, DiffPhRx] = receive(BaseSignal,ifftsize,carriers,...
				 wordsize,guardtype,guardtime);

			[PhError, Summary] = calcerr(Datatx,Datarx,DiffPhRx,wordsize);
	
			Result(k,1) = SNR;
			%Add up the number of errors
			Result(k,l+1) = Result(k,l+1)+Summary(3);	
			Result(k,l+NumSizes+1) = Result(k,l+NumSizes+1)+Summary(2);
		end
	end	
end
headerstr = [headerstr, BERstr, PhErrstr];
NumData=size(Datatx,1)*size(Datatx,2);		%find the total number of data word sent
Result(:,2:NumSizes*2+1) = Result(:,2:NumSizes*2+1)/rep;		%Average the std dev
Result(:,2:NumSizes+1) = Result(:,2:NumSizes+1)/(NumData);	%Find the BER

savefile(filename,Result,headerstr);
disp(['Total Time: ' num2str(toc) 'sec']);
disp(['Total FLOPS: ' num2str(flops)]);
disp(['Process Speed : ' num2str(flops/toc) ' flops/sec']);
disp(['Results stored in ' filename]);