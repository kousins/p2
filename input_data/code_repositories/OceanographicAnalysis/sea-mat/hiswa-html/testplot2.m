dolandscape
[ub,x,y]=testhisread('test.ubot.dat');       

% create a suitable color map:
%
c=jet(32);
cmap=c(20:29,:);

%plot contours:

extcontour(x/1000,y/1000,ub.*100,...
              [0 10 20 30 40 50],'fill','label');...
colormap(cmap);...

set(gca,'xlim',[690 770]);...
set(gca,'ylim',[150 195]);...
set(gca,'aspect',[1.77777 1]);

hold on

set(gca,'tickdir','out')

titl='Bottom Orbital Velocity (cm/sec)'
title(titl);
xlabel('km');ylabel('km');

%put a wind arrow on plot:

dir=270.0;
wind=8*exp(sqrt(-1)*dir*(pi/180));

% draw a label box in which to put the wind arrow
%
%  ll=[755 180]; %lower left of label box
%  ur=[765 190]; %upper right
%  xbox=[ll(1) ur(1) ur(1) ll(1) ll(1)];
%  ybox=[ll(2) ll(2) ur(2) ur(2) ll(2)];
%  patch(xbox,ybox,'black');...

%  draw and label the wind arrow
%
  cg4=[.8 .8 .8];
  fatarrow(760,187,wind,cg4);...
  text(760,181,'Wind: 20 m/s','hor','center','fontsize',20,...
      'fontweight','bold','color',cg4);
     
