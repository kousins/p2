function [u,x,y]=read_hiswa(file);
% function [u,x,y]=read_hiswa(file);
% file = hiswa output name
% nx = number of colums in hiswa output
% ny = number of rows in hiswa output

% parameters specific to L. Pontch. run:

xorig=6.9e5;             %x-origin of FRAME output
yorig=1.5e5;             %y-origin of FRAME output
xlength=79600;           %x length of FRAME output 
ylength=43600;           %y length of FRAME output 
xdivs=199;               %numb of x divisions in FRAME output
ydivs=109;               %numb of y divisions in FRAME output

nx=xdivs+1;              %numb of x output nodes
ny=ydivs+1;              %numb of y output nodes

deltax=xlength/xdivs;    %x output grid spacing
deltay=ylength/ydivs;    %y output grid spacing


% create x and y plotting vectors:

x=xorig+[0:xdivs]*deltax;  
y=yorig+[0:ydivs]*deltay;

% read in HISWA ouput;

fid=fopen(file,'r');
if(fid==-1),
  disp('file not found')
  return
end
u=fscanf(fid,'%g');
fclose(fid);

%manipulate array so it's the right size and orientation:

u=reshape(u,nx,ny);
u=flipud(u');

% where Ub=0, set to nan so it doesn't get plotted:

ind=find(u==0);
u(ind)=u(ind)*nan;


