% Test Files for running HISWA and plotting bottom orbital velocity results with MATLAB
% 
% (tried to include all supporting m-files; let me know if I missed any)
% 
% 
% test.his            HISWA parameter file; outputs Ub with output code
%                     IDLA=1 (note that release notes with the 110.43
%                     version of HISWA mentions that IDLA=4 will now work,
%                     and is useful for MATLAB processing; I have not
%                     tested)
% 
% lpontch.bot        Lake Pontchartrain bottom elevation grid 
% 
% test.ubot.dat       sample HISWA output from test.his
% 
% 
% main m-files for plotting:
% 
% testplot.m          color raster plot example
%                     
% testplot2.m         color contour plot example (slow, but worth it)
% 
% 
% supporting m-files:
% 
% testhisread.m       read in Ub array specific to L. Pontchartrain
%                         FRAME/BLOCK output;
% 
% dolandscape.m       orients and sizes the MATLAB plot 
% 
% pslice.m            Signell's raster color plotter with scale bar
% 
% clegend4.m          Signell's color scale bar maker        
% 
% extcontour.m        programs to do color-filled, contour-labeled plots
% contourfill.m       (from R. Pawlowicz)
% contoursurf.m
% extclabel.m
%         
% fatarrow.m         Signell's wind arrow plot generator
% 
