function y=quadrat(x)

% QUADRAT(x) returns the quadratic solution for use with LINREG1.
%
% y=QUADRAT(x) returns the value
%    y = x^2 + B*x + C
% for use with LINREG1.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ver. 1: 11/25/96 (RB)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global B C
y=x.^2 + B.*x + C;
end

