%    DO Pgram 
%
%      A routine for computing, not showing, the Raw Periodogram
%      the input data file is assumed to be      -- xin --
%      the output file of the periodogram is   -- rawpgram --
%      the input file is NOT overwritten
%      no plots from this version
%
%       orig  Aug 10  90
%        2.0  dec 17  91
%
npts = length(xin);
dat  = detrend(xin);
pg1  = fft(dat);
pg2  = pg1 .* conj(pg1);
np   = fix(npts/2);
%
rawpgram = (pg2(2:(np+1)) ./ npts);
%
clear pg2
clear np
clear npts
clear pg1
clear dat
%
%    end of file


