% Extracting scalar data from NetCDF model output
% 
%   kslice     - Get horizontal slice along constant k
%   islice     - Get vertical slice along constant i
%   jslice     - Get vertical slice along constant j
%   zslice     - Get horizontal slice at constant z (interpolates)
%   depave     - Get depth-averaged field
%   ctotal     - Determine total and average amount in domain
% 
% Plotting scalar data
%
%   pslice     - Color-shaded image with color legend
%   extcon     - Contour plot with color-fill, labeling options
%
% Extracting vector data from NetCDF model output
% 
%   ksliceuv     - Get horizontal slice along constant k
%   zsliceuv     - Get horizontal slice at constant z (interpolates)
%   depaveuv     - Get depth-averaged field
%
% Plotting vector data
%
%   psliceuv     - Draws field of arrows on existing plot
%
