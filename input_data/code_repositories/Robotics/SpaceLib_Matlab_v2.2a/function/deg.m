
function g=deg(r)

%GRAD (spacelib): Conversion from radians to degrees
% 
% Usage:
% 			g=deg(r)
% 
% (c) G.Legnani 2003
%___________________________________________________________________________

g=(r*180/pi);

