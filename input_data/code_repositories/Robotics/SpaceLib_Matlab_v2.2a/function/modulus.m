
function m=modulus(v)

%MODULUS (Spacelib): Evaluates the module of a vector
% not really useful in MATLAB.
% Supported only for compatility with the c-language version
%
% renamed from mod() to modulus() on november 2001 to avoid conflict with matlab function
% Usage:
% 			m=modulus(v)
%
%___________________________________________

m=norm(v);



