function [KM,Ns,phis]=fill_kernel3D(c,Ns,phis)

%FILL_KERNEL3D calculates density estimation with fractal kernel
% function [KM,Ns,phis]=fill_kernel3D(c,Ns,phis)
% c -  matrix with counts (fwd or bck) obtained with count_repeat.m
% NS - values of N to be used (optional) default: Ns=1:10
% phis - values of phi to be used (optional) default: phis=[0.1,0.25,1:10]
% KM - 3 dimensional matrix with density values (Ns x phis x Position)
% Caculates matrix with (fracta-)kernel values given vectors of counts c
% See also: count_repeat.m, kernel_analytical.m
% Susana Vinga, svinga@algos.inesc-id.pt revision 2005.04.15
% Comments 2006.02.01

if nargin<2
    Ns=1:10;
    phis=[0.1,0.25,1:10];
end

for N=1:length(Ns)
    for phi=1:length(phis)

        k=[];
        for i=1:length(c);
            k=[k,kernel_analytical(c(i,:),Ns(N),phis(phi))];
        end

        KM(N,phi,:)=k;

    end
end

%normalize
%KMn=(KM-repmat(mean(KM,3),[1 1 2000]))./repmat(std(KM,0,3),[1,1,2000]);

%limits
% phi_inf=4^N/L * c(N-tuple i)
% phi_0=1