function KMn=normKM(KM,opt)

% function KMn=normKM(KM,opt)
% Normalize matrix KM
% Susana Vinga, svinga@algos.inesc-id.pt 2006.02.01

[a1,a2,L]=size(KM); %a1 -> Ns; a2 -> phis ; L -> length seqs

if nargin<2
    opt=1
end

if opt==1
    KMn=(KM-repmat(mean(KM,3),[1 1 L]))./repmat(std(KM,0,3),[1,1,L]);
elseif opt==2
    %ignore first N positions
    KM=KM(:,:,10:end);
    KMn=(KM-repmat(mean(KM,3),[1 1 L-10+1]))./repmat(std(KM,0,3),[1,1,L-10+1]);
    size(KMn)
    KMn=cat(3,zeros(a1,a2,9),KMn);
else
    disp('Other options not yet available - using option 1');
    KMn=(KM-repmat(mean(KM,3),[1 1 L]))./repmat(std(KM,0,3),[1,1,L]);
end

    


