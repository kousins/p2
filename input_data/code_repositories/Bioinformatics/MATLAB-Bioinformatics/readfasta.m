function fasta=readfasta(txt)

% READFASTA reads fasta files and extracts sequence to fasta variable
%
% Syntax fasta=readfasta(txt)
% Reads text file with sequences
% in the format:
%
% >> Global title / descriptor (optinal line)
% > qualifier for 1st sequence
% .....
% .....  (1st sequence)
% .....
% > qualifier for 2nd sequence
% .....
% .....  (2nd sequence)
% .....
%
%  (...)
%
% > qualifier for nth sequence
% .....
% .....  (nth sequence)
% .....
%
%{the only reserved character in this format is ">"}
%
% fasta is a structured variable of the type:
% fasta.title = description of the dataset
% fasta.legend = sequence descriptions
% fasta.sequence = sequences

% Jonas Almeida, MUSC, 26 Jan 2002

fid=fopen(txt,'r');

% READS TITLE (global qualifier), if it exists

linha=fgetl(fid);i=0;
fasta.title=['source file: ',txt];
if strncmp(linha,'>>',2)*length(linha)>2
   fasta.title=linha(3:end);
elseif strncmp(linha,'>',1) % otherwise retrieve the 1st legend
   i=i+1;fasta.legend{i}=linha(2:end);fasta.sequence{i}='';
else
   error('this is not fasta formated, there is no ''>'' qualifier')
end

% READS SEQUENCES AND THEIR LEGENDS

while feof(fid)==0
   linha=fgetl(fid);
   if strncmp(linha,'>',1)
      i=i+1;fasta.legend{i}=linha(2:end);fasta.sequence{i}='';
   else
      fasta.sequence{i}=[fasta.sequence{i},linha];
   end
end

fclose(fid);
