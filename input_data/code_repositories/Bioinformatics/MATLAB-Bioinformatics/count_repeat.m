function [cf,cb]=count_repeat(seq)

%COUNT_REPEAT counts L-tuple repetitions for each position in sequences
% function [cf,cb]=count_repeat(seq)
% Counts forward and backward repetitions of L-tuple for each position
% seq -> original string sequence
% cf -> matrix (triangular inferior) w/ forward counting
% cb -> matrix (triangular superior w/ backward counting
% EXAMPLE:
% seq='GTACACC'
% cf =
% 
%      1     0     0     0     0     0     0
%      1     1     0     0     0     0     0
%      2     1     1     0     0     0     0
%      3     2     1     1     0     0     0
%      2     1     1     1     1     0     0
%      3     2     1     1     1     1     0
%      3     1     1     1     1     1     1
% 
% 
% cb =
% 
%      1     1     1     1     1     1     1
%      1     1     1     1     1     1     0
%      2     2     1     1     1     0     0
%      3     1     1     1     0     0     0
%      2     2     1     0     0     0     0
%      3     1     0     0     0     0     0
%      3     0     0     0     0     0     0
%
% See also: kernel_analytical.m
% Susana Vinga created:2005/02/03
% Comments 2006.02.01; !implement a cutting parameter to create a NxR
% matrix instead (R<<N)
% svinga@algos.inesc-id.pt

%substitute with strfind
%K = STRFIND(TEXT,PATTERN)

N=length(seq);
cf=zeros(N,N);
disp('Forward counts...');
for i=1:N   %for each position p in the sequence
    %disp(['position ' num2str(i)]);
    for j=1:i
          cf(i,j)=length(strfind(seq,seq( (i-j+1):i) ));
    end
end
  
seq=seq(end:-1:1);  %inverter a sequencia
cb=zeros(N,N);
disp('Backward counts...');
for i=1:N   %for each position p in the sequence
    %disp(['position ' num2str(i)]);
    for j=1:i
          cb(i,j)=length(strfind(seq,seq( (i-j+1):i) ));
    end
end

cb=cb(end:-1:1,:);

% %ATEN��O:ISTO N�O FUNCIONA POR CAUSA DOS OVERLAPS! REGEXP('AAA','AA') SO
% %DEVOLVE 1
% N=length(seq);
% cf=zeros(N,N);
% for i=1:N   %for each position p in the sequence
%     disp(['position ' num2str(i)]);
%     for j=1:i
%           cf(i,j)=length(regexp(seq,seq( (i-j+1):i) ));
%     end
% end
%   
% seq=seq(end:-1:1);  %inverter a sequencia
% cb=zeros(N,N);
% for i=1:N   %for each position p in the sequence
%     disp(['position ' num2str(i)]);
%     for j=1:i
%           cb(i,j)=length(regexp(seq,seq( (i-j+1):i) ));
%     end
% end
% 
% cb=cb(end:-1:1,:);
% 
