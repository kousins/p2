function k=kernel_analytical(counts,N,phi)

%KERNEL_ANALYTICAL closed form for weighted-step kernel calculation
% function k=kernel_analytical(counts,N,phi)
% Analytically calculates the weighted-step kernel density for a given
% position in the DNA sequence
% counts -> counting profile for a given position, vector with 1 to L-tuple
%           counting, for example GTACACC and position 4 counts=(3,2,1,1)
% N -> number of steps in the kernel definition
% phi -> relation between step volumes phi=V(i)/V(i-1) if phi->inf only
%        weigths N-tuple counts
% k -> density value for the requested position
%
% See also: count_repeat.m (to obtain variable counts); kernel.density.doc (demostration details) 
% Susana Vinga svinga@algos.inesc-id.pt 2005/02/03
% Comments 2006.02.01

L=length(counts);    %MUST BE length of sequence

if length(counts) < N
    warning('Length of vector counts is smaller than N --- changing to length(counts)');
    N=length(counts);
end

e = (4*phi).^(1:N) .* counts(1:N);
f = phi.^(0:N);

k=( 1 + 1/L * sum(e) ) / sum(f);

