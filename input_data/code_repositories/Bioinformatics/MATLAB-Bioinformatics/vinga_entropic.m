function vinga_entropic(fastafile,N,phi,position)

%VINGA_ENTROPIC is the main function of the toolbox for processing sequences
%Syntax: vinga_entropic(fastafile,N,phi,position)
%Description: this is the main function that calls all others and is the
%right function to compile for sequence processing. The numerical results,
%figures and intermediate calculations will be stored with names built
%from the fasta file name. For example, if the fastafile is m4.seq, all
%graphs and figures will be saved as m4_*.pdf and m4_*.png.
%****Input arguments:
%   fastafile - text file with sequence to be analyised (in FASTA format)
%   N - kernel resolution parameter 
%   phi - kernel smoothing parameter
%   position - local study of particular symbol in the original sequence
%****Output:
%   figure files with all the graphical results (see webpage)
%   XML page with numerical data
%
% EXAMPLE: vinga_entropic('m4.seq',6,12,253)
%
% NOTE: The compiled version takes char inputs, therefore the type conversion is
% checked and corrected if needed
%-----------------------------------------------------------------
% Authors: Susana Vinga and Jonas S Almeida
% Reference: "Local R�nyi entropic profiles of DNA sequences"
%             BMC Bioinformatics (submitted)
% Version: 2007.08.27
% Webpage: http://algos.inesc-id.pt/~svinga/ep/
%-----------------------------------------------------------------

if ischar(N);N=str2num(N);compiled=1;else compiled=0;end
if ischar(phi);phi=str2num(phi);end
if ischar(position);position=str2num(position);end

%disp(fastafile)
%disp(class(fastafile))
%disp(N)
%disp(class(N))
%disp(phi)
%disp(class(phi))
%disp(position)
%disp(class(position))

%Extract sequence name as a template to name intermediate results
seq_name=regexprep(fastafile,'(.+)\.([^\.]+)','$1');
seq_name=regexprep(seq_name,'\W','_');


%find out if the intermediate results are available
%make sure to delete them if they are to be recalculated for the same
%sequence or for a sequence with the same name

if exist([seq_name,'_kernel3D_cf.mat'])==2 %intermediate results available
    disp(['intermediate results were found (',seq_name,'_kernel3D_cf.mat)'])
    disp('   for a sequence with the same name and will now be retrieved.')
    load([seq_name,'_kernel3D_cf.mat'])
else

    %%Read fasta file
    fasta=readfasta(fastafile);

    %Concatenate sequences
    seq=[];
    for i=1:length(fasta.sequence);
        seq=[seq,fasta.sequence{i}];
    end
    L=length(seq);disp(['nucleotides: ',num2str(L)]);disp('Calculating Kernel distribution function ...')
    kernel3D_cf.L=L;
    

    %%Count all the repetitions, forward and backward

    [cf,cb]=count_repeat(seq);

    % Forward counts...
    % Backward counts...

    %%Calculate the density values for each position and show example

    [kernel3D_cf.KM,kernel3D_cf.Ns,kernel3D_cf.phis]=fill_kernel3D(cf); %use default parameters (forward counts)
    kernel3D_cf.fasta=fasta;
    kernel3D_cf.seq=seq;
    save([seq_name,'_kernel3D_cf'],'kernel3D_cf')
    disp(['save results also as XML: ',seq_name,'_kernel3D_cf.xml']);
    xml=mat2xml(kernel3D_cf,[seq_name,'_kernel3D_cf']);
    fid=fopen([seq_name,'_kernel3D_cf.xml'],'w');
    fprintf(fid,'%s',xml);
    fclose(fid);
    
    
end
KM=kernel3D_cf.KM;
Ns=kernel3D_cf.Ns;
phis=kernel3D_cf.phis;
fasta=kernel3D_cf.fasta;
L=kernel3D_cf.L;
seq=kernel3D_cf.seq;
%study a particular N=6 for all phis --> input variable
figure;
%plot(squeeze(KM(6,:,:))','.-');
%plot(squeeze(KM(N,:,:))','.-'); %does not work, don't know why
plot(squeeze(KM(N,:,:))');
xlabel('position');
ylabel('kernel density');
%title([fasta.title '  N=6; \phi variable']);
title([fasta.title '  N=' num2str(N) '; \phi variable'])
legend(num2str((1:10)'),2);

disp('saving plot of density as a function of phi in png and pdf format')
try
    print('-dpng',[seq_name,'_phi_variable.png'])
    print('-dpdf',[seq_name,'_phi_variable.pdf'])
catch
    disp(lasterr)
end
% %study KM=f(pos,Ns) for fixed phis
% for i=1:12;
% figure
% plot(squeeze(KM(:,i,:))','.-');
% xlabel('position');
% ylabel('kernel density');
% title([fasta.title '  \phi=' num2str(i) '; N variable']);
% legend(num2str((1:10)'),2);
% end

%study KM=f(pos,Ns) for fixed phis

figure
%plot(squeeze(KM(:,12,:))','.-');
%plot(squeeze(KM(:,phi,:))','.-');%doesn�t work, don't know why
plot(squeeze(KM(:,phi,:))');
xlabel('position');
ylabel('kernel density');
%title([fasta.title '  \phi=' num2str(12) '; N variable']);
title([fasta.title '  \phi=' num2str(phi) '; N variable']);
legend(num2str((1:10)'),2);

disp('saving plot of density as a function of position with N variable in png and pdf format')
try
    print('-dpng',[seq_name,'_N_variable.png'])
    print('-dpdf',[seq_name,'_N_variable.pdf'])
catch
    disp(lasterr)
end
%%Normalize KM for each pair (N,phi)

KMn=normKM(KM,1);

%%Find the scale where quantile values are extreme and plot results

[pos,code]=find_scale(KMn,Ns,phis);
%...and plot
% figure;plot(pos,'.-');legend(code);xlabel('position');ylabel('kernel density')
% %partial plots
% figure;plot(pos(:,[1,4]),'.-');legend(code([1,4]));xlabel('position');ylabel('kernel density')
% figure;plot(pos(:,[2,5]),'.-');legend(code([2,5]));xlabel('position');ylabel('kernel density')
% figure;plot(pos(:,[3,6]),'.-');legend(code([3,6]));xlabel('position');ylabel('kernel density')

figure;plot(pos);legend(code);xlabel('position');ylabel('kernel density')

%partial plots
figure;plot(pos(:,[1,4]));legend(code([1,4]));xlabel('position');ylabel('kernel density')
figure;plot(pos(:,[2,5]));legend(code([2,5]));xlabel('position');ylabel('kernel density')
figure;plot(pos(:,[3,6]));legend(code([3,6]));xlabel('position');ylabel('kernel density')



%%Study one particular position , for example p=253

%local_study(253,KMn,phis,Ns,pos,L,seq,KM);
local_study(position,KMn,phis,Ns,pos,L,seq,KM);


% F =
%
%     1.2438
%
%
% Fn =
%
%    -2.3430
fnum=gcf-2;
figure(fnum)
disp('saving plot of density as a function of position for maximum values of KMn in png and pdf format')
try
    print('-dpng',[seq_name,'_maxKMn.png'])
    print('-dpdf',[seq_name,'_maxKMn.pdf'])
catch
    disp(lasterr)
end

fnum=fnum+1;
figure(fnum)
disp('saving plot of density as a function of position detail (MAX) in png and pdf format')
try
    print('-dpng',[seq_name,'_DetailMax.png'])
    print('-dpdf',[seq_name,'_DetailMax.pdf'])
catch
    disp(lasterr)
end

fnum=fnum+1;
figure(fnum)
disp('saving plot of density as a function of position detail (MIN) in png and pdf format')
try
    print('-dpng',[seq_name,'_DetailMin.png'])
    print('-dpdf',[seq_name,'_DetailMin.pdf'])
catch
    disp(lasterr)
end

if compiled;close all;exit;end

