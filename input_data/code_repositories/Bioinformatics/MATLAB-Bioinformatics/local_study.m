function local_study(p,KMn,phis,Ns,pos,L,seq,KM)

% function local_study(p,KMn,phis,Ns,pos,L,seq,KM)
% Susana Vinga, svinga@algos.inesc-id.pt 2006.02.03
%correct comment 2006.11.23
%comment F and Fn 2007.08.27

LNs=length(Ns); %default 10
Lphis=length(phis); %default 12

%p = input('Which position? ')
subplot(2,2,1);
%plot(KMn(:,:,p),'.-');
plot(KMn(:,:,p));

title(['Position ' num2str(p)]);
xlabel('N');
legend(num2str(phis'));axis([1 LNs -inf inf])
hold on;plot([1 LNs],[1 1],'--');plot([1 LNs],[0,0],'b--');plot([1 LNs],[-1,-1],'b--');hold off;

subplot(2,2,2);
plot(phis,KMn(:,:,p)','.-');
title(['Position ' num2str(p)]);
xlabel('\phi');
legend(num2str(Ns'));
hold on;plot([0 LNs],[1 1],'--');plot([0 LNs],[0,0],'b--');plot([0 LNs],[-1,-1],'b--');hold off;

subplot(2,2,3);
%N maximo pos(p,2)
%phi maximo pos(p,3)
%posicoes: find(pos(p,2)==Ns)
%          find(pos(p,3)==phis)
%
%plot(squeeze(KMn(Ns(find(pos(p,2)==Ns)),phis(find(pos(p,3)==phis)),:)),'.-');
% Add local maxima? useful for motifs with errors! localmax.m
% localmax(KMn(:,4,56)) %spans all Ns for maximum phi
plot(squeeze(KMn(find(pos(p,2)==Ns),find(pos(p,3)==phis),:)),'.-');
%hold on; plot(p,pos(p,1),'r.');hold off;
%correct point to KMn
hold on; plot(p,KMn(find(pos(p,2)==Ns),find(pos(p,3)==phis),p),'r.');hold off;

prc=sum( squeeze(KMn(find(pos(p,2)==Ns),find(pos(p,3)==phis),:)) <= KMn(find(pos(p,2)==Ns),find(pos(p,3)==phis),p))/L;    %quantile
title(['Entropic profile for maximum values (N=' num2str(pos(p,2)) ',\phi=' num2str(pos(p,3)) ') QUANTILE=' num2str(prc*100)]);
xlabel('position')

subplot(2,2,4);
plot(squeeze(KMn(find(pos(p,5)==Ns),find(pos(p,6)==phis),:)),'.-');
hold on; plot(p,pos(p,4),'r.');hold off;
prc=sum( squeeze(KMn(find(pos(p,5)==Ns),find(pos(p,6)==phis),:)) <= KMn(find(pos(p,5)==Ns),find(pos(p,6)==phis),p))/L;    %quantile
title(['Entropic profile for minimum values (N=' num2str(pos(p,5)) ',\phi=' num2str(pos(p,6)) ') QUANTILE=' num2str(prc*100)]);
xlabel('position')


figure;
plot(pos,'.-');legend({'KMn_{max}' 'N_{max}' 'phi_{max}' 'KMn_{min}' 'N_{min}' 'phi_{min}'});
title('Entropic profile for maximum values of KMn');
xlabel('position');

%detail
figure
len=pos(p,2);   %motif length
plot((p-len+1):p,pos( (p-len+1):p ,:,:),'.-',(p-len+1):p ,repmat(0,1,len),'b--');
h=get(gcf,'Children');
set(h,'XTick',p-len+1:p);   %ajust tick marks...
set(h,'XTickLabel',seq(p-len+1:p)') %...and labels
%find compond value
% (p-len+1):p
% KM(len,find(phis==pos(p,3)),(p-len+1):p)
F=prod(KM(len, find(phis==pos(p,3)),(p-len+1):p));
Fn=prod(KMn(len, find(phis==pos(p,3)),(p-len+1):p));
title(['Entropic profile DETAIL. Positions ' num2str(p-len+1) ':' num2str(p) ' @ N=' num2str(len) ' \phi=' num2str(pos(p,3)) ' F=' num2str(F) ' Fn=' num2str(Fn)]);
legend({'KMn_{max}' 'N_{max}' 'phi_{max}' 'KMn_{min}' 'N_{min}' 'phi_{min}'},2);
hold on;plot(p,pos(p,1),'ro',p,pos(p,4),'ro');hold off


%detail
figure
len=pos(p,5);   %motif length - under-represented
plot((p-len+1):p,pos( (p-len+1):p ,:,:),'.-',(p-len+1):p ,repmat(0,1,len),'b--');
h=get(gcf,'Children');
set(h,'XTick',p-len+1:p);
set(h,'XTickLabel',seq(p-len+1:p)');
title(['Entropic profile DETAIL - minimum. Positions ' num2str(p-len+1) ':' num2str(p) ' @ N=' num2str(len) ' \phi=' num2str(pos(p,6))]);
legend({'KMn_{max}' 'N_{max}' 'phi_{max}' 'KMn_{min}' 'N_{min}' 'phi_{min}'},2);
hold on;plot(p,pos(p,1),'ro',p,pos(p,4),'ro');hold off


