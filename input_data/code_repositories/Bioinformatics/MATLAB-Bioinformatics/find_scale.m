function [pos,code]=find_scale(KM,Ns,phis)

% function [pos,code]=find_scale(KM,Ns,phis)
% Find the scale where maxima and minima of KM occur
% Susana Vinga, svinga@algos.inesc-id.pt 2006.02.01


[a1,a2,L]=size(KM); %a1 -> Ns; a2 -> phis ; L -> length seqs

for p=1:L;
    mx=max(max(KM(:,:,p)));
    mn=min(min(KM(:,:,p)));
    [x,y]=find(mx==KM(:,:,p));
    [xn,yn]=find(mn==KM(:,:,p));
%     size(KM(x,y,p))
%     size(Ns(x))
%     size(phis(y))
%correct later - finds multiple solutions for KMn opt=2, extract the first
x=x(1);
y=y(1);
xn=xn(1);
yn=yn(1);
%
    pos(p,1:6)=[KM(x,y,p),Ns(x),phis(y),KM(xn,yn,p),Ns(xn),phis(yn)];
    
%     %add new columns
%     if p-Ns(x)+1<1; Ns(x)=p; end
%     F=prod(KM(x,y,(p-Ns(x)+1):p))
%     Fn=prod(KMn(x,y,(p-Ns(x)+1):p))
%     pos(p,7:8)=[F,Fn];
end;
code={'KM_{max}' 'N_{max}' 'phi_{max}' 'KM_{min}' 'N_{min}' 'phi_{min}'};