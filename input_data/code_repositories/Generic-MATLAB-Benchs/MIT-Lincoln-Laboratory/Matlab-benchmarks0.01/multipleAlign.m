% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function MA = multipleAlign(A, S, GA, misPenalty, gapPenalty)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function multipleAlign() - Kernel 5 - Multiple Sequence Alignment.
%
% This function uses the 'center star' method for 'sum of pairs' multiple
% alignment, as described in Gusfield, Algorithms on Stings, Trees, and
% Sequences, pg 348.  Using the alginment score data from Kernel 4, it finds
% an ideal center sequence, one which minimizes the sum of its score versus
% all of the other sequences.  (The sequences are sorted by those scores to
% clarify the results.)  Then each sequence is aligned with the center
% sequence and the alignments are merged by adding extra spaces as necessary.
%
% This function uses the same variant of the Smith-Waterman dynamic
% programming algorithm as is used in Kernel 4, but includes the tracepath
% function which generates an actual alignment.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% A                - [structure] including results of kernel 1 and kernel 2
%   simMatrix      - [structure] codon similarity created by genSimMatrix()
%     similarity   - [2D array int8] 1-based codon/codon similarity table
%     aminoAcid    - [1D char vector] 1-based codon to aminoAcid table
%     bases        - [1D char vector] 1-based encoding to base letter table
%     codon        - [64 x 3 char array] 1-based codon to base letters table
%     encode       - [uint8 vector] aminoAcid character to last codon number
%     hyphen       - [uint8] encoding representing a hyphen (gap or space)
%     exact        - [integer] value for exactly matching codons
%     similar      - [integer] value for similar codons (same amino acid)
%     dissimilar   - [integer] value for all other codons
%     gapStart     - [integer] penalty to start gap (>=0)
%     gapExtend    - [integer] penalty to for each codon in the gap (>0)
%     matchLimit   - [integer] longest match including hyphens
% S                - [structure array] similar sequence sets for each bestSeq
%   bestStarts     - [Mx2 integer] main/match startpoints
%   bestEnds       - [Mx2 integer] main/match endpoints
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
%   bestScores     - [Mx1 integer] the scores for the bestSeqs
% GA               - [1-D Data structure] alignments for set g
%   globalScores   - [2-D integer array] alignment score (x,y)
% misPenalty       - [integer] penalty for each base/base mismatch
% gapPenalty;      - [integer] penalty for each space in a gap
%
% OUTPUT
% MA               - [1-D Data structure] alignments for set g
%   scores         - [1-D integer vector] score for each alignment
%   alignments     - [1-D cell array of strings] final alignments
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: multipleAlign.m,v 1.10 2005/08/19 16:05:22 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

disMatrix = ~eye(4) * misPenalty;       % the distance function s
hyphen = A.simMatrix.hyphen;
bases = A.simMatrix.bases;
space = '-';

codon2bases(1,:) = floor([0:63] / 16) + 1;
codon2bases(2,:) = mod(floor([0:63] / 4), 4) + 1;
codon2bases(3,:) = mod([0:63], 4) + 1;

% For each set of similar sequences g in S
for g = 1:length(S)
    M = length(S(g).bestScores);
    alignment = cell(M,1);              % preallocate alignment vector
    if M == 0
        MA(g).alignment = alignment;
        MA(g).scores = [];
        continue;                       % if no data was found
    end
    seqs = S(g).bestSeqs;
    globalScores = GA(g).globalScores;
    
    sumScores = sum(globalScores);      % sum over all sequence matches
    [unused centers] = min(sumScores);  % find the minimum(s)
    center = centers(1);                % use the first as the center
    [MA(g).scores order] = sort(globalScores(center,:));

    C = seqs{center,1};
    C = C(find(C ~= hyphen));            % remove any hyphens
    alignment{1} = reshape(A.simMatrix.codon(C,:).', 1, []);

    % Align each other sequence with the center sequence.
    for x = 2:M
        [xAligned cAligned gs] = alignPair(codon2bases, gapPenalty, disMatrix, hyphen, space, bases, seqs{order(x),1}, C);
        if gs ~= MA(g).scores(x)        %DEBUG
            error('multipleAlign score differs from globalAlign score');
        end

        i = 1;                          % add spaces to make the centers match
        while ~strcmp(alignment(1), cAligned)
            if i > length(cAligned)
                flag = true;            % extend cAligned
            elseif i > length(alignment{1})
                flag = false;           % extend alignment table
            elseif alignment{1}(i) == cAligned(i)
                i = i + 1;
                continue;               % continue to find a mismatch
            else
                flag = alignment{1}(i) == space;
            end
            
            if flag
                cAligned(i:end+1) = [space cAligned(i:end)];
                xAligned(i:end+1) = [space xAligned(i:end)];
            else
                for j = 1:x-1
                    alignment{j}(i:end+1) = [space alignment{j}(i:end)];
                end
            end
        end
        alignment{x} = xAligned;
    end
    MA(g).alignment = alignment;
end


% --------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function alignPair() - Compute the global alignment for two sequences
%
% INPUT
% X                - [char string] first codon sequence
% Y                - [char string] second codon sequence
%
% OUTPUT
% xA               - [char string] global alignment of X with Y
% yA               - [char string] global alignment of Y with X
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [xA, yA, score] = alignPair(codon2bases, gapPenalty, disMatrix, hyphen, space, bases, X, Y)
X = X(find(X ~= hyphen));
Xb = reshape(codon2bases(:,X), 1, []);  % unpack X
n = length(Xb);

Yb = reshape(codon2bases(:,Y), 1, []);  % unpack Y
m = length(Yb);
T = zeros(n, m, 'uint8');               % preallocate traceback tree

V = gapPenalty * [1:m];                 % previous row's best score
F = V;

% Note: possibly this loop can be speeded up (compare Kernel 4), but at least
% this version is more or less parallel to the version in Kernel 3, without
% the gapStart penalty.

% For each Xb base
for i = 1:n
    G = double(disMatrix(Xb(i), Yb)) + [gapPenalty*(i-1) V(1:m-1)];
    V = min(F, G);                      % best score ending with this pair
    
    E(1) = V(1) + gapPenalty;
    for j = 2:m                         % for each Yb base
        E(j) = min(E(j-1), V(j-1)) + gapPenalty;
        V(j) = min(E(j), V(j));
    end

% D.Vh(i,:) = V; D.Eh(i,:) = E; D.Fh(i,:) = F; D.Gh(i,:) = G; %DEBUG
    T(i,:) = 4*(V == E) + 8*(V == F) + 16*(V == G);
    Vg = V + gapPenalty;
    F = min(F + gapPenalty, Vg);
    T(i,:) = T(i,:) + uint8([E(2:m) 0] == Vg) + 2*uint8(F == Vg);
end
score = V(m);

% Use the trace table to find the actual sequences with gaps including gaps.
[rs xA yA] = tracepath(space, bases, T, Yb, Xb, n, m, 0);
if length(rs) ~= 2 || rs(2) ~= 1        %DEBUG
    error('multipleAlign tracepath bug');
end


% ------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Generate a typical matching pair.
%
% With a small change this recursive approach can be used to generate all
% possible matches.  We only need to identify a single match.  If the match is
% longer than about 100 bases, Matlab encounters a recursion limit error;
% this limit can be set higher, using "set(0, 'RecursionLimit', 200);", but it
% might be better to modify this routine to use an array to control its own
% recursion.
%
% INPUTS
%   ci,cj          [integers] subsequence end coordinates, used recursively
%   dir            [integer] 1 when extending a gap in main (down),
%                            2 when extending a gap in match (right), else 0
% OUTPUT
%   rs             [i, j] coordinates of end of path
%   ri             [uint8 row vector] main sequence
%   rj             [uint8 row vector] match sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [rs, ri, rj] = tracepath(space, bases, T, Yb, Xb, ci, cj, dir)
if ci == 0                              % if done in the i direction
    ri = repmat(space,1,cj);            % cj will usually be 0 too
    rj = bases(Yb(1:cj));
    rs = [1 1];
    return;
end
if cj == 0                              % if done in the j direction
    ri = bases(Xb(1:ci));
    rj = repmat(space,1,ci);
    rs = [1 1];
    return;
end

if dir == 0 || bitand(dir, T(ci,cj))    % if not skipping, or start of gap
    dir = bitshift(T(ci,cj), -2);       % fan out from this point
end

ri = [];
rj = [];

if dir == 0                             % if we did not find a sequence
    rs = [];
    return;
end

Ci = bases(Xb(ci));
Cj = bases(Yb(cj));

% Use the first working alternative as the path to return.
if bitand(dir, 4)                       % match
    [rs ti tj] = tracepath(space, bases, T, Yb, Xb, ci-1, cj-1, 0);
    if any(rs)
        ri = [ti Ci];
        rj = [tj Cj];
        return;
    end
end
if bitand(dir, 2)                       % down
    [rs ti tj] = tracepath(space, bases, T, Yb, Xb, ci-1, cj, 2);
    if any(rs)
        ri = [ti Ci];
        rj = [tj space];
        return;
    end
end
if bitand(dir, 1)                       % right
    [rs ti tj] = tracepath(space, bases, T, Yb, Xb, ci, cj-1, 1);
    if any(rs)
        ri = [ti space];
        rj = [tj Cj];
        return;
    end
end
%% end % tracepath()
%% end % alignPair()

%% end % multipleAlign()

