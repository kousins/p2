% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function S = locateSimilar(A, minScore, maxReports, minSeparation, maxMatch)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function locateSimilar() - Kernel 3 - Locate sets of similar sequences.
% This function requires Matlab 7 due to its use of subfunctions.
%
% This function uses a variant of the Smith-Waterman dynamic programming
% algorithm to match all subsequences of seqData.main against each of the
% exact subsequences from seqData.match reported by Kernel 2.  This variant is
% a sort of hybrid of local and global alignment -- it is local in main, but
% global in match.  The match must involve the entire match subsequence.
%
% This kernel combines the functions of Kernel 1 and Kernel 2 in one pass by
% building the trace table T during the forward pass.  Once a promising
% end-point pair has been found, T is used to find the start-points and
% generate an alignment.  For each of the A.bestSeqs input sequences, the best
% of these are reported in the S output structure.
%
% This is possible since it is reasonable to limit the length of an alignment
% in main via the maxMatch parameter, and the length of the match sequence is
% known, so we can afford to generate the trace table T as we scan the main
% sequence.  T is organized as a rectangular array operating as a ring buffer
% of rows.  As previously, only the best match from a cluster of closely
% related matches is reported.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% A                - [structure] including results of kernel 1 and kernel 2
%   seqData        - [structure] data sequences created by genScalData()
%     main         - [char string] first codon sequence
%     match        - [char string] second codon sequence
%     maxValidation- [Integer] longest matching validation string.
%   simMatrix      - [structure] codon similarity created by genSimMatrix()
%     similarity   - [2D array int8] 1-based codon/codon similarity table
%     aminoAcid    - [1D char vector] 1-based codon to aminoAcid table
%     bases        - [1D char vector] 1-based encoding to base letter table
%     codon        - [64 x 3 char array] 1-based codon to base letters table
%     encode       - [uint8 vector] aminoAcid character to last codon number
%     hyphen       - [uint8] encoding representing a hyphen (gap or space)
%     exact        - [integer] value for exactly matching codons
%     similar      - [integer] value for similar codons (same amino acid)
%     dissimilar   - [integer] value for all other codons
%     gapStart     - [integer] penalty to start gap (>=0)
%     gapExtend    - [integer] penalty to for each codon in the gap (>0)
%     matchLimit   - [integer] longest match including hyphens
%   goodEnds       - [Mx2 integer] M matches; main/match endpoints
%   goodScores     - [Mx1 integer] good scores for upto maxReports endpoints
%   bestStarts     - [Mx2 integer] main/match startpoints
%   bestEnds       - [Mx2 integer] main/match endpoints
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
%   bestScores     - [Mx1 integer] the scores for the bestSeqs
% minScore         - [integer] minimum endpoint score
% maxReports       - [integer] maximum number of endpoints reported
% minSeparation    - [integer] minimum endpoint separation in codons
% maxMatch         - [double] factor which limits the match length
%
% OUTPUT
% S                - [structure array] similar sequence sets for each bestSeq
%   bestScores     - [Mx1 integer] the scores for the bestSeqs
%   bestStarts     - [Mx2 integer] main/match startpoints
%   bestEnds       - [Mx2 integer] main/match endpoints
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: locateSimilar.m,v 1.9 2005/08/19 16:05:22 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

sortReports = 3 * maxReports;           % point at which to sort and
                                        % discard; heuristic

weights = A.simMatrix.similarity;
gapExtend = A.simMatrix.gapExtend;
gapFirst = A.simMatrix.gapStart + gapExtend; % penalty for first codon in gap
hyphen = A.simMatrix.hyphen;

S(1).bestScores = [];                   % in case A.bestSeqs is empty

bestScores = zeros(sortReports, 1);     % preallocate results
bestEnds = zeros(sortReports, 2);
bestStarts = zeros(sortReports, 2);
bestSeqs = cell(sortReports, 2);

for b = 1:size(A.bestSeqs, 1)           % for each input sequence
    report = 0;
    [report, bestStarts, bestEnds, bestSeqs, bestScores] = locateSeq(hyphen, weights, report, sortReports, bestStarts, bestEnds, bestSeqs, bestScores, minSeparation, minScore, maxMatch, gapFirst, gapExtend, A.seqData.main, A.bestSeqs{b, 2}); % scan in main sequence

    [unused perm] = sort(bestScores(1:report)); % avoid using 'descend'
    worst = max(length(perm) - maxReports + 1, 1);
    S(b).bestScores = bestScores(perm(end:-1:worst));
    S(b).bestStarts = bestStarts(perm(end:-1:worst), :);
    S(b).bestEnds = bestEnds(perm(end:-1:worst), :);
    S(b).bestSeqs = bestSeqs(perm(end:-1:worst), :);
end % for b ...


% --------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Scan the main sequence, locating promising alignments.
% Report the score, start-pair, end-pair, and alignment for the best of these.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [report, bestStarts, bestEnds, bestSeqs, bestScores] = locateSeq(hyphen, weights, report, sortReports, bestStarts, bestEnds, bestSeqs, bestScores, minSeparation, minScore, maxMatch, gapFirst, gapExtend, mainSeq, rawMatchSeq)
% Eliminate hyphens from rawMatchSeq
matchSeq = rawMatchSeq(find(rawMatchSeq ~= hyphen));

n = length(mainSeq);
m = length(matchSeq);

maxResult = ceil(m * maxMatch);         % maximum possible match including gaps
T = zeros(maxResult, m, 'uint8');       % preallocate result tree

V = -gapFirst - gapExtend .* [0:m-1];   % previous row's best score
F = V;                                  % previous row if skipping vertically

% Loop over each codon in the mainSeq sequence, matching it with each codon in
% the matchSeq subsequence, using a hybrid local/global affine-gap version of
% Smith-Waterman.
for i = 1:n
    G = double(weights(mainSeq(i), matchSeq)) + [0 V(1:m-1)];
    V = max(F, G);                      % best score ending with this pair

    E(1) = max(-gapFirst, V(1) - gapFirst); % V(1) can be negative
    for j = 2:m                         % for each matchSeq codon:
        E(j) = max(E(j-1) - gapExtend, V(j-1) - gapFirst);
        V(j) = max(E(j), V(j));
    end

% S(b).Vh(i,:) = V; S(b).Eh(i,:) = E; S(b).Fh(i,:) = F; S(b).Gh(i,:) = G; %DEBUG
    iT = mod(i-1, maxResult) + 1;       % the traceback table wraps
    T(iT,:) = 4*(V == E) + 8*(V == F) + 16*(V == G);
    Vg = V - gapFirst;
    F = max(F - gapExtend, Vg);
    T(iT,:) = T(iT,:) + uint8([E(2:m) 0] == Vg) + 2*uint8(F == Vg);

    % If score is good enough
    if V(j) >= minScore
        [report, bestStarts, bestEnds, bestSeqs, bestScores, minScore] = considerAdding(hyphen, report, sortReports, bestStarts, bestEnds, bestSeqs, bestScores, minScore, minSeparation, V, iT, i, j, maxResult, T, mainSeq, matchSeq);               % update bestScores/bestEnds
    end
end
% S(b).T = T;                            % DEBUG


% ----------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Consider adding this end-point pair, and possibly deleting nearby end points.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [report, bestStarts, bestEnds, bestSeqs, bestScores, minScore] = considerAdding(hyphen, report, sortReports, bestStarts, bestEnds, bestSeqs, bestScores, minScore, minSeparation, V, iT, i, j, maxResult, T, mainSeq, matchSeq)
% Find any nearby end points -- use only the best one.
for r = report:-1:1
    if i - bestEnds(r,1) >= minSeparation
        break;                          % retain point r
    end
    if bestScores(r) > V(j)
        return;                         % discard new point (and maybe others)
    end
    % discard point r
    bestScores(r:report-1) = bestScores(r+1:report);
    bestStarts(r:report-1, :) = bestStarts(r+1:report, :);
    bestEnds(r:report-1, :) = bestEnds(r+1:report, :);
    bestSeqs(r:report-1, :) = bestSeqs(r+1:report, :);
    report = report - 1;
end

% Use the trace table to find the actual sequences with gaps including gaps.
[rs ri rj, iT] = tracepath(hyphen, maxResult, T, mainSeq, matchSeq, iT, i, i, j, 0);
if rs(2) ~= 1                           % if result was truncated
    return;
end

% Find any nearby start points -- use only the best one.
for r = report:-1:1
    if abs(rs(1) - bestStarts(r,1)) >= minSeparation
        break;                          % retain point r
    end
    if bestScores(r) > V(j)
        return;                         % discard new point (and maybe others)
    end
    % discard point r
    bestScores(r:report-1) = bestScores(r+1:report);
    bestStarts(r:report-1, :) = bestStarts(r+1:report, :);
    bestEnds(r:report-1, :) = bestEnds(r+1:report, :);
    bestSeqs(r:report-1, :) = bestSeqs(r+1:report, :);
    report = report - 1;
end


% Add new point
report = report + 1;
bestScores(report) = V(j);
bestStarts(report, :) = rs;
bestEnds(report, :) = [i j];
bestSeqs(report, :) = {ri rj};

% When the table is full, sort and discard all but the best matches.
% Keep the table in entry order, just compact-out the discarded entries.
if report == sortReports
    [scores index] = sort(bestScores);
    worst = sortReports - maxReports + 1;% index of worst score to keep
    minScore = scores(worst) + 1;       % we will have to beat this score
    best = sort(index(worst:end));      % pick the best of the scores
    bestScores(1:maxReports) = bestScores(best);
    bestEnds(1:maxReports, :) = bestEnds(best, :);
    report = maxReports;
end


% ------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Generate a typical matching pair.
%
% With a small change this recursive approach can be used to generate all
% possible matches.  We only need to identify a single match.  If the match is
% longer than about 100 codons, Matlab encounters a recursion limit error;
% this limit can be set higher, using "set(0, 'RecursionLimit', 200);", but it
% might be better to modify this routine to use an array to control its own
% recursion.
%
% INPUTS
%   ci,cj          [integers] subsequence end coordinates, used recursively
%   dir            [integer] 1 when extending a gap in main (down),
%                            2 when extending a gap in match (right), else 0
% OUTPUT
%   rs             [i, j] coordinates of end of path
%   ri             [uint8 row vector] main sequence
%   rj             [uint8 row vector] match sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [rs, ri, rj, iT] = tracepath(hyphen, maxResult, T, mainSeq, matchSeq, iT, i, ci, cj, dir)
ri = [];
rj = [];

if ci == 0 || cj == 0 || ci == i - maxResult  % if done
    rs = [ci+1 cj+1];
    return;
end

iT = mod(ci-1, maxResult) + 1;          % the traceback table wraps
if dir == 0 || bitand(dir, T(iT,cj))    % if not skipping, or start of gap
    dir = bitshift(T(iT,cj), -2);       % fan out from this point
end

if dir == 0                             % if we did not find a sequence
    rs = [];
    return;
end

Ci = mainSeq(ci);
Cj = matchSeq(cj);

% Use the first working alternative as the path to return.
if bitand(dir, 4)                       % match
    [rs ti tj, iT] = tracepath(hyphen, maxResult, T, mainSeq, matchSeq, iT, i, ci-1, cj-1, 0);
    if any(rs)
        ri = [ti Ci];
        rj = [tj Cj];
        return;
    end
end
if bitand(dir, 2)                       % down
    [rs ti tj, iT] = tracepath(hyphen, maxResult, T, mainSeq, matchSeq, iT, i, ci-1, cj, 2);
    if any(rs)
        ri = [ti Ci];
        rj = [tj hyphen];
        return;
    end
end
if bitand(dir, 1)                       % right
    [rs ti tj, iT] = tracepath(hyphen, maxResult, T, mainSeq, matchSeq, iT, i, ci, cj-1, 1);
    if any(rs)
        ri = [ti hyphen];
        rj = [tj Cj];
        return;
    end
end
%% end % tracepath()
%% end % considerAdding()
%% end % locateSeq()

%% end
