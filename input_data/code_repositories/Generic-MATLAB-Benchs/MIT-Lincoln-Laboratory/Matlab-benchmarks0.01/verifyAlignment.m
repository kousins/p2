% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifyAlignment(A, maxDisplay)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifyAlignment() - Kernel 2 - Pairwise Sequence Alignment.
% This function requires Matlab 7 due to its use of subfunctions.
%
% This functions rescores each match to verify that all reported matches
% have the reported score.  It does not verify that they are the best
% match.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% A                - [structure] reporting the best alignments
%   seqData        - [structure] raw sequences
%     main         - [uint8 string] sequence created by genScalData()
%     match        - [uint8 string] sequence aligned with main
%   simMatrix      - [structure] similarity info from genSimMatrix()
%     similarity   - [64x64 int8] codon/codon similarity scores
%     gapStart     - [integer] penalty to start a gap (>= 0)
%     gapExtend    - [integer] penalty per gap length (normally >0)
%   goodEnds       - [Mx2 integer] M matches; main/match endpoints
%   goodScores     - [Mx1 integer] good scores for upto maxReports endpoints
%   bestScores     - [Mx1 vector] best scores over all possible matches
%   bestStarts     - [Mx2 integer] M matches; main/match startpoints
%   bestEnds       - [Mx2 vector] end points; main/match
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
% maxDisplay       - [integer] number of reports to display to the console
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifyAlignment.m,v 1.10 2005/08/19 16:05:22 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

global P ENABLE_PAUSE;

M = length(A.bestScores);
if M == 0
    fprintf('\nFound no acceptable alignments.\n');
else
    fprintf('\nFound %d acceptable alignments with scores from %d to %d.\n', ...
            M, A.bestScores(1), A.bestScores(M));
    if maxDisplay > 0
        fprintf(['\nStarting   Amino     Codon           Ending\n' ...
                 'position   acids     bases           position\n']);
    end
end

% For each of the bestScores reported by Kernel 2
for m = 1:M
    score = 0;
    main = A.bestSeqs{m, 1};
    match = A.bestSeqs{m, 2};
    % Recompute the alignment score in the most obvious way.
    for i = 1:length(main)
        if main(i) == A.simMatrix.hyphen
            if mainMatch
                mainMatch = false;
                score = score - A.simMatrix.gapStart;
            end
            score = score - A.simMatrix.gapExtend;
            continue;
        end
        if match(i) == A.simMatrix.hyphen
            if matchMatch
                matchMatch = false;
                score = score - A.simMatrix.gapStart;
            end
            score = score - A.simMatrix.gapExtend;
            continue;
        end
        mainMatch = true;
        matchMatch = true;
        score = score + double(A.simMatrix.similarity(main(i), match(i)));
    end
    
    % Check the results and report success or failure.
    if score ~= A.bestScores(m)
        P.failure = 1;
        fprintf(['\nverifyAlignment %g failed; reported %g vs actual %g:', ...
                 '   ---------------------------\n'], ...
                m, A.bestScores(m), score);
    elseif m <= maxDisplay
        fprintf('\nverifyAlignment %g, succeeded; score %g:\n', ...
                m, A.bestScores(m));
    end
    if m <= maxDisplay
        fprintf('%7d  %s  %s %7d\n%7d  %s  %s %7d\n', ...
                A.bestStarts(m,1), A.simMatrix.aminoAcid(main), ...
                strcat(A.simMatrix.codon(main,:).'), A.bestEnds(m,1), ...
                A.bestStarts(m,2), A.simMatrix.aminoAcid(match), ...
                strcat(A.simMatrix.codon(match,:).'), A.bestEnds(m,2));
    end
end

% Print out the actual sequences as amino acids if they aren't too long.
if length(A.seqData.main) < 400
    printSeq(A, 'main', A.seqData.main);
    printSeq(A, 'match', A.seqData.match);
end
    
if ENABLE_PAUSE
    pause;
end


% --------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Print out a sequence as amino acids.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function printSeq(A, name, seq)
group = 10;                             % length of a group
width = 5;                              % number of groups per line

cpl = width * group;                    % codons per line
aa = A.simMatrix.aminoAcid(seq);
n = 0;                                  % current codon number-1
while n < length(aa)
    if mod(n, cpl) == 0                 % once per line
        fprintf('\n%5s %3d', name, n+1);
        name = '';                      % first time only
    end
    m = min(n+10, length(aa));
    fprintf(' %s', aa(n+1:m));
    n = m;
end
fprintf('\n');
%% end % printSeq()

%% end
