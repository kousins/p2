% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function simMatrix = genSimMatrix(exact, similar, dissimilar, ...
                                  gapStart, gapExtend, matchLimit)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function genSimMatrix() - Generate the Kernel 1/2/3 Similarity Matrix
%
% The simMatrix data structure contains the data elements needed to define
% codon-level similarity for later use by various kernels.  These include a
% matrix which gives the similarity score for any pair of codons, a vector
% which is used to map codons to amino acids, an array which is used to map
% codons to base sequences, the various UI parameters, etc.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% exact          - [integer] value for exactly matching codons
% similar        - [integer] value for similar codons (same amino acid)
% dissimilar     - [integer] value for all other codons
% gapStart       - [integer] penalty to start gap (>=0)
% gapExtend      - [integer] penalty to for each codon in the gap (>0)
% matchLimit     - [integer] longest match including hyphens
%
% OUTPUT
%
% simMatrix      - [structure] holds the generated similarity parameters
%   similarity   - [2D array int8] 1-based codon/codon similarity table
%   aminoAcid    - [1D char vector] 1-based codon to aminoAcid table
%   bases        - [1D char vector] 1-based encoding to base letter table
%   codon        - [64 x 3 char array] 1-based codon to base letters table
%   encode       - [uint8 vector] aminoAcid character to last codon number
%   hyphen       - [uint8] encoding representing a hyphen (gap or space)
%   exact        - [integer] value for exactly matching codons
%   similar      - [integer] value for similar codons (same amino acid)
%   dissimilar   - [integer] value for all other codons
%   gapStart     - [integer] penalty to start gap (>=0)
%   gapExtend    - [integer] penalty to for each codon in the gap (>0)
%   matchLimit   - [integer] longest match including hyphens
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: genSimMatrix.m,v 1.12 2005/08/19 16:05:22 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Kernels 1/2/3 -- Similarity matrix
similarities = {{'A', 'gct', 'gcc', 'gca', 'gcg'};
                {'C', 'tgt', 'tgc'};
                {'D', 'gat', 'gac'};
                {'E', 'gaa', 'gag'};
                {'F', 'ttt', 'ttc'};
                {'G', 'ggt', 'ggc', 'gga', 'ggg'};
                {'H', 'cat', 'cac'};
                {'I', 'att', 'atc', 'ata'};
                {'K', 'aaa', 'aag'};
                {'L', 'ttg', 'tta', 'ctt', 'ctc', 'cta', 'ctg'};
                {'M', 'atg'};
                {'N', 'aat', 'aac'};
                {'P', 'cct', 'ccc', 'cca', 'ccg'};
                {'Q', 'caa', 'cag'};
                {'R', 'cgt', 'cgc', 'cga', 'cgg', 'aga', 'agg'};
                {'S', 'tct', 'tcc', 'tca', 'tcg', 'agt', 'agc'};
                {'T', 'act', 'acc', 'aca', 'acg'};
                {'V', 'gtt', 'gtc', 'gta', 'gtg'};
                {'W', 'tgg'};
                {'Y', 'tat', 'tac'};
                {'*', 'taa', 'tag', 'tga'}};

simMatrix.star = uint8(49);             % used for verification
simMatrix.encode = repmat(simMatrix.star, 1, 128);


simMatrix.hyphen = uint8(65);           % larger than any codon
simMatrix.codon(simMatrix.hyphen, :) = '---';
simMatrix.aminoAcid(simMatrix.hyphen) = '-';
simMatrix.bases = 'agct';               % in order as encoded

for s = similarities.'
    aa = s{1}{1};
    for codon = s{1}(2:end)
        ccode = 0;
        for base = codon{1}             % encode this codon
            switch base
             case 'a'
              ccode = 0 + 4*ccode;
             case 'g'
              ccode = 1 + 4*ccode;
             case 'c'
              ccode = 2 + 4*ccode;
             case 't'
              ccode = 3 + 4*ccode;
            end
        end
        ccode = ccode + 1;              % 1-based indexing
        simMatrix.codon(ccode, :) = codon{1};
        simMatrix.aminoAcid(ccode) = aa;
    end
    simMatrix.encode(aa) = ccode;       % for verification only
end

simMatrix.similarity = repmat(int8(dissimilar), 64, 64);
for ccode = 1:64
    m = find(simMatrix.aminoAcid == simMatrix.aminoAcid(ccode));
    simMatrix.similarity(ccode, m) = similar;
end
simMatrix.similarity(find(eye(64))) = exact;

simMatrix.exact = exact;
simMatrix.similar = similar;
simMatrix.dissimilar = dissimilar;
simMatrix.gapStart = gapStart;
simMatrix.gapExtend = gapExtend;
simMatrix.matchLimit = matchLimit;

