% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function [bestStarts, bestEnds, bestScores, bestSeqs] = ...
    scanBackward(A, maxReports, minSeparation)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function scanBackward() - Kernel 2 - Find actual codon sequences
% This function requires Matlab 7 due to its use of subfunctions.
%
% This function uses a variant of the Smith-Waterman dynamic programming
% algorithm to locate each actual aligned sequence from its end points and
% score as reported by Kernel 1.  Some of end-points pairs may be rejected,
% primarily because their matching start-points fall within a specified
% interval of a better match.  Only the best maxReports matches are reported.
%
% While the start-points are being located, a record is kept of the
% alternatives at each point.  Then the recursive tracepath function is
% used to locate the match to report; there may be one or more equally
% valid matches and if so the first one found is reported.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% A                - [structure] results from pairwiseAlign
%   seqData        - [structure] data sequences created by genScalData()
%     main         - [char string] first codon sequence
%     match        - [char string] second codon sequence
%     maxValidation- [Integer] longest matching validation string.
%   simMatrix      - [structure] codon similarity created by genSimMatrix()
%     similarity   - [2D array int8] 1-based codon/codon similarity table
%     aminoAcid    - [1D char vector] 1-based codon to aminoAcid table
%     bases        - [1D char vector] 1-based encoding to base letter table
%     codon        - [64 x 3 char array] 1-based codon to base letters table
%     encode       - [uint8 vector] aminoAcid character to last codon number
%     hyphen       - [uint8] encoding representing a hyphen (gap or space)
%     exact        - [integer] value for exactly matching codons
%     similar      - [integer] value for similar codons (same amino acid)
%     dissimilar   - [integer] value for all other codons
%     gapStart     - [integer] penalty to start gap (>=0)
%     gapExtend    - [integer] penalty to for each codon in the gap (>0)
%     matchLimit   - [integer] longest match including hyphens
%   goodEnds       - [Mx2 integer] M matches; main/match endpoints
%   goodScores     - [Mx1 integer] the scores for the goodEnds
% maxReports       - [integer] maximum number of endpoints reported
% minSeparation    - [integer] minimum startpoint separation in codons
%
% OUTPUT
% bestStarts       - [Mx2 integer] main/match startpoints
% bestEnds         - [Mx2 integer] main/match endpoints
% bestSeqs         - [Mx2 cell array of uint8 rows] main/match sequences
% bestScores       - [Mx1 integer] the scores for the bestSeqs
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: scanBackward.m,v 1.14 2005/08/19 16:05:22 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% NOTE: While this general technique is valid, and while this code seems
% to work in the cases we have tested, there is a good chance that BUGS
% still lurk here.  The problems come from the fact the back-scan is not
% necessarily entirely symmetric with the forward scan, and in those cases
% it may not get a correct result.  Corrections are welcome.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

global P;

% Set up local names for input variables
mainSeq = A.seqData.main;
matchSeq = A.seqData.match;
weights = A.simMatrix.similarity;
gapStart = A.simMatrix.gapStart;
gapExtend = A.simMatrix.gapExtend;
gapFirst = gapStart + gapExtend;        % total penalty for first codon in gap

% Preallocate the outputs
bestStarts = zeros(maxReports, 2);
bestEnds = zeros(maxReports, 2);
bestSeqs = cell(maxReports, 2);
bestScores = zeros(maxReports, 1);

% Preallocate working storage
sizeT = 2 * max(A.simMatrix.matchLimit, A.seqData.maxValidation); % *2 for gaps
T = zeros(sizeT, 'uint8');              % T is used to trace exact sequences

bestR = 0;
for report = 1:length(A.goodScores)
    goal = A.goodScores(report);        % predicted score
    ei = A.goodEnds(report, 1);         % main endpoint
    ej = A.goodEnds(report, 2);         % match endpoint
    T(:) = 0;
    [bestR, bestStarts, bestEnds, bestSeqs, bestScores] = doScan(A, T, ei, ej, mainSeq, matchSeq, weights, bestR, bestStarts, bestEnds, bestSeqs, bestScores, gapFirst, gapExtend, minSeparation, report, goal);% see below
    
    if bestR == maxReports              % quit with requested results
        break;                          % all done
    end
end

bestStarts(bestR+1:end, :) = [];        % truncate outputs (if necessary)
bestEnds(bestR+1:end, :) = [];
bestSeqs(bestR+1:end, :) = [];
bestScores(bestR+1:end) = [];


% ------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Scan from the current end point -- return if expected match is found.
%
% This variant of the Smith-Waterman algorithm starts at an end-point pair and
% proceeds diagonally up and to the left, searching for the matching
% start-point pair.  The outer loop goes right to left horizontally across the
% matchSeq dimension.  The inner loop goes diagonally up and right, working
% backward through the main sequence and simultaneously forward through the
% match sequence.  The loops terminate at the edges of the square of possible
% matches; the algorithm terminates at the first match with the specified
% score.
%
% This variant differs from the Kernel 1 variant in that its initial point
% is fixed (the end-point pair), and that its goal is known.  To find this
% goal it may be necessary for intermediate values of the score to go
% negative, which is impossible for Kernel 1.  However such matches are
% rejected during traceback and may cause the end-point pair to be rejected.
% For example the match
%       ABCDEF------G
%       ABCDEFXXXXXXG
% might be found by Kernel 1 since its score stays positive scanning right to
% left and its end-point pair is distinct from the F/F end-point pair.  But
% scaning left to right the G/G match is not enough to prevent the gap from
% taking the score negative.  (In this case, this match would also be rejected
% since its start-point pair A/A matches the better ABCDEF/ABCDEF match.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [bestR, bestStarts, bestEnds, bestSeqs, bestScores] = doScan(A, T, ei, ej, mainSeq, matchSeq, weights, bestR, bestStarts, bestEnds, bestSeqs, bestScores, gapFirst, gapExtend, minSeparation, report, goal)

global P;

m = max(ei, ej);                        % longest possible result
V(1:2, 1:m+1) = -Inf;                   % diagonal best scores, one extra
E(1:m) = -Inf;                          % previous row if skipping horizontally
F(1:m) = -Inf;                          % previous col if skipping vertically

% Special case the first point:
s = double(weights(mainSeq(ei), matchSeq(ej)));
if s == goal
    return;                             % discard length one sequences
end
V(1,2) = s;
E(1) = s - gapFirst;
F(1) = s - gapFirst;
T(1,1) = 16+3;

fj = ej - 1;                            % first point on the diagonal
fi = ei;
lj = ej;                                % last point on the diagonal
v = 2;
while fi > 0                            % loop over diagonal starting points
    dj = fj;                            % current point on the diagonal
    di = fi;
    e = ei - di + 1;                    % subscript for E vector
    f = ej - dj + 1;                    % subscript for F and V vectors

    while dj < lj+1 && di > 0
        % match each main with each match codon
        % add the weight of the best match ending just after it
        G = double(weights(mainSeq(di), matchSeq(dj))) + V(v,f);

        % find the very best weight ending with this pair
        s = max([E(e), F(f), G]);
        V(v, f+1) = s;
        if s > 0                        % if score is okay, track this path
            T(e,f) = 4*(s == E(e)) + 8*(s == F(f)) + 16*(s == G);
        else
            T(e,f) = 0;                 % eliminate this path
        end
        
        if s == goal
            % discard if start is too close to a better sequence
            for r = 1:bestR
                if max(abs([di dj] - bestStarts(r,:))) < minSeparation
%%                  fprintf('Start too close to %d: report %d discarded\n', ...
%%                          r, report); % DEBUG
                    return;
                end
            end

            [rs ri rj] = tracepath(A, T, ei, ej, mainSeq, matchSeq, e, f, 0);
            if ~any(rs)
%%              fprintf('No path: report %d discarded\n', report);  % DEBUG
                return;
            end

            bestR = bestR + 1;          % record the result and return
            bestStarts(bestR, :) = [di dj];
            bestEnds(bestR, :) = [ei ej];
            bestSeqs(bestR, :) = {ri rj};
            bestScores(bestR) = goal;
            return;                     % success
        end

%%      Dw(e,f) = weights(mainSeq(di), matchSeq(dj)); % DEBUG
%%      Dv(e,f) = s;                    % DEBUG
        s = s - gapFirst;
        
        % find the best weight assuming a gap in matchSeq
        E(e) = max(E(e) - gapExtend, s);

        % find the weight assuming a gap in mainSeq
        F(f) = max(F(f) - gapExtend, s);
        T(e,f) = T(e,f) + uint8(E(e) == s) + 2*(F(f) == s);

        dj = dj + 1;
        di = di - 1;
        e = e + 1;
        f = f - 1;
    end

    v = 3 - v;
    if fj ~= 1
        fj = fj - 1;
    else
        fi = fi - 1;
    end
end

P.failure = 1;
warning('Sequence %g was not found', report);
%% end % doscan()


% ------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Generate a typical matching pair.
%
% With a small change this recursive approach can be used to generate all
% possible matches.  We only need to identify a single match.  If the match is
% longer than about 100 codons, Matlab encounters a recursion limit error;
% this limit can be set higher, using "set(0, 'RecursionLimit', 200);", but it
% might be better to modify this routine to use an array to control its own
% recursion.
%
% INPUTS
%   i,j            [integers] subsequence end coordinates, used recursively
%   dir            [integer] 1 when extending a gap in main (down),
%                            2 when extending a gap in match (right), else 0
% OUTPUT
%   rs             [i, j] coordinates of end of path
%   ri             [uint8 row vector] main sequence
%   rj             [uint8 row vector] match sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [rs, ri, rj] = tracepath(A, T, ei, ej, mainSeq, matchSeq, i, j, dir)
ri = [];
rj = [];

if i == 0 || j == 0                     % if done
    rs = [i+1 j+1];
    return;
end

if dir == 0 || bitand(dir, T(i,j))      % if not skipping, or start of gap
    dir = bitshift(T(i,j), -2);         % fan out from this point
end

if dir == 0                             % if we did not find a sequence
    rs = [];
    return;
end

Ci = mainSeq(ei-i+1);
Cj = matchSeq(ej-j+1);

% Use the first working alternative as the path to return.
if bitand(dir, 4)                       % match
    [rs ti tj] = tracepath(A, T, ei, ej, mainSeq, matchSeq, i-1, j-1, 0);
    if any(rs)
        ri = [Ci ti];
        rj = [Cj tj];
        return;
    end
end
if bitand(dir, 2)                       % down
    [rs ti tj] = tracepath(A, T, ei, ej, mainSeq, matchSeq, i-1, j, 2);
    if any(rs)
        ri = [Ci ti];
        rj = [A.simMatrix.hyphen tj];
        return;
    end
end
if bitand(dir, 1)                       % right
    [rs ti tj] = tracepath(A, T, ei, ej, mainSeq, matchSeq, i, j-1, 1);
    if any(rs)
        ri = [A.simMatrix.hyphen ti];
        rj = [Cj tj];
        return;
    end
end
%% end % tracepath()

%% end
