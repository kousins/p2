% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifyGlobal(G, misPenalty, gapPenalty, maxDisplay)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifyGlobal() - Kernel 4 - Verify sets of global alignment scores.
%
% This function simply displays (some of) the kernel 4 results.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% G                - [1-D Data structure] alignments for set g
%   globalScores   - [2-D integer array] alignment score (x,y)
% misPenalty       - [integer] penalty for each base/base mismatch
% gapPenalty;      - [integer] penalty for each space in a gap
% maxDisplay       - [integer] number of reports to display to the console
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifyGlobal.m,v 1.5 2005/08/19 16:05:23 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

declareGlobals;

B = length(G);
D = min(maxDisplay, B);

fprintf('\nDisplaying %d of %d sets of global alignment pair scores.\n', D, B);
fprintf('\n');
fprintf('       Penalty for mismatching bases: %d\n', misPenalty);
fprintf('     Penalty for each space in a gap: %d\n', gapPenalty);

if D > 0
    fprintf('\nThe score for sequence x matched against sequence y.\n\n');
end

% For each of the scores being displayed
for b = 1:D
    if length(G(b).globalScores) > 0
        fprintf('Set %d:\n', b);
        disp(G(b).globalScores);        % display the entire matrix
    end
end
    
if ENABLE_PAUSE
    pause;
end
