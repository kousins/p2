% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifySummary(P)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifySummary() - Print verification summary information.
%
% Prints results to the Matlab console.
%
% INPUT
% P              - [Data structure] see initMatlabMPI
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifySummary.m,v 1.2 2005/08/19 16:05:23 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

if P.failure
    fprintf(['\nAt least one verification test has FAILED --\n' ...
             'see the console log above for details.\n']);
else
    fprintf('\nAll verification tests PASSED.\n');
end
