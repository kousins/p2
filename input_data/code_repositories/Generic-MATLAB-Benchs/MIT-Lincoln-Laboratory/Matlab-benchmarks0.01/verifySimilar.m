% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifySimilar(A, S, maxDisplay)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifySimilar() - Kernel 3 - Verify sets of similar sequences.
%
% This function rescores each match to verify that all reported matches
% have the reported score.  It does not verify that they are the best
% match.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% A                - [structure] reporting the best alignments
%   seqData        - [structure] raw sequences
%     main         - [uint8 string] sequence created by genScalData()
%     match        - [uint8 string] sequence aligned with main
%     maxValidation- [Integer] longest matching validation string.
%   simMatrix      - [structure] similarity info from genSimMatrix()
%     similarity   - [64x64 int8] codon/codon similarity scores
%     gapStart     - [integer] penalty to start a gap (>= 0)
%     gapExtend    - [integer] penalty per gap length (normally >0)
%   goodEnds       - [Mx2 integer] M matches; main/match endpoints
%   goodScores     - [Mx1 integer] good scores for upto maxReports endpoints
%   bestScores     - [Mx1 vector] best scores over all possible matches
%   bestStarts     - [Mx2 integer] M matches; main/match startpoints
%   bestEnds       - [Mx2 vector] end points; main/match
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
% S                - [structure array] similar sequence sets for each bestSeq
%   bestScores     - [Mx1 integer] the scores for the bestSeqs
%   bestStarts     - [Mx2 integer] main/match startpoints
%   bestEnds       - [Mx2 integer] main/match endpoints
%   bestSeqs       - [Mx2 cell array of uint8 rows] main/match sequences
% maxDisplay       - [integer] number of reports to display to the console
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifySimilar.m,v 1.7 2005/08/19 16:05:23 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

global P ENABLE_PAUSE;

B = length(S);
D = min(maxDisplay, B);

fprintf('\nFound %d acceptable sets of alignments.\n', B);
if D > 0
    if D < B
        fprintf('Displaying the first %d of them.\n', D);
    end
    fprintf(['\nStarting   Amino     Codon           Ending\n' ...
               'position   acids     bases           position\n']);
end

% For each set of similar sequences
for b = 1:length(S)
    % For each match found by Kernel 3
    for m = 1:length(S(b).bestScores)
        score = 0;
        main = S(b).bestSeqs{m, 1};
        match = S(b).bestSeqs{m, 2};
        % Recompute the alignment score in the most obvious way.
        for i = 1:length(main)
            if main(i) == A.simMatrix.hyphen
                if mainMatch
                    mainMatch = false;
                    score = score - A.simMatrix.gapStart;
                end
                score = score - A.simMatrix.gapExtend;
                continue;
            end
            if match(i) == A.simMatrix.hyphen
                if matchMatch
                    matchMatch = false;
                    score = score - A.simMatrix.gapStart;
                end
                score = score - A.simMatrix.gapExtend;
                continue;
            end
            mainMatch = true;
            matchMatch = true;
            score = score + double(A.simMatrix.similarity(main(i), match(i)));
        end
    
        % Check the results and report success or failure.
        if score ~= S(b).bestScores(m)
            P.failure = 1;
            fprintf(['\nverifySimilar %g/%g failed; reported %g vs actual %g:', ...
                     '   ---------------------------\n'], ...
                    b, m, S(b).bestScores(m), score);
        elseif b <= D
            fprintf('\nverifySimilar %g/%g, succeeded; score %g:\n', ...
                    b, m, S(b).bestScores(m));
        end
        if b <= D
            fprintf('%7d  %s  %s %7d\n%7d  %s  %s %7d\n', ...
                    S(b).bestStarts(m,1), A.simMatrix.aminoAcid(main), ...
                    strcat(A.simMatrix.codon(main,:).'), S(b).bestEnds(m,1), ...
                    S(b).bestStarts(m,2), A.simMatrix.aminoAcid(match), ...
                    strcat(A.simMatrix.codon(match,:).'), S(b).bestEnds(m,2));
        end
    end
end

% Print out the actual sequences as amino acids if they aren't too long.
if length(A.seqData.main) < 400
    printSeq(A, 'main', A.seqData.main);
    printSeq(A, 'match', A.seqData.match);
end

if ENABLE_PAUSE
    pause;
end


% -------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Print out a sequence as amino acids.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function printSeq(A, name, seq)
group = 10;                             % length of a group
width = 5;                              % number of groups per line

cpl = width * group;                    % codons per line
aa = A.simMatrix.aminoAcid(seq);
n = 0;                                  % current codon number-1
while n < length(aa)
    if mod(n, cpl) == 0                 % once per line
        fprintf('\n%5s %3d', name, n+1);
        name = '';                      % first time only
    end
    m = min(n+10, length(aa));
    fprintf(' %s', aa(n+1:m));
    n = m;
end
fprintf('\n');
%% end % printSeq()

%% end
