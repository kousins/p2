% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function A = pairwiseAlign(seqData, simMatrix, ...
                           minScore, maxReports, minSeparation)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function pairwiseAlign() - Kernel 1 - Pairwise Local Sequence Alignment.
% This function requires Matlab 7 due to its use of subfunctions.
%
% This function uses a variant of the Smith-Waterman dynamic programming
% algorithm to match all subsequences of seqData.main against all subsequences
% of seqData.match, to score them using the 'local affine gap' codon matching
% function defined by simMatrix, and to report an ordered set of best matches.
% The best matches include only the score and the end points of each match.
%
% When selecting a set of 'best' local sequence alignments it is important to
% chose only one match from a cluster of closely related matches.  Various
% heuristics to accomplish are discussed in the literature.  This code reports
% only the best match ending within a specified interval of each end point.
% In addition, further filtering is performed by Kernel 2, which locates the
% actual aligned sequences from their end points and scores.
%
% If you don't understand the Smith-Waterman algorithm you will have trouble
% understanding this code.  The recurrences used are discussed in Gusfield,
% Algorithms on Stings, Trees, and Sequences, pg 244.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% seqData          - [structure] data sequences created by genScalData()
%   main           - [char string] first codon sequence
%   match          - [char string] second codon sequence
%   maxValidation  - [Integer] longest matching validation string.
% simMatrix        - [structure] codon similarity created by genSimMatrix()
%   similarity     - [2D array int8] 1-based codon/codon similarity table
%   aminoAcid      - [1D char vector] 1-based codon to aminoAcid table
%   bases          - [1D char vector] 1-based encoding to base letter table
%   codon          - [64 x 3 char array] 1-based codon to base letters table
%   encode         - [uint8 vector] aminoAcid character to last codon number
%   hyphen         - [uint8] encoding representing a hyphen (gap or space)
%   exact          - [integer] value for exactly matching codons
%   similar        - [integer] value for similar codons (same amino acid)
%   dissimilar     - [integer] value for all other codons
%   gapStart       - [integer] penalty to start gap (>=0)
%   gapExtend      - [integer] penalty to for each codon in the gap (>0)
%   matchLimit     - [integer] longest match including hyphens
% minScore         - [integer] minimum endpoint score
% maxReports       - [integer] maximum number of endpoints reported
% minSeparation    - [integer] minimum endpoint separation in codons
%
% OUTPUT
% A                - [Data structure] reporting the good alignments
%   seqData        - copy of input argument
%   simMatrix      - copy of input argument
%   goodEnds       - [Mx2 integer] M matches; main/match endpoints
%   goodScores     - [Mx1 integer] good scores for upto maxReports endpoints
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: pairwiseAlign.m,v 1.14 2005/08/19 16:08:25 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Copy input arguments:
A.seqData = seqData;
A.simMatrix = simMatrix;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Forward pass (Kernel 2 does the backward pass)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sortReports = 3 * maxReports;           % point at which to sort and
                                        % discard; heuristic
mainSeq = seqData.main;
matchSeq = seqData.match;
weights = simMatrix.similarity;
gapExtend = simMatrix.gapExtend;
gapFirst = simMatrix.gapStart + gapExtend; % penalty for first codon in gap

goodScores = zeros(sortReports, 1);     % preallocate
goodEnds = zeros(sortReports, 2);       % preallocate
report = 0;

n = length(mainSeq);
m = length(matchSeq);
V(1:m) = 0;                             % previous row's best score
F(1:m) = -gapFirst;                     % previous row if skipping vertically

% Loop over each codon in the mainSeq sequence, matching it with each codon
% in the matchSeq sequence, using the local-affine version of Smith-Waterman.
for i = 1:n
    % the first point in match is special (we assume it can't end a good match)
    main = mainSeq(i);
    G = double(weights(main, matchSeq(1)));
    Vp = V(1);                          % value of previous row
    V(1) = max([0, F(1), G]);           % best score ending with this pair
    F(1) = max(F(1) - gapExtend, V(1) - gapFirst);

    E = V(1) - gapFirst;
    for j = 2:m                         % and for each matchSeq codon:
        % match each mainSeq with each matchSeq codon to get a similarity weight
        W = double(weights(main, matchSeq(j)));
        % add the weight of the best match ending just before it
        G = W + Vp;
        
        % find the very best weight ending with this pair
        Vp = V(j);                      % value of previous row
        V(j) = max([0, E, F(j), G]);

% A.Vh(i,j) = V(j); A.Eh(i,j) = E; A.Fh(i,j) = F(j); A.Gh(i,j) = G; % DEBUG

        % If score is good enough, this is an improvement, is not a skip,
        % and the next pair doesn't improve the match
        if (V(j) >= minScore && W > 0 && V(j) == G && ...
            (j == m || i == n || weights(mainSeq(i+1), matchSeq(j+1)) <= 0))
            [goodEnds, goodScores, minScore, report] = considerAdding(V, goodEnds, goodScores, minScore, report, minSeparation, i, j, sortReports, maxReports); % update goodScores/goodEnds
        end

        % find the best weight assuming a gap in mainSeq
        E = max(E - gapExtend, V(j) - gapFirst);

        % find the best weight assuming a gap in matchSeq
        F(j) = max(F(j) - gapExtend, V(j) - gapFirst);
    end
end

[unused perm] = sort(goodScores(1:report)); % avoid using 'descend'
worst = max(length(perm) - maxReports + 1, 1);
A.goodScores = goodScores(perm(end:-1:worst));
A.goodEnds = goodEnds(perm(end:-1:worst), :);


% ----------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Consider adding this end point, and possibly deleting nearby end points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [goodEnds, goodScores, minScore, report] = considerAdding(V, goodEnds, goodScores, minScore, report, minSeparation, i, j, sortReports, maxReports)
% Find any nearby end points -- use only the best one.
for r = report:-1:1
    if i - goodEnds(r,1) >= minSeparation
        break;                          % retain point r
    end
    if abs(j - goodEnds(r,2)) >= minSeparation
        continue;                       % if not nearby
    end
    if goodScores(r) > V(j)
        return;                         % discard new point (and maybe others)
    end
    % discard point r
    goodScores(r:report-1) = goodScores(r+1:report);
    goodEnds(r:report-1, :) = goodEnds(r+1:report, :);
    report = report - 1;
end

% Add a new point
report = report + 1;
goodScores(report) = V(j);
goodEnds(report, :) = [i j];

% When the table is full, sort and discard all but the best end points.
% Keep the table in entry order, just compact-out the discarded entries.
if report == sortReports
    [scores index] = sort(goodScores);
    worst = sortReports - maxReports + 1; % index of worst score to keep
    minScore = scores(worst) + 1;       % we will have to beat this score
    best = sort(index(worst:end));      % pick the best of the scores
    goodScores(1:maxReports) = goodScores(best);
    goodEnds(1:maxReports, :) = goodEnds(best, :);
    report = maxReports;
end
%% end % considerAdding()

%% end
