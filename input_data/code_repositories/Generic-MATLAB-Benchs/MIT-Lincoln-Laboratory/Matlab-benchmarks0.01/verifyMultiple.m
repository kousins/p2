% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifyMultiple(M, maxDisplay)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifyMultiple() - Kernel 5 - Verify multiple alignments
%
% This function simply displays (some of) the kernel 5 results.
%
% For a detailed description of the SSCA #1 Optimal Pattern Matching problem, 
% please see the SSCA #1 Written Specification.
%
% INPUT
% MA               - [1-D Data structure] alignments for set g
%   scores         - [1-D integer vector] score for each alignment
%   alignments     - [1-D cell array of strings] final alignments
% maxDisplay       - [integer] number of reports to display to the console
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifyMultiple.m,v 1.7 2005/08/19 16:05:23 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

declareGlobals;

B = length(M);
D = min(maxDisplay, B);

fprintf('\nDisplaying %d of %d sets of global alignments.\n', D, B);

if D > 0
    fprintf('\nWithin each set the aligned center-sequence is shown first,\n');
    fprintf('then the other aligned sequences ordered by score.\n');
end

% For each of the scores being displayed
for b = 1:D
    s = length(M(b).scores);
    if s == 0
        fprintf('\nSet %d is empty.\n', b);
    else
        t = sum(M(b).scores);
        fprintf('\nSet %d; size %d, total score %d, average score %.2f\n', ...
                b, s, t, t/s);
        for i = 1:length(M(b).scores)
            fprintf('  %4d  %s\n', M(b).scores(i), M(b).alignment{i});
        end
    end
end
    
if ENABLE_PAUSE
    pause;
end
