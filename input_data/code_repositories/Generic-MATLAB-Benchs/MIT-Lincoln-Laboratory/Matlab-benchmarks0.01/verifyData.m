% Generated Fri Aug 19 12:15:17 EDT 2005 by buildserial version 1.1
% Any changes must be made in the original file or they will be lost.

function verifyData(simMatrix, seqData)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Function verifyData() - verify genSimMatrix() and genScalData().
%
% First level checks on the functionality of the code in the Scalable
% Data Generators.
%
% Prints results to the Matlab console.
%
% For a detailed description of the SCCA #1 Graph Construction, 
% please see the SCCA #2 Graph Analysis Written Specification.
% 
% INPUT
% simMatrix      - [structure] holds the generated similarity parameters.
% seqData        - [structure] holds the generated sequences.
%
% REVISION
% August 19, 2005  0.6 Release  MIT Lincoln Laboratory.
% $Id: verifyData.m,v 1.3 2005/08/19 16:05:23 tmeuse Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

declareGlobals;

fprintf('\n');
fprintf('   Length of main sequence in codons: %d\n', length(seqData.main));
fprintf('  Length of match sequence in codons: %d\n', length(seqData.match));
fprintf('  Weight for exactly matching codons: %d\n', simMatrix.exact);
fprintf('           Weight for similar codons: %d\n', simMatrix.similar);
fprintf('        Weight for dissimilar codons: %d\n', simMatrix.dissimilar);
fprintf('              Penalty to start a gap: %d\n', simMatrix.gapStart);
fprintf('     Penalty for each codon in a gap: %d\n', simMatrix.gapExtend);
    
if ENABLE_PAUSE
    pause;
end
