
NPOINTS = 64;
ORDER = 32;

coefficient=[...
0.25, 0.51 , 0.75,-0.25,0.5, 0.75, 0.7,  0.625,...
0.21, 0.22 , 0.93 , 0.81, 0.48, 0.208, 0.4 , 0.6,...
0.25, 0.35, 0.875, 0.295, 0.25, 0.725,0.380, 0.295,...
0.2, 0.22 , 0.73 , 0.91, 0.48, 0.28, 0.694 , 0.6,...
0.25,0.5,0.75,0.525,0.5,0.75,-0.5,0.725,...
0.62, 0.272 , 0.83 , 0.81, 0.48, 0.28, 0.814 ,0.56,...
0.25,0.5,0.75,-0.25,0.5 ,0.75, -0.810, 0.25,...
0.02, 0.252 , 0.913 , 0.81, 0.468, 0.28, 0.4 ,0.36...
];


internal_state=[...
0.25,0.5,0.75,-0.29,0.5,0.75,-0.3,0.422,...
0.82, 0.922 , 0.63 , 0.71, 0.48, 0.218, -0.24 ,0.6,...
0.92, 0.22 , 0.93 , 0.51, 0.498, 0.28, -0.84 ,0.26,...
0.25,0.95, 0.75,-0.27,0.5,0.75,0.80,0.25,...
0.25,0.25, 0.735, -0.25,0.5,0.75, 0.20,0.525,...
0.92, 0.22 , 0.31 ,  0.81, 0.468, 0.288, -0.14 ,0.65,...
0.2, 0.272 , 0.93 , 0.91, 0.485, 0.218, -0.4 ,0.6,...
0.82, 0.72 , 0.63 , 0.21, 0.438, 0.928, -0.24 ,0.6...
];

MAGNITUDE = 32;
FRACTION = 16;

quant1=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
 
 x=(0:NPOINTS-1)./10;
 data = 10*sin(x); %+rand([1 NPOINTS]);
 
bottom=0;
%@@ DENSE quantize BEGIN
  for i = 1:1:NPOINTS 
    top = quantize(quant1,data(i));
    for j = 2:1:ORDER
      left = top;
      right = quantize(quant1,internal_state(j));
      internal_state(j) = bottom;
      top = quantize(quant1,coefficient(j-1) * left - coefficient(j) * right);
      bottom = quantize(quant1,coefficient(j-1) * right + coefficient(j) * left);
    end
        
    internal_state(ORDER-1) = bottom;
    internal_state(ORDER) = top;
    sum = quantize(quant1,0.0);
    
    for j = 1:1:ORDER
      sum = quantize(quant1,sum + internal_state(j) * coefficient(j+ORDER));
    end
        
    outa(i) = quantize(quant1,sum);
  end
%@@ DENSE quantize END

save fixed.dsp outa -ASCII;

% plot results
figure(1);
plot(x, data, 'b', x, outa, 'g');

% compare with C results
%o1=load('output.dsp');
%o2=load('original_c\output.dsp');

% absolute error
%figure(2);
%plot(x, abs(o1-o2'));


