%
% AMADEUS project, Portugal
%
% Description:
%	This MATLAB code implements a function to report the log
%	performed by applying access_calculation.
%
% Modification Log:
% 	v0.1, Jo�o M. P. Cardoso, April 2010.
%
%

function report_accesses()

		 global logTable;

		 fprintf(1,'==== report accesses of %d variables:\n',length(logTable));


		 find = false;
		 N = length(logTable);

  		 for i = 1:N
  		 	 reg=logTable(i);
  		 	 fprintf(1,'var %s : %g accesses\n',reg.name, reg.count);
		 end
%end

