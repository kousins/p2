%
% AMADEUS project, Portugal
%
% Description:
%	This MATLAB code implements a function to log and determine
%	the number of accesses per variables.
%
% Modification Log:
% 	v0.1, Jo�o M. P. Cardoso, April 2010.
%
%

function access_calculation(var, var_name)

		 %fprintf(1, 'var: %s length: %d\n',var_name, length(var));

		 global logTable;

		 find = false;
		 N = length(logTable);

  		 for i=1:N
  		 	 reg=logTable(i);

		   	 if(strcmp(reg.name, var_name))

					logTable(i).count = logTable(i).count+1;
				
				find = true;

				%fprintf(1,'length log_table = %d\n',length(logTable));
				%fprintf(1,'var %s, min = %g; max = %g\n',reg.name, reg.min, reg.max);

				break;
  		 	 end
		 end

		 if(find == false)
			logTable(N+1).name = var_name;
			logTable(N+1).count = 1;

			%fprintf(1,'var %s, min = %g; max = %g\n',logTable(N+1).name, logTable(N+1).min, logTable(N+1).max);
			%fprintf(1,'length log_table = %d\n',length(logTable));
		 end

%end

