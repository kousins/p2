%
% AMADEUS project, Portugal
%
% Description:
%	This MATLAB function implements 32nd-order Normalized Lattice filter 
%	processing 64 points.
% 	
%	This implementation is a translation of the C code existent in the respository:
%	source: UTDSP benchmark suite.
%	Corinna G. Lee, UTDSP benchmark suite, University of Toronto, 
%	Ontario, Canada, 1992.
%
% Modification Log:
% 	v0.2, Jo�o M. P. Cardoso, December 2009.
%
%

function [outa] = latnrm(data, coefficient, internal_state, NPOINTS, ORDER)
% data:            input sample array
% outa:            output sample array
% coefficient:     coefficient array
% internal_state:  internal state array

%@@ DENSE access_calculation BEGIN
access_calculation(data, 'latnrm.data'); %range find call
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call
access_calculation(NPOINTS, 'latnrm.NPOINTS'); %range find call
access_calculation(ORDER, 'latnrm.ORDER'); %range find call

  bottom=0;
access_calculation(bottom, 'latnrm.bottom'); %range find call

  for i = 1:1:NPOINTS
access_calculation(i, 'latnrm.i'); %range find call
access_calculation(i, 'latnrm.i'); %range find call

    top = data(i);
access_calculation(data, 'latnrm.data'); %range find call
access_calculation(top, 'latnrm.top'); %range find call

    for j = 2:1:ORDER
access_calculation(j, 'latnrm.j'); %range find call
access_calculation(j, 'latnrm.j'); %range find call

      left = top;
access_calculation(left, 'latnrm.left'); %range find call
access_calculation(top, 'latnrm.top'); %range find call

      right = internal_state(j);
access_calculation(right, 'latnrm.right'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call

      internal_state(j) = bottom;
access_calculation(bottom, 'latnrm.bottom'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call

      top = coefficient(j-1) * left - coefficient(j) * right;
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(right, 'latnrm.right'); %range find call
access_calculation(left, 'latnrm.left'); %range find call
access_calculation(top, 'latnrm.top'); %range find call

      bottom = coefficient(j-1) * right + coefficient(j) * left;
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(right, 'latnrm.right'); %range find call
access_calculation(left, 'latnrm.left'); %range find call
access_calculation(bottom, 'latnrm.bottom'); %range find call
    end

    internal_state(ORDER-1) = bottom;
access_calculation(bottom, 'latnrm.bottom'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call

    internal_state(ORDER) = top;
access_calculation(top, 'latnrm.top'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call

    sum = 0.0;
access_calculation(sum, 'latnrm.sum'); %range find call

    for j = 1:1:ORDER
access_calculation(j, 'latnrm.j'); %range find call
access_calculation(j, 'latnrm.j'); %range find call

      sum = sum + internal_state(j) * coefficient(j+ORDER);
access_calculation(sum, 'latnrm.sum'); %range find call
access_calculation(internal_state, 'latnrm.internal_state'); %range find call
access_calculation(coefficient, 'latnrm.coefficient'); %range find call
access_calculation(sum, 'latnrm.sum'); %range find call

    end

    outa(i) = sum;
access_calculation(sum, 'latnrm.sum'); %range find call
access_calculation(outa(i), 'latnrm.outa'); %range find call
  end
%@@ DENSE access_calculation END
%end

