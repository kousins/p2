%
% AMADEUS project, Portugal
%
% Description:
%	This MATLAB function implements 32nd-order Normalized Lattice filter 
%	processing 64 points.
% 	
%	This implementation is a translation of the C code existent in the respository:
%	source: UTDSP benchmark suite.
%	Corinna G. Lee, UTDSP benchmark suite, University of Toronto, 
%	Ontario, Canada, 1992.
%
% Modification Log:
% 	v0.2, Jo�o M. P. Cardoso, December 2009.
%
%

function [outa] = latnrm(data, coefficient, internal_state, NPOINTS, ORDER)
% data:            input sample array
% outa:            output sample array
% coefficient:     coefficient array
% internal_state:  internal state array
%@@ DENSE range_find BEGIN
range_find(data, 'latnrm.data'); %range find call
range_find(coefficient, 'latnrm.coefficient'); %range find call
range_find(internal_state, 'latnrm.internal_state'); %range find call
range_find(NPOINTS, 'latnrm.NPOINTS'); %range find call
range_find(ORDER, 'latnrm.ORDER'); %range find call

  bottom=0;
range_find(bottom, 'latnrm.bottom'); %range find call

  for i = 1:1:NPOINTS
range_find(i, 'latnrm.i'); %range find call
    top = data(i);
range_find(top, 'latnrm.top'); %range find call
    for j = 2:1:ORDER
range_find(j, 'latnrm.j'); %range find call
      left = top;
range_find(left, 'latnrm.left'); %range find call
      right = internal_state(j);
range_find(right, 'latnrm.right'); %range find call
      internal_state(j) = bottom;
range_find(internal_state(j), 'latnrm.internal_state'); %range find call
      top = coefficient(j-1) * left - coefficient(j) * right;
range_find(top, 'latnrm.top'); %range find call
      bottom = coefficient(j-1) * right + coefficient(j) * left;
range_find(bottom, 'latnrm.bottom'); %range find call
    end

    internal_state(ORDER-1) = bottom;
range_find(internal_state(ORDER-1), 'latnrm.internal_state'); %range find call
    internal_state(ORDER) = top;
range_find(internal_state(ORDER-1), 'latnrm.internal_state'); %range find call
    sum = 0.0;
range_find(sum, 'latnrm.sum'); %range find call

    for j = 1:1:ORDER
range_find(j, 'latnrm.j'); %range find call
      sum = sum + internal_state(j) * coefficient(j+ORDER);
range_find(sum, 'latnrm.sum'); %range find call
    end

    outa(i) = sum;
range_find(outa(i), 'latnrm.outa'); %range find call
  end
%@@ DENSE range_find END

%end

