%
% Copyright 2009 - AMADEUS project, Portugal
%
% Description:
%	This MATLAB function implements the Discrete Fourier Transform (DFT)
%	and outputs as result the magnitude of the resultant signal.
%
% Modification Log:
% 	v0.2, Jo�o M. P. Cardoso, January 2010.
%
%

function [mag] = dft_custom(x, n)
% assume n is a power of 2
%this.nu = (int)((float) oMathFP.log(N)/ (float) 0.301029995663981);


xre = zeros(1,n); % real part
xim = zeros(1,n); % imaginary part

x2 = zeros(1,n); % output, real part
y2 = zeros(1,n); % output, imaginary part

mag = zeros(1,n/2); % the magintude of the resultant signal

PI = 3.1415926535897932384626433832795; % constant pi exists in MATLAB
twoPI = 2.0 * PI;

n2=n/2;

for i = 1:n2
    xre(i) = x(i);
    %xim(i) = 0.0; 
end

% for i = n2+1:n
%     xre(i) = 0.0;
%     xim(i) = 0.0;
% end

for i = 0:n-1
%    x2(i+1) = 0;
%    y2(i+1) = 0;
    arg = twoPI * i / n;

    for k = 0:n-1
        cosarg = cos(k * arg);
        sinarg = sin(k * arg);

        x2(i+1) = x2(i+1) + xre(k+1) * cosarg - xim(k+1) * sinarg;
        y2(i+1) = y2(i+1) + xre(k+1) * sinarg + xim(k+1) * cosarg;
    end
end

for i = 1:n/2
    mag(i)= sqrt(x2(i)*x2(i) + y2(i)*y2(i));
end
