function [mag] = fft_custom(input, n)

y = zeros(1,n);
x = zeros(1,n);

mag = zeros(1,n/2);

% precompute tables
cos2 = zeros(1,n/2);
sin2 = zeros(1,n/2);

PI = 3.1415926535897932384626433832795; % constant pi exists in MATLAB

m=ceil(log2(n));

for i=1:n/2
    cos2(i) = cos(-2*PI*(i-1)/n);
    sin2(i) = sin(-2*PI*(i-1)/n);
end


for i = 1:n/2
    %y(i) = 0;
    x(i) = input(i);
end
% for i = n/2+1:n
%     y(i) = 0;
%     x(i) = 0;
% end


% Bit-reverse
j = 0;
n2 = n/2;
for i=1:n-1
    n1 = n2;
    while ( j >= n1 )
        j = j - n1;
        n1 = n1/2;
    end
    j = j + n1;

    if i < j
        t1 = x(i+1);
        x(i+1) = x(j+1);
        x(j+1) = t1;
        t1 = y(i+1);
        y(i+1) = y(j+1);
        y(j+1) = t1;
    end
end


n1 = 0;
n2 = 1;

for i=0:m-1
    n1 = n2;
    n2 = n2 + n2;
    a = 1;

    for j=0:n1-1
        c = cos2(a);
        s = sin2(a);
        
        a = a+bitshift(1, m-i-1); % equivalent to a + 1 << (m-i-1);

        for k=j:n2:n-1
            t1 = c*x(k+n1+1) - s*y(k+n1+1);
            t2 = s*x(k+n1+1) + c*y(k+n1+1);
            x(k+n1+1) = x(k+1) - t1;
            y(k+n1+1) = y(k+1) - t2;
            x(k+1) = x(k+1) + t1;
            y(k+1) = y(k+1) + t2;
        end
    end
end

for i = 1:n/2
    mag(i) = sqrt(x(i)*x(i) + y(i)*y(i));
end

