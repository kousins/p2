function [y] = sin1(x)


%// Source AMS 55, eqn 4.3.97. Handbook of Mathematical Functions, Pub by
%U.S. Dept of Commerce
%float sinx(float x)
%{
%static const float a[] =
%{-.1666666664,.0083333315,-.0001984090,.0000027526,-.0000000239};
%float xsq = x*x;
%float temp = x*(1 + a[0]*xsq + a[1]*xsq*xsq + a[2]* xsq*xsq*xsq
%+a[3]*xsq*xsq*xsq*xsq
%+ a[4]*xsq*xsq*xsq*xsq*xsq);
%return temp;
%}

pi = 3.1416;
x = x - 2*pi*floor(x/(2*pi));

%y = sine[ 180 - x];          /*  90 <= x <= 180  */
%y = -sine[x - 180];          /* 180 <= x <= 270  */
%y = -sine[360 - x];          /* 270 <= x <= 360  */
signal = 1;
if x >= pi/2 && x <= pi
  x = pi-x;
else
  if x > pi && x <= 3*pi/2
    x = x-pi;
    signal = -1;
  else
    if x > 3*pi/2 %&& x <= 2*pi
        x = 2*pi-x;
        signal = -1;
    end
  end
end


%Taylor series
a1 = -0.1666666664;
a2 = 0.0083333315;
a3 = -0.0001984090;
a4 = 0.0000027526;
a5 = -0.0000000239;

xsq = x*x;
temp = x*(1 + a1*xsq + a2*xsq*xsq + a3*xsq*xsq*xsq+a4*xsq*xsq*xsq*xsq+a5*xsq*xsq*xsq*xsq*xsq);

y = signal*temp;
