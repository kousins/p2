%t1=clock;
NO_POINTS = 1024;

y=zeros(1, NO_POINTS/2);

x=zeros(1, NO_POINTS); %[1 0 0 0 0 0 ];


%x(1:16) = 1; %OLD
for u=1:1:16 %NEW
	x(u) = 1;
end

tic;

% using DFT function
y = dft_cos2sin2(x, NO_POINTS);

%clock-t1
toc