function y = sin3(x, table)

%global table;

%sin implemented with a table of 128 values in the [0..pi/2]
%quadrant
%this implementation does not use interpolation


pi=3.1416;
x = x - 2*pi*floor(x/(2*pi));


%y = sine[ 180 - x];          /*  90 <= x <= 180  */
%y = -sine[x - 180];          /* 180 <= x <= 270  */
%y = -sine[360 - x];          /* 270 <= x <= 360  */
signal = 1;
if x >= pi/2 && x <= pi
  x = pi-x;
else
  if x> pi && x <= 3*pi/2
    x = x-pi;
    signal = -1;
  else
    if x > 3*pi/2 %&& x <= 2*pi
        x = 2*pi-x;
        signal = -1;
    end
  end
end


%alfa=int16(127*x*2/pi);
alfa = floor(127*x*2/pi); %needed to avoid fpoint index
temp = table(alfa+1,1);

y = signal*temp;
