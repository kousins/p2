function [mag] = dft_cos5sin5(x, n)
% assume n is a power of 2
%this.nu = (int)((float) oMathFP.log(N)/ (float) 0.301029995663981);


xre = zeros(1,n);
xim = zeros(1,n);

x2 = zeros(1,n);
y2 = zeros(1,n);

mag = zeros(1,n/2);

PI = 3.1415926535897932384626433832795; % constant pi exists in MATLAB
twoPI = 2.0 * PI;

n2=n/2;

for z = 1:n2
    xre(z) = x(z);
    %xim(z) = 0.0;
end

% for z = n2+1:n
%     xre(z) = 0.0;
%     xim(z) = 0.0;
% end

for z = 0:n-1
    arg = twoPI * z / n;
    for k = 0:n-1
        cosarg = cos5(k * arg);
        sinarg = sin5(k * arg);
        x2(z+1) = x2(z+1) + xre(k+1) * cosarg - xim(k+1) * sinarg;
		y2(z+1) = y2(z+1) + xre(k+1) * sinarg + xim(k+1) * cosarg;
    end
end

for z = 1:n/2
mag(z)= sqrt(x2(z)*x2(z) + y2(z)*y2(z));
end

