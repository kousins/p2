function y = cos1(x)

pi2 = 3.1415;
y = -sin1(x-pi2/2);
