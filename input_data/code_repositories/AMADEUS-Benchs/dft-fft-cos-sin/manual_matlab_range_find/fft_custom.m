function [mag] = fft_custom(input, n)
%@@ DENSE range_find BEGIN
y = zeros(1,n);
range_find(y, 'fft.y'); %range find call
x = zeros(1,n);
range_find(x, 'fft.x'); %range find call

mag = zeros(1,n/2);
range_find(mag, 'fft.mag'); %range find call

% precompute tables
cos2 = zeros(1,n/2);
range_find(cos2, 'fft.cos2'); %range find call
sin2 = zeros(1,n/2);
range_find(sin2, 'fft.sin2'); %range find call

PI = 3.1415926535897932384626433832795; % constant pi exists in MATLAB
range_find(PI, 'fft.PI'); %range find call

m=ceil(log2(n));
range_find(m, 'fft.m'); %range find call

for i=1:n/2
range_find(i, 'fft.i'); %range find call
	cos2(i) = cos(-2*PI*(i-1)/n);
range_find(cos2(i), 'fft.cos2'); %range find call
    sin2(i) = sin(-2*PI*(i-1)/n);
range_find(sin2(i), 'fft.sin2'); %range find call
end
%@@ DENSE range_find END

for i = 1:n/2
range_find(i, 'fft.i'); %range find call
    %y(i) = 0;
    x(i) = input(i);
range_find(x(i), 'fft.x'); %range find call
end
% for i = n/2+1:n
%     y(i) = 0;
%     x(i) = 0;
% end


% Bit-reverse
j = 0;
range_find(j, 'fft.j'); %range find call
n2 = n/2;
range_find(n2, 'fft.n2'); %range find call
for i=1:n-1
range_find(i, 'fft.i'); %range find call
    n1 = n2;
range_find(n1, 'fft.n1'); %range find call
    while ( j >= n1 )
        j = j - n1;
range_find(j, 'fft.j'); %range find call
        n1 = n1/2;
range_find(n1, 'fft.n1'); %range find call
    end
    j = j + n1;
range_find(j, 'fft.j'); %range find call

    if i < j
        t1 = x(i+1);
range_find(t1, 'fft.t1'); %range find call
        x(i+1) = x(j+1);
range_find(x(i+1), 'fft.x'); %range find call
        x(j+1) = t1;
range_find(x(j+1), 'fft.x'); %range find call
        t1 = y(i+1);
range_find(t1, 'fft.t1'); %range find call
        y(i+1) = y(j+1);
range_find(y(i+1), 'fft.y'); %range find call
        y(j+1) = t1;
range_find(y(j+1), 'fft.y'); %range find call
    end
end


n1 = 0;
range_find(n1, 'fft.n1'); %range find call
n2 = 1;
range_find(n2, 'fft.n2'); %range find call

for i=0:m-1
range_find(i, 'fft.i'); %range find call
    n1 = n2;
range_find(n1, 'fft.n1'); %range find call
    n2 = n2 + n2;
range_find(n2, 'fft.n2'); %range find call
    a = 1;
range_find(a, 'fft.a'); %range find call

    for j=0:n1-1
range_find(j, 'fft.j'); %range find call
        c = cos2(a);
range_find(c, 'fft.c'); %range find call
        s = sin2(a);
range_find(s, 'fft.s'); %range find call
        
        a = a+bitshift(1, m-i-1); % equivalent to a + 1 << (m-i-1);
range_find(a, 'fft.a'); %range find call

        for k=j:n2:n-1
range_find(k, 'fft.k'); %range find call
            t1 = c*x(k+n1+1) - s*y(k+n1+1);
range_find(t1, 'fft.t1'); %range find call
            t2 = s*x(k+n1+1) + c*y(k+n1+1);
range_find(t2, 'fft.t2'); %range find call
            x(k+n1+1) = x(k+1) - t1;
range_find(x(k+n1+1), 'fft.x'); %range find call
            y(k+n1+1) = y(k+1) - t2;
range_find(y(k+n1+1), 'fft.y'); %range find call
            x(k+1) = x(k+1) + t1;
range_find(x(k+1), 'fft.x'); %range find call
            y(k+1) = y(k+1) + t2;
range_find(y(k+1), 'fft.y'); %range find call
        end
    end
end

for i = 1:n/2
range_find(i, 'fft.i'); %range find call
    mag(i) = sqrt(x(i)*x(i) + y(i)*y(i));
range_find(mag(i), 'fft.mag'); %range find call
end

