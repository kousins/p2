function [y] = cos1_custom(x)

%cos(x)=-sin(x - pi/2)

pi = 3.1415926535897932384626433832795;
range_find(pi, 'cos1.pi'); %range find call
y = -sin1_custom(x-pi/2);range_find(y, 'cos1.y'); %range find call
