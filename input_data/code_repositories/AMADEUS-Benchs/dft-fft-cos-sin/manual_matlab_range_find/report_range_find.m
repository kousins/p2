%
% AMADEUS project, Portugal
%
% Description:
%	This MATLAB code implements a function to report the log
%	performed by applying range_find.
%
% Modification Log:
% 	v0.1, Jo�o M. P. Cardoso, January 2010.
%
%

function report_range_find()

		 global logTable;

		 fprintf(1,'==== report range values of %d variables:\n',length(logTable));


		 find = false;
		 N = length(logTable);

  		 for i = 1:N
  		 	 reg=logTable(i);
  		 	 fprintf(1,'var %s [min, max]: [%g, %g]\n',reg.name, reg.min, reg.max);
		 end
%end

