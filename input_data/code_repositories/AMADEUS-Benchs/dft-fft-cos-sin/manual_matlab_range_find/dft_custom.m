%
% Copyright 2009 - AMADEUS project, Portugal
%
% Description:
%	This MATLAB function implements the Discrete Fourier Transform (DFT)
%	and outputs as result the magnitude of the resultant signal.
%
% Modification Log:
% 	v0.2, Jo�o M. P. Cardoso, January 2010.
%
%

function [mag] = dft_custom(x, n)
% assume n is a power of 2
%this.nu = (int)((float) oMathFP.log(N)/ (float) 0.301029995663981);

%@@ DENSE range_find BEGIN
xre = zeros(1,n); % real part
range_find(xre, 'dft.xre'); %range find call
xim = zeros(1,n); % imaginary part
range_find(xim, 'dft.xim'); %range find call

x2 = zeros(1,n); % output, real part
range_find(x2, 'dft.x2'); %range find call
y2 = zeros(1,n); % output, imaginary part
range_find(y2, 'dft.y2'); %range find call

mag = zeros(1,n/2); % the magintude of the resultant signal
range_find(mag, 'dft.mag'); %range find call

PI = 3.1415926535897932384626433832795; % constant pi exists in MATLAB
range_find(PI, 'dft.PI'); %range find call
twoPI = 2.0 * PI;
range_find(twoPI, 'dft.twoPI'); %range find call

n2=n/2;
range_find(n2, 'dft.n2'); %range find call

for i = 1:n2
range_find(i, 'dft.i'); %range find call
    xre(i) = x(i);
range_find(xre(i), 'dft.xre'); %range find call
    %xim(i) = 0.0; 
end

% for i = n2+1:n
%     xre(i) = 0.0;
%     xim(i) = 0.0;
% end

for i = 0:n-1
range_find(i, 'dft.i'); %range find call
%    x2(i+1) = 0;
%    y2(i+1) = 0;
    arg = twoPI * i / n;
range_find(arg, 'dft.arg'); %range find call

    for k = 0:n-1
range_find(k, 'dft.k'); %range find call
        cosarg = cos(k * arg);
range_find(cosarg, 'dft.cosarg'); %range find call
        sinarg = sin(k * arg);
range_find(sinarg, 'dft.sinarg'); %range find call

        x2(i+1) = x2(i+1) + xre(k+1) * cosarg - xim(k+1) * sinarg;
range_find(x2(i+1), 'dft.x2'); %range find call
        y2(i+1) = y2(i+1) + xre(k+1) * sinarg + xim(k+1) * cosarg;
range_find(y2(i+1), 'dft.y2'); %range find call
    end
end

for i = 1:n/2
range_find(i, 'dft.i'); %range find call
    mag(i)= sqrt(x2(i)*x2(i) + y2(i)*y2(i));
range_find(mag(i), 'dft.mag'); %range find call
end
%@@ DENSE range_find END
