function [y] = sin1_custom(x)


%// Source AMS 55, eqn 4.3.97. Handbook of Mathematical Functions, Pub by
%U.S. Dept of Commerce
%float sinx(float x)
%{
%static const float a[] =
%{-.1666666664,.0083333315,-.0001984090,.0000027526,-.0000000239};
%float xsq = x*x;
%float temp = x*(1 + a[0]*xsq + a[1]*xsq*xsq + a[2]* xsq*xsq*xsq
%+a[3]*xsq*xsq*xsq*xsq
%+ a[4]*xsq*xsq*xsq*xsq*xsq);
%return temp;
%}

%@@ DENSE range_find BEGIN

x = x - 2*pi*floor(x/(2*pi));
range_find(x, 'sin1.x'); %range find call

%y = sine[ 180 - x];          /*  90 <= x <= 180  */
%y = -sine[x - 180];          /* 180 <= x <= 270  */
%y = -sine[360 - x];          /* 270 <= x <= 360  */
signal = 1;
range_find(signal, 'sin1.signal'); %range find call
if x >= pi/2 && x <= pi
  x = pi-x;
range_find(x, 'sin1.x'); %range find call
else
  if x > pi && x <= 3*pi/2
    x = x-pi;
range_find(x, 'sin1.x'); %range find call
    signal = -1;
range_find(signal, 'sin1.signal'); %range find call
  else
    if x > 3*pi/2 %&& x <= 2*pi
        x = 2*pi-x;
range_find(x, 'sin1.x'); %range find call
        signal = -1;
range_find(signal, 'sin1.signal'); %range find call
    end
  end
end


%Taylor series
a1 = -0.1666666664;
range_find(a1, 'sin1.a1'); %range find call
a2 = 0.0083333315;
range_find(a2, 'sin1.a2'); %range find call
a3 = -0.0001984090;
range_find(a3, 'sin1.a3'); %range find call
a4 = 0.0000027526;
range_find(a4, 'sin1.a4'); %range find call
a5 = -0.0000000239;
range_find(a5, 'sin1.a5'); %range find call

xsq = x*x;
range_find(xsq, 'sin1.xsq'); %range find call
temp = x*(1 + a1*xsq + a2*xsq*xsq + a3*xsq*xsq*xsq+a4*xsq*xsq*xsq*xsq+a5*xsq*xsq*xsq*xsq*xsq);
range_find(temp, 'sin1.temp'); %range find call

y = signal*temp;
range_find(y, 'sin1.y'); %range find call
%@@ DENSE range_find END
