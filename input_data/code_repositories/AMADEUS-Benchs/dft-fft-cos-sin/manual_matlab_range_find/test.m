function testdft;

NO_POINTS = 256;

y1=zeros(1, NO_POINTS/2);
y2=zeros(1, NO_POINTS/2);
y3=zeros(1, NO_POINTS/2);

x=zeros(1, NO_POINTS); %[1 0 0 0 0 0 ];
x(1:16) = 1;
t = (1:1:NO_POINTS);

%t = (0:1/1000:NO_POINTS/1000-1/1000);             % Time vector
%x = sin(2*pi*30*t) + sin(2*pi*80*t);% Signal
%x = sin(2*pi*30*t);% Signal

% using MATLAB built-in FFT function
y1 = fft(x, NO_POINTS);

% using DFT function
%y2 = dft_custom(x, NO_POINTS);

% using FFT function
y3 = fft_custom(x, NO_POINTS);


t1=(1:NO_POINTS/2);
figure(1);
plot(t1, x(1:NO_POINTS/2), '-', t1, abs(y1(1:NO_POINTS/2)), '.', t1, abs(y2(1:NO_POINTS/2)), 'o', t1, abs(y3(1:NO_POINTS/2)), '+');

%plot(t1, x(1:NO_POINTS/2), '-', t1, abs(y1(1:NO_POINTS/2)), '.', t1, abs(y3(1:NO_POINTS/2)), '+');

report_range_find();