function testdft;

NO_POINTS = 256;
NO_COEF = NO_POINTS/4;


x=zeros(1, NO_POINTS); %[1 0 0 0 0 0 ];
x(1:16) = 1;

%t = (0:1/1000:NO_POINTS/1000-1/1000);             % Time vector
%x = sin(2*pi*30*t) + sin(2*pi*80*t);% Signal
%x = sin(2*pi*30*t);% Signal

% using MATLAB built-in FFT function
tic;
y1 = fft(x, NO_POINTS);
fft_elapsed = toc;

% using >Goertzel, function
tic;
y2 = goertzel(x, NO_POINTS, NO_COEF);
goertzel_elapsed = toc;

t1=(1:NO_POINTS/2);
%figure(1);
%plot(t1, abs(y1(1:NO_POINTS/2)), '.', t1, abs(y2(1:NO_POINTS/2)), 'o');
%plot(t1, x(1:NO_POINTS/2), '-', t1, abs(y1(1:NO_POINTS/2)), '.', t1, abs(y2(1:NO_POINTS/2)), 'o');



sprintf('FFT=%f\nGOERTZEL=%f\nDifference=%f', fft_elapsed, goertzel_elapsed, (goertzel_elapsed - fft_elapsed))