%
% Copyright 2009 - AMADEUS project, Portugal
%
% Description:
%	This MATLAB function implements the Goertzel algorithm to
%	compute the kth DFT coefficient.
%
% Source:
%        based on the article:
%        "PRODUCT HOW-TO: Efficient Fixed-Point Implementation of 
%        the Goertzel Algorithm on a Blackfin DSP"
%         By Hazarathaiah Malepati and Yosi Stein
%         Embedded.com
%         (01/18/10, 05:56:00 PM EST)
%        URL: http://www.embedded.com/design/embeddeddsp/222301344
%
%
% Modification Log:
% 	v0.1, Jo�o M. P. Cardoso, January 2010.
%
%

function [y] = goertzel(x, n, num_coef)


% the result matrixcz
y = zeros(n/2);



for k=1:1:num_coef

    q = 2*cos(2*pi*k/n);
    
    c=0;
    b=0;
    
    for i=1:1:n
        a = x(i)+q*b-c;
        c=b;
        b=a;
    end
        
    y(k) = a;
    
end

