function [y] = dft_specialized_uniform_1(x, MAGNITUDE, FRACTION)

y=zeros(size(x));

N=length(x);

t=(0:N-1)/N;
quant1=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
%@@ DENSE quantize BEGIN
t=quantize(quant1, t);

pi_fix = quantize(quant1, pi);


for k=1:N
  v1 = quantize(quant1, (k-1)*t);
  v2 = quantize(quant1, pi_fix*v1);
  v3 = quantize(quant1, -j*2*v2);
  v4 = quantize(quant1, exp(v3));
  v5 = quantize(quant1, x.*v4);
  y(k) = quantize(quant1, sum(v5));
 

  %y(k) = quantize(quant1, sum(x.*exp(-j*2*pi*(k-1)*t)));
end
%@@ DENSE quantize END
