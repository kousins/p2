%
% Copyright 2009 - AMADEUS project, Portugal
%
% Description:
%   This MATLAB function implements the Discrete Fourier Transform (DFT).
%
% Modification Log:
%   v0.2, Joao M.P. Cardoso, January 2010.
%

function [y] = dft(x)
y=zeros(size(x));
N=length(x);
t=(0:N-1)/N;
for k=1:N
   y(k) = sum(x.*exp(-j*2*pi*(k-1)*t));
end