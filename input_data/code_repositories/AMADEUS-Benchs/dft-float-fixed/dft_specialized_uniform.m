function [y] = dft_specialized_uniform(x, MAGNITUDE, FRACTION)

y=zeros(size(x));

N=length(x);
%@@ DENSE quantizer BEGIN
t=(0:N-1)/N;
quant1=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
%@@ DENSE quantize BEGIN
t=quantize(quant1, t);

quant2=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
pi_fix = quantize(quant2, pi);

quant3=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
quant4=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
quant5=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
quant6=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
quant7=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
quant8=quantizer('fixed', 'floor', 'wrap', [MAGNITUDE FRACTION]);
%@@ DENSE quantizer END
for k=1:N
  v1 = quantize(quant3, (k-1)*t);
  v2 = quantize(quant4, pi_fix*v1);
  v3 = quantize(quant5, -j*2*v2);
  v4 = quantize(quant6, exp(v3));
  v5 = quantize(quant7, x.*v4);
  y(k) = quantize(quant8, sum(v5));
end
%@@ DENSE quantize END