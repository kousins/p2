function testdft;

NO_POINTS = 256;

x=zeros(1, NO_POINTS); %[1 0 0 0 0 0 ];
x(1:16) = 1;
t = (1:1:NO_POINTS);

%t = (0:1/1000:NO_POINTS/1000-1/1000);             % Time vector
%x = sin(2*pi*30*t) + sin(2*pi*80*t);% Signal
%x = sin(2*pi*30*t);% Signal


% mode = 'fixed';
% overflow ='saturate'; %wrap
% round = 'floor';
% format = [10 5];
% 
% quantizerProp=quantizer(mode, round, overflow, format);
% x = quantize(quantizerProp, x)
%x=fi(x, 'RoundMode', 'floor', 'OverflowMode','wrap');

%y=fft(x, NO_POINTS);

% double
y1=dft(x);

% fixed-point uniform
%x2=fi(x, 1, 15, 8);
%y2=dft(x2);

% fixed-point specialized-uniform
quant1=quantizer('fixed', 'floor', 'wrap', [24 16]);
x2 = quantize(quant1, x);
y2=dft_specialized_uniform(x2, 24, 10);

% fixed-point specialized-uniform
quant1=quantizer('fixed', 'floor', 'wrap', [24 16]);
x3 = quantize(quant1, x);
y3=dft_specialized_uniform_1(x3, 24, 8);

% fixed-point specialized
%quant1=quantizer('fixed', 'floor', 'wrap', [16 8]);
%x3 = quantize(quant1, x);
%y3=dft_specialized(x3);


t1=(1:NO_POINTS/2);

%figure(1);
%plot(t, x, '-', t, abs(y),'.');

%plot([0:length(x)-1], x, '-', [0:length(y1)-1], abs(y1), '+');

figure(1);
plot(t1, x(1:NO_POINTS/2), '-', t1, abs(y1(1:NO_POINTS/2)), '.', t1, abs(y2(1:NO_POINTS/2)), 'o', t1, abs(y3(1:NO_POINTS/2)), '+');

%figure(1);
%plot(t1, x, '-', t1, abs(y1), '.', t1, abs(y2), '*');

%figure(2);
%plot([0:length(y1)-1], abs(y2- y1), '+', [0:length(y1)-1], abs(y3 -y1), '*');