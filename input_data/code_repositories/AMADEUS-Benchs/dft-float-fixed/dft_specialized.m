function [y] = dft_specialized(x)

y=zeros(size(x));

N=length(x);
%@@ DENSE quantizer BEGIN
%@@ DENSE quantize BEGIN
t=(0:N-1)/N;
quant1=quantizer('fixed', 'floor', 'wrap', [18 16]);
t=quantize(quant1, t);

quant2=quantizer('fixed', 'floor', 'wrap', [23 20]);
pi_fix = quantize(quant2, pi);

quant3=quantizer('fixed', 'floor', 'wrap', [20 8]);
quant4=quantizer('fixed', 'floor', 'wrap', [23 10]);
quant5=quantizer('fixed', 'floor', 'wrap', [24 10]);
quant6=quantizer('fixed', 'floor', 'wrap', [26 12]);
quant7=quantizer('fixed', 'floor', 'wrap', [28 14]);
quant8=quantizer('fixed', 'floor', 'wrap', [32 16]);
%@@ DENSE quantizer END
for k=1:N
  v1 = quantize(quant3, (k-1)*t);
  v2 = quantize(quant4, pi_fix*v1);
  v3 = quantize(quant5, -j*2*v2);
  v4 = quantize(quant6, exp(v3));
  v5 = quantize(quant7, x.*v4);
  y(k) = quantize(quant8, sum(v5));
end
%@@ DENSE quantize END
