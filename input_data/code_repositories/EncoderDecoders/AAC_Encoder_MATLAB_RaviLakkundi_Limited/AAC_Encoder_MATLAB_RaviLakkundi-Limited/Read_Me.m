%   E for Codeword, Leaf End
%   HCB_SF Table generation
%                                                   1
%                                   1(E)                             2
%                                             3                                              4 
%                                        5(E)      6                        7                                 8
%                                             9(E)   10(E)          11(E)            12              13              14
%                                                                                  15  16        17       18      19      20
%                                                                                             21(E)  22                                                   



% If the code is 111000, then its decoded as
% Node - 2 - 4 - 8 - 13 - 17 - 21


%           Huffman code book choosing
% 
%           Tuple size  Maxabsvalue   Signed
% 0               0
% 1               4       1           yes
% 2               4       1           yes
% 3               4       2           no
% 4               4       2           no
% 5               2       4           yes
% 6               2       4           yes
% 7               2       7           no
% 8               2       7           no
% 9               2       12          no
% 10              2       12          no
% 11              2       16(ESC)     no
%             Huffman Codebooks.


% Header writing, from FAAC
% 
%         /* Fixed ADTS header */
%         PutBit(bitStream, 0xFFFF, 12); /* 12 bit Syncword */
%         PutBit(bitStream, hEncoder->config.mpegVersion, 1); /* ID == 0 for MPEG4 AAC, 1 for MPEG2 AAC */
%         PutBit(bitStream, 0, 2); /* layer == 0 */
%         PutBit(bitStream, 1, 1); /* protection absent */
%         PutBit(bitStream, hEncoder->config.aacObjectType, 2); /* profile */
%         PutBit(bitStream, hEncoder->sampleRateIdx, 4); /* sampling rate */
%         PutBit(bitStream, 0, 1); /* private bit */
%         PutBit(bitStream, hEncoder->numChannels, 3); /* ch. config (must be > 0) */
%                                                      /* simply using numChannels only works for
%                                                         6 channels or less, else a channel
%                                                         configuration should be written */
%         PutBit(bitStream, 0, 1); /* original/copy */
%         PutBit(bitStream, 0, 1); /* home */
%         /* Variable ADTS header */
%         PutBit(bitStream, 0, 1); /* copyr. id. bit */
%         PutBit(bitStream, 0, 1); /* copyr. id. start */
%         PutBit(bitStream, hEncoder->usedBytes, 13);
%         PutBit(bitStream, 0x7FF, 11); /* buffer fullness (0x7FF for VBR) */
%         PutBit(bitStream, 0, 2); /* raw data blocks (0+1=1) */
% 
%     /*
%      * MPEG2 says byte_aligment() here, but ADTS always is multiple of 8 bits
%      * MPEG4 has no byte_alignment() here
%      */
%     /*
%     if (hEncoder->config.mpegVersion == 1)
%         bits += ByteAlign(bitStream, writeFlag);
%     */

