function [coeff] = slowmdct(block);
sz = size(block);
n = sz(2);
if (sz(1) > sz(2))
  block = block';
  n = sz(1);
end
coeff = zeros(1,n/2);
for m = 0:n/2-1
  xt = 0;
  for k = 0:n-1
    xt = xt + block(k+1)*cos(pi/(2*n)*(2*k+1+n/2)*(2*m+1));
  end
  coeff(m+1) = xt;
end

