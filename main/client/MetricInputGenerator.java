package client;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ListIterator;
import java.util.Random;

import cccfinder.pairs.ParserTokens;
import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.Toolbox;

public class MetricInputGenerator {

	private static final String INPUT_PATH = "C:\\Users\\Bruno Palma LT\\Desktop\\Tese MIEI\\code_repositories\\Code Repository";

	private static final String OUTPUT_FILENAME = "C:\\Users\\Bruno Palma LT\\Desktop\\Tese MIEI\\resultados cccexplorer\\metrics_input2.csv";
			
	@SuppressWarnings("resource")
	private static void generateMetricsExample(CollectedData data) throws IOException {
		FileWriter writer = new FileWriter(OUTPUT_FILENAME);
		Random rand = new Random();
		Toolbox toolbox = data.getTopToolbox();
		writer.append("M-File Name");
		writer.append(";");
		writer.append("Path");
		writer.append(";");
		writer.append("Pairs of if and nargin");
		writer.append(";");
		writer.append("size");
		writer.append(";");
		writer.append("eval");
		writer.append(";");
		writer.append("for");
		writer.append(";");
		writer.append("while");
		writer.append(";");
		writer.append("function");
		writer.append(";");
		writer.append("Random");
		writer.append(";");
		writer.append("cnt");
		writer.append('\n');
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			int count1 = 0;
			int count2 = 0;
			int count3 = 0;
			int count4 = 0;
			int count5 = 0;
			int count6 = 0;
			int sumCount = 0;
			int randomNumber = rand.nextInt(50);
			ParserTokens pt = new ParserTokens(mFile.getFile());
			LexicalAnalyser la = pt.processMFile2();			
			ListIterator<Token> li = la.listIterator();
			while(li.hasNext()){
				Token first = li.next();
				if(first.getValue().contentEquals("if")){
					Token second = li.next();
					if(second.getValue().contentEquals("nargin")){
						count1++;	
					}
				}
				if(first.getValue().contentEquals("size")){
					count2++;
				}
				if(first.getValue().contentEquals("eval")){
					count3++;
				}
				if(first.getValue().contentEquals("for")){
					count4++;
				}
				if(first.getValue().contentEquals("while")){
					count5++;
				}
				if(first.getValue().contentEquals("function")){
					count6++;
				}
			}
			
			sumCount = count1 + count2 + count3 + count4 + count5 + count6;
			
			writer.append(mFile.getName());
			writer.append(';');
			writer.append(mFile.getPathname());
			writer.append(';');
			writer.append(String.valueOf(count1));
			writer.append(';');
			writer.append(String.valueOf(count2));
			writer.append(';');
			writer.append(String.valueOf(count3));
			writer.append(';');
			writer.append(String.valueOf(count4));
			writer.append(';');
			writer.append(String.valueOf(count5));
			writer.append(';');
			writer.append(String.valueOf(count6));
			writer.append(';');
			writer.append(String.valueOf(randomNumber));
			writer.append(';');
			writer.append(String.valueOf(sumCount));
			writer.append('\n');
		}
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		CollectedData data = new CollectedData(INPUT_PATH);
	    generateMetricsExample(data);
	    System.out.print("Done.");
	}
	
}
