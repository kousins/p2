package client;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.MFunction;
import cccfinder.tool.Toolbox;

public class IterateThroughFunctions {

	private static final String INPUT_PATH = "input_data\\code_repositories";
	private static final String OUTPUT_FILENAME = "output\\functions.csv";

	@SuppressWarnings("resource")
	private static void testMFunctionExtraction(CollectedData data) throws IOException {
		Toolbox toolbox = data.getTopToolbox();
		FileWriter writer = new FileWriter(OUTPUT_FILENAME);
		writer.append("Function Name");
		writer.append(";");
		writer.append("M-File Name");
		writer.append(";");
		writer.append("Domain Name");
		writer.append(";");
		writer.append("No. Tokens");
		writer.append('\n');
		util.ProgressDisplay.progressDisplayOn();
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			util.ProgressDisplay.updateProgress();
			//proccess functions for mfile
			mFile.extractMFunctions();
			
			Iterator<MFunction> funcIt = mFile.funcIterator();
			while(funcIt.hasNext()){
				MFunction mFun = funcIt.next();
				writer.append(mFun.getName());
				writer.append(';');
				writer.append(mFun.getMFileName());
				writer.append(';');
				writer.append(mFun.getDomainName());
				writer.append(';');
				writer.append(String.valueOf(mFun.getTokenCount()));
				writer.append('\n');
			}
		}
		writer.close();
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		CollectedData data = new CollectedData(INPUT_PATH);
		System.out.print("Extracting functions from m-files...\n");
	    testMFunctionExtraction(data);
	    System.out.print("\nDone.");
	}	
}
