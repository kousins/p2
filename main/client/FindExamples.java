package client;

import java.io.IOException;
import java.util.TreeSet;

import cccfinder.density.MFilePlusTokens;
import cccfinder.token_classifier.token_table.TokenTable;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.TokenWithStatistics;
import cccfinder.tool.Toolbox;
import util.ProgressDisplay;

/**
 * This program locates the m-files
 * 
 * @author Miguel P. Monteiro
 */
public class FindExamples {
	private static final String INPUT_PATH = "input_data\\code_repositories";
	private static final String TABLE_PATH = "dat\\token_table.txt";
	private static final String[] CONCERNS = { "Messages and monitoring",
			"I/O data", "Verification of function arguments and return values",
			"Data type verification and specialization", "System",
			"Memory allocation/deallocation", "Parallelization",
			"Dynamic properties" };

	static {
		ProgressDisplay.progressDisplayOn();
		ProgressDisplay.outputOn();
	}

	static int calcTotal(TreeSet<TokenWithStatistics> set) {
		int total = 0;
		for (TokenWithStatistics tok : set)
			total += tok.getCount();
		return total;
	}

	private static interface IEvaluate {
		void setMFile(MFile mFile);
		boolean eval(TreeSet<TokenWithStatistics> set, MFilePlusTokens collector);
	}

	static class MostDistinctTokens implements IEvaluate {
		public boolean eval(TreeSet<TokenWithStatistics> set,
				MFilePlusTokens collector) {
			return set.size() > collector.size();
		}
		public void setMFile(MFile mFile) {
		}
	}

	private static class HighestCounts implements IEvaluate {
		public boolean eval(TreeSet<TokenWithStatistics> set,
				MFilePlusTokens collector) {
			int total = calcTotal(set);
			return total > collector.totals();
		}
		public void setMFile(MFile mFile) {
		}
	}

	private static MFilePlusTokens find(CollectedData data, TokenTable table,
			String concern, IEvaluate evaluate) {
		MFilePlusTokens collector = new MFilePlusTokens(concern);
		Toolbox toolbox = data.getTopToolbox();
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			TreeSet<TokenWithStatistics> tokenSet = new TreeSet<TokenWithStatistics>();

			for (TokenWithStatistics tok : mFile)
				if (table.concernHasToken(concern, tok.getValue()))
					tokenSet.add(tok);
			if (evaluate.eval(tokenSet, collector))
				collector.update(tokenSet, mFile);
		}
		return collector;
	}

	private static void findAndReport(TokenTable table, CollectedData data,
			String concern, IEvaluate eval) {
		MFilePlusTokens collector = find(data, table, concern, eval);
		ProgressDisplay.println(collector.toString() + "\n");
	}

	private static void findForAllConcerns(TokenTable table,
			CollectedData data, IEvaluate eval) {
		for (String concern : CONCERNS)
			findAndReport(table, data, concern, eval);
	}

	private static void findDensity(TokenTable table, CollectedData data,
			String concern) {
		MFilePlusTokens collector = new MFilePlusTokens(concern);
		Toolbox toolbox = data.getTopToolbox();
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			TreeSet<TokenWithStatistics> tokenSet = new TreeSet<TokenWithStatistics>();

			for (TokenWithStatistics tok : mFile)
				if (table.concernHasToken(concern, tok.getValue()))
					tokenSet.add(tok);
			double density = ((double) calcTotal(tokenSet)) / mFile.nrLines();
			if (density > collector.density())
				collector.update(tokenSet, mFile);
		}
		ProgressDisplay.println(collector + "\n");
	}

	private static void findDensityForAllConcerns(TokenTable table,
			CollectedData data) {
		for (String concern : CONCERNS) {
			System.out.println(concern + ":");
			findDensity(table, data, concern);
		}
	}

	static final String SEPARATOR_BAR = "-----------------------------------------------------------------------";

	public static void main(String[] args) throws IOException {
		TokenTable table = new TokenTable(TABLE_PATH);
		CollectedData data = new CollectedData(INPUT_PATH);
		ProgressDisplay.println(data.getTopToolbox().getNrMFiles()
				+ " m-files processed.");
		ProgressDisplay.println("Creation of CollectedData object completed.\n");

		ProgressDisplay.println("m-files with most disticnt tokens:");
		findForAllConcerns(table, data, new MostDistinctTokens());

		ProgressDisplay.println("\n" + SEPARATOR_BAR + "\n");
		ProgressDisplay.println("m-files with the highest count of tokens:");
		findForAllConcerns(table, data, new HighestCounts());

		ProgressDisplay.println("\n" + SEPARATOR_BAR + "\n");
		ProgressDisplay.println("m-files with the greatest density of tokens:");
		findDensityForAllConcerns(table, data);

		ProgressDisplay.println("Terminated.");
	}
}
