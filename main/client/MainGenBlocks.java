package client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import cccfinder.token_classifier.blocks.BlockManager;
import cccfinder.token_classifier.blocks.Domain;
import cccfinder.token_classifier.lexical_analyser.Parser;
import cccfinder.token_classifier.lexical_analyser.Tag;
import cccfinder.token_classifier.lexical_analyser.Token;

import util.FileUtil;
import util.ProgressDisplay;

public class MainGenBlocks {
	private static final String INPUT_PATH = "input_data\\code_repositories";
	private static final String OUTPUT_PATH = "BLOCKS";

	private static final String DOMAIN_FILENAME = "domain.txt";
	private static String domain = "---";

	static class InternalPrintFacility {
		static void printTags(PrintStream out, File file, Parser parser) {
			out.print(file.getPath() + ": ");

			Iterator<String> it = parser.iteratorARGs();
			if (it.hasNext()) {
				out.print("ARGS: ");
				while (it.hasNext())
					out.print(it.next() + " ");
				out.println();
			} else
				out.println("No ARGS");
		}
		static void printFUNCS(Parser parser) {
			Iterator<String> it = parser.iteratorFUNCs();
			if (it.hasNext()) {
				ProgressDisplay.print("FUNCS: ");
				while (it.hasNext())
					ProgressDisplay.print(it.next() + " ");
				ProgressDisplay.println();
			} else
				ProgressDisplay.println("No FUNCs ");
		}
		static void printVARS(Parser parser) {
			Set<String> set = parser.getVars();
			System.out.print(parser.getMFile().getName() + ":");
			for (String tok : set)
				System.out.print(" " + tok);
			System.out.println();
		}

		static int xxx = 0;
		static void reportBlocks(File mFile, BlockManager blockManager,
				Set<String> vars, Set<String> funcs, Set<String> args) {
			xxx++;
			if(xxx != 1) return;
			System.out.println();
			System.out.println("VARs: " + vars.size());
			System.out.println("FUNCs: " + funcs.size());
			System.out.println("ARGs: " + args.size());
			System.out.print(mFile.getName() + ":\n" + 
					mFile + ":\n" + 
					blockManager.size() + " pairs\n" +
					blockManager.nrBlocks() + " blocks\n" +
					blockManager.hashCode() + " domains.\n");
		}
	}

	private static File getDomainTextFile(File[] files) {
		for (File f : files)
			if (f.getName().equals(DOMAIN_FILENAME))
				return f;
		return null;
	}

	/*
	 * This method assumes as pre-condition that argument folder is a directory.
	 * It looks among the files enclosed within that directory for a text file
	 * named DOMAIN_FILENAME and reads its first line as the application's
	 * domain (or perhaps the toolbox's) assigned to domain field. If no file
	 * named DOMAIN_FILENAME is found, domain keeps its previous value. This
	 * way, the domain tag propagates through the folder hierarchy.
	 */
	private static void updateDomain(File folder, BlockManager blockManager) {
		File[] files = folder.listFiles();
		File domainFile = getDomainTextFile(files);
		if (domainFile != null) // else, keep domain as it is.
			try {
				Scanner scanner = new Scanner(domainFile);
				domain = scanner.nextLine();
				blockManager.newDomain(domain);
				scanner.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when trying to load domain at "
						+ domainFile.getAbsolutePath());
			}
	}

	private static void generateBlockFolder(BlockManager bm, String folderPath)
			throws FileNotFoundException {
		File dir = new File(folderPath);
		dir.mkdir();
		Iterator<Domain> it = bm.iterator();
		while (it.hasNext()) {
			Domain dom = it.next();
			PrintStream pr = new PrintStream(folderPath + "\\" + dom.getName()
					+ ".tk");
			dom.print(pr);
			pr.close();
		}
	}

	private static void processFolders(File folder, BlockManager blockManager) throws IOException {
		//First, process all m-files at this folder level
		List<File> mFiles = FileUtil.getMFiles(folder);
		for(File mFile : mFiles) {
			blockManager.newMFile(mFile.getName());

			Parser parser = new Parser(mFile);
			parser.processMFile();

			Iterator<Token> it = parser.taggedTokensIterator();
			while(it.hasNext()) {
				Token tok = it.next();
				if(tok.getTag() != Tag.NWD)
					blockManager.mark(tok.getValue(), tok.getTag());
			}
		}

		//Next, process all sub-folders
		List<File> folders = FileUtil.getSubFolders(folder);
		for(File dir : folders) {
			updateDomain(dir, blockManager);
			blockManager.newToolbox(dir.getName());
			processFolders(dir, blockManager);
		}
	}

	public static void main(String[] args) throws IOException {
		BlockManager blockManager = new BlockManager();
		File path = new File(INPUT_PATH);

		ProgressDisplay.outputOn();
		ProgressDisplay.progressDisplayOn();
		if (!path.isDirectory()) {
			System.out.println("Path should be a folder:\n" + path.getName());
			System.exit(0);
		}
		ProgressDisplay.println("Processing [" + path.getPath() + "]\n");
		processFolders(path, blockManager);

		ProgressDisplay.println("\n" + blockManager.nrBlocks()
				+ " blocks processed.");
		// actually print the block files
		ProgressDisplay.print("Generating block files...");
		// build block folder and generate block files
		generateBlockFolder(blockManager, OUTPUT_PATH);
		ProgressDisplay.println(" done");

		ProgressDisplay.println("\nTerminated.");
	}
}
