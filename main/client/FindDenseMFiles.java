package client;

import java.io.IOException;
import java.util.TreeSet;

import cccfinder.density.DenseMFilesShortlist;
import cccfinder.density.MinimumLoCFilter;

import cccfinder.token_classifier.token_table.TokenTable;
import cccfinder.tool.CollectedData;
import cccfinder.tool.IFilter;
import cccfinder.tool.MFile;
import cccfinder.tool.TokenWithStatistics;
import cccfinder.tool.Toolbox;

import util.ProgressDisplay;

/**
 * This program locates the m-files
 * 
 * @author Miguel P. Monteiro
 */
public class FindDenseMFiles {
	private static final String INPUT_PATH = "input_data\\code_repositories";
	private static final String TABLE_PATH = "dat\\token_table.txt";
	private static final String[] CONCERNS = { "Messages and monitoring",
			"I/O data", "Verification of function arguments and return values",
			"Data type verification and specialization", "System",
			"Memory allocation/deallocation", "Parallelization",
			"Dynamic properties" };

	static {
		ProgressDisplay.progressDisplayOn();
		ProgressDisplay.outputOn();
	}

	static int calcTotal(TreeSet<TokenWithStatistics> set) {
		int total = 0;
		for (TokenWithStatistics tok : set)
			total += tok.getCount();
		return total;
	}

	private static void findDenseMFiles(TokenTable table, CollectedData data,
			String concern, int maximum, IFilter filter) {
		DenseMFilesShortlist shortlist = new DenseMFilesShortlist(concern, maximum);
		Toolbox toolbox = data.getTopToolbox();
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			if(!filter.itPasses(mFile))
				continue;
			TreeSet<TokenWithStatistics> tokenSet = new TreeSet<TokenWithStatistics>();

			for (TokenWithStatistics tok : mFile)
				if (table.concernHasToken(concern, tok.getValue()))
					tokenSet.add(tok);
			shortlist.update(tokenSet, mFile);
		}
		ProgressDisplay.println(shortlist.size() + " m-files selected\n" + 
				shortlist + "\n");
	}

	static void findDensityForAllConcerns(TokenTable table,
			CollectedData data, int maximum, IFilter filter) {
		for (String concern : CONCERNS) {
			System.out.println("\n" + concern + ":");
			findDenseMFiles(table, data, concern, maximum, filter);
		}
	}

	public static void main(String[] args) throws IOException {
		TokenTable table = new TokenTable(TABLE_PATH);
		CollectedData data = new CollectedData(INPUT_PATH);
		ProgressDisplay.println(data.getTopToolbox().getNrMFiles()
				+ " m-files processed.");
		ProgressDisplay.println("\nm-files with the greatest density of tokens:");
		findDensityForAllConcerns(table, data, 5, new MinimumLoCFilter());
//		final String CONCERN = "Data type verification and specialization";
//		findDenseMFiles(table, data, CONCERN, 5, new MinimumLoCFilter());
		ProgressDisplay.println("Terminated.");
	}
}
