package client;

import java.io.FileNotFoundException;
import java.io.IOException;

import util.ProgressDisplay;
import cccfinder.output_view.CSVGen;
import cccfinder.output_view.DataOutputGen;
import cccfinder.tool.CollectedData;

/**
 * Simple driver to call CCCFinder in filter mode. Filter mode - extracts only
 * tokens referred in text token file The filter mode is based on the Strategy
 * class FilteringTokenChecker.
 * 
 * @author Miguel P. Monteiro
 */
public class MainBigCSV {
	private static final String INPUT_PATH = "input_data\\code_repositories";

	// Folder where output files (csv, xls) are to be created.
	private static final String OUTPUT_PATH = "output";
	private static final String OUTPUT_FILENAME = "funcs_stats_cr";

	private static void reportSystemMemory() {
		ProgressDisplay.println("Memory available for the JVM: "
				+ Runtime.getRuntime().maxMemory() + " bytes.");
	}

	private static void reportResultValues(CollectedData data) {
		ProgressDisplay.println("\n" + data.totalMFiles() + " total m-files processed.");
		ProgressDisplay.println(data.totalLines() + " total source code lines");
	}

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		ProgressDisplay.outputOn();
		ProgressDisplay.progressDisplayOn();
		reportSystemMemory();
		System.out.println("Output: " + OUTPUT_PATH + "\\" + OUTPUT_FILENAME + ".*");

		System.out.println("Processing started.");
		CollectedData data = new CollectedData(INPUT_PATH);
		reportResultValues(data);

		System.out.println("Writing data on the CSV file...");
		DataOutputGen csvGen = new CSVGen(data, OUTPUT_PATH, OUTPUT_FILENAME);
		csvGen.generateOutput();

		System.out.println("Terminated.");
	}
}
