package exps;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;


public class DropboxFileProcessorTest {
	private static final String PATH_FILE =
		"%DROPBOX%\\AMADEUS-AspectMining\\Code-Repositories";

	public static void main(String[] args)
	throws FileNotFoundException, IOException {
		Scanner scanner = new Scanner(new FileReader(PATH_FILE + "\\TESTE.txt"));
		PrintWriter pw = new PrintWriter(PATH_FILE + "\\COPY.txt");
		String line = null;
		System.out.println("STARTING:");
		while(scanner.hasNextLine()) {
			line = scanner.nextLine();
			System.out.println(line);
			pw.println(line);
		}
		pw.close();
		scanner.close();
		System.out.println("OVER.");
	}
}
