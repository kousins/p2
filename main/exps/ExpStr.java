package exps;

public class ExpStr {

	/**
	 * Extracts the toolbox name from the 1st argument, which is a complete path.
	 * This operation also must know the input path, i.e., the part in the path
	 * that corresponds to the folder where the toolboxes are located. The input
	 * path is provided by the 2nd argument. 
	 */
	public static String extractToolboxName(String path, String inputPath) {
		String result = "";
		//remove input path
		result = path.substring(path.indexOf(inputPath) + inputPath.length());
	
		//Remove one more folder level from the start of the string
		result = result.substring(result.indexOf('\\') + 1);
		result = result.substring(result.indexOf('\\') + 1);
	
		//Start of string should now contain toolbox name.
		//clip the subsequent characters, from the following '\'.
		return result.substring(0, result.indexOf('\\'));
	}
}
