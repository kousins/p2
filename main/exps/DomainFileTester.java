package exps;

import java.io.File;
import java.io.FileNotFoundException;
//import java.io.IOException;
import java.util.Scanner;

public class DomainFileTester {
	private static final String KPMTOOLS_PATH = "test_data\\KPMtools";
	private String _path;

	public DomainFileTester(String path) {
		_path = path;
	}

	public String pathname() {
		return _path + "\\domain.txt";
	}

	public void run() throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(pathname()));
		System.out.println("DOMAIN: [" + scanner.nextLine() + "]");
		scanner.close();
	}

	boolean exists() throws FileNotFoundException {
		File file = new File(pathname());
		return file.exists();
	}

	public static void main(String[] args) throws Exception {
		DomainFileTester exp = new DomainFileTester(KPMTOOLS_PATH);
		if (exp.exists())
			System.out.println("File " + exp.pathname() + " exists.");
		else
			System.out.println("File " + exp.pathname() + " does not exist.");
		exp.run();
		System.out.println("1st test terminated.");
	}

}
