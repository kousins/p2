package exps;

import java.util.StringTokenizer;

public class ExpStringTokenizer {
	private static final String DELIMITER_CHARS = " ^&~![]{}()<>+-/%*;,:=\"";

	public static void main0(String[] args) {
		String line = "if nargin < 3, return; end";
		StringTokenizer st = new StringTokenizer(line, DELIMITER_CHARS);
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
	}

	public static void main1(String[] args) {
		String line = "  j = find(bigdom==outdom(i, 'haydn mozart beethoven'));";
		StringTokenizer st = new StringTokenizer(line, DELIMITER_CHARS);
		while(st.hasMoreTokens())
			System.out.println(st.nextToken());
	}
	public static void main(String[] args) {
		String text = "linha um\nlinha dois\nlinha tr�s\nlinha quatro";
		StringTokenizer st = new StringTokenizer(text, "\n");
		while(st.hasMoreTokens())
			System.out.println("[" + st.nextToken() + "]");
	}
}
