package util;

import java.io.PrintStream;

public class ProgressDisplay {
	private static Integer progress = 0;
	private static boolean progressFlag = false;
	private static int GROUP = 200;
	private static char ch = '.';

	public static void progressDisplayOn() {
		progressFlag = true;
	}
	public static void progressDisplayOff() {
		progressFlag = false;
	}
	public static void updateProgress() {
		if (!progressFlag)
			return;
		progress++;
		if (progress % GROUP == 0)
			out.print("" + ch);
		if (progress % (GROUP * 10) == 0)
			out.print(" ");
		if (progress % (GROUP * 100) == 0)
			out.println();
	}
	public static void resetProgress(char newch, int group) {
		println();
		progress = 0;
		ch = newch;
		GROUP = group;
	}


	private static final PrintStream out;

	static {
		out = System.out;
	}
	private static boolean outputFlag = false;

	public static void outputOn() {
		outputFlag = true;
	}
	public static void outputOff() {
		outputFlag = false;
	}
	public static void print(String output) {
		if (outputFlag)
			out.print(output);
	}
	public static void println(String output) {
		if (outputFlag)
			out.println(output);
	}
	public static void println() {
		if (outputFlag)
			out.println();
	}
}
