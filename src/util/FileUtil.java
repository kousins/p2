package util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	public static List<File> getMFiles(File folder) {
		List<File> mFiles = new ArrayList<File>();
		File[] allFiles = folder.listFiles();
		for(File file : allFiles)
			if(!file.isDirectory())
				if(file.getName().endsWith(".m"))
					mFiles.add(file);
		return mFiles;
	}

	public static List<File> getSubFolders(File folder) {
		List<File> folders = new ArrayList<File>();
		File[] allFiles = folder.listFiles();
		for(File file : allFiles)
			if(file.isDirectory())
				folders.add(file);
		return folders;
	}

}
