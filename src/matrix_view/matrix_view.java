package matrix_view;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import multiSOM.Point2D;
import processing.core.PApplet;
import processing.data.Table;
import processing.data.TableRow;
import processing.pdf.*;


// matrix_view  :  visualiza ficheiros csv como um conjunto de heatmaps de 20x40 (matriz), uma matriz para cada coluna.
//         cada programa (matrix_view -- nome original processing) � um SOM (classe Codebook), que inclui as v�rias colunas (array values da classe matrix_view).


public class matrix_view extends PApplet {
	public matrix_view() {
		System.out.println("INSTANTIATED");
	}

	boolean print_pdf=false;
	final int numLines=20;
	final int numCols=40;

	Point2D sz = new Point2D(440.0f, 390.0f);
	// float sz_x = 440.0f; // * prc_screen; // todo : usar precentage_screen e
	// // tamanho matriz
	// float sz_y = 390.0f; // * prc_screen;

	int maxVectSz = 1000;

	Codebook som;

	SelectedUnits som_select[];
	int num_selected;

    final int max_selected=40;

	File prototypes_file;
	long lastMod;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	String curr_csv_file="dat\\NT22_prototypes_10000.csv";  // ficheiro default
	
	int min_sel_val[];
	int max_sel_val[]; /// mostra todas as CP (ou entao valores minimos e maximos)
	int sel_cp[];    /// aux para min/max_sel_val: Selecionar o color_map para/das CP quando com x
    int curr_sel = 0;
    
    public Map<Character, IKeyPressed> keypressers = new HashMap<>();
    
    private void GenerateKeyPressers()
	{
		this.keypressers.put('d',new KeyPressed_d());
		this.keypressers.put('n',new KeyPressed_n());
		this.keypressers.put('p',new KeyPressed_p());
		this.keypressers.put('P',new KeyPressed_p_C());
		this.keypressers.put('x',new KeyPressed_x());
		this.keypressers.put('S',new KeyPressed_s_C());
		this.keypressers.put('b',new KeyPressed_b());
		this.keypressers.put('0',new KeyPressed_int());
		this.keypressers.put('1',new KeyPressed_int());
		this.keypressers.put('2',new KeyPressed_int());
		this.keypressers.put('3',new KeyPressed_int());
		this.keypressers.put('4',new KeyPressed_int());
		this.keypressers.put('5',new KeyPressed_int());
	}
    
	public void setup() {

		read_csv();
		
		min_sel_val= new int [max_selected];
		max_sel_val= new int [max_selected]; 
		sel_cp= new int [max_selected];
		
		for(int i=0;i<max_selected; i++) {
			min_sel_val[i] = -1;
			max_sel_val[i]= -1; 
			sel_cp[i]= -1;
		}
		
		GenerateKeyPressers();
	}

	public void read_csv() {
		  println("selected:"+curr_csv_file);
		  
			prototypes_file = new File(curr_csv_file);
			som = new Codebook(curr_csv_file, 20, 40);
			som_select = new SelectedUnits[max_selected + 1];
			lastMod = prototypes_file.lastModified();
			num_selected = 0;

		//	background(245);
	}
	
	int curr_dir=-1;   /// posicao do ficheiro na pasta, no inicio eh o protypes.csv

	public void next_csv_file_name() {
		File dir; 
		File [] files;
		 
		// println("start 1");
		dir= new File(dataPath("dat\\"));
		files= dir.listFiles();
		
		int idx_csv=0;
		for(int i=0;i<files.length; i++) {
			
			String path = files[i].getAbsolutePath();
			println(i, path);
			if (path.toLowerCase().endsWith(".csv") && idx_csv<=i) {
				print("i"+i+" "+idx_csv);
				
				if(idx_csv == i) idx_csv++;
				else {
					println(path);
					files[idx_csv++]=files[i];
				}
              }
		  }  
		
		println("files:"+files.length+" "+curr_dir, idx_csv);
		curr_dir++;
		
		if( curr_dir >= idx_csv) curr_dir=0;
		
		curr_csv_file = files[curr_dir].getAbsolutePath();
		
		read_csv();
			    
	}
	
	boolean redraw=true;
	
	public void draw() {

		long currFMod;

		currFMod = prototypes_file.lastModified();

		// println("refreshing..."+nFramesToRedraw);
		if (currFMod != lastMod) {
			som = new Codebook(curr_csv_file, numLines, numCols);
			background(245);
			lastMod = currFMod;

		}
		
		if(mouseX !=pmouseX || mouseY != pmouseY  ) redraw= true; 
		
		if(print_pdf) {
			beginRecord(PDF, "dat/matrix_view_####.pdf");
			redraw=true;
		}
		if(!redraw) return;
		
	    background(245);   //  redraw em cada iteracao ?? (TODO: so quando h� movimento?)
	    
		if(mouse_cp>= som.m.length ) {mouse_cp=0;} // BUG: evita erro em caso de mudanca de ficheiro ... deve estar noutro local
		                                         // mas aqui parece controlar o problema

		som.draw_mats(som.nFeatures);
		textSize(10);
		fill(80, 60, 180, 100);
		text(curr_csv_file+":"+sdf.format(lastMod), 400, 10);
		
		if( som.mouse_line >= 0 ) text("mouse ("+som.mouse_line+", "+ som.mouse_col+")", 10,10);
		
		redraw=false;
		if (print_pdf) {
			endRecord();
			print_pdf = false;
			background(0); // efeito fotografia
			redraw=true;
		}


	}

	
	//// comportamento global da interface (devera passar para a classe interface??)
	

	
	boolean select_by_val=false;  //// booleana para obter modo select/not select
	boolean black_white_cp=false; ///  modo BW nas CP
	
	//   highligh da linha da CP . Com -1 nao ha nada selecionado
     
	int highlight_idx = -1;
	int mouse_cp=0;   /// Selected CP
	
	
	public void keyPressed() {
		redraw = true;
		IKeyPressed keypressed = keypressers.get(key);
		if(keypressed != null)
			keypressed.Pressed();
	}
	
	interface IKeyPressed{
		void Pressed();
	}
	
	class KeyPressed_d implements IKeyPressed{
		
		public void Pressed()
		{
			// load default file
			curr_csv_file="dat\\NT22_prototypes_10000.csv";
			read_csv();
		}
	}
	class KeyPressed_n implements IKeyPressed{
		
		public void Pressed()
		{
			// load next file
			next_csv_file_name();
		}
	}
	class KeyPressed_p implements IKeyPressed{
		
		public void Pressed()
		{
			// load prev file
			if(curr_dir >0) {curr_dir-=2; next_csv_file_name();}
		}
	}
	class KeyPressed_p_C implements IKeyPressed{
		
		public void Pressed()
		{
			// printscreen to pdf
			print_pdf=true;
		}
	}
	class KeyPressed_x implements IKeyPressed{
		
		public void Pressed()
		{
			/// if on a map, detect min to max val for curr component, else set to -1
			if(som.mouse_line>=0 && som.mouse_col >= 0) {
				if(select_by_val) {
					select_by_val=false;  /// trocar o modo de seleccao
				} else {
					min_sel_val[curr_sel] = -1; max_sel_val[curr_sel] = -1;
					select_by_val=true;
				}
			} else {
				min_sel_val[curr_sel] = -1; max_sel_val[curr_sel] = -1;  // mostrar todos os CPs
				sel_cp[curr_sel] = -1;
				select_by_val=false;
			}
		}
	}
	class KeyPressed_s_C implements IKeyPressed{
		
		public void Pressed()
		{
			// seleciona a feature nas coordenadas para a matrix CP
			 if(mouse_cp == -1 || highlight_idx == -1) 
				 println("Swap requested but :"+mouse_cp+"<->"+highlight_idx+" is invalid.");  // nao faz nada se nao estao ambas selecionadas
			 else {
				 som.swap(mouse_cp,highlight_idx); 
			 }
		}
	}
	class KeyPressed_b implements IKeyPressed{
		
		public void Pressed()
		{
			// black and white
			black_white_cp=!black_white_cp;
		}
	}
	class KeyPressed_int implements IKeyPressed{
		
		public void Pressed()
		{
			// todos os anteriores
		 	 curr_sel=(int) (key-'0');
		}
	}
	
	
	///////////////////////////////
	
	class SelectedUnits {

		int linha;
		int col;

		public SelectedUnits(int l, int c) {
			linha = l;
			col = c;
		}

	}

	public int getSelectedId(int l, int c) {
		for (int i = 0; i < num_selected; i++)
			if (som_select[i].linha == l && som_select[i].col == c)
				return i;
		return -1;
	}

	public void mousePressed() {

		if (som.mouse_col != -1) {
			// println("click:");

			int sid = getSelectedId(som.mouse_line, som.mouse_col);  // seleccao num mapa/(componente?)

			if (sid >= 0) {
				for (int i = sid; i < num_selected; i++)
					som_select[i] = som_select[i + 1];
				num_selected--;
				//  background(245);   //  Verificar
				return;
			}
			som_select[num_selected] = new SelectedUnits(som.mouse_line, som.mouse_col);
			if (num_selected < max_selected) {
				// print(num_selected+":"+som_select[num_selected].linha+","+som_select[num_selected].col);
				num_selected++;
			} else
				for (int i = 1; i <= max_selected; i++) {
					som_select[i - 1] = som_select[i];
					// print(i+":"+som_select[i].linha+","+som_select[i].col+"
					// ");
				}

		}
	}

	class Matrix_view {   // responsavel por desenhar cada matriz 

		int values[][];    // matriz com valores de cada celula
		int n_lines = 20;
		int n_cols = 40;
		String title;

		// tamanho e coordenadas celulas da ultima matriz utilizada (valor
		// default, valor real definido em draw)

		int coord_x[];
		int coord_y[];

		float txt_sz;

		int mouse_col = -1;
		int mouse_line = -1;
		boolean mouse_detected = false;
		

		int min_x=-1, max_x=-1, min_y=-1, max_y=-1;  // definido em draw

		public Matrix_view(int nl, int nc, String stitle) {

			values = new int[nl][nc];
			n_lines = nl;
			n_cols = nc;
			title = stitle.trim();

			values = new int[n_lines][n_cols];
			coord_x = new int[n_cols];
			coord_y = new int[n_lines];

		}

		/// todo: de string
		public Matrix_view(String s) {

			if (s == "teste") {
				values = new int[][] { { 100, 200, 300 }, { 200, 300, 500 }, { 300, 800, 100 }, { 200, 300, 100 } };
				n_lines = 4;
				n_cols = 3;
				title = "teste 123";
				// } else {
				// Class aux = new Matrix_view();
				// values = aux.values;
			} // ???
		}

		public void paint_unit(int x, int y, int cl) {
			fill(cl);
			rect(x, y, sz.getfX(), sz.getfY());
		}

		public void print_unit(int l, int c) {
			int x = coord_x[c], y = coord_y[l];

			if (x < mouseX && x + sz.getfX() > mouseX && y < mouseY && y + sz.getfY() > mouseY) {

				mouse_col = c;
				mouse_line = l;
				mouse_detected = true;
			}

			if (mouse_col == c && mouse_line == l) {
				highlight_unit(x, y, jet_colormap(values[l][c]));
			} else {
				paint_unit(x, y, jet_colormap(values[l][c]));
			}

			int sid = getSelectedId(l, c);

			if (sid >= 0) {
				fill(255);
				textSize(txt_sz * 1.2f);

				text(sid, x + sz.getfX() / 4, y + sz.getfY() / 2); // x+c*sz.getfX()+txt_sz/1.5+txt_sz/5-2,
														// y+l*sz.getfY()+2*txt_sz+1);
				text(sid, x + sz.getfX() / 4 + 1, y + sz.getfY() / 2); // x+c*sz.getfX()+txt_sz/1.5+txt_sz/5-2,
															// y+l*sz.getfY()+2*txt_sz+1);
				text(sid, x + sz.getfX() / 4, y + sz.getfY() / 2 + 1); // x+c*sz.getfX()+txt_sz/1.5+txt_sz/5-2,
															// y+l*sz.getfY()+2*txt_sz+1);
				text(sid, x + sz.getfX() / 4 + 1, y + sz.getfY() / 2 + 1); // x+c*sz.getfX()+txt_sz/1.5+txt_sz/5-2,
																// y+l*sz.getfY()+2*txt_sz+1);
				textSize(txt_sz);

				fill(40);
				text(sid, x + sz.getfX() / 4 + 1, y + sz.getfY() / 2); // +c*sz.getfX()+txt_sz/1.5+txt_sz/5,
															// y+l*sz.getfY()+2*txt_sz);
			}
		}

		
		
		public void highlight_unit(int x, int y, int val, int w, int c) {


			strokeWeight(w);
			fill(val);
			rect(x + sz.getfX() * 0.18f, y + sz.getfY() * 0.18f, sz.getfX() * 0.7f, sz.getfY() * 0.7f);

			fill(c);
			strokeWeight(w);
			rect(x, y, sz.getfX(), sz.getfY());

			strokeWeight(1);

		}
		
		
//		private int add_alpha_to_color(int c, int alpha) {
//			return (c & 0xffffff) | (alpha << 24);
//		}   // nao funciona??
		
		public void highlight_unit(int x, int y, int val) {
			/// add_alpha_to_color(val,1)  ??
			highlight_unit(x, y, val , 4, color(250, 200, 0));
		}

		public void draw(int x, int y, float prc_screen) {

			mouse_detected = false;
			// println("mouse:"+mouseX+","+mouseY);
			sz.setfX(440.0f * prc_screen); // todo : usar precentage_screen e
										// tamanho matriz
			sz.setfY(390.0f * prc_screen);

			// Color_map c=new Color_map();
			txt_sz = 20.0f * sqrt(prc_screen) + 3;

			stroke(128);
			fill(40);

			textSize(txt_sz * 1.4f);
			text(title, x + 90, y - txt_sz * 1.4f);

			textSize(txt_sz);

			// header

			for (int c = 0; c < n_cols; c++) {
				text(c, x + (c + 1) * sz.getfX() - txt_sz - 4, y + txt_sz - 4);
				coord_x[c] = round(x + c * sz.getfX() + txt_sz / 1.5f); // aproveita e
																	// define
																	// colunas
			}
			min_y= (int) (y + txt_sz - 4);
			
			
			// selected

			for (int i = 0; i < num_selected; i++) {
				Point2D pp = new Point2D(round(x + i * sz.getfX() + txt_sz / 1.5f), round(y + 22 * sz.getfY() + txt_sz));
				// int xx = round(x + i * sz.getfX() + txt_sz / 1.5f);
				// int yy = round(y + 22 * sz.getfY() + txt_sz);
				paint_unit(pp.getX(), pp.getY(), jet_colormap(values[som_select[i].linha][som_select[i].col]));

				fill(40);
				textSize(txt_sz * 1.1f);

				text(i, pp.getX(), pp.getY() - txt_sz);
			}

			// matrix

			for (int l = 0; l < n_lines; l++) {
				fill(40);
				text(l, x + txt_sz / 5 - 4, y + l * sz.getfY() + 2 * txt_sz - 4);
				coord_y[l] = round(y + l * sz.getfY() + txt_sz);

				for (int c = 0; c < n_cols; c++) {

					print_unit(l, c);

				}
			}
			
			min_x=coord_x[0];
			max_x = coord_x[n_cols-1];

			max_y = coord_y[n_lines-1];

		}
		
		public void select() {
			stroke(228, 200, 0);
			strokeWeight(3);
			noFill();
			rect(min_x-2-txt_sz, min_y-2-txt_sz, max_x-min_x+4+txt_sz+2*sz.getfX(), max_y-min_y+2*sz.getfY()+txt_sz +4);
			strokeWeight(1);
		}
		
		

	}

	///////////////////////////////////////
//   colormap
	
	
	//int startJet = color(0, 0, 150);
	//int midJet = color(40, 150, 40);// color midJet=color(110,230,110); ///color
									// midJet=color(127,255,127);
	//int endJet = color(150, 0, 0);

	 //int startJet = color(0, 0, 150);
	 int startJet = color(100, 100, 150);
	 //int midJet = color(40, 150, 40);
	 int midJet=color(110,230,50); ///color
	 //int midJet=color(120,120,20); ///color
	        // midJet=color(127,255,127);
	 int endJet = color(120, 0, 0);
	
	public int jet_colormap(float val) { // fonte (e TODO, para melhor
											// colormap):
											// http://cresspahl.blogspot.pt/2012/03/expanded-control-of-octaves-colormap.html
		// println("cor jetColorMap:"+val);
		int retv;

		float midVal = maxVectSz / 2;
		// println("val:"+val);

		if (val <= midVal)
			retv = lerpColor(startJet, midJet, val / midVal);
		else
			retv = lerpColor(midJet, endJet, (val) / midVal - 1);

		return retv;
	}

	public int jet_colormap(int val) { // fonte (e TODO, para melhor colormap):
										// http://cresspahl.blogspot.pt/2012/03/expanded-control-of-octaves-colormap.html
		return jet_colormap((float) val);
	}

	class Color_map {
		// color startJet=color(127,0,0);
		// JET:
//		int startJet = color(110, 0, 0);
//		int midJet = color(30, 140, 30);// color midJet=color(110,230,110);
//										// ///color(127,255,127);
//		int endJet = color(0, 0, 110);
		// Bolsa:
//		int startFinance = color(0, 180, 0);
//		int midFinance = color(70, 70, 70);// color midJet=color(110,230,110);
//											// ///color(127,255,127);
//		int endFinance = color(180, 0, 0);

		Color_map() {
		}

		public int finance(float val) { // fonte (e TODO, para melhor colormap):
										// http://cresspahl.blogspot.pt/2012/03/expanded-control-of-octaves-colormap.html
			// println("cor jetColorMap:"+val);

			val = maxVectSz - val;
			if (val <= maxVectSz / 2)
				return lerpColor(startJet, midJet, val / 500);
			return lerpColor(midJet, endJet, val / 500 - 1);
		}

	}

	/////////////////////////////////////////////////

	class Codebook {
		int maxVectSz = 1000;

		int nFeatures = 20;
		int n_lines = 20;
		int n_cols = 40;

		String f_name[];
		Matrix_view m[];

		int mouse_col = -1;
		int mouse_line = -1;
		

		public Codebook(String fn, int nl, int nc) {
			Table table = loadTable(fn, "header");

			nFeatures = table.getColumnCount();

			n_lines = nl;
			n_cols = nc;

			f_name = new String[nFeatures];
			m = new Matrix_view[nFeatures];
			for (int i = 0; i < nFeatures; i++)
				m[i] = new Matrix_view(nl, nc, "n/a");

			load(table);

		}

		public void swap(int sel_cp, int highlight_idx) {
			// TODO Auto-generated method stub
			Matrix_view m_aux;
			
			println("Swapping CP:"+sel_cp+" <->"+highlight_idx);
			m_aux= m[sel_cp];
			m[sel_cp]=m[highlight_idx];
			m[highlight_idx]=m_aux;
			
		}

		public Codebook(int nF, int nl, int nc) {
			nFeatures = nF;
			n_lines = nl;
			n_cols = nc;

			f_name = new String[nFeatures];
			m = new Matrix_view[nFeatures];
			for (int i = 0; i < nFeatures; i++)
				m[i] = new Matrix_view(nl, nc, "n/a");

		}

		public void load(String fn) {
			Table table = loadTable(fn, "header");
			load(table);
		}

	
		public void load(Table table) {
			int l = 0, c = 0;

			println(table.getRowCount() + " total rows in table");

			String titles[] = table.getColumnTitles();
			int nF = table.getColumnCount();

			println("Features: " + titles + " # " + nF);

			for (String s : titles) {
				int i = table.getColumnIndex(s);
				m[i].title = s.trim();
			}
			
			int [] min = new int[nF];
			int [] max = new int[nF];
			
			for(int i = 0; i <nF;i++){
				min[i] = 1000;
				max[i] = 0;
			}
			
			for (TableRow row : table.rows()) {
				for (int i = 0; i < nF; i++) {
					int val = row.getInt(i);

					if (c >= n_cols) {
			//			print("Ignoring data beyond last column");
			//			println("F" + i + ": (" + l + "," + c + ") val=" + val);
					} else {
						m[i].values[l][c] = val;
						if(val > max[i]) max[i] = val;
						if(val < min[i]) min[i] = val;
				//		println("F" + i + ": (" + l + "," + c + ") val=" + val);
					}
					// String species = row.getString("species");
					// String name = row.getString("name");

					// println(name + " (" + species + ") has an ID of " + id);
				}
				
				l++;
				if (l >= n_lines) {
					l = 0;
					c++;
				}

			}
			
			for(int i = 0; i < nF; i++){
				for(int j = 0; j < n_cols; j++){
					for(int k = 0; k < n_lines; k++){
						if(max[i] - min[i] > 0){ 
							m[i].values[k][j] = ((1000 * (m[i].values[k][j] - min[i]))/(max[i] - min[i]));
						}
						else{
							m[i].values[k][j] = (m[i].values[k][j] - min[i]);
						}
					}
				}
			}
		}




		public void draw_mats(int n) {
			draw_mats(n+2,2);
		}

		public void draw_mats_pc(int n) {   // versao com coordenadas paralelas (uma dimensao/coluna por cada matriz, com os valores de cada celula como dataset.
			// pc -- coordenadas paralelas
			draw_mats(n+2,2);
		}

		class DrawMats_Params
		{
			int xm;
			int ym;
			int nm_line;
			Point2D sz_m = new Point2D();
			// float sz_xm;
			// float sz_ym;
			float sc;

			public DrawMats_Params()
			{
				this.xm = 0;
				this.ym = 0;
				this.nm_line = 5;
				this.sz_m.setfX(0.2f);
				this.sz_m.setfY(0.22f);
				this.sc = 0.70f / m[0].n_cols;
			}
		}

		public int draw_mats_setN(int n)
		{
			if (n > 10)
				n = 10; // maximo para as primeiras 10 caracteristicas
			return n;
		}

		public DrawMats_Params draw_mats_setParamsByN(DrawMats_Params params, int n)
		{
			if (n > 6)
			{
				params.sz_m.setfX(0.25f);
				params.sz_m.setfY(0.2333f);
				params.sc = 0.8f / m[0].n_cols;
				params.nm_line = 4;
				return params;
			}

			if (n >= 3)
			{
				params.sz_m.setfX(0.3125f);
				params.sz_m.setfY(0.3333f);
				params.sc = 1.0f / m[0].n_cols;
				params.nm_line = 3;
				return params;
			}
			if (n > 1)
			{
				params.sz_m.setfX(0.5f);
				params.sz_m.setfY(0.5f);
				params.sc = 1.6f / m[0].n_cols;
				params.nm_line = 2;
			}
			return params;
		}

		public void draw_mats_SyncMouse(int n)
		{
			// sincronizacao de
			// mouse_col/mouse_lin detectado na
			// iteracao anterior
			// NOTA: Deveria utilizar variavel
			// global ou fazer set em codebook?
			for (int i = 0; i < n; i++) { 
				m[i].mouse_col = mouse_col;
				m[i].mouse_line = mouse_line;
			}
			// update para proxima iteracao, caso nao haja
			// deteccao durante desenho
			mouse_col = -1; 
			mouse_line = -1;
		}
		
		public DrawMats_Params draw_mats_UpdateMouse(DrawMats_Params params, int n)
		{
			for (int i = 0; i < n; i++) {

				if (params.xm >= params.nm_line) {
					params.xm = 0;
					params.ym++;
				}
				m[i].draw(params.xm * round(width* params.sz_m.getfX()) + 20, params.ym * round(height* params.sz_m.getfY()) + 30, params.sc*(width/1600f));
				if (m[i].mouse_detected) {  /// detecta o mouse durante o desenho e faz
											/// update dos varios modos necessarios

					mouse_col = m[i].mouse_col;
					mouse_line = m[i].mouse_line;
					mouse_cp=i;


					if(select_by_val) {
						sel_cp[curr_sel] = i;

						if(min_sel_val[curr_sel]==-1)  /// no caso de ser o primeiro
							min_sel_val[curr_sel]=m[mouse_cp].values[som.mouse_line][som.mouse_col];

					//	if(max_sel_val==-1 ||  < m[mouse_cp].values[som.mouse_line][som.mouse_line])
						max_sel_val[curr_sel]=m[mouse_cp].values[som.mouse_line][som.mouse_col];

						m[mouse_cp].highlight_unit(mouseX, mouseY, jet_colormap(max_sel_val[curr_sel]));

					//	if(max_sel_val< min_sel_val) {
					//		int swap;
					//		swap=max_sel_val;
					//		max_sel_val=min_sel_val;
					//		min_sel_val=swap;
					//	}
						// eh inconveniente mas nao pode trocar maximo e minimo
						// ou perde efeito
					
					}
				}
				params.xm++;
			}
			return params;
		}
		
		public void draw_mats_ShowString(int n)
		{
			if (mouse_col != -1) {
				String val_s = "[" + PApplet.parseFloat(m[0].values[mouse_line][mouse_col]) / maxVectSz;
				for (int i = 1; i < n; i++)
					val_s = val_s + "," + PApplet.parseFloat(m[i].values[mouse_line][mouse_col]) / maxVectSz;

				textSize(20);
			//	fill(255);
			//	rect(50, height - 50, width - 100, 25);
				fill(0);
				text("mouse"+((select_by_val) ? " Sel"+min_sel_val+"-"+max_sel_val :"")+" (" + mouse_line + "," + mouse_col + "): " + val_s + "].               ", 55, height - 30);

			}
		}

		public DrawMats_Params draw_mats_ResetParams(DrawMats_Params params)
		{
			if (params.xm >= params.nm_line)
			{
				params.xm = 0;
				params.ym++;
				return params;
			}

			if(params.xm>=2) params.xm=0; params.ym++;
			return params;
		}

		public void draw_mats_Stroke(DrawMats_Params params)
		{
			Point2D p0 = new Point2D(params.xm * round(width* params.sz_m.getfX()) + 20, params.ym * round(height* params.sz_m.getfY()) + 30+ (int) (44*sz.getY()));
			// int x0=	params.xm * round(width* params.sz_m.getfX()) + 20;
			// int y0= params.ym * round(height* params.sz_m.getfY()) + 30+ (int) (44*sz_y);

			int col_sz= (width-p0.getX()-20)/(2+m.length);  // 2 colunas para lin e col de som
					// 2* ym * round(height* sz_m.getfY())/m.length;  // tamanho por coluna   -- todo


			draw_pc(p0 , 20*params.sc, col_sz);

			stroke(230, 230,0);
			for (int i = 0; i < num_selected; i++) {
				line_pc(i, p0, 20*params.sc, col_sz, v_sel);
			}

			if (mouse_col != -1) {

		//		String val_s = "[" + PApplet.parseFloat(m[0].values[mouse_line][mouse_col]) / maxVectSz;
				stroke(150, 200,200);
				strokeWeight(4);
				line_pc(mouse_line,mouse_col, p0, 20*params.sc, col_sz, v_sel);
				strokeWeight(1);
			}
		}

		public void draw_mats(int n, int n_pc)
		{
			n = draw_mats_setN(n);
			DrawMats_Params params = new DrawMats_Params();
			params = draw_mats_setParamsByN(params, n);
			n=n-n_pc;     // repor n ... todo: melhorar isto
			draw_mats_SyncMouse(n);
			params = draw_mats_UpdateMouse(params,n);
			if(mouse_cp >= 0) m[mouse_cp].select();
			draw_mats_ShowString(n);
			
			if(n_pc==0) return;
			
			params = draw_mats_ResetParams(params);
			draw_mats_Stroke(params);
		}
		
	//	int v_sel[]={1,2,3,0};  /// Ordem de apresenta��o de colunas nas coordenadas paralelas
		int v_sel[]={0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
		/// Ordem de apresenta��o de colunas nas coordenadas paralelas

		
		
       private boolean is_pc_sel(int i, int l, int c) {
    	   
    	   if(i==5)   ///hardcoded # de memorias select no cp 
    		   return is_pc_sel(0, l, c)||is_pc_sel(1, l, c)||
    				   is_pc_sel(2, l, c)||is_pc_sel(3, l, c)||
    				   is_pc_sel(4, l, c);
    	   
    	   return sel_cp[i] == -1 || 
					( m[sel_cp[i]].values[l][c]>= min_sel_val[i] &&
					  m[sel_cp[i]].values[l][c]<= max_sel_val[i]);
       }

		void draw_pc(Point2D p0, float sc, int col_sz) {

			if(mouse_cp>= m.length ) {mouse_cp=0;} // BUG: evita erro em caso de mudanca de ficheiro ... deve estar noutro local
            // mas aqui parece controlar o problema

			stroke(0);
			for(int l=0;l<n_lines;l++) {
	//		{   int l=0;
				for(int c=0; c<n_cols; c++) { // para cada valor na matriz do SOM
					if(!black_white_cp )
						stroke(jet_colormap(m[mouse_cp].values[l][c]));

					if( is_pc_sel(curr_sel, l, c) ) /// apenas 3 pos de sel?
						line_pc(l,c, p0, sc, col_sz, v_sel);  // desenha linha com prototipo
				}
			}

			// Eixos

			stroke(50);
			fill(70);
			textSize(10);

			int curr_y=(int) (p0.getY()-44*sz.getY());

			text("100 % _ ", p0.getX()-35, curr_y+40);
			text("  0 % _ ", p0.getX()-30, p0.getY()+30);
			textSize(8);

			int highlight_x=-1;

			String highlight_s="";


			int idx=-1;

			for(int i=0; i<m.length+2;i++) {
				String s;

				int curr_x=calc_pc_x_axis(p0.getX(), col_sz, i); /// 32 de borda para ver txt

				line(curr_x, p0.getY()+30, curr_x, curr_y+40);

				fill(150);
				if(i< m.length) {
					if(i<v_sel.length)    // prioridade ao vector v_sel, se existir
						idx= v_sel[i];
					else
						idx=i;
					s=m[idx].title;
				} else {
					s=((i==m.length) ? "col": "lin");
				}
				

				
				text(s, curr_x, curr_y);
				
				if( mouseX+col_sz/2> curr_x && mouseX-col_sz/2 < curr_x && mouseY>curr_y) {
					highlight_x = curr_x;
					highlight_s = s;
					highlight_idx= idx;  // NB: idx = -1 == RESET, no caso das colunas lin e col do SOM.
				//	println("set highlight_idx="+highlight_idx);
				}

			}

			// eixo(s) seleccionado(s)
			for(int i=0;i<sel_cp.length;i++)
				if( sel_cp[i] != -1) {  // realcar seleccao actual.
					stroke(200,100,0);
					strokeWeight(5);
					int curr_x=calc_pc_x_axis(p0.getX(), col_sz, sel_cp[i]);
					line(curr_x, p0.getY()-round(min_sel_val[i]*sc/1.4F)+30,
							curr_x, p0.getY()-round(max_sel_val[i]*sc/1.4F)+30);
					textSize(15);
					text("S"+i, curr_x-30, p0.getY()-round(max_sel_val[i]*sc/1.4F)+30);
					strokeWeight(1);
				}


			textSize(16);
			if(highlight_x >=0) {
				fill(240);
				noStroke();
				rect(highlight_x, curr_y-8, 16*highlight_s.length(), 16);
				fill(70);
				text(highlight_s, highlight_x, curr_y);
				stroke(250, 200, 0);
				strokeWeight(4);
				line(highlight_x, p0.getY()+30, highlight_x, curr_y+40);
				strokeWeight(1);

			} else {
			//	println("reset highlight_idx");
				highlight_idx = -1;
			}

			fill(200);

		}

		private int calc_pc_x_axis(int x0, int col_sz, int i) {
			return x0+i*(col_sz-1)+32;
		}

		void line_pc(int i, Point2D p0, float sc, int col_sz, int v_sel[]) {
			line_pc(som_select[i].linha, som_select[i].col, p0, sc, col_sz, v_sel);
			textSize(10);
			text(i, p0.getX()-10,
					p0.getY()+30- round(m[v_sel[0]].values[som_select[i].linha][som_select[i].col])*(sc/1.4F));
		}

		void line_pc(int l, int c, Point2D p0, float sc, int col_sz, int v[]) {

			int last_v, new_v, x1=p0.getX()+32, x2=0; // TODO usar calc_pc_x_axis !
			sc=sc/1.4F;
			p0.setY(p0.getY() + 30);
			p0.setX(p0.getX() + 40);

			if(v.length>0) {   // prioridade ao vector v, se existir
				last_v= p0.getY()-round(m[v[0]].values[l][c]*sc);  // nota: referencia indirecta para potenciar troca de colunas?
			} else {
				last_v= p0.getY()-round(m[0].values[l][c]*sc);  // nota: referencia indirecta para potenciar troca de colunas?
			}

			for(int i=1; i<m.length;i++) {  // imprimir por dimensao ou coluna
				x2=x1+col_sz-1;
				if(i<v.length) {   // prioridade ao vector v, se existir
					new_v= p0.getY()- round(m[v[i]].values[l][c]*sc);
				} else {
					new_v= p0.getY()- round(m[i].values[l][c]*sc);
				}
				//println("pc:",last_v, new_v, x1, sc, col_sz);
				line(x1, last_v, x2, new_v);
				last_v=new_v;
				x1=x2;
			}
			new_v= p0.getY()- round(c*1000/n_cols*sc); /// ....
			x2=x1+col_sz-1;
			line(x1, last_v, x2, new_v);
			line(x2+1, new_v, x2+col_sz-1, p0.getY()-round(l*1000/n_lines*sc));

		}

	}









	////////////////////////////////

	public void settings() {
		// size(1600,900);   /// todo --- usar vars processing: width e height
	  	//
		 fullScreen(2);
		//size(1024,720);
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "matrix_view.matrix_view" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}