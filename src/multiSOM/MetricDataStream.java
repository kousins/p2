package multiSOM;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import cccfinder.output_table.ConcernMetricTable;
import cccfinder.tool.MFile;

public class MetricDataStream implements IDataStream {

	//CCCExplorer table initializer
	ConcernMetricTable metricTable;
	
	//CCCExplorer data
	Map<MFile, List<Integer>> metricData;
	Iterator<Entry<MFile,List<Integer>>> metricIterator;
	
	//current mFile being analyzed
	MFile currentFile;

	//map to store BMU data: for HTML report
	List<Entry<Point2D, MFile>> bmuData;	
	
	//samples for resampling
	List<Entry<MFile,int[]>> samples;
	
	int sampleIndex = -1;
	
	int resetCounter = 1;
	
	Random rand;
	double randomNumber;
	
	//probability of resampling
	static double finalResamplingThreshold = 0.1;
	
	public void setup(MultiSOM src) {
		// TODO Auto-generated method stub
		try {
			setup_cccexplorer();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setup_cccexplorer() throws FileNotFoundException, IOException {
		metricTable = new ConcernMetricTable();
		
		//build table
		metricTable.buildMetricTableNormByDensity();
		
		//normalize table values
		metricTable.normalizeTable();
		
		metricData = metricTable.getResults();
		metricIterator = metricData.entrySet().iterator();

		//for the HTML Report
		bmuData = new ArrayList<Entry<Point2D, MFile>>();
		
		//for resampling
		rand = new Random();
		samples = new ArrayList<Entry<MFile, int[]>>();
	}

	public int[] next() {
		
		//condition to start resampling process
		if(!metricIterator.hasNext()){
			if(sampleIndex < samples.size()){
				System.out.println("Inserted sample no. " + (sampleIndex+1) + " out of " + samples.size() );
				currentFile = samples.get(sampleIndex).getKey();
				return samples.get(sampleIndex).getValue();				
			}
			else{
				System.out.println("Warning: No more samples. Resetting.");
				resetCounter++;
				sampleIndex = 0;
				currentFile = samples.get(sampleIndex).getKey();
				return samples.get(sampleIndex).getValue();
			}
		}
		
		Map.Entry <MFile, List<Integer>> thisEntry = (Entry<MFile, List<Integer>>) metricIterator.next();
		
		currentFile = thisEntry.getKey();
				
		List<Integer> line = thisEntry.getValue();
		
		int values[] = new int[line.size()];
		
		//read metric values
		for(int i = 0; i < line.size(); i++){
			values[i] = line.get(i);
		}

		randomNumber = rand.nextDouble();

		//save sample
		if (randomNumber > finalResamplingThreshold) {

			Entry<MFile, int[]> sample = new AbstractMap.SimpleEntry<MFile, int[]>(currentFile, values);
			samples.add(sample);
					
			System.out.println("Sample extracted. Current Size: " + samples.size());
		}
		
		return values;	
	}

	public boolean hasNext() {
		//return next point
		if(metricIterator.hasNext()){
			return true;
		}
		//start random sampling process
		else if(sampleIndex < samples.size()){
			sampleIndex++;
			return true;
		}
		//reset samples
		else if(resetCounter-- > 0){
			sampleIndex = 0;
			System.out.println("Resetting samples...");
			return true;
		}
		
		return false;
	}

	public void setAnnotation(Point2D bmu, double distance) {
		// save bmu for table
		Entry<Point2D, MFile> bmuEntry = new AbstractMap.SimpleEntry<Point2D, MFile>(bmu, currentFile);
		bmuData.add(bmuEntry);
	}

	public void setAnnotation(int[] prototype) {

	}

	public void noteKeyTyped(char key) {
	}

/*	private double normalizeValue(double value, double max) {
		double norm = Math.log(value + 1) / (Math.log(max + 1));

		if (norm > 0.8) {
			norm = 1000;
		} else {
			norm = 900 * norm;
		}
		return norm;
	}*/
	
	public void generateHTMLReports(int it) throws FileNotFoundException, IOException{
		generateBmuDataReport(it);
	}

	//get info for each file
	private String getFilesTags(MFile mfile){
		String tags = "<td> ";
			
		String fileName = mfile.getName();
		String pathName = mfile.getPathname(); //code_repositories/Rep_One/Classification/MATLABArsenal/netlab/gppak.m
			
		pathName = pathName.substring(pathName.indexOf("code_repositories"));
		pathName = pathName.replace("\\", "/");
			
		//System.out.println(pathName);
			
		List<Integer> values = metricData.get(mfile);
			
		String aux = " ";
		for(int j = 0; j < values.size(); j++){
			aux = aux + "<td>" + values.get(j) + "</td>";
		}
		aux = aux + "<td>" + mfile.nrLines() + "</td>";
			
		tags = tags.concat("<a href='" + pathName + "'>" + fileName + "</a> " + aux);
		
		tags = tags.concat("</td>");
						
		return tags;
	}
	
	//generate BMU data report
	private void generateBmuDataReport(int it) throws IOException{
		
		//get last entry reference
		int lastEntry = bmuData.size();
		
		//prepare data
		List<Map.Entry<Point2D,MFile>> subData = bmuData.subList(Math.max(0, lastEntry-5000), lastEntry);
		
		//prepare writer
		StringBuilder buf = new StringBuilder();
		File output = new File("C:/Users/Bruno Palma/Desktop/SOM Reports/BMU_Report_" + it + ".html");
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));
		
		//prepare HTML document
		buf.append("<style>" + "table, th, td { border: 1px solid black; }" + "tr:hover{background-color:#f5f5f5}"
				+ "</style>");
					
		String concernsHeader = " "; 
				
		//get concern names for header
		for(int i = 0; i < metricTable.concernNames.length; i++){
			concernsHeader = concernsHeader + "<th>" + metricTable.concernNames[i] + "</th>";
		}
				
		concernsHeader = concernsHeader + "<th>LoC</th>";

		buf.append("<html>" + "<head>" + "<script src='sorttable.js'></script>" + "</head>" + "<body>" + "<h1>#Files Per Point</h1>" + "<table class='sortable'>" + "<tr>" + "<th>BMU POINT (X,Y)</th>" + "<th>MFILES</th>" + concernsHeader + "<th>ITERATION</th>" + "</tr>");
		
		for(int j = 0; j < subData.size(); j++){
			Entry<Point2D, MFile> currEntry = subData.get(j);
			
			//info for each point
			Point2D point = currEntry.getKey();
			String bmuPoint = point.toString();
			int iteration = point.getIteration();
			
			//info for each file
			MFile mfile = currEntry.getValue();
			String fileTags = getFilesTags(mfile);
			
			buf.append("<tr>" + "<td>" + bmuPoint + "</td>" + fileTags + "<td>" + iteration + "</td>" + "</tr>");
		}
		buf.append("</table>" + "</body>" + "</html>");
		
		String html = buf.toString();
		bw.write(html);
		bw.close();
	}
}
