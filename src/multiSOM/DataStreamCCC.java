package multiSOM;

import java.io.FileNotFoundException;
import java.io.IOException;

public class DataStreamCCC implements IDataStream {

	MetricDataStream mds = new MetricDataStream();
	MultiSOM multiSOM;
	
	//iteration counter for CCCExplorer data
	int cccexp_it_count = 0;
	
	boolean notEmp;
	int [] temp = {};
	
	public int[] next(){
		if(cccexp_it_count > 3){
			return temp;
		}
		
		if (notEmp == false) {
			temp = mds.next();
		}

		return temp;
	}
	
	public boolean hasNext(){
		
		if (mds.hasNext()) {
			temp = mds.next();
			if (temp.length == 0) {
				notEmp = false;
			} else {
				notEmp = true;
			}
		} else {
			notEmp = false;
			cccexp_it_count++;
			setup(multiSOM);
		}
		
		return notEmp;
	}

	public void setup(MultiSOM src) {
		mds = new MetricDataStream();
		mds.setup(src);
		multiSOM = src;
	}

	public void setAnnotation(Point2D bmu, double distance) {
		mds.setAnnotation(bmu, distance);
	}

	public void setAnnotation(int[] prototype) {
		mds.setAnnotation(prototype);
	}

	public void noteKeyTyped(char key) {
		mds.noteKeyTyped(key);
	}
	
	public void generateHTMLReports(int it) throws FileNotFoundException, IOException{
		mds.generateHTMLReports(it);
	}
}
