package multiSOM;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IDataStream {

	public void setup(MultiSOM src);
	// setup - start process and setup/open datastream

	public int[] next();
	// get next pattern/example from stream. Each pattern is
	// a normalized value. multiSOM currently supports integer
	// values between 0 and 1000 in each position . Number of
	// features should be less than 20 for better results on
	// multiSOM.

	public boolean hasNext();
	// should return true if there is a next item available in the
	// dataStream. If false, it hasNext() will be called
	// again when next frame/example can be processed.
	// If true, next() will be called

	public void setAnnotation(Point2D bmu, double distance);
	// receives the BMU and distance for the last pattern
	// returned by next. Stream processing (if any) is left
	// to stream manager

	public void setAnnotation(int[] prototype);
	// TO BE DONE SOON: similar to setAnnotation but for prototype

	public void noteKeyTyped(char key);
	// This could be used to pass some pressed keys (or processing
	// events) to the Stream manager. Probably will be changed
	// soon to be more generic and to simulate key pressed and
	// other events to multiSOM user interface
	
	public void generateHTMLReports(int it) throws FileNotFoundException, IOException;

}
