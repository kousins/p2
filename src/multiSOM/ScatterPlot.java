package multiSOM;


//import java.math.*;

import processing.core.*;

public class ScatterPlot {
	MultiSOM parent;

	/////////////////////////
	PGraphics plot = null;
	final float x_start = 35;
	final float y_end = 200;
	float x_size = 3;

	float min;
	float delta;

	ScatterPlot(MultiSOM g, float v[], float emax, int start_val) { // plot6

		parent = g;
		makeAxis(v, emax, start_val);

	}

	ScatterPlot(MultiSOM g, float v[], int start_val) { // plot6

		parent = g;
		makeAxis(v, 0.2F, start_val);

	}

	void draw(int x0, int y0, int x1, int y1) {
		parent.image(plot, x0, y0, x1, y1);
	}

	public void add(float v[], int c) {

		float x = x_start;

		float y = (v[0] - min) / delta;

		plot.beginDraw();
		plot.stroke(c);
		for (int i = 1; i < v.length; i++) {
			float ox = x;
			float oy = y;
			x = x + x_size;
			y = y_end - (v[i] - min) / delta;
			plot.line(ox, oy, x, y);
		}
		plot.endDraw();

		// return plot;
	}

	// PGraphics thePlot=null;
	//
	//
	//
	// public void setup() {
	//
	// float v[];
	//
	// v = new float[500];
	//
	// for(int i=0;i<v.length; i++) v[i]=random(1);
	//
	//
	//
	// thePlot=makePlot(v, color(0,0,200));
	//
	// }
	//
	// public void draw() {
	// background(250);
	// image(thePlot, 0,0, mouseX, mouseY);
	// fill(0);
	// text(mouseX+","+mouseY, mouseX+20, mouseY-20);
	// }
	//
	// public void mousePressed() {
	//
	// float v [] = new float[100+round(random(200))];
	//
	// for(int i=0;i<v.length; i++) v[i]=random(0.50f)+0.25f;
	//
	// thePlot=makePlot(thePlot, v, color(random(255),random(255),0));
	//
	// }
	// public void settings() { fullScreen(); }
	// static public void main(String[] passedArgs) {
	// String[] appletArgs = new String[] { "plot" };
	// if (passedArgs != null) {
	// PApplet.main(concat(appletArgs, passedArgs));
	// } else {
	// PApplet.main(appletArgs);
	// }
	// }
	///////////////////////////////

	void makeAxis(float v[], float emax, int sv) {
		makeAxis(v, emax, 3, sv);
	}

	void makeAxis(float v[], float emax, int new_x_size, int sv) {
		// PGraphics plot;

		x_size = new_x_size;

		min = parent.min(0, parent.min(v));
		delta = parent.max(emax, parent.max(v)) - min;
	
		plot = parent.createGraphics(parent.round(v.length * x_size) + 32, parent.round(y_end) + 20);

		plot.beginDraw();

		float x_end = x_start + (v.length - 1) * x_size;

		plot.background(plot.color(220, 220, 220));

		plot.textSize(14);
		plot.stroke(plot.color(20, 20, 20));
		plot.fill(plot.color(20, 20, 20));

		plot.line(x_start, y_end, x_end, y_end);
		plot.line(x_start, 0, x_start, y_end);

		for (float i = 0; i <= delta + 1; i += delta / 10) {
			// int vint=i+min;
			plot.text(String.format("%.2f", i + min), x_start - 35, y_end * (delta - i) / delta + 14);
		}

		int nticks = parent.round(v.length / 10);
		float x = x_start - 10; // centrado
		for (int i = 0; i < v.length; i += nticks) {
			if (sv > v.length)
				plot.text(sv - i * parent.dataBMU.refresh_err_plot, x + 10, y_end + 19);
			else
				plot.text(sv - i, x, y_end + 19);
			x = x + x_size * nticks;
		}

		plot.stroke(parent.color(180, 180, 180));

		for (float i = 0; i <= delta; i += delta / 10) {
			plot.line(x_start - 2, y_end * (delta - i) / delta, x_end, y_end * (delta - i) / delta);
		}

		delta = delta / y_end;

		plot.endDraw();

		// return plot;
	}

	// PGraphics makePlot(float v[], int c) {
	// return makePlot(makeAxis(v),v,c);
	// }

}