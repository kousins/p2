package multiSOM;

//import java.math.*;
import processing.core.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import processing.net.*;
//import ubisom.library.a.*;
import somlp.model.nn.ubisom.*;
//import ubisom.library.b.*;

//import java.util.HashMap;
//import java.util.ArrayList;
//import java.io.File;
//import java.io.BufferedReader;
//import java.io.PrintWriter;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.IOException;


public class MultiSOM extends PApplet {

	// multidim : experiencias em projeccao multidimensional para 2D
	// Neste teste utiliza-se um conjunto de n-coordenadas/componentes, para um
	// analogo da projeccao ortogonal.
	// Ideia: tentar coordenadas polares no 2o quadrante para DIM>3?

	//  N. Marques, B. Santos, H. Silva, "A Multidimensional Interactive SOM". IV 2016.

	// import processing.sound.*;

	float[][] old_transform;
	Point2D point0 = new Point2D();
	int dimSz = 60; // Numero de dimensoes ou colunas (maximo)a utilizar.

	int currDimSz = dimSz; /// numero de dimensoes recebidas (dinamico para
							/// display)

	boolean pause_network = false; // suspender tratamento de dados da rede
	boolean show_umat_flag = false; // Modo UMat?
	boolean show_axis = true; // Mostrar eixos
	boolean show_plot_err = true; // Mostrar erro de ajuste do mapa na umat?
	///

	PImage curr_axis_img = null;

	Point2D axis_shift = new Point2D(0, 0);
	// int axis_shift_X = 0; /// Shift nos eixos (apenas na imagem base,
	// 						/// normalmente
	// int axis_shift_Y = 0; // deve estar 0,0 , mas com o IRIS assim fica
							// interessante

	// int axis_shift_X =200; // Valores usados para imagens teste IRIS
	// int axis_shift_Y =-200;
	///

	boolean show_grid_multidim = false; // Mostrar grelha multidimensional

	Axis axis;    // Eixos de projec��o (podem ser alterados por interaccao do utilizador)

	final int maxSizeBMU = 500; // memoria de BMU: quantos BMU h� em buffer

	MultiPoint[] exPoint = new MultiPoint[maxSizeBMU]; // Buffer circular de BMUs
	
	int curr_ex = 0; // total de pontos input

	int show_msg = 0;                         // Mensagens a imprimir no topo do ecr�
	                                          //  colocar o numero de frames a exibir a mensagem.
	String notification_msg = "";

    DataStream dataSource;
    
    public Map<Character, KeyPressed> keypressers = new HashMap<>();
    public Map<String, CommandProcessor> commands = new HashMap<>();
    
	public void setup() {                 // Setup do processing

		axis = new Axis();

		surface.setTitle("MultiDim Projector Interactive Server");
		textSize(32);
		println("setup");
		// size(1700, 1000); //iMac?
		// size(1200, 960); // projector iscte
		// size(1100, 600); // MacMini
		println("SZ:" + displayWidth, displayHeight);
		// size(displayWidth, displayHeight);

		set_transform();

		mClear();
		adjust_coord();

		// setupAxis();
		setup_server();
		old_transform = transform;

		for (int i = 0; i < exPoint.length; i++)
			exPoint[i] = new MultiPoint(v0.p);
		new_grid();
		setup_SOM();
		
		dataSource = new DataStream();
		dataSource.setup(this);
		
		umat_avg = new int [ubisom.getWidth()][ubisom.getHeight()];
		
		GenerateKeyPressers();
		GenerateCommandProcessors();
	}
	
	private void GenerateKeyPressers()
	{
		this.keypressers.put('0',new KeyPressed_0());
		this.keypressers.put('A',new KeyPressed_A_C());
		this.keypressers.put('b',new KeyPressed_b());
		this.keypressers.put('c',new KeyPressed_c());
		this.keypressers.put('E',new KeyPressed_E_C());
		this.keypressers.put('h',new KeyPressed_h());
		this.keypressers.put('H',new KeyPressed_H_C());
		this.keypressers.put('j',new KeyPressed_j());
		this.keypressers.put('k',new KeyPressed_k());
		this.keypressers.put('l',new KeyPressed_l());
		this.keypressers.put('L',new KeyPressed_L_C());
		this.keypressers.put('m',new KeyPressed_m());
		this.keypressers.put('M',new KeyPressed_M_C());
		this.keypressers.put('n',new KeyPressed_n());
		this.keypressers.put('o',new KeyPressed_o());
		this.keypressers.put('p',new KeyPressed_p());
		this.keypressers.put('P',new KeyPressed_P_C());
		this.keypressers.put('r',new KeyPressed_r());
		this.keypressers.put('R',new KeyPressed_R_C());
		this.keypressers.put('s',new KeyPressed_s());
		this.keypressers.put('S',new KeyPressed_S_C());
		this.keypressers.put(TAB,new KeyPressed_TAB());
		this.keypressers.put('w',new KeyPressed_w());
	}
	
	private void GenerateCommandProcessors()
	{
		this.commands.put("writePrototypes", new CommandProcessor_Write());
		this.commands.put("label", new CommandProcessor_Label());
		this.commands.put("nolabel", new CommandProcessor_NoLabel());
		this.commands.put("resetlabel", new CommandProcessor_ResetLabel());
		this.commands.put("mpoint", new CommandProcessor_MPoint());
		this.commands.put("resample", new CommandProcessor_ReSample());
		this.commands.put("resetid", new CommandProcessor_ResetId());
		this.commands.put("transformy", new CommandProcessor_Transformy());
		this.commands.put("transformx", new CommandProcessor_Transformx());
		this.commands.put("catdim", new CommandProcessor_CatDim());
		this.commands.put("colordim", new CommandProcessor_ColorDim());
	}

	public int show_points() {
		int cnt = 0;

		for (int i = 0; i < min(totDataPoints, exPoint.length); i++) {
			int relExample = (curr_ex - min(totDataPoints, exPoint.length) - i) % exPoint.length;
			if (relExample < 0) {
				relExample += min(totDataPoints, exPoint.length);
			}

			cnt++;
			exPoint[relExample].display();
			if (gridPt_cursor >= 0) {
				float v = gridPt[gridPt_cursor].p[axis.ax] - exPoint[relExample].p[axis.ax];
				if (v >= 0 && v < deltaGrid) {
					float v1 = gridPt[gridPt_cursor].p[axis.ay] - exPoint[relExample].p[axis.ay];
					if (v1 >= 0 && v1 < deltaGrid) {
						float v2 = gridPt[gridPt_cursor].p[axis.az] - exPoint[relExample].p[axis.az];
						if (v2 >= 0 && v2 < deltaGrid) {
							stroke(200, 130, 100, 40);
							strokeWeight(2);
							exPoint[relExample].display_axis_XY();
							defaultColor();
						}
					}
				}
			}
		}

		return cnt;

	}
	
	//used to regularly save data for each "x" iterations (zero to switch "off")
	static int iterationsToSave = 5000;

	public void draw() {
		
		if((SOMIteration % iterationsToSave == 0) && (SOMIteration > 0) && (iterationsToSave > 0)){
			mClear();
			save_data();
		}

		mClear();

		if (show_umat_flag)
			show_umat();
		else
			axis.draw_all();

		// receber dados
		defaultColor();
		textSize(32);

		if (!pause_network)
			draw_server();
		else
			if(show_umat_flag) show_point_coordinates();

		int cnt = 0;

		if (!show_umat_flag) {
			cnt = show_points();

			if (idxZoomData >= 0)
				exPoint[idxZoomData].display_axis_XY(32, 200);
		}

		dataLabels.displayAll();
		textSize(20);
		defaultColor();
		text("showing " + cnt + " points (" + SOMIteration + " it).", 2, 20);

		if (show_msg > 0) {
			text(notification_msg, 300, 20);
			show_msg--;
		}
	}

	////// Input do user

	int idxZoomData = -1;

	public void zoomDataPoint() {
		float minD = 10;

		idxZoomData = -1;
		for (int i = 0; i < min(totDataPoints, exPoint.length); i++) {
			if (exPoint[i].distance(mouseX, mouseY) < minD) {
				minD = exPoint[i].distance(mouseX, mouseY);
				idxZoomData = i;
			}
		}

	}

	int maxSelectDist = 30;
	MLine cursor;
	boolean show_line_cursor = false;
	int axis_cursor = -1;
	int gridPt_cursor = -1;

	public void mouseDragged() {

		if (show_umat_flag)
			return;

		if (gridPt_cursor >= 0 && axis_cursor >= 0) {
			gridPt[gridPt_cursor].p[axis_cursor] = max(0,
					min(maxVectSz, gridPt[gridPt_cursor].p[axis_cursor] + (mouseX - pmouseX + pmouseY - mouseY)));
			curr_axis_img = null; // for\u00e7a refresh da grelha inicial:
									// moveu-se ponto da grelha

		} else if (axis_cursor >= 0) {
			transform[0][axis_cursor] = max(0, min(1.0f,
					transform[0][axis_cursor] + 0.01f * (transform[0][axis_cursor] + 0.01f) * (mouseX - pmouseX)));
			transform[1][axis_cursor] = max(0, min(1.0f,
					transform[1][axis_cursor] + 0.01f * (transform[1][axis_cursor] + 0.01f) * (pmouseY - mouseY)));
			// YYs invertido
			adjust_coord();

		}
	}

	public void keyTyped() {
		
		KeyPressed keypressed = keypressers.get(key);
		if(keypressed != null)
			keypressed.Pressed();
		else
			dataSource.noteKeyTyped(key);
	}

	public void save_data() {
				
		calc_umat();
		
		show_msg = 100;
		writeMulti();
		// save (serialize) instance
		ubisom.serialize("som.dat");
		
		//creates HTML report
		try {
			dataSource.generateHTMLReports(SOMIteration);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		notification_msg = "prototypes.csv, som.dat and HTML saved.";
	}

	public void writeMulti() {
		String sSOM[] = prototypeSOMCSVString();
		// ubisom.serialize("curr_ubisom_model.dat");
		printArray(sSOM);
		saveStrings("dat\\" + SOMIteration + ".csv", sSOM);
		println("XTransform:");
		printArray(transform[0]);
		println("YTransform:");
		printArray(transform[1]);
		println("colorDim:" + colorDim);
		println("curr x,y,z dim: (" + axis.ax + "," + axis.ay + "," + axis.az + ")");

	}

	public void mousePressed() {
		if (show_umat_flag)
			return;

		float maxDist = maxSelectDist;
		axis_cursor = -1;

		if (gridPt_cursor >= 0) {
			for (int i = 0; i < dimSz; i++)
				if (axis.axis_p[i].distance(mouseX, mouseY) <= maxDist) {
					maxDist = axis.axis_p[i].distance(mouseX, mouseY);
					axis_cursor = i;
					// gridPt_cursor = i;
				}
		}

		for (int i = 0; i < dimSz; i++) {
			if (axis.axis[i].p2.distance(mouseX, mouseY) < maxDist) {
				maxDist = axis.axis[i].p2.distance(mouseX, mouseY);
				axis_cursor = i;
				gridPt_cursor = -1;
			}
		}

		if (axis_cursor == -1) {
			gridPt_cursor = -1;
		}

		for (int i = 0; i < gridPt.length; i++) {
			if (gridPt[i].distance(mouseX, mouseY) < maxDist) {
				maxDist = gridPt[i].distance(mouseX, mouseY);
				gridPt_cursor = i;
				// println("distance to axis "+axis_cursor+": "+maxDist);
			} // else println("nosel dist("+i+")="+axis[i].p2.distance(mouseX,
				// mouseY));
		}

	}

	public void mClear() {

		background(255); // 0);
		// problemas com mouse...
		// translate(0, -height/100);
		// scale(0.98);

		stroke(0, 200, 200);
		strokeWeight(1); // Default

		fill(100, 200, 200, 200);

	}

	public void defaultColor() {
		stroke(0, 75, 75); // stroke(0,150,150);
		strokeWeight(1); // Default
		// fill(100,200,200, 150);
		fill(5, 30, 50, 200);

	}

	//////// Grid

	MultiPoint[] gridPt = new MultiPoint[200]; // Pontos na grid
	final int deltaGrid = 333; // tamanho grid de pontos

	public void new_grid()
	{
		new_grid_Set3MainAxis();
		new_grid_SetGrid();		
	}
	
	public void new_grid_Set3MainAxis()
	{
		float vx = 0, vy = 0, vz = 0;
		for (int i = 0; i < dimSz; i++) 
		{ // determinar os 3 eixos principais onde mostrar grid
			if (vx < transform[0][i]) 
			{
				axis.az = axis.ax;
				vz = vx;
				axis.ax = i;
				vx = transform[0][i];
			} 
			else 
				if (vz < transform[0][i]) 
				{
					axis.az = i;
					vz = transform[0][i];
				}
			
			if (vy < transform[1][i]) 
			{
				axis.ay = i;
				vy = transform[1][i];
			}
		}
		
	}
	
	public void new_grid_SetGrid()
	{
		int gx = deltaGrid;
		int gy = deltaGrid;
		int gz = deltaGrid;

		MultiPoint vnew = new MultiPoint(v0.p);
		// MultiPoint vnewTop=new MultiPoint(v0.p);
		for (int i = 0; i < dimSz; i++)
			vnew.p[i] = maxVectSz;

		for (int i = 0; i < gridPt.length; i++) {
			if (gx > maxVectSz) {
				gx = deltaGrid;
				gy += deltaGrid;
				if (gy > maxVectSz) {
					gy = deltaGrid;
					gz += deltaGrid;
					if (gz > maxVectSz) {
						vnew.p[axis.ax] = 0;
						// vnew.p[ay]=maxVectSz;
						// vnew.p[az]=maxVectSz;

						for (int j = i; j < gridPt.length; j++)
							gridPt[j] = new MultiPoint(vnew.p);
						break;
					}

				}
			}

			vnew.p[axis.ax] = gx;
			vnew.p[axis.ay] = gy;
			vnew.p[axis.az] = gz;
			gridPt[i] = new MultiPoint(vnew.p);

			vnew.p[axis.ax] = maxVectSz;
			vnew.p[axis.ay] = maxVectSz;
			vnew.p[axis.az] = maxVectSz;

			gx += deltaGrid;

		}
	}
	
	public void grid(MultiPoint v7) {
		// hiper-quadrado paralelo aos eixos, mas por agora apenas um cubo 3d...
		float deltaDim[];

		deltaDim = v0.delta(v7);

		MultiPoint v1 = new MultiPoint(v0.p);
		v1.p[axis.ax] = deltaDim[axis.ax];
		v1.display(2, "");

		MultiPoint v2 = new MultiPoint(v0.p);
		v2.p[axis.ay] = deltaDim[axis.ay];
		v2.display(2, "");

		MultiPoint v3 = new MultiPoint(v0.p);
		v3.p[axis.az] = deltaDim[axis.az];
		v3.display(2, "");

		MultiPoint v4 = new MultiPoint(v0.p);
		v4.p[axis.az] = deltaDim[axis.az];
		v4.p[axis.ay] = deltaDim[axis.ay];
		v4.display(3, "");

		MultiPoint v5 = new MultiPoint(v0.p);
		v5.p[axis.ay] = deltaDim[axis.ay];
		v5.p[axis.ax] = deltaDim[axis.ax];
		v5.display(2, "");

		MultiPoint v6 = new MultiPoint(v0.p);
		v6.p[axis.az] = deltaDim[axis.az];
		v6.p[axis.ax] = deltaDim[axis.ax];
		v6.display(2, "");

		stroke(40, 60, 55, 30);
		strokeWeight(2);

		MLine l1 = new MLine(v2, v4);
		l1.display_noColorDim();
		MLine l2 = new MLine(v2, v5);
		l2.display_noColorDim();
		MLine l3 = new MLine(v1, v5);
		l3.display_noColorDim();
		MLine l4 = new MLine(v3, v6);
		l4.display_noColorDim();
		MLine l5 = new MLine(v1, v6);
		l5.display_noColorDim();
		MLine l6 = new MLine(v4, v7);
		l6.display_noColorDim();
		MLine l7 = new MLine(v5, v7);
		l7.display_noColorDim();
		MLine l8 = new MLine(v6, v7);
		l8.display_noColorDim();

	}

	public void draw_grid() {
		if (!show_grid_multidim)
			return;

		for (int i = 0; i < gridPt.length; i++)
			grid(gridPt[i]);

	}

	/////////////////////////

	public void set_transform() {

		transform = new float[2][dimSz];

		transform[0][axis.ax] = 1.0f;
		transform[1][axis.ax] = 0.0f;
		transform[0][axis.ay] = 0.0f;
		transform[1][axis.ay] = 1.0f;

		float ang = PI / 3; // 2/3 . PI/2
		transform[0][axis.az] = cos(ang) / 3;
		transform[1][axis.az] = sin(ang) / 3;

		float inc = PI / (6 * dimSz);
		ang += inc;
		int div = max(4, dimSz - 4);
		for (int i = 0; i < dimSz; i++) {
			if (i != axis.ax && i != axis.ay && i != axis.az) {
				transform[0][i] = cos(ang) / div;
				transform[1][i] = sin(ang) / div;
				ang += inc;
				// div++;
			}
		}
	}

	public void adjust_coord() {

		for (int i = 0; i < transform.length; i++) {
			sz_dim[i] = 0.0f;
			for (float val : transform[i])
				sz_dim[i] += val;
		}

		point0 = new Point2D (  15 + (int) map(0, 0, sz_dim[0] * maxVectSz, 15, width - 40),
				               height - 10 - (int) map(0, 0, sz_dim[1] * maxVectSz, 15, height - 35), SOMIteration);
		
		println("The point0 : "+ point0 );
	//	point0.x0 = 15 + (int) map(0, 0, sz_dim[0] * maxVectSz, 15, width - 40); /// TODO:
																			/// ATENCAO
																			/// coordenar
																			/// com
																			/// calcX
																			/// e
																			/// calcY
																			/// em
																			/// MultiPoint
	//	point0.y0 = height - 10 - (int) map(0, 0, sz_dim[1] * maxVectSz, 15, height - 35);

		curr_axis_img = null; // for\u00e7a refresh da grelha inicial
		gridSOM = null; // incluir o SOM
	}
	
	interface CommandProcessor{
		void Proccess(int[] data);
	}
	class CommandProcessor_Write implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			writeMulti();
			show_msg = 100;
			notification_msg = "prototypes.csv and som.dat saved.";
		}
	}
	class CommandProcessor_Label implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			if (data.length > 1)
				dataLabels.set(data[0], data[1]);
			else
				dataLabels.set(data[0]);

			if (data.length > 2)
				id_label = data[2];
			// sound_file.play();
			timer_to_new_pause = iterations_to_pause_on_mark;
		}
	}
	class CommandProcessor_NoLabel implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			dataLabels.isActive = false;
		}
	}
	class CommandProcessor_ResetLabel implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			dataLabels.reset();
		}
	}
	class CommandProcessor_MPoint implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			generateMPoint(data);
		}
	}
	class CommandProcessor_ReSample implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			int startResample = curr_ex - data[0];

			if (startResample < 0) {
				startResample += exPoint.length;
			}
			for (int i = 0; i < data[0]; i++)
				dataBMU.SOMLearn(exPoint[(startResample + i) % exPoint.length]);
		}
	}
	class CommandProcessor_ResetId implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			show_msg = 1000;
			notification_msg = "Id reset (old SOMIteration=" + SOMIteration + ").";
			if (data.length > 0)
				SOMIteration = data[0];
			else
				SOMIteration = 0;
		}
	}
	class CommandProcessor_Transformy implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			int i = 0;
			for (i = 0; i < data.length; i++)
				transform[1][i] = ((float) data[i]) / 10000;

			for (i = data.length; i < transform[1].length; i++)
				transform[1][i] = 0;

			new_grid();

			adjust_coord();
		}
	}
	class CommandProcessor_Transformx implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			int i = 0;
			for (i = 0; i < data.length; i++) {
				transform[0][i] = ((float) data[i]) / 10000;
				println("dt_trx:" + i + ":" + transform[0][i]);
			}
			for (i = data.length; i < transform[1].length; i++)
				transform[0][i] = 0;

			new_grid();

			adjust_coord();
		}
	}
	class CommandProcessor_CatDim implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			catDim = data[0];
			println("setting colordim to:" + colorDim);
		}
	}
	class CommandProcessor_ColorDim implements CommandProcessor
	{
		public void Proccess(int[] data)
		{
			colorDim = data[0];
			println("setting colordim to:" + colorDim);
		}
	}
	
	interface KeyPressed{
		void Pressed();
	}
	
	class KeyPressed_M_C implements KeyPressed{
		
		public void Pressed()
		{
			show_umat_flag = !(show_umat_flag);
		}
	}
	class KeyPressed_A_C implements KeyPressed{
		public void Pressed()
		{
			show_axis = !(show_axis);
		}
	}
	class KeyPressed_l implements KeyPressed{
		public void Pressed()
		{
			axis_cursor--;

			if (axis_cursor < 0) {
				axis_cursor = dimSz - 1;
				if (gridPt_cursor > 0)
					gridPt_cursor--;
				else
					gridPt_cursor = gridPt.length - 1;
			}
		}
	}
	class KeyPressed_P_C implements KeyPressed{
		public void Pressed()
		{
			pause_network = !pause_network;
			show_msg = 100;
			if (pause_network)
				notification_msg = "Pause on Network Information.";
			else
				notification_msg = "Activating Network Information.";
		}
	}
	class KeyPressed_p implements KeyPressed{
		public void Pressed()
		{
			if (gridPt_cursor > 0)
				gridPt_cursor--;
			else
				gridPt_cursor = gridPt.length - 1;
		}
	}
	class KeyPressed_o implements KeyPressed{
		public void Pressed()
		{
			if (gridPt_cursor < gridPt.length - 1)
				gridPt_cursor++;
			else
				gridPt_cursor = 0;
		}
	}
	class KeyPressed_h implements KeyPressed{
		public void Pressed()
		{
			axis_cursor++;

			if (axis_cursor >= dimSz) {
				axis_cursor = 0;
				gridPt_cursor++;
			}

			if (gridPt_cursor >= gridPt.length)
				gridPt_cursor = -1;
		}
	}
	class KeyPressed_k implements KeyPressed{
		public void Pressed()
		{
			if (gridPt_cursor >= 0 && axis_cursor >= 0 && gridPt[gridPt_cursor].p[axis_cursor] < maxVectSz)
				gridPt[gridPt_cursor].p[axis_cursor]++;
			else if (axis_cursor >= 0 && transform[0][axis_cursor] < 1.0f && transform[1][axis_cursor] < 1.0f) {
				transform[0][axis_cursor] = min(1.0f, max(0.005f, transform[0][axis_cursor]) * 1.05f);
				transform[1][axis_cursor] = min(1.0f, max(0.005f, transform[1][axis_cursor]) * 1.05f);
				adjust_coord();
			} else
				java.awt.Toolkit.getDefaultToolkit().beep();
		}
	}
	class KeyPressed_j implements KeyPressed{
		public void Pressed()
		{
			if (gridPt_cursor >= 0 && axis_cursor >= 0 && gridPt[gridPt_cursor].p[axis_cursor] >= 1.0f)
				gridPt[gridPt_cursor].p[axis_cursor]--;
			else if (axis_cursor >= 0) {
				transform[0][axis_cursor] = transform[0][axis_cursor] * 0.95f;
				transform[1][axis_cursor] = transform[1][axis_cursor] * 0.95f;
				adjust_coord();
			}
		}
	}
	class KeyPressed_m implements KeyPressed{
		public void Pressed()
		{
			arrayCopy(transform, old_transform);
			transform[0][axis.ax] = 20.0f;
			transform[1][axis.ax] = 0;
			transform[0][axis.ay] = 0;
			transform[1][axis.ay] = 20.0f;
			transform[0][axis.az] = 0.7f;
			transform[1][axis.az] = 0.5f;
			adjust_coord();
		}
	}
	class KeyPressed_n implements KeyPressed{
		public void Pressed()
		{
			arrayCopy(transform, old_transform);
			new_grid();
			set_transform();
			adjust_coord();
		}
	}
	class KeyPressed_R_C implements KeyPressed{
		public void Pressed()
		{
			arrayCopy(transform, old_transform);
			new_grid();
			adjust_coord();
		}
	}
	class KeyPressed_b implements KeyPressed{
		public void Pressed()
		{
			float aux[][];
			aux = new float[2][dimSz];
			arrayCopy(transform, aux);
			arrayCopy(old_transform, transform);
			old_transform = aux;
			new_grid();
			adjust_coord();
		}
	}
	class KeyPressed_w implements KeyPressed{
		public void Pressed()
		{
			save_data();
		}
	}
	class KeyPressed_r implements KeyPressed{
		public void Pressed()
		{
			show_msg = 100;
			ubisom = UbiSOMFactory.loadFromSave("som.dat");
			notification_msg = "som.dat loaded from server disk.";
		}
	}
	class KeyPressed_c implements KeyPressed{
		public void Pressed()
		{
			if (axis_cursor >= 0) {
				colorDim = axis_cursor;
				println("new colorDim:" + colorDim);
			} else
				colorDim = -1;
		}
	}
	class KeyPressed_H_C implements KeyPressed{
		public void Pressed()
		{
			if (axis_cursor >= 0) {
				show_msg = 100;
				notification_msg = "Axis hidden: " + axis_cursor;

				transform[0][axis_cursor] = 0.001f;
				transform[1][axis_cursor] = 0.001f;
				new_grid();

				adjust_coord();

			}
		}
	}
	class KeyPressed_s implements KeyPressed{
		public void Pressed()
		{
			if (show_umat_flag)
				return;

			zoomDataPoint();
		}
	}
	class KeyPressed_S_C implements KeyPressed{
		public void Pressed()
		{
			show_grid_som = !show_grid_som;
		}
	}
	class KeyPressed_L_C implements KeyPressed{
		public void Pressed()
		{
			show_msg = 100;
			learn_mode = !(learn_mode);
			notification_msg = "Learning is " + ((learn_mode) ? "on" : "off");
		}
	}
	class KeyPressed_0 implements KeyPressed{
		public void Pressed()
		{
			show_msg = 100;
			send_server = !(send_server);
			notification_msg = "Send server is now " + ((send_server) ? "on" : "off");
		}
	}
	class KeyPressed_TAB implements KeyPressed{
		public void Pressed()
		{
			if (gridPt_cursor >= 0)
				gridPt_cursor = -1;
			else if (axis_cursor >= 0)
				axis_cursor = -1;
			else
				java.awt.Toolkit.getDefaultToolkit().beep();
		}
	}
	class KeyPressed_E_C implements KeyPressed{
		public void Pressed()
		{
			if (show_umat_flag)
				show_plot_err = !(show_plot_err);
		}
	}

	///////////////////////////////
	/// Axis

	class Axis {

		MLine axis[]; // = new MLine [dimSz];

		MultiPoint axis_p[]; // = new MultiPoint [dimSz];
		String axis_s[];
		int ax = 0, ay = 1, az = 2; // principais dimensoes de projeccao

		Axis() {
			axis = new MLine[dimSz];
			axis_p = new MultiPoint[dimSz];
			axis_s = new String[dimSz];

			for (int i = 0; i < dimSz; i++) {
				MultiPoint p = new MultiPoint(v0.p);
				axis_s[i] = str(i);
				p.p[i] = maxVectSz;
				axis[i] = new MLine(v0, p);
			}
		}

		public void Draw() {

			int i = 0;

			textSize(25);
			fill(30, 70, 30, 50);
			stroke(10, 90, 10, 100);
			strokeWeight(2); // Default

			for (int k = 0; k < axis.length; k++) {
				MLine l = axis[k]; // eixos

				if (transform[0][k] > 0.01f || transform[1][k] > 0.01f) {
					l.display_noColorDim();
					text(axis_s[k], l.p2.calcX(), l.p2.calcY());
				} // else { // Axis is hidden

			}

			defaultColor();

		}

		public void draw_all() {

			// Desenhar eixos e seleccao de pontos
			if (curr_axis_img == null) {

				if (show_axis) {
					draw_grid();

					Draw();
				}
				curr_axis_img = get();
			} else if (show_axis)
				image(curr_axis_img, axis_shift.getX(), axis_shift.getY());

			if (totDataPoints > 10)
				printSOMGrid(); // so mostra SOM com alguns dados
			defaultColor();

			if (show_axis && axis_cursor >= 0) {

				stroke(150, 0, 0, 160);
				strokeWeight(6); // Default
				// println("display");
				axis[axis_cursor].display();
			}

			if (show_axis && gridPt_cursor >= 0) {
				stroke(150, 0, 0, 160);
				strokeWeight(6); // Default
				// println("display");
				gridPt[gridPt_cursor].display_axis_val();

			}

		}

	}

	/////////////////////////////////////
	////////////////////////////////////////////////////////////
	// MultiPoint -- Classe para calculos com pontos multi dimensionais

	// float [][] transform = {{1, 0.0, 0.6, 0.15, 0.03, 0.02},
	// {0.0, 1, 0.6, 0.03, 0.15, 0.2}};

	// matriz de transforma\u00e7\u00e3o inicial ... talvez tornar
	// automatico?
	float[][] transform;

	float maxVectSz = 1000;
	float sz_dim[] = { 0.0f, 0.0f };
	MultiPoint v0 = new MultiPoint(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);

	final int PointSz = 6; /// default point sz

	int colorDim = -1;
	// color startJet=color(127,0,0);
	// JET:
	int startJet = color(130, 0, 0);
	int midJet = color(30, 150, 30);// color midJet=color(110,230,110);
									// ///color(127,255,127);
	int endJet = color(0, 0, 180);
	// Bolsa:
	int startFinance = color(0, 180, 0);
	int midFinance = color(70, 70, 70);// color midJet=color(110,230,110);
										// ///color(127,255,127);
	int endFinance = color(180, 0, 0);

	public int jetColorMap(float val) { // fonte (e TODO, para melhor colormap):
										// http://cresspahl.blogspot.pt/2012/03/expanded-control-of-octaves-colormap.html
		// println("cor jetColorMap:"+val);

		val = maxVectSz - val;
		if (val <= maxVectSz / 2)
			return lerpColor(startJet, midJet, val / 500);
		return lerpColor(midJet, endJet, val / 500 - 1);
	}

	public int financeColorMap(float val) { // fonte (e TODO, para melhor
											// colormap):
											// http://cresspahl.blogspot.pt/2012/03/expanded-control-of-octaves-colormap.html

		val = maxVectSz - val;
		if (val <= maxVectSz / 2)
			return lerpColor(startFinance, midFinance, val / 500);
		return lerpColor(midFinance, endFinance, val / 500 - 1);
	}

	public int defaultColorMap(float val) {
		return jetColorMap(val);
	}

	class MultiPoint {
		float[] p = null;
		MultiPoint related[] = null;

		// geral: pode ser chamado com 0 args
		MultiPoint(float... numbers) { // throws Exception {
			p = new float[dimSz];
			int i = 0;

			for (float x : numbers) {
				p[i++] = x;
				if (i >= dimSz)
					return;
			}

			for (; i < dimSz; i++)
				p[i] = 0.0f;

		}

		MultiPoint(int[] v) { // Overload nao funciona neste caso??
			p = new float[dimSz];

			for (int i = 0; i < min(dimSz, v.length); i++)
				p[i] = (float) v[i];

			for (int i = v.length; i < dimSz; i++)
				p[i] = 0.0f;

		}

		MultiPoint(double[] v) { // Overload nao funciona neste caso??
			p = new float[dimSz];

			for (int i = 0; i < min(dimSz, v.length); i++)
				p[i] = maxVectSz * (float) v[i];

			for (int i = v.length; i < dimSz; i++)
				p[i] = 0.0f;

		}

		MultiPoint(float[] v, int dummy) { // Overload nao funciona neste caso??
			p = new float[dimSz];

			arrayCopy(v, 0, this.p, 0, min(dimSz, v.length));

			for (int i = v.length; i < dimSz; i++)
				p[i] = 0.0f;

		}

		final int deltaView = 4;
		// int calc_lvx=0;
		// int calc_lvy=0;

		public int calc(int dim) {
			float mxD = 0;
			int td = 0;
			float v;
			// println(dim+","+dimSz);
			for (int i = 0; i < dimSz; i++) {
				v = p[i] * transform[dim][i];
				if (v > mxD)
					mxD = v;
				td += v;
			}

			// if(mxD < deltaView) {return -1;}
			return td;
		}

		public int calcX() { /// borda de 5 pontos (para ver negativos?...)
			// int calclvx=calc(0);

			// if(v< deltaView) return -1;

			return 25 + (int) map(calc(0), 0, round(sz_dim[0] * maxVectSz), 15, width - 40);
		}

		public int calcY() {
			// int calclvy=calc(1);

			// if(v< deltaView) return -1;

			return height - 15 - (int) map(calc(1), 0, round(sz_dim[1] * maxVectSz), 15, height - 35);
		}

		public void display() {
			int x = this.calcX(), y = this.calcY();

			if (x < 0 || y < 0) {
				return;
			}

			if (colorDim >= 0) {
				int c = defaultColorMap((int) this.p[colorDim]);
				// println("cor:"+c);
				stroke(c);
				fill(c, 90);
			}

			ellipse(x, y, PointSz, PointSz);
		}

		public void display(int sz, String txt) {
			this.display(sz, 90, txt);
		}

		public void display(int sz, int alpha, String txt) {
			int x, y;

			x = this.calcX();
			y = this.calcY();
			// noStroke();

			if (x < 0 || y < 0) {
				return;
			}

			if (colorDim >= 0) {
				int c = defaultColorMap((int) this.p[colorDim]);
				stroke(c);
				fill(c, alpha);
			}
			ellipse(x, y, sz, sz);
			text(txt, x, y);
		}

		public void display_axis_val() {
			int x, y;
			String txt;

			textSize(12);
			ellipse(this.calcX(), this.calcY(), 8, 8);

			for (int i = 0; i < dimSz; i++) {

				txt = new Float(this.p[i]).toString();
				axis.axis_p[i] = new MultiPoint(v0.p);

				axis.axis_p[i].p[i] = this.p[i];
				axis.axis_p[i].display(8, txt);

			}
		}

		public void display_axis_XY() {
			display_axis_XY(8);
		}

		public void display_axis_XY(int textSz) {
			display_axis_XY(textSz, 90);
		}

		public void display_axis_XY(int textSz, int alpha) {
			int x, y;

			String txt;
			MultiPoint v1 = new MultiPoint(v0.p);

			ellipse(this.calcX(), this.calcY(), 8, 8);

			txt = "(";

			// for(int i=0;i<dimSz;i++) {

			// }

			// println("Vx:"+ax+" Vy:"+ay);

			textSize(textSz);
			for (int i = 0; i < dimSz; i++) {
				txt = txt + " " + new Float(this.p[i]).toString();
				v1.p[i] = this.p[i];
				MLine la = new MLine(this, v1);
				la.display(30);
				v1.display(3, ((Float) this.p[i]).toString());
				v1.p[i] = 0;
			}

			this.display(textSz, alpha, "  " + txt + ")");
		}

		public String toString_p() {
			String txt = "";
			for (int i = 0; i < dimSz; i++)
				txt = txt + " " + new Float(this.p[i]).toString();
			return txt;
		}

		public float[] delta(MultiPoint v2) {
			float d[] = new float[dimSz];

			for (int i = 0; i < dimSz; i++)
				d[i] = v2.p[i] - this.p[i];

			return d;
		}

		public float distance(int x, int y) {
			return dist((float) this.calcX(), (float) this.calcY(), (float) x, (float) y);
		}

	}

	// MLine
	class MLine extends MultiPoint {

		MultiPoint p2;

		MLine(MultiPoint p1, MultiPoint p2) {
			this.p = p1.p;
			this.p2 = p2;
		}

		public void display(int alpha) {
			if (colorDim >= 0) {
				int c = defaultColorMap((int) this.p[colorDim]);
				// println("cor:"+c);
				stroke(c, alpha);
				fill(c, alpha * 0.6f);
			}

			line(this.calcX(), this.calcY(), p2.calcX(), p2.calcY());
		}

		public void display() {
			if (colorDim >= 0) {
				int c = defaultColorMap((int) this.p[colorDim]);
				// println("cor:"+c);
				stroke(c);
				fill(c, 90);
			}

			line(this.calcX(), this.calcY(), p2.calcX(), p2.calcY());
		}

		public void display_noColorDim() {
			line(this.calcX(), this.calcY(), p2.calcX(), p2.calcY());
		}

		public float distance(int x, int y) {
			// fonte para os calculos distancia ponto a linha:
			// https://en.wikipedia.org/wiki/Multiview_orthographic_projection
			int x1 = this.calcX();
			int y1 = this.calcY();
			int x2 = p2.calcX();
			int y2 = p2.calcY();

			float dx = x2 - x1;
			float dy = y2 - y1;

			// println(x1+","+x2+","+y1+","+y2);
			// println( dx * x + dy* y + x2*y1+y2*x1);
			// println( sqrt(dx*dx + dy*dy) );
			// calcular distancia manualmente, pois ja ha coordenadas e valores
			// parciais
			return abs(dx * x + dy * y + x2 * y1 + y2 * x1) / sqrt(dx * dx + dy * dy);
		}

	}

	// UbiSOM

	UbiSOM3 ubisom;
	// DataStreamFeatureRange minmax_norm;
	double[] observation;

	PImage gridSOM = null; // desenho do grid do SOM

	int catDim = -1; // Dimensao a ignorar (com categoria)

	int SOMIteration = 0; // contar itera\u00e7\u00f5es de treino
	float BMU_max_cnt = 30; // contagem m\u00e1xima na UMat

	float maxDistanceSOM = 0; // 0.005; // valor maximo (cor red) para distancia
								// na UMat
	float maxDistanceSOMy = 0; // 0.05; // valor maximo (cor red) para distancia
								// na UMat (vertical?? TODOisto faz sentido??)

	final int SOM_MAP_X = 20;
	final int SOM_MAP_Y = 40;

	SOMPat dataBMU;
	SOMLabel dataLabels;

	boolean learn_mode = true;
	// TODO: Classes SOM e UMAT...

	boolean show_grid_som = false; // draw som grid

	float dh[][], dv[][]; /// UMat Distance aux mat

	double[] minValues = new double[dimSz];
	double[] maxValues = new double[dimSz];

	public void setup_SOM() {
		// UbiSOM3

		// Inicializa\u00e7\u00e3o de referencia

		// Parameters.SOM_FIXED_INITIALIZATON = true; //uses a fixed random seed

		// UbiSOM: Tamanho da janela

		// Parameters.ORDERING_ITERATIONS = 1500;
		// Parameters.STATISTICS_WINDOW_SIZE = 1500;

		//
		ubisom = UbiSOMFactory.create(SOM_MAP_X, // width
				SOM_MAP_Y, // height
				dimSz, // data dimensionality
				0.1f, // eta_0
				0.08f, // eta_f
				0.6f, // sigma_0
				0.2f, // sigma_f
				0.7f, // beta
				2000 // T
		);
		// new UbiSOM3(SOM_MAP_X, SOM_MAP_Y, dimSz,
		// 0.1, 0.08,
		// 0.6, 0.2,
		// 0.7);

		for (int i = 0; i < dimSz; i++) {
			minValues[i] = 0;
			maxValues[i] = (double) maxVectSz;
		}

		// minmax_norm = new DataStreamFeatureRange(minValues, maxValues);

		dataBMU = new SOMPat();
		dataLabels = new SOMLabel();

		dh = new float[dataBMU.maxSzX][dataBMU.maxSzX];
		dv = new float[dataBMU.maxSzX][dataBMU.maxSzX];

	}

	class SOMLabel {

		boolean isActive;
		int currLabel;
		int label[][];
		double lastDist[][];
		double distLabel = 0.1f;
		int iterationDef[][];
		// int lastIteration; // ultima vez que foi pesquisado: evita chamada
		// duplicada??
		int bmu_x;
		int bmu_y; // o processo de marcacao acaba por calcular a bmu. fica aqui
					// guardado para evitar duplicacao.

		SOMLabel() {
			label = new int[SOM_MAP_X][SOM_MAP_Y];
			iterationDef = new int[SOM_MAP_X][SOM_MAP_Y];
			lastDist = new double[SOM_MAP_X][SOM_MAP_Y];
			isActive = false;

			reset();

		}

		public void reset() {
			for (int x = 0; x < SOM_MAP_X; x++)
				for (int y = 0; y < SOM_MAP_Y; y++) {
					label[x][y] = -1;
					lastDist[x][y] = Double.MAX_VALUE;
					iterationDef[x][y] = 0;
				}
		}

		public void set(int val) {
			isActive = true;
			currLabel = val;
		}

		public void set(int val, int dist) {
			isActive = true;
			currLabel = val;
			distLabel = ((double) dist) / 1000.0f;
		}

		public void markUnits(int x, int y, double dist) { // determina o match
															// de cada unidade e
															// classifica todas
															// as relevantes
			if (isActive && ((dist < distLabel && dist < lastDist[x][y]) || distLabel == 0)) {
				lastDist[x][y] = dist;
				label[x][y] = currLabel;
				iterationDef[x][y] = SOMIteration;
			}
		}

		public void colorUnit(float p[], int x, int y) {

			int c = color(0, 75, 75);

			if (label[x][y] >= 0) {
				c = defaultColorMap(label[x][y]);

				strokeWeight(5);
			} else if (colorDim >= 0) {
				c = defaultColorMap((int) p[colorDim]);// min(unit.p[colorDim]+50,255));
				strokeWeight(2);
			} else
				strokeWeight(1);

			stroke(c);
			// textSize(8);
			textSize(8);
		}

		public void displayAll() { // apresenta ultima unidade marcada (padrao
									// atual?)

			int px, py;

			if (show_umat_flag) {
				return;
			} /// Se em modo umat, nao faz nada

			for (int x = 0; x < SOM_MAP_X; x++)
				for (int y = 0; y < SOM_MAP_Y; y++)
					if (label[x][y] >= 0) {
						if (iterationDef[x][y] > SOMIteration - 30) { // &&
																		// !show_umat_flag)
																		// {
							int sz = min(20, (35 + iterationDef[x][y] - SOMIteration) / 3);

							MultiPoint unit = new MultiPoint(ubisom.get(x, y).getDoubleVector());
							px = unit.calcX();
							py = unit.calcY();
							strokeWeight(sz / 2 + 4);

							stroke(255, 255, 255, 150);
							line(px - sz - 1, py - sz - 1, px + sz - 1, py + sz - 1);
							line(px + sz - 1, py - sz - 1, px - sz - 1, py + sz - 1);
							strokeWeight(sz / 2);

							colorUnit(unit.p, x, y);

							line(px - sz, py - sz, px + sz, py + sz);
							line(px + sz, py - sz, px - sz, py + sz);
						} else {
							label[x][y] = -1;
							lastDist[x][y] = Double.MAX_VALUE;
							iterationDef[x][y] = 0;
						}
					}

		}
	}

	// classe SOMPat -- Gere os padr�es do SOM, possibilitando historico da
	// stream e resampling.

	final int maxSizeBMUDisplay = 30; /// faz display destes pontos

	class SOMPat {
		double dist[];
		int bmu_x[];
		int bmu_y[];
		MultiPoint mp[];
		int currBMU = 0;
		int nPats = 0;

		// double top_err[]; -- basta tirar media das (ultimas) distancias em
		// dist[]
		double top_err[]; // vector para calculo do erro de topologia
		float mean_err[]; // vector para calculo do erro m�dio nas ultimas
							// maxSizeBMU iterac�es

		// UMat stuff

		int maxSzX = 0;
		int maxSzY = 0;

		// plot6 plot_err;
		ScatterPlot plot_err;

		// double max_v[];
		// double min_v[];

		float mean_top[];

		SOMPat() {
			dist = new double[maxSizeBMU];
			bmu_x = new int[maxSizeBMU];
			bmu_y = new int[maxSizeBMU];
			mp = new MultiPoint[maxSizeBMU];
			top_err = new double[maxSizeBMU];
			// quant_err= new double [maxSizeBMU]; // 1 ou 0
			mean_err = new float[maxSizeBMU];
			mean_top = new float[maxSizeBMU];
			// max_v=new double [dimSz];
			// min_v=new double [dimSz];

			for (int i = 0; i < maxSizeBMU; i++) {
				mean_err[i] = 1;
				mean_top[i] = 1;
			}
			// quant_err[i] = Double.MAX_VALUE;
			// }

			// UMat stuff

			maxSzX = width * 5 / (6 * ubisom.getWidth());
			maxSzY = height * 5 / (6 * ubisom.getHeight());

		}

		public double[] normalize(double[] v) {

			for (int i = 0; i < dimSz; i++) {
				// if(v[i]>max_v[i]) max_v[i]=v[i];
				// if(v[i]<minValues[i]) min_v[i]=v[i];

				v[i] = (v[i] - minValues[i]) / (maxValues[i] - minValues[i]);
			}
			return v;
		}

		public double[] SOMLearn0(MultiPoint p) {

			observation = new double[dimSz];

			for (int i = 0; i < dimSz; i++) // ignora a cor
				if (i != catDim) {
					observation[i] = (double) p.p[i]; /// 1000.0;
				} else {
					observation[i] = 0;
				}

			observation = normalize(observation);
			if (learn_mode) {
				ubisom.learn(observation);
				SOMIteration++;

			}
			gridSOM = null;

			return observation;
		}

		public void SOMLearn(MultiPoint p) {

			double o[] = SOMLearn0(p);

			mp[currBMU] = p;
			find_bmu(currBMU, o); /// inclui marcacao de unidade qd activa...

			if (nPats < maxSizeBMU)
				nPats++;
			else
				plot_err(currBMU); // So calcula o erro quando ha pontos
									// suficientes

			currBMU++;
			if (currBMU >= maxSizeBMU)
				currBMU = 0;

		}

		// calcula o melhor BMU para idx e faz reset a todos os calculos de
		// erro.
		public void find_bmu(int idx, double p[]) {
			double d;
			dist[idx] = Double.MAX_VALUE; /// Nota: faz sentido guardar a
											/// distancia entre iteracoes:
											/// trata-se da BMU no momento
											/// anterior e em cada indice o
											/// valor eh reset para maximo.
			double second_best = dist[idx];
			int bmu_x2 = -2, bmu_y2 = -2;

			for (int k = 0; k < ubisom.getHeight(); k++) {
				for (int i = 0; i < ubisom.getWidth(); i++) {
					d = distanceSOM(p, ubisom.get(i, k).getDoubleVector());
					find_bmu_setMarkUnits(d,k,i);
					
					if (d < dist[idx]) 
					{
						if (second_best > dist[idx]) {
							bmu_x2 = bmu_x[idx];
							bmu_y2 = bmu_y[idx];
							second_best = dist[idx];
						}
						dist[idx] = d;
						bmu_x[idx] = i;
						bmu_y[idx] = k;
						// println("found on "+idx+"d:"+d+":"+i+","+k);
					} 
					else 
					{
						if (second_best > d) {
							second_best = d;
							bmu_x2 = i;
							bmu_y2 = k;
						}
					}
				}
			}
			
			find_bmu_setTopErr(bmu_x2, bmu_y2, idx);

			if (dataLabels.distLabel == 0)
				dataLabels.markUnits(bmu_x[idx], bmu_y[idx], dist[idx]);

			// println("final:"+idx+"("+bmu_x[idx]+","+bmu_y[idx]+")="+dist[idx]+"top_err:"+top_err[idx]+"
			// x2:"+bmu_x2+"y2:"+bmu_y2);

			// println("distance to:");
			// printArray(ubisom.get(bmu_x[idx],bmu_y[idx]).getDoubleVector());

		}
		
		void find_bmu_setTopErr(int bmu_x2, int bmu_y2, int idx)
		{
			if ((bmu_x2 == bmu_x[idx] && abs(bmu_y2 - bmu_y[idx]) == 1)
					|| (bmu_y2 == bmu_y[idx] && abs(bmu_x2 - bmu_x[idx]) == 1))
				top_err[idx] = 0;
			else
				top_err[idx] = 1;
		}
		
		void find_bmu_setMarkUnits(double d, int i, int k)
		{
			if (dataLabels.distLabel > 0)
				dataLabels.markUnits(i, k, d);
		}

		////////// calculo do erro do SOM

		int cnt_last_calc_err = 0;

		final int refresh_err_plot = 15;

		void plot_err(int idx) {

			if (!show_plot_err) {
				return;
			}

			if (cnt_last_calc_err < refresh_err_plot && iterations_to_sleep < 20) {
				cnt_last_calc_err++;
				return; // se houver trabalho, so actualiza grafico do erro de
						// 30 em 30 pontos (senao fica muito lento)
			}

			cnt_last_calc_err = 0;

			float v_err[] = new float[maxSizeBMU];
			float v_top[] = new float[maxSizeBMU];

			// for(int i=idx-refresh_err_plot+1;i<=idx; i++) ????????
			// if(i<0) mean_err[i+maxSizeBMU]=0; else mean_err[i]=0;

			float curr_mean_err = 0, curr_mean_top = 0;

			for (int i = 0; i < maxSizeBMU; i++) {
				curr_mean_err += dist[idx]; // calcula media actual
				v_err[i] = (float) dist[idx];

				// curr_mean_top += top_err[idx];
				if (top_err[idx] > 0) {
					curr_mean_top++;
					v_top[i] = 1;
				} else
					v_top[i] = 0;

				idx--;
				if (idx < 0)
					idx = maxSizeBMU - 1;
			}

			float hard_line[] = new float[maxSizeBMU];

			curr_mean_err = curr_mean_err / maxSizeBMU;

			for (int i = maxSizeBMU - 1; i > 0; i--) {
				hard_line[i] = curr_mean_err;
				mean_err[i] = mean_err[i - 1];
				mean_top[i] = mean_top[i - 1];
			}
			hard_line[0] = curr_mean_err;
			mean_err[0] = curr_mean_err;
			mean_top[0] = curr_mean_top / maxSizeBMU;

			println("erro:" + mean_err[0]);

			// plot_err= new plot6( mean_err, max(mean_top), SOMIteration); ///
			// new plot(multidim.this, v_err, SOMIteration); //plot6
			plot_err = new ScatterPlot(my_screen, mean_err, max(mean_top), SOMIteration); /// new
																				/// plot(multidim.this,
																				/// v_err,
																				/// SOMIteration);
																				/// //plot6

			plot_err.add(mean_err, color(200, 120, 120));
			plot_err.add(v_err, color(200, 200, 120));

			plot_err.add(hard_line, color(20, 20, 180));
			plot_err.add(mean_err, color(150, 20, 150));
			plot_err.add(mean_top, color(150, 150, 120));

			// return err_plot;

			// return err_plot; //.makePlot()
		}

		public int cnt(int x, int y) {
			int c = 0;

			for (int i = 0; i < min(maxSizeBMU, nPats); i++) {
				if (bmu_x[i] == x && bmu_y[i] == y)
					c++;
			}

			// if(c>max_cnt) max_cnt=c; else max_cnt=0.9*max_cnt+0.1*c;
			return c;
		}

		public void display() {
			int j, i;

			j = currBMU - 1;
			if (j < 0) {
				j = maxSizeBMU - 1;
			}

			for (i = 0; i < min(maxSizeBMUDisplay, nPats); i++) {
				MultiPoint unit = new MultiPoint(ubisom.get(bmu_x[j], bmu_y[j]).getDoubleVector());
				MLine l1 = new MLine(mp[j], unit);

				// println(j);
				if (colorDim >= 0) {
					int c = defaultColorMap((int) unit.p[colorDim]);// min(unit.p[colorDim]+50,255));
					// println("cor:"+c);
					stroke(c);
					// fill(financeColorMap((int) unit.p[colorDim]),90);
				}
				textSize(max(8, 20 - i / 3));
				strokeWeight(2);
				l1.display();
				mp[j].display(max(2, 30 - i), 155, // min(10+i*3,100),
						"_" + ((float) round((float) dist[j] * 100)) / 100);

				// "u("+bmu_x[j] +","+bmu_y[j]+")="+dist[j]);
				j--;
				if (j < 0) {
					j = maxSizeBMU - 1;
				}
			}
		}

		public int getUMatX(int k) {

			return (int) ((k + 0.1f) * maxSzX);
		}

		public int getUMatY(int i) {
			return (int) (30 + (i + 0.5f) * maxSzY);
		}

		public String StringPat(int offset) { //// TODO: EM CONSTRUCAO
			// String s;
			int j = currBMU - offset - 1;

			if (j < 0) {
				j = maxSizeBMU + j;
			}
			return "u(" + bmu_x[j] + "," + bmu_y[j] + ")=" + dist[j] + " " + (SOMIteration - offset) + " p("
					+ mp[j].toString_p() + ")";

		}

		public void displayUMat_line(int offset) {
			int j;
			int xtxt = width * 5 / 6 + 10, ytxt = 42 + 20 * offset;

			j = currBMU - offset - 1;
			if (j < 0) {
				j = maxSizeBMU + j;
			}
			println(j + " " + getUMatX(bmu_x[j]) + " " + getUMatY(bmu_y[j]), xtxt, ytxt);

			stroke(0, 0, 0, 100);
			strokeWeight(3);
			line(getUMatX(bmu_x[j]) + dataBMU.maxSzX / 2, getUMatY(bmu_y[j]), xtxt, ytxt);

			// stroke(50,180,50,150);
			stroke(5, 30, 50, 50);
			strokeWeight(2);
			line(getUMatX(bmu_x[j]) + dataBMU.maxSzX / 2, getUMatY(bmu_y[j]), xtxt, ytxt);

			String txt = "-" + (SOMIteration - offset) + " p(" + mp[j].toString_p() + ")";
			fill(5, 30, 50, 200);
			text(txt.substring(0, 44), xtxt, ytxt + 2);

		}

		public void displayUMat() {

			defaultColor();

			textSize(10);
			stroke(50, 180, 50, 150);

			strokeWeight(2);

			if (!show_grid_som)
				return; /// S\u00f3 mostra informacao complementar em modo
						/// show_grid_som

			for (int i = 0; i < min(maxSizeBMUDisplay, nPats); i++)
				displayUMat_line(i);

		}
	}

	public static double square(double num) {
		return num * num;
	}

	// to obtain the closest (denormalized) codebook vector for observation, do:
	// codeVector = minmax_norm.denormalize(
	// ubisom.getCodebookVectorFor(observation) );
	// ellipse((int)codeVector[0], (int)codeVector[1], 2, 2);

	// ubisom.getCodebookVectorFor(observation)
	// public final somlp.model.nn.ubisom.BestMatchingUnit
	// ubisom.bmu(observation);

	// BestMatchingUnit
	// public class somlp.model.nn.ubisom.BestMatchingUnit extends
	// java.lang.Object{
	// public int x;
	// public int y;
	// public double quantizationError;
	// public somlp.model.nn.ubisom.BestMatchingUnit(int, int);
	// public void reset(int, int);
	// public double distance(somlp.model.nn.ubisom.BestMatchingUnit);
	// public static double distance(somlp.model.nn.ubisom.BestMatchingUnit,
	// somlp.model.nn.ubisom.BestMatchingUnit);
	// public boolean isNeighbor(somlp.model.nn.ubisom.BestMatchingUnit);

	public MultiPoint mapSOMProt(double[] p) {
		MultiPoint out = new MultiPoint();

		for (int i = 0; i < p.length; i++)
			out.p[i] = map((float) p[i], 0, 1, 0, maxVectSz);

		return out;
	}

	public void printSOMGrid() {

		if (!show_grid_som)
			return;
		int ox, oy, x, y, lx, ly;
		double[] prot, protO, protL;

		// background(0);

		if (gridSOM != null) {
			image(gridSOM, 0, 0);
			return; // N\u00e3o h\u00e1 pontos novos, nem nova
					// transforma\u00e7\u00e3o
		}

		gridSOM = createImage(width, height, RGB);
		// strokeWeight(2);
		stroke(102, 152, 152, 100);
		// stroke(255,202,202);

		for (int k = 0; k < ubisom.getHeight() - 1; k++) {

			protO = ubisom.get(0, k).getDoubleVector();
			protL = ubisom.get(0, k + 1).getDoubleVector();

			MLine ln = new MLine(mapSOMProt(protO), mapSOMProt(protL));

			ln.display();

			for (int i = 1; i < ubisom.getWidth(); i++) {

				prot = ubisom.get(i, k).getDoubleVector();
				protL = ubisom.get(i, k + 1).getDoubleVector();

				MLine ln1 = new MLine(mapSOMProt(protO), mapSOMProt(prot));
				MLine ln2 = new MLine(mapSOMProt(prot), mapSOMProt(protL));

				ln1.display();
				ln2.display();

				protO = prot;

				// print("["+i+","+k+"]=("+x+","+y+") ");
			}

		}

		int k = ubisom.getHeight() - 1;
		protO = ubisom.get(0, k).getDoubleVector();

		for (int i = 1; i < ubisom.getWidth(); i++) {

			prot = ubisom.get(i, k).getDoubleVector();

			MLine ln = new MLine(mapSOMProt(protO), mapSOMProt(prot));
			ln.display_noColorDim();

			protO = prot;
		}

		/// TODO: Corrigir o bug que n\u00e3o faz update do x,y do neuronio
		dataBMU.display();

		gridSOM = get();

	}

	// public String[] prototypeSOMString() { //// Nao utilizado, ver
	// prototypeSOMCsvString()
	// double prot[];
	// String s[]=new String[ubisom.getHeight()*ubisom.getHeight()+1];
	// int cnt=0;
	//
	//
	// for(int i=0;i<ubisom.getHeight();i++)
	// for(int k=0;k<ubisom.getWidth();k++) {
	// prot=ubisom.get(k,i).getDoubleVector();
	// if(cnt++==0) { // se for 0 passa a 1 (header) senao passa para proxima
	// linha
	// s[0]="#SOM prototypes line_col_sz_it_date "+ubisom.getHeight()+"
	// "+ubisom.getWidth()+" "+prot.length+" "+SOMIteration+" "+mydate(0);
	// }
	// s[cnt++]="# line_col "+k+" "+i;
	// for(int m=0;m<prot.length;m++) {
	// if(m==0) s[cnt]= ""+round((float) prot[m]*maxVectSz); else s[cnt]=s[cnt]
	// + " "+ round((float) prot[m]*maxVectSz);
	// }
	// }
	//
	// return s;
	// }
	
	int [][] umat_avg;

	public String[] prototypeSOMCSVString() {
		double prot[];
		String s[] = new String[ubisom.getHeight() * ubisom.getHeight() + 1];
		int cnt = 0;

		for (int i = 0; i < ubisom.getHeight(); i++)
			for (int k = 0; k < ubisom.getWidth(); k++) {
				int cntBMU = dataBMU.cnt(k, i);
				prot = ubisom.get(k, i).getDoubleVector();
				if (cnt++ == 0) { // se for 0 passa a 1 (header) senao passa
									// para proxima linha
					// s[0]="#SOM prototypes line_col_sz_it_date
					// "+ubisom.getHeight()+" "+ubisom.getWidth()+"
					// "+prot.length+" "+SOMIteration+" "+mydate(0);
					s[0] = axis.axis_s[0]; // f1,f2,f3,f4"; ///BUG: TODO
					for (int ii = 1; ii < currDimSz; ii++)
						s[0] = s[0] + ", " + axis.axis_s[ii];
					s[0] = s[0] + ", cnt, UMat";
				}
				// s[cnt++]="# line_col "+k+" "+i;
				for (int m = 0; m < currDimSz; m++) { /// prot.length;m++) {
					if (m == 0)
						s[cnt] = "" + round((float) prot[m] * maxVectSz);
					else
						s[cnt] = s[cnt] + "," + round((float) prot[m] * maxVectSz);
				}
				
				if(cntBMU == 0){
					s[cnt] = s[cnt] + "," + cntBMU + "," + umat_avg[k][i] ; // Math.log(cntBMU + 1);
				}
				else{
					s[cnt] = s[cnt] + "," + (cntBMU+10) + "," + umat_avg[k][i] ; // Math.log(cntBMU + 1);
				}
				
				
			}

		return s;
	}

	////////////////////

	public String mydate(int offset) {
		Date d = new Date();
		long timestamp = d.getTime() + (86400000 * offset);
		String date = new java.text.SimpleDateFormat("yyyyMMdd HHmmss").format(timestamp);
		return date;
	}

	// int time_out_show_umat=150;

	///////////////////////////////////////////////////////////

	float minDistanceSOM = 0;

	public void set_umat_cell_color(float v) {

		v = (v - minDistanceSOM) / (maxDistanceSOM - minDistanceSOM);

		v = sqrt(sqrt((v * v * v))); // realcar valores intermedios
		/// v=sqrt(v); // realcar valores intermedios

		int c = jetColorMap(min(maxVectSz, v * maxVectSz));

		stroke(c);
		fill(c);

	}
	
	public void show_umat()
	{
		// if(time_out_show_umat<1) {
		// time_out_show_umat=150;
		// show_umat_flag=false;
		// } else
		// time_out_show_umat--;

		
		
		// , protL;

		int x, y, cx, cy;
		cx = dataBMU.maxSzX / 3;
		cy = dataBMU.maxSzY / 3;

		calc_umat();
		show_legenda_umat(cx, cy);
		show_umat_ProccessXY(cx, cy);

		//// Cantos

		stroke(40);
		fill(40);
		textSize(8);
		text("^(" + ubisom.getWidth() + "," + ubisom.getHeight() + ")", width * 5 / 6 - 30, height * 7 / 8 + 10);
		text("Dist  [" + minDistanceSOM + ";" + round(maxDistanceSOM * 1000) / 1000.0f + "]", width * 5 / 6 - 30,
				height * 7 / 8 + 20);
		//////////// Nm. hits

		strokeWeight(3);
		stroke(0);
		show_umat_Proccess(cx, cy);
		
		dataBMU.displayUMat();

		//// Grafico do erro

		if (iterations_to_sleep > 20 && dataBMU.cnt_last_calc_err > 0) {
			dataBMU.plot_err(dataBMU.currBMU);
		} // update erro em caso de idle
		if (dataBMU.plot_err != null && show_plot_err)
			dataBMU.plot_err.draw(0, height * 5 / 6 + 2, width - 30, height / 6 - 10);

	}
	
	public void show_umat_ProccessXY(int cx, int cy)
	{
		int km, im, kr, ir, x, y;
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) {

				/// evita calculos nos extremos
				km = k - 1 < 1 ? 1 : k-1;
				kr = k < 1 ? 1 : k;
				// kp=k+1;
				
				// if(kp>=ubisom.getWidth()) {kp=ubisom.getWidth()-1;}

				im = i - 1 < 1 ? 1 : i-1;
				ir = i < 1 ? 1 : i;
				// ip=i+1;
				// if(ip>=ubisom.getHeight()) {ip=ubisom.getHeight()-1;}

				x = dataBMU.getUMatX(k) + cx / 2;
				y = dataBMU.getUMatY(i) - cy / 2;

				// up left
				set_umat_cell_color((dh[km][ir] + dv[kr][im]) / 2.0f);
				rect(x - cx, y - cy, cx + 1, cy);
				// up center
				set_umat_cell_color(dv[kr][im]);
				rect(x, y - cy, cx, cy);
				// up right
				set_umat_cell_color((dh[kr][ir] + dv[kr][im]) / 2.0f);
				rect(x + cx, y - cy, cx + 1, cy);

				// center left
				set_umat_cell_color(dh[km][ir]);
				rect(x - cx, y, cx, cy);
				// up center
				set_umat_cell_color((dh[kr][ir] + dv[kr][ir]) / 2.0f);
				rect(x, y, cx, cy);
				// up right
				set_umat_cell_color(dh[kr][ir]);
				rect(x + cx, y, cx + 1, cy);

				// bt left
				set_umat_cell_color((dh[km][ir] + dv[kr][ir]) / 2.0f);
				rect(x - cx, y + cy, cx, cy);
				// bt center
				set_umat_cell_color(dv[kr][ir]);
				rect(x, y + cy, cx, cy);
				// bt right
				set_umat_cell_color((dh[kr][ir] + dv[kr][ir]) / 2.0f);
				rect(x + cx, y + cy, cx + 1, cy);

			}
	}

	public void show_umat_Proccess(int cx, int cy)
	{
		int x,y;
		float cntBMU;
		double prot[];
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) {
				cntBMU = dataBMU.cnt(k, i);

				x = dataBMU.getUMatX(k);
				y = dataBMU.getUMatY(i);
				
				if((x -cx  < mouseX) && (x + 2*cx > mouseX) && (y- 2*cy < mouseY) && (y + 2*cy > mouseY)){
	                        textSize(16);
	                        fill(250);
	                        noStroke();
	                        rect(width * 5 / 6 - 30, height * 7 / 8 + 24,200,16);
	                        fill(40);
	                        stroke(0);
	                        text("Unit (" + k + " , " + i + ")= " + cntBMU + " & "+umat_avg[k][i], width * 5 / 6 - 30, height * 7 / 8 + 40);
	                        textSize(8);
	                    }

				/// todo -- CAT: obter cor de classe maioritaria? fazer pie?
				if (colorDim >= 0) {

					prot = ubisom.get(k, i).getDoubleVector();

					int c = defaultColorMap((float) prot[colorDim] * maxVectSz);// min(unit.p[colorDim]+50,255));
					// println("cor:"+c);
					fill(c, 220);
				}

				//desenha os pontos da Umat
				if (cntBMU > 0)
					ellipse(x + cx, y, 3 + min(2 * cx, sqrt(cntBMU) * dataBMU.maxSzX / sqrt(BMU_max_cnt)),
							3 + min(2 * cy, sqrt(cntBMU) * dataBMU.maxSzY / sqrt(BMU_max_cnt))); // TODO:
																									// 15
																									// funciona
																									// no
																									// iris:
																									// sqrt(500/3)

			}
	}
	
	private void calc_umat(){
		
		
		/// matriz distancias

		if (SOMIteration > 200) {
			maxDistanceSOM = 0.95f * maxDistanceSOM;
			minDistanceSOM = 1.05f * minDistanceSOM;
		}
		
		calc_umat_ProccessDH();
		calc_umat_ProccessDV();
		calc_umat_ProccessXY();
	}
	
	private void calc_umat_ProccessDH()
	{
		double[] prot;
		double[] prot0;
		
		for (int k = 1; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) 
			{
				prot = ubisom.get(k, i).getDoubleVector();
				prot0 = ubisom.get(k - 1, i).getDoubleVector();

				dh[k][i] = distanceSOM(prot, prot0);

				if (dh[k][i] < minDistanceSOM && SOMIteration > 200)
					minDistanceSOM = dh[k][i];

				if (dh[k][i] > maxDistanceSOM && SOMIteration > 200)
					maxDistanceSOM = dh[k][i];
			}
	}
	
	private void calc_umat_ProccessDV()
	{
		double[] prot;
		double[] prot0;
	
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 1; i < ubisom.getHeight(); i++)
			{
				prot = ubisom.get(k, i).getDoubleVector();
				prot0 = ubisom.get(k, i - 1).getDoubleVector();

				dv[k][i] = distanceSOM(prot, prot0);

				if (dv[k][i] < minDistanceSOM && SOMIteration > 200)
					minDistanceSOM = dv[k][i];

				if (dv[k][i] > maxDistanceSOM && SOMIteration > 200)
					maxDistanceSOM = dv[k][i];
			}
	}

	private void calc_umat_ProccessXY()
	{
		int cx, cy;
		cx = dataBMU.maxSzX / 3;
		cy = dataBMU.maxSzY / 3;

		int km, im, kr, ir; // , kp, ip;
		
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) 
			{
				/// evita calculos nos extremos
				km = k - 1;
				kr = k;
				// kp=k+1;
				if (km < 1) {
					km = 1;
				}
				if (kr < 1) {
					kr = 1;
				}
				// if(kp>=ubisom.getWidth()) {kp=ubisom.getWidth()-1;}

				im = i - 1;
				ir = i;
				// ip=i+1;
				if (im < 1) {
					im = 1;
				}
				if (ir < 1) {
					ir = 1;
				}
				// if(ip>=ubisom.getHeight()) {ip=ubisom.getHeight()-1;}

				x = dataBMU.getUMatX(k) + cx / 2;
				y = dataBMU.getUMatY(i) - cy / 2;

				// NOTA: pode ser enviada nova vers�o desta f�rmula (para
				// retirar a m�dia)
				umat_avg[k][i] = (int) ((dh[km][ir] + dv[kr][im] + dh[kr][ir] + dv[kr][ir] + dh[km][ir] + dh[kr][ir])
						* 1000 / 6);
			}
	}
	
	private void show_point_coordinates(){
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) {
				int cntBMU = dataBMU.cnt(k, i);

				x = dataBMU.getUMatX(k);
				y = dataBMU.getUMatY(i);
				
				int maxSzX = this.width * 5 / (6 * this.ubisom.getWidth());
				int maxSzY = this.height * 5 / (6 * this.ubisom.getHeight());
				
				int cx = maxSzX / 3;
				int cy = maxSzY / 3;
				
				if((x - cx < mouseX) && (x + cx > mouseX) && (y - cy < mouseY) && (y + cy> mouseY)){
					textSize(8);
					text("Unit ( " + k + " , " + i + " ) : " + cntBMU, width * 5 / 6 - 30, height * 7 / 8 + 40);
				}
			}
	}
	

	///////////////////////////////////////////////////////////////////

	public void show_umat_dyamonds()
	{
		double prot[];
		double protO[];
		double prot0[];

		// , protL;

		int cx, cy;

		cx = dataBMU.maxSzX / 2;
		cy = dataBMU.maxSzY / 2;
		show_legenda_umat(cx, cy);
		
		show_umat_dyamonds_ProccessXY(cx, cy);
		
		prot = ubisom.get(0, 0).getDoubleVector();

		prot0 = new double[prot.length];
		for (int i = 0; i < prot.length; i++)
			prot0[i] = 0;

		protO = ubisom.get(ubisom.getWidth() - 1, ubisom.getHeight() - 1).getDoubleVector();

		defaultColor();
		// fill(5, 30, 50, 200);
		if (distanceSOM(prot, prot0) < distanceSOM(protO, prot0)) {
			// text("(0,0)",10,10);
			textSize(8);
			text("^(" + ubisom.getWidth() + "," + ubisom.getHeight() + ")", width * 5 / 6, height * 5 / 6);
		} else {
			// text("(1,1)",10,10);
			textSize(8);
			text("^(0,0)/(" + ubisom.getWidth() + "," + ubisom.getHeight() + ")", width * 5 / 6, height * 5 / 6);
		}

		// fill(0,250,0,150);
		// defaultColor();
		strokeWeight(3);
		stroke(0);
		
		show_umat_dyamonds_Proccess(cx,cy);
		dataBMU.displayUMat();
	}
	
	public void show_umat_dyamonds_ProccessXY(int cx, int cy)
	{
		int x,y;
		double[] prot;
		double[] protO;
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) {
				prot = ubisom.get(k, i).getDoubleVector();

				x = dataBMU.getUMatX(k);
				y = dataBMU.getUMatY(i);

				if (k > 0) {
					float d;

					protO = ubisom.get(k - 1, i).getDoubleVector();
					d = distanceSOM(prot, protO);
					if (d > maxDistanceSOM && SOMIteration > 1000) {
						maxDistanceSOM = 0.8f * maxDistanceSOM + 0.2f * d;
						// println("new maxDistanceSOM:"+maxDistanceSOM);
					} else // if(d<0.95*maxDistanceSOM)
					{
						maxDistanceSOM = 0.98f * maxDistanceSOM;
					}

					stroke(jetColorMap(min(maxVectSz, (d / maxDistanceSOM) * maxVectSz)));
					fill(jetColorMap(min(maxVectSz, (d / maxDistanceSOM) * maxVectSz)));

					quad(x - cx, y, x, y - cy, x + cx, y, x, y + cy);
					// quad(x-cx,y,x,y-0.7*cy,x+cx,y, x, y+0.7*cy);
					if (show_grid_som) {
						stroke(255); // jetColorMap( min(maxVectSz,
										// (d/maxDistanceSOM)*maxVectSz)));
						line(x - 0.2f * cx, y, x + 0.2f * cx, y);
					}
					// line(x-cx, y, x+cx,y);
				}
				if (i < ubisom.getHeight() - 1) {
					float d;

					protO = ubisom.get(k, i + 1).getDoubleVector();
					d = distanceSOM(prot, protO);
					if (d > maxDistanceSOMy && SOMIteration > 1000) {
						maxDistanceSOMy = 0.8f * maxDistanceSOMy + 0.2f * d;
						// println("new maxDistanceSOMy:"+maxDistanceSOMy);
					} else // if(d<0.95*maxDistanceSOMy)
					{
						maxDistanceSOMy = 0.98f * maxDistanceSOMy;
					}

					stroke(jetColorMap(min(maxVectSz, (d / maxDistanceSOM) * maxVectSz)));
					fill(jetColorMap(min(maxVectSz, (d / maxDistanceSOMy) * maxVectSz)));

					quad(x, y + cy, x + cx, y, x + 2 * cx, y + cy, x + cx, y + 2 * cy);
					// quad(x+0.3*cx,y+cy,x+cx,y,x+1.7*cx,y+cy, x+cx, y+2*cy);

					if (show_grid_som) {
						stroke(255);
						line(x + cx, y + 0.5f * cy, x + cx, y + cy);
					}
				}

			}
	}
	public void show_umat_dyamonds_Proccess(int cx, int cy)
	{
		double[] prot0;
		float cntBMU;
		int x,y;
		for (int k = 0; k < ubisom.getWidth(); k++)
			for (int i = 0; i < ubisom.getHeight(); i++) {
				cntBMU = dataBMU.cnt(k, i);

				x = dataBMU.getUMatX(k);
				y = dataBMU.getUMatY(i);

				/// todo -- CAT: obter cor de classe maioritaria? fazer pie?
				if (colorDim >= 0) {

					prot0 = ubisom.get(k, i).getDoubleVector();

					int c = jetColorMap((float) prot0[colorDim] * maxVectSz);// min(unit.p[colorDim]+50,255));
					// println("cor:"+c);
					fill(c, 220);
				}

				if (cntBMU > 0)
					ellipse(x + cx, y, 3 + min(2 * cx, sqrt(cntBMU) * dataBMU.maxSzX / sqrt(BMU_max_cnt)),
							3 + min(2 * cy, sqrt(cntBMU) * dataBMU.maxSzY / sqrt(BMU_max_cnt))); // TODO:
																									// 15
																									// funciona
																									// no
																									// iris:
																									// sqrt(500/3)

			}
	}
	

	//////////////////////////////////////////////////////////

	PImage img_legenda_umat = null;

	public void show_legenda_umat(int cx, int cy) {
		// legenda

		if (img_legenda_umat == null) {
			defaultColor();
			strokeWeight(3);
			stroke(0, 100, 20); // fundo preto

			y = dataBMU.getUMatY(ubisom.getHeight() + 2);
			int y2 = dataBMU.getUMatY(ubisom.getHeight() + 3);
			float sz_x, sz_y;

			textSize(15);
			println(BMU_max_cnt);
			for (int i = 1; i < sqrt(BMU_max_cnt); i++) {
				x = dataBMU.getUMatX((int) i);
				sz_x = i * dataBMU.maxSzX / sqrt(BMU_max_cnt);
				sz_y = i * dataBMU.maxSzY / sqrt(BMU_max_cnt);
				ellipse(x, dataBMU.getUMatY(ubisom.getHeight() + 2), 3 + min(2 * cx, sz_x), 3 + min(2 * cy, sz_y));
				text(">=" + i * i, x - 5, y2);
				if (sz_x > 2 * cx && sz_y > 2 * cy)
					break;
			}
			img_legenda_umat = get();
		} else
			image(img_legenda_umat, 0, 0);
	}

	public float distanceSOM(double a[], double b[]) {
		float d = 0;

		for (int i = 0; i < min(a.length, b.length); i++)
			if (i != catDim)
				d += square(a[i] - b[i]);

		d = sqrt((float) d);

		return d;
	}

	///// Estatisticas

	public void UbiSOM_Stat() {
		// Parameters.SOM_TRACK_STATISTICS = true;
		// estatisticas em memoria
		// Parameters.SOM_STATISTICS_TO_FILE = true;
		// gravar para ficheiro

	}

	////////////////////////////// FICHEIRO Server
	// baseado em https://processing.org/tutorials/network/ + generic_server2
	// 2A: Shared drawing canvas (Server)

	// TODO: passar para classe de comunica��o. Passar algumas variaveis para locais?

	// servidor e argumentos
	Server s, ssend;
	Client c;

	boolean send_server = false;

	/// TODO:
	int scaleDim[] = null; // vector com escala de dimensoes
	int deltaDim[] = null; // vector com delta de dimensoes
							// (vinput-deltaDim)/scaleDim ??

	String input = "";    /// Input do servidor
	String cmd = ""; // comando atual
	String txtArg = ""; // String argumento
	int ts = 0; // Timestamp para id de comandos
	int tframe = 0; // frame corrente

	int totDataPoints = 0;

	// Variaveis Globais graficas
	int ox = 0; // ultimo ponto
	int oy = 0;
	int x = 0; // ponto x,y atual
	int y = 0;
	int bg_color = 204; // cor do fundo
	int fg_color = 3; // cor do ponto
	int pnt_sz = 2; // tamanho de um plot
	int delta = 8; // divis\u00e3o esperada default em vplot, vbar, candle
	int gd = 0; // y do ultimo ponto no grafico
	int gd_x = 20; // origem no grafico
	int gd_y = 200; //
	int max_gd_x = 200;
	int max_gd_y = 200;

	final int iterations_to_pause_on_mark = 100; // numero de pausas apos um
													// mark (cada %5)
													// 0 desliga a pausa
	final int tot_iterations_to_sleep = 1200; // nm. de itera��es antes de
												// baixar o frameRate
	final int base_FrameRate = 40; // base frameRate
	final int sleep_FrameRate = 1; // slow frameRate

	public void setup_server() {

		s = new Server(this, 12345); // Start a simple server on a port
		ssend = new Server(this, 12356); // Start a simple server on a port

		frameRate(base_FrameRate); // � o socket (ou a quantidade de trabalho
									// que define o frameRate
	}

	////////////////////////////
	// when background_cmd_idx>=0 allows the execution of other command (in
	//////////////////////////// background, this is just a small demo)

	String background_cmd[] = {
			"transformx 0 1000 500 250\ntransformy 1000 0 500 750\ncatlabel 0 X\ncatlabel 1 Y\ncatlabel 2 Z\ncatlabel 3 W", // posicao
																															// 0
																															// so
																															// �
																															// executada
																															// na
																															// primeira
																															// vez
			"mpoint 0 0 0 0", "mpoint 1000 0 0 0", "mpoint 0 1000 0 0", "mpoint 0 0 1000 0", "mpoint 0 0 0 1000",
			"mpoint 1000 1000 1000 1000" };

	// int background_cmd_idx=0; //// LIGAR DEMO EM BKG

	int background_cmd_idx = -1; //// DESLIGAR DEMO EM BKG

	int iterations_to_sleep = 0;
	final int cmd_start_iterations = 300; // Numero de iteracoes sem comandos no
											// socket para iniciar demo.

	public boolean background_work_cmd() {

		if (iterations_to_sleep > cmd_start_iterations && background_cmd_idx == 0) {
			println("starting background cmds");
			input = background_cmd[background_cmd_idx++];
			return true;
		}

		if (iterations_to_sleep > 2 && background_cmd_idx >= 1) {
			input = background_cmd[background_cmd_idx++];
			if (background_cmd_idx >= background_cmd.length)
				background_cmd_idx = 1;
			return true;
		}

		return false;
	}
	
	public void draw_server()
	{
		get_server_input();
		int n_exec_cmd = draw_server_UpdateNByDataSource();
		draw_server_CheckIterationstoSleep(n_exec_cmd);
		tframe++;
		draw_server_CheckTimeToPause();
	}
	
	public int draw_server_UpdateNByDataSource()
	{
		int n_exec_cmd = 0;
		if(input=="" && dataSource.hasNext()) {  //  Se houver, executar ponto da datasource interna
			ts++;
			n_exec_cmd++;
			generateMPoint(dataSource.next());
		} else {
			ts++;

			while (parse_input()) {
				n_exec_cmd++;
				exec_cmd();
			}
		}
		return n_exec_cmd;
	}

	public void draw_server_CheckIterationstoSleep(int n_exec_cmd)
	{
		if (n_exec_cmd == 0 && !background_work_cmd()) {
			iterations_to_sleep++;
			if (iterations_to_sleep > tot_iterations_to_sleep && frameRate > sleep_FrameRate * 3) {
				println("no commands: Sleep mode is active (fps<=" + frameRate + ").");
				frameRate(sleep_FrameRate);
			}

		} else {
			if (iterations_to_sleep > tot_iterations_to_sleep) {
				println("Commands detected: Sleep mode is inactive");
				frameRate(base_FrameRate);
			}
			iterations_to_sleep = 0;
		}
	}

	public void draw_server_CheckTimeToPause()
	{
		if (timer_to_new_pause > 0) {
			timer_to_new_pause--;
			if (timer_to_new_pause % 5 == 0) {
				pause_network = true;
				show_msg = 10000;
				notification_msg = "Pause Network Information due to timer(" + timer_to_new_pause + ") label on id"
						+ id_label + " (press P to reactivate).";
			}
		}
	}

	public void get_server_input() {

		// Receive and parse data from client


		c = s.available();
		if (c != null) {
			input = c.readString();
			if (input.length() < 3) { // Nao ha comandos menores que 3 letras,
				// considerar que pode haver lixo...
				input = "";
			}
		} else {
			input = "";
		}


	}

	public boolean parse_input() {
		int args; // v. local: posicao dos argumentos e fim de linha
		int eoc;

		if (input.length() < 2) {
			return false;
		}

		args = input.indexOf(" ");
		eoc = input.indexOf("\n"); // Only up to the newline
		// println(input+":"+args+"|"+eoc);

		txtArg = "";

		if (eoc < 0) {
			input = input + "\n"; // strange ... try to recover
			println("detected no new line...");
			cmd = "";
			return true;

		} else {

			if (args > 0 && eoc > args) {
				cmd = input.substring(0, args);

				txtArg = trim(input.substring(args + 1, eoc));

			} else
				cmd = trim(input.substring(0, eoc));

			input = input.substring(eoc + 1, input.length());
		}

		print(ts + ":" + cmd); // +" buf:"+input.length());
		return true;

	}

	int timer_to_new_pause = 0;
	int id_label = 0;
	
	public void exec_cmd() {

		int data[] = { 0, 0, 0, 0 }; // Vector de dados argumento

		// Processamento de comandos

		// comandos sem argumentos [ox oy x y]
		if(proccessComands_HasNoArguments())
			return;

		/////////// Comandos com argumentos int

		data = proccessComands_SetDataVector();
		
		CommandProcessor command = commands.get(cmd);
		if(command != null)
			command.Proccess(data);
	}
	
	boolean proccessComands_HasNoArguments()
	{
		switch(cmd)
		{
			case "" : println("--Ignoring empty ln"); 
				break;
			case "text": show_msg = 10000;
				notification_msg = txtArg;
				break;
			case "catlabel" : 
				String[] varg = split(txtArg, ' ');
				int idx = PApplet.parseInt(varg[0]);
				axis.axis_s[idx] = varg[1];
				println("catlabel: " + idx + " is called " + axis.axis_s[idx] + ".");
				curr_axis_img = null;
				break;
			default : return false;
		}
		
		return true;
	}

	int[] proccessComands_SetDataVector()
	{
		if (txtArg.length() == 0) {
			int[] empty = {}; // TODO isto deve poder ser feito de outra forma
			println(" -- No args.");
			return empty;
		}
		
		int[] data = PApplet.parseInt(split(txtArg, ' ')); // Split values into an
															// array
		print(" (" + data[0]);
		for (int i = 1; i < data.length; i++)
			print("," + data[i]);
		println(")");
		return data;
	}
	/**
	 * @param data
	 */
	private void generateMPoint(int[] data) {
		
		
		
		if (currDimSz != data.length) { // TODO: confirmar labels no IRIS
			// este if deixa o programa funcionar quando o numero de dimensoes muda
			// dinamicamente: os restantes valores s�o zero...
			
			// TODO: 20 est� hardcoded no
			// inicio...
			println("setting dimSz from " + currDimSz + " to new mpoint datasize:" + (data.length ));
			currDimSz = data.length ;  /// TODO: estava data.length- 1;  ???
		}

		exPoint[curr_ex] = new MultiPoint(data);

		dataBMU.SOMLearn(exPoint[curr_ex]);

		if (dataBMU.currBMU > 0) {
			int idx = dataBMU.currBMU - 1;
			
			if(send_server) {
				ssend.write("point(" + totDataPoints + "): " + exPoint[curr_ex].toString_p() + "\n");
				ssend.write("BMU: " + dataBMU.bmu_x[idx] + " " + dataBMU.bmu_y[idx] + " " + dataBMU.dist[idx] + "\n");
			}
			
			dataSource.setAnnotation(new Point2D(dataBMU.bmu_x[idx], dataBMU.bmu_y[idx], SOMIteration), dataBMU.dist[idx]);
			/// dataSource.setAnnotation(exPoint[curr_ex]);   --TODO retornar o vector prototipo como 
			   // vector de inteiros??  Melhor separar o multipoint e deixar parte grafica aqui...
						

		}
		curr_ex = curr_ex + 1;

		if (curr_ex >= exPoint.length)
			curr_ex = 0;
		totDataPoints++;
	}



	////////////////////////////////////////////////////

    MultiSOM my_screen;   // Variavel para a classe multidim: responsavel pelo ecran / acesso � PApplet do processing
    
	public void settings() {
		my_screen=this;	
		fullScreen(2);
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "multiSOM.MultiSOM" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
			
		} else {
			PApplet.main(appletArgs);
		}
	}

}
