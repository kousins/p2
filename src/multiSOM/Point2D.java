package multiSOM;

public class Point2D {
	public Integer x0;
	public Integer y0;
	public Integer iteration;
	public Float f_x0;
	public Float f_y0;

	public Point2D() {
	}

	public Point2D(int x, int y, int it) {
		x0=x;
		y0=y;
		iteration=it;
	}

	public Point2D(int x, int y) {
		x0=x;
		y0=y;
	}

	public Point2D(float x, float y) {
		f_x0=x;
		f_y0=y;
	}

	public String toString() {
		if (x0 == null) {
			return "("+ f_x0 + ", "+f_y0 +")";
		}
		return "("+ x0 + ", "+y0 +")";
	}

	public int getIteration(){
		return iteration;
	}

	public int getX(){
		return x0;
	}

	public int getY(){
		return y0;
	}

	public float getfX(){
		return f_x0;
	}

	public float getfY(){
		return f_y0;
	}

	public void setX(int x){
		this.x0 = x;
	}

	public void setY(int y){
		this.y0 = y;
	}

	public void setfX(float x){
		this.f_x0 = x;
	}

	public void setfY(float y){
		this.f_y0 = y;
	}
}