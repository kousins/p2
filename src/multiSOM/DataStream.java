package multiSOM;

import java.io.FileNotFoundException;
import java.io.IOException;

public class DataStream implements IDataStream {

private int iterator=0;
	
    final private int nIts= 10000;  // teste deste dataset
	// final private int nIts= 600;  // teste rapido, ainda sem convergencia
	// final private int nIts= 0;  // modo de socket (ignora este codigo)
	private int nCMD=1;
	// 	final private int nCMD=0;  // desliga comandos iniciais

	MultiSOM multiSOM;
	String backgroundCmd[];
	int dataSet[][];
	
	@Override
	public void setup(MultiSOM scr) {
	    IrisDemo iris=new IrisDemo();
			multiSOM=scr;
			multiSOM.frameRate(20);  /// 20 FPS
			backgroundCmd = iris.getCMD();
			dataSet=iris.getData();
			
	}

	public int[] next() {
		if(nCMD>0) {
			if(nCMD < backgroundCmd.length ) {
				multiSOM.input= backgroundCmd[nCMD-1];
				multiSOM.parse_input();
				multiSOM.exec_cmd();  // TODO: isto assim nao fica muito bem...
				nCMD++;
			} else {
				nCMD=0;
			}
		}

		if(iterator == 100 ) nCMD=1;  /// Repetir comandos background pelas 100 itera��es.

		if(iterator == 800 ) multiSOM.frameRate(10);  /// 10 FPS ap�s 2000 itera��es
		if(iterator == 2000 ) multiSOM.frameRate(4);  /// 4 FPS ap�s 2000 itera��es
		
		return dataSet[(iterator++) % dataSet.length];  //enviar o DS at� 3000 pontos
		// AULAQS: ligar amostragem aleatoria:
		// iterator++;   
		// return dataSet[ (int)multiSOM.random(dataSet.length)];  //enviar o DS at� 3000 pontos
	}

	@Override
	public boolean hasNext() {
		return iterator<nIts;   //iterar nIts pontos
	}

	@Override
	public void setAnnotation(Point2D bmu, double distance) {
		if(nIts>0 && iterator>nIts-10)  // imprimir os ultimos 10 pontos
			System.out.println("BMUOUT: "+ bmu+"dist="+distance);
	}

	@Override
	public void setAnnotation(int[] prototype) {
		// TODO Auto-generated method stub

	}
	
	public void noteKeyTyped(char key) {
		if(key=='T') {
			String s="Transform x:";
			for (int i = 0; i < multiSOM.currDimSz; i++) {
				s = s + " " + multiSOM.transform[0][i]*10000;
			}
			MultiSOM.println(s);
			
			s="Transform y:";
			for (int i = 0; i < multiSOM.currDimSz; i++) {
				s = s + " " +  multiSOM.transform[1][i]*10000;
			}
			MultiSOM.println(s);		}
	}

	@Override
	public void generateHTMLReports(int it) throws FileNotFoundException,
			IOException {
		// TODO Auto-generated method stub
	}

}
