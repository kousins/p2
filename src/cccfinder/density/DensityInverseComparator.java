package cccfinder.density;

import java.util.Comparator;

public class DensityInverseComparator implements Comparator<MFilePlusTokens> {
	@Override
	public int compare(MFilePlusTokens col1, MFilePlusTokens col2) {
		if(col1.density() < col2.density())
			return 1;
		else if(col1.density() > col2.density())
			return -1;
		else return 0;
	}
}
