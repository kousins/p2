package cccfinder.density;

import cccfinder.tool.IFilter;
import cccfinder.tool.MFile;

public class MinimumLoCFilter implements IFilter {
	public boolean itPasses(MFile mFile) {
		return mFile.nrLines() > 5;
	}
	
	public boolean itPassesTwo(MFile mFile){
		return mFile.nrLines() > 5 && mFile.nrLines() < 10000;
	}
}
