package cccfinder.density;

import java.util.TreeSet;
import cccfinder.tool.MFile;
import cccfinder.tool.TokenWithStatistics;

public class MFilePlusTokens {
	private String _concern;
	private TreeSet<TokenWithStatistics> _tokens = new TreeSet<TokenWithStatistics>();
	private MFile _mFile;

	public MFilePlusTokens(String concern) {
		_concern = concern;
	}
	public String concern() {
		return _concern;
	}
	public TreeSet<TokenWithStatistics> tokens() {
		return _tokens;
	}
	public MFile mFile() {
		return _mFile;
	}
	public void update(TreeSet<TokenWithStatistics> set, MFile mFile) {
		_tokens = set;
		_mFile = mFile;
	}

	public boolean hasData() {
		return _mFile != null;
	}
	public int nrLines() {
		if(_mFile == null)
			return 0;
		return _mFile.nrLines();
	}
	public int size() {
		return _tokens.size();
	}
	public int totals() {
		int result = 0;
		for(TokenWithStatistics token : _tokens)
			result += token.getCount();
		return result;
	}
	public double density() {
		if(_mFile == null)
			return 0;
		return ((double)totals()) / nrLines();
	}

	public String toString() {
		if(_mFile == null)
			return "m-file is null";
		StringBuffer result = new StringBuffer("[" + _concern + "]");
		result.append(": " + _mFile.getName() + "(" + _mFile.nrLines() + " LoC): " +
				size() + " tokens; density " + density() +  
				"\n" + _mFile.getPathname() + "\n"); 
		result.append(totals() + " total occurrences - ");
		for(TokenWithStatistics tok : _tokens)
			result.append(" " + tok.getValue() + "(" + tok.getCount() + ")");
		return result.toString();
	}
}
