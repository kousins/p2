package cccfinder.density;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import cccfinder.tool.MFile;
import cccfinder.tool.TokenWithStatistics;

public class DenseMFilesShortlist implements Iterable<MFilePlusTokens> {
	private String _concern;
	private ArrayList<MFilePlusTokens> _list;
	private int _maxElems;
	private Comparator<MFilePlusTokens> _comparator;

	/*
	 * This is a "worker object": an object used for optimization purposes,
	 * to avoid the overhead of creating a large number of objects
	 */
	private MFilePlusTokens _worker;


	public DenseMFilesShortlist(String concern, int maximum) {
		_concern = concern;
		_maxElems = maximum;
		_list = new ArrayList<MFilePlusTokens>();
		_worker = new MFilePlusTokens(_concern);
		_comparator = new DensityComparator();
	}

	public int size() {
		return _list.size();
	}

	private int howManyLarger() {
		int result = 0;
		for(MFilePlusTokens tc : _list)
			if(_comparator.compare(_worker, tc) < 0)
				result++;
		return result;
	}
	private MFilePlusTokens smallest() {
		if(_list.size() == 0)
			return null;
		MFilePlusTokens smallest = _list.get(0);
		for(MFilePlusTokens tc : _list)
			if(_comparator.compare(tc, smallest) < 0)
				smallest = tc;
		return smallest;
	}
	private TreeSet<TokenWithStatistics> cloneTokenCollector(MFilePlusTokens collector) {
		TreeSet<TokenWithStatistics> tokens = new TreeSet<TokenWithStatistics>();
		for(TokenWithStatistics tok : collector.tokens())
			tokens.add(new TokenWithStatistics(tok));
		return tokens;
	}
	private MFilePlusTokens cloneWorker() {
		MFilePlusTokens clone = new MFilePlusTokens(new String(_worker.concern()));
		clone.update(cloneTokenCollector(_worker), _worker.mFile());
		return clone;
	}

	/*
	 * Receives a new pair <list of tokens, m-file> and adds it to the internal
	 * list, if the density 
	 */
	public void update(TreeSet<TokenWithStatistics> tokset, MFile mFile) {
		//uses worker to avoid creating a new MFilePlusTokens object:
		_worker.update(tokset, mFile);

		if(howManyLarger() < _maxElems)
			_list.add(cloneWorker());
		if(_list.size() > _maxElems)
			_list.remove(smallest());
	}

	public void sort() {
//		_list.sort(new DensityInverseComparator());
	}

	@Override
	public Iterator<MFilePlusTokens> iterator() {
		return _list.iterator();
	}

	public String toString() {
		StringBuffer result = new StringBuffer("");
		sort();
		for(MFilePlusTokens tc : _list)
			result.append(tc.toString() + "\n\n");
		return result.toString();
	}
}
