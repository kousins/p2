package cccfinder.density;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.tool.IFilter;
import cccfinder.tool.MFile;

public class IdTagFilter implements IFilter {

	public boolean itPasses(Token tk) {
		// TODO Auto-generated method stub
		return tk.getValue().equals(LexicalElem.ID.toString());
	}

	@Override
	public boolean itPasses(MFile mFile) {
		// TODO Auto-generated method stub
		return false;
	}

}
