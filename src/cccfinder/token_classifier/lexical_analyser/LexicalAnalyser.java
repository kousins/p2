package cccfinder.token_classifier.lexical_analyser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Each instance of this class (lexer objects) comprise an (almost) 
 * fully-fledged lexical analyzer for Matlab code. To our knowledge,
 * only the ' and the @ operator are not handled well. In the case of '
 * it is because ' is also a delimiter for string literals.
 * 
 * Operation listIterator provides the complete sequence of tokens,
 * respecting order of occurrence and with repetitions.
 * A listIterator is used rather than an Iterator so that parser objects
 * can can go backward a forward in this sequence according to its needs.
 * 
 * All non-keyword identifiers are tagged ID.
 * In this system, parser objects use the sequence to, e.g., further classify
 * ID tokens as variables, function names (in declarations, in calls), etc.
 * 
 * @author Miguel P. Monteiro
 */
public class LexicalAnalyser {
	private static final String[] KEYWORDS = { "break", "case", "catch",
			"classdef", "continue", "else", "elseif", "end", "for", "function",
			"global", "if", "otherwise", "parfor", "persistent", "return",
			"spmd", "switch", "try", "while"};
	private static final Set<String> _keywords = new HashSet<String>();

	static {
		for (String tok : KEYWORDS)
			_keywords.add(tok);
	}

	public static LexicalElem str2Tok(String str) {
		LexicalElem result = LexicalElem.UNKNOWN;
		switch (str) {
		case "&":
			result = LexicalElem.AND;
			break;
		case "&&":
			result = LexicalElem.AND2;
			break;
		case "|":
			result = LexicalElem.OR;
			break;
		case "||":
			result = LexicalElem.OR2;
			break;
		case "+":
			result = LexicalElem.ADD;
			break;
		case "-":
			result = LexicalElem.SUB;
			break;
		case "*":
			result = LexicalElem.MUL;
			break;
		case "/":
			result = LexicalElem.DIV;
			break;
		case "\\":
			result = LexicalElem.VID;
			break;
		case "(":
			result = LexicalElem.LPAR;
			break;
		case ")":
			result = LexicalElem.RPAR;
			break;
		case ";":
			result = LexicalElem.SEMI;
			break;
		case ".":
			result = LexicalElem.DOT;
			break;
		case ",":
			result = LexicalElem.COMMA;
			break;
		case "{":
			result = LexicalElem.LBRCKT;
			break;
		case "}":
			result = LexicalElem.RBRCKT;
			break;
		case "[":
			result = LexicalElem.LSQBR;
			break;
		case "]":
			result = LexicalElem.RSQBR;
			break;
		case "^":
			result = LexicalElem.CARET;
			break;
		case "=":
			result = LexicalElem.EQ;
			break;
		case "==":
			result = LexicalElem.EQ2;
			break;
		case "~":
			result = LexicalElem.NEG;
			break;
		case "~=":
			result = LexicalElem.NEQ;
			break;
		case "<":
			result = LexicalElem.LT;
			break;
		case "<=":
			result = LexicalElem.LTE;
			break;
		case ">":
			result = LexicalElem.GT;
			break;
		case ">=":
			result = LexicalElem.GTE;
			break;
		case ":":
			result = LexicalElem.COLON;
			break;
		case "'":
			result = LexicalElem.APOSTROPHE;
			break;
		case "@":
			result = LexicalElem.AT;
			break;
		case "break":
			result = LexicalElem.BREAK;
			break;
		case "case":
			result = LexicalElem.CASE;
			break;
		case "catch":
			result = LexicalElem.CATCH;
			break;
		case "classdef":
			result = LexicalElem.CLASSDEF;
			break;
		case "continue":
			result = LexicalElem.CONTINUE;
			break;
		case "else":
			result = LexicalElem.ELSE;
			break;
		case "elseif":
			result = LexicalElem.ELSEIF;
			break;
		case "end":
			result = LexicalElem.END;
			break;
		case "for":
			result = LexicalElem.FOR;
			break;
		case "function":
			result = LexicalElem.FUNCTION;
			break;
		case "global":
			result = LexicalElem.GLOBAL;
			break;
		case "if":
			result = LexicalElem.IF;
			break;
		case "otherwise":
			result = LexicalElem.OTHERWISE;
			break;
		case "parfor":
			result = LexicalElem.PARFOR;
			break;
		case "persistent":
			result = LexicalElem.PERSISTENT;
			break;
		case "return":
			result = LexicalElem.RETURN;
			break;
		case "spmd":
			result = LexicalElem.SPMD;
			break;
		case "switch":
			result = LexicalElem.SWITCH;
			break;
		case "try":
			result = LexicalElem.TRY;
			break;
		case "while":
			result = LexicalElem.WHILE;
			break;
		}
		return result;
	}

	private File _mFile;
	private String _mFileName;
	private List<Token> _list;
	private LexicalElem _latestToken;
	private int _numberOfLines;

	public LexicalAnalyser(File file) {
		_mFile = file;
		_mFileName = file.getName();
		_list = new ArrayList<Token>();
		_latestToken = LexicalElem.UNKNOWN;
	}

	//For testing scenarios
	LexicalAnalyser() {
		_mFile = null;
		_mFileName = null;
		_list = new ArrayList<Token>();
		_latestToken = LexicalElem.UNKNOWN;
	}
	public String getMFile() {
		return _mFileName;
	}

	public int numberOfLines() {
		return _numberOfLines;
	}
	public int numberOfTokens() {
		return _list.size();
	}

	public static boolean isKeyword(String token) {
		return _keywords.contains(token);
	}

	static boolean isDigit(char ch) {
		return ch >= '0' && ch <= '9';
	}

	static boolean isSpace(char ch) {
		return ch == ' ' || ch == '\t';
	}

	static boolean isAlfanum(char ch) {
		return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
	}

	/*
	 * Returns true if character may be start of multiple-char operator
	 */
	static boolean isTwoCharPrefix(char ch) {
		return ch == '=' || ch == '<' || ch == '>' || ch == ':' || ch == '~'
				|| ch == '|' || ch == '&';
	}

	static boolean isSingleCharToken(char ch) {
		return ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '\\'
				|| ch == '(' || ch == ')' || ch == ';' || ch == '.'
				|| ch == ',' || ch == '{' || ch == '}' || ch == '['
				|| ch == ']' || ch == '^' || ch == '@';
	}

	private static boolean isCharId(char ch) {
		return isAlfanum(ch) || ch == '_' || isDigit(ch);
	}

	private void register(LexicalElem type, String sTok) {
		_latestToken = type;
		Token token = new Token(type, sTok);
		_list.add(token);
	}

	int analyseLine(String line) {
		boolean lineCounts = false;
		int ci = 0;
		while (ci < line.length()) {
			// comment
			if (line.charAt(ci) == '%') {
				ci = line.length(); // ignore rest of the line
				break;
			}

			// white space
			if (isSpace(line.charAt(ci)))
				ci++;
			// identifier
			else {
				//if anything other than spaces & comments is found,
				//the line is counted for the LoC metric
				lineCounts = true;
				if (isAlfanum(line.charAt(ci)) || line.charAt(ci) == '_') {
				lineCounts = true;
				int beg = ci++;
				while (ci < line.length() && isCharId(line.charAt(ci)))
					ci++;
				String id = line.substring(beg, ci);
				if (isKeyword(id))
					register(str2Tok(id), id);
				else
					register(LexicalElem.ID, id);
			} else
				// number
				if (isDigit(line.charAt(ci))) {
					lineCounts = true;
					int beg = ci++;
					while (ci < line.length() && isDigit(line.charAt(ci)))
						ci++;
					//Has floating point?
					if(ci < line.length() && line.charAt(ci) == '.') {
						if(ci + 1 < line.length() && isDigit(line.charAt(ci + 1))) {
							ci++;
							while (ci < line.length() && isDigit(line.charAt(ci)))
								ci++;
						}
					}
					//Uses E notation?
					if(ci < line.length() && (line.charAt(ci) == 'e' || line.charAt(ci) == 'E')) {
						if(ci + 1 < line.length() && (isDigit(line.charAt(ci + 1)) || line.charAt(ci + 1) == '-')) {
							ci++; //consume 'e'
							if(line.charAt(ci) == '-')
								ci++; //consume minus sign '-'
							while (ci < line.length() && isDigit(line.charAt(ci)))
								ci++;
						}
					}
					String sNum = line.substring(beg, ci);
					register(LexicalElem.NUM, sNum);
				} else
				// single-char operator
				if (isSingleCharToken(line.charAt(ci))) {
					register(str2Tok(line.charAt(ci) + ""), line.charAt(ci) + "");
					ci++;
				} else
				// single-char operator or possibly two-char operator
				if (isTwoCharPrefix(line.charAt(ci))) {
					String tok = line.charAt(ci) + "";
					if (ci < line.length() - 1) {
						switch (line.charAt(ci)) {
						case '&':
							if (line.charAt(ci + 1) == '&') {
								tok += '&';
								ci++;
							}
							break;
						case '|':
							if (line.charAt(ci + 1) == '|') {
								tok += '|';
								ci++;
							}
							break;
						case '<':
						case '>':
						case '=':
						case '~':
						case ':':
							if (line.charAt(ci + 1) == '=') {
								tok += '=';
								ci++;
							}
							break;
						}
					}
					ci++;
					register(str2Tok(tok), tok);
				} else
				// string literal
				if (line.charAt(ci) == '\'')
					if (_latestToken == LexicalElem.ID
							|| _latestToken == LexicalElem.RPAR
							|| _latestToken == LexicalElem.RSQBR) {
						register(LexicalElem.APOSTROPHE, "'");
						ci++;
					} else {
						int beg = ci++;
						while (ci < line.length() && line.charAt(ci) != '\'')
							ci++;
						LexicalElem tk = LexicalElem.UNKNOWN; // default:
																// hopefully
																// will be STR
						if (ci < line.length()) {
							if (line.charAt(ci) == '\'') {
								ci++;
								tk = LexicalElem.STR;
							}
						}
						String strLit = line.substring(beg, ci);
						register(tk, strLit);
					}
				// what else?
				else {
					String leftover = line.substring(ci);
					register(LexicalElem.UNKNOWN, leftover);
					break;
				}
			}
		}
		// does this line count as a valid line for the LoC metric?
		return lineCounts ? 1 : 0;
	}

	public void analyseMFile() throws IOException {
		BufferedReader txt = new BufferedReader(new FileReader(_mFile));
		String line;
		while ((line = txt.readLine()) != null) {
			_numberOfLines += analyseLine(line);
		}
		txt.close();
	}

	//mostly for testing
	boolean contains(String sTok) {
		for(Token tok : _list)
			if(tok.getValue().equals(sTok)) return true;
		return false;
	}
	//for testing
	boolean contains(LexicalElem type) {
		for(Token tok : _list)
			if(tok.type() == type) return true;
		return false;
	}
	//for testing
//	boolean contains(LexicalElem type, String value) {
//		for(Token tok : _list)
//			if(tok.type() == type && tok.getValue().equals(value))
//				return true;
//		return false;
//	}
	//definitely just for testing
	boolean containsNumber(String s) {
		for(Token tok : _list)
			if(tok.type() == LexicalElem.NUM)
				if(tok.getValue().equals(s)) return true;
		return false;
	}

	//definitely just for testing
	boolean contains(Tag tag, String s) {
		for(Token tok : _list)
			if(tok.type() == LexicalElem.ID)
				if(tok.getTag() == tag)
					if(tok.getValue().equals(s))
						return true;
		return false;
	}
	public ListIterator<Token> listIterator() {
		return _list.listIterator();
	}
	public List<Token> getTokens() {
		return _list;
	}
}
