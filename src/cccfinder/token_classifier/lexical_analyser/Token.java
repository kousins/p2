package cccfinder.token_classifier.lexical_analyser;

public class Token implements Comparable<Token> {
	private final String _string;
	private final LexicalElem _type;
	private Tag _tag;

	public Token(LexicalElem type, String tokenStr) {
		_type = type;
		_string = tokenStr;
		_tag = Tag.NWD;
	}

	//To be used by the CCCFinder component only
	public Token(String tokenStr) {
		this(LexicalElem.ID, tokenStr);
	}

	public boolean isKeyword() {
		return _type.ordinal() >= LexicalElem.BREAK.ordinal()
				&& _type.ordinal() <= LexicalElem.WHILE.ordinal();
	}
	public LexicalElem type() {
		return _type;
	}
	public void setTag(Tag tag) {
		_tag = tag;
	}
	public Tag getTag() {
		return _tag;
	}

	public String getValue() {
		return _string;
	}

	@Override
	public int compareTo(Token other) {
		return getValue().compareTo(other.getValue());
	}

	public Token clone() {
		Token clone = new Token(_type, new String(_string));
		clone.setTag(_tag);
		return clone;
	}

	public String toString() {
		if (_type == LexicalElem.ID || _type == LexicalElem.STR)
			return _type.toString() + "<" + _tag + "><" + _string + ">";
		return _type.toString();
	}

}
