package cccfinder.token_classifier.lexical_analyser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import util.ProgressDisplay;

public class Parser {
	protected File _mFile;
	protected int _nrLines;
	private Set<String> _vars;
	private Set<String> _funcs;
	private Set<String> _args;
	protected List<Token> _taggedTokens;

	public Parser(File file) {
		_mFile = file;
		_vars = new TreeSet<String>();
		_args = new TreeSet<String>();
		_funcs = new TreeSet<String>();
	}

	//strictly for testing purposes
	public Parser(String text) {
		_vars = new TreeSet<String>();
		_args = new TreeSet<String>();
		_funcs = new TreeSet<String>();
		LexicalAnalyser lexer = new LexicalAnalyser();
		String[] lines = text.split("\n");
		for(String line : lines)
			lexer.analyseLine(line);
		_nrLines = lexer.numberOfLines();
		parseTokens(lexer);
		tagWordTokens(lexer);
	}

	public File getMFile() {
		return _mFile;
	}
	static void printTokens(String mFileName, LexicalAnalyser lexer) {
		ListIterator<Token> it = lexer.listIterator();
		Token token;
		ProgressDisplay.print(mFileName + ": ");
		while (it.hasNext()) {
			token = it.next();
			if (token.type() != LexicalElem.COM) {
				ProgressDisplay.print(token + " ");
				if (token.type() == LexicalElem.SEMI)
					ProgressDisplay.println();
			}
		}
		ProgressDisplay.println("\n");
	}

	public static boolean isTokenOfInterest(Token token) {
		return token.isKeyword() || token.type() == LexicalElem.ID
				|| token.type() == LexicalElem.UNKNOWN;
	}

	public static boolean isKeyword(Token token) {
		boolean result = false;
		switch (token.type()) {
		case BREAK:
		case CASE:
		case CATCH:
		case CLASSDEF:
		case CONTINUE:
		case ELSE:
		case ELSEIF:
		case END:
		case FOR:
		case FUNCTION:
		case GLOBAL:
		case IF:
		case OTHERWISE:
		case PARFOR:
		case PERSISTENT:
		case RETURN:
		case SPMD:
		case SWITCH:
		case TRY:
		case WHILE:
			result = true;
			break;
		default:
			break;
		}
		return result;
	}

	/*
	 * The results returned by a m-function are either a non empty list of ids
	 * enclosed within square brackets or the result is a single id.
	 */
	public Token processFunctionHeader(Iterator<Token> it) {
		Token token = it.next(); // consume 'function'

		// if the next token is a ID... could also be a LSQBR('[')
		if (token.type() == LexicalElem.ID) {
			// is the id a lone result arg or the function name?
			Token tok2 = it.next(); // let's find out - consume ID
			if (tok2.type() == LexicalElem.EQ) {
				// Ok, it is a single result arg
				_args.add(token.getValue());
				token = it.next(); // consume '='
				if (token.type() == LexicalElem.ID)
					_funcs.add(token.getValue());
				token = it.next(); // consume function name
			} else { // it is the name of the function
				_funcs.add(token.getValue());
				token = tok2;
			}
		}
		// otherwise we should have a "return results" section
		else {
			if (token.type() == LexicalElem.LSQBR) {
				// process "return results" section
				token = it.next(); // consume '['
				while (token.type() != LexicalElem.RSQBR) {
					_args.add(token.getValue());
					token = it.next(); // consume result variable name
					if (token.type() == LexicalElem.COMMA)
						token = it.next(); // consume ','
				}
				token = it.next(); // consume ']'
				token = it.next(); // consume '='
				_funcs.add(token.getValue());
				token = it.next(); // consume function name
			}
		}
		// do we next have a parameters section?
		if (token.type() == LexicalElem.LPAR) {
			// process a parameters section
			token = it.next(); // consume '('
			while (token.type() != LexicalElem.RPAR) { // while token is not ')'
				if (token.type() == LexicalElem.ID)
					_args.add(token.getValue());
				token = it.next(); // consume parameter name
				if (token.type() == LexicalElem.COMMA)
					token = it.next(); // consume ','
			}
			if (it.hasNext())
				token = it.next(); // consume ')'
		}
		return token;
	}

	public Token processVarCreation(ListIterator<Token> it) {
		Token token = null;

		// is token before '=' a single id or a [..] ?
		it.previous(); // go back to '='
		token = it.previous(); // go to token before '='
		if (token.type() == LexicalElem.ID)
			_vars.add(token.getValue());
		else
		// is token ']'?
		if (token.type() == LexicalElem.RSQBR) {
			// process [..]
			token = it.previous(); // consume ']'
			// while token != '['
			while (it.hasPrevious() && token.type() != LexicalElem.LSQBR) {
				if (token.type() == LexicalElem.ID)
					_vars.add(token.getValue());
				token = it.previous(); // consume id
				if (token.type() == LexicalElem.COMMA)
					token = it.previous(); // consume ','
			}
		}
		// set iterator position just after '=' (i.e., section processed here)
		while (token.type() != LexicalElem.EQ)
			token = it.next();
		if (it.hasNext())
			token = it.next();
		return token;
	}

	public void tagWordTokens(LexicalAnalyser lexer) {
		List<Token> tokens = lexer.getTokens();
		_taggedTokens = new ArrayList<Token>();

		for(Token tok1 : tokens) {
			Token tok2 = tok1.clone();
			if(isTokenOfInterest(tok1)) {
				if(isKeyword(tok1))
					tok2.setTag(Tag.KEY);
				else if (tok1.type() == LexicalElem.ID)
					if(_args.contains(tok1.getValue()))
						tok2.setTag(Tag.ARG);
					else if(_vars.contains(tok1.getValue()))
						tok2.setTag(Tag.VAR);
					else if(_funcs.contains(tok1.getValue()))
						tok2.setTag(Tag.FHD);
					else tok2.setTag(Tag.FUN);
			}
			_taggedTokens.add(tok2);
		}
		if(tokens.size() != _taggedTokens.size())
			System.err.println("INTERNAL ERROR (tagAllTokens): tokens1 = " +
					tokens.size() + ", tokens2 = " + _taggedTokens.size());
	}

	public void parseTokens(LexicalAnalyser lexer) {
		ListIterator<Token> it = lexer.listIterator();
		Token token;
		while (it.hasNext()) {
			token = it.next();
			if (token.type() == LexicalElem.EQ)
				token = processVarCreation(it);
			else if (isTokenOfInterest(token))
				if (token.type() == LexicalElem.FUNCTION) {
					try {
						token = processFunctionHeader(it);
					} catch (NoSuchElementException e) {
						System.err.println("\nERROR on " + _mFile.getPath());
					}
				}
		}
	}

	public void processMFile() throws IOException {
		LexicalAnalyser lexer = new LexicalAnalyser(_mFile);

		//processes the source file completely
		lexer.analyseMFile();
		_nrLines = lexer.numberOfLines();

		//subsequent processing is based on list iterator of tagged tokens
		parseTokens(lexer);
		tagWordTokens(lexer);
		ProgressDisplay.updateProgress();
		//listTokens(lexer);
	}

	/*
	 * For debugging purposes
	 */
	void listTokens(LexicalAnalyser lexer) {
		System.out.println(lexer.getMFile() + ":");
		ListIterator<Token> it = lexer.listIterator();
		while(it.hasNext()) {
			Token token = it.next();
			if(token.type() == LexicalElem.ID)
				System.out.println(token + " ");
		}
		System.out.println();
	}
	public int numberOfLines() {
		return _nrLines;
	}

	public Set<String> getVars() {
		return _vars;
	}
//	public 
	Set<String> getArgs() {
		return _args;
	}
//	public 
	Set<String> getFuncs() {
		return _funcs;
	}
	public Iterator<String> iteratorARGs() {
		return _args.iterator();
	}
	public Iterator<String> iteratorFUNCs() {
		return _funcs.iterator();
	}
	public Iterator<Token> taggedTokensIterator() {
		return _taggedTokens.iterator();
	}

//	public 
	boolean hasFunction(String word) {
		for(Token tok : _taggedTokens)
			if(tok.type() == LexicalElem.ID)
				if(tok.getTag() != Tag.ARG && tok.getTag() != Tag.VAR && tok.getTag() != Tag.KEY)
					if(tok.getValue().equals(word))
						return true;
		return false;
	}
//	public 
	boolean hasWordWithThisTag(Tag tag, String word) {
		for(Token tok : _taggedTokens)
			if(tok.type() == LexicalElem.ID)
				if(tok.getTag() == tag)
					if(tok.getValue().equals(word))
						return true;
		return false;
	}
}

/*
	static void reportData(int turn, File mFile, LexicalAnalyser lexer,
			Set<String> vars, Set<String> funcs, Set<String> args) {
		if(turn != 2) return;
		if(!mFile.getName().equals("cos1_custom.m") && !mFile.getName().equals("sin1_custom.m"))
			return;
		System.out.print("TOKENs: ");
		ListIterator<Token> it = lexer.listIterator();
		while(it.hasNext()) System.out.print(it.next() + " ");
		System.out.println();

		System.out.print("VARs: ");
		for(String var : vars) System.out.print(var + " ");
		System.out.println();

		System.out.print("FUNCs: ");
		for(String func : funcs) System.out.print(func + " ");
		System.out.println();

		System.out.print("ARGs: ");
		for(String arg : args) System.out.print(arg + " ");
		System.out.println();

		System.out.println();
	}
*/
