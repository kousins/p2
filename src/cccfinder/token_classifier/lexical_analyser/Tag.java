package cccfinder.token_classifier.lexical_analyser;

public enum Tag {
	NWD("NWD"), //Non word
	KEY("KEY"), ARG("ARG"), VAR("VAR"), FHD("FHD"), FUN("FUN");

	private final String asString;

	Tag(String str) {
		this.asString = str;
	}

	public String toString() {
		return asString;
	}
}
