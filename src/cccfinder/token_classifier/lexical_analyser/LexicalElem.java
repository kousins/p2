package cccfinder.token_classifier.lexical_analyser;

public enum LexicalElem {
	//non-keyword, word/string tokens
	COM("COM"), ID("ID"), STR("STR"), NUM("NUM"),

	//keywords
	BREAK("break"), CASE("case"), CATCH("catch"), CLASSDEF("classdef"),
	CONTINUE("continue"), ELSE("else"), ELSEIF("elseif"), END("end"),
	FOR("for"), FUNCTION("function"), GLOBAL("global"), IF("if"),
	OTHERWISE("otherwise"), PARFOR("parfor"), PERSISTENT("persistent"),
	RETURN("return"), SPMD("spmd"), SWITCH("switch"), TRY("try"), WHILE("while"), NARGIN("nargin"),

	//symbol tokens - logical operators
	AND("&"), AND2("&&"), OR("|"), OR2("||"), NEG("~"),
	EQ("="), EQ2("=="), NEQ("~="), LT("<"), LTE("<="), GT(">"), GTE(">="),

	//symbol tokens - arithmetic operators
	ADD("+"), SUB("-"), MUL("*"), DIV("/"),
	APOSTROPHE("'"), 

	//symbol tokens - parenthesis, brackets, etc
	LPAR("("), RPAR(")"), LBRCKT("{"), RBRCKT("}"), LSQBR("["), RSQBR("]"),

	//symbol tokens - assignment & others
	ASSGN(":="), SEMI(";"), COMMA(","), DOT("."), COLON(":"), CARET("^"), VID("\\"), AT("@"),

	//what's left?
	UNKNOWN("UNKNOWN");

	private final String asString;

	LexicalElem(String str) {
		this.asString = str;
	}

	public String toString() {
		return asString;
	}
}
