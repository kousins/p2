package cccfinder.token_classifier.blocks;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Token;

public class Token4Output extends Token {
	public Token4Output(LexicalElem lexElem, String token) {
		super(lexElem, token);
	}
	public String toString() {
		return getTag() + ":" + getValue();
	}
	public Token4Output clone() {
		Token4Output clone = new Token4Output(this.type(), new String(this.getValue()));
		clone.setTag(this.getTag());
		return clone;
	}
}
