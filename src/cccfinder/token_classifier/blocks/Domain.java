package cccfinder.token_classifier.blocks;

import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;

/**
 * @author Miguel P. Monteiro
 */
public class Domain {
	private String _domain;

	/*
	 * Entire block data from all toolboxes from a domain
	 */
	private List<BlockSeq> _blockSeqs;

	public Domain(String domain, List<BlockSeq> blockSeqs) {
		_domain = domain;
		_blockSeqs = blockSeqs;
	}

	public String getName() {
		return _domain;
	}

	public int nrBlockSeqs() {
		return _blockSeqs.size();
	}

	public int nrBlocks() {
		int result = 0;
		for (BlockSeq bs : _blockSeqs)
			result += bs.nrBlocks();
		return result;
	}

	public int size() {
		int result = 0;
		for (BlockSeq blkseq : _blockSeqs)
			result += blkseq.size();
		return result;
	}

	public void print(PrintStream txt) {
		for (BlockSeq bseq : _blockSeqs)
			bseq.print(txt);
	}

	/*
	 * NOTE: toString should be used for testing purposes only
	 * (otherwise performance will be dreadful in real cases!)
	 */
	@Override
	public String toString() {
		String result = "";
		for (BlockSeq bseq : _blockSeqs)
			result += bseq.toString();
		return result;
	}

	public Domain clone() {
		List<BlockSeq> copyList = new ArrayList<BlockSeq>();
		for(BlockSeq bs : _blockSeqs)
			copyList.add(bs.clone());
		return new Domain(new String(_domain), copyList);
	}
}