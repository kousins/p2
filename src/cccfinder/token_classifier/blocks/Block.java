package cccfinder.token_classifier.blocks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.PrintStream;
//import token_classifier.lexical_analyser.Token;

/**
 * @author Miguel P. Monteiro
 */
public class Block implements Iterable<Token4Output> {
	private String _mFile;
	private List<Token4Output> _taggedTokens;

	Block(String mFile, List<Token4Output> tokens) {
		this._mFile = mFile;
		this._taggedTokens = tokens;
	}

	public String getMFile() {
		return _mFile;
	}
	public int size() {
		return _taggedTokens.size();
	}
	public Iterator<Token4Output> iterator() {
		return _taggedTokens.iterator();
	}
	void print(PrintStream txt) {
		txt.print(getMFile());
		for (Token4Output token: _taggedTokens)
			txt.print(" " + token);
	}

	/**
	 * returns a deep copy
	 */
	public Block clone() {
		List<Token4Output> pairClones = new ArrayList<Token4Output>();
		for(Token4Output tok : _taggedTokens)
			pairClones.add(tok.clone());
		return new Block(_mFile, pairClones);
	}

	/*
	 * NOTE: toString should be used for testing purposes only
	 * (otherwise performance will be dreadful with big systems!)
	 */
	public String toString() {
		String result = _mFile;
		for (Token4Output token : _taggedTokens)
			result += " " + token;
		return result;
	}
}