package cccfinder.token_classifier.blocks;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cccfinder.token_classifier.lexical_analyser.Tag;


/**
 * Each OldDomain object holds block data from an entire domain (i.e., multiple
 * toolboxes). A BlockManager object holds block data from an entire repository
 * of (possibly many) toolboxes, covering multiple domains.
 * 
 * @author Miguel P. Monteiro
 */
public class BlockManager implements /*IBlockManager,*/ Iterable<Domain> {
	private String _currentDomain;
	private BlockSeq _currentBlockSeq;
	private List<BlockSeq> _currentBlockList;
	private List<Domain> _domains;

	public BlockManager() {
		this("", "");
	}

	public BlockManager(String domain, String toolbox) {
		_currentDomain = domain;
		_currentBlockSeq = new BlockSeq(domain, toolbox);
		_currentBlockList = new ArrayList<BlockSeq>();
		_domains = new ArrayList<Domain>();
	}

	/**
	 * Deep copy constructor
	 */
	public BlockManager(BlockManager original) {
		_currentDomain = new String(original.currentDomain());
		_currentBlockSeq = original._currentBlockSeq.clone();
		_currentBlockList = new ArrayList<BlockSeq>();
		for(BlockSeq bs : original._currentBlockList)
			_currentBlockList.add(bs);
		_domains = new ArrayList<Domain>();
		for(Domain d : original._domains)
			_domains.add(d.clone());
	}

	public void mark(String tok, Tag tag) {
		_currentBlockSeq.mark(tok, tag);
	}

	public void setToolbox(String domain, String toolbox) {
		newDomain(domain);
		newToolbox(toolbox);
	}

	public void newToolbox(String toolbox) {
		if(_currentBlockSeq.toolbox().equals(toolbox))
			return;
		if (_currentBlockSeq.size() > 0)
			_currentBlockList.add(_currentBlockSeq);
		_currentBlockSeq = new BlockSeq(currentDomain(), toolbox);
	}

	public String currentToolbox() {
		return _currentBlockSeq.toolbox();
	}

	public void newDomain(String domain) {
		if (!_currentDomain.equals(domain)) {
			if (_currentBlockSeq.size() > 0) {
				_currentBlockList.add(_currentBlockSeq);
				_currentBlockSeq = new BlockSeq(currentDomain(), currentToolbox());
			}
			if (_currentBlockList.size() > 0) {
				Domain lastDom = new Domain(currentDomain(), _currentBlockList);
				_domains.add(lastDom);
				_currentBlockList = new ArrayList<BlockSeq>();
			}
			_currentDomain = domain;
		}
	}

	public int nrDomains() {
		int result = 0;
		if(_currentBlockSeq.size() > 0 || _currentBlockList.size() > 0)
			result++;
		result += _domains.size();
		return result;
	}

	public String currentDomain() {
		return _currentDomain;
	}

	public void setMFile(String mFile) {
		_currentBlockSeq.setMFile(mFile);
	}

	public void newMFile(String mFile) {
		_currentBlockSeq.newMFile(mFile);
	}

	public int size() {
		int result = _currentBlockSeq == null ? 0 : _currentBlockSeq.size();
		for (BlockSeq bs : _currentBlockList)
			result += bs.size();
		for (Domain dom : _domains)
			result += dom.size();
		return result;
	}

	public int nrBlocks() {
		int result = _currentBlockSeq == null ? 0 : _currentBlockSeq.nrBlocks();
		for (BlockSeq bs : _currentBlockList)
			result += bs.nrBlocks();
		for (Domain dom : _domains)
			result += dom.nrBlocks();
		return result;
	}

	private void newCurrent() {
		if (_currentBlockSeq.size() > 0) {
			_currentBlockList.add(_currentBlockSeq);
			_currentBlockSeq = new BlockSeq(currentDomain(), currentToolbox());
		}
		if (_currentBlockList.size() > 0) {
			Domain lastDom = new Domain(currentDomain(), _currentBlockList);
			_domains.add(lastDom);
			_currentBlockList = new ArrayList<BlockSeq>();
		}
	}

	public void print(PrintStream txt) {
		newCurrent();
		for (Domain dom : _domains)
			dom.print(txt);
	}

	/**
	 * returns a deep copy of this object
	 */
	public BlockManager clone() {
		return new BlockManager(this);
	}

	/*
	 * NOTE: toString should be used for testing purposes only (otherwise
	 * performance will be dreadful in real cases!)
	 */
	private String asString() {
		newCurrent();
		String result = "";
		for (Domain dom : _domains)
			result += dom.toString();
		return result;
	}

	public String toString() {
		BlockManager clone = this.clone();
		return clone.asString(); 
	}

	@Override
	public Iterator<Domain> iterator() {
		newCurrent();
		return _domains.iterator();
	}
}
