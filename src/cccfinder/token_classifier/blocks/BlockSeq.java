package cccfinder.token_classifier.blocks;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Tag;


/**
 * Each BlockSeq object holds block data from an entire toolbox.
 * 
 * @author Miguel P. Monteiro
 */
public class BlockSeq implements Iterable<Block> {
	static final String UNKNOWN_MFILE = "UNKNOWN.m";

	private List<Token4Output> _currentBlock;
	private String _currentMFile;
	private List<Block> _blocks;
	private String _domain;
	private String _toolbox;

	public BlockSeq(String domain, String toolbox) {
		_domain = domain;
		_toolbox = toolbox;
		_currentMFile = UNKNOWN_MFILE;
		_currentBlock = new ArrayList<Token4Output>();
		_blocks = new ArrayList<Block>();
	}

	public void setMFile(String mFile) {
		_currentMFile = mFile;
	}

	void moveCurrentBlock() {
		if (_currentBlock.size() > 0) {
			Block block = new Block(_currentMFile, _currentBlock);
			_blocks.add(block);
			_currentBlock = new ArrayList<Token4Output>();
		}
	}

	public void newMFile(String newMFile) {
		if (this.currentMFile().equals(newMFile))
			return;
		moveCurrentBlock();
		_currentMFile = newMFile;
	}

	public static Token4Output makeToken(String tok, Tag tag) {
		Token4Output token = new Token4Output(LexicalElem.ID, tok);
		token.setTag(tag);
		return token;
	}

	public void mark(String tok, Tag tag) {
		_currentBlock.add(makeToken(tok, tag));
		if (tag == Tag.KEY && tok.equals(LexicalElem.END.toString()))
			// special case: 'end' is used to partition blocks
			moveCurrentBlock();
	}

	public void setToolbox(String domain, String toolbox) {
		_domain = domain;
		_toolbox = toolbox;
	}
	public String domain() {
		return _domain;
	}

	public String toolbox() {
		return _toolbox;
	}

	public String currentMFile() {
		return _currentMFile;
	}

	public int size() {
		int result = _currentBlock == null ? 0 : _currentBlock.size();
		for (Block block : _blocks)
			result += block.size();
		return result;
	}

	public int nrBlocks() {
		int result = 0;
		if (_currentBlock != null)
			if (_currentBlock.size() > 0)
				result++;
		result += _blocks.size();
		return result;
	}

	public Iterator<Block> iterator() {
		return _blocks.iterator();
	}

	void print(PrintStream txt) {
		moveCurrentBlock();
		for (Block blk : _blocks) {
			txt.print(domain() + " " + toolbox() + " ");
			blk.print(txt);
			txt.println();
		}
	}

	private static List<Block> blocksCopy(List<Block> list) {
		if(list == null) return null;
		List<Block> copyList = new ArrayList<Block>();
		for(Block b : list)
			copyList.add(b);
		return copyList;
	}

	/**
	 * returns a deep copy of this object
	 */
	public BlockSeq clone() {
		BlockSeq clone = new BlockSeq(new String(_domain), new String(_toolbox));
		clone.setMFile(_currentMFile);
		clone._blocks = blocksCopy(_blocks);
		if(_currentBlock != null) {
			List<Token4Output> pairClones = new ArrayList<Token4Output>();
			for(Token4Output tok : _currentBlock)
				pairClones.add(tok.clone());
			clone._currentBlock = pairClones;
		}
		return clone;
	}

	private String asString() {
		String result = "";
		moveCurrentBlock();
		for (Block blk : _blocks)
			result += domain() + " " + toolbox() + " " + blk.toString() + "\n";
		return result;
	}

	/*
	 * NOTE: toString should be used for testing purposes only (otherwise
	 * performance will be dreadful in real cases!)
	 */
	public String toString() {
		BlockSeq clone = this.clone();
		return clone.asString();
	}
}
