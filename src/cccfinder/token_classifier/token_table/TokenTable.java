package cccfinder.token_classifier.token_table;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
//import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class TokenTable {
	private TreeMap<String, List<String>> _table;
	private List<String> _allTokens;

	public TokenTable(String filename) throws IOException {
		_table = new TreeMap<String, List<String>>();
		_allTokens = new ArrayList<String>();
		load(filename);
	}
	public void load(String filename) throws IOException {
		BufferedReader txt = new BufferedReader(new FileReader(filename));
		String line;
		line = txt.readLine();
		while (line != null) {
			//this line should be a concern name
			List<String> tokens = new ArrayList<String>();
			_table.put(line, tokens);

			//read sequence of tokens (in a single line)
			line = txt.readLine();
			if(line == null)
				break;
			String[] toks = line.split(", ");
			for(String tok : toks) {
				tokens.add(tok);
				_allTokens.add(tok);
			}

			//read next concern name
			line = txt.readLine();
		}
		txt.close();
	}
	public boolean hasConcern(String concern) {
		return _table.containsKey(concern);
	}
	public boolean hasToken(String token) {
		return _allTokens.contains(token);
	}

	public boolean concernHasToken(String concern, String token) {
		if(hasConcern(concern)) {
			List<String> tokens = _table.get(concern);
			return tokens.contains(token);
		}
		return false;
	}

	public String toString() {
		StringBuffer result = new StringBuffer("");
		for(String concern : _table.keySet()) {
			List<String> tokens = _table.get(concern);
			result.append(concern + "\n");
			for(String token : tokens)
				result.append(token + " ");
			result.append("\n\n");
		}
		return result.toString();
	}
}
