package cccfinder.pairs;

import java.io.File;
import java.io.IOException;

import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Parser;

import util.ProgressDisplay;

public class ParserTokens extends Parser {

	public ParserTokens(File file)
			throws IOException {
		super(file);
	}

	public LexicalAnalyser processMFile2() throws IOException {
		LexicalAnalyser lexer = new LexicalAnalyser(_mFile);

		//processes the source file completely
		lexer.analyseMFile();
		_nrLines = lexer.numberOfLines();

		//subsequent processing is based on list iterator of tagged tokens
		parseTokens(lexer);
		tagWordTokens(lexer);
		ProgressDisplay.updateProgress();
		return lexer;
	}
}
