package cccfinder.tool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import util.ProgressDisplay;

/**
 * This class represents a system comprising at least one folder plus all
 * m-files contained in that folder and sub-folders.
 * 
 * A Matlab system is an arbitrarily large and complex hierarchy folders
 * containing any combination of m-files and "toolbox folders". Initially, this
 * class was meant to represent a single MATLAB toolbox, e.g., a simple folder
 * containing *.m files. This class was later extended to represent all data
 * from a set of folders relating to a entire system of toolboxes.
 * 
 * Each Matlab system is associated to a specific domain of application.
 * 
 * @author Simona Posea
 * @author Miguel P. Monteiro
 */
public class Toolbox {
	private static final String DEFAULT_DOMAIN = "DEFAULT";
	private static final String DOMAIN_FILENAME = "domain.txt";

	private List<MFile> _MFiles;
	private File _file;
	private List<TokenWithStatistics> _collectedTokens;
	private String _domain;

	public Toolbox(File fileHandle, String path) {
		_file = fileHandle;
		_MFiles = new ArrayList<MFile>();
		_collectedTokens = new ArrayList<TokenWithStatistics>();
		_domain = DEFAULT_DOMAIN;
		collectMFiles(_file, clipLastNameFromPath(path));
	}

	public static String clipLastNameFromPath(String s) {
		return s.substring(0, s.lastIndexOf('\\'));
	}

	public String getDomain() {
		return _domain;
	}

	public boolean isMFile(File file) {
		return file.isFile() && file.getName().endsWith(".m");
	}

	public boolean isDirectory() {
		return _file.isDirectory();
	}

	public String getName() {
		return _file.getName();
	}

	public File[] getListOfFiles() {
		return _file.listFiles();
	}

	public Integer getNrMFiles() {
		return _MFiles.size();
	}

	public MFile getMFile(Integer index) {
		return _MFiles.get(index);
	}

	public Iterator<MFile> getMFilesIterator() {
		return _MFiles.iterator();
	}

	private File getDomainTextFile(File[] files) {
		for (File f : files)
			if (f.getName().equals(DOMAIN_FILENAME))
				return f;
		return null;
	}

	/*
	 * This method assumes as pre-condition that argument fin (the current File)
	 * is a directory. It looks among the files enclosed within that directory
	 * for a text file named DOMAIN_FILENAME and reads its first line as the
	 * application's domain (or perhaps the toolbox's) assigned to _domain
	 * field. If no file named DOMAIN_FILENAME is found, _domain keeps its
	 * previous value. This way, the domain tag propagates through the folder
	 * hierarchy.
	 */
	void updateDomain(File fin) {
		File[] files = fin.listFiles();
		File domainFile = getDomainTextFile(files);
		if (domainFile != null) // else, keep _domain as it is.
			try {
				Scanner sc = new Scanner(domainFile);
				_domain = sc.nextLine();
				// Global.blockManager.newToolbox(_domain, getName());
				sc.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error when trying to load domain at "
						+ domainFile.getAbsolutePath());
			}
	}

	/**
	 * Loads relevant data from all m-files enclosed within the folder pointed
	 * by the File parameter. Data from each m-file is stored on MFile objects
	 * which are inserted to a MFileCollection object. Relevant data comprises
	 * the function names appearing in non-blank, non-comment lines. This method
	 * is recursive. It starts with the root folder. All sub-folders and
	 * enclosed m-files are subsequently processed through recursive calls.
	 */
	public void collectMFiles(File file, String path) {
		path += "\\" + file.getName();
		if (file.isDirectory()) {
			updateDomain(file);
			File[] children = file.listFiles();
			for (File child : children)
				collectMFiles(child, path);
		} else if (isMFile(file)) {
			MFile mfile = new MFile(file, getDomain());
			_MFiles.add(mfile);
		}
	}

	public Integer getTotalLines() {
		Integer tlines = 0;
		for (MFile mf : _MFiles)
			tlines += mf.nrLines();
		return tlines;
	}

	public void collectWordCounts(List<Integer> accumulated) {
		for (MFile mf : _MFiles)
			accumulated.add(mf.wordCount());
	}

	public List<TokenWithStatistics> collectedTokens() {
		return _collectedTokens;
	}

	public TokenWithStatistics getToken(Integer index) {
		return collectedTokens().get(index);
	}

	public void discardEmptyMFiles() {
		Iterator<MFile> it = _MFiles.iterator();
		while (it.hasNext()) {
			MFile mf = it.next();
			if (mf.hasZeroLoC())
				it.remove();
		}
	}

	public Integer getTokenIndex(String word) {
		for (Integer i = 0; i < _collectedTokens.size(); i++)
			if (_collectedTokens.get(i).getValue().equals(word))
				return i;
		return -1;
	}

	public static String getExcelColName(Integer col, String[] colName) {
		String name = "";
		if (col / 26 == 0)
			name = colName[col];
		if (col / 26 >= 1)
			name = colName[col / 26 - 1] + "" + colName[col % 26];
		return name;

	}

	private void sortTokens() {
		boolean changed = true;
		while (changed) {
			changed = false;
			for (Integer i = 0; i < _collectedTokens.size() - 1; i++) {
				if (_collectedTokens.get(i).getCount() < _collectedTokens.get(
						i + 1).getCount()) {
					changed = true;
					Collections.swap(_collectedTokens, i, i + 1);
				}
			}
		}
	}

	public void collectTokens() {
		ProgressDisplay.resetProgress('.', 100);
		Iterator<MFile> it = getMFilesIterator();
		while (it.hasNext()) {
			MFile mf = it.next();
			for (TokenWithStatistics tk : mf) {
				Integer index = getTokenIndex(tk.getValue());
				if (index == -1) {
					TokenWithStatistics token = new TokenWithStatistics(tk);
					token.setCount(tk.getCount());
					token.setNrMFiles(1);
					_collectedTokens.add(token);
				} else {
					_collectedTokens.get(index).incrCount(tk.getCount());
					_collectedTokens.get(index).incrNrOfFiles();
				}
			}
			ProgressDisplay.updateProgress();
		}
		sortTokens();
	}

	public void computeWordCounts() {
		ProgressDisplay.resetProgress('+', 100);
		for (Integer j = 0; j < collectedTokens().size(); j++) {
			Integer partialTotalCounts = 0;
			Integer maximum = 0;
			Integer minimum = 500000; // TODO: replace this magic number
			for (Iterator<MFile> it = getMFilesIterator(); it.hasNext();) {
				MFile mf = it.next();
				Integer index = mf.getTkIndex(getToken(j).getValue());
				if (index != -1) { // if count found
					mf.incrWordCount();
					Integer tokenCount = mf.getTkCount(index);
					maximum = (maximum < tokenCount) ? tokenCount : maximum;
					minimum = (minimum > tokenCount) ? tokenCount : minimum;
					partialTotalCounts += tokenCount;
				} else
					minimum = 0;
			}
			getToken(j).setMaxCount(maximum);
			getToken(j).setMinCount(minimum);
			getToken(j).setTotalCounts(partialTotalCounts);

			ProgressDisplay.updateProgress();
		}
	}

	public void processMFiles() throws FileNotFoundException, IOException {
		for (MFile mf : _MFiles) {
			mf.process();
			ProgressDisplay.updateProgress();
		}
	}
}
