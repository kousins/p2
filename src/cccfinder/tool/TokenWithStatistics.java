package cccfinder.tool;

import cccfinder.token_classifier.lexical_analyser.Token;

/**
 * @author Miguel P. Monteiro
 */
public class TokenWithStatistics extends Token {
	private int _totalCounts; //total counts from all m-files in this toolbox/ system
	private int _count;
	private int _numberOfMFiles;
	private int _maximumCount;
	private int _minimumCount; // minimum non-zero, of course

	public TokenWithStatistics(Token token) {
		super(token.type(), token.getValue());
		_count = 1;
		_numberOfMFiles = 0;
		_maximumCount = 0;
		_minimumCount = 0;
	}

	public TokenWithStatistics(TokenWithStatistics token) {
		super(token.type(), token.getValue());
		_count = token.getCount();
		_numberOfMFiles = token.nrMFiles();
		_maximumCount = token.getMaxCount();
		_minimumCount = token.getMinCount();
	}

	public void setMaxCount(int maximum) {
		_maximumCount = maximum;
	}

	public int getMaxCount() {
		return _maximumCount;
	}

	public void setMinCount(int minimum) {
		_minimumCount = minimum;
	}

	public int getMinCount() {
		return _minimumCount;
	}

	public void setTotalCounts(int totalCounts) {
		_totalCounts = totalCounts;
	}

	public int getTotalCounts() {
		return _totalCounts;
	}

	public int getCount() {
		return _count;
	}

	public void incrCount() {
		_count++;
	}

	public void incrCount(int additional) {
		_count += additional;
	}

	public void setCount(int newCountValue) {
		_count = newCountValue;
	}

	public int nrMFiles() {
		return _numberOfMFiles;
	}

	public void setNrMFiles(int nrFiles) {
		_numberOfMFiles = nrFiles;
	}

	public void incrNrOfFiles() {
		_numberOfMFiles++;
	}

	public String toString() {
		return getValue() + ":" + getCount();
	}

	public boolean equals(Token token) {
		return getValue().equals(token.getValue());
	}
}
