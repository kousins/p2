package cccfinder.tool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cccfinder.token_classifier.lexical_analyser.Token;


public class TokenStatisticsRegistry implements Iterable<TokenWithStatistics> {
	private List<TokenWithStatistics> _tokens;

	public TokenStatisticsRegistry() {
		_tokens = new ArrayList<TokenWithStatistics>();
	}

	private int searchIndex(String word) {
		for (int i = 0; i < _tokens.size(); i++)
			if (_tokens.get(i).getValue().equals(word))
				return i;
		return -1;
	}
	
	public boolean contains(Token token) {
		return searchIndex(token.getValue()) != -1;
	}
	
	public void register(Token tok) {
		int index = searchIndex(tok.getValue());
		if (index != -1)
			_tokens.get(index).incrCount();
		else
			_tokens.add(new TokenWithStatistics(tok));
	}

	public int numberOfTokens() {
		return _tokens.size();
	}
	public TokenWithStatistics getToken(int i) {
		return _tokens.get(i);
	}

	@Override
	public Iterator<TokenWithStatistics> iterator() {
		return _tokens.iterator();
	}
}
