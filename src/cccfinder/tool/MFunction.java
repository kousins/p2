package cccfinder.tool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cccfinder.token_classifier.lexical_analyser.Token;


public class MFunction {
	
	private String _funcName;
	private String _fileName;
	private String _domainName;
	public List<Token> tokens;
	private ParserWithStatistics _mfParser;
	
	public MFunction(){
		tokens = new ArrayList<Token>();
	}
	
	public String getName(){
		return _funcName;
	}
	
	public void setName(String name){
		_funcName = name;
	}
	
	public String getMFileName(){
		return _fileName;
	}
	
	public void setMFileName(String fileName){
		_fileName = fileName;
	}
	
	public String getDomainName(){
		return _domainName;
	}
	
	public void setDomainName(String domainName){
		_domainName = domainName;
	}
	
	public int getTokenCount(){
		return tokens.size();
	}
	
	public void addToken(Token tk){
		tokens.add(tk);
	}
	
	public Iterator<Token> iterator(){
		return tokens.iterator();
	}
}
