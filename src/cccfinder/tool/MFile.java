package cccfinder.tool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Parser;
import cccfinder.token_classifier.lexical_analyser.Tag;
import cccfinder.token_classifier.lexical_analyser.Token;


/**
 * This class represents a MATLAB source file, i.e. a *.m file. More
 * specifically, each instance of this class holds data extracted from a
 * specific *.m file.
 * 
 * @author Miguel P. Monteiro
 * @author Simona Posea
 */
public class MFile implements Iterable<TokenWithStatistics> {
	private String _domain;
	private File _file;
	private ParserWithStatistics _mfParser;
	private List<MFunction> _mFunctions;

	MFile() { } // Note: package-level visibility is on purpose

	public MFile(File file, String domain) {
		_file = file;
		_domain = domain;
		_mFunctions = new ArrayList<MFunction>();
	}

	public File getFile(){
		return _file;
	}
	
	public String getName() {
		return _file.getName();
	}

	public String getPathname() {
		return _file.getPath();
	}

	public String getToolboxName() {
		return _file.getParentFile().getName();
	}

	public int nrLines() {
		if(_mfParser == null) return 0;
		return _mfParser.numberOfLines();
	}

	public boolean hasZeroLoC() {
		return nrLines() == 0;
	}

	public String getDomain() {
		return _domain;
	}

	public void incrWordCount() {
		// do nothing
	}

	public int wordCount() {
		if(_mfParser == null) return 0;
		return _mfParser.numberOfTokens();
	}

	public int nrTokens() {
		if(_mfParser == null) return 0;
		return _mfParser.numberOfTokens();
	}

	public int getTkCount(Integer index) {
		if(_mfParser == null) return 0;
		return _mfParser.getToken(index).getCount();
	}

	public int getTkIndex(String word) {
		Integer nrTokens = _mfParser.numberOfTokens();
		for (Integer i = 0; i < nrTokens; i++)
			if (_mfParser.getToken(i).getValue().equals(word))
				return i;
		return -1;
	}

	public void process() throws FileNotFoundException, IOException {
		_mfParser = new ParserWithStatistics(_file);
		_mfParser.processMFile();
		_mfParser.computeStatistics();
	}

	@Override
	public Iterator<TokenWithStatistics> iterator() {
		return _mfParser.iterator();
	}

	public String toString() {
		return this.getName();
	}
	
	public void addMFunc(MFunction mfunc){
		_mFunctions.add(mfunc);
	}
	
	public Iterator<MFunction> funcIterator(){
		return _mFunctions.iterator();
	}
	
	//decompose the m-file-wide list of tokens into a list of m-functions
	public void extractMFunctions() throws IOException{
		
		Parser parser = new Parser(this.getFile());
		parser.processMFile();

		Iterator<Token> li = parser.taggedTokensIterator();
		
		Token first = li.next();
		MFunction mfunction;
		
		//creates a new function if first token is a "function"
		if(first.getValue().equals(LexicalElem.FUNCTION.toString())) {
			mfunction = new MFunction();
			mfunction.setMFileName(getName());
			mfunction.setDomainName(getDomain());
			this.addMFunc(mfunction);
			mfunction.addToken(first);
			
			while(li.hasNext()){
				Token second = li.next();	
				if(second.getTag() == Tag.FHD){
					mfunction.addToken(second);
					String fName = second.getValue();					
					mfunction.setName(fName);
				} else if(!second.getValue().equals(LexicalElem.FUNCTION.toString())){
					mfunction.addToken(second);
				} else if(second.getValue().equals(LexicalElem.FUNCTION.toString())){
					mfunction = new MFunction();
					mfunction.setMFileName(getName());
					mfunction.setDomainName(getDomain());
					this.addMFunc(mfunction);
					mfunction.addToken(second);
				}
			}
		}
	}
}
