package cccfinder.tool;

/**
 * IFilter objects decide whether a given word token is considered for further
 * processing or discarded.
 * @author Miguel P. Monteiro
 */
public interface IFilter {
	public boolean itPasses(MFile mFile);
}
