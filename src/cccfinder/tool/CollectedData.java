package cccfinder.tool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import util.ProgressDisplay;

/**
 * This class represents data extracted from MATLAB systems, to be used for
 * aspect mining.
 * 
 * @author Miguel P. Monteiro
 * @author Simona Posea
 */
public class CollectedData implements Iterable<Toolbox> {
	private final ArrayList<Toolbox> _toolboxSystems = new ArrayList<Toolbox>();
	private Toolbox _topToolbox;

	/**
	 * Creating a CollectedData object initiates the data extraction from all
	 * m-files contained in the specified folders. The extraction process is
	 * initiated by this class' constructor.
	 * 
	 * In a simple scenario, a toolbox is simply a folder containing m-files.
	 * Toolboxes can also contain sub-folders, in which case their m-files are
	 * processed as well. The processing recursively navigates through the
	 * entire folder hierarchy.
	 */
	public CollectedData(String inputPath)
			throws IOException, FileNotFoundException {
		Toolbox toolbox = new Toolbox(new File(inputPath), inputPath);
		_topToolbox = toolbox;
		_toolboxSystems.add(toolbox);
		for (Toolbox tb : _toolboxSystems) {
			tb.processMFiles();
			ProgressDisplay.println(" over");
			ProgressDisplay.print("Second step: collecting token statistics");
			tb.collectTokens();
			ProgressDisplay.println(" over");
			ProgressDisplay.print("Third step: compute word counts");
			tb.computeWordCounts();
			ProgressDisplay.println(" over");
		}
		ProgressDisplay.print("finishing... ");
		for (Toolbox tb : this)
			tb.discardEmptyMFiles();
		sortToolboxesByNrMFiles();
		ProgressDisplay.println("over");
	}

	public Toolbox getTopToolbox() {
		return _topToolbox;
	}

	public int nrToolboxSystems() {
		return _toolboxSystems.size();
	}

	public int totalLines() {
		Integer sum = 0;
		for (Toolbox app : _toolboxSystems)
			sum += app.getTotalLines();
		return sum;
	}

	public int totalMFiles() {
		int sum = 0;
		for (Toolbox tb : _toolboxSystems)
			sum += tb.getNrMFiles();
		return sum;
	}

	public Iterator<MFile> mFileIterator() {
		List<MFile> allMFiles = new ArrayList<MFile>();
		for (Toolbox tb : _toolboxSystems) {
			Iterator<MFile> it = tb.getMFilesIterator();
			while (it.hasNext())
				allMFiles.add(it.next());
		}
		return allMFiles.iterator();
	}

	public ArrayList<Integer> wordCounts() {
		ArrayList<Integer> accumulated = new ArrayList<Integer>();
		for (Toolbox tb : _toolboxSystems)
			tb.collectWordCounts(accumulated);
		return accumulated;
	}

	public List<String> collectedTokens() {
		ArrayList<String> result = new ArrayList<String>();
		for (Toolbox app : _toolboxSystems)
			for (TokenWithStatistics kw : app.collectedTokens())
				result.add(kw.getValue());
		return result;
	}

	/**
	 * Orders the toolboxes by descending order of number of m-files. Uses a
	 * (somewhat optimized) bubble sort
	 * 
	 * @author Simona Posea
	 */
	private void sortToolboxesByNrMFiles() {
		boolean changed = true;
		while (changed) {
			changed = false;
			for (Integer i = 0; i < nrToolboxSystems() - 1; i++)
				if (_toolboxSystems.get(i).getNrMFiles() < _toolboxSystems.get(
						i + 1).getNrMFiles()) {
					changed = true;
					Collections.swap(_toolboxSystems, i, i + 1);
				}
		}
	}

	@Override
	public Iterator<Toolbox> iterator() {
		return _toolboxSystems.iterator();
	}
}
