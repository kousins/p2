package cccfinder.output_view;

import java.io.IOException;

public interface DataOutputGen {
	public abstract void generateOutput() throws IOException;
}
