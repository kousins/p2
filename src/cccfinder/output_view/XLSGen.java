package cccfinder.output_view;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cccfinder.tool.CollectedData;
import cccfinder.tool.Toolbox;


import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableHyperlink;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * Operations for writing Matlab system data into XLS files. This implementation
 * is based on the JXL API.
 * 
 * @author Simona Posea
 * @author Miguel P. Monteiro
 */
public class XLSGen implements DataOutputGen {

	/**
	 * Enum type to determine whether word counts are presented as plain
	 * integers or as ratios to LoCs (number of non-blank, non-comment lines).
	 */
	private static enum WordCountForm {
		INTEGER, RATIO;
		public void writeCount(WritableSheet sheet, double value, int col,
				int row) throws WriteException, RowsExceededException {
			sheet.addCell(new Number(col, row, value));
			if (value != 0 && this == WordCountForm.INTEGER)
				sheet.getWritableCell(col, row).setCellFormat(
						nonZeroNrCellFormat());
		}
	}

	private static interface XLSCoordinates {
		static final int TOP_HEADER_ROW = 0;
		static final int TOTAL_HEADER_ROW = TOP_HEADER_ROW;
		static final int MAX_HEADER_ROW = TOTAL_HEADER_ROW + 1;
		static final int MIN_HEADER_ROW = MAX_HEADER_ROW + 1;
		static final int BOTTOM_HEADER_ROW = MIN_HEADER_ROW + 2;

		// bulk of table starts just below titles:
		static final int ROW_OFFSET = BOTTOM_HEADER_ROW + 1;

		// First set of column offsets for cells in the XLS primary table
		static final int COL_MFILE_NAME = 0;
		static final int COL_DOMAIN = COL_MFILE_NAME + 1;
		static final int COL_TOOLBOX_NAME = COL_DOMAIN + 1;
		static final int COL_NR_LOCS = COL_TOOLBOX_NAME + 1;
		static final int COL_WORD_COUNT = COL_NR_LOCS + 1;
		static final int COL_TOTAL_COUNTS = COL_WORD_COUNT + 1;
		static final int COL_OFFSET = COL_TOTAL_COUNTS + 1;
	}

	static final String[] XLS_COLUMN_NAMES = { "A", "B", "C", "D", "E", "F",
			"G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
			"T", "U", "V", "W", "X", "Y", "Z" };

	private static WritableCellFormat _nonZeroNrCellFormat;
	private static WritableCellFormat _headerCellFormat;
	private static WritableCellFormat _highlightFormat;

	private static final int PRIMARY_SHEET_INDEX = 0;
	private static final int RATIO_SHEET_INDEX = PRIMARY_SHEET_INDEX + 1;

	static {
		try {
			_headerCellFormat = new WritableCellFormat();
			_headerCellFormat.setBackground(Colour.LIGHT_ORANGE);
			_headerCellFormat.setAlignment(Alignment.CENTRE);

			_nonZeroNrCellFormat = new WritableCellFormat();
			_nonZeroNrCellFormat.setBackground(Colour.GRAY_25);

			_highlightFormat = new WritableCellFormat();
			_highlightFormat.setBackground(Colour.YELLOW);
		} catch (Exception e) {
			System.err.println("Error when creating XLS cell format.");
		}
	}

	private Map<Toolbox, WritableWorkbook> _toolbox2workbookMap = new HashMap<Toolbox, WritableWorkbook>();
	private CollectedData _data;

	/*
	 * Maximum number of tokens (words) to be included in the metrics tables
	 * collected from the m-files.
	 */
	public static final Integer WORD_COUNT_LIMIT = 200;

	public XLSGen(CollectedData data, String outputPath, String outputFilename)
			throws IOException {
		_data = data;
		_toolbox2workbookMap = new HashMap<Toolbox, WritableWorkbook>();
		String outputFilePath = outputPath + "\\"
				+ outputFilename.concat(".xls");
		WritableWorkbook wb = Workbook.createWorkbook(new File(outputFilePath));
		put(_data.getTopToolbox(), wb);
	}

	private void put(Toolbox tbSys, WritableWorkbook wb) {
		_toolbox2workbookMap.put(tbSys, wb);
	}

	private WritableWorkbook get(Toolbox tbSys) {
		return _toolbox2workbookMap.get(tbSys);
	}

	private static WritableCellFormat headerCellFormat() {
		return _headerCellFormat;
	}

	private static WritableCellFormat nonZeroNrCellFormat() {
		return _nonZeroNrCellFormat;
	}

	private static WritableCellFormat highlightFormat() {
		return _highlightFormat;
	}

	/******************************************************************************
	 ******************************************************************************/

	private void writeHeaderLabel(WritableSheet sheet, String label, int col,
			int row) throws WriteException, RowsExceededException {
		sheet.addCell(new Label(col, row, label));
		sheet.getWritableCell(col, row).setCellFormat(headerCellFormat());
	}

	private void writeHeaderNumber(WritableSheet sheet, double value, int col,
			int row) throws WriteException, RowsExceededException {
		sheet.addCell(new Number(col, row, value));
		sheet.getWritableCell(col, row).setCellFormat(headerCellFormat());
	}

	private void writeHeaderRows(Toolbox toolbox, WritableSheet sheet,
			int startCol, int startRow) throws WriteException,
			RowsExceededException {
		int offset = 0;

		// First row: for each token, total of m-files in which the token
		// appears
		sheet.addCell(new Label(startCol - 1, startRow + offset, "#m-files:"));
		sheet.getWritableCell(startCol - 1, startRow + offset).setCellFormat(
				headerCellFormat());
		for (int i = 0; i < WORD_COUNT_LIMIT
				&& i < toolbox.collectedTokens().size(); i++)
			writeHeaderNumber(sheet, toolbox.collectedTokens().get(i)
					.nrMFiles(), startCol + i, startRow + offset);

		// Second row: sum of all counts for this token
		offset++;
		writeHeaderLabel(sheet, "Sum counts:", startCol - 1, startRow + offset);
		for (int i = 0; i < WORD_COUNT_LIMIT
				&& i < toolbox.collectedTokens().size(); i++)
			writeHeaderNumber(sheet, toolbox.getToken(i).getTotalCounts(),
					startCol + i, startRow + offset);

		// Third row: maximum token counts, for each m-file
		offset++;
		writeHeaderLabel(sheet, "Max count:", startCol - 1, startRow + offset);
		for (int i = 0; i < WORD_COUNT_LIMIT
				&& i < toolbox.collectedTokens().size(); i++) {
			writeHeaderNumber(sheet, toolbox.getToken(i).getMaxCount(),
					startCol + i, startRow + offset);
		}

		// Fourth row: minimum token count, for each m-file
		offset++;
		writeHeaderLabel(sheet, "Min count:", startCol - 1, startRow + offset);
		for (int i = 0; i < WORD_COUNT_LIMIT
				&& i < toolbox.collectedTokens().size(); i++)
			writeHeaderNumber(sheet, toolbox.getToken(i).getMinCount(),
					startCol + i, startRow + offset);

		// Bottom row: tokens selected for the table, from all extracted tokens
		offset++;
		for (int i = 0; i < WORD_COUNT_LIMIT
				&& i < toolbox.collectedTokens().size(); i++)
			writeHeaderLabel(sheet,
					toolbox.collectedTokens().get(i).getValue(), startCol + i,
					startRow + offset);
	}

	private void writePrimaryTabHeader(Toolbox toolbox, WritableSheet sheet)
			throws WriteException, RowsExceededException {
		writeHeaderLabel(sheet, "File Name", XLSCoordinates.COL_MFILE_NAME,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderLabel(sheet, "Domain", XLSCoordinates.COL_DOMAIN,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderLabel(sheet, "Toolbox", XLSCoordinates.COL_TOOLBOX_NAME,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderLabel(sheet, "#LoC", XLSCoordinates.COL_NR_LOCS,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderLabel(sheet, "#words", XLSCoordinates.COL_WORD_COUNT,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderLabel(sheet, "#total counts",
				XLSCoordinates.COL_TOTAL_COUNTS,
				XLSCoordinates.BOTTOM_HEADER_ROW);
		writeHeaderRows(toolbox, sheet, XLSCoordinates.COL_OFFSET + 1,
				XLSCoordinates.TOP_HEADER_ROW);

		int col_path = XLSCoordinates.COL_OFFSET + 1
				+ Math.min(WORD_COUNT_LIMIT, toolbox.collectedTokens().size());
		writeHeaderLabel(sheet, "Path to the m-file", col_path,
				XLSCoordinates.BOTTOM_HEADER_ROW);
	}

	private void writePrimaryTabFooter(Toolbox toolbox, WritableSheet sheet)
			throws WriteException, RowsExceededException {
		sheet.addCell(new Label(0, toolbox.getNrMFiles()
				+ XLSCoordinates.ROW_OFFSET + 1, "#Files"));
		sheet.addCell(new Number(1, toolbox.getNrMFiles()
				+ XLSCoordinates.ROW_OFFSET + 1, toolbox.getNrMFiles()));
	}

	private void writePrimaryTable(Toolbox toolbox, WritableSheet sheet,
			WritableCellFormat cellFormat4Percentage,
			WritableCellFormat cellFormat4WordCount, WordCountForm wrdCntForm)
			throws WriteException, RowsExceededException {
		// Special cell format for the word count totals:
		writePrimaryTabHeader(toolbox, sheet);

		/*
		 * bulk of the table, comprising the highest-count tokens from the
		 * entire repository
		 */
		// header for the main part of the table
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			sheet.addCell(new Label(XLSCoordinates.COL_MFILE_NAME,
					XLSCoordinates.ROW_OFFSET + i, toolbox.getMFile(i)
							.getName()));
			sheet.addCell(new Label(XLSCoordinates.COL_DOMAIN,
					XLSCoordinates.ROW_OFFSET + i, toolbox.getMFile(i)
							.getDomain()));
			sheet.addCell(new Label(XLSCoordinates.COL_TOOLBOX_NAME,
					XLSCoordinates.ROW_OFFSET + i, toolbox.getMFile(i)
							.getToolboxName()));
			sheet.addCell(new Number(XLSCoordinates.COL_NR_LOCS,
					XLSCoordinates.ROW_OFFSET + i, toolbox.getMFile(i)
							.nrLines()));
			sheet.addCell(new Number(XLSCoordinates.COL_WORD_COUNT,
					XLSCoordinates.ROW_OFFSET + i, toolbox.getMFile(i)
							.wordCount()));
			sheet.getWritableCell(XLSCoordinates.COL_WORD_COUNT,
					XLSCoordinates.ROW_OFFSET + i).setCellFormat(
					highlightFormat());

			int totalTKCounts = 0;
			for (int j = 0; j < toolbox.collectedTokens().size()
					&& j < WORD_COUNT_LIMIT; j++) {
				int index = toolbox.getMFile(i).getTkIndex(
						toolbox.collectedTokens().get(j).getValue());
				double number = (index == -1) ? 0 : toolbox.getMFile(i)
						.getTkCount(index);
				totalTKCounts += number;
				if (wrdCntForm == WordCountForm.RATIO)
					number = number / toolbox.getMFile(i).nrLines();
				wrdCntForm.writeCount(sheet, number, j
						+ XLSCoordinates.COL_OFFSET + 1, i
						+ XLSCoordinates.ROW_OFFSET);
			}
			sheet.addCell(new Number(XLSCoordinates.COL_TOTAL_COUNTS, i
					+ XLSCoordinates.ROW_OFFSET, totalTKCounts));
			sheet.getWritableCell(XLSCoordinates.COL_TOTAL_COUNTS,
					i + XLSCoordinates.ROW_OFFSET).setCellFormat(
					highlightFormat());
			int colPath = toolbox.collectedTokens().size();
			colPath = colPath > WORD_COUNT_LIMIT ? WORD_COUNT_LIMIT : colPath;
			colPath += XLSCoordinates.COL_OFFSET + 1;
			WritableHyperlink hyperlink = new WritableHyperlink(colPath, i
					+ XLSCoordinates.ROW_OFFSET, new File(toolbox.getMFile(i)
					.getPathname()));
			sheet.addHyperlink(hyperlink);
		}
		writePrimaryTabFooter(toolbox, sheet);
	}

	private void writeXLSTables(Toolbox toolbox, WritableWorkbook wb,
			String sheetName, int sheetIndex, WordCountForm wordCountForm)
			throws WriteException, IOException {
		WritableSheet sheet = wb.createSheet(sheetName, sheetIndex);
		WritableCellFormat cellFmtPer100 = new WritableCellFormat(
				new NumberFormat("0.00%"));

		writePrimaryTable(toolbox, sheet, cellFmtPer100, headerCellFormat(),
				wordCountForm);
	}

	private void generateXLS4Toolbox(Toolbox tbSystem) throws WriteException,
			IOException {
		java.lang.System.gc(); // just in case. fairly large data sets here

		WritableWorkbook wb = get(tbSystem);
		writeXLSTables(tbSystem, wb, "#Words", PRIMARY_SHEET_INDEX,
				XLSGen.WordCountForm.INTEGER);
		writeXLSTables(tbSystem, wb, "#Words%", RATIO_SHEET_INDEX,
				XLSGen.WordCountForm.RATIO);
		wb.write();
		wb.close();
	}

	@Override
	public void generateOutput() throws IOException {
		try {
			for (Toolbox tb : _data)
				generateXLS4Toolbox(tb);
		} catch (WriteException we) {
			throw new IOException(we);
		}
	}
}
