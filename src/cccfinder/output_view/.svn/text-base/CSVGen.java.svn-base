package cccfinder.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import cccfinder.tool.CollectedData;
import cccfinder.tool.Toolbox;
import util.ProgressDisplay;

public class CSVGen implements DataOutputGen {
	private static final String CSV_SEPARATOR = ";";
	private static final int HEADER_BLANKS = 5;

	private String _outputPath;
	private String _filename;
	private CollectedData _data;
	private PrintWriter _out;

	public CSVGen(CollectedData data, String outputPath, String outputFilename)
			throws FileNotFoundException {
		ProgressDisplay.println("CSVGen outputPath: " + outputPath + "\\" + outputFilename);

		_outputPath = outputPath;
		_filename = outputFilename;
		_data = data;
	}

	private void printBlankColumns(Integer nrBlankCols) {
		for (int i = 0; i < nrBlankCols; i++)
			_out.print(CSV_SEPARATOR);
	}

	private void printHeader(Toolbox toolbox) {
		// First row: for each token, total of m-files in which token appears
		printBlankColumns(HEADER_BLANKS);
		_out.print("#m-files:");

		for (int i = 0; i < toolbox.collectedTokens().size(); i++) {
			if(toolbox.collectedTokens().get(i).getTotalCounts() <= 1)
				break;
			_out.print(CSV_SEPARATOR
					+ toolbox.collectedTokens().get(i).nrMFiles());
		}
		_out.println();

		// Second row: sum of all counts for this token
		printBlankColumns(HEADER_BLANKS);
		_out.print("Sum counts:");
		for (int i = 0; i < toolbox.collectedTokens().size(); i++) {
			if(toolbox.collectedTokens().get(i).getTotalCounts() <= 1)
				break;
			_out.print(CSV_SEPARATOR + toolbox.getToken(i).getTotalCounts());
		}
		_out.println();

		// Third row: maximum token counts, for each m-file
		printBlankColumns(HEADER_BLANKS);
		_out.print("Max count:");
		for (int i = 0; i < toolbox.collectedTokens().size(); i++) {
			if(toolbox.collectedTokens().get(i).getTotalCounts() <= 1)
				break;
			_out.print(CSV_SEPARATOR + toolbox.getToken(i).getMaxCount());
		}
		_out.println();

		//Fourth row:
		//"File Name", "Domain", "Toolbox", "#LoC", "#words", "#total counts", ...
		_out.print("File Name");
		_out.print(CSV_SEPARATOR + "Domain");
		_out.print(CSV_SEPARATOR + "Toolbox");
		_out.print(CSV_SEPARATOR + "#LoC");
		_out.print(CSV_SEPARATOR + "#words");
		_out.print(CSV_SEPARATOR + "#total counts");

		//...followed by the function names
		for (int i = 0; i < toolbox.collectedTokens().size(); i++) {
			if(toolbox.collectedTokens().get(i).getTotalCounts() <= 1)
				break;
			_out.print(CSV_SEPARATOR + toolbox.getToken(i).getValue());
		}
		_out.println();
	}

	@Override
	public void generateOutput() throws IOException {
		String outfile = _outputPath + "\\" + _filename + ".csv";
		_out = new PrintWriter(outfile );

		Toolbox toolbox = _data.getTopToolbox();
		printHeader( toolbox );
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			//print the left header for the i-th m-file
			_out.print(toolbox.getMFile(i).getName() + CSV_SEPARATOR);
			_out.print(toolbox.getMFile(i).getDomain() + CSV_SEPARATOR);
			_out.print(toolbox.getMFile(i).getToolboxName() + CSV_SEPARATOR);
			_out.print(toolbox.getMFile(i).nrLines() + CSV_SEPARATOR);
			_out.print(toolbox.getMFile(i).wordCount() + CSV_SEPARATOR);

			//Compute the total counts of tokens for the i-th m-file
			int totalTKCounts = 0;
			for (int j = 0; j < toolbox.collectedTokens().size(); j++) {
				if(toolbox.collectedTokens().get(j).getTotalCounts() <= 1)
					break;
				int index = toolbox.getMFile(i).getTkIndex(
						toolbox.collectedTokens().get(j).getValue());
				//Using int, meaning no ratios are computed
				int number = (index == -1) ? 0 : toolbox.getMFile(i)
						.getTkCount(index);
				totalTKCounts += number;
			}
			_out.print(totalTKCounts + CSV_SEPARATOR);

			//Main body of the table: the counts of each token 
			for (int j = 0; j < toolbox.collectedTokens().size(); j++) {
				if(toolbox.collectedTokens().get(j).getTotalCounts() <= 1)
					break;
				int index = toolbox.getMFile(i).getTkIndex(
						toolbox.collectedTokens().get(j).getValue());
				//Using int, meaning no ratios are computed
				int number = (index == -1) ? 0 : toolbox.getMFile(i)
						.getTkCount(index);
				_out.print(number + CSV_SEPARATOR);
			}
			_out.println();
		}
		_out.close();
		_out = null;
	}
}
