package cccfinder.metrics;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import cccfinder.pairs.ParserTokens;
import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.token_classifier.token_table.TokenTable;
import cccfinder.tool.MFile;

//counts the number of ocurrences of a concern in each file

public class TokenDensityConcern {
	
	public int getTokenDensityConcern(MFile mFile, TokenTable table, String concern) throws IOException{
		ParserTokens pt = new ParserTokens(mFile.getFile());
		LexicalAnalyser la = pt.processMFile2();
		
		int totalCount = 0;
		
		ListIterator<Token> li = la.listIterator();
		
		while(li.hasNext()){
			Token token = li.next();
			if(table.concernHasToken(concern, token.getValue())){
				totalCount++;
			}
		}
		return totalCount;
	}
	
	//counts the number of distinct tokens for each concern
	public String[] getDistinctTokensPerConcern(MFile mFile, TokenTable table, String concern) throws IOException{
		ParserTokens pt = new ParserTokens(mFile.getFile());
		LexicalAnalyser la = pt.processMFile2();
		
		ListIterator<Token> li = la.listIterator();
		
		Set<String> checkedTokens = new TreeSet<String>();
		
		while(li.hasNext()){
			Token token = li.next();
			if(table.concernHasToken(concern, token.getValue())){
				checkedTokens.add(token.getValue());
			}
		}
		
		String[] tokens = checkedTokens.toArray(new String[checkedTokens.size()]);
		return tokens;
	}
}
