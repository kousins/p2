package cccfinder.metrics;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import cccfinder.pairs.ParserTokens;
import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.tool.CollectedData;
import cccfinder.tool.IFilter;
import cccfinder.tool.MFile;
import cccfinder.tool.MFunction;
import cccfinder.tool.Toolbox;

public class PairsOfTokens {

	public static final String INPUT_PATH = "C:\\Users\\Bruno Palma LT\\Desktop\\Tese MIEI\\code_repositories\\Code Repository";
	
	static HashMap<String, Integer> results;
	
	public static HashMap<String,Integer> findPairsOfTokensMFunctions(CollectedData data, String token1, String token2)
			throws IOException {
		Toolbox toolbox = data.getTopToolbox();
		results = new HashMap<String, Integer>();
		
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {

			MFile mFile = toolbox.getMFile(i);

			mFile.extractMFunctions();

			Iterator<MFunction> funcIt = mFile.funcIterator();

			while (funcIt.hasNext()) {
				MFunction func = funcIt.next();
				Iterator<Token> li = func.iterator();
				
				int count = 0;
				
				while(li.hasNext()){
					Token first = li.next();
					if(first.getValue().contentEquals(token1)){
						Token second = li.next();
						if(second.getValue().contentEquals(token2)){
							count++;	
						}
					}
				}
				
				results.put(func.getName(),count);
				//System.out.println(func.getName() + " in file " + func.getMFileName() + " has " + Integer.toString(count) + " occurrences");
			}
		}
		//System.out.println("Results size" + " " + results.size());
		return results;
	}
	
	public static HashMap<String,Integer> findPairsOfTokensMFiles(CollectedData data, IFilter filter, String token1, String token2) throws IOException {
		Toolbox toolbox = data.getTopToolbox();
		results = new HashMap<String, Integer>();
		
		for (int i = 0; i < toolbox.getNrMFiles(); i++) {
			MFile mFile = toolbox.getMFile(i);
			int count = 0;
			ParserTokens pt = new ParserTokens(mFile.getFile());
			LexicalAnalyser la = pt.processMFile2();			
			ListIterator<Token> li = la.listIterator();
			while(li.hasNext()){
				Token first = li.next();
				if(first.getValue().contentEquals(token1)){
					Token second = li.next();
					if(second.getValue().contentEquals(token2)){
						count++;	
					}
				}
			}
			//System.out.println("Ocurrences: " + count + " in file: " +  mFile.getName());
			results.put(mFile.getName(),count);
		}
		//System.out.println("Results size" + " " + results.size());
		return results;
	}

	public Iterator<Integer> iterator(){
		return results.values().iterator();
	}
	
/*	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Scanner input = new Scanner(System.in);
		CollectedData data = new CollectedData(INPUT_PATH);
	    System.out.print("Insert first token: ");
	    String tk1 = input.next();
	    System.out.print("Insert second token: ");
	    String tk2 = input.next();
	    System.out.print("----------------------");
		findPairsOfTokensMFiles(data, new MinimumLoCFilter(), tk1, tk2);
		//findPairsOfTokensMFunctions(data, tk1, tk2);
	}*/
}
