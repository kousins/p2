package cccfinder.metrics;

import java.io.IOException;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;

import cccfinder.pairs.ParserTokens;
import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.token_classifier.token_table.TokenTable;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.Toolbox;

public class ConcernFrequency {

	//private
	static final String INPUT_PATH = "C:\\Users\\Bruno Palma LT\\Desktop\\Tese MIEI\\code_repositories\\Code Repository";
	//private
	static final String TABLE_PATH = ".\\dat\\token_table.txt";
	
	/*private static final String[] CONCERNS = { "Messages and monitoring", "I/O data",
			"Verification of function arguments and return values", "Data type verification and specialization",
			"System", "Memory allocation/deallocation", "Parallelization", "Dynamic properties" };*/

	static HashMap<MFile, Integer> results;

	public static HashMap<MFile, Integer> getConcernFrequencyMFiles(TokenTable table, CollectedData data, String concern) throws IOException{
		Toolbox toolbox = data.getTopToolbox();
		results = new HashMap<MFile, Integer>();
		
		for(int i = 0; i < toolbox.getNrMFiles();i++){
			MFile mFile = toolbox.getMFile(i);

			ParserTokens pt = new ParserTokens(mFile.getFile());
			LexicalAnalyser la = pt.processMFile2();
			
			int totalCount = 0;
			
			ListIterator<Token> li = la.listIterator();
			
			while(li.hasNext()){
				Token token = li.next();
				if(table.concernHasToken(concern, token.getValue())){
					totalCount++;
				}
			}
			results.put(mFile, totalCount);
			//System.out.println(mFile.getName() + " " + totalCount);
		}
		return results;
	}
	
	public int getConcernFrequencyMFile(MFile mFile, TokenTable table, String concern) throws IOException{
		ParserTokens pt = new ParserTokens(mFile.getFile());
		LexicalAnalyser la = pt.processMFile2();
		
		int totalCount = 0;
		
		ListIterator<Token> li = la.listIterator();
		
		while(li.hasNext()){
			Token token = li.next();
			if(table.concernHasToken(concern, token.getValue())){
				totalCount++;
			}
		}
		return totalCount;
	}
	
	public int getDistinctConcernFrequencyMFile(MFile mFile, TokenTable table, String concern) throws IOException{
		ParserTokens pt = new ParserTokens(mFile.getFile());
		LexicalAnalyser la = pt.processMFile2();
		
		ListIterator<Token> li = la.listIterator();
		
		Set<Token> checkedTokens = new TreeSet<Token>();
		
		while(li.hasNext()){
			Token token = li.next();
			if(table.concernHasToken(concern, token.getValue())){
				checkedTokens.add(token);
			}
		}
		return checkedTokens.size();
	}
	
/*	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		TokenTable table = new TokenTable(TABLE_PATH);
		CollectedData data = new CollectedData(INPUT_PATH);		
		System.out.println("Frequency of Messages and monitoring Concern\n");
		getConcernFrequencyMFiles(table, data, "Messages and monitoring");
		System.out.println("Frequency of I/O data Concern\n");
		getConcernFrequencyMFiles(table, data, "I/O data");
		System.out.println("Max Value: " + getMaxValue());
	}*/

}
