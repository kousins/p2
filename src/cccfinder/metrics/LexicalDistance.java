package cccfinder.metrics;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

import cccfinder.pairs.ParserTokens;
import cccfinder.token_classifier.lexical_analyser.LexicalAnalyser;
import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.MFunction;
import cccfinder.tool.Toolbox;

public class LexicalDistance {

	//private
	static final String INPUT_PATH = "C:\\Users\\Bruno Palma LT\\Desktop\\Tese MIEI\\code_repositories\\Code Repository";
	
	static HashMap<MFile, Integer> results;
	
	public static HashMap<MFile, Integer> getLexicalDistanceMFiles(CollectedData data, String token1, String token2, int distance)
			throws IOException {
		Toolbox toolbox = data.getTopToolbox();
		results = new HashMap<MFile, Integer>();

		for (int i = 0; i < toolbox.getNrMFiles(); i++) {

			MFile mFile = toolbox.getMFile(i);

			ParserTokens pt = new ParserTokens(mFile.getFile());
			LexicalAnalyser la = pt.processMFile2();

			// calculate lexical distance

			int tokenCount = 0;
			int pairCount = 0;

			ListIterator<Token> li = la.listIterator();

			while (li.hasNext()) {
				Token first = li.next();
				if (first.getValue().contentEquals(token1)) {
					ListIterator<Token> li2 = li;

					while (li2.hasNext()) {
						Token second = li.next();
						if (second.getValue().contentEquals(token1)) {
							tokenCount = 0;
						} else if (second.getValue().contentEquals(token2)) {
							if (tokenCount <= distance) {
								pairCount++;
							}
						} else if (!second.getValue().contentEquals(token1)
								&& !second.getValue().contentEquals(token2)) {
							tokenCount++;
						}
					}
				}
			}
			results.put(mFile, pairCount);
			System.out.println(mFile.getName() + " " + Integer.toString(pairCount));
		}

		System.out.println("Results size" + " " + results.size());
		return results;
	}

	public static HashMap<MFunction,Integer> getLexicalDistanceMFunctions(CollectedData data, String token1, String token2, int distance)
			throws IOException {
		Toolbox toolbox = data.getTopToolbox();
		HashMap<MFunction, Integer> results = new HashMap<MFunction,Integer>();

		for (int i = 0; i < toolbox.getNrMFiles(); i++) {

			MFile mFile = toolbox.getMFile(i);

			mFile.extractMFunctions();

			Iterator<MFunction> funcIt = mFile.funcIterator();

			while (funcIt.hasNext()) {
				MFunction func = funcIt.next();
				Iterator<Token> li = func.iterator();
				
				int tokenCount = 0;
				int pairCount = 0;
				
				while (li.hasNext()) {
					Token first = li.next();
					if (first.getValue().contentEquals(token1)) {
						Iterator<Token> li2 = li;

						while (li2.hasNext()) {
							Token second = li.next();
							if (second.getValue().contentEquals(token1)) {
								tokenCount = 0;
							} else if (second.getValue().contentEquals(token2)) {
								if (tokenCount <= distance) {
									pairCount++;
								}
							} else if (!second.getValue().contentEquals(token1)
									&& !second.getValue().contentEquals(token2)) {
								tokenCount++;
							}
						}
					}
				}
				results.put(func, pairCount);
				//System.out.print(func.getName() + " function in file " + func.getMFileName() + " has " + Integer.toString(pairCount) + " occurrences\n");
			}
		}
		//System.out.println("Results size" + " " + results.size());
		return results;
	}
	
	public static int getMaxValue(){
		return Collections.max(results.values());
	}

/*	
 *lexical distance between 0 and n-1 values
 * @SuppressWarnings("resource")
	private static void getLexicalDistanceMulti(CollectedData data, String token1, String token2, int distance)
			throws IOException {

		Toolbox toolbox = data.getTopToolbox();
		FileWriter writer = new FileWriter(OUTPUT_FILENAME);

		writer.append("Filename");
		for (int i = 0; i < distance; i++) {
			writer.append(';');
			writer.append(i + " between " + token1 + " and " + token2);
		}
		writer.append('\n');

		for (int i = 0; i < toolbox.getNrMFiles(); i++) {

			MFile mFile = toolbox.getMFile(i);

			// write mfile name in output file
			writer.append(mFile.getName());

			int[] counts = new int[distance];

			ParserTokens pt = new ParserTokens(mFile.getFile());
			LexicalAnalyser la = pt.processMFile2();

			// calculate lexical distances
			for (int aux = 0; aux < counts.length; aux++) {

				int tokenCount = 0;
				int pairCount = 0;

				ListIterator<Token> li = la.listIterator();

				while (li.hasNext()) {
					Token first = li.next();
					if (first.getValue().contentEquals(token1)) {
						ListIterator<Token> li2 = li;

						while (li2.hasNext()) {
							Token second = li.next();
							if (second.getValue().contentEquals(token1)) {
								tokenCount = 0;
							} else if (second.getValue().contentEquals(token2)) {
								if (tokenCount == aux) {
									pairCount++;
								}
							} else if (!second.getValue().contentEquals(token1)
									&& !second.getValue().contentEquals(token2)) {
								tokenCount++;
							}
						}
					}

				}
				counts[aux] = pairCount;
			}

			for (int k = 0; k < counts.length; k++) {
				System.out.println("No. of pairs " + token1 + " and " + token2 + " with distance " + k + " in "
						+ mFile.getName() + " : " + counts[k]);
				writer.append(';');
				writer.append(String.valueOf(counts[k]));
			}
			writer.append('\n');
		}
	}*/
	
/*	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Scanner input = new Scanner(System.in);
		CollectedData data = new CollectedData(INPUT_PATH);
		System.out.print("Insert first token: ");
		String tk1 = input.next();
		System.out.print("Insert second token: ");
		String tk2 = input.next();
		System.out.print("Insert distance value: ");
		String dt = input.next();
		System.out.print("----------------------");
		getLexicalDistanceMFiles(data, tk1, tk2, Integer.parseInt(dt));
		//getLexicalDistanceMFunctions(data, tk1, tk2, Integer.parseInt(dt));
		System.out.println(getMaxValue());
		System.out.println(getMinValue());
	}*/

}
