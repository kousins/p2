package cccfinder.output_table;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import cccfinder.tool.MFile;

public interface IMetricTable {

	public void buildMetricTable() throws IOException;

	public Map<MFile, List<Integer>> getResults();
	
	public int getMaxColumn(int i);
	
	public int getMinColumn(int i);
	
	public void generateCSVFile() throws IOException;
}
