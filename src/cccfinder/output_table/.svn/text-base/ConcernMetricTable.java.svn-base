package output_table;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cccfinder.density.MinimumLoCFilter;
import cccfinder.metrics.TokenDensityConcern;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import cccfinder.tool.Toolbox;
import token_classifier.token_table.TokenTable;

//creates table with token density data for each concern

public class ConcernMetricTable implements IMetricTable {

	    //private static final String INPUT_PATH = "C:\\Users\\Bruno Palma\\Desktop\\code_repositories\\Rep_One";
		//private static final String INPUT_PATH = "C:\\Users\\Bruno Palma\\Desktop\\code_repositories\\Rep_Two";
		private static final String INPUT_PATH = "C:\\Users\\Bruno Palma\\Desktop\\code_repositories\\Rep_Three";
		//private static final String INPUT_PATH = "C:\\Users\\Bruno Palma\\Desktop\\code_repositories\\TEST_REP";
		//private static final String INPUT_PATH = "C:\\Users\\Bruno Palma\\Desktop\\code_repositories\\Filtered_Rep";
		
		private static final String OUTPUT_FILENAME = "C:\\Users\\Bruno Palma\\workspace\\CCCExplorer_1.03\\METRIC_CONCERN_FILE.csv";
		private static final String OUTPUT_FILENAME_2 = "C:\\Users\\Bruno Palma\\workspace\\CCCExplorer_1.03\\no_concerns_per_file_bigrep_v4.csv";
		private static final String OUTPUT_FILENAME_3 = "C:\\Users\\Bruno Palma\\workspace\\CCCExplorer_1.03\\DISTINCT_TOKENS_TEST.csv";
		
		// token-concern table
		private static final String TABLE_PATH = "C:\\Users\\Bruno Palma\\workspace\\CCCExplorer_1.03\\dat\\token_table_v4.txt";
		
		//results
		public Map<MFile, List<Integer>> results;
				
		TokenDensityConcern cf;
		
		CollectedData data;
		TokenTable table;
		
		Toolbox toolbox;

		MinimumLoCFilter filt;
								
		/*public static String [] concernNames = {"Console messages", "Visualization - Plots", "Visualization - Images and videos", 
				"I/O data through files", "Verification of function arguments and return values", "Data type specialization", 
				"Data type verification", "Printing", "System - others", "System - libs, other lang.", "System - clear workspace/environment/memory", 
				"System - calendar", "System - debuging", "System - control execution", "System - persistency", "System - timing", "System - profiling", "Memory allocation/deallocation", "Parallelization", "Dynamic properties"};*/
			
		//concenrs from version 3 of the table
		public static String [] concernNames = {"1- Verification of function arguments and return values",
				"2- Data type specialization",
				"3- Data type verification",
				"4- Dynamic properties",
				"5- Console messages",
				//"6- Printing",
				"7- Visualization",
				"8- File I/O",
				"9- System",
				"10- Memory allocation/deallocation",
				"11- Parallelization"};
		
		static int maxColumns[];
		static int minColumns[];
		
		public ConcernMetricTable() throws FileNotFoundException, IOException{
			cf = new TokenDensityConcern();
			
			data = new CollectedData(INPUT_PATH);
			table = new TokenTable(TABLE_PATH);
			
			results = new HashMap<MFile, List<Integer>>();
						
			maxColumns = new int[concernNames.length];
			for(int i = 0; i < maxColumns.length; i++){
				maxColumns[i] = Integer.MIN_VALUE;
			}
			
			minColumns = new int[concernNames.length];
			for(int i = 0; i < minColumns.length; i++){
				minColumns[i] = Integer.MAX_VALUE;
			}
			
			toolbox = data.getTopToolbox();
			
			filt = new MinimumLoCFilter();
		}
		
		public void buildMetricTable() throws IOException{
			for(int i =0; i < toolbox.getNrMFiles(); i++){
				
				MFile mFile = toolbox.getMFile(i);

				if (filt.itPassesTwo(mFile)) {
					
					List<Integer> densityValues = new ArrayList<Integer>();
					
					for(int j = 0; j < concernNames.length; j++){
						
						String currentConcern = concernNames[j];
						
						int density = cf.getTokenDensityConcern(mFile, table, currentConcern);
						
						densityValues.add(density);
						
						//update maximum and minimum for each value
						if (density > maxColumns[j]) {
							maxColumns[j] = density;
						}
						
						if (density < minColumns[j]) {
							minColumns[j] = density;
						}
						
					}
					
					int zeroCount = Collections.frequency(densityValues, 0);
					
					int concernCount = densityValues.size() - zeroCount;
					
					//checks number of concerns in file
					if(checkColumns(concernCount)){
						results.put(mFile, densityValues);	
					}				
				}
			}
		}

		public void buildMetricTableNormByDensity() throws IOException{
			for(int i =0; i < toolbox.getNrMFiles(); i++){
				
				MFile mFile = toolbox.getMFile(i);

				if (filt.itPassesTwo(mFile)) {
					
					List<Integer> densityValues = new ArrayList<Integer>();
					int LoC = mFile.nrLines();
					
					for(int j = 0; j < concernNames.length; j++){
						
						String currentConcern = concernNames[j];
						
						int density = (cf.getTokenDensityConcern(mFile, table, currentConcern) * 10000)/LoC;
						
						densityValues.add(density);
						
						//update maximum and minimum for each value
						if (density > maxColumns[j]) {
							maxColumns[j] = density;
						}
						
						if (density < minColumns[j]) {
							minColumns[j] = density;
						}
						
					}
								
					int zeroCount = Collections.frequency(densityValues, 0);
					
					int concernCount = densityValues.size() - zeroCount;
					
					//checks number of concerns in file
					if(checkColumns(concernCount)){
						results.put(mFile, densityValues);	
					}	
				}
			}
		}
		
		//checks number of concerns in file
		public boolean checkColumns(int columns){
			return columns >= 3;
		}
		
		//returns result set
		public Map<MFile,List<Integer>> getResults(){
			return this.results;
		}
		
		//get max for column i
		public int getMaxColumn(int i){
			return maxColumns[i];
		}
		
		//get min for column i
		public int getMinColumn(int i){
			return minColumns[i];
		}
		
		//normalization formula: without log
		public double normalizeValue(int value, int max, int min){
			return (( (double) value - min)/(max - min)) * 1000;
		}
		
		//normalization formula: with log
		public double normalizeValueLog(int value, int max, int min){
			
			double norm = (( (double) value - min)/(max - min));
			
			if(norm > 1){
				System.out.println("M�ximo e valor errado: " + max + " , " + value);
			}
			
			return  (Math.log((norm * 6000) + 1) * (1000/Math.log(6001)));
		}
		
		//normalizes values from table
		public void normalizeTable(){
			Iterator<Entry<MFile, List<Integer>>> it = results.entrySet().iterator();
			
			while(it.hasNext()){
				
				Entry<MFile,List<Integer>> entry = it.next();
				MFile file = entry.getKey();
				List<Integer> values = entry.getValue();
				
				for (int i = 0; i < values.size(); i++){
					
					//applies normalization formula to each value 
					
					//int normValue = (int) this.normalizeValue(values.get(i),this.getMaxColumn(i),this.getMinColumn(i));
					int normValue = (int) this.normalizeValueLog(values.get(i),this.getMaxColumn(i),this.getMinColumn(i));
					
					values.set(i, normValue);
				}
				
				results.put(file, values);
			}
		}
		
		//generates csv file
		public void generateCSVFile() throws IOException{
			FileWriter writer = new FileWriter(OUTPUT_FILENAME);
			Iterator<Entry<MFile, List<Integer>>> it = results.entrySet().iterator();
			
			writer.append("MFile");
			writer.append(";");
			writer.append("Domain");
			writer.append(";");
			writer.append("Toolbox");
			writer.append(";");
			
			for(int i = 0; i < concernNames.length; i++){
				writer.append(concernNames[i]);
				writer.append(";");
			}
			
			writer.append("LoC");
			writer.append("\n");
			
			while(it.hasNext()){
				
				Entry<MFile,List<Integer>> entry = it.next();
				MFile file = entry.getKey();
				List<Integer> values = entry.getValue();
				
				writer.append(file.getName());
				writer.append(";");
				writer.append(file.getDomain());
				writer.append(";");
				writer.append(file.getToolboxName());
				writer.append(";");
				
				for(int j = 0; j < values.size(); j++){
					writer.append(String.valueOf(values.get(j)));
					writer.append(";");
				}
				
				writer.append(String.valueOf(file.nrLines()));
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		}
		
		private void getNumberOfConcernsPerFile() throws IOException{
			FileWriter writer = new FileWriter(OUTPUT_FILENAME_2);
			Iterator<Entry<MFile, List<Integer>>> it = results.entrySet().iterator();
			
			writer.append("MFile");
			writer.append(";");
			writer.append("No. of Concerns");
			writer.append(";");
			writer.append("\n");
			
			while(it.hasNext()){
				
				Entry<MFile,List<Integer>> entry = it.next();
				MFile file = entry.getKey();
				List<Integer> values = entry.getValue();
				int noZeros = Collections.frequency(values, 0);
				
				writer.append(file.getName());
				writer.append(";");
				writer.append(String.valueOf(values.size()-noZeros));
				writer.append(";");
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		}
		
		private void generateDistinctTokensFile() throws IOException{
			FileWriter writer = new FileWriter(OUTPUT_FILENAME_3);
			Iterator<Entry<MFile, List<Integer>>> it = results.entrySet().iterator();
			
			writer.append("MFile");
			writer.append(";");
			writer.append("Domain");
			writer.append(";");
			writer.append("Toolbox");
			writer.append(";");
			
			for(int i = 0; i < concernNames.length; i++){
				writer.append(concernNames[i]);
				writer.append(";");
			}
			
			writer.append("LoC");
			writer.append("\n");
			
			while(it.hasNext()){
			
				Entry<MFile,List<Integer>> entry = it.next();
				MFile mFile = entry.getKey();
									
				writer.append(mFile.getName());
				writer.append(";");
				writer.append(mFile.getDomain());
				writer.append(";");
				writer.append(mFile.getToolboxName());
				writer.append(";");
					
				for(int j = 0; j < concernNames.length; j++){
						
					String currentConcern = concernNames[j];
						
					String[] distinctTokens = cf.getDistinctTokensPerConcern(mFile, table, currentConcern);
						
					writer.append(Arrays.toString(distinctTokens));
					writer.append(";");
				}
				writer.append(String.valueOf(mFile.nrLines()));
				writer.append("\n");
			}
		}					
		
		public static void main(String[] args) throws FileNotFoundException, IOException{
			ConcernMetricTable tmt = new ConcernMetricTable();
					
			tmt.buildMetricTable();
			/*tmt.buildMetricTableNormByDensity();
			
			tmt.normalizeTable();*/
			
			Map<MFile, List<Integer>> results = tmt.getResults();
			
			System.out.println("No. Of Files: " + results.size());
			
			System.out.println("Max Values: " + Arrays.toString(maxColumns));
			System.out.println("Min Values: " + Arrays.toString(minColumns));
					
			/*tmt.generateCSVFile();
			tmt.generateDistinctTokensFile();
			tmt.getNumberOfConcernsPerFile();*/
		}
	}