function [b,params] = glmfitting(x,y,params)
%GLMFITTING Fitting an exponential family model to data.
%   [B,PARAMS] = GLMFITTING(X,Y,PARAMS) fits an exponential
%   distribution specified in DISTRIB to the predictor matrix X and the
%   predictand Y, using link function LINK. The rows of x contain
%   individual observations, the columns are the values of individual
%   predictors. Input parameters are specified in fields of PARAMS:
%
%   distrib:
%    'norm'  - Normal (Gaussian) distribution. This is equivalent to simple
%              least squares fitting. The estimated dispersion phi is the
%              standard deviation of the distribution.
%    'gam'   - Gamma distribution. The estimated dispersion phi is the
%              inverse of the shape parameter of this distribution.
%    'binom' - Binomial distribution. For the Bernoulli distribution, the
%              vector y must contain only one column of binary (0/1) data.
%              For binomial trials, y must be two column with the second
%              column containing the number of trials for each observation.
%   link:
%    'id'    - Identity function g(mu)=mu
%    'recip' - Reciprocal function g(mu)=1/mu
%    'log'   - Log function g(mu)=log(mu)
%    'logit' - Logit function g(mu)=log(mu/(1-mu))
%
%   The returned structure PARAMS is augmented with the following fields:
%    phi       - Estimated dispersion parameter
%    df        - Degrees of freedom
%    dfe       - Error degrees of freedom
%    BIC       - Value of the Bayesian information criteria for the fit
%    deviance  - Deviance of the fit
%    residdev  - Deviance residuals
%    residsq   - Squared residuals
%    residansc - Anscombe residuals
%
% (cc) Max Little, 2008. This software is licensed under the
% Attribution-Share Alike 2.5 Generic Creative Commons license:
% http://creativecommons.org/licenses/by-sa/2.5/
% If you use this work, please cite:
% Little MA et al. (2008), "Parsimonious Modeling of UK Daily Rainfall for
% Density Forecasting", in Geophysical Research Abstracts, EGU General
% Assembly, Vienna 2008, Volume 10.

[n,m] = size(x);

% Check inputs
if (size(x,1) ~= size(y,1))
    error('x and y must have the same number of rows');
end

% Select link functions
switch params.link
    case 'id'
        linkfun = @(mu) mu;
        linkinvfun = @(eta) eta;
        linkderivfun = @(mu) ones(size(mu));
        
    case 'recip'
        linkfun = @(mu) 1./mu;
        linkinvfun = @(eta) 1./eta;
        linkderivfun = @(mu) -1./(mu.^2);

    case 'log'
        linkfun = @(mu) log(mu);
        linkinvfun = @(eta) exp(eta);
        linkderivfun = @(mu) 1./mu;

    case 'logit'
        linkfun = @(mu) log(mu./(1-mu));
        linkinvfun = @(eta) exp(eta)./(1+exp(eta));
        linkderivfun = @(mu) 1./(mu.*(1-mu));
        
    otherwise
        error('Link function not recognised');
end


% Select distribution functions
switch params.distrib
    case 'norm'
        varfun = @(mu) ones(size(mu));
        devfun = @(mu,y) (y-mu).^2;
        sqresidfun = @(mu,y) (y-mu).^2;
        anscresidfun = @(mu,y) y-mu;
        initfun = @(y) y;
        if (size(y,2) ~= 1)
            error('y must have only one column');
        end
        
    case 'gam'
        varfun = @(mu) mu.^2;
        devfun = @(mu,y) 2*(-log(y./mu)+(y-mu)./mu);
        sqresidfun = @(mu,y) ((y-mu)./mu).^2;
        anscresidfun = @(mu,y) 3*(y.^(1/3)-mu.^(1/3))./mu.^(1/3);
        initfun = @(y) y;
        if (size(y,2) ~= 1)
            error('y must have only one column');
        end
        if (any(y <= 0))
            error('One or more y outside support of distribution');
        end
        
    case 'binom'
        if (size(y,2) == 1)
            trials = 1;
        elseif (size(y,2) == 2)
            trials = y(:,2);
            y = y(:,1)./trials;
        else
            error('y must have one or two columns for binomial distribution');
        end
        if (any((y < 0) | (y > 1)))
            error('One or more y outside support of distribution');
        end
        varfun = @(mu) mu.*(1-mu)./trials;
        devfun = @(mu,y) 2*trials.*(y.*log((y+(y==0))./mu)+(1-y).*log((1-y+(y==1))./(1-mu)));
        sqresidfun = @(mu,y) (y-mu).^2./(mu.*(1-mu)./trials);
        anscresidfun = @(mu,y) beta(2/3,2/3)*(betainc(y,2/3,2/3)-betainc(mu,2/3,2/3))./((mu.*(1-mu)).^(1/6)./sqrt(trials));
        initfun = @(y) (trials.*y+0.5)./(trials+1);

    otherwise
        error('Distribution not recognised');
end

% Augment input variables with offset column
X = [ones(n,1) x];

% Initialize mu
mu = initfun(y);
eta = linkfun(mu);

% Perform IRLS iterations
devnew = 1;
deltadev = 1;
stoptolerance = 1e-6;
maxiterations = 100;
iter = 0;
while ((abs(deltadev) > stoptolerance) && (iter < maxiterations))
    etaderiv = linkderivfun(mu);
    W = sparse(1:n,1:n,1./((etaderiv.^2).*varfun(mu)));
    z = (y-mu).*etaderiv+eta;
    b = (X'*W*X)\(X'*W*z);
    eta = X*b;
    mu = linkinvfun(eta);
    devold = devnew;
    devnew = sum(devfun(mu,y));
    deltadev = devnew-devold;
    iter = iter+1;
end
if (iter == maxiterations)
    warning('IRLS did not converge within maximum number of iterations');
end

% Calculate final statistics
residdev  = devfun(mu,y);
residsq   = sqresidfun(mu,y);
residansc = anscresidfun(mu,y);

df = m+1;
dferror = max(n-m,0);
deviance = sum(residdev);
BIC = df*log(n)+deviance;
sqsumresid = sum(residsq);

params.phi = sqsumresid/dferror;
params.df  = df;
params.dfe = dferror;
params.BIC = BIC;
params.deviance  = deviance;
params.residdev  = residdev;
params.residsq   = residsq;
params.residansc = residansc;
