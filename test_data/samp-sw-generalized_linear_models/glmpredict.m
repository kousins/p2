function muhat = glmpredict(x,b,params)
%GLMPREDICT Predict using an exponential family model to data.
%   MUHAT = GLMPREDICT(X,B,PARAMS) predicts the mean values MUHAT using a
%   fitted exponential family model obtained using GLMFITTING. The matrix X
%   contains the predictors, the vector B the coefficients of the fit, and
%   PARAMS the parameters of the exponential family model. Input
%   parameters are X the matrix of predictors. Each row is an observation,
%   each column a variable. For a vector B of m coefficients, X must have
%   (m-1) columns.
%
%   The PARAMS structure should contain the following fields:
%   link:
%    'id'    - Identity function g(mu)=mu
%    'recip' - Reciprocal function g(mu)=1/mu
%    'log'   - Log function g(mu)=log(mu)
%    'logit' - Logit function g(mu)=log(mu/(1-mu))
%
% (cc) Max Little, 2008. This software is licensed under the
% Attribution-Share Alike 2.5 Generic Creative Commons license:
% http://creativecommons.org/licenses/by-sa/2.5/
% If you use this work, please cite:
% Little MA et al. (2008), "Parsimonious Modeling of UK Daily Rainfall for
% Density Forecasting", in Geophysical Research Abstracts, EGU General
% Assembly, Vienna 2008, Volume 10.

[n,m] = size(x);

if (m ~= (length(b)-1))
    error('x must have (m-1) columns, where m is the number of elements of b');
end

% Select link functions
switch params.link
    case 'id'
        linkinvfun = @(eta) eta;
        
    case 'recip'
        linkinvfun = @(eta) 1./eta;

    case 'log'
        linkinvfun = @(eta) exp(eta);

    case 'logit'
        linkinvfun = @(eta) exp(eta)./(1+exp(eta));
        
    otherwise
        error('Link function not recognised');
end

% Augment input variables with offset column
X = [ones(n,1) x];

muhat = linkinvfun(X*b);
