%GLMTEST Demonstrate fitting an exponential family model to data.
%   This test routine generates a set of data with known linear predictor
%   and link function, and tests the accuracy of GLMFITTING at being able
%   to find the given linear predictor coefficients.
%
% (cc) Max Little, 2008. This software is licensed under the
% Attribution-Share Alike 2.5 Generic Creative Commons license:
% http://creativecommons.org/licenses/by-sa/2.5/
% If you use this work, please cite:
% Little MA et al. (2008), "Parsimonious Modeling of UK Daily Rainfall for
% Density Forecasting", in Geophysical Research Abstracts, EGU General
% Assembly, Vienna 2008, Volume 10.

clear all;

% Number of points in data set
n = 300;

% Set up predictor variable
x = linspace(5, 10, n)';

% Calculate predictands eta with particular coefficients
coeffs = [-2.87 1.3];
eta = x*coeffs(2)+coeffs(1);

% Apply inverse link function to predictands to obtain exponential family
% mean
mu = 1./eta;

% Choose gamma shape parameter
shape = 10.0;
scale = mu/shape;

% Generate a series of gamma distributed values with the given mean values
% calculated above
y = gamrnd(shape,scale);

% Set up parameters for gamma GLM fit
params.distrib = 'gam';
params.link = 'recip';
[b,params] = glmfitting(x,y,params);

% Plot results
close all;
hold on;
plot(x,y,'k.');
xp = linspace(3,12,100)';
muhat = glmpredict(xp,b,params);
plot(xp,muhat,'b-');
axis tight;
v = axis;
xlabel('Predictors x');
ylabel('Predictands \mu');
legend({'Data','GLM model'});
title('Gamma GLM with reciprocal link function');
tx = (v(2)-v(1))*0.6+v(1);
ty1 = (v(4)-v(3))*0.70+v(3);
ty2 = (v(4)-v(3))*0.65+v(3);
text(tx,ty1,sprintf('Actual coeffs: %0.2f, %0.2f',coeffs(1),coeffs(2)));
text(tx,ty2,sprintf('Fitted coeffs: %0.2f, %0.2f',b(1),b(2)));
