#UbiSOM-Library

##Documentation
The folder **doc/** contains the Javadoc for the supported classes in this library.

##Usage
The following code illustrates how to create/save instances of the UbiSOM algorithm.

```

	public static void main(String[] args) {
        //create an instance from hard-coded parameters
        UbiSOM3 ubisom = UbiSOMFactory.create(
                20, //width
                40, //height
                2, //data dimensionality
                0.1, //eta_0
                0.08, //eta_f
                0.6, //sigma_0
                0.2, //sigma_f
                0.7, //beta
                2000 //T
        );
        
        for(int i=0; i<100; i++) {            
            double[] observation = new double[]{Math.random(),Math.random()};
            ubisom.learn(observation);
        }
        
        //save (serialize) instance
        ubisom.serialize("test.dat");
        
        //read from serialization file
        ubisom = UbiSOMFactory.loadFromSave("test.dat");
        
        for(int i=0; i<100; i++) {
            double[] observation = new double[]{Math.random(),Math.random()};
            ubisom.learn(observation);
        }
        
        //create from properties-file containing parameter values        
        UbiSOM3 ubisom2 = UbiSOMFactory.loadFromProperties("ubisom.conf");
        
        for(int i=0; i<100; i++) {
            double[] observation = new double[]{Math.random(),Math.random()};
            ubisom2.learn(observation);
        }
        
        System.out.println(ubisom2);
        
    }
    
```

##Author
The author can be contacted at *brunomnsilva@gmail.com*. 
