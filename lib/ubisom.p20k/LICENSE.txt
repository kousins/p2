                     LICENSE AGREEMENT
-----------------------------------------------------
The author can be contacted at brunomnsilva@gmail.com
-----------------------------------------------------

By using UbiSOM-Library you agree with the following: 

1. UbiSOM-Library ("the Software") is provided without any warranty.

2. You may use the library to create derivative works, as long as you attach
a copy of this license and provide attribution to the author. If used for 
research/academic purposes you should cite the following article:

Silva, Bruno, and Nuno Cavalheiro Marques. "The ubiquitous self-organizing map for non-stationary data streams." Journal of Big Data 2.1 (2015): 1-22.

3. Restricted Uses:

3.1. No Distribution, etc. You may not distribute, license, loan, or sell the Software or other content that is contained or displayed in it. An exception is made when (2) applies (when your derived work uses the Software as a library); you must, however, comply with (2).

3.2. No Reverse Engineering. You may not reverse engineer, decompile, decode, decrypt, disassemble, or derive any source code from the Software.

3.3. Proprietary Notices. You may not remove, alter, or obscure any copyright, trademark, or other proprietary rights notice on or in the Software.

