package multiSOM;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestMultiSOM {

  @Rule
  public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

  @Test
  public void testExecCmd() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    multi.cmd = "";
    multi.exec_cmd();
    assertEquals("--Ignoring empty ln\n", systemOutRule.getLog());
  }

  @Test
  public void testExecCmdText() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    multi.cmd = "text";
    multi.exec_cmd();
    assertEquals(10000, multi.show_msg);
    assertEquals(multi.notification_msg, multi.txtArg);
  }

  @Test
  public void testExecCmdCatlabel() {
    MultiSOM multi = Mockito.spy(new MultiSOM());

    multi.setup();
    multi.setup_SOM();
    multi.setup_server();
    multi.cmd = "catlabel";
    multi.exec_cmd();
    assertEquals("\n", systemOutRule.getLog());
    assertEquals(null, multi.curr_axis_img);
  }
}
