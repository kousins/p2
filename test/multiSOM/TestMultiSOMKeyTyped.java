package multiSOM;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestMultiSOMKeyTyped {

  @Mock
  public char key;

  // @InjectMocks
  // public MultiSOM multi = new MultiSOM();

  @Test
  public void testKeyTypedM() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('M');
    multi.keyTyped();
    assertEquals(true, multi.show_umat_flag);
    multi.keyTyped();
    assertEquals(false, multi.show_umat_flag);
  }

  @Test
  public void testKeyTypedA() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('A');
    multi.keyTyped();
    assertEquals(false, multi.show_axis);
    multi.keyTyped();
    assertEquals(true, multi.show_axis);
  }

  /**
   *  axis_cursor = -1 by default
   *  gridPt_cursor = -1;
   *  gridPt is a inherited array field
   */
  @Test
  public void testKeyTypedl() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('l');
    multi.keyTyped();
    int gridPt_cursor = multi.gridPt_cursor;
    assertEquals(59, multi.axis_cursor);
    assertEquals(gridPt_cursor - 1, multi.gridPt_cursor);

    multi.axis_cursor = 10;
    multi.keyTyped();
    assertEquals(9, multi.axis_cursor);
    assertEquals(gridPt_cursor - 1, multi.gridPt_cursor);

    multi.keyTyped();
    multi.gridPt_cursor = 4;
    assertEquals(8, multi.axis_cursor);
    assertEquals(5, multi.axis_cursor);
  }

  @Test
  public void testKeyTypedP() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('P');
    multi.keyTyped();
    assertEquals(true, multi.pause_network);
    assertEquals("Pause on Network Information.", multi.notification_msg);

    multi.keyTyped();
    assertEquals(false, multi.pause_network);
    assertEquals("Activating Network Information.", multi.notification_msg);
  }

  @Test
  public void testKeyTypedp() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('p');
    multi.keyTyped();
    assertEquals(multi.gridPt.length - 1, multi.gridPt_cursor);

    multi.gridPt_cursor = 10;
    multi.keyTyped();
    assertEquals(9, multi.gridPt_cursor);
  }

  @Test
  public void testKeyTypedo() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('o');
    multi.keyTyped();
    assertEquals(0, multi.gridPt_cursor);

    multi.gridPt_cursor = multi.gridPt.length - 1;
    multi.keyTyped();
    assertEquals(0, multi.gridPt_cursor);
  }

  @Test
  public void testKeyTypedh() {
    MultiSOM multi = Mockito.spy(new MultiSOM());
    when(key).thenReturn('h');
    multi.keyTyped();
    assertEquals(0, multi.axis_cursor);
    assertEquals(-1, multi.gridPt_cursor);

    multi.axis_cursor = 59;
    int gridPt_cursor = multi.gridPt_cursor;
    multi.keyTyped();
    assertEquals(0, multi.axis_cursor);
    assertEquals(gridPt_cursor + 1, multi.gridPt_cursor);

    multi.axis_cursor = 59;
    multi.gridPt_cursor = multi.gridPt.length - 1;
    multi.keyTyped();
    assertEquals(0, multi.axis_cursor);
    assertEquals(-1, multi.gridPt_cursor);
  }
}
