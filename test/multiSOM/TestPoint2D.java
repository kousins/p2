package multiSOM;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

public class TestPoint2D {

	private static final double DELTA = 1e-15;

    @Test
    public void testConstructor() {
      Point2D p = new Point2D();

      assertEquals(null, p.x0);
      assertEquals(null, p.y0);
      assertEquals(null, p.iteration);
      assertEquals(null, p.f_x0);
      assertEquals(null, p.f_y0);
    }

    @Test
    public void testConstructor1() {
      Point2D p = new Point2D(323, 5, 60);

      assertEquals((int)323, (int)p.x0);
      assertEquals((int)5, (int)p.y0);
      assertEquals((int)60, (int)p.iteration);
      assertEquals(null, p.f_x0);
      assertEquals(null, p.f_y0);
    }

    @Test
    public void testConstructor2() {
      Point2D p = new Point2D(323, 5);

      assertEquals((int)323, (int)p.x0);
      assertEquals((int)5, (int)p.y0);
      assertEquals(null, p.iteration);
      assertEquals(null, p.f_x0);
      assertEquals(null, p.f_y0);
    }

    @SuppressWarnings("deprecation")
	@Test
    public void testFloatConstructor() {
      Point2D p = new Point2D(11.34f, 65.9f);

      assertEquals(11.34f, p.f_x0, DELTA);
      assertEquals(65.9f, p.f_y0, DELTA);
      assertEquals(null, p.iteration);
      assertEquals(null, p.x0);
      assertEquals(null, p.y0);
    }

    @Test
    public void testToString() {
      Point2D p = new Point2D(323, 5, 60);
      Point2D f_p = new Point2D(11.34f, 65.9f);
      assertEquals("(323, 5)", p.toString());
      assertEquals("(11.34, 65.9)", f_p.toString());
    }

    @SuppressWarnings("deprecation")
	@Test
    public void testGetters() {
      Point2D p = new Point2D(323, 5, 60);
      Point2D f_p = new Point2D(11.34f, 65.9f);
      assertEquals(60, p.getIteration());
      assertEquals(323, p.getX());
      assertEquals(5, p.getY());
      assertEquals(11.34f, f_p.getfX(), DELTA);
      assertEquals(65.9f, f_p.getfY(), DELTA);
    }

    @SuppressWarnings("deprecation")
	@Test
    public void testSetters() {
      Point2D p = new Point2D();
      Point2D f_p = new Point2D();
      p.setX(44);
      p.setY(6);
      f_p.setfX(11.34f);
      f_p.setfY(65.9f);
      assertEquals(44, p.getX());
      assertEquals(6, p.getY());
      assertEquals(11.34f, f_p.getfX(), DELTA);
      assertEquals(65.9f, f_p.getfY(), DELTA);
    }
}
