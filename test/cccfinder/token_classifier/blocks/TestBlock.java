package cccfinder.token_classifier.blocks;

import static org.junit.Assert.*;

import org.junit.Test;

import cccfinder.token_classifier.blocks.Block;
import cccfinder.token_classifier.blocks.BlockSeq;
import cccfinder.token_classifier.blocks.Token4Output;
import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Tag;


import java.util.ArrayList;
import java.util.List;

public class TestBlock {
	@Test
	public void testNew() {
		List<Token4Output> pairs = new ArrayList<Token4Output>();
		pairs.add(BlockSeq.makeToken("one", Tag.ARG));
		pairs.add(BlockSeq.makeToken("two", Tag.VAR));
		Block block = new Block("proc1.m", pairs);
		assertEquals(2, block.size());
		assertEquals("proc1.m ARG:one VAR:two", block.toString());
	}

	@Test
	public void testClone() {
		List<Token4Output> pairs = new ArrayList<Token4Output>();
		pairs.add(BlockSeq.makeToken("one", Tag.ARG));
		pairs.add(BlockSeq.makeToken("two", Tag.VAR));
		Block block = new Block("proc1.m", pairs);

		Block clone = block.clone();
		assertNotSame(clone, block);
		assertEquals(block.size(), clone.size());
		assertEquals("proc1.m ARG:one VAR:two", clone.toString());
	}
	@Test
	public void testToken4OutputClone() {
		Token4Output tok = new Token4Output(LexicalElem.ID, "someword");
		tok.setTag(Tag.VAR);
		Token4Output clone = tok.clone();
		assertNotSame(tok, clone);
		assertEquals(tok.getValue(), clone.getValue());
		assertNotSame(tok.getValue(), clone.getValue());
		assertEquals(tok.type(), clone.type());
		assertEquals(tok.getTag(), clone.getTag());
		assertEquals("VAR:someword", tok.toString());
	}
}
