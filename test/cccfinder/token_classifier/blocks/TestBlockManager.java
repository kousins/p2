package cccfinder.token_classifier.blocks;

import static org.junit.Assert.*;
import org.junit.Test;

import cccfinder.token_classifier.blocks.BlockManager;
import cccfinder.token_classifier.lexical_analyser.Tag;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;


public class TestBlockManager {
	public static final String TESTING_TOOLBOX = "testing";

	@Test
	public void testBMnew() {
		BlockManager bm = new BlockManager("test_domain", "tb1");
		assertEquals(0, bm.size());
		assertEquals("test_domain", bm.currentDomain());
		assertEquals("tb1", bm.currentToolbox());
	}

	@Test
	public void testRegister() {
		BlockManager bm = new BlockManager("test_domain", TESTING_TOOLBOX);
		assertEquals(0, bm.size());
		bm.mark("one", Tag.FHD);
		assertEquals(1, bm.size());
		bm.mark("two", Tag.ARG);
		assertEquals(2, bm.size());
	}

	@Test
	public void testMFileChange() {
		BlockManager bm = new BlockManager("test_domain", TESTING_TOOLBOX);
		bm.setMFile("mfile1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.VAR);
		bm.newMFile("mfile2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.VAR);
	}

	@Test
	public void testToolboxChange() {
		BlockManager bm = new BlockManager("domain1", "tb1");
		assertEquals("tb1", bm.currentToolbox());
		bm.setMFile("mfile1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		bm.newMFile("mfile2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.VAR);
		bm.newMFile("mfile3.m");
		bm.mark("five", Tag.FHD);
		bm.mark("six", Tag.VAR);
		bm.mark("end", Tag.KEY); // must change block here
		bm.mark("eight", Tag.VAR);
		bm.mark("nine", Tag.VAR);
		bm.newToolbox("tb2");
		assertEquals("tb2", bm.currentToolbox());
		bm.mark("eleven", Tag.FHD);
		bm.mark("twelve", Tag.VAR);
		bm.mark("end", Tag.KEY); // must change block here
		bm.mark("thirteen", Tag.VAR);
		bm.mark("fourteen", Tag.VAR);
		assertEquals(14, bm.size());
		assertEquals(6, bm.nrBlocks());
	}

	@Test
	public void testSize() {
		BlockManager bm = new BlockManager("domain_test", "tb1");
		assertEquals(0, bm.size());

		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		assertEquals(2, bm.size());

		bm.newMFile("mf2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.FUN);
		assertEquals(4, bm.size());

		bm.newToolbox("tb2");
		assertEquals(4, bm.size());

		bm.setMFile("mf3.m");
		bm.mark("five", Tag.FHD);
		bm.mark("six", Tag.FUN);
		bm.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		assertEquals(7, bm.size());

		bm.mark("seven", Tag.VAR);
		bm.mark("eight", Tag.VAR);
		assertEquals(9, bm.size());

		bm.toString();
		assertEquals(9, bm.size());

		bm.newDomain("domain_2");
		assertEquals(9, bm.size());

		bm.newToolbox("tb3");
		bm.setMFile("mf4.m");
		assertEquals(9, bm.size());

		bm.mark("nine", Tag.FHD);
		bm.mark("ten", Tag.FUN);
		assertEquals(11, bm.size());

		bm.toString();
		assertEquals(11, bm.size());
	}

	@Test
	public void testNrBlocks() {
		BlockManager bm = new BlockManager("domain_test", "tb1");
		assertEquals(0, bm.nrBlocks());

		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		assertEquals(1, bm.nrBlocks());

		bm.newMFile("mf2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.FUN);
		assertEquals(2, bm.nrBlocks());

		bm.newToolbox("tb2");
		assertEquals(2, bm.nrBlocks());

		bm.setMFile("mf3.m");
		assertEquals(2, bm.nrBlocks());
		bm.mark("five", Tag.VAR);
		assertEquals(3, bm.nrBlocks());

		bm.mark("six", Tag.VAR);
		bm.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		assertEquals(3, bm.nrBlocks());

		bm.mark("seven", Tag.VAR);
		assertEquals(4, bm.nrBlocks());
		bm.mark("eight", Tag.VAR);
		assertEquals(4, bm.nrBlocks());

		bm.toString();
		assertEquals(4, bm.nrBlocks());

		bm.newDomain("domain_2");
		assertEquals(4, bm.nrBlocks());

		bm.newToolbox("tb3");
		bm.setMFile("mf4.m");
		assertEquals(4, bm.nrBlocks());

		bm.mark("nine", Tag.VAR);
		bm.mark("ten", Tag.VAR);
		assertEquals(5, bm.nrBlocks());

		bm.toString();
		assertEquals(5, bm.nrBlocks());
	}

	@Test
	public void testToString0() {
		BlockManager bm = new BlockManager("domain", "toolbox");
		assertEquals("", bm.toString());
	}

	@Test
	public void testToString() {
		BlockManager bm = new BlockManager("domain_test", "tb1");
		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		bm.newMFile("mf2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.FUN);
		bm.newToolbox("tb2");
		assertEquals("tb2", bm.currentToolbox());

		bm.setMFile("mf3.m");
		bm.mark("five", Tag.FHD);
		bm.mark("six", Tag.ARG);
		bm.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		bm.mark("seven", Tag.VAR);
		bm.mark("eight", Tag.VAR);
		String actual = bm.toString();
		String expected =
			"domain_test tb1 mf1.m FHD:one ARG:two\n" +
			"domain_test tb1 mf2.m FHD:three FUN:four\n" +
			"domain_test tb2 mf3.m FHD:five ARG:six KEY:end\n" +
			"domain_test tb2 mf3.m VAR:seven VAR:eight\n";
		assertEquals(expected, actual);

		bm.newDomain("domain2");
		actual = bm.toString();
		assertEquals(expected, actual);
	}

	@Test
	public void testToString2() {
		BlockManager bm = new BlockManager("domain1", "tb1");
		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		bm.newMFile("mf2.m");
		bm.mark("three", Tag.ARG);
		bm.mark("four", Tag.FHD);
		bm.newToolbox("tb2");
		bm.setMFile("mf3.m");
		bm.mark("five", Tag.FHD);
		bm.mark("six", Tag.VAR);
		bm.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		bm.mark("seven", Tag.VAR);
		bm.mark("eight", Tag.VAR);
		bm.newDomain("domain2");
		bm.newToolbox("tb3");
		bm.setMFile("mf4.m");
		bm.mark("nine", Tag.ARG);
		bm.mark("ten", Tag.FHD);
		String actual = bm.toString();
		String expected =
			"domain1 tb1 mf1.m FHD:one ARG:two\n" +
			"domain1 tb1 mf2.m ARG:three FHD:four\n" +
			"domain1 tb2 mf3.m FHD:five VAR:six KEY:end\n" +
			"domain1 tb2 mf3.m VAR:seven VAR:eight\n" +
			"domain2 tb3 mf4.m ARG:nine FHD:ten\n";
		actual = bm.toString();
		assertEquals(expected, actual);
	}

	@Test
	public void testPrint1() throws UnsupportedEncodingException {
		BlockManager bm = new BlockManager("domain1", "tb1");
		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.ARG);
		bm.newMFile("mf2.m");
		bm.mark("three", Tag.ARG);
		bm.mark("four", Tag.FHD);
		bm.newToolbox("tb2");
		bm.setMFile("mf3.m");
		bm.mark("five", Tag.FHD);
		bm.mark("six", Tag.ARG);
		bm.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		bm.mark("seven", Tag.VAR);
		bm.mark("eight", Tag.VAR);
		bm.newDomain("domain2");
		bm.newToolbox("tb3");
		bm.setMFile("mf4.m");
		bm.mark("nine", Tag.VAR);
		bm.mark("ten", Tag.VAR);
		String expected = 
			"domain1 tb1 mf1.m FHD:one ARG:two\r\n" +
			"domain1 tb1 mf2.m ARG:three FHD:four\r\n" +
			"domain1 tb2 mf3.m FHD:five ARG:six KEY:end\r\n" +
			"domain1 tb2 mf3.m VAR:seven VAR:eight\r\n" +
			"domain2 tb3 mf4.m VAR:nine VAR:ten\r\n";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		bm.print(ps);
		String actual = baos.toString();

		assertEquals(expected, actual);
		assertEquals(expected.length(), actual.length());
	}

	@Test
	public void testClone() {
		BlockManager original = new BlockManager("domain1", "tb1");
		original.setMFile("mf1.m");
		original.mark("one", Tag.FHD);
		original.mark("two", Tag.ARG);
		original.newMFile("mf2.m");
		original.mark("three", Tag.ARG);
		original.mark("four", Tag.FHD);
		original.newToolbox("tb2");
		original.setMFile("mf3.m");
		original.mark("five", Tag.FHD);
		original.mark("six", Tag.ARG);
		original.mark("end", Tag.KEY); //'end' marks end of a block but is not registered
		original.mark("seven", Tag.VAR);
		original.mark("eight", Tag.VAR);
		original.newDomain("domain2");
		original.newToolbox("tb3");
		original.setMFile("mf4.m");
		original.mark("nine", Tag.VAR);
		original.mark("ten", Tag.VAR);

		BlockManager clone = original.clone();
		assertNotSame(original, clone);
		assertEquals(original.currentDomain(), clone.currentDomain());
		assertNotSame(original.currentDomain(), clone.currentDomain());
		assertEquals(original.currentToolbox(), clone.currentToolbox());
		assertNotSame(original.currentToolbox(), clone.currentToolbox());
		assertEquals(original.nrBlocks(), clone.nrBlocks());
		assertEquals(original.nrDomains(), clone.nrDomains());
		assertEquals(original.size(), clone.size());
		assertEquals(original.toString(), clone.toString());
	}
}
