package cccfinder.token_classifier.blocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import org.junit.Test;

import cccfinder.token_classifier.blocks.BlockSeq;
import cccfinder.token_classifier.lexical_analyser.Tag;

public class TestBlockSeq {
	@Test
	public void testBlockSeqNew() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		assertEquals(0, blockSeq.nrBlocks());
		assertEquals(0, blockSeq.size());
		assertEquals(BlockSeq.UNKNOWN_MFILE, blockSeq.currentMFile());
	}

	@Test
	public void testRegistrations() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		blockSeq.setMFile("proc1.m");
		assertEquals(0, blockSeq.size());
		blockSeq.mark("one", Tag.FHD);
		assertEquals(1, blockSeq.size());
		blockSeq.mark("two", Tag.ARG);
		assertEquals(2, blockSeq.size());
		assertEquals("proc1.m", blockSeq.currentMFile());
	}

	@Test
	public void testMFileChanges() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		assertEquals(0, blockSeq.nrBlocks());

		blockSeq.setMFile("proc1.m");
		blockSeq.mark("func1", Tag.FUN);
		blockSeq.mark("param1", Tag.ARG);
		assertEquals(2, blockSeq.size());
		assertEquals("proc1.m", blockSeq.currentMFile());

		blockSeq.newMFile("proc2.m");

		blockSeq.mark("three", Tag.VAR);
		blockSeq.mark("four", Tag.VAR);
		assertEquals(4, blockSeq.size());

		blockSeq.newMFile("proc3.m");
		blockSeq.mark("five", Tag.VAR);
		blockSeq.mark("while", Tag.KEY);
		assertEquals(6, blockSeq.size());
	}

	@Test
	public void testMFileChangeOnEmpty() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		assertEquals(0, blockSeq.nrBlocks());
		blockSeq.newMFile("second.m"); // No token was registered
		assertEquals(0, blockSeq.nrBlocks());
	}

	@Test
	public void testRegisterEnd() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		blockSeq.setMFile("proc1.m");
		blockSeq.mark("one", Tag.ARG);
		blockSeq.mark("two", Tag.FHD);
		blockSeq.mark("end", Tag.KEY); // must change block here
		assertEquals(1, blockSeq.nrBlocks());
		blockSeq.mark("three", Tag.VAR);
		blockSeq.mark("four", Tag.VAR);
		assertEquals(2, blockSeq.nrBlocks());
		blockSeq.newMFile("proc2.m");
		assertEquals(2, blockSeq.nrBlocks());
		blockSeq.mark("five", Tag.FHD);
		blockSeq.mark("six", Tag.ARG);
		blockSeq.mark("end", Tag.KEY); // must change block here

		assertEquals(3, blockSeq.nrBlocks());
		assertEquals(8, blockSeq.size());
	}

	@Test
	public void testNrBlocks() {
		BlockSeq bs = new BlockSeq("test_domain", "test_toolbox");
		assertEquals(0, bs.nrBlocks());
		bs.setMFile("proc1.m");
		assertEquals(0, bs.nrBlocks());
		bs.mark("one", Tag.FHD);
		bs.mark("two", Tag.ARG);
		assertEquals(1, bs.nrBlocks());
		bs.newMFile("proc2.m");
		assertEquals(1, bs.nrBlocks());
		bs.mark("three", Tag.FHD);
		bs.mark("four", Tag.VAR);
		assertEquals(2, bs.nrBlocks());
		bs.newMFile("proc3.m");
		assertEquals(2, bs.nrBlocks());
		bs.mark("five", Tag.FHD);
		bs.mark("six", Tag.VAR);
		assertEquals(3, bs.nrBlocks());
		bs.moveCurrentBlock();
		assertEquals(3, bs.nrBlocks());
	}

	@Test
	public void testToString1() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		blockSeq.setMFile("proc1.m");
		blockSeq.mark("one", Tag.ARG);
		blockSeq.mark("two", Tag.FHD);
		blockSeq.newMFile("proc2.m");
		blockSeq.mark("three", Tag.FHD);
		blockSeq.mark("four", Tag.ARG);
		blockSeq.newMFile("proc3.m");
		blockSeq.mark("five", Tag.FHD);
		blockSeq.mark("six", Tag.VAR);

		assertEquals(6, blockSeq.size());
		String expected =
				"test_domain test_toolbox proc1.m ARG:one FHD:two\n" +
				"test_domain test_toolbox proc2.m FHD:three ARG:four\n" +
				"test_domain test_toolbox proc3.m FHD:five VAR:six\n";
		String actual = blockSeq.toString();
		assertEquals(expected, actual);
	}

	@Test
	public void testToString2() {
		BlockSeq blockSeq = new BlockSeq("test_domain", "test_toolbox");
		blockSeq.setMFile("proc1.m");
		blockSeq.mark("one", Tag.FHD);
		blockSeq.mark("two", Tag.VAR);
		blockSeq.mark("end", Tag.KEY); //must change block here
		blockSeq.mark("three", Tag.VAR);
		blockSeq.mark("four", Tag.VAR);
		String expected =
				"test_domain test_toolbox proc1.m FHD:one VAR:two KEY:end\n" +
				"test_domain test_toolbox proc1.m VAR:three VAR:four\n";
		assertEquals(expected, blockSeq.toString());
	}

	@Test
	public void testClone() {
		BlockSeq original = new BlockSeq("test_domain", "test_toolbox");
		original.setMFile("proc1.m");
		original.mark("one", Tag.ARG);
		original.mark("two", Tag.FHD);
		original.newMFile("proc2.m");
		original.mark("three", Tag.FHD);
		original.mark("four", Tag.ARG);
		original.newMFile("proc3.m");
		original.mark("five", Tag.FHD);
		original.mark("six", Tag.VAR);

		BlockSeq clone = original.clone();

		assertNotSame(original, clone);
		assertEquals(original.size(), clone.size());
		String expected = "test_domain test_toolbox proc1.m ARG:one FHD:two\n"
				+ "test_domain test_toolbox proc2.m FHD:three ARG:four\n"
				+ "test_domain test_toolbox proc3.m FHD:five VAR:six\n";
		assertEquals(expected, clone.toString());
	}
}
