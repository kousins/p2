package cccfinder.token_classifier.blocks;

import org.junit.Test;

import cccfinder.token_classifier.blocks.BlockManager;
import cccfinder.token_classifier.lexical_analyser.Tag;
import static org.junit.Assert.assertEquals;

public class TestDomain {
	@Test
	public void testNewDomain1() {
		BlockManager bm = new BlockManager("domain_1", "tb1");
		assertEquals(0, bm.nrDomains());

		bm.setMFile("mf1.m");
		assertEquals(0, bm.nrDomains());
		bm.mark("one", Tag.ARG);
		assertEquals(1, bm.nrDomains());
		bm.newMFile("mf2.m");
		bm.mark("two", Tag.FHD);
		bm.newToolbox("tb2");
		bm.setMFile("mf3.m");
		bm.mark("three", Tag.ARG);
		bm.mark("end", Tag.FHD);
		bm.mark("four", Tag.VAR);
		assertEquals(1, bm.nrDomains());

		bm.newDomain("domain_2");
		assertEquals(1, bm.nrDomains());
		bm.mark("five", Tag.FHD);
		assertEquals(2, bm.nrDomains());
		bm.newDomain("domain_2"); //same domain
		assertEquals(2, bm.nrDomains());
		bm.mark("six", Tag.FHD);
		assertEquals(2, bm.nrDomains());

		bm.newDomain("domain_3");
		assertEquals(2, bm.nrDomains());
		bm.mark("six", Tag.FHD);
		assertEquals(3, bm.nrDomains());
	}

	@Test
	public void testNewDomain2() {
		BlockManager bm = new BlockManager("domain_1", "tb1");
		assertEquals("domain_1", bm.currentDomain());

		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		bm.mark("two", Tag.VAR);
		bm.newMFile("mf2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.ARG);
		bm.newToolbox("tb2");
		bm.setMFile("mf3.m");
		bm.mark("five", Tag.ARG);
		bm.mark("six", Tag.FHD);
		bm.mark("end", Tag.KEY);
		bm.mark("seven", Tag.VAR);
		bm.mark("eight", Tag.VAR);
		assertEquals("domain_1", bm.currentDomain());

		bm.newDomain("domain_2");
		assertEquals("domain_2", bm.currentDomain());
	}

	@Test
	public void testNewDomain3() {
		BlockManager bm = new BlockManager("domain_1", "tb1");
		assertEquals(0, bm.nrDomains());
		assertEquals("domain_1", bm.currentDomain());

		bm.setMFile("mf1.m");
		bm.mark("one", Tag.FHD);
		assertEquals(1, bm.nrDomains());
		bm.mark("two", Tag.VAR);
		bm.newDomain("domain_2");
		bm.newMFile("mf2.m");
		bm.mark("three", Tag.FHD);
		bm.mark("four", Tag.VAR);
		assertEquals(2, bm.nrDomains());
		bm.newDomain("domain_2"); //same domain
		assertEquals(2, bm.nrDomains());
		bm.mark("five", Tag.FHD);
		assertEquals(2, bm.nrDomains());
		bm.mark("six", Tag.VAR);

		bm.newDomain("domain_3");
		assertEquals(2, bm.nrDomains());
		bm.mark("seven", Tag.FHD);
		assertEquals(3, bm.nrDomains());
		assertEquals("domain_3", bm.currentDomain());
	}
}
