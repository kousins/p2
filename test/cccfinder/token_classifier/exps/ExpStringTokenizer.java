package cccfinder.token_classifier.exps;

import java.util.StringTokenizer;

public class ExpStringTokenizer {
	private static final String DELIMITER_CHARS = " ^&~![]{}()<>+-/%*;,:=\"";

	public static void main0(String[] args) {
		String line = "if nargin < 3, return; end";
		StringTokenizer st = new StringTokenizer(line, DELIMITER_CHARS);
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
	}

	public static void main1(String[] args) {
		String line = "  j = find(bigdom==outdom(i, 'haydn mozart beethoven'));";
		StringTokenizer st = new StringTokenizer(line, DELIMITER_CHARS);
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
	}
	public static void main(String[] args) {
		String text = "for i = 1:length(linkingedges)" + "\n" +
                       "\tspurdegree = connectioninfo(linkingedges(i));" + "\n" +
                       "\tif spurdegree" + "\n" +
                           "\t\tspurs = [spurs linkingedges(i)];" + "\n" +  
                           "\t\tlen = [len edgelistlength(edgelist{linkingedges(i)})];" + "\n" +  
                       "\tend" + "\n" +
                   "end";
		String[] lines = text.split("\n");
		for(String line : lines)
			System.out.println(line);
	}
}
