package cccfinder.token_classifier.token_table;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

import cccfinder.token_classifier.token_table.TokenTable;

public class TestTokenTable {
	private static final String TABLE_PATH = ".\\dat\\token_table.txt";

	@Test
	public void testHasConcerns() throws IOException {
		TokenTable table = new TokenTable(TABLE_PATH);
		assertTrue(table.hasConcern("Messages and monitoring"));
		assertTrue(table.hasConcern("I/O data"));
		assertTrue(table.hasConcern("Verification of function arguments and return values"));
		assertTrue(table.hasConcern("Data type verification and specialization"));
		assertTrue(table.hasConcern("System"));
		assertTrue(table.hasConcern("Memory allocation/deallocation"));
		assertTrue(table.hasConcern("System"));
		assertTrue(table.hasConcern("Parallelization"));

		assertFalse(table.hasConcern("Asterix"));
	}

	@Test
	public void testFrontierTokens() throws IOException {
		TokenTable table = new TokenTable(TABLE_PATH);
		assertTrue(table.concernHasToken("Messages and monitoring", "plottools"));
		assertTrue(table.concernHasToken("Messages and monitoring", "display"));
		assertFalse(table.concernHasToken("Messages and monitoring", "Asterix"));

		assertTrue(table.concernHasToken("I/O data", "imwrite"));
		assertTrue(table.concernHasToken("I/O data", "fwrite"));
		assertFalse(table.concernHasToken("I/O data", "Asterix"));

		assertTrue(table.concernHasToken("Verification of function arguments and return values", "nargchk"));
		assertTrue(table.concernHasToken("Verification of function arguments and return values", "varargout"));
		assertFalse(table.concernHasToken("Verification of function arguments and return values", "Asterix"));

		assertTrue(table.concernHasToken("Data type verification and specialization", "int8"));
		assertTrue(table.concernHasToken("Data type verification and specialization", "size"));
		assertFalse(table.concernHasToken("Data type verification and specialization", "Asterix"));

		assertTrue(table.concernHasToken("System", "pause"));
		assertTrue(table.concernHasToken("System", "batch"));
		assertFalse(table.concernHasToken("System", "Asterix"));

		assertTrue(table.concernHasToken("Memory allocation/deallocation", "clear"));
		assertTrue(table.concernHasToken("Memory allocation/deallocation", "global"));
		assertFalse(table.concernHasToken("Memory allocation/deallocation", "Asterix"));

		assertTrue(table.concernHasToken("Parallelization", "parfor"));
		assertTrue(table.concernHasToken("Parallelization", "pctRunDeployedCleanup"));
		assertFalse(table.concernHasToken("Parallelization", "Asterix"));

		assertTrue(table.concernHasToken("Dynamic properties", "eval"));
		assertTrue(table.concernHasToken("Dynamic properties", "inline"));
		assertFalse(table.concernHasToken("Dynamic properties", "Asterix"));
	}

	@Test
	public void testHasToken() throws IOException {
		TokenTable table = new TokenTable(TABLE_PATH);
		assertTrue(table.hasToken("plottools"));
		assertTrue(table.hasToken("display"));
		assertTrue(table.hasToken("imwrite"));
		assertTrue(table.hasToken("fwrite"));
		assertTrue(table.hasToken("nargchk"));
		assertTrue(table.hasToken("varargout"));
		assertTrue(table.hasToken("int8"));
		assertTrue(table.hasToken("size"));
		assertTrue(table.hasToken("pause"));
		assertTrue(table.hasToken("batch"));
		assertTrue(table.hasToken("clear"));
		assertTrue(table.hasToken("global"));
		assertTrue(table.hasToken("parfor"));
		assertTrue(table.hasToken("pctRunDeployedCleanup"));
		assertTrue(table.hasToken("eval"));
		assertTrue(table.hasToken("inline"));

		assertFalse(table.hasToken("Asterix"));
	}
}
