package cccfinder.token_classifier.lexical_analyser;

import static org.junit.Assert.*;

import org.junit.Test;

import cccfinder.token_classifier.lexical_analyser.Parser;
import cccfinder.token_classifier.lexical_analyser.Tag;

import java.io.File;
import java.io.IOException;

public class TestParser {
	private static String PATH = ".\\test_data\\KPMtools";

	public void testNumberOfFuncs(String mFile, int funcs, int vars, int args) throws IOException {
		File file = new File(PATH + "\\" + mFile);
		Parser parser = new Parser(file);
		parser.processMFile();
		assertEquals(funcs, parser.getFuncs().size());
		assertEquals(vars, parser.getVars().size());
		assertEquals(args, parser.getArgs().size());
	}

	@Test
	public void testWordCollection() throws IOException {
		testNumberOfFuncs("em_converged.m", 1, 6, 6);
	}

	@Test
	public void testLiveWordTagging() throws IOException {
		File file = new File(PATH + "\\" + "em_converged.m");
		Parser parser = new Parser(file);
		parser.processMFile();
		assertEquals(16, parser.numberOfLines());
		assertTrue(parser.hasFunction("em_converged"));
		assertFalse(parser.hasWordWithThisTag(Tag.ARG, "em_converged"));

		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "converged"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "decrease"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "loglik"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "previous_loglik"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "threshold"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "check_increased"));

		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "converged"));
		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "decrease"));
		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "loglik"));
		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "previous_loglik"));
		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "threshold"));
		assertFalse(parser.hasWordWithThisTag(Tag.VAR, "check_increased"));

		assertFalse(parser.hasFunction("converged"));
		assertFalse(parser.hasFunction("decrease"));
		assertFalse(parser.hasFunction("loglik"));
		assertFalse(parser.hasFunction("previous_loglik"));
		assertFalse(parser.hasFunction("threshold"));
		assertFalse(parser.hasFunction("check_increased"));

		assertTrue(parser.hasFunction("nargin"));
		assertTrue(parser.hasFunction("abs"));
	}

	@Test
	public void testHeaderParsing() {
		String text = "function [converged, decrease] = em_converged(loglik, previous_loglik, threshold, check_increased)";
		Parser parser = new Parser(text);
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "converged"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "decrease"));
		assertTrue(parser.hasWordWithThisTag(Tag.FHD, "em_converged"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "loglik"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "previous_loglik"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "threshold"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "check_increased"));
	}

	@Test
	public void testParsing() {
		String text =
			"function nedgelist = cleanedgelist(edgelist, minlength)" + "\n" +
			"for i = 1:length(linkingedges)" + "\n" +
			"spurs = [spurs linkingedges(i)];" + "\n" +
			"len = [len edgelistlength(edgelst{linkingedges(i)})];";

		Parser parser = new Parser(text);
		assertTrue(parser.hasWordWithThisTag(Tag.FHD, "cleanedgelist"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "nedgelist"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "edgelist"));
		assertTrue(parser.hasWordWithThisTag(Tag.ARG, "minlength"));
		assertTrue(parser.hasWordWithThisTag(Tag.VAR, "i"));
		assertTrue(parser.hasWordWithThisTag(Tag.FUN, "length"));
		assertTrue(parser.hasWordWithThisTag(Tag.VAR, "spurs"));
		assertTrue(parser.hasWordWithThisTag(Tag.FUN, "linkingedges"));
		assertTrue(parser.hasWordWithThisTag(Tag.VAR, "len"));
		assertTrue(parser.hasWordWithThisTag(Tag.FUN, "edgelistlength"));
		assertTrue(parser.hasWordWithThisTag(Tag.FUN, "edgelst"));
		assertTrue(parser.hasWordWithThisTag(Tag.FUN, "linkingedges"));
	}
}
