package cccfinder.token_classifier.lexical_analyser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ListIterator;

public class TestLexicalAnalyser {
	private static String PATH = "test_data\\KPMtools";

	public void testNumberOfLines(String mFile, int expected) throws IOException {
		LexicalAnalyser lexer = new LexicalAnalyser(new File(PATH + "\\" + mFile));
		lexer.analyseMFile();
		assertEquals(expected, lexer.numberOfLines());
	}

	@Test
	public void testNumberOfLines() throws IOException {
		testNumberOfLines("div.m", 2);
		testNumberOfLines("factorial.m", 6);
		testNumberOfLines("sample_discrete.m", 22);
		testNumberOfLines("plotcov2.m", 31);
		testNumberOfLines("hungarian.m", 194);
	}

	public void testNumberOfTokens(String mFile, int expected) throws IOException {
		LexicalAnalyser lexer = new LexicalAnalyser(new File(PATH + "\\" + mFile));
		lexer.analyseMFile();
		assertEquals(expected, lexer.numberOfTokens());
	}

	@Test
	public void testNumberOfTokens() throws IOException {
		testNumberOfTokens("div.m", 18);
		testNumberOfTokens("factorial.m", 28);
		testNumberOfTokens("sample_discrete.m", 143);
	}

	@Test
	public void testProcessingAT() {
		LexicalAnalyser lexer = new LexicalAnalyser();
		lexer.analyseLine("h = @func1");
		assertTrue(lexer.contains("func1"));
		assertTrue(lexer.contains("h"));
	}
	@Test
	public void testNumberWithFloatingPoint() {
		LexicalAnalyser lexer = new LexicalAnalyser();
		assertFalse(lexer.containsNumber("0"));
		assertFalse(lexer.containsNumber("1.1"));
		assertFalse(lexer.containsNumber("2"));
		assertFalse(lexer.containsNumber("2.95"));
		assertFalse(lexer.containsNumber("3.78"));
		assertFalse(lexer.contains("y"));

		lexer.analyseLine("y = [0 1.1 2 2.95 3.78];");
		assertTrue(lexer.containsNumber("0"));
		assertTrue(lexer.containsNumber("1.1"));
		assertTrue(lexer.containsNumber("2"));
		assertTrue(lexer.containsNumber("2.95"));
		assertTrue(lexer.containsNumber("3.78"));
		assertTrue(lexer.contains("y"));
	}

	@Test
	public void testNumberWithENotation() {
		LexicalAnalyser lexer = new LexicalAnalyser();
		assertFalse(lexer.containsNumber("5.3009E-4"));
		assertFalse(lexer.containsNumber("1.6483E-2"));
		assertFalse(lexer.contains("B1"));
		assertFalse(lexer.contains("T"));

		lexer.analyseLine("B1 = (-5.3009E-4*T+1.6483E-2).*T+7.944E-2;");
		assertTrue(lexer.containsNumber("5.3009E-4"));
		assertTrue(lexer.containsNumber("1.6483E-2"));
		assertTrue(lexer.contains("B1"));
		assertTrue(lexer.contains("T"));
	}

	@Test
	public void testHeaderProcessing1() {
		//we should try others, including this:
		//function [h,utip]=do_it(smult,nu, mf,blf,io)

		LexicalAnalyser lexer = new LexicalAnalyser();
		assertFalse(lexer.contains("function"));
		assertFalse(lexer.contains("count"));
		assertFalse(lexer.contains("compute_counts"));
		assertFalse(lexer.contains("data"));
		assertFalse(lexer.contains("sz"));

		lexer.analyseLine("function count = compute_counts(data, sz)");

		assertTrue(lexer.contains("function"));
		assertTrue(lexer.contains("count"));
		assertTrue(lexer.contains("compute_counts"));
		assertTrue(lexer.contains("data"));
		assertTrue(lexer.contains("sz"));

		assertEquals(9, lexer.numberOfTokens());
	}

	static void printTokens(LexicalAnalyser lexer) {
		ListIterator<Token> it = lexer.listIterator();
		System.out.println();
		while(it.hasNext())
			System.out.print(it.next() + " ");
		System.out.println();
	}

	@Test
	public void testSpecificCase1() throws IOException {
		File file = new File(PATH + "\\nchoose2.m");
		LexicalAnalyser lexer = new LexicalAnalyser(file);
		lexer.analyseMFile();

		assertFalse(lexer.contains("d"));
		assertFalse(lexer.contains("x"));
		assertFalse(lexer.contains("while"));

		assertTrue(lexer.contains("function"));
		assertTrue(lexer.contains("c"));
		assertTrue(lexer.contains("nchoose2"));
		assertTrue(lexer.contains("v"));
		assertTrue(lexer.contains("f"));
		assertTrue(lexer.contains("nargs"));
		assertTrue(lexer.contains("nargin"));
		assertTrue(lexer.contains("error"));
		assertTrue(lexer.contains("length"));
		assertTrue(lexer.contains("end"));
		assertTrue(lexer.contains("elseif"));

//		printTokens(lexer);
	}

	@Test
	public void testHeaderProcessing2() {
		//range_find(pi, 'cos2.pi'); %range find call
		LexicalAnalyser lexer = new LexicalAnalyser();
		assertFalse(lexer.contains("range_find"));
		assertFalse(lexer.contains("pi"));

		lexer.analyseLine("range_find(pi, 'cos2.pi'); %range find call");

		assertTrue(lexer.contains("range_find"));
		assertTrue(lexer.contains("pi"));

		assertEquals(7, lexer.numberOfTokens());
	}

	@Test
	public void testSpecificCase2() throws IOException {
		LexicalAnalyser lexer = new LexicalAnalyser(new File(PATH + "\\cos1_custom.m"));
		lexer.analyseMFile();
		assertFalse(lexer.contains("range_find"));
		assertTrue(lexer.contains("sin1_custom"));
	}

}
