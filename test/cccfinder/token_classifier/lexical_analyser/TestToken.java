package cccfinder.token_classifier.lexical_analyser;

import static org.junit.Assert.*;
import org.junit.Test;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Token;

public class TestToken {
	@Test
	public void testClone() {
		Token keyword = new Token(LexicalElem.BREAK, "break");
		Token clone = keyword.clone();
		assertNotSame(clone, keyword);
		assertEquals(keyword.type(), clone.type());

		assertEquals(keyword.getValue(), clone.getValue());
		assertNotSame(keyword.getValue(), clone.getValue());

		assertEquals(keyword.getTag(), clone.getTag());
	}
}
