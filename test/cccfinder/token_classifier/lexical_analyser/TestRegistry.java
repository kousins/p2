package cccfinder.token_classifier.lexical_analyser;

import static org.junit.Assert.*;
import org.junit.Test;

import cccfinder.token_classifier.lexical_analyser.Token;
import cccfinder.tool.TokenStatisticsRegistry;
import cccfinder.tool.TokenWithStatistics;

public class TestRegistry {
	@Test
	public void testRegistry() {
		TokenStatisticsRegistry ifr = new TokenStatisticsRegistry();
		ifr.register(new Token("asterix"));
		ifr.register(new Token("asterix"));
		ifr.register(new Token("asterix"));

		ifr.register(new Token("ideafix"));

		ifr.register(new Token("obelix"));
		ifr.register(new Token("obelix"));

		for(TokenWithStatistics token : ifr) {
			switch(token.getValue()) {
			case "asterix": assertEquals(3, token.getCount()); break;
			case "ideafix": assertEquals(1, token.getCount()); break;
			case "obelix": 	assertEquals(2, token.getCount()); break;
			default:
				fail("Found unexpected function name " + token.getValue());
			}
		}
	}
}
