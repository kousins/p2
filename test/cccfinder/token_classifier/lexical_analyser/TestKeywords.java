package cccfinder.token_classifier.lexical_analyser;

import static org.junit.Assert.*;
import org.junit.Test;

import cccfinder.token_classifier.lexical_analyser.LexicalElem;
import cccfinder.token_classifier.lexical_analyser.Parser;
import cccfinder.token_classifier.lexical_analyser.Token;


public class TestKeywords {
	@Test
	public void testIsFunction() {
		Token token = new Token(LexicalElem.BREAK, "break");
		assertTrue(Parser.isKeyword(token));
	}
}
