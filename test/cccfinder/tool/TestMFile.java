package cccfinder.tool;

import org.junit.Before;
import org.junit.Test;

import util.ProgressDisplay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

public class TestMFile {
	private MFile mf;

	@Before
	public void setUp() throws FileNotFoundException {
		ProgressDisplay.progressDisplayOff();
		mf = new MFile();
	}

	@Test
	public void testMFileCreation() {
		assertEquals(0, mf.nrLines());
		assertEquals(0, mf.nrTokens());
		assertEquals(0, mf.wordCount());
		assertTrue(mf.hasZeroLoC());
	}
}
