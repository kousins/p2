package cccfinder.tool;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import cccfinder.metrics.TokenDensityConcern;
import cccfinder.token_classifier.token_table.TokenTable;

public class TestTokenDensityConcern implements ICommonTest{
	
	private static final String TOKEN_TABLE_PATH =
			"dat\\token_table_v4.txt";
	CollectedData data;
	TokenDensityConcern cf;
	TokenTable table;
	Toolbox tb;
	
	@Before
	public void setUp() throws Exception {
		data = new CollectedData(SAMP_SW_PATH);
		cf = new TokenDensityConcern();
		table = new TokenTable(TOKEN_TABLE_PATH);
		tb = data.getTopToolbox();
	}

	@Test
	public void test() throws IOException {
		
		//1� valor esperado : 2� chamada ao m�todo
		
		//1� ficheiro - glmfitting.m
		//has 0 Vfarv tokens
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(0), table, "1- Verification of function arguments and return values"));
		//has 9 Data type verification tokens ('size')
		assertEquals(9, cf.getTokenDensityConcern(tb.getMFile(0), table, "3- Data type verification"));
		//has 8 Console messages tokens ('error')
		assertEquals(8, cf.getTokenDensityConcern(tb.getMFile(0), table, "5- Console messages"));
		
		//2� ficheiro - glmpredict.m
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(1), table, "1- Verification of function arguments and return values"));
		assertEquals(2, cf.getTokenDensityConcern(tb.getMFile(1), table, "3- Data type verification"));
		//has 2 Console messages tokens ('error')
		assertEquals(2, cf.getTokenDensityConcern(tb.getMFile(1), table, "5- Console messages"));
		
		//3� ficheiro - glmtest.m
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(2), table, "1- Verification of function arguments and return values"));
		//has 0 Data type verification tokens
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(2), table, "3- Data type verification"));
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(2), table, "5- Console messages"));
		
		//test non zero results with last m-file
		//has 12 Visualization tokens
		assertEquals(12, cf.getTokenDensityConcern(tb.getMFile(2), table, "7- Visualization"));
		//has 1 System token ('clear')
		assertEquals(1, cf.getTokenDensityConcern(tb.getMFile(2), table, "9- System"));
		
		//4� ficheiro - zzzzzz.m
		assertEquals(4, cf.getTokenDensityConcern(tb.getMFile(3), table, "1- Verification of function arguments and return values"));
		assertEquals(5, cf.getTokenDensityConcern(tb.getMFile(3), table, "3- Data type verification"));
		assertEquals(0, cf.getTokenDensityConcern(tb.getMFile(3), table, "5- Console messages"));
	}

}
