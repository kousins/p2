package cccfinder.tool;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import cccfinder.tool.CollectedData;
import cccfinder.tool.MFile;
import util.ProgressDisplay;

public class TestToolbox implements ICommonTest {

	@Before
	public void setUp() {
		ProgressDisplay.progressDisplayOff();
	}

	@Test
	public void testMFileIterator() throws FileNotFoundException, IOException {
		CollectedData data = new CollectedData(SAMP_SW_PATH);
		assertEquals(4, data.totalMFiles());

		Iterator<MFile> it = data.mFileIterator();
		assertEquals(".\\test_data\\samp-sw-generalized_linear_models\\glmfitting.m", it.next().getPathname());
		assertEquals(".\\test_data\\samp-sw-generalized_linear_models\\glmpredict.m", it.next().getPathname());
		assertEquals(".\\test_data\\samp-sw-generalized_linear_models\\glmtest.m", it.next().getPathname());
		assertEquals(".\\test_data\\samp-sw-generalized_linear_models\\z.m", it.next().getPathname());
	}

	@Test
	public void testMFileIteratorBIG() throws FileNotFoundException, IOException {
		CollectedData data = new CollectedData(KPMTOOLS_PATH);

		Iterator<MFile> it = data.mFileIterator();
		//Test the first four pathnames:
		String s1 = it.next().getPathname();
		assertEquals(".\\test_data\\KPMtools\\approxeq.m", s1);
		String s2 = it.next().getPathname();
		assertEquals(".\\test_data\\KPMtools\\approx_unique.m", s2);
		String s3 = it.next().getPathname();
		assertEquals(".\\test_data\\KPMtools\\argmax.m", s3);
		String s4 = it.next().getPathname();
		assertEquals(".\\test_data\\KPMtools\\argmin.m", s4);

		while(it.hasNext()) {
			MFile mf = it.next();
			s1 = s2; s2 = s3; s3 = s4; s4 = mf.getPathname();
		}
		//Test the last four pathnames:
		assertEquals(".\\test_data\\KPMtools\\wrap.m", s1);
		assertEquals(".\\test_data\\KPMtools\\xticklabel_rotate90.m", s2);
		assertEquals(".\\test_data\\KPMtools\\zipload.m", s3);
		assertEquals(".\\test_data\\KPMtools\\zipsave.m", s4);
	}

}
