/**
 * 
 */
package cccfinder.tool;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cccfinder.token_classifier.lexical_analyser.Token;


/**
 * @author Bruno Palma LT
 *
 */
public class TestMFunction {

	/**
	 * @throws java.lang.Exception
	 */
	MFunction mf;
	
	@Before
	public void setUp() throws Exception {
		mf = new MFunction();
	}

	@Test
	public void testBrunoPalma() {
		assertEquals(mf.getName(),null);
		mf.setName("cos");
		assertNotSame(mf.getName(), "cos1");
		mf.setMFileName("cos1.m");
		assertEquals(mf.getMFileName(),"cos1.m");
		mf.setDomainName("math");
		assertEquals(mf.getDomainName(),"math");
		assertTrue(mf.getTokenCount() == 0);
		Token tk = new Token(null);
		mf.addToken(tk);
		assertTrue(mf.getTokenCount() == 1);
	}

}
