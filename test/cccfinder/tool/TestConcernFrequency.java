package cccfinder.tool;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import cccfinder.metrics.ConcernFrequency;
import cccfinder.token_classifier.token_table.TokenTable;

public class TestConcernFrequency implements ICommonTest{
	
	private static final String TOKEN_TABLE_PATH = "dat\\token_table.txt";
	CollectedData data;
	ConcernFrequency cf;
	TokenTable table;
	Toolbox tb;
	
	@Before
	public void setUp() throws Exception {
		data = new CollectedData(SAMP_SW_PATH);
		cf = new ConcernFrequency();
		table = new TokenTable(TOKEN_TABLE_PATH);
		tb = data.getTopToolbox();
	}

	@Test
	public void test() throws IOException {
		
		MFile file1 = tb.getMFile(0);
		MFile file2 = tb.getMFile(1);
		MFile file3 = tb.getMFile(2);
		
		int val1 = cf.getConcernFrequencyMFile(file1, table, "Messages and monitoring");
		int val2 = cf.getConcernFrequencyMFile(file2, table, "I/O data");
		int val3 = cf.getConcernFrequencyMFile(file3, table, "Verification of function arguments and return values");
		
		System.out.println(val2);
		
		assertTrue(val1 >= 0);
		assertEquals(val2, 0);
		assertTrue(val3 >= 0);
		
	}

}
