package cccfinder.tool;

/**
 * This interface holds a number of constants common to several test classes. 
 * @author Miguel P. Monteiro
 */
public interface ICommonTest {
	public static final String KPMTOOLS_PATH = ".\\test_data\\KPMtools";
	public static final String KPMTOOLS_NAME = "KPMtools_test";
	public static final String SAMP_SW_PATH = ".\\test_data\\samp-sw-generalized_linear_models";
	public static final String SAMP_SW_NAME = "samp-sw_test";
	
	public static final String WORDS_TXT_FILENAME = ".\\test_data\\words.txt";

	public static final int WORD_COUNT_LIMIT = 50;
	/**
	 * Path to the directory (folder) from where the applications and toolboxes can be found.
	 */
	public static final String FOLDER_ROOT = "pipiverde"; //"C:\\util\\Dropbox";

}
